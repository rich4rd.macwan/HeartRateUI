function metrics(name)
method='PYR';
ploton=true;
%For individual subject
% name='subject26'
fullrppgData=loadRPPGData(method);
for i=1:size(fullrppgData,1)
    if(strcmp(fullrppgData(i,1).name,name)==1 || strcmp(fullrppgData(i,2).name,name)==1 || strcmp(fullrppgData(i,3).name,name)==1 )
        loadeddata=fullrppgData(i,:);
        break;
    end
end

rppgData=[];
separated=[];
%For each type of video
for i=2:3
    % calculateHeartRate(loadeddata(i).rppgData,[name num2str(i)],true);
    if(ploton)
        hold on
        subplot(3,1,i);
    end
    % disp([name num2str(i)]);
    rppgData=loadeddata(i).rppgData;
   if(numel(rppgData)==0)
       continue;
   end
    %Skip first 15 seconds
    ts=rppgData(:,1);
    ts=ts-ts(1);
   % rppgData=rppgData(ts(:,1)>15000,:);
    
    %plotfft(rppgData);
    if(strcmp(method,'PYR_ICA')==1)
        %TODO:
        %Perform ICA
        %Select the HR trace from the returned components with highest PSNR
        %plotfft(rppgData);
        separated=performICA(rppgData,false);
        calculateHeartRateICA(separated,rppgData,[name num2str(i)],ploton);
        
    else
        calculateHeartRate(rppgData,[name num2str(i)],ploton);
    end
    
    %timestamps=loadeddata(i).rppgData(:,1);
    
end
pause
close all

end