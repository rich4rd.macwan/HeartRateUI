function [A]=normalize(A)
	R=[0 1];
	dR = diff( R );

	A =  A - min( A(:)); % set range of A between [0, inf)
	A =  A ./ max( A(:)) ; % set range of A between [0, 1]
	A =  A .* dR ; % set range of A between [0, dRange]
	A =  A + R(1); % shift range of A to R
end