function plotfft(rppgData,separated)


%Resample the data
ts=rppgData(:,1);
ts=ts-ts(1);
Tmax=ts(end);
Fs=Tmax/numel(ts);


t=0:Fs:Tmax;
y=interp1(ts,rppgData(:,6),t);
psd(t,y,Fs);
averageHRGTrec=mean(rppgData(:,2))
for i=1:size(separated,1)
    y=interp1(ts,separated(i,:),t);
    psd(t,y,Fs);
    
end

end

function psd(t,y,Fs)
y=y-mean(y);


%Fs = 1000; % sampling frequency 1 kHz
%t = 0 : 1/Fs : 0.296; % time scale
%f = 100; % Hz, embedded dominant frequency
%x = cos(2*pi*f*t) + randn(size(t)); % time series
x=y;
plot(t,x), axis('tight'), grid('on'), title('Time series'), figure

nfft = 512; % next larger power of 2
y = fft(x,nfft); % Fast Fourier Transform
y = abs(y.^2); % raw power spectrum density
y = y(1:1+nfft/2); % half-spectrum

f_scale = (0:nfft/2)* Fs/nfft; % frequency scale
f_scale=f_scale*60;

y(f_scale>200 | f_scale<50)=[];
f_scale(f_scale>200 | f_scale<50)=[];


[v,k] = max(y); % find maximum
plot(f_scale, y);

axis('tight'),grid('on'),title('Dominant Frequency')
f_est = f_scale(k); % dominant frequency estimate
fprintf('Dominant freq.: estimated %f bpm, max power=%f\n', f_est,v)
fprintf('Frequency step (resolution) = %f Hz\n', f_scale(2))

% 	% format long
% 	 y=rppgData(:,6);
% 	 y=y-mean(y);
% 	 t=rppgData(:,1)-rppgData(1,1);
% 	 t=t/60000;
%
% 	avgHRGTRec=mean(rppgData(:,2))
% 	% %LOMB Periodogram
% 	[f,P,prob] = lomb(t,y,4,1);
%
% 	[Pmax,jmax] = max(P);
% 	f0=f(jmax);
% 	pmax_freqbpm=[Pmax f0]
% 	hold on
% 	plot(f,P,f0,Pmax,'o')
% 	% disp(['Most significant period is',num2str(1/f(jmax)),' with FAP of ',num2str(prob(jmax))])
% 	xlabel('Heart rate - bpm')
% 	title('Lomb periodogram')
%
% 	% pause
end