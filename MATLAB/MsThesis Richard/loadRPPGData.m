function [fullrppgData] =loadRPPGData(suffix)
	%Read files from the dump directory and for each plot the results
	if(strcmp(suffix,'PYR_ICA')==1)
		suffix='PYR';
	end
	spath=['../data/evaluation/' suffix];
	addpath(spath);
	overwrite=true;
	if(exist(['fullrppgData' suffix '.mat']))
		str=input(['Previous rppg data file found: fullrppgData' suffix '.mat ! Overwrite?y/N  \n'],'s');

		if(isempty(str) || ~(str=='y' || str=='Y'))
			overwrite=false;
		else 
			overwrite=true;
		end
	else
		
	end

	if(overwrite)
		files=dir(spath);
		%Create the matrix for rppgData
		%nx3 matrix of rppgVideoData structs
		%First get the highest number of index
		indices=[];
		prefix=files(3).name(1:end-7);
		ext=files(3).name(end-3:end);
		for n=1:numel(files)
				indices=[indices str2num(files(n).name(17:end-5))];
		end
		indices=sort(unique(indices));
		fullrppgData=struct([]);
		%Alphabetical order
		for n=1:numel(indices)
			for i=1:3
				if(indices(n))<10
                    filename=[prefix num2str(0) num2str(indices(n)) num2str(i) ext];
					filepath=[spath '/' prefix num2str(0) num2str(indices(n)) num2str(i) ext];
                else
                    filename=[prefix num2str(indices(n)) num2str(i) ext];
					filepath=[spath '/' prefix num2str(indices(n)) num2str(i) ext];
				end
				
				disp(['Reading ' filename]);
				%load each rppgVideoData in a struct
				if(exist(filepath))
					 fullrppgData(n,i).rppgData=csvread(filepath);
					 fullrppgData(n,i).name=filename(10:18);
				else
					disp(['File: ' filename ' not present. You might need to rerun the evalutaion(qt app).']);
				end
			end
		end
		
		save(['fullrppgData' suffix],['fullrppgData']);
		
		%rppgdump file format:timestamp,HRGT,rrpgsignal[3],gtwaveform
		% filename='rppgdump-subject341.txt';
		% rppgData=csvread(filename); 
	else
		disp([ 'Loading fullrppgData' suffix ' from file'])
		load(['fullrppgData' suffix]);
	end
end