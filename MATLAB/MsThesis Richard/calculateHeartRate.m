function[averageHR1 averageHRGT avgdiffHR NRMSE correlationCoefficient] =calculateHeartRate(rppgData,subjectname,ploton)
%subjectname is the name of the subject
%ploton is true or false
chnG=4; %Only extract green trace
averageHR1=0;
avgdiffHR=0;
NRMSE=0;
correlationCoefficient=0;
averageHRGT=0;

if(ploton)
    
    
    set(gcf,'Units','normal');
    % set(gca,'Position',[0 0 1 1]);
    set(gca,'ButtonDownFcn','selectmoveresize');
    set(gcf,'WindowButtonDownFcn','selectmoveresize'); %optional
    hold on;
    
    % subplot(3,1,1);
end
%Peak detection
%Scale the rppg signal to match the ppg signal

normalizedPPG=normalize(rppgData(:,6));
%Green channel
normalizedRPPG=normalize(rppgData(:,chnG));

%	x=1:size(normalizedRPPG);
x=rppgData(:,1);
x=x-x(1);
%     xshifted=[x(2:end);0];
%      x=xshifted-x;
%      x(end)=30;
x=x/1000;

[pksgt,locsgt] =findpeaks(normalizedPPG,'MinPeakDistance',10);

[pks,locs] = findpeaks(normalizedRPPG,'MinPeakDistance',10);


calculatedHR=zeros(size(rppgData,1),2);

%Plot RPPG signal
if(ploton)
    %plot(x,normalizedPPG,'g-'); grid on
  %  plot(x,normalizedPPG,'g-',x,normalizedRPPG,'r'); grid on
    
    %plot(x,normalizedRPPG,x(locs),pks,'rv','MarkerFaceColor','r'); grid on
    plot(x,normalizedPPG,x(locsgt),pksgt,'rv','MarkerFaceColor','r'); grid on
    
    % xlabel('Frame no.'); ylabel('Sunspot Number')
    % legend('RPPG','Peaks')
    % hold on
    % title(['RPPG-' subjectname])
    
end

for c=1:2
    %Iterate through signal peaks using a sliding window
    winSize=900;
    totalDelta=0;
    count=0;
    HR=0;
    avgIBI=0;
    for i=1:size(rppgData,1)
        totDeltaT=0;
        if(i+winSize) <size(rppgData,1)
            window=locs(locs>=i & locs<=i+winSize);
        else
            window=locs(locs>=i & locs<=size(rppgData,1));
        end
        % numel(window)
        % pause
        % for j=1:numel(window)-1
        % %Calculate IBI between every second peak in this window
        % 	ts1=rppgData(window(j),1);
        % 	ts2=rppgData(window(j+1),1);
        % 	totDeltaT=totDeltaT+(ts2-ts1);
        % 	totalDelta=totalDelta+(ts2-ts1);
        % 	count=count+1;
        % end
        % if ~(i+winSize>numel(calculatedHR) )
        % 	if numel(window) ~= 0
        % 		avgIBI=totDeltaT/numel(window);
        % 	end
        % end
        % if(avgIBI ~=0 )
        % 	HR=60000/avgIBI;
        % end
        % calculatedHR(i,1)=HR/2;
        if(numel(window)>5)
            ts1=rppgData(window(1),1);
            ts2=rppgData(window(numel(window)),1);
            totDeltaT=ts2-ts1;
            avgIBI=totDeltaT/numel(window);
            %[totDeltaT numel(window)]
            if(avgIBI ~=0)
                if(c==1)
                    HR=60000/(avgIBI);
                else
                    HR=60000/avgIBI;
                end
                
            end
        end
        calculatedHR(i,c)=HR;
        
    end
    
    calculatedHR(:,c)=smooth([1:size(calculatedHR,1)],calculatedHR(:,c),0.1,'loess');
    % calculatedHR(:,c)'
    %Plot Heart rate
    if(false)
        if c==1
            plot(calculatedHR(:,c),'r--');
        else
            plot(calculatedHR(:,c),'b-');
        end
    end
    
    locs=locsgt;
    pks=pksgt;
end
%Plot gt rec heart rate here
if(false)
    plot(rppgData(:,2),'g-');
    legend('HR','HR-GT','HR-GTR')
    title('\color{red}Calculated HR,\color{blue}Calculated Ground truth HR,\color{green} Recorded GT')
end
averageHR1=mean(calculatedHR(:,1));
averageHRGT=mean(calculatedHR(:,2));
averageHRGTrec=mean(rppgData(:,2));


[pksgt,locsgt] =findpeaks(normalizedPPG,'MinPeakDistance',10);
peakwidth=10;
if(strcmp(subjectname(end-2:end-1),'12')==1 || strcmp(subjectname(end-2:end-1),'23')==1)
    peakwidth=15;
end
[pks,locs] = findpeaks(normalizedRPPG,'MinPeakDistance',peakwidth);


ibis=diff(rppgData(locs,1));
ibisgt=diff(rppgData(locsgt,1));


averageHR1=60000/mean(ibis);
averageHRGT=60000/mean(ibisgt);


avgdiffHR= averageHR1-averageHRGT;


disp('                ');
% averageGTHR=mean(rppgData(:,2));

%NRMSE=sqrt(sum(rppgData(:,6)-rppgData(:,chnG)).^2)/size(rppgData,1);
NRMSE=sqrt(sum(calculatedHR(:,2)-calculatedHR(:,1)).^2)/size(rppgData,1);

%Pearson correlation coefficient between HR and HR-GT
[r,p]=corrcoef(calculatedHR)


correlationCoefficient=r(1,2)
disp('Avg HR          Avg HR GT      Avg HR GTrec   Difference  Corr-Coeff(R)');
disp('---------------------------------------------------------------------------');
disp([num2str(averageHR1) '          ' num2str(averageHRGT) '          ' num2str(averageHRGTrec) '          ' num2str(avgdiffHR) '          ' num2str(correlationCoefficient)]);


end

function [A]=normalize(A)
R=[0 1];
dR = diff( R );

A =  A - min( A(:)); % set range of A between [0, inf)
A =  A ./ max( A(:)) ; % set range of A between [0, 1]
A =  A .* dR ; % set range of A between [0, dRange]
A =  A + R(1); % shift range of A to R
end

function [IBIcleaned]=ncvt(ibis)
    N=numel(ibis);
    L=1;
    STEP=5;
    sum=0;
    un=5;
    um=5;
    IBIcleaned=zeros(size(ibis));
    M=mean(ibis);
    for i=2:N-1
        Un1=abs(ibis(i)-ibis(L))/ibis(L)
        Un2=abs(ibis(i)-ibis(i+1))/ibis(L)
        Um=abs(ibis(i)-M)/M
        if( (abs(ibis(i)-ibis(L))/ibis(L))<un || ...
            (abs(ibis(i)-ibis(i+1))/ibis(L))<un || ...
            abs(ibis(i)-M)/M<um)
            IBIcleaned(i)=ibis(i);
            if(i>=STEP && mod(i-2,STEP)==0)
                L=i;
                %update un,um, M
                M=mean(ibis(i-STEP:i));
                un=std(ibis(i-STEP:i))/M;
                um=un
                
            end
        end
    end
end