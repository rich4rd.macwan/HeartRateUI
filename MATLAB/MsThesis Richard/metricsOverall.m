method='PYR_ICA';
path=['../data/evaluation/' method]
addpath(path);


fullrppgData=loadRPPGData(method);
differences=zeros(size(fullrppgData,1),3);
avgHRs=zeros(size(fullrppgData,1),3);
avgHRGTs=zeros(size(fullrppgData,1),3);
nrmse=zeros(size(fullrppgData,1),3);
corrcoeffs=zeros(size(fullrppgData,1),3); 
ignorelist=[2,3,4,26,27]; %Based on skipping first 15 sec
for i=1:size(fullrppgData,1)
    
    if(numel(find(i==ignorelist))==1)
        disp(['Skipping subject' num2str(i) ' based on ignorelist']);
        continue;
    end
    clf
    %Check if the rppg data is present and was loaded
    if(numel(fullrppgData(i).rppgData)~=0)
        for n=2:3
            
            % calculateHeartRate(loadeddata(i).rppgData,name,false);
            filename=[fullrppgData(i,n).name num2str(n) ];
            %For each type of video
            disp(filename)
            rppgData=fullrppgData(i,n).rppgData;
            name=fullrppgData(i,n).name;
            %Skip first 15 seconds
            ts=rppgData(:,1);
            ts=ts-ts(1);
            rppgData=rppgData(ts(:,1)>15000,:);
            if(numel(rppgData)>0)
                if(strcmp(method,'PYR_ICA')==1)
                    %TODO:
                    %Perform ICA
                    %Select the HR trace from the returned components with highest PSNR
                     hold on
                     subplot(3,1,n);
                     %plotfft(rppgData)

                   separated=performICA(rppgData,false);
                    
                    [avgHR avgHRGT diffr NRMSE correlationCoefficient]=calculateHeartRateICA(separated,rppgData,[name num2str(i)],false);
                else
                    
                    [avgHR avgHRGT diffr NRMSE correlationCoefficient]=calculateHeartRate(rppgData,filename,true);
                    
                end
                differences(i,n)=diffr;
                nrmse(i,n)=NRMSE;
                avgHRs(i,n)=avgHR;
                avgHRGTs(i,n)=avgHRGT;
                corrcoeffs(i,n)=correlationCoefficient;
            end
        end
      
        
    end
end
disp('Subject no.     Diff1     Diff2     Diff3      Corr-Coeff');
disp('----------------------------------------------------------')
for i=1:size(differences,1)
    disp([fullrppgData(i).name '    ' ...
        num2str(differences(i,:))  ...
        '     ' num2str(corrcoeffs(i,:))])
end
differences= differences(differences(:,3)~=0,:);
avgHRs=avgHRs(avgHRs(:,3)~=0,:);
avgHRGTs=avgHRGTs(avgHRGTs(:,3)~=0,:);
nrmse=nrmse(nrmse(:,3)~=0,:);
corrcoeffs=corrcoeffs(corrcoeffs(:,3)~=0,:);

save(['results' method],'differences','avgHRs','avgHRGTs','nrmse','corrcoeffs');

%Only use the non zero values for evaluation metrics

mean_of_diffs=mean(differences)
stddev_of_diffs=std(differences)
mean_of_abs_diffs=mean(abs(differences))
% disp('Mean of NRMSE');
% disp('-------------------');
% mean(nrmse)
pause
close all

