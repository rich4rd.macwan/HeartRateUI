clear all;
close all;
clc;


addpath('../main-stable/');
addpath('../BlandAltman/');
addpath(genpath('../tools/tftb-0.2'));
addpath(genpath('../tools'));
%% Get traces

vidFolder = '/media/yannick/Data/Dataset/HeartRate/IUT_Dataset/subject38/vid2/';
%vidFolder = '/media/yannick/Data/Dataset/HeartRate/IUT_Dataset/subject20/vid2/';
%vidFolder = '/media/yannick/Data/Dataset/HeartRate/IUT_Dataset/subject17/vid2/';
%vidFolder = '/media/yannick/Data/Dataset/HeartRate/IUT_Dataset/subject17/vid2/';
%vidFolder = 'D:\Dataset\HeartRate\RichardSummerVideos\after-exercise\';
matFileName = 'pulseTraceGreen_skin';

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
WINDOW_LENGTH_SEC = 20; % length of the sliding window (in seconds)
STEP_SEC = 0.5; % step between 2 sliding window position (in seconds)
LOW_F=.7; % low freq for PSD range
UP_F=3; % high freq for PSD range
BEGIN_LENGTH_SEC = 0; % Begin after x seconds
SAVEFILES = 1;  
NBMAXCANDIDATES=3;

pulseFile = [vidFolder matFileName '.mat'];
   

if(exist(pulseFile, 'file') == 2)
    load(pulseFile);
else
    disp('oops, no input file');
    return;
end

traceSize = length(pulseTrace);

% get exact Fs
Fs = 1/mean(diff(timeTrace));

% load PPG
[gtTrace, gtHR, gtTime] = loadPPG(vidFolder);
gtTime = gtTime / 1000;  % in seconds

winLength = round(WINDOW_LENGTH_SEC*Fs); % length of the sliding window for FFT
step = round(STEP_SEC*Fs);

ind = 0;
for i=1:step:traceSize-winLength
    ind = ind+1;
    
    % get start/end index of current window
    startInd = i;
    endInd = i+winLength-1;
    startTime = timeTrace(startInd);
    endTime = timeTrace(endInd);
    
    % get current pulse window
    crtPulseWin = pulseTrace(startInd:endInd);
    crtTimeWin = timeTrace(startInd:endInd);
    
    % get current PPG window
    [~, startIndGt] = min(abs(gtTime-startTime));
    [~, endIndGt] = min(abs(gtTime-endTime));
    
    crtPPGWin = gtTrace(startIndGt:endIndGt);
    crtTimePPGWin = gtTime(startIndGt:endIndGt);
    
    % get exact PPG Fs
    Fs_PPG = 1/mean(diff(crtTimePPGWin));
    
    %%%%%%%%%%%%
    % with Welch
    %%%%%%%%%%%
    %     RPPG
    [rPPG_power,rPPG_freq] = pwelch(crtPulseWin,[],[],length(crtPulseWin)*3,Fs, 'psd'); % zero pading to increase the nb of points in the PSD
    rangeRPPG = (rPPG_freq>LOW_F & rPPG_freq < UP_F);   % frequency range to find PSD peak
    rPPG_power = rPPG_power(rangeRPPG);
    rPPG_freq = rPPG_freq(rangeRPPG);
    [pksrPPG,loc] = findpeaks(rPPG_power,'SortStr','descend','NPeaks',4); % get 3 first peaks
    rPPG_peaksLoc = rPPG_freq(loc);
    tfrFull(:,ind)=rPPG_power;
    
    %     PPG
    if(length(crtPPGWin) > 0) % sometime there is no PPG
        [PPG_power,PPG_freq] = pwelch(crtPPGWin,[],[],length(crtPPGWin)*3,Fs_PPG, 'psd');
        rangePPG = (PPG_freq>LOW_F & PPG_freq < UP_F); % frequency range to find PSD peak
        PPG_power = PPG_power(rangePPG);
        PPG_freq = PPG_freq(rangePPG);
        [pksPPG,loc] = findpeaks(PPG_power,'SortStr','descend','NPeaks',4); % get 2 first peaks
        PPG_peaksLoc = PPG_freq(loc);
    end
    
   
    if 0
        fig5 = figure(5);
        clf(fig5);
        subplot(1,2,1);
        hold on;grid on;
        plot(rPPG_freq,rPPG_power);
        plot(rPPG_peaksLoc(1), pksrPPG(1), 'r*');
        xlim([0.1 3.5]), title('rPPG Welch periodogram'), xlabel('Heart Rate (Hz)');
        hold off;
        
        subplot(1,2,2);
        hold on; grid on;
        plot(PPG_freq,PPG_power)
        plot(PPG_peaksLoc(1), pksPPG(1), 'r*');
        xlim([0.1 3.5]), title('PPG Welch periodogram'), xlabel('Heart Rate (Hz)');
        hold off;
        
        pause(0.1);
    end

      
    % save final results
    crtTime =  (startTime + endTime)/2;
    time(ind) = crtTime;
   
    for j=1:NBMAXCANDIDATES
        if j>length(rPPG_peaksLoc)
            tfr(ind,j,1) = 0;
            tfr(ind,j,2) = 0;  
        else
            tfr(ind,j,1) = rPPG_peaksLoc(j)*60;
            tfr(ind,j,2) = pksrPPG(j);            
        end
    end
end

rahrt_data_inst=rahrt_data(tfr, STEP_SEC);
i=size(tfr,1);
result=rahrt(i,rahrt_data_inst);

% we reconstruct the optimal path given result.q
% bestPath(1,:) -> Hz
% bestPath(2,:) -> Nrj
[~, idx]= max(result.D(:,i)) ;     
bestPath(1,i) = result.locs(idx,i);
bestPath(2,i) = result.pks(idx,i);
nextIdx = result.q(idx,i);
for k=size(tfr,1)-1:-1:1
  idx = nextIdx;
  nextIdx = result.q(idx,k);   
  bestPath(1,k) = result.locs(idx,k);
  bestPath(2,k) = result.pks(idx,k);
end
    
       
figure(1);
hold on
xlabel('T');
ylabel('F');
zlabel('abs(TFR)');
figure(1), mesh(time,rPPG_freq,tfrFull),axis xy,colormap('jet'),colorbar, xlabel('Time (s)'), ylabel('Frequency (Hz)'),xlim([time(1) time(end)]), ylim([rPPG_freq(1) rPPG_freq(end)]);
view(-50,70);
hold on;
plot3(time,bestPath(1,:)/60,bestPath(2,:),'r^','MarkerSize',10)
plot3(time,tfr(:,1,1)/60,tfr(:,1,2),'b*','MarkerSize',10)