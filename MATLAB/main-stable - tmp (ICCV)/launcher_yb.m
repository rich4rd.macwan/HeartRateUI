function launcher_yb(varargin)

addpath('D:\Recherche\Projects\HeartRate\Code\HeartRateUI\MATLAB\tools\export_fig');
close all;

%VIDFOLDER='D:\data\2017_01_16-10_51_55\';
VIDFOLDER='D:\Dataset\HeartRate\IUT_Dataset\subject32\vid2\';
%VIDFOLDER='D:\Dataset\HeartRate\RichardSummerVideos\5-gt\';
METHOD = 'Chrom';  % can be: Green, Chrom, PCA, ICA
POSTPROC='Heuristic'; % PostProcessing methods -> 'none','Heuristic','Kalman','DP'

%%%%%%
% Get RGB traces
%getTraceFromVidFile('VIDFOLDER', VIDFOLDER); 
%getTraceFromFileSeq('VIDFOLDER', VIDFOLDER); 

%%%%%%
% Get Pulse from RGB traces
getPulseSignalFromTrace('VIDFOLDER', VIDFOLDER, 'METHOD', METHOD, 'VERBOSE', 2);

%%%%%%
% Get HR from pulse
result = getHRFromPulse('VIDFOLDER', VIDFOLDER, 'POSTPROC', POSTPROC, 'VERBOSE', 2);

clc;
fprintf('Result for vid: %s \n', VIDFOLDER)
fprintf('SSE: %.2f \t', result{1})
fprintf('r: %.2f \n', result{2})
fprintf('precision at 2.5bpm: %.2f and at 5bpm: %.2f \n', result{3}, result{4})
fprintf('MAE %.2f \t', result{5})
fprintf('MAE(5bpm) %.2f \n', result{6})
fprintf('RMSE %.2f \t', result{7})
fprintf('CR %.2f \n', result{8})
fprintf('meanSNR %.2f \n', result{9})


