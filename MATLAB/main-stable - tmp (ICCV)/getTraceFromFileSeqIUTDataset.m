function [nFrames]=getTraceFromFileSeqIUTDataset(varargin)

close all;
%clc;

addpath('../tools/skindetector');

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
DO_FACECROP = 0;
DO_SKINDETECTION = 1;
SKIN_THRESH = -1; % threshold for skin detection (default: 0 - the lower the more skin we detect)
SHOWALLPLOTS = 0;
SAVESKINPIXELS = 1; % for 2SR (quite slow...)

%Videos are contained in individual folders
vidFolder = 'D:/IUT/subject17/vid2/';  
%fileName = 'subject013.vid';

%vidFolder = 'D:\Dataset\HeartRate\IUT_Dataset\subject05\vid3\';  

if DO_FACECROP==1
    outFileName = 'rgbTraces_crop';
elseif DO_SKINDETECTION==1
    outFileName = 'rgbTraces_skin';
else
    outFileName = 'rgbTraces';
end
outFileName = 'rgbTraces';

silentMode=false;

i = 1;  
while(nargin > i)  
    if(strcmp(varargin{i},'vidFolder')==1)  
        vidFolder=varargin{i+1};  
    end  
     if(strcmp(varargin{i},'outFolder')==1)
        outFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'silent')==1)
        silentMode=varargin{i+1};
    end
    if(strcmp(varargin{i},'countFrames')==1)
        countFrames=varargin{i+1};
    end
    i = i+2;  
end  

indexofsubject=strfind(vidFolder,'subject');
fileName=[vidFolder(indexofsubject:indexofsubject+8) vidFolder(indexofsubject+13) '.vid'];

fileid=[];
nRows=0;
nCols=0;
nChannels=0;

disp(['Get traces from file seq using: ' vidFolder]);

% Create a cascade detector object.
%faceDetector = vision.CascadeObjectDetector('ClassificationModel','ProfileFace','MinSize',[200 200]);
faceDetector = vision.CascadeObjectDetector('ClassificationModel','FrontalFaceCART','MinSize',[100 100]);
%faceDetector = vision.CascadeObjectDetector();

% Create the point tracker object.
pointTracker = vision.PointTracker('MaxBidirectionalError', 5);

% nb of frames
%Get number of frames from the file
fileid=fopen([vidFolder fileName],'rb');
%Read file header
A=fread(fileid,[1 5],'uint32');
%The second value is the number of frames. A(1) should be 43200 which
%we are not checking right now.
nbFiles=A(2);
nRows=A(3);
nCols=A(4);
nChannels=A(5);

rgbTraces = zeros(1+nChannels, nbFiles-1);

numPts = 0;
%hold on;
manualbbox=[];
t0 = [];
nbOxyLines = 1;

nFrames=nbFiles;
if countFrames==true
    nFrames=nbFiles;
    return;
end
for n=1:nbFiles
    %Read next frame from custom file
    %First read frame header
    frameHeader=fread(fileid,[1 3],'uint32');
    %Then read frame data
    img=fread(fileid,[nCols*nChannels nRows] ,'*uchar');
    img=cat(3,img(3:3:end,:)',img(2:3:end,:)',img(1:3:end,:)');
    
    img_copy = img;
    
    if numPts < 10
        % Detection mode.
        bbox  = step(faceDetector, img);
        if numel(bbox)==0
            if ~isempty(manualbbox)
                bbox=manualbbox;
            end
        end
        
        if numel(bbox)~=0
            % Find corner points inside the detected region.
            points = detectMinEigenFeatures(rgb2gray(img), 'ROI', bbox(1, :));
            
            % Re-initialize the point tracker.
            xyPoints = points.Location;
            numPts = size(xyPoints,1);
            release(pointTracker);
            initialize(pointTracker, xyPoints, img);
            
            % Save a copy of the points.
            oldPoints = xyPoints;
            
            % Convert the rectangle represented as [x, y, w, h] into an
            % M-by-2 matrix of [x,y] coordinates of the four corners. This
            % is needed to be able to transform the bounding box to display
            % the orientation of the face.
            x = bbox(1, 1);
            y = bbox(1, 2);
            w = bbox(1, 3);
            h = bbox(1, 4);
            bboxPoints = [x,y;x,y+h;x+w,y+h;x+w,y];
            
            % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
            % format required by insertShape.
            bboxPolygon = reshape(bboxPoints', 1, []);
            
            % Display a bounding box around the detected face.
            img = insertShape(img, 'Polygon', bboxPolygon);
            
            % Display detected corners.
            img = insertMarker(img, xyPoints, '+', 'Color', 'white');
        else
            %Face wasn't detected, manual roi mode
            imshow(img);
            title('Face detector did not detect a face. Please select one manually');
            
            rect= ginput(2);
            rect=round(rect);
            rect(2,:)=rect(2,:)-rect(1,:);
            manualbbox=reshape(rect',[1 4]);

        end
        
    else
        % Tracking mode.
        [xyPoints, isFound] = step(pointTracker, img);
        visiblePoints = xyPoints(isFound, :);
        oldInliers = oldPoints(isFound, :);
        
        numPts = size(visiblePoints, 1);
        
        if numPts >= 10
            % Estimate the geometric transformation between the old points
            % and the new points.
            [xform, oldInliers, visiblePoints] = estimateGeometricTransform(...
                oldInliers, visiblePoints, 'similarity', 'MaxDistance', 4);
            
            % Apply the transformation to the bounding box.
            bboxPoints = transformPointsForward(xform, bboxPoints);
             Min=min(bboxPoints);  
            Max=max(bboxPoints);  

            bbox=[Min(1) Min(2) Max(1)-Min(1) Max(2)-Min(2)];  
            % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]  
            % format required by insertShape.  
            bboxPolygon = reshape(bboxPoints', 1, []);  
            
            % Display a bounding box around the face being tracked.
            img = insertShape(img, 'Polygon', bboxPolygon);
            
            % Display tracked points.
            img = insertMarker(img, visiblePoints, '+', 'Color', 'white');
            
            % Reset the points.
            oldPoints = visiblePoints;
            setPoints(pointTracker, oldPoints);
        end
    end

    if ~isempty(bbox)
        if(SHOWALLPLOTS)
            figure(1),
            IFaces = insertObjectAnnotation(img, 'rectangle', bbox, 'Face');
            imshow(IFaces)
        end
        if(DO_FACECROP)
            % crop the face
            % horizontal cropping
            scaleW = 0.6;
            bbox(1,3) = bbox(1,3)*scaleW;
            bbox(1,1) = bbox(1,1) + bbox(1,3)*((1-scaleW)/2);
            % vertical cropping
            scaleV = 0.6;
            bbox(1,4) = bbox(1,4)*scaleV;
            bbox(1,2) = bbox(1,2) + bbox(1,4)*((1-scaleV)/2);
        end;
        
        
        if strcmp(vidFolder(end-4:end-1),'vid3')==1
            %Video 3, dont crop
            %imgROI =img_copy;
            imgROI = imcrop(img_copy,bbox(1,:));    
        else
            imgROI = imcrop(img_copy,bbox(1,:));    
        end
        
        if(SHOWALLPLOTS)
            figure(2), imshow(imgROI), title('Detected Face');
            pause(0.1);
        end
       
        if (DO_SKINDETECTION)
            
            % average RGB value of skin pixels
            imgD = double(imgROI);
            skinprob = computeSkinProbability(imgD);
            mask = skinprob>SKIN_THRESH;
            maskRGB = zeros(size(skinprob,1),size(skinprob,2),3);
            maskRGB(:,:,1) = mask;
            maskRGB(:,:,2) = mask;
            maskRGB(:,:,3) = mask;
            imgD = imgD.*maskRGB;
            skinPixels = imgD; %for 2SR
            imgROI(imgD==0)=nan;
            
            imgROI_nan = double(imgROI);
            imgROI_nan(imgD==0)=nan;
            [r,c,w]=size(imgROI_nan);
            imgROI_nan_reshaped = reshape(imgROI_nan(:),r*c,[]);
            avg = nanmean(imgROI_nan_reshaped);
                
        else
            avg = nanmean(nanmean(imgROI));
            
            % for 2SR copy imgROI to skinPixels
            skinPixels = imgROI;
        end;
        
        % save skin pixels for 2SR
        skinPixels = double(skinPixels);
        [r,c,w]=size(skinPixels);
        skinPixels = reshape(skinPixels(:),r*c,[]);
        % Remove the 0s
        skinPixels(all(skinPixels==0,2),:)=[];        
        % end of save skin pixels for 2SR
        
        
        if(SHOWALLPLOTS)
            figure(3);
            imshow(imgROI), title('Detected Face + skin');
            pause(0.1);
        elseif (mod(n,200) == 0)
            if ~silentMode
                figure(3), imshow(imgROI), title('Detected Face + skin');
                pause(.001);
            end
        end
        
        rgbTraces(1:nChannels,n) = avg;
        if (SAVESKINPIXELS)
            skinPixelsTraces{n,1,:} = skinPixels ; % save all pixels (slow)
        end
                
        %Get the timestamp from oximeter data in the file
        %Get number of oximeter rows
        time=fread(fileid,[1 1],'uint64');
        if (isempty(t0))
            t0 = time;
        end
        nOxi=fread(fileid,[1 1],'uint32');
        for o=1:nOxi
             oxirow(o,:)=fread(fileid,[1 4],'uint32');
             oxits=fread(fileid,[1 1],'uint64');
        end
        if(nOxi > 0)
            % we only take the last one
            t_tmp = time-t0; % gtTime
            hr_tmp = mean(nonzeros(oxirow(:,2))); % HR
            oxy_tmp = mean(nonzeros(oxirow(:,3))); % oxy
            ppg_tmp = mean(nonzeros(oxirow(:,4))); % ppg
            
        end
        gtdata(n, 1) = t_tmp; % gtTime
        if(isnan(hr_tmp)) % HR
            gtdata(n, 2) = gtdata(n-1, 2); 
        else
            gtdata(n, 2) = hr_tmp;
        end
        if(isnan(oxy_tmp)) % oxy
            gtdata(n, 3) = gtdata(n-1, 3); 
        else
            gtdata(n, 3) = oxy_tmp;
        end
        if(isnan(ppg_tmp)) % ppg
            gtdata(n, 4) = gtdata(n-1, 4); 
        else
            gtdata(n, 4) = ppg_tmp;
        end
        
        clear oxirow; 
            
        rgbTraces(1+nChannels,n) = time-t0;
        if (SAVESKINPIXELS)
            skinPixelsTraces{n,2,:} =  time-t0 ; % in case save all pixels (slow)
        end   
        
        if ~silentMode
            if (mod(n,200) == 0)
               fprintf('%i sur %i , timestamp %13i \n', n, nbFiles, time);
            end
        end
    end
end
save([vidFolder outFileName '.mat'], 'rgbTraces');
csvwrite([vidFolder  'gtdump.xmp'],gtdata)

disp(['Writing traces to: ' vidFolder outFileName '.mat']);

if(SAVESKINPIXELS)
   save([vidFolder 'allSkinPixels.mat'], 'skinPixelsTraces', '-v7.3'); 
end

%clear all
close all
end
