function [nFrames]= getTraceFromFileSeq(varargin)
% extract rPPG trace from image sequences
% face detection, tracking, skin detection, and spatial average
% argument can be a video folder (input / output)
% contact: yannick.benezeth@u-bourgogne.fr
% first version: 17/02/2016
tic
close all;
%clear all;
% clc;

addpath('../tools/skindetector');

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
USEFULL=0; % 0 or 1 - use full frame (no detection, tracking, etc...)
DO_FACECROP = 0; % 0 or 1 - use 60% of the detected face
DO_SKINDETECTION = 1; % 0 or 1
SKIN_THRESH = -1; % threshold for skin detection (default: 0 - the lower the more skin we detect)
SHOWALLPLOTS = 0;
FORCEMANUALSELECTION = 0;
SAVESKINPIXELS = 1; % for 2SR (quite slow...)

bmp_ppm_jpg=0; % 0,1 or 2 - 0 is ppm and 1 is bmp, 2 is jpg
silentMode=false;
vidFolder='D:/MMSE-HR/MMSE-HR/first 10 subjects 2D/F022/T1';
outFolder='D:/MMSE-HR/MMSE-HR/first 10 subjects 2D/F022/T1';
%vidFolder = '/run/media/richard/Le2iHDD/Le2i/roberto1-ueye/';
%vidFolder='/run/media/richard/841A-95A7/Le2i/synthetic/';
%vidFolder='/run/media/richard/LE2IHDD/CHU/patient5-normal2/';
%outFolder = vidFolder;
%vidFolder='../../../Videos/8-gt/';
% outFolder='./data/RichardSummer2015/after-exercise/';
countFrames=false;
i = 1;
while(nargin > i)
    if(strcmp(varargin{i},'vidFolder')==1)
        vidFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'outFolder')==1)
        outFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'DO_FACECROP')==1)
        DO_FACECROP=varargin{i+1};
    end
    if(strcmp(varargin{i},'DO_SKINDETECTION')==1)
        DO_SKINDETECTION=varargin{i+1};
    end
    if(strcmp(varargin{i},'keyword')==1)
        keyword=varargin{i+1};
    end
    if(strcmp(varargin{i},'silent')==1)
        silentMode=varargin{i+1};
    end
    if(strcmp(varargin{i},'SKIN_THRESH')==1)
        SKIN_THRESH=varargin{i+1};
    end
    if(strcmp(varargin{i},'countFrames')==1)
        countFrames=varargin{i+1};
    end
    if(strcmp(varargin{i},'bmp_ppm_jpg')==1)
        bmp_ppm_jpg=varargin{i+1};
    end
    i = i+2;
end

if DO_FACECROP==1
    keyword = 'crop';
elseif DO_SKINDETECTION==1
    keyword = 'skin';
end

if DO_SKINDETECTION==0 && DO_FACECROP==0
    keyword='';
end
keyword='';

disp(['Get RGB trace from image sequence here: ' vidFolder]);
disp(['outFolder: ' outFolder]);
%Default filename for the full face without skin or crop
if ~isempty(keyword)
    outFileName = ['rgbTraces_' keyword];
else
    outFileName='rgbTraces';
end

disp(outFileName);

% Create a cascade detector object.
faceDetector = vision.CascadeObjectDetector('ClassificationModel','FrontalFaceCART','MinSize',[100 100]);
%faceDetector = vision.CascadeObjectDetector('ClassificationModel','ProfileFace','MinSize',[100 100]);
% Create the point tracker object.
pointTracker = vision.PointTracker('MaxBidirectionalError', 5);

% nb of frames
if bmp_ppm_jpg==1
    nbFiles=length(dir([vidFolder 'img/*.bmp']));
elseif bmp_ppm_jpg==0
    nbFiles=length(dir([vidFolder 'img/*.ppm']));
elseif bmp_ppm_jpg==2
    nbFiles=length(dir([vidFolder '/*.jpg']));
end
 nFrames=nbFiles;
if countFrames==true
    nFrames=nbFiles;
    return;
end
nChannels=3;
rgbTraces = zeros(1+nChannels, nbFiles-1); % why minus 1 ?

figure, hold on;

numPts = 0;
manualbbox=[];
nLeadingZeros=floor(log10(nbFiles));
for n=1:nbFiles
    % read frame by frame number (maybe not ordered)
    imgPrefix='/img/';
    if bmp_ppm_jpg==1
        %imgName = dir([vidFolder '/img/Image_' num2str(n-1, '%.4d') '_*.bmp']); % just for bodySync v1
        imgName = dir([vidFolder '/img/frame_' num2str(n-1) '_*.bmp']);
    elseif bmp_ppm_jpg==0
        imgName = dir([vidFolder '/img/frame_' num2str(n-1) '_*.ppm']);
    elseif bmp_ppm_jpg==2
        %For MMSE database
        if n==1
            prefix=zeros(1,nLeadingZeros-floor(log10(n)));
        else
            prefix=zeros(1,nLeadingZeros-floor(log10(n-1)));
        end
        
        if isempty(prefix)
            prefix='';
        else
            prefix=mat2str(prefix);
        end
        %Remove the square brackets
        if numel(prefix) >1
            fname=regexprep([prefix(2:end-1) num2str(n-1)],'[^\w'']','');
        else
            fname=regexprep([prefix num2str(n-1)],'[^\w'']','');
        end
        
%         
%         if n<=10
%             fname=['000' fname];
%         elseif n<=100
%             fname=['00' fname];
%         elseif n<=1000
%             fname=['0' fname];
%         end
        
        imgName = dir([vidFolder '/' fname '.jpg']);
        imgPrefix='/';
     end
    if(size(imgName,1)==0)
        fprintf('oops, no image file \n');
        continue;
    end
    imgName = imgName.name;
    img = imread([vidFolder imgPrefix imgName]);
    
    img_copy = img;
    
    if ~ USEFULL
        % face localisation by detection then tracking
        if numPts < 10
            % Detection mode.
            if(~FORCEMANUALSELECTION)
                bbox  = step(faceDetector, img);
            else
                bbox = [];
            end
            
            if isempty(bbox)
                if ~isempty(manualbbox)
                    bbox=manualbbox;
                end
            end
            
            if (numel(bbox)~=0)
                % Find corner points inside the detected region.
                points = detectMinEigenFeatures(rgb2gray(img), 'ROI', bbox(1, :));
                
                % Re-initialize the point tracker.
                xyPoints = points.Location;
                numPts = size(xyPoints,1);
                release(pointTracker);
                initialize(pointTracker, xyPoints, img);
                
                % Save a copy of the points.
                oldPoints = xyPoints;
                
                % Convert the rectangle represented as [x, y, w, h] into an
                % M-by-2 matrix of [x,y] coordinates of the four corners. This
                % is needed to be able to transform the bounding box to display
                % the orientation of the face.
                x = bbox(1, 1);
                y = bbox(1, 2);
                w = bbox(1, 3);
                h = bbox(1, 4);
                bboxPoints = [x,y;x,y+h;x+w,y+h;x+w,y];
                
                % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
                % format required by insertShape.
                bboxPolygon = reshape(bboxPoints', 1, []);
                
                % Display a bounding box around the detected face.
                img = insertShape(img, 'Polygon', bboxPolygon);
                
                % Display detected corners.
                img = insertMarker(img, xyPoints, '+', 'Color', 'white');
            else
                %Face wasn't detected, manual roi mode
                imshow(img);
                title('Face detector did not detect a face. Please select one manually');
                rect= ginput(2);
                rect=round(rect);
                rect(2,:)=rect(2,:)-rect(1,:);
                manualbbox=reshape(rect',[1 4]);
                
                warning('Face detector did not detect a face. Choosing the whole image');
                
            end
        else
            % Tracking mode.
            [xyPoints, isFound] = step(pointTracker, img);
            %fprintf('%i %f\n', n, xyPoints(1))
            visiblePoints = xyPoints(isFound, :);
            oldInliers = oldPoints(isFound, :);
            
            numPts = size(visiblePoints, 1);
            %fprintf('%i %f\n', n, numPts)
            
            if numPts >= 10
                % Estimate the geometric transformation between the old points
                % and the new points.
                [xform, oldInliers, visiblePoints] = estimateGeometricTransform(...
                    oldInliers, visiblePoints, 'similarity', 'MaxDistance', 4);
                
                % Apply the transformation to the bounding box. [xi yi], 4x2
                bboxPoints = transformPointsForward(xform, bboxPoints);
                Min=min(bboxPoints);
                Max=max(bboxPoints);
                
                bbox=[Min(1) Min(2) Max(1)-Min(1) Max(2)-Min(2)];
                % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
                % format required by insertShape.
                bboxPolygon = reshape(bboxPoints', 1, []);
                
                %Save to bbox [x1 y1 w h]
                % Display a bounding box around the face being tracked.
                img = insertShape(img, 'Polygon', bboxPolygon);
                
                % Display tracked points.
                img = insertMarker(img, visiblePoints, '+', 'Color', 'white');
                
                % Reset the points.
                oldPoints = visiblePoints;
                setPoints(pointTracker, oldPoints);
            end
        end
        
        % once we have the face location -> we extract the RGB value
        if ~isempty(bbox)
            %if (mod(n,50) == 0)
            %IFaces = insertObjectAnnotation(img, 'rectangle', bbox, 'Face');
            
            %end
            
            if(SHOWALLPLOTS)
                figure(1), imshow(img), title('Detected Face 1');
            end
            
            if(DO_FACECROP)
                % crop the face
                % horizontal cropping
                scaleW = 0.6;
                bbox(1,3) = bbox(1,3)*scaleW;
                bbox(1,1) = bbox(1,1) + bbox(1,3)*((1-scaleW)/2);
                % vertical cropping
                scaleV = 0.6;
                bbox(1,4) = bbox(1,4)*scaleV;
                bbox(1,2) = bbox(1,2) + bbox(1,4)*((1-scaleV)/2);
            end;
            
            imgROI = imcrop(img_copy,bbox(1,:));
            if(SHOWALLPLOTS)
                figure(2), imshow(imgROI), title('Detected Face 2');
            end
            
            
            if (DO_SKINDETECTION)
                % average RGB value of skin pixels
                imgD = double(imgROI);
                skinprob = computeSkinProbability(imgD);
                mask = skinprob>SKIN_THRESH;
                %mask = imfill(mask, 'holes');
                maskRGB = zeros(size(skinprob,1),size(skinprob,2),3);
                maskRGB(:,:,1) = mask;
                maskRGB(:,:,2) = mask;
                maskRGB(:,:,3) = mask;
                imgD = imgD.*maskRGB;
                skinPixels = imgD; %for 2SR
                imgROI(imgD==0)=nan;
                
                imgROI_nan = double(imgROI);
                imgROI_nan(imgD==0)=nan;
                
                [r,c,w]=size(imgROI_nan);
                imgROI_nan_reshaped = reshape(imgROI_nan(:),r*c,[]);
                
                avg = nanmean(imgROI_nan_reshaped);
            else
                avg = nanmean(nanmean(imgROI));
                % for 2SR copy imgROI to skinPixels
                skinPixels = imgROI;
            end
            
            % save skin pixels for 2SR
            skinPixels = double(skinPixels);
            [r,c,w]=size(skinPixels);
            skinPixels = reshape(skinPixels(:),r*c,[]);
            % Remove the 0s
            skinPixels(all(skinPixels==0,2),:)=[];
            % end of save skin pixels for 2SR
        
            
            if(SHOWALLPLOTS)
                imshow(imgROI), title('Detected Face + skin');
                pause();
            elseif (mod(n,50) == 0)
                if ~silentMode
                    figure(1), imshow(imgROI), title('Detected Face + skin');
                    pause(.001);
                end
            end
            
            rgbTraces(1:nChannels,n) = avg;
            if (SAVESKINPIXELS)
                skinPixelsTraces{n,1,:} = skinPixels ; % save all pixels (slow)
            end
            
            %get timestamp from name
            [startIndex,endIndex] = regexp(imgName,'\_\d*\.');
            time = imgName(startIndex+1:endIndex-1);
            
            rgbTraces(1+nChannels,n) = str2double(time);
            if (SAVESKINPIXELS)
                skinPixelsTraces{n,2,:} =  str2double(time) ; % in case save all pixels (slow)
            end
        
            if (mod(n,200) == 0)
                fprintf('%i sur %i %s, timestamp %s \n', n, nbFiles, imgName, time);
            end
        end
    else
        avg=mean(mean(img));
        rgbTraces(1:nChannels,n) = avg;
        %get timestamp from name
        [startIndex,endIndex] = regexp(imgName,'\_\d*\.');
        time = imgName(startIndex+1:endIndex-1);
        
        rgbTraces(1+nChannels,n) = str2double(time);
        if (mod(n,50) == 0)
            fprintf('%i sur %i %s, timestamp %s \n', n, nbFiles, imgName, time);
        end
    end
end

save([outFolder '/' outFileName '.mat'], 'rgbTraces');

if(SAVESKINPIXELS)
   save([vidFolder 'allSkinPixels.mat'], 'skinPixelsTraces', '-v7.3'); 
end


%clear all
close all
disp(toc);
end
