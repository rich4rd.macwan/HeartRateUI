% test data driven PBV
close all;
clc;

addpath('../main-stable');
VERBOSE = 2;
VIDFOLDER='D:\Dataset\HeartRate\vid-bike\2017_06_14-15_27_13\';
matFileName = 'rgbTraces'; % input file
filtOrder = 8;
lowF=.7; % low freq for BP filtering
upF=3.5; % high freq for BP filtering

%getTraceFromVidFile('VIDFOLDER', VIDFOLDER); 

% load PPG
[gtTrace, gtHR, gtTime] = loadPPG(VIDFOLDER);
gtTime = gtTime / 1000;  % in seconds

% load RGB traces
if(exist([VIDFOLDER matFileName '.mat'], 'file') == 2)
    load([VIDFOLDER matFileName '.mat']);
else
    disp('oops, no input file');
    return;
end

% get current traces
timeTrace = rgbTraces(end,:);
timeTrace = timeTrace/1000; % in second
crtTrace = rgbTraces(1:3,:);
traceSize = size(crtTrace,2);

%normalize
crtTrace = crtTrace ./ repmat(mean(crtTrace,2),[1 traceSize]) -1;

if (1)
    figure(1);
    hold on
    %plot(timeTrace,crtTrace(1,:),'Color',[.8,.1,.3]);
    plot(timeTrace,crtTrace(2,:),'Color',[.1,.8,.3]);
    % plot(timeTrace,crtTrace(3,:),'Color',[.3,.1,.8]);
    title('Full RGB traces');
end

% get exact Fs
Fs = 1/mean(diff(timeTrace));

% get coef of butterworth filter
[b, a]= butter(filtOrder/2,[lowF upF]/(Fs/2)); % filtOrder/2 because of filtfilt

% detrend
for k=1:size(crtTrace,1)
    crtTrace(k,:) = detrendsignal(crtTrace(k,:)')';
end 


if (1)
    figure(2);
    hold on
    %plot(timeTrace,crtTrace(1,:),'Color',[.8,.1,.3]);
    plot(timeTrace,crtTrace(2,:),'Color',[.1,.8,.3]);
    % plot(timeTrace,crtTrace(3,:),'Color',[.3,.1,.8]);
    title('Full RGB traces');
end

% band pass filter
for k=1:size(crtTrace,1)
    crtTrace(k,:) = filtfilt(b,a,crtTrace(k,:)')'; % filtfilt for zero-phase filtering
end


if (1)
    figure(3);
    hold on
    %plot(timeTrace,crtTrace(1,:),'Color',[.8,.1,.3]);
    plot(timeTrace,crtTrace(2,:),'Color',[.1,.8,.3]);
    % plot(timeTrace,crtTrace(3,:),'Color',[.3,.1,.8]);
    title('Full RGB traces');
end

% CHROM
Rf = crtTrace(1,:);
Gf = crtTrace(2,:);
Bf = crtTrace(3,:);
Xf = 3*Rf-2*Gf;
Yf = 1.5*Rf+Gf-1.5*Bf;
alpha = std(Xf)/std(Yf);
Wc = [3*(1-alpha/2), -2*(1+alpha/2), 3*alpha/2];
pulseChrom = Wc * crtTrace;

if (1)
    figure(4);
    hold on
    %plot(timeTrace,crtTrace(1,:),'Color',[.8,.1,.3]);
    plot(timeTrace,pulseChrom,'Color',[.1,.8,.3]);
    % plot(timeTrace,crtTrace(3,:),'Color',[.3,.1,.8]);
    title('Chrom');
end
%  
% W = Wc;
% 
% W = rand(1,3);
% W = W/norm(W);
% figure(2);hold on;
% plot(timeTrace,W*crtTrace,'Color',[.1,.8,.3]);
% 
% for i=1:10
%     Pbv = (W * crtTrace) * crtTrace';
%     %Pbv = Pbv/norm(Pbv,2);
%     Wpbv = Pbv / (crtTrace*crtTrace'); % = Pbv * inv(crtTrace*crtTrace')
%     Wpbv = Wpbv/norm(Wpbv,2);
%     pulse = Wpbv *  crtTrace;
%     plot(timeTrace,pulse);
%     err = norm(Wpbv - W);
%     W = Wpbv;
% end
% 
% 
% % plot final traces
% if(1)
%     figure(2),hold on;
%     p1 = plot(gtTime, gtTrace/max(gtTrace), 'k');
%     p2 = plot(timeTrace, pulseChrom/max(pulseChrom), 'b', 'Linewidth', 1.5);
%     xlim([5 25]),
%     title('Final rPPG trace');
%     legend([p1 p2], 'PPG', 'rPPG');
% end;
% 
% 
% 
% 
