% color amplification
clear all;
close all;
clc;

addpath('./tools/skindetector');

disp('Amplify color - just for fun');

scalingFactor = 20;
vidFolder = '/media/yannick/Data/Dataset/HeartRate/vidStress/Julien_2016_03_02/1_neutral/';

% Create a cascade detector object.
faceDetector = vision.CascadeObjectDetector('ClassificationModel','FrontalFaceCART','MinSize',[200 200]);

%matFileName = 'pulseTraceChrom';
matFileName = 'pulseTraceGreen';

pulseFile = [vidFolder matFileName '.mat'];
disp(['Working with  ' vidFolder matFileName '.mat']);

if(exist(pulseFile, 'file') == 2)
    load(pulseFile);
else
    disp('oops, no input file');
    return;
end

traceSize = length(pulseTrace);

% nb of frames
nbFiles=length(dir([vidFolder '/img/*.ppm']));

if(nbFiles ~= traceSize)
    fprintf('Oops, there is something somewhere \n');
    return;
end

Fs = 1/mean(diff(timeTrace));

% write video
profile = 'Motion JPEG AVI';
vw = VideoWriter([vidFolder 'out.avi'],profile);
vw.Quality = 90;
vw.FrameRate =  Fs;
vw.open;

firstIt = 1;
curTime = 0;

startT = 200;
endT = 500;

pulseTrace = -pulseTrace;

for n=startT:endT
    if(mod(n,10) == 0)
        disp(n);
    end
    % read frame by frame number (maybe not ordered)
    imgName = dir([vidFolder '/img/frame_' num2str(n-1) '_*.ppm']);
    if(size(imgName,1)==0)
        fprintf('oops, no image file \n');
        continue;
    end
    imgName = imgName.name;
    img = imread([vidFolder '/img/' imgName]);
    
    if(firstIt == 1)
        % face detection
        bbox  = step(faceDetector, img);
        firstIt = 0;
    end
        
    img_copy = double(img);
        
    imgROI = img_copy;%imcrop(img_copy,bbox(1,:));
    
    % skin detection
    imgD = double(imgROI);
    skinprob = computeSkinProbability(imgD);
    mask = skinprob>0;
    %mask = imfill(mask, 'holes');
    maskRGB = zeros(size(skinprob,1),size(skinprob,2),3);
    maskRGB(:,:,1) = mask;
    maskRGB(:,:,2) = mask;
    maskRGB(:,:,3) = mask;
    
    imgD = imgD./255;
    imgD = rgb2hsv(imgD);
    imgD(:,:,2) = imgD(:,:,2) + 0.1*pulseTrace(n)*mask;
    imgD = hsv2rgb(imgD);
    imgD = imgD*255;
   
    imgOut = uint8(zeros(size(img,1), size(img,2)*3+20,3));
    imgOut(1:size(img,1),1:size(img,2),:) = img;
    imgOut(1:size(img,1),size(img,2)+11:size(img,2)*2+10,:) = uint8(imgD);
    
    figure(1);
    clf;
    hold on;grid on;
    set(gca, 'FontSize', 20); %<- Set properties
    plot(timeTrace(startT:endT), pulseTrace(startT:endT), 'g', 'LineWidth', 2);
    ylimit=get(gca,'ylim');
    plot([timeTrace(n) timeTrace(n)],[ylimit(1),ylimit(2)],'r--', 'LineWidth', 2);
    xlabel('Time (s)');
    title('remote PPG trace');
    hold off;
    M=getframe(gcf);
    M = imresize(M.cdata, [size(img,1) size(img,2)]);
    pause(0.01)

    imgOut(1:size(img,1),2*size(img,2)+21:size(img,2)*3+20,:) = M;
    
    
    imgOut = insertText(imgOut,[size(img,2)-35 size(img,1)-50],[num2str(curTime,'%.2f') ' s'],'FontSize',25,'BoxColor','red','BoxOpacity',0.4,'TextColor','white');
    imgOut = insertText(imgOut,[size(img,2)/2-100 10],'Original video','FontSize',25,'BoxColor','blue','BoxOpacity',0.4,'TextColor','white');
    imgOut = insertText(imgOut,[1.5*size(img,2)-100 10],'Amplified video','FontSize',25,'BoxColor','blue','BoxOpacity',0.4,'TextColor','white');
    
    %imwrite(imgOut, '~/julienAmplified.png');
    
    vw.writeVideo(imgOut);
    
%     imshow(imgOut);
%     pause(0.01);
    curTime = curTime+1/Fs;
end

vw.close;