function IBI = pulseToIBI(pulseTrace, pulseTime, IBIFile)

Fs = 256;
inc = 1/Fs;

pulseTimeTmp = pulseTime(1) : inc : pulseTime(end);
pulseTrace = interp1(pulseTime,pulseTrace,pulseTimeTmp, 'pchip');
pulseTime = pulseTimeTmp;
clear pulseTimeTmp;

% find peaks
%[pks, locs]=findpeaks(pulseTrace, pulseTime, 'MinPeakProminence',1);
[pks, locs]=findpeaks(pulseTrace, pulseTime);

% get interbeat interval (IBI = HRV)
HRV = diff(locs);
HRVTime = locs(2:end);

IBI = HRV';%[HRVTime ; HRV]';

csvwrite(IBIFile,IBI);

if(0)
    figure(1), hold on;
    title('PPG & HRV trace')
    xlabel('Time(s)')
    p1 = plot(pulseTime,pulseTrace);
    plot(locs,pks,'X','MarkerSize',12)
    
    % Plot HRV
    figure(2);
    p2 = plot(HRVTime,HRV, 'r', 'LineWidth',2);
end
    
   