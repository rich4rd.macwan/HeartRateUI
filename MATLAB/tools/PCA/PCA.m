function result = PCA(rgb)
%Input: each row of matrix rgb is one observed signal (i.e., r,g,b signals)

    % fprintf('Extracting PCA trace\n');
    testR=rgb(1,:);
    testG=rgb(2,:);
    testB=rgb(3,:);

    meanR=mean(testR);
    meanG=mean(testG);
    meanB=mean(testB);
    newR=(testR-meanR);
    newG=(testG-meanG);
    newB=(testB-meanB);

    testrgb=[newR; newG; newB];

    [E,D]=pcamat(testrgb);
    result=E'*rgb;
    result=result';
end

