function sig = getMostPeriodicSignal(signals, Fs)
% most periodic component based on SNR

LOW_F=.7; % low freq for BP filtering
UP_F=3; % high freq for BP filtering
WIDTH = 0.1; % Width for the door step function to compute SNR (in Hz)


maxsnr = -10000;
for index=1:size(signals,1)
    
    x = signals(index,:);
    N = length(x)*3;
    sig_freq = [0 : N-1]*Fs/N;
    sig_power = abs(fft(x,N)).^2;    
    range = (sig_freq>LOW_F & sig_freq < UP_F);   % frequency range to find PSD peak
    sig_power = sig_power(range);
    sig_freq = sig_freq(range);
    [~,loc] = max(sig_power); % findpeaks(sig_power,'SortStr','descend','NPeaks',1); % get first peak
    sig_peakLoc = sig_freq(loc);
    %peaksPower(index) = sig_peakPower;
    
    range = (sig_freq>(sig_peakLoc-WIDTH/2) & sig_freq < (sig_peakLoc+WIDTH/2));
    signal = sig_power.*range;
    noise = sig_power.*(~range);
    
%             fig4 = figure(4);
%             clf(fig4);
%             hold on;
%             plot(sig_freq, signal, 'r'), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
%             plot(sig_freq, noise, 'b'), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
%             title('rPPG FFT')
%             pause();
    
    n = sum(noise);
    s = sum(signal);
    snr = s/n;
    
    if snr>maxsnr
        maxsnr=snr;
        sig=x';
    end
end