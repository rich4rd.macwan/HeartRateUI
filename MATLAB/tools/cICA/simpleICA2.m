function [y, W, A, selectedSig,selectedIndex] = simpleICA2(X, maxIter, OverValue,varargin)
%Default num of independent components same as the number of input signals
verbose='on';
[vectorSize, IClen]=size(X);
ICnum=vectorSize;
initialStateMode=0;
guess= 0;
chooseBestComponent=true;
func=0; %0=tanh, 1=negentropy
if (rem(length(varargin),2)==1)
    error('Optional parameters should always go by pairs');
else
    for i=1:2:(length(varargin)-1)
        if ~ischar (varargin{i}),
            error (['Unknown type of optional parameter name (parameter' ...
                ' names must be strings).']);
        end
        % change the value of parameter
        switch lower (varargin{i})
            case 'initguess'
                A = varargin{i+1};
                
                if size(A,1) ~= size(X,1)
                    initialStateMode = 0;
                    if verbose
                        fprintf('Warning: size of initial guess is incorrect. Using random initial guess.\n');
                    end
                else
                    initialStateMode = 1;
                    if size(A,2) < ICnum
                        if verbose
                            fprintf('Warning: initial guess only for first %d components. Using random initial guess for others.\n', size(guess,2));
                        end
                        A(:, size(A, 2) + 1:ICnum) = ...
                            rand(vectorSize,ICnum-size(A,2))-.5;
                    elseif size(A,2)>ICnum
                        A=A(:,1:ICnum);
                        fprintf('Warning: Initial guess too large. The excess column are dropped.\n');
                    end
                    if verbose, fprintf('Using initial guess.\n'); end
                end
            case 'choosebestcomponent'
                chooseBestComponent=varargin{i+1};
            case 'func'
                if strcmp(varargin{i+1},'tanh')
                    func=0;
                    OverValue=1e-12;
                elseif strcmp(varargin{i+1},'negentropy')
                    func=1;
                    OverValue=1e-12;
                end
            case 'timetrace'
                timeTrace=varargin{i+1};
        end
    end
end
LOW_F=0; % low freq for PSD range
UP_F=5; % high freq for PSD range
y=X(2,:);
fprintf('Starting ICA for extracting the desired source signal ..\n');
flag = 1;
loop = 1;
a1=1;
mixedsig=X;
mixedmean=mean(X,2);

Fs = 1/mean(diff(timeTrace));
nfft = Fs*60; % nb of points for FFT
window = hanning(nfft);

% Calculate PCA
firstEig=1;
lastEig=ICnum;
interactivePCA='off';

addpath('../tools/FastICA');
[E, D]=pcamat(X, firstEig, lastEig, interactivePCA, verbose);
% Calculate the whitening
[whitesig, whiteningMatrix, dewhiteningMatrix] = whitenv(X, E, D, verbose);
X=whitesig;
ICnum=size(X,1);
if ICnum<vectorSize
    fprintf(2,'Dimensions reduced to %d !\n',ICnum);
    vectorSize=ICnum;
    initialStateMode=0;
end
if initialStateMode == 0
    % Take random orthonormal initial vectors.
    B = orth (randn (ICnum, vectorSize));
elseif initialStateMode == 1
    % Use the given initial vector as the initial state
    B = whiteningMatrix * A;
end


BOld = zeros(size(B));
BOld2 = zeros(size(B));

finetuningEnabled=1;
notFine=1;
ind=1;
%B=[pinv(whiteningMatrix)]';
while (flag == 1)
    
    % output at current iteration

    
%      W=B'*whiteningMatrix;
      y = B' * X;
      v_Gaus = zeros(size(X));
        for i=1:size(v_Gaus,1)
            % Gaussian signal with the same mean and variance
            v_Gaus(i,:) = normrnd(0,1, 1, IClen);
        end
        %W = B' * whiteningMatrix;
        %W = real(W);
        % output
        a=1;
        y =  a*X'*B;%W * mixedsig + (W * mixedmean) * ones(1, IClen);
        result=y';

        negents(loop,:)= (mean( log(cosh(a*X'*B))/a)- mean(log(cosh(a*v_Gaus'))/a)).^2
      figure(1);
      subplot(3,1,1),plot(y(:,1));
      subplot(3,1,2),plot(y(:,2));
      subplot(3,1,3),plot(y(:,3));
      pause()
      

    % Symmetric orthogonalization.
    B = B * real(inv(B' * B)^(1/2));        
        
%     

    
    minAbsCos = min(abs(diag(B' * BOld)));
    minAbsCos2 = min(abs(diag(B' * BOld2)));
    fprintf('No.%d iteration: change in w is %g\n',loop, 1-minAbsCos);
    if (1 - minAbsCos2 < OverValue)
        fprintf('Converged after %d iteration\n',loop);
        if finetuningEnabled & notFine
            fprintf('Initial convergence, fine-tuning: \n');
            notFine = 0;
            %                 usedNlinearity = gFine;
            %                 myy = myyK * myyOrig;
            BOld = zeros(size(B));
            BOld2 = zeros(size(B));
            
        else
            fprintf('Convergence after %d steps. Change in w is %g \n', loop,1 - minAbsCos);
            
            % Calculate the de-whitened vectors.
            A = dewhiteningMatrix * B;
            flag = 0;
            break;
        end
        
    end
    
    if loop >= maxIter
        fprintf('After %d iteration, still cannot convergent.\n',loop);
        flag = 0;
    end
    %oldw = w;
    BOld2 = BOld;
    BOld = B;
    
    
    
    if func==0
        hypTan = tanh(a1 * X' * B);
        B = X * hypTan / IClen - ones(size(B,1),1) * sum(1 - hypTan .^ 2) .* B / IClen * a1;

    elseif func==1
        std_X = std((X'*B)');                          % standard deviation
        v_Gaus = zeros(size(X));
        for i=1:size(v_Gaus,1)
            % Gaussian signal with the same mean and variance
            v_Gaus(i,:) = normrnd(0,1, 1, IClen);
        end
        W = B' * whiteningMatrix;
        W = real(W);
        % output
        y =  W * mixedsig + (W * mixedmean) * ones(1, IClen);
        result=y';
        a=2;
        negents(loop,:)= (mean( log(cosh(a*X'*B))/a)- mean(log(cosh(a*v_Gaus'))/a)).^2;
        %negents(loop,:)= (mean( - exp(-(X'*B).^2))- mean(-exp(-v_Gaus'))).^2;
        %negents(loop,:)= (mean( log(cosh(a*result))/a)).^2;
        
    end
    
    loop = loop + 1;
    %     hold on;
    %     quiver3(0,0,0,W(1),W(2),W(3));
    %     %quiver3(0,0,0,.8541,-.7340,-.0121,'Color','red');
    %     xlabel('x');
    %     ylabel('y');
    %     zlabel('z');
    %     pause(0.001)
end
% figure(1);
% clf
% hold on;
% plot(negents(:,1));
% plot(negents(:,2));
% plot(negents(:,3));
% hold off
% pause();
W = B' * whiteningMatrix;
A = dewhiteningMatrix * B;
    
if ~isreal(A)
    if b_verbose, fprintf('Warning: removing the imaginary part from the result.\n'); end
    A = real(A);
    W = real(W);
end

% output
y =  W * mixedsig + (W * mixedmean) * ones(1, IClen);
result=y';
selectedSig=[];
selectedIndex=0;
if chooseBestComponent
    %     for index=1:numberofsignals
    %         sig=result(:,index);
    %         Fs = 20;
    %         L = length(sig);
    %         y=sig;
    %         NFFT = 2^nextpow2(L);
    %         Y=fft(y,NFFT)/L;
    %         temp_y=2*abs(Y(1:NFFT/2+1));
    %         length_y=size(temp_y,1);
    %         cut_factor=0.075;
    %         temp_y(1:ceil(cut_factor*length_y))=0;
    %         [~,loc]=max(temp_y);
    %         max_p=temp_y(loc);
    %         max_pp=temp_y(2*loc);
    %         max_pp=max_pp^2;
    %         max_p=max_p^2+max_pp;
    %
    %         all_p=sum(temp_y.^2);
    %         ratio=max_p/all_p;
    %         if ratio>maxRatio
    %             maxRatio=ratio;
    %             selectedSig=sig;
    %             selectedIndex=index;
    %         else
    %         end
    %     end
    LOW_F=0.7; % low freq for BP filtering
    UP_F=3; % high freq for BP filtering
    WIDTH = 0.1; % Width for the door step function to compute SNR (in Hz)
    
    maxsnr = 0;
%     f1=figure('Name','SNR','units','normalized','outerposition',[0 0 1 1]);hold on;
%     f2=figure('Name','XCorr','units','normalized','outerposition',[0 0 1 1]);hold on;
    for index=1:size(result,2)
        
        x = result(:,index);
        N = length(x)*3;
        sig_freq = [0 : N-1]*Fs/N;
        sig_power = abs(fft(x,N)).^2;
        
        %         fig3 = figure(3);
        %         clf(fig3);
        %         plot(sig_freq, sig_power), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
        %         title('rPPG FFT')
        
        range = (sig_freq>LOW_F & sig_freq < UP_F);   % frequency range to find PSD peak
        sig_power = sig_power(range);
        sig_freq = sig_freq(range);
        [sig_peakPower,loc] = max(sig_power); % findpeaks(sig_power,'SortStr','descend','NPeaks',1); % get first peak
        sig_peakLoc = sig_freq(loc);
        peaksPower(index) = sig_peakPower;
        
        range = (sig_freq>(sig_peakLoc-WIDTH/2) & sig_freq < (sig_peakLoc+WIDTH/2))';
        signal = sig_power.*range;
        noise = sig_power.*(~range);
       

%         

    
    
        n = sum(noise);
        s = sum(signal);
        snr = s/n;
        
        %[G,g1,g2]=autocorrd(x',result',1/mean(diff(t)));
%         [G,g1,g2]=autocorrd(x',result');
%         %G(1:round(0.7*30))=0;
%         %G(3*30:end)=0;
%         Gmean=mean(G.*G);
        
%         figure(f1);
%         subplot(1,3,index),plot(sig_freq, signal, 'r'), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');hold on;
%         subplot(1,3,index),plot(sig_freq, noise, 'b'), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
%         title('rPPG FFT')
%         annotation('textbox',[0.2*(index+0.1) 0.5 0.2 0.3], 'String',['snr=' num2str(snr) 'Gmean=' num2str(Gmean)],'FitBoxToText','on');
%         hold off;
%         
%         figure(f2);
%         subplot(3,1,index),plot(G.*G);
        
        if snr>maxsnr
            maxsnr=snr;
            selectedSig=x';
            selectedIndex=index;
        end
    end
    
    selectedSig=selectedSig';
    if selectedSig(1)>0
        selectedSig=selectedSig*(-1);
    else
    end
end

%  pause();
%  close all;
selectedIndex
%Plot negents
% 
% figure(42);
% cla
% hold on;
% for c=1:size(X,1)
%     plot(negents(:,c));
% end
% Y=(a1*X'*B)';
% std_y=std(Y(selectedIndex,:));
% v_Gaus= normrnd(0, std_y, 1, IClen);
% 
% neg=(mean(log(cosh(Y(selectedIndex,:))))-mean(log(cosh(v_Gaus)))).^2;
% plot(loop-1,neg,'-o');
%hold off;

%selectedSig=y(1,:);
fprintf('End of simpleICA algorithm !\n');
