function getTraceFromFileSeqSWIR(varargin)
% extract rPPG trace from image sequences
% input SWIR monoband tiff images
% yannick.benezeth@u-bourgogne.fr
% 18/03/2016
tic
close all;
clear all;
clc;

% vidFolder = '/media/yannick/Data/Dataset/HeartRate/SWIR_DavidDarson/1_940nm_15et02fps/';
% imageName = 'capture-one';
% fps = 15.02;

% vidFolder = '/media/yannick/Data/Dataset/HeartRate/SWIR_DavidDarson/2_940nm_15et02_fps_bis/';
% imageName = 'capture-deux';
% fps = 15.02;

% vidFolder = '/media/yannick/Data/Dataset/HeartRate/SWIR_DavidDarson/3_15et979fps_phalange940adroite_autrephalanges1050nmagauche/';
% imageName = 'capture-trois';
% fps = 15.979;

vidFolder = '/media/yannick/Data/Dataset/HeartRate/SWIR_DavidDarson/9_CaptureVascularisationSWIR17_03aveceffort13et498fps';
imageName = 'capture_neuf';
fps = 13.498;

outFolder = vidFolder;
outFileName = 'rgbTraces_skin';



% nb of frames
nbFiles=length(dir([vidFolder '/img/*.tif']));

nChannels=3; % actually one but 3 to easilly use getPulse function
rgbTraces = zeros(1+nChannels, nbFiles-1); % why minus 1 ?

figure, hold on;

% Create the point tracker object.
pointTracker = vision.PointTracker('MaxBidirectionalError', 5);

numPts = 0;
manualbbox=[];
bbox=[];
for n=1:1500
    % read frame by frame
    img = imread([vidFolder '/img/' imageName num2str(n-1, '%.4d') '.tif']);
    img_copy = img;
    
    % face localisation by detection then tracking
    if numPts < 10
        
        if numel(bbox)==0
            if ~isempty(manualbbox)
                bbox=manualbbox;
            end
        end
        
        if numel(bbox)~=0
            % Find corner points inside the detected region.
            points = detectMinEigenFeatures(img, 'ROI', bbox(1, :));
            
            % Re-initialize the point tracker.
            xyPoints = points.Location;
            numPts = size(xyPoints,1);
            release(pointTracker);
            initialize(pointTracker, xyPoints, img);
            
            % Save a copy of the points.
            oldPoints = xyPoints;
            
            % Convert the rectangle represented as [x, y, w, h] into an
            % M-by-2 matrix of [x,y] coordinates of the four corners. This
            % is needed to be able to transform the bounding box to display
            % the orientation of the face.
            x = bbox(1, 1);
            y = bbox(1, 2);
            w = bbox(1, 3);
            h = bbox(1, 4);
            bboxPoints = [x,y;x,y+h;x+w,y+h;x+w,y];
            
            % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
            % format required by insertShape.
            bboxPolygon = reshape(bboxPoints', 1, []);
            
            % Display a bounding box around the detected face.
            img = insertShape(img, 'Polygon', bboxPolygon);
            
            % Display detected corners.
            img = insertMarker(img, xyPoints, '+', 'Color', 'white');
            
        else
            
            %Face wasn't detected, manual roi mode
            imshow(img);
            title('Please select ROI');
            
            rect= ginput(2);
            rect=round(rect);
            rect(2,:)=rect(2,:)-rect(1,:);
            manualbbox=reshape(rect',[1 4]);
        end
    else
        % Tracking mode.
        [xyPoints, isFound] = step(pointTracker, img);
        visiblePoints = xyPoints(isFound, :);
        oldInliers = oldPoints(isFound, :);
        
        numPts = size(visiblePoints, 1);
        
        if numPts >= 10
            % Estimate the geometric transformation between the old points
            % and the new points.
            [xform, oldInliers, visiblePoints] = estimateGeometricTransform(...
                oldInliers, visiblePoints, 'similarity', 'MaxDistance', 4);
            
            % Apply the transformation to the bounding box.
            bboxPoints = transformPointsForward(xform, bboxPoints);
            
            % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
            % format required by insertShape.
            bboxPolygon = reshape(bboxPoints', 1, []);
            
            % Display a bounding box around the face being tracked.
            % img = insertShape(img, 'Polygon', bboxPolygon);
            
            % Display tracked points.
            img = insertMarker(img, visiblePoints, '+', 'Color', 'white');
            
            % Reset the points.
            oldPoints = visiblePoints;
            setPoints(pointTracker, oldPoints);
            
             % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
            % format required by insertShape.
            bboxPolygon = reshape(bboxPoints', 1, []);
            
            % Display a bounding box around the detected face.
            img = insertShape(img, 'Polygon', bboxPolygon);
            
           % figure(1), imshow(img);
           % pause(0.1);
        end
    end
    
    % once we have the roi location -> we extract the RGB value
    if ~isempty(bbox)
                
        imgROI = imcrop(img_copy,bbox(1,:));
                
        avg = mean(imgROI(:));
        
        rgbTraces(1:nChannels,n) = [avg, avg, avg];
        
        %get timestamp 
        time = n/fps*1000;
        rgbTraces(1+nChannels,n) = time;
        
        if (mod(n,50) == 0)
            figure(1), imshow(img);
            figure(2), imshow(imgROI), title('Selected ROI');
            fprintf('%i sur %i \n', n, nbFiles);
            pause(0.01);
        end
        
    end
end
save([outFolder '/' outFileName '.mat'], 'rgbTraces');

clear all
close all
disp(toc);
end
