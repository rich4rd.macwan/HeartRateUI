function setPath_ICA(root)
    addpath([root 'demos/']);
    addpath([root 'gensignal/']);
    addpath([root 'instAlg/']);
    addpath([root 'performance/']);
    addpath([root 'tools/']);
end