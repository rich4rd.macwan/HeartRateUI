function [Y, B, E] = NLPCARLS(Z, B, A, b, source_type)
% NLPCARLS --- NonLinear recursive least-squares algorithm for ICA. 
%              It is a nonlinear modification of the PAST algorithm 
%              introduced by Yang for the standard linear PCA. It is 
%              assumed that the input signals are prewhitened.         
%
% Parameters:
% < Input >
%            Z --- whitened data in matrix form.Each row represents a signal;
%                  each column is sampling point.
%            B --- initial separating matrix
%            A --- mixing matrix. If processing real world signals, set A
%                  identity matrix
%            b --- forgetting constant that should be close to unity
%  source_type --- if 'sub': subgaussian sources
%                  if 'sup': supergaussian sources
% < Output >
%            Y --- Separated signals in matrix form like Z
%            B --- Separating matrix
%            E --- row vector of performance index
%
% Commands:
%   [Y,B,E] = NLPCARLS(Z, B, A, b, 'sub')
%   [Y,B,E] = NLPCARLS(Z, B, A, b, 'sup')
%
% Use:
%    Tri     CTPI
%
% See also:
%    NLPCARLSdemo   
%
% References:
%  [1] Aapo Hyvarinen,Juha Karhunen,Erkki Oja: Independent Component
%  Analysis, pp.258-260. John Wiley & Sons.Inc,2001
%  [2] P.Pajunen and J.Karhunen: Least-squares methods for blind source
%  separation based on nonlinear PCA. Int.J.Neural Syst.,vol.8, 
%  pp.601-612, Dec.1998
%
%  Author: Zhi-Lin Zhang (zlzhang@uestc.edu.cn)
% Version: 1.0
%    Date: Apr.3, 2005



if nargin ~= 5
    error('The number of input parameters is wrong !\n');
elseif ~(strcmp(lower(source_type),'sup') | strcmp(lower(source_type), 'sub'))
    error('The fifth parameter''s value is wrong ! \n');
end

[numOfIC, numOfSamp] = size(Z);

P = eye(numOfIC);     % initialize auxiliary variable

fprintf('Starting RLS based Nonlinear PCA algorithm ...\n');

if lower(source_type) == 'sup'       % separating supergaussian sources   
    
    fprintf('Select non-linearity g(y)=y-tanh(y) for SUPER-Gaussian sources.\n');
    fprintf('Running ... \n');
        
    for n = 1 : numOfSamp
             
        Y(:,n) = B * Z(:,n);
        
        q = Y(:,n) - tanh(Y(:,n));  % nonlinearity for super-Gaussian source signals
        h = P * q;
        m = h/(b + q'*h);
        P = 1/b * tri(P - m * h');
        r = Z(:,n) - B'* q;
        B = B + m * r';
        
        E(n) = CTPI( B*A,'ave');   % performance index at current iteration      
    end
    
% separating super-Gaussian sources    
else  
    fprintf('Select non-linearity g(y)=tanh(y) for SUB-Gaussian sources.\n');
    fprintf('Running ... \n');
    
    for n = 1 : numOfSamp

        Y(:,n) = B * Z(:,n);
        
        q = tanh(Y(:,n));  % nonlinearity for sub-Gaussian source signals
        h = P * q;
        m = h/(b + q'*h);
        P = 1/b * tri(P - m * h');
        r = Z(:,n) - B'* q;
        B = B + m * r';
        
        E(n) = CTPI( B*A,'ave');   % performance index at current iteration      
    end
end

fprintf('End ! \n');