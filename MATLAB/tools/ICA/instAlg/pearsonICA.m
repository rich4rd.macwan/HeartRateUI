function [Y, W, E] = pearsonICA(X, W, mu, A, iteration, trace)
% PEARSONICA --- pearson-ICA algorithm in offline type.
%                    The algorithm uses the combination of fixed nonlinearities and
%                adaptive nonlinearities. When the source signals are clearly
%                super-Gaussian, tanh function is employed; when the source signals
%                are clearly sub-Gaussian, cubic function is employed; when the
%                source signals are near Gaussian and skewed, the nonlinearities
%                derived from the generalized beta distribution (GBD) and pearson
%                system are employed.
%                    The contrast function is the maximum likelihood function. Note
%                that the input data had better be pre-whitened.
% 
% Format:
%          [Y, W, E] = pearsonICA(X, W, mu, A, iteration, trace)
%
% Parameter:
% < Input >
%           X --- whitened mixed signals in matrix form N*P, where N represent how many 
%                 input(mixed) singals sources there are, and where P 
%                 represent how many samples in the data. It is suggested that the
%                 input data X be prewhitened.
%           W --- initial separating matrix
%          mu --- step size
%           A --- mixing matrix
%   iteration --- maximum iteration times
%       trace --- if trace~=0, then print the progress of the algorithm; if trace==0, then not print 
% < Output >
%        Y --- Separated signals in matrix form like X
%        W --- Separating matrix
%        E --- row vector of performance index
%
% Example:
%        W0 = eye(4); W0 = W0/norm(W0);
%        mu = 0.01;
%        iteration = 1000;
%        trace = 1;
%        [Y, W, E] = pearsonICA(X, W0, mu, V*A, iteration, trace);
%
% See also:
%      pearsonICAdemo
%
% References:
%    [1] Karvanen, J.,Eriksson, J., and Koivunen, V.: "Pearson System Based Method for Blind Separation", 
%        Proceedings of Second International Workshop on Independent Component Analysis and Blind Signal Separation,
%        Helsinki 2000, pp. 585-590
%    [2] Eriksson, J., Karvanen, J., and Koivunen, V.:  "Source Distribution Adaptive Maximum Likelihood Estimation of
%        ICA Model", Proceedings of Second International Workshop on Independent Component Analysis and Blind Signal 
%        Separation, Helsinki 2000, pp. 227--232
%
% Author  : Zhi-Lin Zhang (zlzhang@uestc.edu.cn)
% Version : 1.0     Feb.7, 2006



[numOfIC, numOfSamp] = size(X);

if (nargin < 1),
    error('At least 1 parameter is required');
elseif (nargin == 1),
    W = eye(numOfIC);  mu = 0.1;  A = W;  iteration = 200;   trace = 0; 
elseif (nargin == 2),
    mu = 0.1;   A = W;  iteration = 200;   trace = 0; 
elseif (nargin == 3)
    A = W;  iteration = 200;  trace = 0;  
elseif (nargin == 4)
    iteration = 200;   trace = 0;  
elseif (nargin == 5)
    trace = 0;  
end;


Y = zeros(size(X));
Im = eye(numOfIC);     % identity matrix

% Starting iteration
for t = 1 : iteration   
    Y = W * X;  
    
    % Estimate 3rd and 4th moments.
    alpha3 = moment(Y',3);    
    alpha4 = moment(Y',4);    
    
    % choose non-linearity for each IC
    for n = 1: numOfIC
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%  strong sub-Gaussian  %%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        if alpha4(n) < 2.6 
            SType(n) = 1;                 % cubic function
            score(n,:) = Y(n,:).^3;
        
            
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%% strong super-Gaussian %%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        elseif alpha4(n) > alpha3(n)^2 + 4  
            SType(n) = 2;                 % tanh function
            score(n,:) = tanh(Y(n,:));
        
            
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% near Gaussian (and skewed) %%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        else
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % when sub-Gaussian distributions are fitted to the nearest GLD
            if alpha4(n) < 1.2 * alpha3(n)^2 + 2.2
                SType(n) = 3;             % GBD utilized
                %------------------------------------------------------------------------------
                % GBD sample min & max estimation used. 
                % For details, see gbd_momentfit.m  and Ref.[1]
                %------------------------------------------------------------------------------
%                 samplemin = min(Y(n,:));
%                 samplemax = max(Y(n,:));
%                 beta = gbd_momentfit(alpha3(n),alpha4(n),samplemin(n),samplemax(n),numOfSamp);
%                 [score(n,:) score2(i,:)] = gbd_score(Y(n,:),beta);

                samplemin = min(Y(n,:));
                samplemax = max(Y(n,:));
                
                %------------------------------------------------------------------------------
                % Moment estimates for beta(3) and beta(4). These are obtained from
                % the equations (15) and (16) in Ref.[2]
                %------------------------------------------------------------------------------
                s = alpha3(n)^2;
                k = alpha4(n);
                a = -((3*(-1 + k - s)*(k^2*(-32 + s) + k*(96 + 78*s - sqrt(s)*sqrt(k^2*(-32 + s) - ...
                    9*s*(7 + 4*s) + 6*k*(16 + 13*s))) - 3*(21*s + 12*s^2 + sqrt(s)*sqrt(k^2*(-32 + s) - ...
                    9*s*(7 + 4*s) + 6*k*(16 + 13*s)))))/ ((2*k - 3*(2 + s))*(k^2*(-32 + s) - 9*s*(7 + 4*s) + ...
                    6*k*(16 + 13*s))));
                
                b = -(3*(-1 + k - s)*(k^2*(-32 + s) - 63*s - 36*s^2 + 3*sqrt(s)*sqrt(k^2*(-32 + s) - 9*s*(7 + 4*s) + ...
                    6*k*(16 + 13*s)) + k*(96 + 78*s + sqrt(s)*sqrt(k^2*(-32 + s) - ...
                    9*s*(7 + 4*s) + 6*k*(16 + 13*s)))))/ (2*k^3*(-32 + s) + k^2*(384 + 246*s - 3*s^2) + ...
                    27*s*(14 + 15*s + 4*s^2) - 18*k*(32 + 49*s + 17*s^2));
                
                %------------------------------------------------------------------------------
                % Note: A=beta(3)+1, B=beta(4)+1 correspond to the Statistics Toolbox
                % values used for the ordinary Beta Distribution
                %------------------------------------------------------------------------------
                if s>0,
                    beta(3) = a-1;   beta(4) = b-1; 
                else
                    beta(3) = b-1;   beta(4) = a-1; 
                end;
                
                %------------------------------------------------------------------------------
                % Estimates for beta(1) and beta(2) based on sample minimum and
                % maximum. The PDF of any GBD distribution is nonzero on interval [beta(1),
                % beta(1)+beta(2)], therefore this "ad-hoc" estimation guarantees
                % that the corresponding pdf is non-zero for all realizations 
                % (this is not the case with the moment estimation, which may
                % lead to problems when calculating the score function...).
                %------------------------------------------------------------------------------
                beta(1) = (numOfSamp+1)*samplemin / numOfSamp;
                beta(2) = (numOfSamp+1)*samplemax / numOfSamp - beta(1);
                
                %------------------------------------------------------------------------------
                % The values of x are forced to interval [beta(1)+delta,beta(1)+beta(2)-delta]
                % to ensure the numerical stability.
                %------------------------------------------------------------------------------
                val = max( (beta(1)+0.01),  min(Y(n,:),beta(1)+beta(2)-0.01) );
                
                %------------------------------------------------------------------------------
                % The functions are given by the equations (22) and (23) in Ref.[2]
                %------------------------------------------------------------------------------
                score(n,:) = ( beta(2)*beta(3) - val.*[beta(3)+beta(4)] + beta(1)*[beta(3)+beta(4)] ) ... 
                              ./ ( [val-beta(1)] .* [-val+beta(1)+beta(2)] );
             
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % use Pearson System
            else
                SType(n) = 4;             % Pearson system utilized
                %------------------------------------------------------------------------------
                % Pearson moment estimation used.
	            % See pearson_momentfit.m and Ref.[1] for more details.
                %------------------------------------------------------------------------------
                C = 10*alpha4(n) - 12*alpha3(n) ^2 - 18;
                b0 = -( 4*alpha4(n) - 3*alpha3(n) ^2 ) / C;
                b1 = -alpha3(n) * (alpha4(n) +3) / C;
                b2 = -(2*alpha4(n) - 3*alpha3(n) ^2 - 6) / C;
                denominator = b0 + b1*Y(n,:) + b2*Y(n,:).^2;
                score(n,:) = -(Y(n,:)-b1)./denominator;
            end   % end of selecting GBD or Pearson System model
            
        end  % end of computing score function for an IC
        
    end % end of computing score functions for all ICs
    
    % update 
    gy	= score * Y';
    W	= W + mu * (Im - Y * Y'/numOfSamp - gy/numOfSamp + gy'/numOfSamp) * W; 
    W = W/norm(W);
    
    % computer performance index when iterating No.t samples
    E(1,t) = CTPI( W*A,'norm');  
    
end  % end of iteration




