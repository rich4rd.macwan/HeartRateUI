function [Y,W,E] = ACY(X, W, VA, mu, source_type, batchsize)
% ACY --- Amari, Cichocki and Yang 's algorithm, published in NIPS'96. The
%         algorithm need not prewhiten, but commend.
% Command:  [Y,W,E] = ACY(X, W, VA, mu, batchsize)
%           [Y,W,E] = ACY(X, W, VA, mu)
% Parameter:
% < Input >
%           X --- whitened mixed signals in matrix form N*P, where N represent how many 
%                 input(mixed) singals sources there are, and where P 
%                 represent how many samples in the data.
%           W --- initial separating matrix
%          VA --- product of V and A,i.e., VA=V*A; V is whitening matrix and A is mixing
%                 matrix
% source_type --- if 'sub': subgaussian sources
%                 if 'sup': supergaussian sources
%          mu --- step size
%   batchsize --- length of each batch, if this parameter is neglect, then
%                 batchsize adopt the default value, that is, 1
% < Output >
%        Y --- Separated signals in matrix form like X
%        W --- Separating matrix
%        E --- row vector of performance index
%
% See also:
%    InfomaxDemo
%
% References:
%    [1] Amari, Cichocki, and Yang, A new learning algorithm for blind
%    signal separation. In: Advances in Neural Information Processing
%    System 8, pp.757-763, MIT Press, Cambridge MA, 1996
%
% Author  : Zhang Zhi-Lin (zhangzl@vip.163.com)
% Update  : 1.2     Mar.24,2005
% Version : 1.1     Dec.7, 2004


% if input parameters are four, choose default value
if nargin < 5 | nargin > 6
    error('The number of input parameters is wrong !\n');
elseif ~(strcmp(lower(source_type),'sup') | strcmp(lower(source_type), 'sub'))
    error('The fifth parameter''s value is wrong ! \n');
elseif nargin == 5
    batchsize = 1;
end

numBatch = floor(length(X)/batchsize);      % number of batches
iter = 0;
E = zeros(1,numBatch);       % initialization of error vector
Im = eye(size(X,1));

if strcmp(lower(source_type),'sub')  
    for k = 1: batchsize : numBatch * batchsize
        Y(:,[k: k+batchsize-1]) = W * X(:, [k: k+batchsize-1]);
        
        % non-linearity for subgaussian sources
%         f = 3/4*Y(:,[k: k+batchsize-1]).^11 + 25/4*Y(:,[k: k+batchsize-1]).^9 ...
%             - 14/3*Y(:,[k: k+batchsize-1]).^7 - 47/4*Y(:,[k: k+batchsize-1]).^5 ...
%             + 29/4*Y(:,[k: k+batchsize-1]).^3 ;
        f = Y(:,[k: k+batchsize-1]).^3 ;
        
        % updating
        W = W + mu * (Im - 1/batchsize * f * Y(:,[k: k+batchsize-1])') * W;    
        
        iter = iter + 1;
        E(1,iter) = CTPI(W*VA,'ave');                % error at each iteration
    end
    
elseif strcmp(lower(source_type),'sup') 
    for k = 1: batchsize : numBatch * batchsize
        Y(:,[k: k+batchsize-1]) = W * X(:, [k: k+batchsize-1]);
        
        % non-linearity for supergaussian sources
        f = tanh(Y(:,[k: k+batchsize-1])); 

        % updating
        W = W + mu * (Im - 1/batchsize * f * Y(:,[k: k+batchsize-1])') * W;     
        
        iter = iter + 1;
        E(1,iter) = CTPI(W*VA,'ave');                % error at each iteration
    end  
end