function [ys, W ] = StoneBSS(mixtures, shf, lhf, max_mask_len)
% StoneBSS-- BSS based on Stone's predictability theory.
% 
% Format:    [ys, W ] = StoneBSS(mixtures, shf, lhf, max_mask_len)
%
% Parameters:
%            ys -- Separated signals. Each row is a separated signal.
%             W -- Separated matrix,i.e. ys = W' * mixtures
%      mixtures -- Mixed signals. Each row is a mixed signal. It does not need
%                  prewhitening before running the algorithm
%           shf -- Short-term half-life. Recommended value: 1
%           lhf -- Long-term half-life.  Recommended value: 90000
%  max_mask_len -- Maximum masking length for prediction
%
% Reference: 
%  [1]JV Stone, "Blind Source Separation Using Temporal Predictability", Neural
%     Computation, 13(7), pp.1559-1574, July, 2001
%
% Author: J.V. Stone  (Slightly modified by Zhi-Lin Zhang in 2006)
% Date  : 2001



n			= 8;           % n = num half lives to be used to make mask.
mixtures    = mixtures';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get masks to be used to find (x_tilde-x) and (x_bar-x)
% Set mask to have -1 as first element,
% and remaining elements sum to unity.

% Short-term mask.
h=shf; t = n*h; lambda = 2^(-1/h); temp = [0:t-1]'; 
lambdas = ones(t,1)*lambda; mask = lambda.^temp;
mask(1) = 0; mask = mask/sum(abs(mask));  mask(1) = -1;
s_mask=mask; s_mask_len = length(s_mask);

% Long-term mask.
h=lhf;t = n*h; t = min(t,max_mask_len); t=max(t,1);
lambda = 2^(-1/h); temp = [0:t-1]'; 
lambdas = ones(t,1)*lambda; mask = lambda.^temp;
mask(1) = 0; mask = mask/sum(abs(mask));  mask(1) = -1;
l_mask=mask; l_mask_len = length(l_mask);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Filter each column of mixtures array.
S=filter(s_mask,1,mixtures); 	L=filter(l_mask,1,mixtures);

% Find short-term and long-term covariance matrices.
U = cov(S,1);		  V = cov(L,1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find eigenvectors W and eigenvalues d.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[W d] = eig(V,U);     W = real(W);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Recover source signals.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ys = mixtures * W;
ys = ys';             % each row is a separated signal



