function [S, B] = geig(X , Q)
% geig -- Blind source separation via generalized eigenvalue decomposition
%         It returns the estimated source signals and separating matrix, 
%         i.e. S = B * A * S, where A is the unknown mixing matrix. Q is an
%         additional symmetric matrix whose form depends upon the particular
%         assumptions, such as non-stationarity, non-white, and non-Gaussian.
%         Details see the reference. Its drawback is that the method critically
%         determines the mixture coefficients and is therefore not robust to
%         estimation errors.
%
% Command: 
%   [S, B] = geig(X , Q);
%
% Parameters:
%       S -- estimated source signals matrix with N * M, where N reprents the number
%            of source signals, and M represents the sample points.
%       B -- separating matrix, i.e., S = B * X;
%       X -- observation data (not requiring prewhitened) matrix with N * M.
%       Q -- an additional symmetric matrix whose form depends upon the particular
%            assumptions, such as non-stationarity, non-white, and non-Gaussian
%
% See also
%     geigdemo
%
% Author  : Zhi-Lin Zhang
% Data    : July 31, 2005
% Version : 1.0
%
% Reference:
% [1] Lucas Parra, Paul Sajda, Blind source separation via generalized eigenvalue
%     decomposition. Journal of Machine Learning Research 4 (2003) 1261-1269


[W,D] = eig(X*X',Q);
B = W';
S  = W'* X;
