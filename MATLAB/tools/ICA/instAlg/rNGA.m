function [Y,B,E] = rNGA(X, B, A, mu, fgt, varargin)
% Robust natural gradient based blind source separation, based on the t-distribution
% and the generalized Gaussian distribution.
%         
% Format : [Y,B,E] = rNGA(X, B, A, mu, fgt, var1, var2);
% Command: [Y,B,E] = rNGA(X, B, A, mu, fgt);
%          [Y,B,E] = rNGA(X, B, A, mu, fgt,'online');
%          [Y,B,E] = rNGA(X, B, A, mu, fgt,'offline',2000);
%
% Parameters:
%     < Input >
%          X --- mixed signals in matrix form N*P, where N represent how many 
%                input(mixed) singals sources there are, and where P 
%                represent how many samples in the data.
%          B --- initial separating matrix, commend identity matrix
%          A --- mixing matrix
%         mu --- step size
%        fgt --- forgetting factor for on-line computing kurtosis, say, 0.5
%       var1 --- If its value is 'online', the algorithm adopts online
%                version. If its value is 'offline', the algorithm adopts
%                offline version. Default value is 'online'
%       var2 --- iteration times. Only when var1's value is 'offline', it
%                is valid.
%   
%   < Output >
%       Y --- Separated sources in matrix form, like X
%       B --- Separating matrix
%       E --- row vector of performance index. The definition of
%             performance index refers to function CTPI (CTPI.m). In this
%             program, CTPI(A,'ave') is called.
%
% Reference:
%       [1]J.Cao, N.Murata, S.Amari, A.Cichocki, T.Takeda, A robust approach to
%       independent component analysis of signals with high-level noise measurements.
%       IEEE Trans. Neural Networks, Vol.14, No.3, May 2003, pp.631-645
%
% See also
%       rNGAdemo        CTPI
%
% Author : Zhi-Lin Zhang (zlzhang@uestc.edu.cn)
% Version: 1.0    Date: Feb.8, 2006



if nargin == 5
    type = 'online';
    
elseif strcmp(lower(varargin{1}),'online') & (nargin == 6)
    type = 'online';
    
elseif strcmp(lower(varargin{1}),'offline') & (nargin == 7)
    type = 'offline';
    iter = varargin{2};
    
else
    error(sprintf('Number of parameters or its value is wrong ! \nType ''help flexiICA''for detail !\n'));

end
    
      
[numOfIC, numOfSamp] = size(X);

% built up the look-up table of kurtosis-beta relationship for the t-distribution
Beta = linspace(4,30,2000);
Ktable = 3*gamma( (Beta-4)./2 ) .* gamma(Beta./2) ./ gamma( (Beta-2)./2 ).^2 - 3;


%============================= Online Version =============================================
if strcmp(type,'online')
    % initialization
    Y = zeros(size(X));
    E = zeros(1,numOfSamp);
    Im = eye(numOfIC);        % identity matrix
    
    M4 = zeros(numOfIC,1);    % initialized value of 4th moment
    M2 = ones(numOfIC,1);     % initialized value of 2th moment
    
    % Starting iteration
    for t = 1 : numOfSamp   
        Y(:,t) = B * X(:,t);   
              
        % choose non-linearity for each IC
        for n = 1: numOfIC
            % on-line compute kurtosis for each IC
            kurt = M4(n,1)/M2(n,1)^2 - 3 ;
            M4(n,1) = fgt * M4(n,1) + (1-fgt) * Y(n,t)^4;
            M2(n,1) = fgt * M2(n,1) + (1-fgt) * Y(n,t)^2;
            
            if kurt > 0
                % using the score function derived from the t-distribution
                Ind = find( Ktable > kurt);
                beta = Beta(Ind(end));
                lambda2 = beta * gamma( (beta-2)/2 )  / ( 2 * M2(n,1) * gamma(beta/2) ) ;
                score(n,1) = (1+beta)*Y(n,t) / ( Y(n,t)^2 + beta/lambda2 );
            else
                % using the score function derived from the generalized Gaussian
                % distribution
                score(n,1) =  Y(n,t).^3;  
            end
                      
%             % If estimated kurtosis of recovered signal Yi(t) is greater than 20
%             if kurt > 20 
%                 beta = 4.25;
%                 lambda2 = beta * gamma( (beta-2)/2 )  / ( 2 * M2(n,1) * gamma(beta/2) ) ;
%                 score(n,1) = (1+beta)*Y(n,t) / ( Y(n,t)^2 + beta/lambda2 );
%                 
%             % If estimated kurtosis of recovered signal Yi(t) is between 10 and 20
%             elseif ( 10 < kurt ) & ( kurt <= 20 )
%                 beta = 4.4;
%                 lambda2 = beta * gamma( (beta-2)/2 )  / ( 2 * M2(n,1) * gamma(beta/2) ) ;
%                 score(n,1) = (1+beta)*Y(n,t) / ( Y(n,t).^2 + beta/lambda2 );
%             
%             % if estimated kurtosis of recovered signal Yi(t) is between 10 and 5
%             elseif ( 5 < kurt ) & ( kurt <= 10 )
%                 beta = 4.85;
%                 lambda2 = beta * gamma( (beta-2)/2 )  / ( 2 * M2(n,1) * gamma(beta/2) );
%                 score(n,1) = (1+beta)*Y(n,t) / ( Y(n,t).^2 + beta/lambda2 );
%                 
%             % if estimated kurtosis of recovered signal Yi(t) is between 10 and 5
%             elseif ( 0 < kurt ) & ( kurt <= 5 )
%                 beta = 6;
%                 lambda2 = beta * gamma( (beta-2)/2 )  / ( 2 * M2(n,1) * gamma(beta/2) );
%                 score(n,1) = (1+beta)*Y(n,t) / ( Y(n,t).^2 + beta/lambda2);
%                 
%             % If estimated kurtosis of recovered signal Yi(t) is negative,
%             else
%                 score(n,1) =  Y(n,t).^3;    
%                 % score(n,1) = Y(n,t) - tanh( Y(n,t) ); 
%             end   
            
        end % end of Computing non-linearity for each IC
        
        % update 
        gy	= score * Y(:,t)';
        B	= B + mu * (Im - Y(:,t) * Y(:,t)' - gy + gy') * B;    % for pre-whitened data  
%         B	= B + mu * (Im - score * Y(:,t)') * B;          % for non-prewhitened data
        B = B/norm(B);
        
        % computer performance index when iterating No.t samples
        E(1,t) = CTPI( B*A,'norm');           
        
    end
end


%============================= Offline Version =============================================
if strcmp(type, 'offline')
    % initialization
    Y = zeros(size(X));
    E = zeros(1,iter);
    Im = eye(numOfIC);        % identity matrix
    
    % Starting iteration
    for t = 1 : iter  
        Y = B * X;   
              
        % choose non-linearity for each IC
        for n = 1: numOfIC
            % compute kurtosis for each IC
            M4(n,1) = mean(Y(n,:).^4);
            M2(n,1) = mean(Y(n,:).^2);
            kurt = M4(n,1)/M2(n,1)^2 - 3;
            
            if kurt > 0
                % using the score function derived from the t-distribution
                Ind = find( Ktable > kurt);
                beta = Beta(Ind(end));
                lambda2 = beta * gamma( (beta-2)/2 )  / ( 2 * M2(n,1) * gamma(beta/2) ) ;
                score(n,:) = (1+beta)*Y(n,:) ./ ( Y(n,:).^2 + beta/lambda2 );
            else
                % using the score function derived from the generalized Gaussian
                % distribution
                score(n,:) =  Y(n,:).^3;  
            end
  
        end % end of Computing non-linearity for each IC
        

        % update 
        gy	= score * Y';
        B	= B + mu * (Im - Y * Y'/numOfSamp - gy/numOfSamp + gy'/numOfSamp) * B;  % for prewhitened data     
%         B   = B + mu * (Im - gy/numOfSamp) * B;          % for non-prewhitened data
%         B   = B/norm(B);
        
        % computer performance index when iterating No.t samples
        E(1,t) = CTPI( B*A,'norm');           
        
    end % end of iteration
    
end

