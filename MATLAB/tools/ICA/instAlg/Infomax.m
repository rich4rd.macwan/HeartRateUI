function [Y, W, b, E] = Infomax(X, W, b, VA, mu, batchsize)
% INFOMAX --- Natural gradient based Infomax algorithm. This is a modified version of 
%             Infomax algorithm, which first proposed by Bell and Sejnowski
%             in 1995. However, this algorithm can only separate supergaussian sources
% Command:  [Y,W,b,E] = Infomax(X, W, b, VA, mu, batchsize)
%           [Y,W,b,E] = Infomax(X, W, b, VA, mu)
% Parameter:
% < Input >
%           X --- whitened mixed signals in matrix form N*P, where N represent how many 
%                 input(mixed) singals sources there are, and where P 
%                 represent how many samples in the data.
%           W --- initial separating matrix
%           b --- initial bias column vector
%          VA --- product of V and A,i.e., VA=V*A; V is whitening matrix and A is mixing
%                 matrix
%          mu --- step size
%   batchsize --- length of each batch, if this parameter is neglect, then
%                 batchsize adopt the default value, that is, 1
% < Output >
%        Y --- Separated signals in matrix form like X
%        W --- Separating matrix
%        b --- bias vector
%        E --- row vector of performance index
%
% See also:
%    Infomaxdemo
%
% References:
%    [1] Bell, A. J., & Sejnowski, T. J. (1995). An information-maximization approach
%    to blind separation and blind deconvolution. Neural Computation, 7, 1129-1159.
%
% Author  : Zhilin Zhang
% Version : 1.0
% Date    : Nov.30, 2004


% if input parameters are four, choose default value
if nargin == 5
    batchsize = 1;     
end

numBatch = floor(length(X)/batchsize);      % number of batches
iter = 0;
E = zeros(1,numBatch);       % initialization of error vector
Im = eye(size(X,1));
OnesVec = ones(1,batchsize);

for k = 1: batchsize : numBatch * batchsize
    Y(:,[k: k+batchsize-1]) = W * X(:, [k: k+batchsize-1]);
    U = Y(:,[k: k+batchsize-1]) + b * OnesVec;
    f = 1 - 2./( exp(2*U) + 1);  
    W = W + mu * batchsize * (Im - 2 * f * Y(:,[k: k+batchsize-1])') * W;     % updating
    b = b + mu * (sum(-2*f'))';
    
    iter = iter + 1;
    E(1,iter) = CTPI(W*VA);                % error at each iteration
end
    