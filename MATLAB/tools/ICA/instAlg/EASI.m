function [Y, B, E] = EASI( X, B, A, mu, source_type, varargin)
% EASI --- EASI algorithm for separating real source signals. The output
%          signals will have unit varance because this algorithm was
%          developed under the condition that E[yy']=I.
% Format:  [Y, B, E] = EASI( X, B, A, mu, source_type, varargin)
% Command:
%          [Y,B,E] = EASI(X, B, A, mu, 'sub', 'ordinary');
%          [Y,B,E] = EASI(X, B, A, mu, 'sup', 'normalize');
% Parameter:
% < Input >
%           X --- mixed signals in matrix form N*P, where N represent how many 
%                 input(mixed) singals sources there are, and where P 
%                 represent how many samples in the data.
%           B --- initial separating matrix
%           A --- mixing matrix. If processing real world signals, set A
%                 identity matrix
%          mu --- step size, commend value is 0.01
% source_type --- if 'sub': subgaussian sources
%                 if 'sup': supergaussian sources
%   varargin2 --- if 'ordinary'(Default): non-normalized EASI Algorithm
%                 if 'normalize': Normalized EASI Algorithms.
% < Output >
%        Y --- Separated signals in matrix form like X
%        B --- Separating matrix
%        E --- row vector of performance index,calculated by CTPI(W*A,'norm')
%
% See also:
%    EASIdemo,  CTPI
%
% References:
%    [1] Jean-Francois Cardoso, Beate Hvam Laheld, Equivariant Adaptive
%        Source Separation, IEEE trans. on Signal Processing, Vol.44,
%        Dec.1996
%
% Author:   Zhi-Lin Zhang (zlzhang@uestc.edu.cn)
% Version:  2.1
% Date:     Dec.7, 2003
% Updating: Feb.27, 2005



if nargin < 5 | nargin > 6
    error('The number of input parameters is wrong !\n');
elseif ~(strcmp(lower(source_type),'sup') | strcmp(lower(source_type), 'sub'))
    error('The fifth parameter''s value is wrong ! \n');
elseif nargin == 5
    type = 'ordinary';
elseif nargin == 6
    type = lower(varargin{1});
    if ~(strcmp(type,'ordinary') | strcmp(type, 'normalize'))
        error('The last parameter is wrong !\n');
    end
end

N = size(X,1);
P = size(X,2);

E = zeros(1,P);             % Initialization          

if strcmp(type,'ordinary')       % non-normalized EASI Algorithm
    fprintf('Start EASI algorithm ...\n');
    if lower(source_type) == 'sup'       % separating supergaussian sources   
        fprintf('Select non-linearity g=tanh(y) for SUPERgaussian sources.\n');
        fprintf('Running ... \n');
        for t = 1:P
            x = X(:,t);              % receive the observed data at time t
            Y(:,t) = B * x;          % output 
            y2	= Y(:,t) * Y(:,t)';
            g	= tanh(Y(:,t)); 
            gy	= g * Y(:,t)';
            G	= y2 - eye(N) + gy - gy' ;
            B	= B - mu * G * B;       % updating the separating matrix
            
            E(t) = CTPI( B*A,'norm');    % store performance index at current iteration
        end
        
    % separating subgaussian sources   
    else                              
        fprintf('Select non-linearity g(y)=y^3 for SUBgaussian sources.\n');
        fprintf('Running EASI algorithm... \n');
        for t = 1:P         
            x = X(:,t);               % receive the observed data at time t
            Y(:,t) = B * x;           % output 
            y2	= Y(:,t) * Y(:,t)';
            g	= diag(y2).* Y(:,t);  % nonlinearity g(y)=y^3
            gy	= g * Y(:,t)';
            G	= y2 - eye(N) + gy - gy';
            B	= B - mu * G * B;     % updating the separating matrix
            
            E(t) = CTPI( B*A,'norm');     % store performance index at current iteration
        end
    end
    fprintf('End ! \n');
    
else      % normalized EASI Algorithm
    
    % separating supergaussian sources
    if lower(source_type) == 'sup'          
        fprintf('Select non-linearity g(y)=tanh(y) for SUPERgaussian sources.\n');
        fprintf('Running normalized EASI algorithm... \n');
        for t = 1:P          
            x = X(:,t);              % receive the observed data at time t
            Y(:,t) = B * x;          % output 
            y2	= Y(:,t) * Y(:,t)';
            g	= tanh(Y(:,t)); 
            gy	= g * Y(:,t)';
            G	= (y2 - eye(N))/(1+ mu*trace(y2)) + (gy - gy')/(1+ mu*abs(g'*Y(:,t))) ;
            B	= B - mu * G * B;    % updating the separating matrix
            
            E(t) = CTPI( B*A,'norm');    % store performance index at current iteration
        end
     
    % separating subgaussian sources
    else                                 
        fprintf('Select non-linearity g(y)=y^3 for SUBgaussian sources.\n');
        fprintf('Running normalized EASI algorithm... \n');
        for t = 1: P
            x = X(:,t);             % receive the observed data at time t
            Y(:,t) = B * x;         % output 
            y2	= Y(:,t) * Y(:,t)';
            g	= diag(y2).* Y(:,t); 
            gy	= g * Y(:,t)';
            G	= (y2 - eye(N))/(1+ mu*trace(y2)) + (gy - gy')/(1+ mu*abs(g'*Y(:,t))) ;
            B	= B - mu * G * B;   % updating the separating matrix
            
            E(t) = CTPI( B*A,'norm');   % store performance index at current iteration      
        end
    end
    fprintf('End ! \n');
end


% Other nonlinearities to be tried............  
% g = Y(:,n).*exp(-Y(:,n).^2/2);  % success in separating sup-guassian
% g = Y(:,n).^3;   % failed
% y2 = Y(:,n)*Y(:,n)';
% g	= diag(y2).*y		;
% g	=   y .* diag(0.1*y2).*diag(y2)	;
% g	=   y .* sqrt(diag(y2))		;
% g	=   y .*  log(diag(y2))		;
% g	= - y .*     (diag(y2)<0.9)	;
% g	=   y ./  log(diag(y2))		;
% g	= - y ./ sqrt(diag(y2))		;
% g	= - y ./      diag(y2)		;
