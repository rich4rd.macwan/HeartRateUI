function [Y, B, E] = VS_EASI( X, B, A, mu, rate, source_type)
% VS_EASI --- varying step-size EASI algorithm
% Format:  [Y, B, E] = VS_EASI( X, B, A, mu, source_type)
% Command:
%          [Y,B,E] = VS_EASI(X, B, A, mu, 'sub');
%          [Y,B,E] = VS_EASI(X, B, A, mu, 'sup');
% Parameter:
% < Input >
%           X --- mixed signals in matrix form N*P, where N represent how many 
%                 input(mixed) singals sources there are, and where P 
%                 represent how many samples in the data.
%           B --- initial separating matrix
%           A --- mixing matrix
%          mu --- initial value of step size
%        rate --- learning rate of step size mu
% source_type --- if 'sub': subgaussian sources
%                 if 'sup': supergaussian sources
% < Output >
%        Y --- Separated signals in matrix form like X
%        B --- Separating matrix
%        E --- row vector of performance index
%
% See also:
%    EASIdemo     VS_EASIdemo
%
% References:
%    [1] Jean-Francois Cardoso, Beate Hvam Laheld, Equivariant Adaptive
%        Source Separation, IEEE trans. on Signal Processing, Vol.44,
%        Dec.1996
%    [2] J.A.Chambers, M.G.Jafari and S.McLaughlim, Variable step-size EASI
%        algorithm for sequential blind source separation. Electronic Letters,
%        vol.40, No.6, 2004
%
% Author  : Zhang Zhi-Lin(zhangzl@vip.163.com)
% Version : 1.0
% Date    : Nov.25, 2004



if nargin ~= 6 
    error('The number of input parameters is wrong !\n');
elseif ~(strcmp(lower(source_type),'sup') | strcmp(lower(source_type), 'sub'))
    error('The last parameter''s value is wrong ! \n');
end


N = size(X,1);
P = size(X,2);

E = zeros(1,P);             % Initialization  of performance index vector      
% Tr = zeros(size(E));        
OldL = B;

fprintf('Start varing step-size EASI algorithm ...\n');
fprintf('Initial step size: %g \n',mu);
if lower(source_type) == 'sup'       % separating supergaussian sources   
    fprintf('Select non-linearity g=tanh(y) for SUPERgaussian sources.\n');
    fprintf('Running ... \n');
    for t = 1:P
        Y(:,t) = B * X(:,t);
        g = tanh(Y(:,t));
        H = Y(:,t) * Y(:,t)' - eye(N) + g * Y(:,t)' - Y(:,t) * g';
        L = H * B;
        Tr(1,t) = -trace( L' * OldL );
        mu = mu + rate * Tr(1,t);         % varing step-size
        B = B - mu * L;                   % update of B
        VStep(1,t) = mu;                  % store the varying step-size
        E(t) = perfdex( B * A );          % error at time t
    end
else                                 % separating subgaussian sources
    fprintf('Select non-linearity g=y^3 for SUBgaussian sources.\n');
    fprintf('Running ... \n');
    for t = 1:P
        Y(:,t) = B * X(:,t);
        g = Y(:,t).^3;
        H = Y(:,t) * Y(:,t)' - eye(N) + g * Y(:,t)' - Y(:,t) * g';
        L = H * B;
        Tr(1,t) = -trace( L' * OldL );
        mu = mu + rate * Tr(1,t);         % varing step-size
        B = B - mu * L;                   % update of B
        VStep(1,t) = mu;                  % store the varying step-size
        OldL = L;
        E(t) = SIRPI( B * A );            % error at time t
    end
end
fprintf('End of varying step-size EASI algorithm! \n');
fprintf('Final step-size: %g\n',mu);
figure; plot(Tr); title('\bf Change of step size (without multiply scale factor)');
figure; plot(VStep); title('\bf Varying step size');

% Other nonlinearities to be tried............  
% g = Y(:,n).*exp(-Y(:,n).^2/2);  % success in separating sup-guassian
% g = Y(:,n).^3;   % failed
% y2 = Y(:,n)*Y(:,n)';
% g	= diag(y2).*y		;
% g	=   y .* diag(0.1*y2).*diag(y2)	;
% g	=   y .* sqrt(diag(y2))		;
% g	=   y .*  log(diag(y2))		;
% g	= - y .*     (diag(y2)<0.9)	;
% g	=   y ./  log(diag(y2))		;
% g	= - y ./ sqrt(diag(y2))		;
% g	= - y ./      diag(y2)		;
