function W = JADEtd( X, m, p)
% Blind identification by joint diagonalization of cumulant matrices with time delays   
% THIS CODE ASSUMES THAT THE SIGNALS ARE TEMPORALLY CORRELATED OF FOURTH ORDER
% This code is addaptation of SOBI, including cumulant matrices with time delays of
% fourth order
%
% Format:
%        W = JADEtd( X, m, p)
% 
% Parameters:
%   W - demixing matrix
%   X - mixed signal matrix
%   m - number of sources
%   p - number of correlation matrices to be diagonalized 1-400, default 100
% 
% Other parameters used in this routine:
%   F - mixing matrix
%   S - estimated source signals
%   n - number of sensors
%
% REVISED: June 6, 2002 by Pando Georgiev         
% 
% REFERENCES:
% [1] P. Georgiev, A. Cichocki, "Robust Blind Source Separation
%     utilizing second and fourth order statistics", to appear in Proc. ICANN (Madrid 2002).
% [2] A. Cichocki and S. Amari, Adaptive Blind Signal and Image Processing, Wiley,  2003.
%
%


[M,N]=size(X);n=M;T=N;
if nargin==1,
 m=n; % source detection 

 %p=100; % number of correlation matrices to be diagonalized 
 end;
if nargin==2,
 p=100 ; % number of correlation matrices to be diagonalized
end; 
pm=p*m; % for convenience
%%% whitening
Rx=(X*X')/N;
if m<n, %assumes white noise
  [U,D]=eig(Rx); [puiss,k]=sort(diag(D));
  ibl= sqrt(puiss(n-m+1:n)-mean(puiss(1:n-m)));
   bl = ones(m,1) ./ ibl ;
   Q=diag(bl)*U(1:n,k(n-m+1:n))';
  I=U(1:n,k(n-m+1:n))*diag(ibl);
else    %assumes no noise
   I=sqrtm(Rx);
   Q=inv(I);
end;
X=Q*X;

 for p1=0:p;
 %%%%%%%%%%%%%cumulant matrices
C22zzp	= zeros(M,M,p);
Xp=X(:,p1+1:N); 
X0=X(:,1:N-p1); 
   	zztzptzp	= X0 * diag(sparse(sum(Xp .* Xp, 1))) * X0' / (N-p1);
	zzt		= X * X' / N;
	trzpzpt		= sum(sum(Xp .* Xp, 1), 2) / (N-p1);
	zzpt		= X0 * Xp' / (N-p1);
	zpzt		= Xp * X0' / (N-p1);
 
	c2(:,:,p1+1)	= zztzptzp - zzt * trzpzpt - 2 * zzpt * zpzt;
  end;
 
  
k=1;for u=1:m:pm; 
A(:,u:u+m-1)=c2(:,:,2+(u-1)/m);
end;
%%%joint diagonalization
seuil=1/sqrt(T)/100; encore=1; V=eye(m);
while encore, encore=0;
 for p=1:m-1,
  for q=p+1:m,
   %%% Givens rotations
   g=[   A(p,p:m:pm)-A(q,q:m:pm)  ;
         A(p,q:m:pm)+A(q,p:m:pm)  ;
      i*(A(q,p:m:pm)-A(p,q:m:pm)) ];
	  [vcp,D] = eig(real(g*g')); [la,K]=sort(diag(D));
   angles=vcp(:,K(3));angles=sign(angles(1))*angles;
   c=sqrt(0.5+angles(1)/2);
   sr=0.5*(angles(2)-j*angles(3))/c; sc=conj(sr);
   oui = abs(sr)>seuil ;
   encore=encore | oui ;
   if oui , %%%update of the A and V matrices 
    colp=A(:,p:m:pm);colq=A(:,q:m:pm);
    A(:,p:m:pm)=c*colp+sr*colq;A(:,q:m:pm)=c*colq-sc*colp;
    rowp=A(p,:);rowq=A(q,:);
    A(p,:)=c*rowp+sc*rowq;A(q,:)=c*rowq-sr*rowp;
    temp=V(:,p);
    V(:,p)=c*V(:,p)+sr*V(:,q);V(:,q)=c*V(:,q)-sc*temp;
   end%% if
  end%% q loop
 end%% p loop
end%% while
%%%estimation of the mixing matrix and signal separation
end_time=clock; 
F=I*V; 
S=V'*X;
 W=V'*Q;
 