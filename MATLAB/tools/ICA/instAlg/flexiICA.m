function [Y,B,E] = flexiICA(X, B, A, mu, fgt, varargin)
% flexiICA: flexible ICA algorithm. It can separate linearly 
%         mixed sub-Gaussian and super-Gaussian sources. Compared to the 
%         extended Infomax algorithm(see ExtICA.m), it has faster
%         convergence speed and better performance when separating mixed
%         super- and sub-Gaussian sources. 
%         
% Format : [Y,B,E] = flexiICA(X, B, A, mu, fgt, var1, var2);
% Command: [Y,B,E] = flexiICA(X, B, A, mu, fgt);
%          [Y,B,E] = flexiICA(X, B, A, mu, fgt,'online');
%          [Y,B,E] = flexiICA(X, B, A, mu, fgt,'offline',2000);
%
% Parameters:
%     < Input >
%          X --- mixed signals in matrix form N*P, where N represent how many 
%                input(mixed) singals sources there are, and where P 
%                represent how many samples in the data. It is suggested that 
%                the data X has been PREWHITENED.
%          B --- initial separating matrix, commend identity matrix
%          A --- mixing matrix
%         mu --- step size
%        fgt --- forgetting factor for on-line computing kurtosis, say, 0.98
%       var1 --- If its value is 'online', the algorithm adopts online
%                version. If its value is 'offline', the algorithm adopts
%                offline version. Default value is 'online'
%       var2 --- iteration times. Only when var1's value is 'offline', it
%                is valid.
%   
%   < Output >
%       Y --- Separated sources in matrix form, like X
%       B --- Separating matrix
%       E --- row vector of performance index. The definition of
%             performance index refers to function CTPI (CTPI.m). In this
%             program, CTPI(A,'ave') is called.
%
% Reference:
%       [1]S.Choi, A.Cichocki, and S.Amari: Flexible independent component
%       analysis. J.of VLSI Signal Processing, vol.20, pp.25-38, 2000
%
% See also
%       flexiICAdemo      ExtICA    CTPI
%
% Author : Zhi-Lin Zhang (zhangzl@vip.163.com)
% Version: 2.1    Date: Feb.7, 2006
% Version: 2.0    Date: Mar.29, 2005
% Version: 1.0    Date: Mar.9,  2005



if nargin == 5
    type = 'online';
    
elseif strcmp(lower(varargin{1}),'online') & (nargin == 6)
    type = 'online';
    
elseif strcmp(lower(varargin{1}),'offline') & (nargin == 7)
    type = 'offline';
    iter = varargin{2};
    
else
    error(sprintf('Number of parameters or its value is wrong ! \nType ''help flexiICA''for detail !\n'));

end
    
      
[numOfIC, numOfSamp] = size(X);


%============================= Online Version =============================================
if strcmp(type,'online')
    % initialization
    Y = zeros(size(X));
    E = zeros(1,numOfSamp);
    Im = eye(numOfIC);     % identity matrix
    
    M4 = zeros(numOfIC,1);
    M2 = ones(numOfIC,1);
    
    % Starting iteration
    for t = 1 : numOfSamp   
        Y(:,t) = B * X(:,t);   
        
        % choose non-linearity for each IC
        for n = 1: numOfIC
            % on-line compute kurtosis for each IC
            kurt = M4(n,1)/M2(n,1)^2 - 3;
            M4(n,1) = fgt * M4(n,1) + (1-fgt) * Y(n,t)^4;
            M2(n,1) = fgt * M2(n,1) + (1-fgt) * Y(n,t)^2;
            
            % If estimated kurtosis of recovered signal Yi(t) is greater
            % than 20, then choose a = 0.8;
            if kurt > 20 
                a = 0.8;
%                 score(n,1) = abs(Y(n,t))^(a-1) * sign(Y(n,t));
                % This nonlinearity is singular around the origin. Thus in the
                % code, for yi between -0.005 and 0.005, the corresponding
                % nonlinearity is restricted to have constant values.
                if abs(Y(n,t)) < 0.005
                    score(n,1) = abs(0.005)^(a-1) * sign(Y(n,t));
                    
                else
                    score(n,1) = abs(Y(n,t))^(a-1) * sign(Y(n,t));
                    
                end
                
            % If estimated kurtosis of recovered signal Yi(t) is between 0
            % and 20, then choose a = 1;
            elseif ( 0 <= kurt ) & ( kurt <= 20 )
                a = 1;
                score(n,1) = abs(Y(n,t))^(a-1) * sign(Y(n,t));
                
            % If estimated kurtosis of recovered signal Yi(t) is negative,
            % then choose a = 4;
            else
                a = 4;
                score(n,1) = abs(Y(n,t))^(a-1) * sign(Y(n,t));
                
            end   
            
        end % end of Computing non-linearity for each IC
        
        % update 
        gy	= score * Y(:,t)';
        B	= B + mu * (Im - Y(:,t) * Y(:,t)' - gy + gy') * B;    % if X is prewhitened
        B = B/norm(B);
        
        % computer performance index when iterating No.t samples
        E(1,t) = CTPI( B*A,'norm');           
        
    end
end


%============================= Offline Version =============================================
if strcmp(type, 'offline')
    % initialization
    Y = zeros(size(X));
    E = zeros(1,iter);
    Im = eye(numOfIC);     % identity matrix
    
    M4 = zeros(numOfIC,1);
    M2 = ones(numOfIC,1);
    
    % Starting iteration
    for t = 1 : iter   
        Y = B * X;  
    
        % choose non-linearity for each IC
        for n = 1: numOfIC
            % compute kurtosis for each IC
            kurt = M4(n,1)/M2(n,1)^2 - 3;
            M4(n,1) = fgt * M4(n,1) + (1-fgt) * mean(Y(n,:).^4);
            M2(n,1) = fgt * M2(n,1) + (1-fgt) * mean(Y(n,:).^2);
            
            % If estimated kurtosis of recovered signal Yi(t) is greater
            % than 20, then choose a = 0.8;
            if kurt > 20 
                a = 0.8;
%                 score(n,:) = abs(Y(n,:)).^(a-1) .* sign(Y(n,:));
                % This nonlinearity is singular around the origin. Thus in the
                % code, for yi between -0.005 and 0.005, the corresponding
                % nonlinearity is restricted to have constant values.
                for p = 1: numOfSamp
                    if abs(Y(n,p)) < 0.005
                        score(n,p) = abs(0.005)^(a-1) * sign(Y(n,p));
                    else
                        score(n,p) = abs(Y(n,p))^(a-1) * sign(Y(n,p));
                    end
                end
                
                            
            % If estimated kurtosis of recovered signal Yi(t) is between 0
            % and 20, then choose a = 1;
            elseif ( 0 <= kurt ) & ( kurt <= 20 )
                a = 1;
                score(n,:) = abs( Y(n,:)).^(a-1) .* sign( Y(n,:) ) ;
                
            % If estimated kurtosis of recovered signal Yi(t) is negative,
            % then choose a = 4;
            else
                a = 4;
                score(n,:) = abs( Y(n,:)).^(a-1) .* sign( Y(n,:) ) ;
                
            end   
            
        end % end of Computing non-linearity for each IC
        
        % update 
        gy	= score * Y';
        B	= B + mu * (Im - Y * Y'/numOfSamp - gy/numOfSamp + gy'/numOfSamp) * B;       
        B = B/norm(B);
        
        % computer performance index when iterating No.t samples
        E(1,t) = CTPI( B*A,'norm');           
        
    end % end of iteration
    
end

