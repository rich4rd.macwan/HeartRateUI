function [Y,B] = acffpICA( X, ICs, orderVector, mu, epsilon, maxIter)
% acffpICA --- Fast fixed-point ICA taking into account both the nongaussianity and
%              the autocorrelations of the time-dependent signals. The input signals
%              X should be PREWHITENED.
%
% Command:
%    [Y,B] = acffpICA( X, ICs, orderVector, mu, epsilon, maxIter)
%
% Parameters:
%          Y --- estimated source signals, where each row is an estimated
%                signal. The number of estimated signals is set by input
%                parameter 'ICs'.
%          B --- separating matrix, that is, Y = B'* X.
%          X --- prewhitened observed mixed signals. Each row is a mixed
%                signal.
%        ICs --- the number of independent component, which should be estimated.
% orderVector--- its each element is the AR degree of the extracted signal
%         mu --- step-size
%    epsilon --- stopping criterion, say, 0.0001. When the changed value 
%                of weighted vector is less than it, then the algorithm 
%                stops estimating the current independent component and to
%                estimate the next independent one.
%    maxIter --- maximum iterations for estimating each independent
%                component.
%
% See also:
%     acffpICAdemo      ffpICA       FastICA
%
% Reference:
%    [1] A.Hyvarinen: Complexity pursuit: combining nongaussianity and
%        autocorrelations for signal separation, ICA 2000
%
% Author��Zhi-Lin Zhang
%         zlzhang@uestc.edu.cn
%
% version: 1.0     Date: June 21, 2006
%



[SensorNum, IClen] = size(X);

B = zeros(SensorNum,ICs);                    % separating matrix


fprintf('Print information ...\n');
fprintf('The input parameters are as follows:\n');
fprintf('    number of estimated sources is :  %d\n',ICs);
fprintf('     number of observed signals is :  %d\n',SensorNum);
fprintf('             stopping criterion is :  %g\n',epsilon);
fprintf('    maximum iterations for each IC :  %d\n',maxIter);

% Now, estimate the independent components one by one
fprintf('\nStarting the acffpICA algorithm.\n');
for i=1:ICs  
   
    flag = 1;       % control variable, which controls the iteration for each IC
    loop = 1;       % variable, recording iterations
    
    w = rand(SensorNum,1)-0.5;         % weighted vector
    w = w - B * B'* w;                 % orthogonalize
    w = w / norm(w);                   % normalize
    oldw = w;                          % store the last value of the demixing vector
    
    % true iteration for each IC
    while (flag == 1)
        y = w'* X;
        order = orderVector(i);           % the AR order of the current signal to extract
        
        % linear prediction
        b = lpc(y,order);                      % obtain the LP coefficience from the output
        for j = 1:size(X,1)
           est_X(j,:) = filter([0 -b(2:end)],1,X(j,:));     % estimated signal
           En(j,:) = X(j,:) - est_X(j,:);                    % prediction error
        end

        % updating rule
        w = w - mu * (En * tanh(w'* En)')/IClen;
        w = w/norm(w,'fro');
       
        % decide whether the algorithm for current IC has converged
        wchange = 1-abs(w'* oldw);
        fprintf('No.%d IC, No.%d iteration: change in w is %g\n',i,loop, wchange);
        if wchange < epsilon
            fprintf('Converged after %d iteration\n\n',loop);
            flag = 0;
            w = w - B * B'* w;     % orthogonalize
            w = w / norm(w);       % normalize
            B(:,i) = w;            % store w in B
        end
        
        % decide whether the iterations for current IC has achieved maximum
        % value
        if loop >= maxIter
            fprintf('After %d iteration, still cannot converge.\n\n',loop);
            flag = 0;
            w = w-B*B'*w;           % orthogonalize
            w = w/norm(w);          % normalize
            B(:,i) = w;             % store w in B
        end
        
        oldw = w;
        loop = loop + 1;
        
    end  % end of iteration for each IC
    
end      % the algorithm stops here

% estimated source signals
Y = B' * X;
fprintf('All the source signals have been recovered !\n');
fprintf('End of the acffpICA algorithm.\n\n');

    