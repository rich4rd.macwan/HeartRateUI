function [Y,B,E] = ExtICA(X, B, A, mu, fgt, varargin)
% ExtICA: extended Infomax algorithm. It can separate linearly mixed
%         sub-Gaussian and super-Gaussian sources via on-line estimating
%         each output's kurtosis and selecting suitable nonlinearity.
%         However, its drawback is slow convergence.
%
% Format : [Y,B,E] = ExtICA(X, B, A, mu, fgt, var1, var2);
% Command: [Y,B,E] = ExtICA(X, B, A, mu, fgt);
%          [Y,B,E] = ExtICA(X, B, A, mu, fgt,'online');
%          [Y,B,E] = ExtICA(X, B, A, mu, fgt,'offline',2000);
%
% Parameters:
%     < Input >
%          X --- mixed signals in matrix form N*P, where N represent how many 
%                input(mixed) singals sources there are, and where P 
%                represent how many samples in the data.
%          B --- initial separating matrix, commend identity matrix
%          A --- mixing matrix
%         mu --- step size
%        fgt --- forgetting factor for on-line computing kurtosis, say, 0.98
%       var1 --- If its value is 'online', the algorithm adopts online
%                version. If its value is 'offline', the algorithm adopts
%                offline version. Default value is 'online'
%       var2 --- iteration times. Only when var1's value is 'offline', it
%                is valid.
%   
%   < Output >
%       Y --- Separated sources in matrix form, like X
%       B --- Separating matrix
%       E --- row vector of performance index. The definition of
%             performance index refers to function CTPI (CTPI.m). In this
%             program, CTPI(A,'norm') is called.
%
% See also:
%       CTPI        ExtICAdemo      flexiICA
%
% Reference:
%    [1] Mark Girolami: An alternative perspective on adaptive independent
%    component analysis algorithms. Neural Computation 10,2103-2114(1998)
%    [2] T.-W. Lee, M. Girolami, T. Sejnowski, Independent component analysis using an
%       extended infomax algorithm for mixed subgaussian and supergaussian sources,
%       Neural Computation 11,417-441(1999) 
%
% Author : Zhi-Lin Zhang (zlzhang@uestc.edu.cn)
% Version: 2.5   Date: Mar.30,2005
% Version: 1.0   Date: Oct.31,2004



 
if nargin == 5
    type = 'online';
    
elseif strcmp(lower(varargin{1}),'online') & (nargin == 6)
    type = 'online';
    
elseif strcmp(lower(varargin{1}),'offline') & (nargin == 7)
    type = 'offline';
    iter = varargin{2};
    
else
    error(sprintf('Number of parameters or its value is wrong ! \nType ''help ExtICA''for detail !\n'));

end

[numOfIC, numOfSamp] = size(X);

%============================= Online Version =============================================
if strcmp(type,'online')
    % initialization
    Y = zeros(size(X));
    E = zeros(1,numOfSamp);
    Im = eye(numOfIC);     % identity matrix
    
    M4 = zeros(numOfIC,1);
    M2 = ones(numOfIC,1);
    
    % Starting iteration
    for t = 1 : numOfSamp   
        Y(:,t) = B * X(:,t);   
        
        % choose non-linearity for each IC
        for n = 1: numOfIC
            % on-line compute kurtosis for each IC
            kurt = M4(n,1)/M2(n,1)^2 - 3;
            M4(n,1) = fgt * M4(n,1) + (1-fgt) * Y(n,t)^4;
            M2(n,1) = fgt * M2(n,1) + (1-fgt) * Y(n,t)^2;
            
            % If estimated kurtosis of recovered signal Yi(t) is greater
            % than 0, then choose nonlinearity: f(y)=y+tanh(y)
            if kurt > 0   
%                 score(n,1) = Y(n,t) + tanh( Y(n,t) );
                score(n,1) = 2*tanh( Y(n,t) );

            % If estimated kurtosis of recovered signal Yi(t) is less than 0
            % then choose nonlinearity: f(y)=y-tanh(y)
            else
%                 score(n,1) = Y(n,t) - tanh( Y(n,t) ); 
                score(n,1) = Y(n,t).^3; 
            end   
            
        end % end of Computing non-linearity for each IC
        
        % update 
        B	= B + mu * (Im - score * Y(:,t)') * B;  

        % computer performance index when iterating No.t samples
        E(1,t) = CTPI( B*A,'norm');           
        
    end
end


%============================= Offline Version =============================================
if strcmp(type, 'offline')
    % initialization
    Y = zeros(size(X));
    E = zeros(1,iter);
    Im = eye(numOfIC);     % identity matrix
    
    M4 = zeros(numOfIC,1);
    M2 = ones(numOfIC,1);
    
    % Starting iteration
    for t = 1 : iter  
        
        if mod(t,50) == 0  fprintf('Iteration # %d \n',t); end
        
        Y = B * X;  
    
        % choose non-linearity for each IC
        for n = 1: numOfIC
            % compute kurtosis for each IC
            kurt = M4(n,1)/M2(n,1)^2 - 3;
            M4(n,1) = fgt * M4(n,1) + (1-fgt) * mean(Y(n,:).^4);
            M2(n,1) = fgt * M2(n,1) + (1-fgt) * mean(Y(n,:).^2);
            
            % If estimated kurtosis of recovered signal Yi(t) is greater
            % than 0, then choose nonlinearity: f(y)=y+tanh(y)
            if kurt > 0   
%                 score(n,:) = Y(n,:) + tanh( Y(n,:) );
                  score(n,:) = tanh( Y(n,:) );

            % If estimated kurtosis of recovered signal Yi(t) is less than 0
            % then choose nonlinearity: f(y)=y-tanh(y)
            else
%                 score(n,:) = Y(n,:) - tanh( Y(n,:) ); 
                  score(n,:) = Y(n,:).^3;
                
            end   
              
        end % end of Computing non-linearity for each IC
        
        % update 
        gy	= score * Y';   
        B	= B + mu * (Im - gy/numOfSamp) * B; 
        
        % computer performance index when iterating No.t samples
        E(1,t) = CTPI( B*A,'norm');           
        
    end % end of iteration
    
end





