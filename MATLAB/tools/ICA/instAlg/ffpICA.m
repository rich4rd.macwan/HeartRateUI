function [Y,B] = ffpICA( X, ICs, epsilon, maxIter)
% ffpICA --- Fast Fixed-Point Algorithm based on kurtosis, using deflationary 
%             method to estimate several source signals. Note that the input
%             signals should be prewhitened.
%
% Command:
%    [Y,B] = ffpICA( X, ICs, epsilon, maxIter)
%
% Parameters:
%          Y --- estimated source signals, where each row is an estimated
%                signal. The number of estimated signals is set by input
%                parameter 'ICs'.
%          B --- separating matrix, that is, Y = B'* X.
%          X --- prewhitened observed mixed signals. Each row is a mixed
%                signal.
%        ICs --- the number of independent component, which should be
%                estimated.
%    epsilon --- stopping criterion, say, 0.0001. When the changed value 
%                of weighted vector is less than it, then the algorithm 
%                stops estimating the current independent component and to
%                estimate the next independent one.
%    maxIter --- maximum iterations for estimating each independent
%                component.
%
% See also:
%     whiten    FastICA
%
% Reference:
%    [1] A.Hyvarinen, E.Oja: A fast fixed-point algorithm for independent
%    component analysis. Neural Computation 9:1483-1492,1997
%
% Author��Zhi-Lin Zhang
%         zlzhang@uestc.edu.cn
%         http://teacher.uestc.edu.cn/teacher/teacher.jsp?TID=zzl80320
%
% version: 1.0     Date: Apr.19,2005
%



[SensorNum, IClen] = size(X);

B = zeros(SensorNum,ICs);                    % separating matrix


fprintf('Print information ...\n');
fprintf('The input parameters are as follows:\n');
fprintf('    number of estimated sources is :  %d\n',ICs);
fprintf('     number of observed signals is :  %d\n',SensorNum);
fprintf('             stopping criterion is :  %d\n',epsilon);
fprintf('    maximum iterations for each IC :  %d\n',maxIter);

% Now, estimate the independent components one by one
fprintf('\nStarting Fast Fixed-Point ICA Algorithm Based on Kurtosis.\n\n');
for i=1:ICs  
   
    flag = 1;       % control variable, which controls the iteration for each IC
    loop = 1;       % variable, recording iterations
    
    w = rand(SensorNum,1)-0.5;         % weighted vector
    w = w - B * B'* w;                 % orthogonalize
    w = w / norm(w);                   % normalize
    oldw = w;                          % store the last value of the weighted vector
    
    % true iteration for each IC
    while (flag == 1)
        
        % updating rule
        w = X * ( X'* w).^3 / IClen - 3 * w;
        
        % normalize
        w = w/norm(w);
        
        % decide whether the algorithm for current IC has converged
        wchange = 1-abs(w'* oldw);
        fprintf('No.%d IC, No.%d iteration: change in w is %g\n',i,loop, wchange);
        if wchange < epsilon
            fprintf('Converged after %d iteration\n\n',loop);
            flag = 0;
            w = w - B * B'* w;     % orthogonalize
            w = w / norm(w);       % normalize
            B(:,i)=w;              % store w in B
        end
        
        % decide whether the iterations for current IC has achieved maximum
        % value
        if loop >= maxIter
            fprintf('After %d iteration, still cannot converge.\n\n',loop);
            flag = 0;
            w=w-B*B'*w;           % orthogonalize
            w=w/norm(w);          % normalize
            B(:,i)=w;             % store w in B
        end
        
        oldw = w;
        loop = loop + 1;
        
    end  % end of iteration for each IC
    
end      % the algorithm stops here

% estimated source signals
Y = B' * X;
fprintf('All the source signals have been recovered.\n');

    