function [S, F, C] = HJbss(E, mu, varargin)
% HJbss -- Herault-Jutten algorithm.
%          When input observed data(mixed signals), step size and other
%          parameters, then output the separated source signals, separating
%          matrix and converged coeficient matrix of the recurrent neural
%          network. The algorithm's stable conditions have two folds:
%          1)c12 * c21 < 1, which depends on the mixing matrix
%          2)E{S1^(m-1) S2^(n+1)} * E{S1^(n+1) S2^(m-1)} < 
%            (n/m)^2 * E{S1^(n-1) S2^(m+1)} * E{S1^(m+1) S2^(n-1)}, which
%            can be expressed only in terms of statistics of sources,
%            assuming the non-lineary functions have the form:
%            f(x) = x^n ,   g(x) = x^m, where n and m are any odd integers,
%            not both equal to one. See Ref.[3] for details.
% Comments:
%   It is the first ICA algorithm(also the on-line algorithm). And its 
%   performance greately depends on the initial value of coeficient matrix 
%   C0. What's more, it often cannot obtain good convergence.
% Command:
%   [S, F, C] = HJbss(E, mu, C0, 'track');
%   [S, F, C] = HJbss(E, mu, C0);
%   [S, F, C] = HJbss(E, mu);
% Format:
%       [S, F, C] = HJbss(E, mu, C0, 'track')
% Parameters:
%        E --- input data matrix; each row represents a signal and each
%              collumn represents a sample vector. The input data is
%              zero-mean and unit-variance; if not so, recommend
%              preprocessing.
%       mu --- step size
%       C0 --- initial value of coeficient matrix of the recurrent neural
%              network. Default value is zero matrix. Notice the diagonal
%              elements of C0 are equal to zero, and other elements should
%              satisfy the stability condition. For example, if C0 is 2*2
%              matrix, then its elements should satisfy:
%              C0(1,1)=C0(2,2)=0; C0(1,2)* C0(2,1) < 1
%   'track'--- if input this parameter, then output C is a 3-dimensional
%              matrix, whose third dimension is time index. That is to say,
%              C(:,:,t) is the neural network's coeficient matrix at time
%              t. If not input this parameter, then output C is a
%              2-dimensional matrix.
%        S --- separated source signal. Each row represents a source signal.
%        F --- separating matrix, i.e., F = ( I + C )^(-1).
%        C --- coeficient matrix of the recurrent neural network. 
% See also:
%       HJbssdemo
% References:
%   [1]Jutten C., Herault J.: Blind separation of sources, part 1: An
%      adaptive algorithm based on neuromimetic architecture. Signal
%      Processing 24(1991)1-10
%   [2]Comon P., Jutten C., Herault J.: Blind separation of sources, part 2: 
%      Problems statement. Signal Processing 24(1991)11-20
%   [3]Sorouchyari E.: Blind separation of sources, part 3: Stability 
%      analysis. Signal Processing 24(1991)21-29.
%
%   Author:  Zhi-Lin Zhang
%     Date:  Feb.14,2005
%  Version:  2.0
%  History:  Nov.8,2003 (Version: 1.0)





[ICnum, len] = size(E);

vin = length(varargin); 
if vin == 0                 % if 2 input variables
    % C is initialized by zero matrix; and is output with the final value
    C0 = zeros(ICnum);
    track = 0;
elseif vin == 1             % if 3 input variables
    % C is initialized by the input C0,and is output with the final value
    C0 = varargin{1};
    track = 0;
elseif vin == 2             % if 4 input variables
    % C is initialized by the input C0,and is output with 3-dimensional
    % matrix.
    C0 = varargin{1};
    track = 1;
else                        % if more than 4 input variables
    fprintf('The number of input variables is wrong !\n');
    return;
end


if track == 1
    C = zeros(ICnum,ICnum,len);
    C(:,:,1) = C0;
    
    for t = 1 : len             % iteration
        S(:,t) = inv(eye(ICnum) + C(:,:,t)) * E(:,t);
        
        % compute coeficient matrix C
        for i = 1 : ICnum       % row
            for k = 1 : ICnum   % column
                if k ~= i
%                     % nonlineary functions f(s)=s^3, g(s)=arctan(s)
%                     C(i,k,t+1) = C(i,k,t) + mu * S(i,t)^3 * atan(S(k,t));
                    
                    % nonlineary functions f(s)=s^3, g(s)=s
                    % when chose these functions, the stable condition is
                    % kurt(source1)*kurt(source2)< 9
                    C(i,k,t+1) = C(i,k,t) + mu * S(i,t)^3 * S(k,t);
                end
            end
        end
        
    end
    
    C(:,:,len+1) = [];
    F = inv( eye(ICnum) + C(:,:,len) );
    
else        % if track == 0
    C = C0;
    
    for t = 1 : len             % iteration
        S(:,t) = inv(eye(ICnum) + C) * E(:,t);
        
        % compute coeficient matrix C
        for i = 1 : ICnum       % row
            for k = 1 : ICnum   % column
                if k ~= i
%                     % nonlineary functions f(s)=s^3, g(s)=arctan(s)
%                     C(i,k) = C(i,k) + mu * S(i,t)^3 * atan(S(k,t));
                    
                    % nonlineary functions f(s)=s^3, g(s)=s
                    % when chose these functions, the stable condition is
                    % kurt(source1)*kurt(source2)< 9
                    C(i,k) = C(i,k) + mu * S(i,t)^3 * S(k,t);
                end 
            end
        end
        
    end
    
    F = inv( eye(ICnum) + C );
end
    
    