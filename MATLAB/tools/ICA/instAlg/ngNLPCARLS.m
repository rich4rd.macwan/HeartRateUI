function [Y, B, E] = ngNLPCARLS(V, B, A, b, source_type)
% ngNLPCARLS --- NonLinear recursive least-squares algorithm based on natural gradient. 
%                It is assumed that the input signals are prewhitened. It is said that 
%                the reviewer believed that this algorithm certainly was the
%                fatest online algorithm at that time(2001-2002).
%                
% Parameters:
% < Input >
%            V --- whitened data in matrix form.Each row represents a signal;
%                  each column is sampling point.
%            B --- initial separating matrix
%            A --- mixing matrix. If processing real world signals, set A
%                  identity matrix
%            b --- forgetting constant that should be close to unity
%  source_type --- if 'sub': subgaussian sources
%                  if 'sup': supergaussian sources
% < Output >
%            Y --- Separated signals in matrix form like V
%            B --- Separating matrix
%            E --- row vector of performance index
%
% Commands:
%   [Y,B,E] = NLPCARLS(Z, B, A, b, 'sub')
%   [Y,B,E] = NLPCARLS(Z, B, A, b, 'sup')
%
% Use:
%    Tri     CTPI    NLPCARLS
%
% See also:
%    NLPCARLSdemo   
%
% References:
%  [1] Xiao-Long Zhu and Xian-Da Zhang: Adaptive RLS algorithm for blind
%      source separation using a natural gradient. IEEE Signal Processing
%      Letters,vol.9(12),Dec.2002
%
%  Author: Zhi-Lin Zhang (zlzhang@uestc.edu.cn)
% Version: 1.0
%    Date: Apr.3, 2005



if nargin ~= 5
    error('The number of input parameters is wrong !\n');
elseif ~(strcmp(lower(source_type),'sup') | strcmp(lower(source_type), 'sub'))
    error('The fifth parameter''s value is wrong ! \n');
end

[numOfIC, numOfSamp] = size(V);

P = eye(numOfIC);     % initialize auxiliary variable

fprintf('Starting natural gradient based Nonlinear PCA algorithm ...\n');

if lower(source_type) == 'sup'       % separating supergaussian sources   
    
    fprintf('Select non-linearity g(y)=y-tanh(y) for SUPER-Gaussian sources.\n');
    fprintf('Running ... \n');
        
    for n = 1 : numOfSamp

        Y(:,n) = B * V(:,n);
        
        z = Y(:,n) - tanh(Y(:,n));  % nonlinearity for sub-Gaussian source signals       
        Q = P / ( b + z'* P * Y(:,n) );
        P = 1/b * ( P - Q * Y(:,n) * z'* P );
        B = B + ( P * z * V(:,n)' - Q * Y(:,n) * z' * B );
        
        E(n) = CTPI( B*A,'ave');   % performance index at current iteration      
    end
    
% separating super-Gaussian sources    
else  
    fprintf('Select non-linearity g(y)=tanh(y) for SUB-Gaussian sources.\n');
    fprintf('Running ... \n');
    
    for n = 1 : numOfSamp

        Y(:,n) = B * V(:,n);
        
        z = tanh(Y(:,n));  % nonlinearity for sub-Gaussian source signals       
        Q = P / ( b + z'* P * Y(:,n) );
        P = 1/b * ( P - Q * Y(:,n) * z'* P );
        B = B + ( P * z * V(:,n)' - Q * Y(:,n) * z' * B );
        
        E(n) = CTPI( B*A,'ave');   % performance index at current iteration      
    end
end

fprintf('End ! \n');