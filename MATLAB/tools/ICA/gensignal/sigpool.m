function Y = sigpool( numOfIC, length, type, show)
% SIGPOOL ---- generate signals with zero mean and unit variance.
% Format:  Y = sigpool( numOfIC, length, type, show)
% Command: 
%          Y = sigpool( 5, 2000, 'sup', 'on');
%          Y = sigpool( 4, 5000, 'sub');
% Parameters:
%    < Input >
%       numOfIC --- number of sources
%        length --- sampling points of sources
%          type --- if type == 'sup', then generate supergaussian sources
%                   if type == 'sub', then generate subgaussian sources
%          show --- if show == 'on', then graph the generated sources and
%                   print the information of the sources (Default)
%                   if show == 'off', then NOT do so           
%
% Author:    Zhi-Lin Zhang
% E-mail:    zlzhang@uestc.edu.cn
% Version:   2.1     Date:   May 8, 2005
% Version:   2.0     Date:   Nov.23, 2004


%====================================================================
% Check the parameters and set default value
if(nargin<4)  graphAndPrint = 1; 
else
    switch lower(show)
        case 'on'
            graphAndPrint = 1;          
        case 'off'
            graphAndPrint = 0;
        otherwise
            error(sprintf('Illegal value [ %s ] for parameter: ''show''\n', show));
    end
    
    if strcmp(lower(type),'sub')
        if (numOfIC > 8) | (numOfIC < 1)
            error('You have choosen wrong number of subgaussian sources.');
            return;
        end    
    elseif strcmp(lower(type),'sup')
        if (numOfIC > 2) | (numOfIC < 1)
            error('You have choosen wrong number of supergaussian sources.');
            return;
        end
    else
        error(sprintf('Illegal value [ %s ] for parameter: ''type''\n', type));
    end
end
%====================================================================
% 产生信号
v=linspace(0,1,length);
t=[0:length-1];
sig(1,:) = sign(cos(2*pi*155*v));                              % 符号信号
sig(2,:) = sin(2*pi*800*v);                                    % 高频正弦信号
sig(3,:) = sin(2*pi*90*v);                                     % 低频正弦信号
sig(4,:) = sin(2*pi*9*v).*sin(2*pi*300*v);                     % 幅度调制信号
sig(5,:)= ((rem(t,27)-13)/9);                                  % 三角波形
sig(6,:) = sin(2*pi*300*v-6*cos(2*pi*60*v));                   % 相位调制信号
sig(7,:) = 2*(rand(1,length)-0.5);                             % 均匀分布噪声
sig(8,:) = sqrt(raylrnd(1,[1,length]));                        % 参数为1的瑞利分布的平方根信号

sig(9,:) = ((rand(1,length)<.5)*2-1).*log(rand(1,length));     % 拉普拉斯分布噪声（超高斯）
sig(10,:)= ((rem(t,23)-11)/9).^5;                              % 曲线波形 （超高斯）

if strcmp(lower(type),'sup') 
    for i=1:numOfIC
        Y(i,:) = sig(10-i+1,:);
    end
else
    for i=1:numOfIC
        Y(i,:) = sig(i,:);
    end
end

Y = standarize(Y);          %  标准化


if(graphAndPrint)    % graph the sources and print the information
    figure;
    for i = numOfIC : -1 : 1
        subplot(numOfIC,1,i);
        plot(Y(i,[length-499:length]));axis([-inf,inf,-5,5]);
    end
    title('\bf Sources');
    fprintf('\n');
    fprintf('=================== Generating artificial sources ... =================\n');
    for i =1:numOfIC
        fprintf('      No. %g sources:  kurtosis %g,   skewness %g \n', i, kurtosis(Y(i,:)'), skewness(Y(i,:)'));
    end
    fprintf('================== All sources have been generated ! ==================\n');
    
end

