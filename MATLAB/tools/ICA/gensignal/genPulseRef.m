function ref = genPulseRef(length, period, firstPulsePos)
% Generate pulse signal with unity amplitude, which is called the reference signal by
% the literature [1]. The output ref is a row vector. The input period is not
% necessarily the interger.
%
% Command:
%    ref = genPulseRef(length, period, firstPulsePos)��
%
% Parameters:
%             ref --- the generated reference signal
%          length --- length of ref
%          period --- the period of pulse, which is not necessarily the interger
%   firstPulsePos --- the first non-zero sample index
%
% See also:
%   genRectangleRef
%
% Reference:
%    [1] Wei Lu, Jagath C. Rajapakse: ICA with reference. ICA 2001
%
% Author��Zhi-Lin Zhang
%         zlzhang@uestc.edu.cn
%         http://teacher.uestc.edu.cn/teacher/teacher.jsp?TID=zzl80320
%
% version: 1.0     Date:  Dec.11, 2005


sig = zeros(1,length);
numPeriod = round(length/period)+2;
sig( round([1:numPeriod] * period) ) = 1;
sig( 1:round(period)-1 ) = [];

firstPulsePos = round(firstPulsePos);
ref(1: firstPulsePos-1) = zeros(1, firstPulsePos-1);
ref = [ref,sig];
ref(length+1 : end) = [];


% ref = zeros(1,length);
% ref(firstPulsePos) = 1;
% 
% for k = 1 :  length - firstPulsePos 
%     if mod(k,period) == 0
%         ref( firstPulsePos + k ) = 1;
%     else
%         ref(firstPulsePos + k) = 0;
%     end
% end

%=========


    