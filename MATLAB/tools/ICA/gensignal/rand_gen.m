    %To generate random numbers given density function as name of function 
    % which returns the values. The required density should have no 0 region 
    %within MIN and MAX 
function [X]=rand_gen(funname,P,M,MIN,MAX,f_max)
%function [X]=rand_gen(funname,P,M,MIN,MAX,f_max)
%
%X		->   Generated randomsequence with the given density function.
%funname	->   The name of the functions returning the density function.
%P and M	->   Returns P by M matrix.
%MIN            ->   Minimum value of X.
%MAX            ->   Maximum value of X.
%f_max 		->   Maximum value the required density function can take

FLAG=0;
X=[];
a=1/((MAX-MIN)*f_max);
N=P*M;
while FLAG==0
  x=unifrnd(MIN,MAX,1,N);
  u=unifrnd(0,1,1,N);
  f=eval([funname '(x)']);
  X=[X x(u<=(f*a*(MAX-MIN)))];
  FLAG=length(X)>N;
end
X=(X(1:N));
X=reshape(X,P,M);
