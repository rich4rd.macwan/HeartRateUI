function s = ARsig(N, rootVector)
% ARSIG--generate AR signal of length N, which is drived by Gaussian noise, given the roots vector rootVector.
% 
% Format:
%       s = ARsig(N, rootVector)
%
% Parameters:
%           s --- generated AR signal, a row vector of length N
%           N --- length of AR signal
%  rootVector --- a vector whose elements are the roots of the AR time series. Note
%                 that each element should lies in (-1,1) in order to be stable, and
%                 that the roots should be conjugate. 
%
% Example:
%       N = 1000;                        % length 
%       rootVector = [0.5  0.7  0.14];   % each element is the root of the AR signal
%       s = ARsig(N,rootVector);         % generate an AR signal with 1000 points
%                 
% Author��Zhi-Lin Zhang
%         zlzhang@uestc.edu.cn
%         http://teacher.uestc.edu.cn/teacher/teacher.jsp?TID=zzl80320
%
% version: 1.0     Date:  Jan.7, 2006




b = 1;   % numerator
ARcoef = real(poly(rootVector));   % a vector whose elements are the coefficients 
                                   % of the polynomial whose roots are the elements of
                                   % rootVector. Its first element is 1.
x = randn(1,N);   % Gaussian white noise
s = filter(b,ARcoef,x);

