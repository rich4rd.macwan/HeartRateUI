%  ICAtoolbox 3.8, Independent Component Analysis/Blind Source Separation Toolbox. 
%
%  Comments:
%  This toolbox aims to carry out simulations in independent component
%  analysis and blind source separation field. It includes the famous
%  algorithms in this field, especially the ones based on linear
%  instantaneous mixing model. 
%
%  Platform:
%    developed on Matlab 6.5
%
%  Version:  3.8       
%
%  History information:
%    Version 3.7      Date: Sep.27, 2006
%    Version 3.6.4    Date: Jul.24, 2006
%    Version 3.5      Date: Mar.15, 2006
%    Version 3.4      Date: Feb.11, 2006
%    Version 3.3      Date: Dec. 6, 2005
%    Version 3.2      Date: Oct.10, 2005
%    Version 3.1      Date: Sep.27, 2005
%    Version 3.0      Date: Jun.17, 2005
%    Version 2.0      Date: Mar.20, 2005
%    Version 1.0      Date: Nov.11, 2003
%
%  ======================================================================
%   Author   : Zhilin Zhang
%   E-mail   : z4zhang@ucsd.edu
%              zlzhangacademy@gmail.com
%   Phone    : +1-858-405-5024
%   Address  : Digital Signal Processing,
%              Department of Electric & Computer Engineering,
%              University of California, San Diego, USA
%   Homepage : http://dsp.ucsd.edu/~zhilin/
%              
%==========================================================================
% 
%
%==========================================================================
%                          Menu of ICAtoolbox 3.8  
%==========================================================================
%
%---------------- The files in Folder 'instAlg' -------------------
%            ACY.m  ------ Amari, Cichocki and Yang 's algorithm, published in NIPS'96
%       acffpICA.m  ------ Fast fixed-point ICA taking into acount both nongaussinity and autocorrelation
%        FastICA.m  ------ Fast Fixed-Point ICA based on negentropy
%         ffpICA.m  ------ Fast Fixed-Point ICA based on kurtosis
%           geig.m  ------ BSS based on generalized eigenvalue decomposition
%           EASI.m  ------ EASI algorithm
%        VS_EASI.m  ------ Varying Step-size EASI algorithm
%       NLPCARLS.m  ------ Nonlinear recursive least-squares algorithm
%     ngNLPCARLS.m  ------ natural gradient based nonlinear PCA algorithm
%          ffpML.m  ------ fast ICA based on maximum likelihood
%       flexiICA.m  ------ flexible ICA algorithm
%          HJbss.m  ------ Herault-Jutten algorithm, 2.0 version
%         ExtICA.m  ------ Extended Infomax Algorithm
%        Infomax.m  ------ Infomax algorithm 
%            PCA.m  ------ Principle Component Analysis Algorithm
%          AMUSE.m  ------ AMUSE algorithm
%           rNGA.m  ------ robust natural gradient based algorithm, based on the t-distribution and GGD
%     pearsonICA.m  ------ pearson ICA, implemented in maximum likelihood framework
%       StoneBSS.m  ------ BSS based on Stone's predictability theory
% -------------------------------------------------------------------------- 
%
%
%
%----------------------- The files in Folder 'tools' -----------------------
%      centering.m  ------ make the input signals' mean value zero
%         remstd.m  ------ make the input signals' variance unit
%         whiten.m  ------ return the whitened signals and the whitening matrix
%        ICAshow.m  ------ plot signals
%         selcol.m  ------ select columns 
%           eqim.m  ------ compress data values in [0,1]
%         permut.m  ------ permutate matrix
%            tri.m  ------ copy the up-triangle elements to the bottom-triangle locations
%             OK.m  ------ set of commands, i.e.,"close all","clear","clc"
%         showAC.m  ------ plot autocorrelation function of each signal
%    insertArray.m  ------ insert an array into another array
%   computPeriod.m  ------ compute periods hidden in signals
%   computPeriodDemo ----- demo file for computPeriod
% -------------------------------------------------------------------------- 
%
%
%----------------------- The files in Folder 'demos' -----------------------
%           demo01.m  ------ demo1 for the use of fastICA
%           demo02.m  ------ demo2 for the use of fastICA
%           demo03.m  ------ demo3 for the use of fastICA
%      demo03_post.m  ------ post-processing of demo03.m
%          ACYdemo.m  ------ demo file for ACY algorithm
%     acffpICAdemo.m  ------ demo file for acffpICA
%        ffpMLdemo.m  ------ demo for ffpML
%     flexiICAdemo.m  ------ demo file for flexible ICA algorithm
%        HJbssdemo.m  ------ demo file for Herault-Jutten algorithm
%         geigdemo.m  ------ demo file for geig 
%         EASIdemo.m  ------ demo file for EASI.m
%      VS_EASIdemo.m  ------ demo file for VS-EASI algorithm
%     NLPCARLSdemo.m  ------ demo file for NonPCARLS.m
%       ExtICAdemo.m  ------ demo file for ExtICA.m
%      InfomaxDemo.m  ------ demo file for Infomax algorithm
%      MixMatrShow.m  ------ demo for separating sources with uniform distribution
%          testPCA.m  ------ demo for PCA
%        easi_demo.m  ------ demo file for EASI, written by Cadoso
%         jadedemo.m  ------ demo file for JADE for complex signals
%        jadeRdemo.m  ------ demo file for JADE for real signals
%    MultiDimICA_1.m  ------ experiments on FECG, based on the idea of
%                            multidimension ICA by Cardoso
%    MultiDimICA_2.m  ------ Similar to MultiDimICA_1.m
%      OverdetDemo.m  ------ Overdetermine ICA case
%         rNGAdemo.m  ------ demo file for rNGAdemo
%   pearsonICAdemo.m  ------ demo file for pearsonICA
%          IRAdemo.m  ------ demo file for IRA
%     StoneBSSdemo.m  ------ demo file for StoneBSS
% -------------------------------------------------------------------------- 
%
%
%--------------------- The files in Folder 'gensignal' ---------------------
%            ARsig.m  ------ generate an AR signal
%           gensig.m  ------ generate 4 typical signals
%          sigpool.m  ------ generate 8 sub-gaussian sources and 2 super-gaussian sourcess
%          subsig5.m  ------ generate 5 sub-gaussian sources
%      genPulseRef.m  ------ generate pulse reference signal
%  genRectangleRef.m  ------ generate rectangle reference signal
% -------------------------------------------------------------------------- 
%
%
%------------------- The files in Folder 'performance' ---------------------
%           CTPI.m  ------ Cross-talk error, used in measuring the accuracy
%                          of separation
%           exPI.m  ------ performance metric of blind signal extraction algorithm
%            ICI.m  ------ Inter-Channel Interference
%      deviation.m  ------ Deviation of the separating matrix away from
%                          orthogonality
%         absDev.m  ------ measuring the deviation between source 1 and source 2
% -------------------------------------------------------------------------- 
%
%
%
