function E = ICI(globalMatrix)
% ICI --- Inter-Channel Interference(ICI), performance measure of BSS/ICA
%         algorithms, which is the ratio of the interfering signal power to
%         the power of the desired signal. It assumes that all source
%         signals have equal power.
% 
% Command:
%         E = ICI( globalMatrix );
%             where globalMatrix is the global matrix,i.e.,globalMatrix = W*A, 
%             where W is demixing matrix and A is mixing matrix. The computing 
%             formula is as following:
%                         1    N      N     |P(i,j)|^2                      
%               ICI(P) = ---  sum (  sum -----------------  - 1  ) 
%                         N   i=1    j=1  max(|P(i,j)|^2)    
%       
% Author  : Zhi-Lin Zhang ( zlzhang@uestc.edu.cn )
% Version : 1.0
% Date    : Mar.31, 2005 



[rows,cols] = size(globalMatrix);
if rows ~= cols
    fprintf('Number of columns are NOT equal to that of rows!\nExit...\n');
    return;
end

dist = globalMatrix .^2;

E = 0;
for i = 1 : rows
    maxEle = max(dist(i,:));
    E = E + sum( dist(i,:) )/maxEle;
end
E = E /rows - 1;


