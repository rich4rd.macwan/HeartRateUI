function E = exPI( v1, v2, type)
% exPI: return the performance metric of blind signal extraction algorithm.
% Command:
%    [1] w = [ 1, 4 ,9]; A = [1 3 5; 2 5 8; 3 1 0];
%        E = exPI(w,A,'glo');  or
%        E = exPI(w,A);
%    [2] s = [ 1  3  5]; e_s = [1.1  2.9   4.9];
%        E = exPI(s, e_s, 'dev');
%
% Parameters:
%    if type = 'glo'(default), then 
%         v1 -- weight column vector 
%         v2 -- mixing matrix or the multiplay of whitening matrix and mixing matrix
%          E -- performance metric, computing as follows,
%                       1      N    (e_i)^2
%                 E = ----- ( sum ------------- - 1 ) ,  where e = w'* A.
%                      N-1    i=1  max (e_i)^2
%            The performance metric has the following features:
%            (1) lies in [0,1] for all vector e
%            (2) E = 1 if and only if |e_i|^2 = |e_j|^2 for all i,j in
%                the range [1,N] (i.e.,maximally mixed sources in the system
%                outputs)
%            (3) E = 0 if and only if e has one non-zero element 
%                (i.e.,extracted a source signal in the system output)
%
%    if type = 'dev', then the code returns the devivation of v1 and v2, where
%    v1 and v2 are two signals. The result is given in terms of dB:
%                P = - 10 * lg( E{ [v1(k)-v2(k)]^2 } )
%    The larger the result is, the more similar the two signals are.
%
% Author  : Zhi-Lin Zhang ( zlzhang@uestc.edu.cn )
% Version : 3.0      Update  : Jan.14, 2007
% Version : 2.0      Date    : Aug. 1, 2005
% Version : 1.0      Date    : Jun.24, 2005 



if nargin == 2
    op = 1; 
else
    if strcmp('glo',lower(type))
        op = 1;
    elseif strcmp('dev',lower(type))
        op = 2;  
    else
        fprintf('Some of the input parameters are wrong ! \n');
        return;
    end
end

if op == 1
    w = v1; A = v2;
    if ( size(w,1) ~= size(A,1) ) | ( size(A,2) < 2 ) | (size(A,1) < 2 )
        fprintf('The size of input parameters are wrong !\n'); return;
    end
    
    e = (w'* A).^2;
    N = length(e);
    E = ( sum(e)/max(e) - 1  ) / (N-1);
    
else
    est = v1 ;
    sour = v2 ;
    if ( size(est,1) ~= size(sour,1) ) & ( size(est,2) ~= size(sour,2) )
        fprintf('The size of input parameters are wrong !\n'); return;
    end
    
    % standarize, making each signal zero mean and unit variance
    est = standarize(est); 
    sour = standarize(sour); 

    p1 = - 10 * log10(  mean((est-sour).^2)  ); 
    p2 = - 10 * log10(  mean((est+sour).^2)  ); 
    E = max(p1,p2);
    
    % The following old measurement should be avoided, since its result towards the
    % method that has one extraction that is sufficiently close to the expected
    % solution.That is say, suppopse at time k, est(k)=sour(k), then the log10(
    % (est(k)-sour(k))^2 ) is negatively infinit, and the PI is positively infinit.
    %     p1 = - 10 * log10(  (est-sour).^2  );   
    %     p2 = - 10 * log10(  (est+sour).^2  );
    %     E = max( mean(p1), mean(p2));
end
