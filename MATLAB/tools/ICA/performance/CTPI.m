function E = CTPI(P,varargin)
% CTPI -- Returns the cross-talk error, which is used in blind source
%         separation to measure the accuracy of separation. The larger it
%         is, the worse performance of separation.
%
% Command:
%          E = CTPI(P,'sum');    
%          E = CTPI(P,'ave');              
%          E = CTPI(P,'norm'); 
%          E = CTPI(P,'single');
%
% Parameter:
% < Input >
%            P --- P=WA,where W is demixed matrix and A is mixing matrix
%                  and whose P(i,j) is the ijth element of the N*N matrix P
%        'sum' --- compute the performance index as follows:
%                          N      N     |P(i,j)|               N      N     |P(j,i)|         
%              CTPI(P) =  sum (  sum --------------- - 1  ) + sum (  sum --------------- - 1  )
%                         i=1    j=1  max(|P(i,j)|)           j=1    i=1  max(|P(j,i)|)
%
%        'ave' --- compute the performance index as follows:
%                         1   N      N     |P(i,j)|              1   N      N     |P(j,i)|         
%              CTPI(P) = --- sum (  sum --------------- - 1 ) + --- sum (  sum --------------- - 1 )
%                         N   i=1   j=1  max(|P(i,j)|)           N  j=1    i=1  max(|P(j,i)|)
%
%     'single' --- compute the performance index as follows:
%                           1   N      N     |P(i,j)|
%              CTPI(P) =   --- sum (  sum --------------- - 1  )
%                           N  i=1    j=1  max(|P(i,j)|)
%
%       'norm' --- compute the performance index as follows:
%                                               max(|P(i,j)|^2)    max(|P(j,i)|^2) 
%                            1        1   m    1<=j<=m            1<=j<=m
%              CTPI(P) =   ----- (m- --- sum ( ---------------- + -----------------) )
%                           m-1       2  i=1     m                 m                     
%                                               sum(|P(i,j)|^2)   sum(|P(j,i)|^2)
%                                               j=1               j=1
%                  the performance metric has the following features:
%                  (1) lies in [0,1] for all matrices P
%                  (2) E = 1 if and only if |Pij|^2 = |Ppq|^2 for all i,j,p,q in
%                      the range [1,m] (i.e.,maximally mixed sources in the system
%                      outputs)
%                  (3) E = 0 if and only if P = \Phi * D (i.e., separated sources
%                      in the system outputs)
%
% Author  : Zhi-Lin Zhang ( zlzhang@uestc.edu.cn )
% Version : 1.0
% Date    : June 13, 2005 



[N,J] = size(P);
if N ~= J
    fprintf('Number of columns are NOT equal to that of rows!\nExit...\n');
    return;
end

if strcmp(lower(varargin{1}),'norm')  
    % compute the performance:
    %                                     max(|P(i,j)|^2)    max(|P(j,i)|^2) 
    %                  1        1   N    1<=j<=N            1<=j<=N
    %    CTPI(P) =   ----- (N- --- sum ( ---------------- + -----------------) )
    %                 N-1       2  i=1     N                 N                     
    %                                     sum(|P(i,j)|^2)   sum(|P(j,i)|^2)
    %                                     j=1               j=1
    P = P.^2;
    E =  (N-sum( max(P)./sum(P) + max(P')./sum(P') )/2 ) / (N-1)  ;
    
elseif strcmp(lower(varargin{1}),'sum')  
    %    'sum' --- compute the performance index as follows:
    %                      N      N     |P(i,j)|               N      N     |P(j,i)|         
    %          CTPI(P) =  sum (  sum --------------- - 1  ) + sum (  sum --------------- - 1  )
    %                     i=1    j=1  max(|P(i,j)|)           j=1    i=1  max(|P(j,i)|)   
    P = abs(P);
    sum1 = 0;
    for i = 1:N
        maxEle = max(P(i,:));
        sum1 = sum1 + sum(P(i,:))/maxEle;
    end
    sum1 = sum1 - N;
    
    sum2 = 0;
    for j = 1:N
        maxEle = max(P(:,j));
        sum2 = sum2 + sum(P(:,j))/maxEle;
    end
    sum2 = sum2 - N;
    
    E = sum1 + sum2;
    
elseif strcmp(lower(varargin{1}),'ave')  
%    'ave' --- compute the performance index as follows:
%                 1   N      N     |P(i,j)|              1   N      N     |P(j,i)|         
%      CTPI(P) = --- sum (  sum --------------- - 1 ) + --- sum (  sum --------------- - 1 )
%                 N   i=1   j=1  max(|P(i,j)|)           N  j=1    i=1  max(|P(j,i)|)
%
    P = abs(P);
    sum1 = 0;
    for i = 1:N
        maxEle = max(P(i,:));
        sum1 = sum1 + sum(P(i,:))/maxEle;
    end
    sum1 = sum1 - N;
    
    sum2 = 0;
    for j = 1:N
        maxEle = max(P(:,j));
        sum2 = sum2 + sum(P(:,j))/maxEle;
    end
    sum2 = sum2 - N;
    
    E = (sum1 + sum2)/N;
    
elseif strcmp(lower(varargin{1}),'single')  
%     'single' --- compute the performance index as follows:
%                           1   N      N     |P(i,j)|
%              CTPI(P) =   --- sum (  sum --------------- - 1  )
%                           N  i=1    j=1  max(|P(i,j)|)
    P = abs(P);
    sum1 = 0;
    for i = 1:N
        maxEle = max(P(i,:));
        sum1 = sum1 + sum(P(i,:))/maxEle;
    end
    E = (sum1 - N)/N;
    
else
    fprintf('You have input wrong arguments! \n');
    return;
end




