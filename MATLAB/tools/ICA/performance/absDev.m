function P = absDev( est, sour )
% absDev -- measuring the deviation between source 1 and source 2.
%           It computes as follows,
%           P = 10 * E{ lg[source1(k)-source2(k)]^2 }
%
% Command
%        P = absDev( est, sour)
%
% Parameters
%       est --- source one
%      sour --- source two
%
% Function called     
%           standarize
%
% Author :  Zhi-Lin Zhang
% E-mail :  zlzhang@uestc.edu.cn
% Date   :  June 4,2005
% Version:  1.0
%
% 

est = standarize(est);
sour = standarize(sour);


p1 = 10 * log10(  (est-sour).^2  );
p2 = 10 * log10(  (est+sour).^2  );

P = min( mean(p1), mean(p2));