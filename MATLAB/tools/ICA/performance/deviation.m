function D = deviation( W )
% DEVIATION --- Deviation of the separating matrix away from
%               orthogonality,i.e., D = || WW' - diag(WW')||(Frobenius
%               norm)
%
% Parameter:
%       W --- separating matrix
%       D --- the result
%
% References:
%  [1] X-L Zhu, X-D Zhang, Adaptive RLS algorithm for blind source
%      separation using a natural gradient. IEEE Signal Processing Letters
%      vol.9, No.12, 2002
%
% Author: Zhang Zhi-Lin (zhangzl@vip.163.com)
% Version: 1.0
% Date: Dec.24, 2004 


Q = W*W' - diag(diag(W*W'));
D = norm(Q,'fro');