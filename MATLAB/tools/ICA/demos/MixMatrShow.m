% 独立同均匀分布的二维信号经线性混合前后对比
% 观察混合后信号的联合分布与混合矩阵的关系
% 原始信号为两个独立的同均匀分布的随机信号，作出二维图形显示为一个正方形
% 经过一个混合矩阵的线性变换后，其联合分布发生了变化，作出二维图形显示为一个菱形
% 菱形的两条斜边所指的方向，分别为混合矩阵的两个行所指的方向

clear;clc;
N=5000;
x1=2*(rand(1,N)-0.5);   % 源信号，在【－1，1】之间均匀分布
x2=2*(rand(1,N)-0.5);

x1=remstd(x1);
x2=remstd(x2);

figure;
plot(x1,x2','.');
axis([-4,4,-4,4]);
title('\bf独立同均匀分布的二维随机信号');
xlabel('\bfs1');ylabel('\bfs2');
axis square;
grid on;

W=[5, 5
   5, 1 ];               % 混合矩阵
fprintf('混合矩阵为：\n');disp(W);
fprintf('从图可以看到：\n    菱形的两条斜边所指的方向，分别为混合矩阵的两个行所指的方向');
y=W*[x1;x2];             % 混合后的信号
y=removmean(y);
y=remstd(y);



figure;
plot(y(1,:),y(2,:)','.');
axis([-4,4,-4,4]);
title('\bf混合后的二维随机信号');
xlabel('\bfx1');ylabel('\bfx2');
axis square;
grid on;


