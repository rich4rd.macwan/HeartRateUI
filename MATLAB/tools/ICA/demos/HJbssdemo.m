% HJbssdemo. Illustrate Herault-Jutten algorithm for blingdly separating sources
% Reference:
%   [1]Sorouchyari E.: Blind separation of sources, part 3: Stability 
%      analysis. Signal Processing 24(1991)21-29.


clear;clc;close all;

N = 5000;  % sampling points

% generate 2 source signals with uniform distribution, zero mean, unit
% variance
S = (rand(2,N)-0.5*ones(2,N))*2; 
% generate 2 source signals with super-Gaussian distribution, zeros mean,
% unit variance.
% S = sigpool(2,N,'sup');
S = removmean(S);   % zero mean
S = remstd(S);      % unit variance
figure;
subplot(2,1,2); plot(S(2,[N-499 : N])); ylabel('\bfCurve wave');axis([-inf,inf,-5,5]);
subplot(2,1,1); plot(S(1,[N-499 : N])); ylabel('\bfLaplasian signal');axis([-inf,inf,-5,5]);
title('\bf\fontsize{12}Source Signals');

% --------mixed signals-----------
% random mixing matrix
A = [1  0.6; 0.3  1];
S = A * S;             % mixed signals

% preprocessing 
% S = removmean(S);
% S = remstd(S);
% S = whiten(S);
ICAshow(S(:,[N-499 : N]),'title','\bf\fontsize{12}Mixed Signals');


% ----------run H-J algorithm (HJbss.m)------------
mu = 0.001;
% c12 = 3.33 ;  c21 = 1.66;   % unstable point
% c12 = -0.57 ;  c21 = 0.81;  % initial value for stable point
% c12 = 1.23 ;  c21 = -1.75;  % initial value for stable point, but converge very slowly
% c12 = 0.6 ;  c21 = 0.3;   % initial value for stable point
c12 = 0; c21 = 0;
C0 = [0, c12; c21, 0];
[Y, F, C] = HJbss(S, mu, C0,'track');        % its only stable point is c12=0.6;c21=0.3
% [Y2,F2,C2]=HJbss(S,mu);

% postprocessing
Y = removmean(Y);
Y = remstd(Y);

figure;
subplot(2,1,2); plot(Y(2,[N-499 : N])); ylabel('\bfy2');axis([-inf,inf,-5,5]);
subplot(2,1,1); plot(Y(1,[N-499 : N])); ylabel('\bfy1');axis([-inf,inf,-5,5]);
title('\bf\fontsize{12}Reconstructed Signals');


for k = 1 : N
    C12(k) = C(1,2,k);
    C21(k) = C(2,1,k);
end
figure;
subplot(2,1,1); plot(C12); legend('C12');
subplot(2,1,2); plot(C21,'r'); legend('C21');
figure;
plot(C12,C21); title('\bf Evolution of the System''s State');
xlabel('\bf C12');ylabel('\bf C21');
hold on;
plot(0.6,0.3,'r*');   % the only stable point c12=0.6;c21=0.3
hold on;
plot(c12,c21,'ro');   % the initial value of c12 and c21

