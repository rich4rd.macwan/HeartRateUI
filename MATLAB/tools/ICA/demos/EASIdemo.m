% Demo file for EASI algorithm
% Author : Zhi-Lin Zhang
% Version: 1.1
% Date   : Feb.27,2005

clear;close all;
%-------------------------------------------
N=4000;
numOfIC = 5;


% sig = subsig5(N);   % five sub-Gaussian source signals
sig = ((rand(numOfIC,N)<.5)*2-1).* log(rand(numOfIC,N));     % super-Gaussian
% sig = sigpool(2,N,'sup');     % two super-Gaussian source signals
%-------------------------------------------
A=rand(numOfIC);           % 产生随机的混合矩阵
% while cond(A)<500000        % 产生病态(ill-condition)的混合矩阵
%     A=rand(numOfIC);
% end
fprintf('cond(A)= %g \n',cond(A));
MixedSig=A*sig;                % 得到混合信号

% 画出混合信号
ICAshow(MixedSig(:,[N-499:N]),'title','Mixed Sources(zero mean,unit variance)');

% compute using EASI algorithm
fprintf('\n');
fprintf('==============================================\n');
fprintf('             Starting  Experiments            \n');
fprintf('==============================================\n');
mu = 0.01;
B = eye(numOfIC);
tic;
[Y,W,E] = EASI(MixedSig, B, A, mu, 'sup');
elapse_time =toc;

%-------------------------------------------
% 恢复信号为0均值，单位方差
Sig = standarize(Y);
fprintf('\n');
fprintf('==============================================\n');
fprintf('            Results of Experiments            \n');
fprintf('==============================================\n');
% show results
fprintf('    Elapse Time      : %g \n',elapse_time);
fprintf('    Perpormance Index: %g \n', CTPI(W*A,'ave') );
fprintf('    Global Matrix    : \n '); disp(W*A);

figure; plot(E); title('\bf Performance Index');

figure;  % 画出恢复信号
for i = numOfIC: -1: 1
    subplot(numOfIC,1,i)
    plot(Sig(i,[N-499:N]));axis([-inf,inf,-5,5]);
end
title('\bfReconstructed Sources(zero mean,unit variance)');

