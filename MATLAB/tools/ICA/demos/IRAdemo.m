% demo file for IRA algorithm
% Zhilin Zhang

clear;clc;close all;
order = 5;          % AR order
T = 5500;           % Data length
ICnum = 4;          % number of sources

% generate sources, which are iid
for i = 1 : ICnum
%     rootVector = rand(1,order)-0.05;
%     ARcoef = real(poly(rootVector));   
    if mod(i,2)==0
        noi(i,:) = ((rand(1,T)<.5)*2-1).*log(rand(1,T));  
    else
        noi(i,:) = randn(1,T);
    end
%     S(i,:) = filter(1,ARcoef,noi);
end
noi = standarize(noi);

rootVector(1,:) = [0.2  0.1   0.3   0.1   -0.1];
rootVector(2,:) = [-0.1  0.1   0.7   -0.4  0.3 ];
rootVector(3,:) = [-0.5  -0.2i   0.2i   0.2   0.1 ];
rootVector(4,:) = [-0.7  -0.1  0.2  0.4    0.6];

% make the original sources have temporal structure
for i = 1 : ICnum
    ARcoef(i,:) = real(poly(rootVector(i,:)));
    S(i,:) = filter(1,ARcoef(i,:),noi(i,:));
end

S = S(:,501:T);         % source length becomes 5000
T = 5000;
S = standarize(S);


% 
% N = 5000;
% S = gensig(N,'off');
% S = standarize(S);
icashow(S(:,1:200),'title','\bfsource signals');

% randomly generate mixing matrix
A = rand(4);

% mixed signals
X = A * S;

% whitening
[Z,V] = whiten(X);                                    

% use IRA algorithm to separate sources with adaptive activation function
learningRate = 0.08;
ARdegree = 5; 
nonlinType = 1;
[Y,W,Az] = IRA(Z,learningRate,ARdegree,nonlinType);
Y = standarize(Y);
icashow(Y(:,1:200),'title','\bfseparated signals by IRA with adaptive activation function');

% IRA with tanh and cubic activation function
nonlinType = 2;
[Y2,W2,Az2] = IRA(Z,learningRate,ARdegree,nonlinType);
Y2 = standarize(Y2);
icashow(Y2(:,1:200),'title','\bfseparated signals by IRA with tanh and cubic activation functions');

% use FastICA to separate sources
[Y3,B] = FastICA(Z);
icashow(Y3(:,1:200),'title','\bfseparated signals by FastICA');

% use Extended Infomax to separate sources (offline type)
mu=0.008;  
fgt=0.98;
B=0.5*eye(4);
[Y4,W4,E] = ExtICA(Z, B, V*A, mu, fgt,'offline',5000);
icashow(Y4(:,1:200),'title','\bfseparated signals by Extended Infomax (offline version)');

% output the performance comparison
PI1 = CTPI(W*V*A,'norm');
PI2 = CTPI(W2*V*A,'norm');
PI3 = CTPI(B'*V*A,'norm');
PI4 = CTPI(W4*V*A,'norm');
fprintf('\n=====================================================================================================\n');
fprintf('Output the performance of each algorithm. The less the performance index, the better the algorithm\n');
fprintf('=====================================================================================================\n');
fprintf('       IRA with adaptive activation function:  PI1 = %g \n',PI1);
fprintf('IRA with tanh and cubic activation functions:  PI2 = %g \n',PI2);
fprintf('                                     FastICA:  PI3 = %g \n',PI3);
fprintf('                            Extended Infomax:  PI4 = %g \n',PI4);

