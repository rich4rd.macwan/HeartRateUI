% rNGAdemo -- demo for the rNGA algorithm
%                Simplily run this file, and you could find the picure
%                drawing original source signals, the picture drawing mixed
%                signals, the picture drawing separated source signals, and
%                the picture drawing the evolution of performance index.
% Author  : Zhi-Lin Zhang




close all;clear;clc;
%==================  产生原始信号 ===================================
N=8000;
% sig=gensig(N,'off');
sig = sigpool(4,N,'sub','off');
sig([5:6],:) = sigpool(2,N,'sup','off');
load longFECGdata.mat;
sig(7,:) = X(7,[1:N]);
sig(8,:) = X(8,[150:N+149]);
% sig = (rand(4,N)-0.5)*2;


% %===================  画出信号  =====================================
% icashow(sig(:,[N-499:N]),'title','Source Signals');


for k = 1:1       
    %================== 把原始信号进行随机混合 ================================
    A=rand(size(sig,1));           % 产生随机的混合矩阵
    MixedSig=A*sig;                % 得到混合信号
    [MixedSig,V] = whiten(MixedSig);  % whitening
    
    
    %========================== 设置算法参数，运行算法 ====================
    B=eye(size(MixedSig,1));
    
    tic; 
%     mu=0.01; fgt = 0.5;  [Y1,B1,E1] = flexiICA(MixedSig, B, V*A, mu, fgt,'offline',500);
    fgt=0.97; mu=0.002; [Y1,B1,E1] = flexiICA(MixedSig, B, V*A, mu, fgt);
    elapse_time1=toc;
    
    tic;
%     mu=0.01; fgt = 0.5; [Y2,B2,E2] = rNGA(MixedSig, B, V*A, mu, fgt,'offline',500);
    fgt=0.97; mu=0.002; [Y2,B2,E2] = rNGA(MixedSig, B, V*A, mu, fgt);
    elapse_time2=toc;
    
    %========================== 显示flexible ICA的运算结果参数 ====================
    fprintf('==============================================\n');
    fprintf('        No.%d    Results of flexible ICA      \n',k);
    fprintf('==============================================\n');
    % show results
    fprintf('    Time:%g \n',elapse_time1);
    fprintf('    Matrix: \n ');disp(B1*V*A);
    fprintf('    Prepormance index: %g \n', CTPI(B1*V*A,'norm') );
    %画图显示
    icashow(Y1(:,[N-499:N]),'title','Separated Source Signals using flexible ICA');
    
    
    
    %========================== 显示rNGA的运算结果参数 ====================
    fprintf('==============================================\n');
    fprintf('        No.%d        Results of rNGA          \n',k);
    fprintf('==============================================\n');
    % show results
    fprintf('    Time:%g \n',elapse_time2);
    fprintf('    Matrix: \n ');disp(B2*V*A);
    fprintf('    Prepormance index: %g \n', CTPI(B2*V*A,'norm') );
    %画图显示
    icashow(Y2(:,[N-499:N]),'title','Separated Source Signals using rNGA');
    
    % 比较性能指标
    figure;
    plot(E1);
    hold on;
    plot(E2,'r');
    legend('flexible ICA','rNGA');
    title('Comparison of Performance Index');
    
    P1(k,:)=E1;
    P2(k,:)=E2;
    
end

% figure;
% plot(mean(P1));  hold on;
% plot(mean(P2),'r');
% legend('flexible ICA','rNGA');
% title('Comparison of Averaged Performance Index over 50 Trails');
% xlabel('Sample Number');
% ylabel('Avaraged Performance Index');