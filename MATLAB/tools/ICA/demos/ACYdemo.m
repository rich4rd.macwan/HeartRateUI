%demo for ACY algorithm
clear;clc;
N=9000;
for iter = 1:1
    close all;
    fprintf('\n===================== No.%d Experiment =====================\n',iter);
    sig = sigpool(2,N,'sup');
    
    % randomly mixing
    A = rand(size(sig,1));
    Mixed = A * sig;
    Mixed = removmean(Mixed);          % 使均值为0
    icashow(Mixed(:,[N-499:N]),'title','Mixed Sources');
      
    % initialization for parameters of the algorithm
    B = eye(size(A));   % initial value of demixing matrix
    mu = 0.001;                         % initial value of learning rate
    batchsize = 20;
    
    % run algorithm
    fprintf('Running the algorithm ...\n');
    tic;
    [Y,W,E] = ACY(Mixed, B, A, mu, 'sup');
    elapse_time = toc;
    
    % post-processing
    Y = removmean(Y);          % 使均值为0
    Y = remstd(Y);             % 使方差为1
    
    % show results
    fprintf('Running is over. The result is:\n');
    fprintf('    Time:%g \n',elapse_time);
    fprintf('    Prepormance index: %g \n', CTPI(W*A) );
    fprintf('    Matrix: \n ');disp(W*A);
    icashow(Y(:,[N-499:N]),'title','Separation Sources');
    figure; plot(E); title('\bf Performance Index');  
    PI(iter,:) = E;  
    fprintf('=================== No.%d Experiment is OVER ===================\n',iter);
end