% Demo file for VS_EASI algorithm

clear;close all;
%-------------------------------------------
N=7000;
% gens=gensig(N,'off');
% sig = gens([2,4],:);
% sig=remstd(sig);             % 使方差为1
% sig=removmean(sig);          % 使均值为0
% 
% figure;  % 画出信号
% subplot(2,1,2); 
%     plot(sig(2,[N-499:N])); ylabel('s2');
%     axis([-inf,inf,-5,5]);
% subplot(2,1,1); 
%     plot(sig(1,[N-499:N])); ylabel('s1'); title('\bf Original Sources(zero mean,unit variance)');
%     axis([-inf,inf,-5,5]);
sig = subsig5(N);
for iteration =1: 1
    %-------------------------------------------
    A=rand(size(sig,1));           % 产生随机的混合矩阵
    
    MixedSig=A*sig;                % 得到混合信号
    
    MixedSig=removmean(MixedSig);          % 使均值为0
    MixedSig=remstd(MixedSig);             % 使方差为1
    
    % 画出混合信号
    ICAshow(MixedSig(:,[N-499:N]),'title','Mixed Sources(zero mean,unit variance)');
    
    % compute using EASI algorithm
    fprintf('\n');
    fprintf('==============================================\n');
    fprintf('          Starting  No %g Experiments         \n',iteration);
    fprintf('==============================================\n');
    mu = 0.004;
    rate = 0.0000000001;
    B = eye(size(sig,1));
    tic;
    [Y,W,E] = VS_EASI(MixedSig, B, A, mu, rate, 'sub');
    elapse_time =toc;
    
    %-------------------------------------------
    % 恢复信号为0均值，单位方差
    Sig=remstd(Y);             % 使方差为1
    Sig=removmean(Y);          % 使均值为0
    fprintf('\n');
    fprintf('==============================================\n');
    fprintf('            Results of Experiments            \n');
    fprintf('==============================================\n');
    % show results
    fprintf('    Prepormance Index: %g \n', SIRPI(W*A) );
    fprintf('    Elapse Time      : %g \n',elapse_time);
    fprintf('    Global Matrix    : \n '); disp(W*A);
    
    figure; plot(E); title('\bf Performance Index');
    
    figure;  % 画出恢复信号
    numOfIC = size(Sig,1);
    for i = numOfIC: -1: 1
        subplot(numOfIC,1,i)
        plot(Sig(i,[N-499:N]));axis([-inf,inf,-5,5]);
    end
    title('\bfReconstructed Sources(zero mean,unit variance)');
    
    PIvs(iteration,:) = E;
end
figure; plot(mean(PIvs));title('\bf Performance Index over 10 average');
