% flexiICAdemo -- demo for the On-line version of flexible ICA algorithm
%                Simplily run this file, and you could find the picure
%                drawing original source signals, the picture drawing mixed
%                signals, the picture drawing separated source signals, and
%                the picture drawing the evolution of performance index.
% Author  : Zhi-Lin Zhang
% Date    : Mar.8,2005
% Version : 1.0



close all;clear;clc;
%==================  产生原始信号 ===================================
N=5000;
sig=gensig(N,'off');
% sig = sigpool(8,N,'sub','off');
% sig = (rand(4,N)-0.5)*2;


%===================  画出信号  =====================================
icashow(sig(:,[N-499:N]),'title','Source Signals');
    
%================== 把原始信号进行随机混合 ================================
A=rand(size(sig,1));           % 产生随机的混合矩阵
MixedSig=A*sig;                % 得到混合信号

icashow(MixedSig(:,[N-499:N]),'title','Mixed Signals');
[MixedSig ,V ]= whiten2(MixedSig);   

%========================== 设置算法参数，运行算法 ====================
 
fgt=0.97;
B=eye(size(MixedSig,1));

tic; 
% mu=0.1; [Y,B,E] = flexiICA(MixedSig, B, V*A, mu, fgt,'offline',200);
mu=0.02; [Y,B,E] = flexiICA(MixedSig, B, V*A, mu, fgt);
elapse_time=toc;

%========================== 显示运算结果参数 ====================
fprintf('==============================================\n');
fprintf('            Results of calculation            \n');
fprintf('==============================================\n');
% show results
fprintf('    Time:%g \n',elapse_time);
fprintf('    Matrix: \n ');disp(B*V*A);
fprintf('    Preformance index: %g \n', CTPI(B*V*A,'norm') );
figure; plot(E); title('\bf Performance Index');

%画图显示
icashow(Y(:,[N-499:N]),'title','Separated Source Signals');
