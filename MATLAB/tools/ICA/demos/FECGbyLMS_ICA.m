% LMS和ExtICA算法提取FECG的比较
clear;clc;close all;
load FECGdata.mat;  % 2500 samples
icashow(X);
% 
w0 = randn(16,1);
for i =1 : 5
[W,E(i,:)]=lms2(X(8,:)', X(i,:)', 16, 0.0000001);
figure;subplot(2,1,1);plot(W');
[W,E(i,:)]=lms2(X(8,:)',X(i,:)',16,0.0000001,W(:,end));
subplot(2,1,2);plot(W');
end

figure;
for k = 1 : 5
    subplot(5,1,k); plot(E(k,:)); 
    E(k,:)=standarize(E(k,:));
    
end

%========== FECG extraction by Extended Infomax
% X = standarize(X);
% X=whiten(X);
% B = eye(8);A=B;mu=0.01;fgt=0.98;
% [Y,B,perfdex1] = ExtICA(X, B, A, mu, fgt);
%     figure;plot(perfdex1);
% [Y,B,perfdex2] = ExtICA(X, B, A, mu, fgt);
%     perfdex=[perfdex1,perfdex2];
%     figure;plot(perfdex);
% [Y,B,perfdex3] = ExtICA(X, B, A, mu, fgt);
%     perfdex=[perfdex,perfdex3];
%     figure;plot(perfdex);
% [Y,B,perfdex4] = ExtICA(X, B, A, mu, fgt);
%     perfdex=[perfdex,perfdex4];
%     figure;plot(perfdex);
% icashow(Y);

