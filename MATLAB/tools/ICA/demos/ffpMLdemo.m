% FFPMLdemo --- demo file for Fast Fixed-Point ICA algorithm for ML estimation
% 
% Files used list:
%         ffpML.m  ---- main function of FastICA algorithm for ML estimation      
%        gensig.m  ---- artificial four signals
%     removmean.m  ---- make value of mean zero
%        remstd.m  ---- make value of variance unit
%       ICAshow.m  ---- plot the signals which in matrix form 
%
% Author: Zhang Zhi-Lin
%         zhangzl@vip.163.com
% Version: 1.0
% Date: Nov.7,2003



X=gensig(500,'off');    % generate four artificial source signals
X=remstd(X);    
X=removmean(X);

ICAshow(X,'title','Original Sources(zero mean,unit varance)');

% g=0.1*hilb(4);
g=[1 0.2 0.3 0;0.1 0 0.1 0.2;0.3 0.1 0.4 0.1;0.9 0.5 0 0.1];
[Y,B] = ffpML(X,'distance',0.05,'initB',g);

ICAshow(Y,'title','Separated Sources(zero mean,unit varance)');
