% test for PCA.m
clear;clc;close all;

X(1,:) = rand(1,500);
X(2,:) = X(1,:);

X = removmean(X);
X = remstd(X);

ICAshow(X,'title','Before PCA');

% ���ø�ʽ��
%     [NewSig,E,D] = PCA(OldSig,'PCNum',4,'minIndex',1,'maxIndex',4,'report','on')
%     [NewSig,E,D] = PCA(OldSig,'PCNum',10,'report','off')
% many ways to try
% [X,E,D] = PCA(X);
[X,E,D] = PCA(X,'PCNum',2);

ICAshow(X,'title','After PCA');