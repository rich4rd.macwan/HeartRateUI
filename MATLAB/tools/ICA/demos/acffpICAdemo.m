% demo file for acffpICA

close all;clear;clc;

order = 5;          % AR模型的阶数
T = 5500;           % 信号长度
ICnum = 4;          % 源数目

% 产生AR源信号(新息为非高斯）
for i = 1 : ICnum
%     rootVector = rand(1,order)-0.05;
%     ARcoef = real(poly(rootVector));   
    if mod(i,2)==0
        noi(i,:) = ((rand(1,T)<.5)*2-1).*log(rand(1,T));  
    else
        noi(i,:) = rand(1,T);
    end
%     S(i,:) = filter(1,ARcoef,noi);
end
noi = standarize(noi);

% % 产生AR源信号（新息为高斯）
% noi = randn(ICnum,T);

rootVector(1,:) = [0.2   0.1     0.3   0.1   -0.1];
rootVector(2,:) = [-0.1  0.1     0.7   -0.4  0.3 ];
rootVector(3,:) = [-0.5  -0.2i   0.2i   0.2   0.1 ];
rootVector(4,:) = [-0.7  -0.1     0.2  0.4    0.6];

for i = 1 : ICnum
    ARcoef(i,:) = real(poly(rootVector(i,:)));
    S(i,:) = filter(1,ARcoef(i,:),noi(i,:));
end

S = S(:,501:T);
T = 5000;
S = standarize(S);
icashow(S,'title','\bfAR source signals');


A = rand(size(S,1));
X = A * S ;
[Z,V] = whiten(X);


ICs = 4;
orderVector = [1:4]*10;
mu = 1;
epsilon = 0.00001;
maxIter =200;
[y1,w1] = acffpICA( Z, ICs, orderVector, mu, epsilon, maxIter);
icashow(y1,'title','\bfextracted signals');

[y2,w2] = FastICA(Z);

for j=1:size(y1,1)
    for i=1:4
        Perf(i) = exPI(y1(j,:),S(i,:),'dev');
    end
    fprintf('PI of No.%d signal.\n',j);
    disp('performance index with each source signals');disp(Perf);
end

fprintf('The global PI of acffpICA is %g \n',CTPI(w1'* V * A,'norm'));

fprintf('The global PI of FastICA  is %g \n',CTPI(w2'*V*A,'norm'));






