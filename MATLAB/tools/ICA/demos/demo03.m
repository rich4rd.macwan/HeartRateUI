% demo03 --- using FastICA to separating 3 mixed images

clear;clc;close all;

% 获取三个大小为256×256的双精度(0-1)图象
%-------------------------- case 1 begin ---------------------------------------
% 第一幅图象：
I1 = imread('testpat1.tif'); 
I1 = im2double(I1); 

% 第二幅图象：
I2 = imread('blood1.tif');
I2 = imresize(I2,[256,256]);
I2 = im2double(I2); 

% 第三幅图象：
I3 = imread('ic.tif'); 
I3 = im2double(I3); 

figure;
subplot(2,3,1); subimage(I1); 
subplot(2,3,2); subimage(I2);
subplot(2,3,3); subimage(I3); 
subplot(2,3,4); imhist(I1);
subplot(2,3,5); imhist(I2);
subplot(2,3,6); imhist(I3);
%----------------------------- case 1 end ---------------------------------------

% %-------------------------- case 2 begin ---------------------------------------
% % 第一幅图象：
% I1 = imread('board.tif'); 
% I1 = rgb2gray(I1);
% I1 = imresize(I1,[256,256]); %改变图象大小
% I1 = im2double(I1); 
% 
% % 第二幅图象：
% I2 = imread('cameraman.tif');
% I2 = imresize(I2,[256,256]);
% I2 = im2double(I2); 
% 
% % 第三幅图象：
% I3 = imread('rice.tif'); I3 = im2double(I3); 
% 
% figure;
% subplot(2,3,1); subimage(I1); 
% subplot(2,3,2); subimage(I2);
% subplot(2,3,3); subimage(I3); 
% subplot(2,3,4); imhist(I1);
% subplot(2,3,5); imhist(I2);
% subplot(2,3,6); imhist(I3);
% %-------------------------- case 2 end ---------------------------------------

% 把图象按行读取，存贮在行向量中，并求其峭度值
imv1 = reshape(I1',1,256*256); kur1=kurtosis(imv1)    
imv2 = reshape(I2',1,256*256); kur2=kurtosis(imv2) 
imv3 = reshape(I3',1,256*256); kur3=kurtosis(imv3) 

% 构造原始图象数据矩阵，其中每行为一个图象数组
S = [imv1;imv2;imv3];

A = rand(3);                       % 构造随机的混合矩阵，  
X = A * S;                         % 生成混合图象数据矩阵

% 显示混合图象
x1 = X(1,:); xx1=reshape(x1,256,256); xxx1=eqim(xx1'); 
x2 = X(2,:); xx2=reshape(x2,256,256); xxx2=eqim(xx2'); 
x3 = X(3,:); xx3=reshape(x3,256,256); xxx3=eqim(xx3'); 
figure;
subplot(2,3,1); subimage(xxx1);
subplot(2,3,2); subimage(xxx2);
subplot(2,3,3); subimage(xxx3);
subplot(2,3,4); imhist(xxx1);
subplot(2,3,5); imhist(xxx2);
subplot(2,3,6); imhist(xxx3);

X = removmean(X);                  % 使均值为0
X = remstd(X);                     % 归一化方差

% 对数据进行白化预处理
covarianceMatrix=cov(X',1);          % 产生协方差阵
[E,D]=eig(covarianceMatrix);         % 求出协方差的特征向量和特征值
X = whitensig(X,E,D);

% 采用快速固定点算法分离图象
[T,W] = FastICA(X,'nonlinfunc','tanh','OverValue',0.0000001);

Y = removmean(T);
Y = remstd(Y);       % 归一化方差
Y = eqim(Y);         % 使象素值在0－1之间     

re1 = Y(1,:); reim1 = reshape(re1,256,256); im1 = reim1';
re2 = Y(2,:); reim2 = reshape(re2,256,256); im2 = reim2';
re3 = Y(3,:); reim3 = reshape(re3,256,256); im3 = reim3';

figure;
subplot(2,3,1); subimage(im1);
subplot(2,3,2); subimage(im2);
subplot(2,3,3); subimage(im3);
subplot(2,3,4); imhist(im1);
subplot(2,3,5); imhist(im2);
subplot(2,3,6); imhist(im3);

