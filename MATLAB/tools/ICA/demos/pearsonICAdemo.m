% demo file for pearsonICA
clear;close all;clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CASE ONE: extract skewed signals with near Gaussian distribution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate source signals, which is near Gaussian and strongly skewed.
signal_length = 5000;
numsig = 4; 
sources = sqrt((randn(numsig,signal_length).^2)+(randn(numsig, signal_length).^2));

% normalization
sources = centering(sources);
sources = remstd(sources);
icashow(sources(:,[1:100]),'title','source signals');

% mixing
A = rand(4);
mix = A * sources;

% whitening
[X,V] = whiten(mix);

% separating
W0 = eye(4); W0 = W0/norm(W0);
mu = 0.1;
iteration = 300;  
trace = 1;
[Y, W, E] = pearsonICA(X, W0, mu, V*A, iteration, trace);

icashow(Y(:,[1:100]),'title','separated signals');
figure;
plot(E);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%% CASE TWO: extrate signals with near Gaussian distribution %%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clear;
% % generate source signals, which is only near Gaussian
% vcorrcoef=eye(4);
% fircoef=[1 1 1 1]'; % no temporal structure
% vmu1=[0 0 0 0]';
% vmu2=[1 1 1 1]';
% vskew=[  0  0   0   0 ]';
% vkurt=[  2.7   2.9   3.2   3.1  ]';
% signal_length = 5000;
% sources=ic_rnd(vcorrcoef,fircoef,vmu1,vmu2,vskew,vkurt,signal_length);
% 
% 
% % normalization
% sources = centering(sources);
% sources = remstd(sources);
% icashow(sources(:,[1:100]),'title','source signals');
% 
% % mixing
% A = rand(4);
% mix = A * sources;
% 
% % whitening
% [X,V] = whiten(mix);
% 
% % separating
% W0 = eye(4); W0 = W0/norm(W0);
% mu = 0.1;   
% iteration = 4000;  
% trace = 1;
% [Y, W, E] = pearsonICA(X, W0, mu, V*A, iteration, trace);
% 
% icashow(Y(:,[1:100]),'title','separated signals');
% figure;
% plot(E);
