% ExtICAdemo -- demo for the On-line version of Extended Infomax algorithm
% Using the on-line stochastic natural gradient algorithm for maximum
% likelihood estimation to separate 2 sup-gaussian signals.
%


close all;clear;clc;
%==================  sources ===================================
N=10000;
numOfIC = 4;
sig=sigpool(numOfIC,N,'sub','off');
sig(5,:) = ((rand(1,N)<.5)*2-1).* log(rand(1,N));   % super-Gaussian
sig(2,:) = randn(1,N);   % Gaussian
sig = standarize(sig);       


%===================  plot  =====================================
icashow(sig(:,[N-499:N]),'title','Source Signals');

    
%================== mixing ================================
A=rand(size(sig,1));          
MixedSig=A*sig;                

icashow(MixedSig(:,[N-499:N]),'title','Mixed Signals');
   

%========================== run algorithm ====================

mu=0.005;  
fgt=0.98;
B=0.5*eye(5);
tic;
% [Y,W,E] = ExtICA(MixedSig, B, A, mu, fgt,'offline',2000);
[Y,W,E] = ExtICA(MixedSig, B, A, mu, fgt);
elapse_time=toc;


%========================== show result ====================
fprintf('==============================================\n');
fprintf('            Results of calculation            \n');
fprintf('==============================================\n');
% show results
fprintf('    Time:%g \n',elapse_time);
fprintf('    Matrix: \n ');disp(W*A);
fprintf('    Prepormance index: %g \n', CTPI(W*A,'norm') );
figure; plot(E); title('\bf Performance Index');


icashow(Y(:,[N-499:N]),'title','Separated Source Signals');
