% Demo for StoneBSS
ok;

load ABio7.mat;
S = standarize(ABio7);
A = rand(size(S,1));

X = A * S;

shf 		= 1; 
lhf 		= 9000; 	
max_mask_len= 500;
[ys, W ] = StoneBSS(X, shf, lhf, max_mask_len);

Y = standarize(ys);

icashow(S,'title','sources'); 
icashow(Y,'title','separated soruces');
