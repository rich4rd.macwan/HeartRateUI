% demo file for NLPCARLS.m

clear;close all;clc;
fprintf('\n\n');
%%%%%%%%%%%%%%%%%%%%%%%%%%% generate sources %%%%%%%%%%%%%%%%%%%%%%%%%%
% generate five source signals with zero mean and unit variance
numOfSamp = 2000;
numOfIC = 5;
sig =sigpool(numOfIC,numOfSamp,'sub','off');

% plot these source signals
icashow(sig(:,[numOfSamp-499 : numOfSamp]),'title','Source signals');
  

%%%%%%%%%%%%%%%%%%%%%%%%%%% mixing randomly %%%%%%%%%%%%%%%%%%%%%%%%%%
% random mix
A = rand(numOfIC);      
Mix = A * sig;               

% plot mixed signals
ICAshow(Mix(:,[numOfSamp-499 : numOfSamp]),'title','Mixed source signals');

    
%%%%%%%%%%%%%%%%%%%%%%%%%%% prewhitening %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% prewhitening
[wsig, V] = whiten(Mix);

% 画出白化后的信号
ICAshow(wsig(:,[numOfSamp-499 : numOfSamp]),'title','prewhitened');

    
%%%%%%%%%%%%%%%%%%%%%%%%%%% running algorithm %%%%%%%%%%%%%%%%%%%%%%%%%
b = 0.98;    % forgetting constant

[Y1,W1,E1] = NLPCARLS(wsig, eye(numOfIC), V*A, b, 'sub');
[Y2,W2,E2] = ngNLPCARLS(wsig, eye(numOfIC), V*A, b, 'sub');

%%%%%%%%%%%%%%%%%%%%%%%%%%% print and plot results %%%%%%%%%%%%%%%%%%%%
fprintf('\n');
fprintf('==============================================\n');
fprintf('            Results of Experiments            \n');
fprintf('              NLPCARLS algorithm              \n');
fprintf('==============================================\n');
fprintf('    Perpormance Index: %g \n', CTPI(W1*V*A,'ave') );
fprintf('    Global Matrix    : \n '); disp(W1*V*A);

% plot recoveried source signals
icashow(Y1(:,[numOfSamp-499 : numOfSamp]),'title','recoveried sources');


fprintf('==============================================\n');
fprintf('            Results of Experiments            \n');
fprintf('             ngNLPCARLS algorithm             \n');
fprintf('==============================================\n');
fprintf('    Perpormance Index: %g \n', CTPI(W2*V*A,'ave') );
fprintf('    Global Matrix    : \n '); disp(W2*V*A);

% plot evolution of error
figure; 
plot(E1); hold on;
plot(E2,'r'); 
legend('NLPCARLS','ngNLPCARLS');
title('\bf Performance Index');

% plot recoveried source signals
icashow(Y2(:,[numOfSamp-499 : numOfSamp]),'title','recoveried sources');

    



