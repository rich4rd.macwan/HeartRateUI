% demo for geig algorithm
clear;close all;clc

load sig4.mat;
sig = standarize(sig);
icashow(sig(:,[1:1000]));

A = rand(4);

X = A*sig;  % observation data

[IC, IClen] = size(X);

% compute an additional symmetric matrix based on nonstationarity
Q = zeros(size(X,1));
for k = 1:5
    tempo = X(:,k+1:IClen) * X(:,1: IClen-k)' / (IClen-k);
    Q = Q + tempo;
end
Q = (Q + Q')/2;

[S, B] = geig(X , Q);

% fprintf('performance index: %g\n',CTPI(B*A,'norm'));

icashow(S(:,[1:1000]));