% 考察当源信号个数小于接收信号个数时的分离情况
clear;clc;close all;
numOfIC = 4;    %源信号个数
numOfSen = 6;   %接收信号个数
N = 5000;
sig = gensig(N);

%随机混合
A = rand(numOfSen,numOfIC);
mix = A * sig;

% 分离
mu=0.001;  
fgt=0.99;
B=eye(size(mix,1));

[Y,B] = fICA(mix, B, A, mu, fgt);
Y = standarize(Y);
figure;
for i = 1 : numOfSen
    subplot(numOfSen,1,i);plot(Y(i,[N-499:N]));axis([-inf,inf,-5,5]);
end