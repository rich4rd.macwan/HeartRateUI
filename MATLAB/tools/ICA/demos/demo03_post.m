% demo03_post --- postprocess of images from demo03.m
% 说明：
%     由于ICA方法在恢复数据上的幅度不确定，在demo03.m中恢复的图象的灰度往往
%     和原始图象的正好相反,为了得到恢复的真正的原始图象，有必要对demo03.m中
%     的结果进行后处理，比如对恢复图象的灰度值进行取反。
% 注意：
%     究竟是哪个图象取反必须要具体分析，因为每次由demo03.m恢复的图象
%     都不相同，使用本演示程序时，需要由使用者修改本程序的前三行代码  
% 作者：张智林


% 通过具体分析从demo03.m中恢复的图象，对其中的2幅图象的灰度值进行取反，得到新的图象
P(1,:) = -T(1,:); 
P(2,:) = T(2,:);
P(3,:) = -T(3,:);

P = eqim(P);   % 把所有图象的灰度值修改为0－1之间

r1 = P(1,:); i1 = reshape(r1,256,256);  
r2 = P(2,:); i2 = reshape(r2,256,256);  
r3 = P(3,:); i3 = reshape(r3,256,256);  

figure; imshow(i1'); title('恢复图象后处理1');
figure; imshow(i2'); title('恢复图象后处理2');
figure; imshow(i3'); title('恢复图象后处理3');

figure; 
subplot(2,3,1); subimage(i1');
subplot(2,3,2); subimage(i2');
subplot(2,3,3); subimage(i3');
subplot(2,3,4); imhist(i1');
subplot(2,3,5); imhist(i2');
subplot(2,3,6); imhist(i3');


% 画出整个过程中产生的图象
figure;
subplot(3,3,3); subimage(I3);
subplot(3,3,2); subimage(I2);
subplot(3,3,1); subimage(I1); 

subplot(3,3,6); subimage(xxx3);
subplot(3,3,5); subimage(xxx2);
subplot(3,3,4); subimage(xxx1);

subplot(3,3,9); subimage(i3');
subplot(3,3,8); subimage(i2');
subplot(3,3,7); subimage(i1');
