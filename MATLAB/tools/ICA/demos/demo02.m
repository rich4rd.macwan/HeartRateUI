% demo02.m ---- using FastICA to separating two mixed audio waves
% 说明：
%    演示通过FastICA算法分离两个混合的真实语音信号
%    过程如下：首先输入两个独立的语音信号。然后通过一个随机的混合矩阵，将这
%    两个独立的语音信号混合起来。最后通过FastICA算法把他们分离出来。
% First done: 4,Oct.,2003
% Last done : 10,Oct.,2003

%--------------------------------------------------
% 输入两个单独的语音信号，并显示波形及归一化频谱
clear;clc;close all;
[s1,FS,NBITS]=wavread('source1.wav',160000);    % 源信号1，列向量
s2=wavread('source2.wav',160000);               % 源信号2，列向量

s1=s1/std(s1);             % 使s1方差为1
s1=s1-mean(s1);            % 使s1均值为0
s2=s2/std(s2);             % 使s2方差为1
s2=s2-mean(s2);            % 使s2均值为0


figure;subplot(2,1,2);plot(s2);subplot(2,1,1);plot(s1);title('\bf原始语音波形');

f=[0:160000-1]/160000;
y1=fft(s1');y2=fft(s2');
figure;subplot(2,1,2);plot(f,abs(y2));subplot(2,1,1);plot(f,abs(y1));title('\bf原始语音归一化频谱');

%-------------------------------------------------
% 把输入的两个单独语音信号随机混合，并显示其波形和归一化频谱，生成混合语音文件
A=rand(2);               % 混合矩阵
X=A*[s1';s2'];           % 混合信号，矩阵X的每行代表一个传感器收集到的混合信号
% X=remstd(X);             % 使方差为1
% X=removmean(X);          % 使均值为0

figure;subplot(2,1,2);plot(X(1,:));subplot(2,1,1);plot(X(2,:));title('\bf混合语音波形');
z1=fft(X(1,:)');z2=fft(X(2,:)');
figure;subplot(2,1,2);plot(f,abs(z2));subplot(2,1,1);plot(f,abs(z1));title('\bf混合语音归一化频谱');

wavwrite(X(1,:)',FS,NBITS,'demo02mix1.wav');
wavwrite(X(2,:)',FS,NBITS,'demo02mix2.wav');

%------------------------------------------------------
% 对混合语音进行预处理（白化）
WhitenedSig = whiten(X);
%------------------------------------------------------
% 进行FastICA计算，分离混合语音，并且显示和生成分离语音文件

% 快速固定点ICA算法(Fast Fixed-Point ICA algorithm)
[Sig,WeightMatrix]=FastICA(WhitenedSig,'method','symm','nonlinfunc','tanh');

% 恢复信号为0均值，单位方差
Sig=remstd(Sig);             % 使方差为1
Sig=Sig-(mean(Sig'))'*ones(1,160000);           % 使均值为0


wavwrite(Sig(1,:)',FS,NBITS,'demo02sd1.wav');
wavwrite(Sig(2,:)',FS,NBITS,'demo02sd2.wav');

figure;subplot(2,1,2);plot(Sig(2,:));subplot(2,1,1);plot(Sig(1,:));title('\bf恢复语音波形');

sp1=fft(s1');sp2=fft(s2');
figure;subplot(2,1,2);plot(f,abs(sp2));subplot(2,1,1);plot(f,abs(sp1));title('\bf原始语音归一化频谱');
