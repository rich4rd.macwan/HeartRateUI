%demo for Infomax algorithm
clear;clc;
N=7000;
for iter = 1:1
    close all;
    fprintf('\n===================== No.%d Experiment =====================\n',iter);
    sig = sigpool(2,N,'sup');
    
    % randomly mixing
    A = rand(size(sig,1));
    Mixed = A * sig;
    Mixed = removmean(Mixed);          % 使均值为0
    icashow(Mixed(:,[N-499:N]),'title','Mixed Sources');
    
    % whitening 
    covarianceMatrix = cov(Mixed',1);     % 产生协方差阵
    [E,D] = eig(covarianceMatrix);           % 求出协方差的特征向量和特征值
    [Z,V] = whitenSig(Mixed,E,D);     % 对信号进行白化
    
    % initialization for parameters of the algorithm
    B = eye(size(A));   % initial value of demixing matrix
    mu = 0.005;                         % initial value of learning rate
    VA = V*A;
    b = zeros(size(B,1),1);
    batchsize = 1;
    
    % run algorithm
    fprintf('Running the algorithm ...\n');
    tic;
    [Y, W, b, E] = Infomax(Z, B, b, VA, mu, batchsize);
    elapse_time = toc;
    
    % post-processing
    Y = removmean(Y);          % 使均值为0
    Y = remstd(Y);             % 使方差为1
    
    % show results
    fprintf('Running is over. The result is:\n');
    fprintf('    Time:%g \n',elapse_time);
    fprintf('    Prepormance index: %g \n', perfdex(W*VA) );
    fprintf('    Matrix: \n ');disp(W*A);
    icashow(Y(:,[N-499:N]),'title','Separation Sources');
    figure; plot(E); title('\bf Performance Index');  
    PI(iter,:) = E;  
    fprintf('=================== No.%d Experiment is OVER ===================\n',iter);
end