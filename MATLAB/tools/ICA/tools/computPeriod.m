function halfy = computPeriod(XX, lag, threshold)
% computPeriod -- 显示输入数据XX中隐含的周期,显示的范围是从延迟0到延时lag-1
% 
% Format:    halfy = computPeriod(XX, lag, threshold)
%
% Parameters:
%      halfy       -- 输出向量，该向量的峰值显示出隐含的周期或明显的时序结构
%      XX          -- 输入数据向量
%      lag         -- 给定延时，函数返回的数据对应着延时0到延时(lag-1)
%      threshold   -- 阈值，用来控制消除随机起伏的程度，通常在1-2之间。值越大，则显示出的峰值越少
%
% See also
%      computPeriodDemo
%
% Reference:
%  [1]Zhi-Lin Zhang, Liqing Zhang, Extraction of Temporally Correlated Signals Using
%     a Two-Stage Method with Applications in Biomedical Engineering, submitted, 2006
%
% Author :  Zhi-Lin Zhang (zlzhang@uestc.edu.cn)
% Date   :  May 20, 2006
% Version:  1.0



LX = length(XX);  

% 去均值，去方差
XX=(XX-mean(XX))/cov(XX); 

% 线性预测
a = lpc(XX,10);

% 误差信号
est_XX = filter([0 -a(2:end)],1,XX);    

% 预测误差信号
ee = XX - est_XX;                     

% 求预测误差信号的自相关函数
y=xcorr(ee,lag-1);

% 标准化
y=standarize(y); 

% 去掉自相关函数中的无用的随机起伏
y=sign(y).* max( abs(y)-threshold,   0 );

% % 作图显示
halfy=y;  halfy(1:lag-1)=[];
% figure;  plot(halfy);  axis tight;
