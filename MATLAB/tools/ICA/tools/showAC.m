function AC = showAC(X,len,varargin)
% Plot the autocorrelation function of each signal, from lag 0 to lag len and return the 
% whole nonnegative part of the autocorrelation function.
%
% Format:
%     AC=showAC(X, len, 'title', '......');
%
% Parameters:
%          AC --- each row represents the nonnegative part of the ACF of an input signal 
%           X --- input signal matrix. Each row represents a signal.
%         len --- the autocorrelation function from lag 0 to lag len; default value
%                 is  length(X)-1
%     'title' --- denote that the chars in the fourth parameter is the title
%    '......' --- the contents of the title
%
% Author: Zhi-Lin Zhang (zlzhang@uestc.edu.cn)
% Version: 1.3         Date: Apr.4, 2006
% Version: 1.2         Date: Feb.9, 2006
%



[ICnum, IClen] = size(X);

flagtitle=0;     % 标志着可选参数'title'是否选择。如果为1表示用户选择该参数，输入该参数值 

% 获取可选参数
if nargin == 1
    len = IClen-1;    % default value
elseif(mod(length(varargin),2)==1)
    error('Optional parameters should always go by pairs.\n');
else
    for i=1:2:(length(varargin)-1)
        switch lower(varargin{i})
            case 'title'
                titlechar=varargin{i+1};
                flagtitle=1;
            otherwise
                error(['Unrecognized parameter: ''' varargin{i} '''']);
        end
    end
end

if len > IClen-1
    fprintf(['The value of ''lag'' is too large and is re-setted as length(' inputname(1) ')-1.\n']);
    len = IClen-1;
elseif len<0
    fprintf(['The value of ''lag'' should be nonnegative. Its value is re-setted as its absolute value.\n']);
    len = -len;
end

figure;
for i = ICnum : -1 : 1
    ac = xcorr(X(i,:),'coeff');
    ac([1:IClen-1]) = [];    % remove the negative part
    AC(i,:) = ac;
    subplot(ICnum,1,i); plot(ac(1:len+1)); 
    if i == ICnum  xlabel('\bfLag'); end;
    if  flagtitle==1 & i ==1
        titletext=['\bf',titlechar];
        title(titletext);
    end

end
