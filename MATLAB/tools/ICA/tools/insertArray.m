function y = insertArray(x, p, pos)
% Insert the array P into the array X at the position POS.
%
% Parameters:
%      y -- return the new array (column vector)
%      x -- the original array
%      p -- an array that will be inserted to the array x
%    pos -- the insert position of x, i.e., p is inserted into x after its No.pos element
%           
% Author:
%     Zhi-Lin Zhang
%     zlzhang@uestc.edu.cn
%
% Version:  1.0
%    Date:  Feb.22, 2006



Nx = length(x);    x = reshape(x,Nx,1);
Np = length(p);    p = reshape(p,Np,1);

% Is the value of pos valid?
if pos<1 | pos>Nx
    error('The value of ''pos'' is not valid');
end

x1 = x(1:pos);
x3 = x(pos+1:end);
x2 = p;
y = [x1;x2;x3];


