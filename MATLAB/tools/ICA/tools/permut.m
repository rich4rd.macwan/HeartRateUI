function Y = permut(N)
% PERMUT -- generate N*N permutation matrix, i.e. each row and each column has
% single one�� and other elements' values are zeros.
%
% Author: Zhang,Zhi-Lin
% Version: 1.0
% Date: Nov.22, 2003



Index = randperm(N);
Y = zeros(N);

for j=1:N
    k = Index(j);
    Y(j,k) = 1;
end
