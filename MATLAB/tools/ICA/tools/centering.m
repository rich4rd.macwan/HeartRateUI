function NewSig = centering(OldSig)
% centering --- make the input signals' mean value zero.
% The input OldSig is in the form N*P, where N is the number of signals, and P is
% the length of each signal, then returned NewSig is also in the form N*P,
% and each row (each signal) has zero mean.
%
% Command:
%     NewSig = centering(OldSig)
%
% See also:
%     remstd    whiten    standarize
%
% Author��Zhi-Lin Zhang
%         zlzhang@uestc.edu.cn
%         http://teacher.uestc.edu.cn/teacher/teacher.jsp?TID=zzl80320
%
% Last version: 1.5     Date: Apr.17,2005
%      Version: 1.0     Date: Oct.31,2003

                     

NewSig = zeros (size (OldSig));
meanValue = mean (OldSig')';
NewSig = OldSig - meanValue * ones (1,size (OldSig, 2));
