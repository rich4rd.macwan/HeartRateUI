function B = tri(A)
% TRI --- only the upper triangular part of A is computed and its transpose
%   is copied to the lower triangular part, making the resulting matrix B
%   symmetric. The number of columns of A should be equal to that of lines.
%
% Author: Zhang Zhi-Lin
%         zzl.private@eyou.com
% Version: 1.0
% Date: Nov.12,2003



if size(A,1) ~= size(A,2)
    error('The number of columns of A should be equal to that of lines\n');
end

d = triu(A);    % the upper triangular part of A
B = d + d' - diag(diag(A));
