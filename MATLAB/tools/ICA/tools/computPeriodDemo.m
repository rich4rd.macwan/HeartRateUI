% demo file for computPeriod
% illustrate how to find the hidden periods from the mixing signals

load FECGdata.mat;
figure;
subplot(2,1,1);
    ACF = xcorr(X(1,:),499,'coeff');
    ACF(1:500) = [];
    plot(ACF);title('\bfBy Ordinary Autocorrelation Method');xlabel('lag');
    axis tight;

subplot(2,1,2); 
    [halfy] = computPeriod(X(1,:),500,1.4);
    plot(halfy);title('\bfBy computPeriod Method');xlabel('lag');
    axis([-inf,inf,0,2]);

