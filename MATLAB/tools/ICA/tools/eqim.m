function Y = eqim(X)

% EQIM --- Return the normalized( value in [0,1] ) data Y from input data X
%
% Author: Zhang Zhi-Lin
%         zhangzl@vip.163.com ,  zzl.private@eyou.com
%         Phone: +86-28-83204006
%         http://teacher.uestc.edu.cn/teacher/teacher.jsp?TID=zzl80320
% Version: 1.0
% Date: Nov.17,2003


xmax = max(max(X));
xmin = min(min(X));
[N,M]=size(X);
for k =1:N
    Y(k,:) = ( X(k,:)-xmin )/(xmax - xmin);
end