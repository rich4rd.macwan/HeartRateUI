function newMatrix = selcol(oldMatrix,maskVector)

% SELCOL ---- Selects the columns of the matrix that marked by one in the given vector.
%             The maskVector is a column vector.
% 根据列向量maskVector中非零元素1的位置，提取矩阵oldMatrix中相应的列向量，组成新的矩阵。
% 调用格式：
%     newMatrix = selcol(oldMatrix,maskVector)
%
% 参数说明 ......
%     oldMatrix    :  输入的矩阵
%    maskVector    :  掩模列向量，元素为0或者为1。若为1，则对应的oldMatrix中的列向量被提取，组成新的矩阵
%     newMatrix    :  输出矩阵。其行数与输入矩阵相同。每列是从输入矩阵中提取出的。
%
% 参考：
%     PCA   FFPICA


if size(maskVector, 1) ~= size(oldMatrix, 2)
  error ('The mask vector and matrix are of uncompatible size.\n');
end

numTaken = 0;     

for i = 1 : size (maskVector, 1)
  if maskVector(i, 1) == 1
    takingMask(1, numTaken + 1) = i;
    numTaken = numTaken + 1;
  end
end

newMatrix = oldMatrix(:, takingMask);