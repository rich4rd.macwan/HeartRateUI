function varargout = whiten2(Mixed)
% whiten2 -- whiten input signals with eigenvalue decomposition, 
% return whitened signals and whitening matrix. Input parameter 
% 'Mixed' should in matrix form: N * P, whose N represents 
% the number of sources, and P represents the sampling points.
%
% Notice: before whitened, the input signals should better be standarized, 
% i.e., have zero mean and unit variance.
%
% Command:
%           [wsig, V] = whiten(Mixed), where wsig = V * Mixed
%            wsig = whiten(Mixed)
%
% Author��Zhi-Lin Zhang
%         zlzhang@uestc.edu.cn
%         http://teacher.uestc.edu.cn/teacher/teacher.jsp?TID=zzl80320
%
% Last version : 1.2      Date:  Dec.20, 2004
%      version : 1.0      Date:  Dec.7, 2004



covarianceMatrix = cov(Mixed',1);     % covariance matrix
[E,D] = eig(covarianceMatrix);        % eigenvalue decomposition

% whitening with eigenvalue decomposition
V = E*inv(sqrt(D))*E';
wsig = V * Mixed;

varargout{1} = wsig;
varargout{2} = V;



