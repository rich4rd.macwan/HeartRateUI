close all;
clear all;
clc;

addpath('./tools/skindetector');

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
% Create a cascade detector object.
faceDetector = vision.CascadeObjectDetector('ClassificationModel','FrontalFaceCART','MinSize',[200 200]);

img = imread('/home/yannick/ownCloud/face2.jpg');
img_copy = double(img);

bbox  = step(faceDetector, img);

x = bbox(1, 1);
y = bbox(1, 2);
w = bbox(1, 3);
h = bbox(1, 4);
bboxPoints = [x,y;x,y+h;x+w,y+h;x+w,y];

% Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
% format required by insertShape.
bboxPolygon = reshape(bboxPoints', 1, []);

% Display a bounding box around the detected face.
img = insertShape(img, 'Polygon', bboxPolygon);

imgROI = imcrop(img_copy,bbox(1,:));
%figure(2), imshow(imgROI), title('Detected Face');
imwrite(uint8(imgROI), '~/faceROI.png');

% average RGB value of skin pixels
imgD = double(imgROI);
skinprob = computeSkinProbability(imgD);
mask = skinprob>-2;
mask = imfill(mask, 'holes');
maskRGB = zeros(size(skinprob,1),size(skinprob,2),3);
maskRGB(:,:,1) = mask;
maskRGB(:,:,2) = mask;
maskRGB(:,:,3) = mask;
imgD = imgD.*maskRGB;
imgROI(imgD==0)=nan;

imgROI_nan = double(imgROI);
imgROI_nan(imgD==0)=nan;

imwrite(uint8(imgD), '~/faceSkin.png');


figure(3), imshow(uint8(imgD)), title('Detected Face + skin');
%pause(1);

