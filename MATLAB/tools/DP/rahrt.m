function [obj]=rahrt(i,obj)
%RAHRT Robust Algorithm for Heart Rate Tracking using dynamic programming
% obj = An instance of the class rahrt_data containing the required values
%       Used for better code visibility
% i = Number of iterations, number of columns in the graph
%
%   References:
%     David Talkin, "A Robust Algorithm for Pitch Tracking(RAPT)"
%     Speech Coding and Synthesis, 1995.


if i==1 % first iteration
    obj.D(:,i)=zeros(obj.nMaxStates,1);
    obj.D(:,i)=obj.pks(:,i);
    obj.q(:,i)=1;
else
    
    for k=1:numel(obj.pks(:,i-1))
        %Memoization
        %if D(k,i-1) is not calculated, do it. Else retreive the value
        if obj.D(k,i-1)==-1
            obj=rahrt(i-1,obj);
        end
    end        
            
    for j=1:numel(obj.pks(:,i)) 
        %Calculate the scores considering all previous states
        del=(obj.locs(j,i)-obj.locs(:,i-1));
        %delta=exp(-(del.^2/obj.sigma))-0.75;
        clear delta
        for k=1:length(del)
           if(abs(del(k)) > 15)
               delta(k)=-20;
           else
               delta(k)=1;
           end
        end
        delta=delta';
        
        Dij = obj.D(:,i-1) + (obj.pks(j,i).*delta);
        [obj.D(j,i), obj.q(j,i)]= max(Dij) ;     
                  
    end
end

end
