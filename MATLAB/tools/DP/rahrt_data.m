%Class containing data structs and utility methods for dynamic programming
%using RAHRT.
classdef rahrt_data
    properties
        % step between 2 sliding window positions (in seconds) -> used to adjust cost function
        step_sec = [];
        %Array that holds the global score
        D=[];
        %f x i array to hold the freq peaks
        pks=[];
        %f x i array to hold the freq peak locations
        locs=[];
        %Backpointers to the min indices,i.e. the optimum path
        q=[];
        %f x i array holding tfr data
        tfrData=[];
        %Number of frames
        nFrames=0;
        %Number of maximum states per frame
        nMaxStates=0;
        %normalisation parameter in exp for calculating transition cost
        %(exp(-delta^2/sigma))
        % sigma large -> small transition cost
        % e.g. sigma=40 (abs(f1-f2)=40 -> delta=0.25)
        % e.g. sigma=30 (abs(f1-f2)=40 -> delta=0.1)
%         sigma =500;
    end
    methods
        function obj = rahrt_data(tfr, step_sec)
            obj.step_sec = step_sec;
            obj.nFrames=size(tfr,1);
            obj.nMaxStates=size(tfr,2);
            obj.D=-1*ones(obj.nMaxStates,obj.nFrames);
            obj.pks=tfr(:,:,2)';% hold the freq peaks %-1*ones(obj.nMaxStates,obj.nFrames);
            obj.locs=tfr(:,:,1)';% hold the freq peak locs %-1*ones(obj.nMaxStates,obj.nFrames);
            obj.q=zeros(obj.nMaxStates,obj.nFrames);
            obj.tfrData=tfr;
        end
    end
    
end