function y = SVD_filter (y, m, sv_ratio )
% y = SVD_filter (u , m, sv_ratio)
% Use the SVD to filter out least significant (noisy) portions of a signal
%
% INPUTS DESCRIPTION
% ========== ===============
% y signal to be filtered , 1 x N
% m rows in the Hankel matrix of y , Y m > N/2+1
% sv_ratio remove components of SVD of Y with s_i < sv_ratio * s 1
%
% OUTPUT DESCRIPTION
% ========== ===============
% y reconstruction of y from low?rank approximation of Y
[l,N] = size (y);
% put signal into a Hankel Matrix . of dimension m x n ; m > n ; m+n = N+1
if (m < N/2) error('SVD_filter : m should be greater than N/2 '); end
n = N+1-m; % number of columns of the Hankel matrix
Y = zeros (m,n);
for k =1: m
    Y ( k , : ) = y ( k :k+n -1 );
end

[U,S,V] = svd ( Y , 'econ' ); % make economical SVD of the Hankel Matrix .

 K = max( find(diag(S)/S(1,1) > sv_ratio )); % find the most significant part

 figure (3)
 loglog(diag(S)/S(1 ,1) , 'o')
 ylabel ('\sigma_i / \sigma_1 ')
 xlabel ('i')
 
 % build a new rank?K matrix from the first K dyads of the SVD
 d = diag(S);
 d=d(1:K);
 S2 = diag(d);
 
 Y = U(: ,1:K)*S2* V(: ,1:K)';

 % Average ant i?diagonal components to make the lower?rank matrix a Hankel matrix
 % Ex t rac t the f i l t e r e d s i g n a l from the f i r s t column and l a s t row of the
 % lower?rank Hankel matrix .

 y = zeros (1,N);
 y (1) = Y (1 ,1);

 for k =2: m % first column of Hankel matrix
     min_kn = min(k,n);
     y(k) = sum(diag(Y(k: -1:1 ,1: min_kn ))) / min_kn ;
 end
 for k =2: n % l a s t row of Hankel matrix
    y(m+k -1) = sum(diag(Y(m: -1:m-n+k,k:n ))) / (n-k +1);
 end

 % ????????????????????????????????????????? SVD f i l t e r HP Gavin 2013?09?10
%%System I d e n t i f i c a t i o n , Duke Uni v e r s i ty , Fa l l 2013