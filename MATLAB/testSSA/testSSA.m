% test the use of SVD of signal Hankel matrix for filtering
clear all;
close all;

randn('seed' ,2); % initialize random number generator
N = 2048; % number of data points
dt = 0.05; % time step increment
t = [1:N]*dt;  % time values

freq = [ (sqrt(5) -1)/2 1 2/(sqrt(5)-1) exp(1) pi]'; % set of frequencies
yt = sum(sin(2*pi*freq*t)) / length(freq); % true signal

figure (1);
clf
hold on;
plot(t,yt,'b')
xlim ([10 20])
ylabel('signals')
xlabel('time , s')

%yt = sin(2*pi*freq*t); % true signal
SNR = 0.5; % works with very poor signal?to?noise ratio
m = ceil(0.6*N + 1 )
sv_ratio = 0.60; % singular value ratio closer to 1 -> more filtering

% add measurement noise
yn = yt + randn(1,N) * sqrt(yt*yt'/N) / (SNR);
plot(t,yn)


yf = SVD_filter(yn , m, sv_ratio); % remove random components

plot( t,yf, 'r')

% yf_yt_err = norm(yf -yt)/norm(yt) % compare f i l t e r e d to true
% yf_yn_err = norm(yf -yn)/norm(yt) % compare f i l t e r e d to noisy
% 
% nfft = 512;
% [PSDyt ,f] = psd(yt ,1/dt,nfft);
% [PSDyn ,f] = psd(yn ,1/dt,nfft);
% [PSDyf ,f] = psd(yf ,1/dt,nfft);

% 
% figure (2)
% clf
% idx = [4: nfft /2];
% loglog(f( idx ), PSDyt ( idx ), f( idx ), PSDyn ( idx ), f( idx ), PSDyf (idx ))
% xlabel ('frequency , Hz ')
% ylabel ('PSD ')
% text(f( nfft /2) , PSDyt ( nfft /2) , 'true ')
% text(f( nfft /2) , PSDyn ( nfft /2) , 'noisy ')
% text(f( nfft /2) , PSDyf ( nfft /2) , 'filtered ')
