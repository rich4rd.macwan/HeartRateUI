function [traces2, ecg2] = preprocessData(traces, ecg, newFs, winLength)

fprintf('preprocessing data...');
traces2=[];
ecg2=[];
deltaT = 1/newFs * 1000;
lowF=.3;
upF=3.5;
maxF=15;
Fs=30;
FsECG=1000;

[becg aecg]=butter(4,[lowF upF]/(FsECG/2));
% i ranges from 1 to the number of videos for a patient
for i=1:size(traces,2)
    
%     if i==4
%         %ecg data missing for patient3-atrialflutter4
%         continue;
%     end
    % get current traces
    crtTrace = traces(i).rgbTraces;
    timeTrace = crtTrace(end,:);
    traceSize = size(crtTrace,2);
    
    % get current ecg
    crtEcg = ecg(i).data;
    timeEcg = crtEcg(end,:);
    ecgSize = size(crtEcg,2);
    crtEcgName=ecg(i).name;
    
    disp([]);
    % number of sliding windows
    nbWin = floor(max(timeTrace)/(winLength*1000));
    
    for j=1:nbWin
        
        % get start/end time of current window
        startT = (j-1)*(winLength*1000) +1;
        endT = j*(winLength*1000);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%% preprocess traces %%%%
        
        % get trace indices (startInd, endInd)
        [val startInd] = min(abs(timeTrace-startT));
        [val endInd] = min(abs(timeTrace-endT));
        
        % get current trace window
        crtTraceWin = crtTrace(1:3,startInd:endInd);
        crtTimeTraceWin = timeTrace(startInd:endInd);
        crtTraceWinSize = size(crtTraceWin,2);
        %figure(), plot(crtTimeTraceWin, crtTraceWin(2,:)), title('raw green trace');
        
        % normalize each signal to have zero mean 
        crtTraceWin = crtTraceWin - repmat(mean(crtTraceWin,2),[1 crtTraceWinSize]);
        % and also unit variance
        crtTraceWin = crtTraceWin ./ repmat(std(crtTraceWin,0,2),[1 crtTraceWinSize]);
        %figure(), plot(crtTimeTraceWin, crtTraceWin(2,:)), title('normalized green trace');
        
        
        %Length of signal for a video is 1. Maybe the end, skip it
        %because spdiags breaks
        if(size(crtTraceWin,2)<10)
            continue;
        end
        
        % detrend
        for k=1:size(crtTraceWin,1)
            crtTraceWin(k,:) = detrendsignal(crtTraceWin(k,:)')';
        end
        %figure(), plot(crtTimeTraceWin, crtTraceWin(2,:),'-o'), title('detrended green trace');
        
        % Uniform sampling + band pass filter
        crtTimeTraceWinUnif = linspace(crtTimeTraceWin(1), crtTimeTraceWin(end), length(crtTimeTraceWin));
        crtTraceWinUnif=zeros(size(crtTraceWin,1),numel(crtTimeTraceWinUnif));
        crtTraceWinFiltered=zeros(size(crtTraceWin,1),numel(crtTimeTraceWinUnif));
        for k=1:size(crtTraceWin,1)
            crtTraceWinUnif(k,:) = interp1(crtTimeTraceWin,crtTraceWin(k,:), crtTimeTraceWinUnif); 
            crtTraceWinFiltered(k,:) = filter(b,a,crtTraceWinUnif(k,:)')';
            %crtTraceWinFiltered(k,:) = crtTraceWinUnif(k,:);
        end
        %figure(), plot(crtTimeTraceWinUnif, crtTraceWinSmoothed(2,:),'-o'), title('detrended green trace + BP filter');
        
        % interpolation (1000Hz = ECG sampling - is it too much ?)
        crtTimeTraceWinUnifResampled = linspace(crtTimeTraceWin(1), crtTimeTraceWin(end), crtTimeTraceWin(end)-crtTimeTraceWin(1)+1);
        crtTraceWinUnifResample=zeros(size(crtTraceWin,1),numel(crtTimeTraceWinUnifResampled));
        for k=1:size(crtTraceWin,1)
            crtTraceWinUnifResample(k,:) = interp1(crtTimeTraceWinUnif,crtTraceWinFiltered(k,:), crtTimeTraceWinUnifResampled, 'pchip'); 
        end
        
%          plot(crtTimeTraceWinUnifResampled, crtTraceWinUnifResample(2,:)), title('detrended green trace + BP filter + resample');
        
        % Moving window filtering (useful ?, why 150 ?)
        for k=1:size(crtTraceWin,1)
            crtTraceWinUnifResample(k,:) = smooth(crtTraceWinUnifResample(k,:), 150, 'moving');
        end
        
        
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%% preprocess ECG %%%%
        
         % get trace indices (startInd, endInd)
        [val startInd] = min(abs(timeEcg-startT));
        [val endInd] = min(abs(timeEcg-endT));
        
        % get current ecg window
        crtEcgWin = crtEcg(1:end,startInd:endInd);
        crtTimeEcgWin = timeEcg(startInd:endInd);
        crtEcgWinSize = size(crtEcgWin,2);
        %figure(), plot(crtTimeEcgWin, crtEcgWin(2,:)), title('raw ecg channel #2');
        
        % normalize each signal to have zero mean and unit variance
        % TODO: remove time
        crtEcgWin = crtEcgWin - repmat(mean(crtEcgWin,2),[1 crtEcgWinSize]);
        crtEcgWin = crtEcgWin ./ repmat(std(crtEcgWin,0,2),[1 crtEcgWinSize]);
        if numel(crtEcgWin>0)
            crtEcgWinFiltered  = filter(becg,aecg,crtEcgWin(2,:)')';

            %Scale rppg waveform to match the ecg 

            rppgMax=max(crtTraceWinUnifResample(2,:));
            rppgMin=min(crtTraceWinUnifResample(2,:));

            ecgMax=max(crtEcgWin(2,:));
            ecgMin=min(crtEcgWin(2,:));
            multiplier=(ecgMax-ecgMin)/(rppgMax-rppgMin);
            hold on

            plot(crtTimeTraceWinUnifResampled, multiplier*crtTraceWinUnifResample(2,:),'r'), title('Normalized rppg scaled');
            plot(crtTimeEcgWin, crtEcgWin(2,:)), title('normalized ecg channel #2');
            legend('RPPG','ECG');
            title([num2str(i) '.  ' crtEcgName '-' num2str(j)]);
            hold off
            %figure(), plot(crtTimeEcgWin, crtEcgWinFiltered), title('normalized filtered ecg channel #2');
            
        end
            pause();
            clf
        %close all;
    end    
end

fprintf('OK \n');
