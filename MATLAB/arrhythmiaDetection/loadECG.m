function [ecg, ecgTime] = loadECG(vidFolder)

fprintf('loading ecg... ');

ecgfiles=dir([vidFolder '/ecg/*.txt']);

ecgData = [];
for j=1:numel(ecgfiles)
    fileID = fopen([vidFolder '/ecg/' ecgfiles(j).name]);
    intro = textscan(fileID,'%s',119,'Delimiter','\n'); % read header
    formatString = repmat('%f',1,13);% Create format string to read 13 channels (signed int16)
    inputText = textscan(fileID,formatString, 'delimiter',',');
    ecgTmp = cell2mat(inputText)';
    %[numRows,numCols] = size(ecgTmp);
    fclose(fileID);
    ecgData = [ecgData ecgTmp];
end

ecgTime = 1 : size(ecgData,2);
ecgTime = ecgTime/1000;
ecg = ecgData ;

fprintf('OK \n');