% Analyse of clinic rPPG data
clear all;
close all;
clc;

vidFolder = '/run/media/richard/Le2iHDD/CHU/patient9-sinus1/';
matFileName = 'pulseTraceGreen_skin';
%matFileName = 'rgbTraces_skin';

pulseFile = [vidFolder matFileName '.mat'];
% load pulse traces
if(exist(pulseFile, 'file') == 2)
    load(pulseFile);
else
    disp('oops, no input file');
    return;
end

% load ECG
[ecg, ecgTime] = loadECG(vidFolder);

% display traces
showTraces(pulseTraceGreen, timeTrace, ecg, ecgTime);
%showTraces(rgbTraces(2,:),rgbTraces(4,:)/1000,ecg,ecgTime);

