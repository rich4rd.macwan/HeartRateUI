function showTraces(pulseTrace, timeTrace, ecg, ecgTime)
% display the ecg + pulse trace
WINDOW_LENGTH_SEC = 10; % length of the sliding window (in seconds)
STEP_SEC = 0.5; % step between 2 sliding window position (in second
LOW_F=.4; % low freq for PSD range
UP_F=4; % high freq for PSD range
USE_RR_ECG=1;
USE_PP_RPPG=0;

FILTER_ORDER=4;


pulseTrace=reshape(pulseTrace,[1 numel(pulseTrace)]);
timeTrace=reshape(timeTrace,[1 numel(timeTrace)]);

%Interpolate rppg to ecgTime
%pulseTrace=interp1(timeTrace,pulseTrace,ecgTime,'pchip');
%timeTrace=ecgTime;

% get exact Fs
Fs = 1/mean(diff(timeTrace));
traceSize = length(pulseTrace);
winLength = round(WINDOW_LENGTH_SEC*Fs); % length of the sliding window for FFT
step = round(STEP_SEC*Fs);
% get coef of butterworth filter
[b, a]= butter(FILTER_ORDER,[LOW_F UP_F]/(Fs/2));


ind = 0;
HR_ECG=zeros(round((traceSize-winLength)/step),1);
HR_RPPG=HR_ECG;
time=HR_ECG;
color_rppg=[0 102 204]/255;
color_ecg=[255 51 51]/255;
ecgChannel=12;
% for i=1:12
%     figure(2);cla;
%     plot(ecg(i,:));
%     pause
% end

%Skip first few seconds
skipsecs=5;
initFrame=floor(skipsecs*Fs);
%Prune current peaks based on the average IBI of the last ibiHistoryWin
%secs
ibiHistoryWin_sec=5; 
ibiHistoryWin=round(Fs*ibiHistoryWin_sec);
ibiThresh=13; %bpm
finalPPs=zeros(size(pulseTrace));
finalRRs=zeros(size(pulseTrace));
ibiInd=1;
if numel(ecg)>0
    useEcg=1;
else
    useEcg=0;
end

for i=initFrame:step:traceSize-winLength
    ind = ind+1;
    
    if(mod(i,10)==0)
        disp(['processing window ' num2str(i) ' on ' num2str(traceSize-winLength)]);
    end
    
    % get start/end index of current window
    startInd = i;
    endInd = i+winLength-1;
    startTime = timeTrace(startInd);
    endTime = timeTrace(endInd);
    crtTime =  (startTime + endTime)/2;
    time(ind) = crtTime;
    
    % get current pulse window
    crtPulseWin = pulseTrace(startInd:endInd);
    %crtPulseWinFilt=filter(b,a,crtPulseWin);
    %crtPulseWinFilt=sosfilt(sos,crtPulseWin)*g;
    crtPulseWinFilt=crtPulseWin;
    crtTimeWin = timeTrace(startInd:endInd);
    %figure(2),cla, plot(crtTimeWin, crtPulseWinFilt), title('pulse trace');  
    
    % get current ecg window
    [~, startInd] = min(abs(ecgTime-startTime));
    [~, endInd] = min(abs(ecgTime-endTime));
    
    if(useEcg)
    % get current ecg window
    crtEcgWin = ecg(ecgChannel,startInd:endInd);
    crtTimeEcgWin = ecgTime(startInd:endInd);
    crtEcgWinSize = size(crtEcgWin,2);
    %figure(), plot(crtTimeEcgWin, crtEcgWin(2,:)), title('raw ecg channel #2');
    
    % normalize each signal to have zero mean and unit variance
    crtEcgWin = crtEcgWin - repmat(mean(crtEcgWin,2),[1 crtEcgWinSize]);
    crtEcgWin = crtEcgWin ./ repmat(std(crtEcgWin,0,2),[1 crtEcgWinSize]);
    
%     crtPulseWinSize=size(crtPulseWinFilt,2);
%     crtPulseWinFilt=crtPulseWinFilt-repmat(mean(crtPulseWinFilt,2),[1 crtPulseWinSize]);
%     crtPulseWinFilt= crtPulseWinFilt./ repmat(std(crtPulseWinFilt,0,2),[1 crtPulseWinSize]);
    % get exact ECG Fs
    Fs_Ecg = 1/mean(diff(crtTimeEcgWin));
    end
    
    if(~USE_PP_RPPG)
        % RPPG using welch
        [rPPG_power,rPPG_freq] = pwelch(crtPulseWinFilt,[],[],length(crtPulseWinFilt)*3,Fs, 'psd'); % zero pading to increase the nb of points in the PSD
        rangeRPPG = (rPPG_freq>LOW_F & rPPG_freq < UP_F);   % frequency range to find PSD peak
        rPPG_power = rPPG_power(rangeRPPG);
        rPPG_freq = rPPG_freq(rangeRPPG);
       
        [pksrPPG,pksrPPG_loc] = findpeaks(rPPG_power,'SortStr','descend','NPeaks',4); % get 2 first peaks (only one is used now but will be useful for postpreocessing
        rPPG_peaksLoc = rPPG_freq(pksrPPG_loc);
        HR_RPPG(ind)=rPPG_peaksLoc(1);
        
        crtIBI=1/HR_RPPG(ind);
        %Find the minimum IBI corresponding to the freq where the 
        %rppg power is 1 std dev less than that of the peak
        stddev=std(rPPG_power);
        %We want a minimum ibi, so choose the freqs greater than the peak
        %freq
        powerRightOfPeak=rPPG_power(rPPG_freq>HR_RPPG(ind));
        freqRightOfPeak=rPPG_freq(rPPG_freq>HR_RPPG(ind));
        %Find the freq closest to one stddev away from the peak
        [m,idx]=min(abs(powerRightOfPeak-(powerRightOfPeak(1)-stddev)));
        minIBI=1/freqRightOfPeak(idx);
        %[pksRPPG,pksrPPG_loc] = findpeaks(crtPulseWinFilt,crtTimeWin,'MinPeakProminence',.5);
        [pksRPPG,pksrPPG_loc] = findpeaks(crtPulseWinFilt,crtTimeWin,'MinPeakDistance',minIBI);
         
%         figure(2);
%         cla
%         findpeaks(crtPulseWinFilt,crtTimeWin,'MinPeakProminence',.5);
        pind=2;
        pruned_pksRPPG_loc=pksrPPG_loc;
        
        %Heuristics for selecting uniform peaks
        while(false && pind<numel(pksrPPG_loc))
            ibi=pksrPPG_loc(pind)-pksrPPG_loc(pind-1)
            %If current p2p distance is much smaller than crtIBI
            if(crtIBI-ibi>ibiThresh/60)
                %Choose the larger peak
                if(numel(pksRPPG)~=numel(pksrPPG_loc))
                 findpeaks(crtPulseWinFilt,crtTimeWin,'MinPeakProminence',.3);
                 end
                figure(3);
                cla;
                hold on;
                plot(crtTimeWin,crtPulseWinFilt);
                plot(pksrPPG_loc,pksRPPG,'v');
                pause(.1)
               
                [M I]=min(pksRPPG(pind-1:pind));
                %pksRPPG(I)=nan;
                pruned_pksRPPG_loc(pksRPPG==M)=nan;
            end
            %If current p2p distance is much larger than crtIBI
            if(ibi-crtIBI>ibiThresh/60)
                %Missing peaks, find a peak close to the expected position
                tmpTime=crtTimeWin(crtTimeWin>pksrPPG_loc(pind-1) & crtTimeWin<pksrPPG_loc(pind));
                tmpPulse=crtPulseWin(crtTimeWin>pksrPPG_loc(pind-1) & crtTimeWin<pksrPPG_loc(pind));
                [tmpPks, tmpLocs]=findpeaks(tmpPulse,tmpTime);
                %Find the peaks nearest to the expected ibi times
                expectedPeakPos=tmpTime(1):crtIBI:tmpTime(end);
                %The first index is already a peak, so start with 2
                expectedPeakPos=expectedPeakPos(2:end);
                %Add the missing peaks
                for idx=1:numel(expectedPeakPos)
                    [~, I]=min(abs(tmpLocs-expectedPeakPos(idx)));
                    pksRPPG=[pksRPPG(pruned_pksRPPG_loc<tmpTime(1)) ... 
                        tmpPks(I) pksRPPG(pruned_pksRPPG_loc>tmpTime(end))];
                    pruned_pksRPPG_loc=[pruned_pksRPPG_loc(pruned_pksRPPG_loc<=tmpTime(1)) ...
                        tmpLocs(I) pruned_pksRPPG_loc(pruned_pksRPPG_loc>=tmpTime(end))];
                    
                end
            end
            pind=pind+1;
        end
        
        pksrPPG_loc=pruned_pksRPPG_loc(~isnan(pruned_pksRPPG_loc));
        pksRPPG=pksRPPG(~isnan(pruned_pksRPPG_loc));
        pruned_pksRPPG_loc=pruned_pksRPPG_loc(~isnan(pruned_pksRPPG_loc));
        
        %Save the peak locations
        if(ibiInd==1)
            %At the first iteration save all the peaks
            finalPPs(ibiInd:ibiInd+length(pruned_pksRPPG_loc)-1)=pruned_pksRPPG_loc;
            ibiInd=ibiInd+length(pruned_pksRPPG_loc);
        else
            %Choose the peaks that arrive later  than the last of the 
            %previous peaks
            candidateLocs=pruned_pksRPPG_loc(pruned_pksRPPG_loc>max(prev_pruned_pksRPPG_loc));
            if length(candidateLocs)~=0
                finalPPs(ibiInd:ibiInd+length(candidateLocs)-1)=candidateLocs;
                ibiInd=ibiInd+length(candidateLocs);
            end
        end
        
%        finalPPs(ibiInd)=pruned_pksRPPG_loc(1);
        
%         plot(crtTimeWin,crtPulseWinFilt);
%         xlim([crtTimeWin(1) crtTimeWin(end)])
%         plot(crtTimeWin(loc),pksRPPG,'v');
%         line([crtTimeWin(1) crtTimeWin(end)],[mu mu],'color',color_rppg);
%         line([crtTimeWin(1) crtTimeWin(end)],[mu+sigma mu+sigma],'LineStyle','--');
%         line([crtTimeWin(1) crtTimeWin(end)],[mu-sigma mu-sigma],'LineStyle','--');
%         line([crtTimeWin(1) crtTimeWin(end)],[crtAvgHR/60 crtAvgHR/60],'color','red');
        prev_pruned_pksRPPG_loc=pksrPPG_loc;
    end
    
    if(useEcg)
    % ECG using Welch
        if(~USE_RR_ECG)
            [ECG_power,ECG_freq] = pwelch(crtEcgWin,[],[],length(crtEcgWin)*3,Fs_Ecg, 'psd');
            rangeECG = (ECG_freq>LOW_F & ECG_freq < UP_F); % frequency range to find PSD peak
            ECG_power = ECG_power(rangeECG);
            ECG_freq = ECG_freq(rangeECG);
            [pksECG,loc] = findpeaks(ECG_power,'SortStr','descend','NPeaks',4); 
            ECG_peaksLoc = ECG_freq(loc);
            HR_ECG(ind)=ECG_peaksLoc(1);
            
        else
            %Using RR intervals 
            [pksECG,pksECG_loc] = findpeaks(crtEcgWin,'MinPeakProminence',4); % get 2 first peaks
            RRs=diff(pksECG_loc);
            ECG_freq=Fs_Ecg/mean(RRs);
            HR_ECG(ind)=ECG_freq;
            finalRRs(ibiInd)=RRs(1);
        end
    end
        
    %ibiInd=ibiInd+1;    
    fig1=figure(1);
    clf(fig1);
   
    
    subplot(2,2,3);
    hold on;grid on;
    if(USE_PP_RPPG==1)
        plot(RPPG_freq,1,'Color',color_rppg,'Marker','*')
        line([RPPG_freq,RPPG_freq],[0,1],'LineWidth',2,'LineStyle','--');
        text(RPPG_freq+.2,.9,[ num2str(round(HR_RPPG(ind)*60))]);
        ylabel('BPM');
    else
        plot(rPPG_freq,rPPG_power);
        plot(rPPG_peaksLoc(1), pksrPPG(1), 'r*');
        text(rPPG_peaksLoc(1)*.9, pksrPPG(1),[num2str(round(HR_RPPG(ind)*60))],...
                'BackGroundColor',[.8 .2 .2 .7],'Color','white');
        HR_PP=1/mean(diff(pksrPPG_loc));
        line([HR_PP,HR_PP],[0,pksrPPG(1)],'LineWidth',2,'LineStyle','--');
        text(HR_PP-.2, pksrPPG(1)/2,[num2str(round(HR_PP*60)) '-pp'],...
            'BackGroundColor',[.2 .2 .2 .7],'Color','white');
        
        ylabel('BPM');
    end
    
    xlim([0.1 3.5]), title([ 'rPPG Welch periodogram' ]), xlabel('Heart Rate (Hz)');
    hold off;
    if(useEcg)
        subplot(2,2,4);
        hold on; grid on;    
        if(USE_RR_ECG==1)
            plot(ECG_freq,1,'Color',color_ecg,'Marker','*')
            line([ECG_freq,ECG_freq],[0,1],'LineWidth',2,'LineStyle','--');
            text(ECG_freq+.2,.9,[ num2str(round(HR_ECG(ind)*60)) ],...
                'BackGroundColor',[.8 .2 .2 .7],'Color','white');
            ylabel('BPM');
        else
            plot(ECG_freq,ECG_power);
            plot(ECG_peaksLoc(1), pksECG(1), 'Color',color_ecg,'Marker','*');
            text(ECG_peaksLoc(1),pksECG(1)*1.05,[ num2str(round(HR_ECG(ind)*60)) ],...
                'BackGroundColor',[.8 .2 .2 .7],'Color','white');
            ylabel('BPM');
        end
    end
    %ylim([0,1.1])
    xlim([0.1 3.5]), title('ECG HR using RR intervals'), xlabel('Heart Rate (Hz)');
    hold off;
 
    hold on;
    subplot(2,2,[1 2]);
    hold on;grid on;
    p1 = plot(crtTimeWin, crtPulseWinFilt,'Color',color_rppg,'LineWidth',1);
    if(useEcg)
        p2 = plot(crtTimeEcgWin, crtEcgWin,'Color',color_ecg);
        if USE_RR_ECG
            plot(crtTimeEcgWin(pksECG_loc),pksECG,'v');
        end
    end
        plot(pksrPPG_loc,pksRPPG,'v');
        legend('RPPG','ECG');
    
    xlim([crtTimeWin(1) crtTimeWin(end)])
    %title(['\color{blue}RPPG' '\color{black} vs ' '\color{red}ECG'])
    
    hold off;
    
    pause(.01)
end

finalPPs(ibiInd:ibiInd+numel(pksrPPG_loc)-2)=pksrPPG_loc(2:end);
if(useEcg)
    %finalRRs(ibiInd:ibiInd+numel(RRs)-2)=RRs(2:end); 
end
%Poincare plot
 finalRRs=finalRRs(finalRRs~=0);
finalPPs=finalPPs(finalPPs~=0);
HRV = diff(finalPPs);
save('HRV','HRV');
x = HRV;
x(end)=[];
y=HRV;
y(1)=[];


SD1 = std(x-y)/sqrt(2);
SD2 = std(x+y)/sqrt(2);
SDRR=sqrt(SD1^2+SD2^2)/sqrt(2);

if (1)
    % we should observe round shape when stress and elongated when zen
    figure, hold on;
    title('Poincare plot');
    xlabel('RR_i'), ylabel('RR_{i+1}');
    plot(x,y,'o');
    xlim([0.1 1.5]), ylim([0.1 1.5]);
    hold off;
end

fprintf('SD1 %.2f \n', SD1);
fprintf('SD2 %.2f \n', SD2);


fig8 = figure(8);
clf(fig8);
hold on;
grid on;
if(useEcg)
p1 = plot(time, HR_ECG*60,'Color',color_ecg,'LineStyle','-','Marker','.','MarkerSize',10);
p2 = plot(time, HR_RPPG*60,'Color',color_rppg,'LineStyle','-','Marker','.','MarkerSize',10);
end
legend('HR ECG', 'HR RPPG');
title('raw results HR estimation');
xlabel('seconds'), ylabel('Heart Rate (bpm)');
ylim([40 120]);
hold off;
pause(1);
