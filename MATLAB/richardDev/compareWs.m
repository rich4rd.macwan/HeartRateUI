function compareWs(isle2i)
close all;
if isle2i
    DOLE2I=true;
    DOIUT=false;
else
    DOLE2I=false;
    DOIUT=true;
end
WINDOW_LENGTH_SEC=30;
currentIndex=1;
suffix='skin';
filterSuffix='Unfilt';
if DOLE2I
    %Le2i dataset
    Le2iFolder='D:/LabData/Le2i\';
    %Le2iFolder='D:\Dataset\HeartRate\RichardSummerVideos\';
    dirs=dir([Le2iFolder '/*gt']);
    d=dir([Le2iFolder '/aft*']);
    dirs=[dirs;d];
    %Iterate through all directories
    while currentIndex<numel(dirs)
        
        
        
        %         ica=true;cica=true;
        %         if currentIndex==1
        %             ica=false; cica=false;
        %         end
        %Display figure of current video
        vidFolder=[Le2iFolder dirs(currentIndex).name '/'];
        %             openfig([vidFolder 'results/CICA_win' num2str(WINDOW_LENGTH_SEC) '_' suffix '_' filterSuffix '_Ws.fig']);
        %             set(gcf,'Name',dirs(currentIndex).name);
        figure
        title(dirs(currentIndex).name);
        currentIndex=currentIndex+1;
        pause(1)
        close all;
    end
end


if DOIUT
    iutVids=[1 3:27 30:49];
    
    IUTFolder='D:\LabData\IUT\';
    while currentIndex<numel(iutVids)
        %for i=[46]
        ind=iutVids(currentIndex);
        if ind<10
            vidFolder=[IUTFolder 'subject' num2str(0) num2str(ind) '/vid2/'];
            vidName=['subject' num2str(0) num2str(ind)];
        else
            vidFolder=[IUTFolder 'subject' num2str(ind) '/vid2/'];
            vidName=['subject' num2str(ind)];
        end
        resultFolder = [vidFolder 'results/'];
        
        ica=true;cica=true;
        currentIndex=currentIndex+1;
        
        %Display figure of current video
        openfig([vidFolder 'results/CICA_win' num2str(WINDOW_LENGTH_SEC) '_' suffix '_' filterSuffix '_Ws.fig']);
        title(['subject' num2str(0) num2str(ind)]);
        pause(1);
        close all;
        
    end
end
end
