function rankTest(crtRGB,crtTimeWin)
%Generate all possible Ws with norm 1
r=1;
theta=linspace(0,2*pi,50);
phi=linspace(0,pi,50);
[phi,theta]=meshgrid(phi,theta);
x=r*cos(theta).*sin(phi);
y=r*sin(theta).*sin(phi);
z=r*cos(phi);
Zeros=zeros(size(x));
ranks=zeros(size(x));
Ws=[x(:),y(:),z(:)];
%For all Ws, calculate rank of H(w'X)
n=floor(size(crtRGB,2)/2);
H = zeros (n);
N=size(Ws,1);
mixedmean=mean(crtRGB,2);
[ICnum, IClen]=size(crtRGB);
firstEig=1;
lastEig=ICnum;
interactivePCA='off';
verbose='on';
[E, D]=pcamat(crtRGB, firstEig, lastEig, interactivePCA, verbose);
% Calculate the whitening
[whitesig, whiteningMatrix, dewhiteningMatrix] = whitenv(crtRGB, E, D, verbose);
X=whitesig;
for i=1:N
    if mod(i,100)==0
        fprintf('%d of %d\n',i,N);
    end
    W=Ws(i,:);
    df=norm(W-[0.1530 0.9399 0.3054]);
     if df<.01
          for h=1:size(H,1)
              plot(H(h,:))
              pause(.01)
          end
     end
    Y=W*X+(W * mixedmean) * ones(1, IClen);
    Y=smooth(Y'/std(Y));
    Y=round(Y,1);
    for k =1: n
        H( k , : ) = Y(k:k +n-1);
    end
    ranks(i)=rank(H'*H);
    
end
ranks=reshape(ranks,size(x));
quiver3(Zeros,Zeros,Zeros,x,y,ranks,'MaxHeadSize',.01);
axis equal
end

