function [y, wout] = GAMulti(crtRGB, t)
%Genetic algorithm based multi object optimization
% X are the RGB traces
% y is the selected signal obtained by wout'*X

%Save X in the workspace so that it can be read in the objective func
assignin('base','crtRGB',crtRGB);
assignin('base','Fs',1/mean(diff(t)));
w=randn(3,1);
w=w/norm(w);
[Ws,fval]=gamultiobj(@negentropy_autocorr,numel(w));
optimIndex=optimFromPareto(Ws,crtRGB,t);
wout=Ws(optimIndex,:);
y=wout*crtRGB;
