function [ y2 ] = adjustSignalLength(x1,y1,x2)
%Adjust the length of PPG signal y1 with time points x1, to a signal with
%time points x2
        %Adjust pulse so that the first and last values are same
        %First offset x1 so that both have the same starting point
%         x1=x1+(x2(1)-x1(1));
%         residue=(x2(end)-x1(end));
%         nRes=numel(x1);
%         pieceWiseResidue=residue/nRes;
%         
%         stretchedPulseTime=x1+(0:nRes-1)*pieceWiseResidue;
        stretchedPulseTime=linspace(x2(1),x2(end),numel(x1));
        
        y2=interp1(stretchedPulseTime,y1,x2,'pchip');
end

