function [excel_row]=ica_rppg(varargin)

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
disp('Testing ICA with RPPG');
%Load rppg traces

% vidFolder='/run/media/richard/Le2iHDD/IUT/subject48/vid2/';
% %vidFolder='D:\Dataset\HeartRate\RichardSummerVideos\5-gt\';
% 
% matFileName = 'pulseTraceRGB'; % input file
% matFileName = 'pulseTraceRGB_crop'; % input file
% matFileName = 'pulseTraceRGB_skin'; % input file
% 
% 
i = 1;
IUTMode=false;
silentMode=true;
func='negentropy';%also negentropy
% while(nargin > i)
%     if(strcmp(varargin{i},'vidFolder')==1)
%         vidFolder=varargin{i+1};
%     end
%     if(strcmp(varargin{i},'matFileName')==1)
%         matFileName=varargin{i+1};
%     end
%     if(strcmp(varargin{i},'IUTMode')==1)
%         IUTMode=varargin{i+1};
%     end
%     if(strcmp(varargin{i},'silent')==1)
%         silentMode=varargin{i+1};
%     end
%     if(strcmp(varargin{i},'func')==1)
%         func=varargin{i+1};
%     end
%     if(strcmp(varargin{i},'DOFILTER')==1)
%         DOFILTER=varargin{i+1};
%     end
%     i = i+2;
% end
% suffix=matFileName(end-3:end);
% if strcmp(suffix,'skin')==0 && strcmp(suffix,'crop')==0
%     suffix='';
% end
% if ~DOFILTER
%     filterSuffix='_Unfilt';
% else
%     filterSuffix='';
% end



% disp(['Starting ICA with  ' vidFolder matFileName filterSuffix '.mat']);
% if(exist([vidFolder matFileName filterSuffix '.mat'], 'file') == 2)
%     load([vidFolder matFileName filterSuffix '.mat']);
% else
%     disp('oops, no input file');
%     return;
% end

%Plot the traces
%hold on
%for i=1:3
%    plot(timeTrace,pulseTraceRGB(:,i));
%end
% if exist('pulseTrace','var')
%     pulseTraceRGB = pulseTrace;
% end
%Perform ICA on the RGB traces
% traces=pulseTraceRGB';
% addpath('../tools/ICA/');
% setPath_ICA('../tools/ICA/');
% if(PLOT_ON)
%     ICAshow(traces,'title','Source RGB signals');
% end
%Convert to unit variance if it isn't already
% traces=standardize(traces);
% N=size(traces,2);
% MixedSig=traces;
N=500;
v=[0:N-1];
%sig(1,:)=sin(v/2);   % ????????

S1=sin(v/2);
%S2=awgn(S1,10,'measured');
%S2=((rem(v,23)-11)/9).^5;
S2=sawtooth(v/2);
% figure('Name','Original');
% subplot(2,1,1)
% plot(S1);
% subplot(2,1,2)
% plot(S2);

S1=awgn(S1,30,'measured');
S2=awgn(S2,2,'measured');

%S3=((rand(1,N)<.5)*2-1).*log(rand(1,N)); 
A=[.4 .6;.8 .2];
%A=[.5 .5;.5 .5];


sig=A*[S1;S2];


%sig(2,:)=((rand(1,N)<.5)*2-1).*log(rand(1,N)); 
% 
figure('Name','Mixed Signal');
subplot(2,1,1)
plot(sig(1,:));
subplot(2,1,2)
plot(sig(2,:));
MixedSig=sig;

mu0 = 1;
lambda0 = 1;
gamma = 1;
learningRate = 1;
OverValue=1e-4;  maxIter = 400;
threshold =20;   % good ref
w = rand(size(MixedSig,1),1);
w=w/norm(w);
addpath('../tools/cICA/');


[Sig,W,initialA,SelectedSignal]=simpleICA(MixedSig,maxIter,OverValue,'choosebestcomponent',true,'func',func);
%[Sig,WeightMatrix]=FastICA(WhitenedSig,'method','symm','nonlinfunc','tanH');



%[Sig, W] = cICAM(MixedSig, timeTrace, threshold, w, learningRate, mu0, lambda0, gamma, maxIter, OverValue,[]);
% expectedW=inv(A);
% figure(8);
% hold on
% quiver(0,0,expectedW(1,1),expectedW(1,2));
% quiver(0,0,expectedW(2,1),expectedW(2,2));
% xlim([-1.2,1.2]);
% ylim([-1.2,1.2]);
% 
% [Sig,W,initialA,SelectedSignal]=simpleICA(MixedSig,maxIter,OverValue,'choosebestcomponent',true,'func',func);
%[Sig,W,finalA,SelectedSignal]=simpleICA(MixedSig,maxIter,OverValue,'initGuess',initialA);
%W
%expectedW
%expectedW=expectedW(1,:)
% if(PLOT_ON)
%     figure;  
%     subplot(3,1,3); 
%         plot(Sig(3,:)); ylabel('y3');
%         axis([-inf,inf,-5,5]);
%     subplot(3,1,2); 
%         plot(Sig(2,:)); ylabel('y2');
%         axis([-inf,inf,-5,5]);
%     subplot(3,1,1); 
%         plot(Sig(1,:)); ylabel('y1');
%         title('\bf separated sources');
%         axis([-inf,inf,-5,5]);
% end
%Choose the component manually(visual inspection) for now

%ICAshow(Sig,'title','Separated')

 %figure
% hold on
 %plot(SelectedSignal)
% plot(sig(2,:));
% legend('Separated','Original');
% 
% pulseTrace=SelectedSignal;
% 
% [gtTrace, gtHR, gtTime] = loadPPG(vidFolder);  
% gtTime = gtTime / 1000;  % in seconds  
% 
% % figure
% % hold on
% % p1 = plot(gtTime, gtTrace, 'b', 'Linewidth', 1);  
% % p2 = plot(timeTrace, pulseTraceRGB(:,2), 'Color',[.4,.8,.4], 'Linewidth', 1.5);  
% % % p3 = plot(timeTrace, pulseTraceRGB(:,3), 'Color',[.4,.4,.8],'Linewidth', 1.5);  
% % % p4 = plot(timeTrace, pulseTraceRGB(:,1), 'Color',[.8,.4,.4], 'Linewidth', 1.5);  
% % p5 = plot(timeTrace, pulseTrace, 'Color',[.4,.4,.4], 'Linewidth', 1.5);  
% addpath('../main-stable');
%  if isempty(suffix)
%      save([vidFolder 'pulseTraceICA' filterSuffix],'pulseTrace','timeTrace');
%  else
%      save([vidFolder 'pulseTraceICA' '_' suffix filterSuffix],'pulseTrace','timeTrace');
%  end
% 
% % 
% % %Pass the selected component to the HR calculation routine
% %  currentDir=cd;
% %  %cd('../main-stable/');
% fileName='pulseTraceICA';
% if ~isempty(suffix)
%     fileName=['pulseTraceICA' '_' suffix];
% end
% 
% [HR_sensor,HR_PPG,HR_RPPG,HR_RPPG_filtered,excel_row]=getHRFromPulse...
%      ('vidFolder',vidFolder,'matFileName',fileName,'IUTMode',IUTMode,...
%      'silent',silentMode,'ICAWIN',false,'DOFILTER',DOFILTER);
%  
%  if isempty(suffix)
%      save([vidFolder 'pulseTraceICA' filterSuffix],'pulseTrace','timeTrace',...
%          'HR_sensor','HR_PPG','HR_RPPG','HR_RPPG_filtered');
%  else
%      save([vidFolder 'pulseTraceICA' '_' suffix filterSuffix],'pulseTrace','timeTrace',...
%          'HR_sensor','HR_PPG','HR_RPPG','HR_RPPG_filtered');
%  end
%  %cd(currentDir);
% %  close all;
%  end
