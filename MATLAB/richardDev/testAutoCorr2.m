global aind;
vidFolder ='D:/Le2i/5-gt/';
matFileName = 'rgbTraces_skin';
WINDOW_LENGTH_SEC=30;
STEP_SEC = 0.5; % step between 2 sliding window position (in seconds)
LOW_F=.7; % low freq for BP filtering
UP_F=3; % high freq for BP filtering
FILTER_ORDER = 8; % Order of the butterworth BP filter

disp(['Get Pulse from ' vidFolder matFileName '.mat']);

if(exist([vidFolder matFileName '.mat'], 'file') == 2)
    load([vidFolder matFileName '.mat']);
else
    disp('oops, no input file');
    return;
end
% load PPG
[gtTrace, gtHR, gtTime] = loadPPG(vidFolder);
gtTime = gtTime / 1000;  % in seconds

% remove 0s if any (happens for example on CHU data)
nTraces=3;
%rgbTraces=[traces(1:nTraces,:);traces(end,:)];
rgbTraces(:,rgbTraces(nTraces+1,:)<=0)=[];

% just for bodySync v1 (duplicate images)
% rgbTraces=rgbTraces(:,1:2:end);

% get current traces
timeTrace = rgbTraces(end,:);
timeTrace = timeTrace/1000; % in second
crtTrace = rgbTraces(1:nTraces,:);
traceSize = size(crtTrace,2);

% Uniform sampling
timeTraceUnif = linspace(timeTrace(1), timeTrace(end), traceSize);
for k=1:nTraces
    crtTrace(k,:) = interp1(timeTrace,crtTrace(k,:), timeTraceUnif);
end
timeTrace = timeTraceUnif;

% get exact Fs
Fs = 1/mean(diff(timeTrace));

% get window length in frames
winLength = round(WINDOW_LENGTH_SEC*Fs);
step = round(STEP_SEC*Fs);

% get coef of butterworth filter
[b, a]= butter(FILTER_ORDER,[LOW_F UP_F]/(Fs/2));
%Hamming
initialA=[];
w0=rand(3,1);
w0=w0/norm(w0);

halfWin = (winLength/2);

pulseTraceGreen=zeros(1,traceSize);
pulseTraceBlue=zeros(1,traceSize);
pulseTraceRed=zeros(1,traceSize);
pulseTraceChrom=zeros(1,traceSize);
pulseTracePCA=zeros(1,traceSize);
ind=1;
for i=halfWin:step:traceSize-halfWin
    
    % get start/end index of current window
    startInd = i-halfWin+1;
    endInd = i+halfWin;
    
    % get current trace window
    crtTraceWin = crtTrace(1:nTraces,startInd:endInd);
    crtTimeWin = timeTrace(startInd:endInd);
    (crtTimeWin(1)+crtTimeWin(end))/2;
    
    % detrend
    for k=1:size(crtTraceWin,1)
        crtTraceWin(k,:) = detrendsignal(crtTraceWin(k,:)')';
    end
    % normalize each signal to have zero mean and also unit variance
    crtTraceWin = crtTraceWin - repmat(mean(crtTraceWin,2),[1 winLength]);
    crtTraceWin = crtTraceWin ./ repmat(std(crtTraceWin,0,2),[1 winLength]);
    figure(1);
    subplot(311);
    plot(crtTraceWin(1,:));
    subplot(312);
    plot(crtTraceWin(2,:));
    subplot(313);
    plot(crtTraceWin(3,:));
    
    
    A = [];
    b = [];
    Aeq = [];
    beq = [];
    lb = [];
    ub = [];
    nonlcon = [];
    options = optimoptions(@fmincon,'SpecifyObjectiveGradient',true,'Display','iter-detailed');
    X=crtTraceWin;
    objf=@(w)objAutocorr(w,X,Fs,ind);
    figure(42);
%     cla;
    aind=ind;
    w = fmincon(objf,w0,A,b,Aeq,beq,lb,ub,nonlcon,options)
%     figure(2);
%     plot(w'*X);
     pause(.01);
    ind=ind+1;
end

