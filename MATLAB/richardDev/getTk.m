function [Tk]=getTk(N,k)
    Tk=sparse([zeros(N-k,k) eye(N-k);zeros(k,N)]);
end
