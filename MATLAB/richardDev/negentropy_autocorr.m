function M = negentropy_autocorr(w)
% Computes the negentropy and autocorrelation for the given w
X=evalin('base','crtRGB');
Fs=evalin('base','Fs');
%Calculate negentropy
y=w*X;
std_y = std(y);                          
v_Gaus = normrnd(0, std_y, 1, numel(y));

M(1)=(mean(log(cosh(w*X)))-mean(log(cosh(v_Gaus)))).^2;

%Calculate autocorrelation
[R,~,~]=autocorrd(y,X,Fs);
M(2)=mean(R.^R);
end