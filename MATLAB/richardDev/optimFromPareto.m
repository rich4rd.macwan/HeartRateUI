function [selectedIndex]=optimFromPareto(Ws,crtRGB,t)
%     figure(42);
    LOW_F=.7; UP_F=3;
    maxsnr=0;
    for i=1:size(Ws,1)
       % subplot(2,2,1:2);
        y=Ws(i,:)*crtRGB;
        %plot(t,y);
        [rPPG_power,rPPG_freq] = pwelch(y,[],[],length(y)*3,1/mean(diff(t)));
        rangeRPPG = (rPPG_freq>LOW_F & rPPG_freq < UP_F);
        rPPG_power = rPPG_power(rangeRPPG);
        rPPG_freq = rPPG_freq(rangeRPPG);
        %subplot(2,2,3:4);
        [pks,locs]=findpeaks(rPPG_power,60*rPPG_freq,'SortStr','descend','NPeaks',2);
        %findpeaks(rPPG_power,60*rPPG_freq,'SortStr','descend','NPeaks',2);
        
        N = length(y)*3;
        Fs=1/mean(diff(t));
        
        sig_freq = [0 : N-1]*Fs/N;
        sig_power = abs(fft(y,N)).^2;
        
        %         fig3 = figure(3);
        %         clf(fig3);
        %         plot(sig_freq, sig_power), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
        %         title('rPPG FFT')
        
        range = (sig_freq>LOW_F & sig_freq < UP_F);   % frequency range to find PSD peak
        sig_power = sig_power(range);
        sig_freq = sig_freq(range);
        [sig_peakPower,loc] = max(sig_power); % findpeaks(sig_power,'SortStr','descend','NPeaks',1); % get first peak
        sig_peakLoc = sig_freq(loc);
        WIDTH = 0.1; 
        range = (sig_freq>(sig_peakLoc-WIDTH/2) & sig_freq < (sig_peakLoc+WIDTH/2))';
        signal = sig_power.*range';
        noise = sig_power.*(~range');
       
    
        n = sum(noise);
        s = sum(signal);
        snr = s/n;
        %text(locs(1),0,['SNR = ' num2str(snr)],'BackGroundColor',[.2,.4,.8,.8]);
        if snr>maxsnr
            maxsnr=snr;
            selectedIndex=i;
        end
        
%         pause
    end
end