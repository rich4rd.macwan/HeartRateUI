function [sorig,salign]=align_signals(s1,s2)
%sz = 300;
%sg = randn(1,randi(8)+3);
%sg=sin(linspace(0,10));
%s1 = [zeros(1,randi(sz)-1) sg zeros(1,randi(sz)-1)];
%s2 = [zeros(1,randi(sz)-1) sg zeros(1,randi(sz)-1)]*.5;

mx = max(numel(s1),numel(s2));

% ax(1) = subplot(2,1,1);
% stem(s1)
% xlim([0 mx+1])

% ax(2) = subplot(2,1,2);
% stem(s2,'*')
% xlim([0 mx+1])

if numel(s1) > numel(s2)
    slong = s1;
    sshort = s2;
else
    slong = s2;
    sshort = s1;
end

[acor,lag] = xcorr(slong,sshort);

[acormax,I] = max(abs(acor));
lagDiff = lag(I);

% figure
% stem(lag,acor)
% hold on
% plot(lagDiff,acormax,'*')
% hold off

if lagDiff > 0
    sorig = sshort;
    %salign = slong(lagDiff+1:end);
    salign=[slong(lagDiff+1:end) slong(1:lagDiff)];
else
    salign =[slong(-lagDiff+1:end) slong(1:-lagDiff)];
    %salign = sshort(-lagDiff+1:end);
    %sorig=[sshort(-lagDiff+1:end) sshort(1:-lagDiff)];
    sorig=sshort;
end

% subplot(2,1,1)
% stem(sorig)
% xlim([0 mx+1])
% 
% subplot(2,1,2)
% stem(salign,'*')
% xlim([0 mx+1])