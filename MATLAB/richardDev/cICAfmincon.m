function [y, wout,A, selectedIndex] = cICAfmincon(X, t, threshold, w0, learningRate, mu0, lambda0, gamma, maxIter, OverValue,onlyAutocorr)
% Multiobjective constraint optimization using Lagrange multipliers
%
% Command:
%   [y, w] = cICAfmincon(X, t, threshold, w0, learningRate, mu0, lambda0, gamma, maxIter, OverValue)??
%
% Parameters:
%            y --- extracted source signal, according to the given optimal time delay.
%            w --- corresponding weight vector, that is, y=w'*X.
%            X --- prewhitened observed mixed signals. Each row is a mixed signal.
%          ref --- reference signal, which can be generated by function genRectangleRef
%                  and genPulseRef
%    threshold --- the closeness of the desired signal and the reference signal is less than
%                  or equal to the threshold, i.e., closeness(y,ref) - threshold <= 0
%           w0 --- the initial weight vector
% learningRate --- learning rate of the weight w
%          mu0 --- the Lagrange multiplier for constraint g(w)
%      lambda0 --- the Lagrange multiplier for constraint h(w)
%        gamma --- the scalar penalty parameter
%  OverValue --- stopping criterion, say, 0.0001. When the changed value
%                of weighted vector is less than it, then the algorithm stops
%    maxIter --- maximum iterations for estimating each independent component


fprintf('Starting constrained optimization for extracting the desired source signal ..\n');


mu = mu0;
lambda = lambda0;
convergedFlag = 1;
loop = 1;
[ICnum, IClen]=size(X);
vectorSize=ICnum;
addpath('../tools/FastICA');
firstEig=1;
lastEig=ICnum;
interactivePCA='off';
verbose='on';

mixedsig=X;
mixedmean=mean(X,2);
%CHROM constraint
Cmat=[3,-2,0;1.5,1,-1.5];
S=Cmat*X;
alpha=std(S(1,:))/std(S(2,:));
Wchrom=([1 -alpha]*Cmat)';


if ~onlyAutocorr
    [E, D]=pcamat(X, firstEig, lastEig, interactivePCA, verbose);
    % Calculate the whitening
    [whitesig, whiteningMatrix, dewhiteningMatrix] = whitenv(X, E, D, verbose);
    X=whitesig;
end
    
if size(X,1)==1
    wout=w0;
    y=X;
    convergedFlag=0;
    return;
end
% w=whiteningMatrix*w0;
% w=w/norm(w);
% oldw=w;
ICnum=size(X,1);
if ICnum<vectorSize
    fprintf(2,'Dimensions reduced to %d !\n',ICnum);
    vectorSize=ICnum;
    initialStateMode=0;
end
w=w0;
oldw=w;
% compute the autocorrelation matrix Rxx
Rxx = X(:,1:end) * X(:,1:end)' / IClen;


%W constraints, maximize these differences
%Nonlinear Inequality constraints
%|w2|-|w3|>zeta

Fs=1/mean(diff(t));
zeta=[5000,.2];
% zeta=.2;



%Anonymous functions for passing extra data to fmincon

objf=@(w)objcICA(w,X);
hessf=@(w,lambda)hessiancICA(w,X,lambda,Fs,Wchrom);
conf=@(w)concICA(w,X,zeta,Fs,Wchrom);
options = optimoptions(@fmincon,'Algorithm','interior-point',...
        'Display','off','SpecifyObjectiveGradient',true,'SpecifyConstraintGradient',true,...
        'HessianFcn',hessf);
[w,y,mflag,output] = fmincon(objf,w,...
           [],[],[],[],[],[],conf,options)
       
       
%y = w'* X;
% output

if ~onlyAutocorr
    W=w'*whiteningMatrix;
    A=dewhiteningMatrix*w;
else
    W=w';
    A=w';
end
y =  W * mixedsig + (W * mixedmean) * ones(1, IClen);
result=y';
selectedSig=[];
selectedIndex=0;
if true
    LOW_F=0.7; % low freq for BP filtering
    UP_F=3; % high freq for BP filtering
    WIDTH = 0.1; % Width for the door step function to compute SNR (in Hz)
    
    maxsnr = 0;
    %figure('units','normalized','outerposition',[0 0 1 1]);hold on;
    for index=1:size(result,2)
        
        x = result(:,index);
        N = length(x)*3;
        sig_freq = [0 : N-1]*Fs/N;
        sig_power = abs(fft(x,N)).^2;
        
        %         fig3 = figure(3);
        %         clf(fig3);
        %         plot(sig_freq, sig_power), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
        %         title('rPPG FFT')
        
        range = (sig_freq>LOW_F & sig_freq < UP_F);   % frequency range to find PSD peak
        sig_power = sig_power(range);
        sig_freq = sig_freq(range);
        [sig_peakPower,loc] = max(sig_power); % findpeaks(sig_power,'SortStr','descend','NPeaks',1); % get first peak
        sig_peakLoc = sig_freq(loc);
        peaksPower(index) = sig_peakPower;
        
        range = (sig_freq>(sig_peakLoc-WIDTH/2) & sig_freq < (sig_peakLoc+WIDTH/2))';
        signal = sig_power.*range;
        noise = sig_power.*(~range);
       

%         

    
    
        n = sum(noise);
        s = sum(signal);
        snr = s/n;
        
        %[G,g1,g2]=autocorrd(x',result',1/mean(diff(t)));
%         G(1:round(0.7*30))=0;
%         G(3*30:end)=0;
        %Gmean=mean(G.*G);
        
%         subplot(1,3,index),plot(sig_freq, signal, 'r'), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');hold on;
%         subplot(1,3,index),plot(sig_freq, noise, 'b'), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
%         title('rPPG FFT')
%         annotation('textbox',[0.2*(index+0.1) 0.5 0.2 0.3], 'String',['snr=' num2str(snr) 'Gmean=' num2str(Gmean)],'FitBoxToText','on');
%         hold off;
                
        
        if snr>maxsnr
            maxsnr=snr;
            selectedSig=x';
            selectedIndex=index;
        end
    end
    
    selectedSig=selectedSig';
    if selectedSig(1)>0
        selectedSig=selectedSig*(-1);
    else
    end
end
%Dewhitened separation matrix
wout=w;
y=selectedSig;
fprintf('End of multiobjective optimization!\n');

