function [f,g,H]=objAutocorr(w,X,Fs,ind) 
    global aind;
    y=w'*X;
    [r,r1,r2]=autocorrd(y,X,Fs);
    f=-mean(r.*r);
    g=r1;
    H=r2;
    figure(42);
    hold on
    plot(aind,-f,'x');
    aind=aind+1;
end
