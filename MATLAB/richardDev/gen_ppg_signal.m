function [ signal ] = gen_ppg_signal( fitresult, varargin)
%% Generate a PPG signal based on the time_limits, cardiac_freq and 
% the fourier model
%fitresult is the structure containing the fourier model (fmodel) and the
%time period (pulseTime)
% Options: 
%'cardiac_freq': between .3 and 3, NOT in bpm
%'time_trace': The time trace for the original signal
%'plot-on' : 0 means no plots

cardiac_freq=1; %Default frequency of 60bpm
time_limits=[0 5]; %Default signal duration in seconds
argi=1;
nargin=length(varargin);
LOW_F=.3; % low freq for PSD range
UP_F=3; % high freq for PSD range
plot_on=0;

%Same sampling as gen_ppg_model
Fs = 256;
inc=1/Fs;
time_trace=time_limits(1):inc:time_limits(2);
addpath('../tools/cICA');
while argi<nargin
    if(strcmp(varargin{argi},'cardiac_freq')==1)
        if varargin{argi+1}>UP_F || varargin{argi+1}<LOW_F 
            warning('Cardiac Frequency beyond normal human limits!');
        end
        if(numel(varargin{argi+1})==1)
            cardiac_freq=varargin{argi+1};
        else
            error('cardiac_freq needs to be a scalar');    
        end
        
    end
    if(strcmp(varargin{argi},'time_trace')==1)
            time_trace=varargin{argi+1};
    end
    if(strcmp(varargin{argi},'plot_on')==1)
        if(numel(varargin{argi+1})==1)
            plot_on=varargin{argi};
        else
            error('plot_on needs to be a scalar, 0 or 1');
        end
    end
    argi=argi+2;
end


total_time=time_trace;
inc=mean(diff(total_time));
desiredPulseTime=0:inc:1/cardiac_freq;
nPulses=floor((total_time(end)-total_time(1))/desiredPulseTime(end));


%Create the adjusted pulse for the given duration
%no of pulses=duration of total_time/duration of pulse
multiplier=floor((total_time(end)-total_time(1))/desiredPulseTime(end));

residue=total_time(end)-total_time(1)-multiplier*desiredPulseTime(end);
residueOld=residue*fitresult.pulseTime(end)/desiredPulseTime(end);
%Obtain pulse from the model: created by multiplier pulses followed by the
%residue pulse
incOld=mean(diff(fitresult.pulseTime));
oldTime=0:inc:multiplier*fitresult.pulseTime(end)+residueOld;
%Offset oldTime by the required amount
oldTime=oldTime+total_time(1);
%pulse=feval(fitresult.fmodel,oldTime);
pulse=gen_ppg_pulse(fitresult,oldTime,desiredPulseTime);


%Concatenate the pulse for this time series

signal=repmat(pulse,[1 nPulses]);
%inc=mean(diff(fitresult.pulseTime));
%pulse=adjustSignalLength(time(1)+inc*(0:numel(pulse)-1),pulse,time);

%Adjust it's length and make it unit variance and wero mean
signalTime=(0:numel(signal)-1)*inc;
signal=adjustSignalLength(signalTime,signal,total_time);
signal=standardize(signal);
if(plot_on)
    hold on
    plot(oldTime,feval(fitresult.fmodel,oldTime));
    plot(total_time,signal);
end

end

