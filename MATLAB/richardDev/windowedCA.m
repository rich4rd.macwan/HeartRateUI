function [excel_row]=windowedCA(varargin)
% preprocess RGB trace to get pulse trace
% Green method: sliding window, detrend, band-pass on green channel
% Chrom method: "Robust Pulse Rate From Chrominance-Based rPPG" Haan et al. 2013
% yannick.benezeth@u-bourgogne.fr
% 17/02/2016

addpath('../main-stable');
addpath('../main-stable/PCA');

disp('preprocess RGB trace to get pulse trace');

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
WINDOW_LENGTH_SEC = 20; % length of the sliding window (in seconds) (use -1 to work on the whole signal at once)
STEP_SEC = .5; % step between 2 sliding window position (in seconds)
LOW_F=.7; % low freq for BP filtering
UP_F=3; % high freq for BP filtering
FILTER_ORDER = 4; % Order of the butterworth BP filter
SHOWPLOTS = 0;

% vidFolder='/home/richard/rppgVideos/testvideo/';
% outFolder='/home/richard/rppgVideos/testvideo/';
% vidFolder='./data/RichardSummer2015/5-gt/';
% outFolder='./data/RichardSummer2015/5-gt/';
vidFolder='/run/media/richard/Le2iHDD/IUT/subject20/vid2/'; 
%outFolder='/run/media/richard/Le2iHDD/Le2i/11-gt/';

matFileName = 'rgbTraces'; % input file
% matFileName = 'rgbTraces_crop'; % input file
%matFileName = 'rgbTraces_skin'; % input file
method='CICA'; %Can be cICA or ICA

i = 1;
silentMode=false;
while(nargin > i)
    if(strcmp(varargin{i},'vidFolder')==1)
        vidFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'matFileName')==1)
        matFileName=varargin{i+1};
    end
    if(strcmp(varargin{i},'method')==1)
        method=varargin{i+1};
    end
    if(strcmp(varargin{i},'outFolder')==1)
        outFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'WINDOW_LENGTH_SEC')==1)
        WINDOW_LENGTH_SEC=varargin{i+1};
    end
    if(strcmp(varargin{i},'FILTER_ORDER')==1)
        FILTER_ORDER=varargin{i+1};
    end
    if(strcmp(varargin{i},'keyword')==1)
        matFileName=['rgbTraces_' varargin{i+1}];
    end
    if(strcmp(varargin{i},'method')==1)
        method=varargin{i+1};
    end
    if(strcmp(varargin{i},'silentMode')==1)
        silentMode=varargin{i+1};
    end
    i = i+2;
end
outFolder=vidFolder;



if strcmp(matFileName(end-3:end),'crop')==1 || strcmp(matFileName(end-3:end),'skin')==1
    suffix=matFileName(end-3:end);
else
    suffix='';
end

i = 1;
initialA=[];
disp(WINDOW_LENGTH_SEC);
disp(FILTER_ORDER);
disp(outFolder);
disp(['Working with  ' vidFolder matFileName '.mat']);

if(exist([vidFolder matFileName '.mat'], 'file') == 2)
    load([vidFolder matFileName '.mat']);
else
    disp('oops, no input file');
    return;
end

%PArameters for cICA
mu0 = 1;
lambda0 = 5;
gamma = 5;
learningRate = 5;
OverValue=1e-6;  maxIter = 100;
othreshold = 7;
prevY=[];

%Kalman heuristic parameters
isTrackInitialized=false;
trendTolerance=10; %Size of window to calculate trend and trend difference
threshold=7;

% load PPG
[gtTrace, gtHR, gtTime] = loadPPG(vidFolder);
gtTime = gtTime / 1000;  % in seconds

% remove 0s if any (happens for example on CHU data)
nTraces=3;
%rgbTraces=[traces(1:nTraces,:);traces(end,:)];
rgbTraces(:,rgbTraces(nTraces+1,:)<=0)=[];
%for cICA
%get the random vector in the plane that is common to a majority of Ws
load('Wplane');
position=[0;0;0];
normal=[p10;p10;-1];

w=getRandomPointInPlane(position,normal,1);
%w = rand(nTraces,1);w=w/norm(w);
% just for bodySync v1 (duplicate images)
% rgbTraces=rgbTraces(:,1:2:end);

% get current traces
timeTrace = rgbTraces(end,:);
timeTrace = timeTrace/1000; % in second
crtTrace = rgbTraces(1:nTraces,:);
traceSize = size(crtTrace,2);
pulseTrace=zeros(1,traceSize);

% Uniform sampling
timeTraceUnif = linspace(timeTrace(1), timeTrace(end), traceSize);

%Switches for IUT and Le2I videos
if ~exist('nameFolder','var')
    %Extract nameFolder from the vidFolder
    tokens=strsplit(vidFolder,'/');
    if strcmp(tokens{end},'')==1
        tokens=tokens(1:end-1);
    end
    if ~isempty(tokens{end})
        tend=tokens{end};
        if numel(tend)==4 && strcmp(tend(1:3),'vid')==1
            nameFolder=[tokens{end-1} '/' tokens{end}];
            gtTraceInterp=gtTrace;
        else
            nameFolder=tokens{end};
            gtTraceInterp=interp1(gtTime,gtTrace,timeTraceUnif);
        end
        
    else
        %nameFolder=tokens{end-1};
        %For IUT videos
        nameFolder=[tokens{end-2} '/' tokens{end-1}];
        gtTraceInterp=gtTrace;
    end
end



for k=1:nTraces
    crtTrace(k,:) = interp1(timeTrace,crtTrace(k,:), timeTraceUnif);
end
timeTrace = timeTraceUnif;

% get exact Fs
Fs = 1/mean(diff(timeTrace));

% get window length in frames
winLength = round(WINDOW_LENGTH_SEC*Fs);
step = round(STEP_SEC*Fs);

% get coef of butterworth filter
[b, a]= butter(FILTER_ORDER,[LOW_F UP_F]/(Fs/2));
if ~silentMode
    fig5 = figure('Name',[vidFolder(end-14:end) suffix]);
    subplot(2,2,3:4);
    grid on;
    legend('HR sensor', 'HR PPG', 'HR rPPG', 'HR rPPG filtered');
    title('raw results HR estimation');
    title('capture 3');
    xlabel('seconds'), ylabel('Heart Rate (bpm)');
end

if(WINDOW_LENGTH_SEC == -1)
    % detrend and filter the whole signal (no sliding window)
    % detrend the whole signal (in case we can work on the whole signal)
    for k=1:3
        crtTrace(k,:) = detrendsignal(crtTrace(k,:)')';
    end
    % filter the whole signal (in case we can work on the whole signal)
    for k=1:3
        crtTrace(k,:) = filter(b,a,crtTrace(k,:)')';
    end
    
    pulseTraceGreen = crtTrace(2,:);
    
    Rf = crtTrace(1,:);
    Gf = crtTrace(2,:);
    Bf = crtTrace(3,:);
    Xf = 3*Rf-2*Gf;
    Yf = 1.5*Rf+Gf-1.5*Bf;
    alpha = std(Xf)/std(Yf);
    pulseTraceChrom = Xf - alpha*Yf;
else
    
    halfWin = (winLength/2);
    
    pulseTraceGreen=zeros(1,traceSize);
    pulseTraceBlue=zeros(1,traceSize);
    pulseTraceRed=zeros(1,traceSize);
    pulseTraceChrom=zeros(1,traceSize);
    ind=1;
    N=numel(halfWin:step:traceSize-halfWin);
    time=zeros(1,N);
    HR_PPG=zeros(1,N);
    HR_RPPG=zeros(1,N);
    HR_RPPG_filtered=zeros(1,N);
    for i=halfWin:step:traceSize-halfWin
        
        %     if(mod(i,10)==0)
        %         disp(['processing window ' num2str(i) ' on ' num2str(traceSize-winLength)]);
        %     end
        %
        % get start/end index of current window
        startInd = i-halfWin+1;
        endInd = i+halfWin;
        startTime = timeTrace(startInd);
        endTime = timeTrace(endInd);
        
        % get current trace window
        crtTraceWin = crtTrace(1:nTraces,startInd:endInd);
        crtTraceGT=gtTraceInterp(startInd:endInd);
        crtTimeWin = timeTrace(startInd:endInd);
        
        if(SHOWPLOTS)
            figure(1), plot(crtTimeWin, crtTraceWin(2,:)),title('raw green trace'), xlabel('seconds'), ylabel('pixel values');
        end
        
        % normalize each signal to have zero mean and also unit variance
        %crtTraceWin = crtTraceWin - repmat(mean(crtTraceWin,2),[1 winLength]);
        %crtTraceWin = crtTraceWin ./ repmat(std(crtTraceWin,0,2),[1 winLength]);
        
        crtTraceWin = crtTraceWin ./ repmat(mean(crtTraceWin,2),[1 winLength]);
        
        if(SHOWPLOTS)
            fig2 = figure(2);
            clf(fig2);
            hold on;
            p1 = plot(crtTimeWin, crtTraceWin(1,:),'r');
            p2 = plot(crtTimeWin, crtTraceWin(2,:),'g');
            p3 = plot(crtTimeWin, crtTraceWin(3,:),'b');
            title('normalized traces'), xlabel('seconds'), ylabel('normalized pixel values');
            hold off;
        end
        
        % detrend
        for k=1:size(crtTraceWin,1)
            crtTraceWin(k,:) = detrendsignal(crtTraceWin(k,:)')';
        end
        if(SHOWPLOTS)
            figure(3), plot(crtTimeWin, crtTraceWin(2,:)), title('detrended green trace');
        end
        
        % band pass filter
        crtTraceWinFilt = zeros(size(crtTraceWin));
        for k=1:size(crtTraceWin,1)
            crtTraceWinFilt(k,:) = filter(b,a,crtTraceWin(k,:)')';
        end
        if(SHOWPLOTS)
            figure(4), plot(crtTimeWin, crtTraceWinFilt(2,:)), title('filtered green trace');
        end
        
        % Method 1: Green channel
        crtPulseRed = crtTraceWinFilt(1,:);
        crtPulseGreen = crtTraceWinFilt(2,:);
        crtPulseBlue = crtTraceWinFilt(3,:);
        
        if(SHOWPLOTS)
            figure(fig5), plot(crtTimeWin, crtPulseGreen), title('crtPulseGreen');
        end
        
        % Method 2: CHROM
        %     Rn = crtTraceWin(1,:);
        %     Gn = crtTraceWin(2,:);
        %     Bn = crtTraceWin(3,:);
        %     Xs = 3*Rn-2*Gn;
        %     Ys = 1.5*Rn+Gn-1.5*Bn;
        %     Xf = filter(b,a,Xs')';
        %     Yf = filter(b,a,Ys')';
        %     alpha = std(Xf)/std(Yf);
        %     crtPulseChrom = Xf - alpha*Yf;
        
        Rf = crtTraceWinFilt(1,:);
        Gf = crtTraceWinFilt(2,:);
        Bf = crtTraceWinFilt(3,:);
        
        Xf = 3*Rf-2*Gf;
        Yf = 1.5*Rf+Gf-1.5*Bf;
        
        %Xf = filter(b,a,Xs')';
        %Yf = filter(b,a,Ys')';
        alpha = std(Xf)/std(Yf);
        crtPulseChrom = Xf - alpha*Yf;
        if(SHOWPLOTS)
            figure(6), plot(crtTimeWin, crtPulseChrom), title('crtPulseChrom');
        end
        
        
        % Method 5 : PCA
        crtTraceWin = crtTrace(1:nTraces,startInd:endInd);
        crtTraceWin = crtTraceWin ./ repmat(mean(crtTraceWin,2),[1 winLength]);
        RGB_input = [crtTraceWin(1,:);crtTraceWin(2,:);crtTraceWin(3,:)];
        %         crtPulsePCA = PCA(RGB_input);
        %
        %         crtPulsePCA = detrendsignal(crtPulsePCA')';
        %         crtPulsePCA = filter(b,a,crtPulsePCA')';
        
        % compare PPG with pulse traces
        %     fig7 = figure(7);
        %     clf(fig7), hold on, title('PPG vs rPPG'), xlim([timeTrace(startInd), timeTrace(endInd)]);
        %     p1 = plot(gtTime, gtTrace, 'b');
        %     p2 = plot(crtTimeWin, crtPulseGreen, 'g');
        %     p3 = plot(crtTimeWin, crtPulseChrom, 'r');
        %     legend([p1,p2,p3],'PPG', 'Green', 'Chrom');
        %     hold off;
        
        % overlap-add to build the final pulse trace
        %         pulseTraceGreen(startInd:endInd)=crtPulseGreen;
        %         pulseTraceBlue(startInd:endInd)=crtPulseBlue;
        %         pulseTraceRed(startInd:endInd)=crtPulseRed;
        %         pulseTraceChrom(startInd:endInd)=crtPulseChrom;
        %         pulseTraceMulti(:,startInd:endInd)=crtPulseMulti;
        %Overlap add
        pulseTraceGreen(startInd:endInd)=pulseTraceGreen(startInd:endInd)+crtPulseGreen;
        pulseTraceBlue(startInd:endInd)=pulseTraceBlue(startInd:endInd)+crtPulseBlue;
        pulseTraceRed(startInd:endInd)=pulseTraceRed(startInd:endInd)+crtPulseRed;
        pulseTraceChrom(startInd:endInd)=pulseTraceChrom(startInd:endInd)+crtPulseChrom;
        
        %Scale amplitudes
        pulseTraceGreen(startInd:endInd)=pulseTraceGreen(startInd:endInd)/std(pulseTraceGreen);
        pulseTraceBlue(startInd:endInd)=pulseTraceBlue(startInd:endInd)/std(pulseTraceBlue);
        pulseTraceRed(startInd:endInd)=pulseTraceRed(startInd:endInd)/std(pulseTraceRed);
        pulseTraceChrom(startInd:endInd)=pulseTraceChrom(startInd:endInd)/std(pulseTraceChrom);
        %Smooth
        pulseTraceGreen(startInd:endInd)=smooth(pulseTraceGreen(startInd:endInd));
        pulseTraceBlue(startInd:endInd)=smooth(pulseTraceBlue(startInd:endInd));
        pulseTraceRed(startInd:endInd)=smooth(pulseTraceRed(startInd:endInd));
        pulseTraceChrom(startInd:endInd)=smooth(pulseTraceChrom(startInd:endInd));
        pulseTraceRGB=cat(1,pulseTraceRed(startInd:endInd),pulseTraceGreen(startInd:endInd),pulseTraceBlue(startInd:endInd));
        %Extract pulse trace
        if strcmp(method,'ICA')==1
            if isempty(initialA)
                [Sig,W,initialA,y1]=simpleICA(pulseTraceRGB,maxIter,OverValue,'choosebestcomponent',true);
            else
                [Sig,W,initialA,y1]=simpleICA(pulseTraceRGB,maxIter,OverValue,'initguess',initialA,'choosebestcomponent',true);
            end
        end
        if strcmp(method,'CICA')==1
            [y1, w,convergedFlag] = cICAM(pulseTraceRGB, timeTrace, othreshold, w, learningRate, mu0, lambda0, gamma, maxIter, OverValue);
            %Check if did not converge, use prevY
            if(isempty(prevY) || convergedFlag==0)
                prevY=y1;
            else
                y1=prevY;
            end
        end
        
        %normalise
        stdy=std(y1);
        y1=y1-mean(y1);
        y1=y1/stdy;
        
        
        pulseTrace(startInd:endInd)=y1;
        crtPPGWin = gtTraceInterp(startInd:endInd);
        
        %Perform welch analyses over this window
        %     RPPG
        [rPPG_power,rPPG_freq] = pwelch(y1,[],[],length(y1)*3,Fs, 'psd'); % zero pading to increase the nb of points in the PSD
        rangeRPPG = (rPPG_freq>LOW_F & rPPG_freq < UP_F);   % frequency range to find PSD peak
        rPPG_power = rPPG_power(rangeRPPG);
        rPPG_freq = rPPG_freq(rangeRPPG);
        [pksrPPG,loc] = findpeaks(rPPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks (only one is used now but will be useful for postpreocessing
        rPPG_peaksLoc = rPPG_freq(loc);
        
        if(length(crtPPGWin) > 0) % sometime there is no PPG
            [PPG_power,PPG_freq] = pwelch(crtPPGWin,[],[],length(crtPPGWin)*3,Fs, 'psd');
            rangePPG = (PPG_freq>LOW_F & PPG_freq < UP_F); % frequency range to find PSD peak
            PPG_power = PPG_power(rangePPG);
            PPG_freq = PPG_freq(rangePPG);
            [pksPPG,loc] = findpeaks(PPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks
            PPG_peaksLoc = PPG_freq(loc);
        end
        
        if ~silentMode
            figure(fig5);
            clf(fig5);
            subplot(2,2,1);
            hold on;grid on;
            plot(rPPG_freq,rPPG_power);
            if ~isempty(rPPG_peaksLoc)
                plot(rPPG_peaksLoc(1), pksrPPG(1), 'r*');
            end
            ylim([0 3]),xlim([0.1 3.5]), title('rPPG Welch periodogram'), xlabel('Heart Rate (Hz)');
            
            hold off;
        end
        % save final results
        crtTime =  (startTime + endTime)/2;
        time(ind) = crtTime;
        
        if ~silentMode
            subplot(2,2,2);
            hold on; grid on;
        end
        if ~isempty(PPG_peaksLoc)
            if ~silentMode
                plot(PPG_freq,PPG_power)
                plot(PPG_peaksLoc(1), pksPPG(1), 'r*');
            end
            HR_PPG(ind) = PPG_peaksLoc(1)*60;
        else
            if ind>1
                HR_PPG(ind) = HR_PPG(ind-1);
            end
        end
        if ~silentMode
            ylim([0 3]),xlim([0.1 3.5]), title('PPG Welch periodogram'), xlabel('Heart Rate (Hz)');
            hold off;
        end
        
        
        if ~isempty(rPPG_peaksLoc)
            HR_RPPG(ind) = rPPG_peaksLoc(1)*60;
            HR_RPPG_filtered(ind) = rPPG_peaksLoc(1)*60;
        else
            %If no peaks are found. Happens sometimes in windowed full face
            HR_RPPG(ind)=HR_RPPG(ind-1);
            HR_RPPG_filtered(ind)=HR_RPPG_filtered(ind-1);
        end
        
        %Predict peak based on previous trend
        %If diff between actual and predicted peak is large,
        %Choose the peak closest to the predicted peak
        
        %Heuristic based on kalman filter
        if ~isTrackInitialized
            kalmanFilter= configureKalmanFilter('ConstantVelocity',HR_RPPG(1), [1 1]*1e5, [10,10], 1e5);
            isTrackInitialized=true;
        else
            
            if ind >1
                %Reconfigure kalman to compensate for changing HR
                if mod(ind,6==0)
                    kalmanFilter= configureKalmanFilter('ConstantVelocity',HR_RPPG_filtered(ind-1), [1 1]*1e5, [10,10], 1e5);
                end
                currDiff=abs(HR_RPPG_filtered(ind-1)-HR_RPPG(ind));
                trendDiff=currDiff;
                
                if ind>trendTolerance
                    trend=mean(HR_RPPG_filtered(ind-trendTolerance:ind-1));
                    trendDiff=abs(trend-HR_RPPG(ind));
                    %stem(ind,trendDiff,'b+');
                end
                
                if(currDiff > threshold && trendDiff>threshold)
                    %Also check if the new values are consistent
                    if ind>trendTolerance/2
                        newTrend=mean(HR_RPPG(ind-trendTolerance/3:ind-1));
                        newTrendDiff=abs(newTrend-HR_RPPG(ind));
                        if(newTrendDiff<threshold)
                            %Seems the new values are consistent, choose, and
                            %update kalman
                            HR_RPPG_filtered(ind)=HR_RPPG(ind);
                            predict(kalmanFilter);
                            correct(kalmanFilter,HR_RPPG_filtered(ind));
                            
                        else
                            %New value is very different from previous as well as trend
                            %predict
                            HR_RPPG_filtered(ind)=predict(kalmanFilter);
                            %Probability of HR being less than 50 is quite
                            %low. Maybe prediction is on the wrong trend
                            if HR_RPPG_filtered(ind)<50 && HR_RPPG(ind)>50
                                HR_RPPG_filtered(ind)=HR_RPPG(ind);
                                predict(kalmanFilter);
                                correct(kalmanFilter,HR_RPPG_filtered(ind));
                            end
                        end
                    else
                        %New value is very different from previous as well as trend
                        %predict
                        HR_RPPG_filtered(ind)=predict(kalmanFilter);
                        %Probability of HR being less than 50 is quite
                        %low. Maybe prediction is on the wrong trend
                        if HR_RPPG_filtered(ind)<50 && HR_RPPG(ind)>50
                            HR_RPPG_filtered(ind)=HR_RPPG(ind);
                            predict(kalmanFilter);
                            correct(kalmanFilter,HR_RPPG_filtered(ind));
                        end
                    end
                elseif currDiff>threshold && trendDiff<threshold
                    %New value is close to the trend
                    HR_RPPG_filtered(ind)=HR_RPPG(ind);
                    %predict(kalmanFilter);
                    correct(kalmanFilter,HR_RPPG_filtered(ind));
                elseif currDiff<threshold && trendDiff>threshold
                    %New value is close to previous value,wrong trend
                    %correct
                    HR_RPPG_filtered(ind)=HR_RPPG(ind);
                    predict(kalmanFilter);
                    correct(kalmanFilter,HR_RPPG_filtered(ind));
                else
                    HR_RPPG_filtered(ind)=HR_RPPG(ind);
                    %predict(kalmanFilter);
                    correct(kalmanFilter,HR_RPPG_filtered(ind));
                end
                
                if(~isempty(gtTime)) % sometime there is no PPG
                    [~, crtTimeInd] = min(abs(gtTime-crtTime));
                    HR_sensor(ind) = gtHR(crtTimeInd);
                end
                if ~silentMode
                subplot(2,2,3:4);
                hold on;grid on;
                xlim([timeTrace(1) timeTrace(end)]);
                ylim([40 210]);
                
                cla
                plot(time(1:ind),HR_PPG(1:ind),'g-*');
                plot(time(1:ind),HR_RPPG(1:ind),'r-o');
                plot(time(1:ind),HR_RPPG_filtered(1:ind),'b-o');
                legend('PPG','RPPG','RPPG Filtered');
                title('raw results HR estimation');
                hold off;
                end
                
            end
        end
        
        ind=ind+1;
        pause(.001);
        
        if(SHOWPLOTS)
            pause();
        end
    end
end
isTrackInitialized=false;
s=subplot(2,2,3:4);
hold on;grid on;
xlim([timeTrace(1) timeTrace(end)]);
ylim([40 210]);
hold on;
cla;
p1 = plot(time, HR_sensor, 'b-*');
p2 = plot(time, HR_PPG, 'g-*');
p3 = plot(time, HR_RPPG, 'c-*');
p4 = plot(time, HR_RPPG_filtered, 'r-*');
title('raw results HR estimation');
legend( 'HR sensor', 'HR PPG', 'HR rPPG', 'HR rPPG filtered');
hold off


if isempty(suffix)
    save([vidFolder 'pulseTrace' method  '-win' num2str(WINDOW_LENGTH_SEC)],'pulseTrace','timeTrace','HR_RPPG','HR_RPPG_filtered','HR_PPG');
else
    save([vidFolder 'pulseTrace' method '-win' num2str(WINDOW_LENGTH_SEC) '_' suffix],'pulseTrace','timeTrace','HR_RPPG','HR_RPPG_filtered','HR_PPG');
end

%method = matFileName(11:end);
fig=figure;
handle=copyobj(s,fig);
set(handle, 'Position', get(0, 'DefaultAxesPosition'));
legend( 'HR sensor', 'HR PPG', 'HR rPPG', 'HR rPPG filtered');
if ~isempty(suffix)
    suffix=['-' suffix];
end
saveas(gcf,['../../../CompareFaceDetectionMatlab/' nameFolder '/'  method '-win' num2str(WINDOW_LENGTH_SEC) suffix '_Temporal.png']);


addpath('../BlandAltman/');

% tit = 'HR vs GT Signal'; % figure title
% label = {'HR', 'HR from GT Signal'};
gnames = {'HR analysis'}; % names of groups in data {dimension 1 and 2}
corrinfo = {'n','SSE','r2','eq'}; % stats to display of correlation scatter plot
BAinfo = {'RPC(%)'}; % stats to display onavg Bland-ALtman plot
limits = 'auto'; % how to set the axes limits

symbols = 'Num'; % symbols for the data sets (default)
colors = 'brg'; % colors for the data sets


if(~isempty(gtTime))
    
    Array_Difference = abs(HR_PPG-HR_RPPG_filtered);
    Selected_Array_Difference_2_5 = Array_Difference(Array_Difference <= 2.5);
    Selected_Array_Difference_5 = Array_Difference(Array_Difference <= 5);
    
    Estimation_percentage_2_5 = double(length(Selected_Array_Difference_2_5))/length(HR_RPPG_filtered);
    Estimation_percentage_5 = double(length(Selected_Array_Difference_5))/length(HR_RPPG_filtered);
    
    M = length(Selected_Array_Difference_5);
    
    MAE = mean(Array_Difference);
    RMSE = sqrt((1/M)*sum((Selected_Array_Difference_5).^2));
    MAE_5 = mean(Selected_Array_Difference_5);
    
    disp(['Estimation_percentage_2_5 : ' num2str(Estimation_percentage_2_5)]);
    disp(['Estimation_percentage_5 : ' num2str(Estimation_percentage_5)]);
    disp(['MAE : ' num2str(MAE)]);
    disp(['MAE_5 : ' num2str(MAE_5)]);
    disp(['RMSE : ' num2str(RMSE)]);
    
    
    
    gnames = {'HR analysis'}; % names of groups in data {dimension 1 and 2}
    corrinfo = {'n','SSE','r2','eq'}; % stats to display of correlation scatter plot
    BAinfo = {'RPC(%)'}; % stats to display onavg Bland-ALtman plot
    limits = 'auto'; % how to set the axes limits
    
    symbols = 'Num'; % symbols for the data sets (default)
    colors = 'brg'; % colors for the data sets
    
    
    tit = 'HR vs HR from PPG'; % figure title
    label = {'HR', 'HR from PPG'};
    
    [cr, fig, statsStruct] = BlandAltman(HR_RPPG_filtered(1,:),HR_PPG(1,:),label,tit,gnames,corrinfo,BAinfo,limits,colors,symbols);
    annotation('textbox', [.2 .8 .1 .1], 'FitHeightToText', 'ON', 'Fontsize', 10, ...
        'String', ['PRECIS 2.5 : ' num2str(Estimation_percentage_2_5) ' | PRECIS 5 : ' num2str(Estimation_percentage_5) ' | MAE : ' num2str(MAE) ' | MAE 5 : ' num2str(MAE_5) ' | RMSE 5 : ' num2str(RMSE)]);
    
    excel_row={statsStruct.SSE statsStruct.r2 Estimation_percentage_2_5 Estimation_percentage_5 MAE MAE_5 RMSE round(statsStruct.CR) round(statsStruct.MeanPerc)};
    saveas(gcf,['../../../CompareFaceDetectionMatlab/' nameFolder '/' method '-win' num2str(WINDOW_LENGTH_SEC) '-' suffix '_Blant.png']);
  %  pause;
   % clear all;
%    close all;
    %clc;
    
end
