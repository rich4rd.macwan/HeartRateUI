function [r,r1,r2]=autocorrd(y,X,varargin)
%If saved r, r1 and r2 are not empty,
%and y is the same as prevY,save prevY=y,and return saved r,r1,r2
if evalin('base','exist(''r'')')
    prevy=evalin('base','y');
    if norm(y)==norm(prevy)
        %Same iteration, return saved variables
        r=evalin('base','r');
        r1=evalin('base','r1');
        r2=evalin('base','r2');
        return;
    end
end

%     N = 1200;
%     Fs = 1000;
%     t = (0:N-1)/Fs;
%    Fs=1/mean(diff(t));

%     sigma = 0.05;
%     rng('default')
%     s = sin(2*pi*t*20)+sigma*randn(size(t));
%     s=x;
%     plot(s)
%     figure
%     [Pxx,f] = periodogram(x,hann(numel(x)),[],Fs);
%     obw(s,Fs)
%     [wd,lo,hi,power]= obw(s,Fs)
if nargin==3
    Fs=varargin{1};
else
    Fs=28.5533;
end
if size(y,1)~=1
    y=y';
end
N=numel(y);
mu=mean(y);
yorig=y;
%x=x-mean(x);
%lags=1:(N-1);
lags=round(Fs/3):round(Fs/.7);
%lags=1:round(Fs/10):N-1;
%Not 0 to N-1 because at lag 0, the correlation will be highest for
%any signal, even random

%           sum((X-Xbar)(Y-Ybar))
%r = -------------------------------------
%    sqrt(sum((X-Xbar)^2).sum((Y-Ybar)^2))
%r=zeros(size(lags));
r=zeros(1,N);
r1y=zeros(N); %First derivative
r2y=zeros(N); %Second derivative

for k=lags
    ylagged=y(1:N-k);
    %Xlagged=X(:,1:N-k);
    %Xpadded=[zeros(3,k) Xlagged];
    ypadded=[zeros(1,k) ylagged];
    r(k+1)=sum(y.*ypadded);
    % y(Tk+Tk')
    r1y(k+1,:)=(ypadded+[y(k+1:N) zeros(1,k)]);
    
    %Bracket of Eqn 32 in RPPG-cICA-doctoralday.pdf, scalar
    r2y(1,k+1)=r(k+1)+r1y(k+1,:)*y';
    %r2y(:,k+1)=(ypadded'+[y(k+1:N) zeros(1,k)]');
    %r2(k+1)=sum(sum(X*(Xpadded+[X(:,k+1:N) zeros(3,k)])')) ;
    
    %Similar values, but scaled, See wikipedia
    %Autocorrelation:Estimation
    %R(k+1)=(sum((y-mu).*(ypadded-mu)))/((N-k)*sigma*sigma);
    
end
%R2=R1y*R1y' + R(Tk+Tk')
%Second term is simplified computationally in a loop
%First term is faster with direct multiplication
%tic

for k=lags
    %Tk=getTk(N,k);
    %r2y(k+1,:)=ypadded*r1y + [r(k+1:N) zeros(1,k)];
    %r2y(k+1,:)=r1y(k+1,:)*r1y + [zeros(1,k) r(1:N-k)] + [r(k+1:N) zeros(1,k)];
    %r2y(k+1,:)=r2y(k+1,:)+[zeros(1,k) r(1:N-k)] + [r(k+1:N) zeros(1,k)];
    
    %Temporal mean on the fly
    %r2y(k+1,:)=r2y(k+1,1)*k*([zeros(1,k) ones(1,N-k)] + [ones(1,N-k) zeros(1,k)]);
    %r2y(:,k+1)=r1*r2y(:,k+1);%+mean(diag(r)*(Tk+Tk'),2);
    %r2y(k,:)=[zeros(1,k-1) r2y(1,1:N-k+1)] ;
end
r2y=r2y+r2y';
%r2y=r2y+r1y*r1y';
%toc
% r=standardize(r);
% r1=standardize(r1);
% r2=standardize(r1);

%r2=mean(r2);

%r1=mean(r1);
%Apply chain rule of derivation
r1=-2*X*r1y*r'/N;
r2=-2*(X*r2y'*X')/N;

%Save r,r1,r2 and prevY=y
assignin('base','r',r);
assignin('base','r1',r1);
assignin('base','r2',r2);
assignin('base','y',y);
end

