function Hout = hessiancICA(w,X,lambda,Fs,Wchrom)
% Hessian of objective
H=X*diag(1-(tanh(w'*X)).^2)*X';

% Hessian of nonlinear equality constraint
Hgeq = 2*(X*X');

% Hessian of nonlinear inequality constraint, get hessian of autocorr
Hgineq = zeros(numel(w));
% [~,~,r2]=autocorrd(w'*X,X,Fs);

%CHROM constraint 2nd derivative
% C=abs(w)-abs(Wchrom);
% c=norm(C);
% C2=1/c - (C'*C)/c^3
%=1/c-1/c=0
Hout = H + lambda.eqnonlin*Hgeq+lambda.ineqnonlin(1)*0;