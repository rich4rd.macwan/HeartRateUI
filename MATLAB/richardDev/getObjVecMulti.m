function [znad, zi]=getObjVecMulti(t)
% Get the best objective vectors for negentropy and autocorrelation for rPPG
% znad and zi are both vectors of length 2 containing the nadir and ideal
% vectors for negentropy and autocorrelation respectively
LOW_F=.7; HIGH_F=3;
%Ideal vector corresponds to the max possible negentropy and autorrelation
%Nadir vector corresponds to the min possible negentropy and
%autocorrelation in the optimal set
idealSignalH=sin(2*pi*HIGH_F*t);
idealSignalL=sin(2*pi*LOW_F*t);

firstEig=1;
lastEig=1;
interactivePCA='off';
verbose='off';
%Whiten and center

% [E, D]=pcamat(idealSignalH, firstEig, lastEig, interactivePCA, verbose);
% % Calculate the whitening
% [whitesig, whiteningMatrix, dewhiteningMatrix] = whitenv(idealSignalH, E, D, verbose);
% idealSignalH=whitesig;
% 
% [E, D]=pcamat(idealSignalL, firstEig, lastEig, interactivePCA, verbose);
% % Calculate the whitening
% [whitesig, whiteningMatrix, dewhiteningMatrix] = whitenv(idealSignalL, E, D, verbose);
% idealSignalL=whitesig;


%Autocorrelations
[RH,~,~] = autocorrd(idealSignalH,1,1/mean(diff(t)));
[RL,~,~] = autocorrd(idealSignalL,1,1/mean(diff(t)));

ACH=mean(RH.*RH);
ACL=mean(RL.*RL);

%Negenetropies
std_H=std(idealSignalH);
v_GausH=normrnd(0, std_H, 1, numel(t));
Neg_H=(mean(log(cosh(idealSignalH))) - mean(log(cosh(v_GausH)))).^2;

std_L=std(idealSignalL);
v_GausL=normrnd(0, std_L, 1, numel(t));
Neg_L=(mean(log(cosh(idealSignalL))) - mean(log(cosh(v_GausL)))).^2;

znad=[Neg_L ACL]';
zi=[Neg_H ACH]';
end