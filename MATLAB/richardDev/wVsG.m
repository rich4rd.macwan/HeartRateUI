function wVsG(vidFolder,X)
    [theta,phi]=meshgrid(-2*pi:.5:2*pi);
    R=1;
    x=R*sin(theta).*cos(phi);
    y=R*sin(theta).*sin(phi);
    z=R*cos(theta);
    Gmean=z;
    J=z;
    I=size(z,1);
    J=size(z,2);
    maxX=0;maxY=0;maxG=0;
    for i=1:I
        fprintf('%d of %d \n',i,I);
        for j=1:J
            w=[x(i,j) y(i,j) z(i,j)]';
            Y=w'*X;
            [G,~,~]=autocorrd(Y,X);
            Gmean(i,j)=mean(G.*G);
            if Gmean(i,j)>maxG
                maxG=Gmean(i,j);
                maxX=x(i,j); maxY=y(i,j);
                maxZ=z(i,j);
            end
        end
%         hold on;
%         plot3(x,y,Gmean,'.');
%         view(45,45);
%         grid on;
%         pause(.0001);
    end
    wG=[maxX,maxY,maxZ];
    save([vidFolder 'Gmean'],'x','y','Gmean','wG','maxG');
%     m=mesh(x,y,z,Gmean);
%     set(m,'FaceColor','none');
%     hold on
%     axis equal
%     quiver3(0,0,0,wG(1),wG(2),wG(3),'LineWidth',3);
%     xlabel('R');ylabel('G');zlabel('B');
end