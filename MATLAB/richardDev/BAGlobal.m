clear all
Le2iFolder='/run/media/richard/Le2iHDD/Le2i/';
IUTFolder='/run/media/richard/Le2iHDD/IUT/';
%SIMPLE
method='ICA';
suffix='';
suffix='crop';
suffix='skin';

IUTMode=false;
fullMode=true;
%Iterate through all directories, load and concatenate the rPPG and HRs
HR_PPG_all=[];
HR_RPPG_all=[];
HR_RPPG_filtered_all=[];


if ~IUTMode
    dirs=dir([ Le2iFolder '/*gt']);
    d=dir([ Le2iFolder 'aft*']);
    dirs=[dirs;d];
    for i=1:size(dirs)
        %cd([ vidFolder dirs(i).name]);
        %Load GT trace
        %load([vidFolder cdir.name]);
        if isempty(suffix)
            if fullMode==true
                filename=['pulseTrace' method];
            else
                filename=['pulseTrace' method '-win15'];
            end
        else
            if fullMode==true
                filename=['pulseTrace' method '_' suffix];
            else
                 filename=['pulseTrace' method '-win15_' suffix];
            end
        end
        %pwd
        vidFolder=[Le2iFolder dirs(i).name '/'];
        load([ vidFolder filename]);
        %     [HR_sensor,HR_PPG,HR_RPPG,HR_RPPG_filtered]=...
        %         getHRFromPulse('vidFolder',vidFolder,'matFileName',['pulseTrace' method],'suffix',suffix,'silent',true,'IUTMode',false);
        %
        %     save([vidFolder filename '_full'],'pulseTrace','HR_sensor','HR_PPG','HR_RPPG','HR_RPPG_filtered');
        
        HR_PPG_all=[HR_PPG_all HR_PPG];
        HR_RPPG_all=[HR_RPPG_all HR_RPPG];
        HR_RPPG_filtered_all=[HR_RPPG_filtered_all HR_RPPG_filtered];
        
    end
else
    for i=[1 3:9 11:19 21:26 30:32 34:40 42:44 46:49]
        if i<10
            vidFolder=[IUTFolder 'subject' num2str(0) num2str(i) '/vid2/'];
            vidName=['subject' num2str(0) num2str(i)];
        else
            vidFolder=[IUTFolder 'subject' num2str(i) '/vid2/'];
            vidName=['subject' num2str(i)];
        end
        if isempty(suffix)
            if fullMode==true
                filename=['pulseTrace' method];
            else
                filename=['pulseTrace' method '-win15'];
            end
        else
            if fullMode==true
                filename=['pulseTrace' method '_' suffix];
            else
                 filename=['pulseTrace' method '-win15_' suffix];
            end
        end
        load([ vidFolder filename]);
        HR_PPG_all=[HR_PPG_all HR_PPG];
        HR_RPPG_all=[HR_RPPG_all HR_RPPG];
        HR_RPPG_filtered_all=[HR_RPPG_filtered_all HR_RPPG_filtered];
        
    end
end

%Perform global BA
%cd(oldDir);
addpath('../BlandAltman/');

Array_Difference = abs(HR_PPG_all-HR_RPPG_filtered_all);
    Selected_Array_Difference_2_5 = Array_Difference(Array_Difference <= 2.5);
    Selected_Array_Difference_5 = Array_Difference(Array_Difference <= 5);

    Estimation_percentage_2_5 = double(length(Selected_Array_Difference_2_5))/length(HR_RPPG_filtered_all);
    Estimation_percentage_5 = double(length(Selected_Array_Difference_5))/length(HR_RPPG_filtered_all);

    M = length(Selected_Array_Difference_5);

    MAE = mean(Array_Difference);
    RMSE = sqrt((1/M)*sum((Selected_Array_Difference_5).^2));
    MAE_5 = mean(Selected_Array_Difference_5);

    disp(['Estimation_percentage_2_5 : ' num2str(Estimation_percentage_2_5)]);
    disp(['Estimation_percentage_5 : ' num2str(Estimation_percentage_5)]);
    disp(['MAE : ' num2str(MAE)]);
    disp(['MAE_5 : ' num2str(MAE_5)]);
    disp(['RMSE : ' num2str(RMSE)]);

    gnames = {'HR analysis'}; % names of groups in data {dimension 1 and 2}
    corrinfo = {'n','SSE','r2','eq'}; % stats to display of correlation scatter plot
    BAinfo = {'RPC(%)'}; % stats to display onavg Bland-ALtman plot
    limits = 'auto'; % how to set the axes limits

    symbols = 'Num'; % symbols for the data sets (default)
    colors = 'brg'; % colors for the data sets


    tit = 'HR vs HR from PPG'; % figure title
    tit='';
    label = {'HR from RPPG', 'HR from PPG'};
    
    [cr, fig, statsStruct] = BlandAltman(HR_RPPG_filtered_all(1,:),HR_PPG_all(1,:),label,tit,gnames,corrinfo,BAinfo,limits,colors,symbols);
    set (fig, 'Units', 'normalized', 'Position', [0,0,1,1]);
    str=['PRECIS 2.5 : ' num2str(Estimation_percentage_2_5) ' | PRECIS 5 : ' num2str(Estimation_percentage_5) ' | MAE : ' num2str(MAE) ];
    
    annotation('textbox', [0.2 0.85 .1 .1], 'FitHeightToText', 'ON', 'Fontsize', 24, ...
        'String',str );
        %'String', ['PRECIS 2.5 : ' num2str(Estimation_percentage_2_5) ' | PRECIS 5 : ' num2str(Estimation_percentage_5) ' | MAE : ' num2str(MAE) ' | MAE 5 : ' num2str(MAE_5) ' | RMSE 5 : ' num2str(RMSE)]);
    
    


