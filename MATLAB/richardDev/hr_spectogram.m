function [HR,freq,power,pks,loc]=hr_spectogram(time,y)
% shift = nfft - Fs; % sliding window shift for spectrogram
% [s,f,t,p] = spectrogram(y,window,shift, linspace(0.3, 3.5,nfft), Fs,'yaxis');
% 
% % HR from spectrogram
% [q,nd] = max(10*log10(p));
% HR_spec = 60*f(nd);
% HR_specTime = t;

LOW_F=.3; % low freq for PSD range
UP_F=3; % high freq for PSD range

Fs = 1/mean(diff(time));
[power,freq] = pwelch(y,[],[],length(y)*3,Fs, 'psd');
rangePPG = (freq>LOW_F & freq < UP_F); % frequency range to find PSD peak
power = power(rangePPG);
freq = freq(rangePPG);
[pks,loc] = findpeaks(power,'SortStr','descend','NPeaks',1); % get 2 first peaks
if ~isempty(loc)
    peaksLoc = freq(loc);
    HR=peaksLoc(1)*60;
else
    [p,l]=findpeaks(y);
    HR=60/mean(diff(time(l)));
end
%plot(freq,power)
