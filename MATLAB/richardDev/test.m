% %Test Kalman
% clear all
% load('kalmantest');
% isTrackInitialized=false;
% hold on
% 
% trendTolerance=10; %Size of window to calculate trend and trend difference
% threshold=7;
% for ind=1:numel(HR_RPPG)
%     if ~isTrackInitialized
%         kalmanFilter= configureKalmanFilter('ConstantVelocity',HR_RPPG(1), [1 1]*1e5, [10,10], 1e5);
%         isTrackInitialized=true;
%     else
%         if ind >1
%             currDiff=abs(HR_RPPG_filtered(ind-1)-HR_RPPG(ind));
%             trendDiff=currDiff;
%             if ind>10
%                 trend=mean(HR_RPPG_filtered(ind-trendTolerance:ind-1));
%                 trendDiff=abs(trend-HR_RPPG(ind));
%                 %stem(ind,trendDiff,'b+');
%             end
%             
%             if(currDiff > threshold && trendDiff>threshold) 
%                 %New value is very different from previous as well as trend
%                 %predict
%                 HR_RPPG_filtered(ind)=predict(kalmanFilter);
%             elseif currDiff>threshold && trendDiff<threshold
%                 %New value is close to the trend
%                 HR_RPPG_filtered(ind)=HR_RPPG(ind);
%                 predict(kalmanFilter);
%             elseif currDiff<threshold && trendDiff>threshold
%                 %Wrong trend, use predicted
%                 %correct
%                 predict(kalmanFilter);
%                 HR_RPPG_filtered(ind)=correct(kalmanFilter,HR_RPPG_filtered(ind));
%             else
%                 HR_RPPG_filtered(ind)=HR_RPPG(ind);
%             end
%             
%         
%         end
%     end
% end
% plot(HR_PPG,'-*');
% plot(HR_RPPG,'-*');
% plot(HR_RPPG_filtered,'-*');
% legend('PPG','RPPG','RPPG-filtered')

N=500;
timeTrace=1:N;
v=[0:N-1];
%sig(1,:)=sin(v/2);   % ????????

S1=sin(v/2);
%S2=awgn(S1,10,'measured');
S2=((rem(v,23)-11)/9).^5;
%S2=sawtooth(v/2);
figure('Name','Original');
subplot(2,1,1)
plot(S1);
subplot(2,1,2)
plot(S2);

A=[.4 .6;.8 .2];
%A=[.5 .5;.5 .5];


MixedSig=A*[S1;S2];
figure('Name','Mixed Signal');
subplot(2,1,1)
plot(MixedSig(1,:));
subplot(2,1,2)
plot(MixedSig(2,:));

mu0 = 1;
lambda0 = 1;
gamma = 1;
learningRate = 1;
OverValue=1e-6;  maxIter = 400;
threshold =20;   % good ref
w = rand(size(MixedSig,1),1);
w=w/norm(w);
addpath('../tools/cICA/');

[Sig, W] = cICAM(MixedSig, timeTrace, threshold, w, learningRate, mu0, lambda0, gamma, maxIter, OverValue);
W
expectedW=inv(A)
% expectedW=inv(A);
% figure(8);
% hold on
% quiver(0,0,expectedW(1,1),expectedW(1,2));
% quiver(0,0,expectedW(2,1),expectedW(2,2));
% xlim([-1.2,1.2]);
% ylim([-1.2,1.2]);
figure('Name','Most periodic Signal-cICA');
plot(Sig);