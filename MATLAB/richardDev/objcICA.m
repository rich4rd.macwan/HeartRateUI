function [f,g,H]=objcICA(w,X) 
    y=w'*X;
    f=zeros(size(y,1),1);
    for i=1:size(y,1)
        std_y = std(y(i,:));
        % Gaussian signal with the same mean and variance
        v_Gaus = normrnd(0, std_y, 1, numel(y(i,:)));    
        f(i) = -mean( log(cosh(y(i,:))) - log(cosh(v_Gaus)) );
    end
    
    g=-X*tanh(y');
    H=-X*diag(1-(tanh(y)).^2)*X';
end
