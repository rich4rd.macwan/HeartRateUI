function [excel_row]=cica_rppg(varargin)
rng('shuffle')
addpath('../tools/cICA/');
%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
WINDOW_LENGTH_SEC = 30; % length of the sliding window (in seconds)
STEP_SEC = 0.5; % step between 2 sliding window position (in seconds)
LOW_F=.3; % low freq for PSD range
UP_F=3; % high freq for PSD range

FILTER_ORDER = 1; % for .mat save
PLOT_ON=1;

disp('Testing ICA with RPPG');
%Load rppg traces

vidFolder='/run/media/richard/Le2iHDD/Le2i/5-gt/'; 
%vidFolder='/home/richard/Downloads/';

matFileName = 'pulseTraceRGB'; % input file
%matFileName = 'pulseTraceRGB_crop'; % input file
%matFileName = 'pulseTraceRGB_skin'; % input file

i = 1;
IUTMode=false;
silentMode=true;
DOFILTER=false;
while(nargin > i)
        if(strcmp(varargin{i},'vidFolder')==1)
        vidFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'matFileName')==1)
        matFileName=varargin{i+1};
    end
    if(strcmp(varargin{i},'IUTMode')==1)
        IUTMode=varargin{i+1};
    end
    if(strcmp(varargin{i},'silent')==1)
        silentMode=varargin{i+1};
    end
    if(strcmp(varargin{i},'func')==1)
        func=varargin{i+1};
    end
    if(strcmp(varargin{i},'DOFILTER')==1)
        DOFILTER=varargin{i+1};
    end
    i = i+2;
end
suffix=matFileName(end-3:end);
if strcmp(suffix,'skin')==0 && strcmp(suffix,'crop')==0
    suffix='';
end
if ~DOFILTER
    filterSuffix='_Unfilt';
else
    filterSuffix='';
end

disp(['Working with  ' vidFolder matFileName filterSuffix '.mat']);
if(exist([vidFolder matFileName filterSuffix '.mat'], 'file') == 2)
   load([vidFolder matFileName filterSuffix '.mat']); %Already detrended
else
    disp('oops, no input file');
    return;
end

%Plot the traces
%hold on
%for i=1:3
%    plot(timeTrace,pulseTraceRGB(:,i));
%end

%Perform ICA on the RGB traces
traces=pulseTraceRGB';

if(PLOT_ON)
    %ICAshow(traces,'title','Source RGB signals');
end
%Convert to unit variance if it isn't already
%traces=traces(1:25,:);
traces=standardize(traces);
N=size(traces,2);
%Whiten the input
% [WhitenedSig,WhitenMatrix]=whiten(traces);
% V=WhitenMatrix;
% if(PLOT_ON)
%     figure;
%      %   plot(WhitenedSig(4,[N-499:N])); ylabel('z4');
%     subplot(3,1,3);2
%         plot(WhitenedSig(3,:)); ylabel('z3');
%     subplot(3,1,2);
%         plot(WhitenedSig(2,:)); ylabel('z2');
%     subplot(3,1,1);
%         plot(WhitenedSig(1,:)); ylabel('z1'); title('\bf whitened signals');
% end

% load PPG
% [gtTrace, gtHR, gtTime] = loadPPG(vidFolder);
% gtTime = gtTime / 1000;  % in seconds
% gtTraceInterp=interp1(gtTime,gtTrace,timeTrace,'pchip');

% plot(timeTrace,traces(2,:));

X=traces;
%get the random vector in the plane that is common to a majority of Ws
load('Wplane');
position=[0;0;0];
normal=[p10;p10;-1];
w = rand(size(X,1),1);w=w/norm(w);
%w=getRandomPointInPlane(position,normal,1);

wVsG(vidFolder,X);

mu0 = 1;
lambda0 = 5;
gamma = 10;
learningRate = 1e4;
OverValue=1e-6;  maxIter = 400;
threshold = 1e4;

[y1, w1] = cICAM(X, timeTrace, threshold, w, learningRate, mu0, lambda0, gamma, maxIter, OverValue);
    
%     load([vidFolder 'Gmean']);
%     [theta,phi]=meshgrid(-2*pi:.5:2*pi);
%     R=1;
%     z=R*cos(theta);
%     m=mesh(x,y,z,Gmean);
%     set(m,'FaceColor','none');
%     hold on
%     axis equal
%     quiver3(0,0,0,wG(1),wG(2),wG(3),'LineWidth',3);
%     xlabel('R');ylabel('G');zlabel('B');
%     quiver3(0,0,0,w1(1),w1(2),w1(3),'LineWidth',3);
ys=standardize(y1);
%ys=(y1-min(y1))/(max(y1)-min(y1));
figure
hold on
%plot(gtTime,gtTrace,'b');
%plot(timeTrace,pulseTraceRGB(:,2),'g');
plot(timeTrace,ys,'r');
%legend('PPG','Green','CICA');
%pulseTrace=pulseTraceRGB(:,2);
pulseTrace=ys;
pause(2);
addpath('../main-stable');
if isempty(suffix)
    save([vidFolder 'pulseTraceCICA' filterSuffix],'pulseTrace','timeTrace');
else
    save([vidFolder 'pulseTraceCICA' '_' suffix filterSuffix],'pulseTrace','timeTrace');
end

%Pass the selected component to the HR calculation routine
%currentDir=cd;
%cd('../main-stable/');
fileName='pulseTraceCICA' ;
if ~isempty(suffix)
    fileName=['pulseTraceCICA' '_' suffix];
else
    fileName=['pulseTraceCICA' ];
end

 [HR_sensor,HR_PPG,HR_RPPG,HR_RPPG_filtered,excel_row]=getHRFromPulse...
     ('vidFolder',vidFolder,'matFileName',fileName,'IUTMode',IUTMode,...
     'silent',silentMode,'ICAWIN',false,'DOFILTER',DOFILTER);
 
 if isempty(suffix)
     save([vidFolder 'pulseTraceCICA' filterSuffix],'pulseTrace','timeTrace',...
         'HR_sensor','HR_PPG','HR_RPPG','HR_RPPG_filtered');
 else
     save([vidFolder 'pulseTraceCICA' '_' suffix filterSuffix],'pulseTrace','timeTrace',...
         'HR_sensor','HR_PPG','HR_RPPG','HR_RPPG_filtered');
 end
%cd(currentDir);

end