function fetalECG(sourcefile)
addpath('../tools/');
addpath('../tools/cICA');
if strcmp(sourcefile(end-3:end),'.edf')==1
    %read edf
    [hdr,record]=edfread(edfSource);
    %read annotations
    fileID=fopen([edfSource '.qrs'],'r');
    annotations=fread(fileID);
    fclose(fileID);
    %Read edf record labels
    % abdsignal=[];
    % thorsignal=[];
    abdI=1;thorI=1;
    for i=1:size(hdr.label,2)
        abdRes=strfind(hdr.label(i),'Abdomen');
        thorRes=strfind(hdr.label(i),'Thorax');
        if abdRes{1}==1
            %Starts with abdomen
            abdsignal(abdI,:)=record(i,:);
            abdI=abdI+1;
        end
        
        if thorRes{1}==1
            %Starts with abdomen
            thorsignal(thorI,:)=record(i,:);
            thorI=thorI+1;
        end
    end
end


if strcmp(sourcefile(end-3:end),'.csv')==1
    ecgdata=csvread(sourcefile,2,0);
    %fid=fopen([sourcefile(1:end-3) 'fqrs.txt']);
    fqrs=textread([sourcefile(1:end-3) 'fqrs.txt'],'%d');
%     fclose(fid);
    timeTrace=ecgdata(:,1);
    AECG1=ecgdata(:,2);
    AECG2=ecgdata(:,3);
    offset2=mean(AECG2);
    AECG3=ecgdata(:,4);
    offset3=mean(AECG3);
    AECG4=ecgdata(:,5);
    offset4=mean(AECG4);
    
    hold on;grid on;grid minor;
    ax = gca;
    ax.MinorGridLineStyle='-';
    ax.GridColor=[.5,.15,.15];
    ax.MinorGridColor=[.5,.15,.15];
    paddperc=1.4;
    plot(timeTrace,AECG4);
    plot(timeTrace,AECG3+paddperc*offset4);
    plot(timeTrace,AECG2+paddperc*(offset4+offset3));
    fqrsy=repmat(1.2*paddperc*(offset4+offset3+offset2),size(fqrs));
    plot(fqrs/1000,fqrsy,'.-');
    plot(timeTrace,AECG1+paddperc*(offset4+offset3+offset2));
    legend('AECG4','AECG3','AECG2','FQRS','AECG1');
    
end


%Perform cICA on abdomen signals to extract the (possibly) maternal ecg
mu0 = 1;
lambda0 = 5;
gamma = 5;
learningRate = 5;
OverValue=1e-5;  maxIter = 400;
threshold = 10;

%Fs=1000;%(1KHz)
% N=10000; %Num of samples
% timeTrace=1:N;%milliseconds

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
WINDOW_LENGTH_SEC = 5; % length of the sliding window (in seconds) (use -1 to work on the whole signal at once)
STEP_SEC = .5; % step between 2 sliding window position (in seconds)
LOW_F=.7; % low freq for BP filtering
UP_F=3; % high freq for BP filtering
FILTER_ORDER = 8; % Order of the butterworth BP filter
SHOWPLOTS = 0;

% get exact Fs
Fs = 1/mean(diff(timeTrace));

% get window length in frames
winLength = round(WINDOW_LENGTH_SEC*Fs);
step = round(STEP_SEC*Fs);


X=[cat(2,AECG1,AECG2,AECG3,AECG4)]';
N=5000;
X=X(:,1:N);
nTraces=size(X,1);
w = rand(nTraces,1);w=w/norm(w);

% halfWin = (winLength/2);
% 
% pulseTraceGreen=zeros(1,traceSize);
% pulseTraceBlue=zeros(1,traceSize);
% pulseTraceRed=zeros(1,traceSize);
% pulseTraceChrom=zeros(1,traceSize);
% ind=1;
% N=numel(halfWin:step:traceSize-halfWin);
% time=zeros(1,N);
% HR_PPG=zeros(1,N);
% HR_RPPG=zeros(1,N);
% HR_RPPG_filtered=zeros(1,N);
%     for i=halfWin:step:traceSize-halfWin
%     end
[y1, w1] = cICAM(X, timeTrace, threshold, w, learningRate, mu0, lambda0, gamma, maxIter, OverValue);
%[Sig,W,initialA,SelectedSignal]=simpleICA(X,maxIter,OverValue,'choosebestcomponent',false);

ys=standardize(y1);

end