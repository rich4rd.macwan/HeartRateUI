function [pos]=ica_cica_move_hndlr(src,evt,destPlot,xline)
pos=get(src,'currentpoint');
pos=pos(1,:);
units=get(destPlot,'Units');
set(destPlot,'Units','pixels');
dims=get(destPlot,'Position');
sizeX=get(destPlot,'XLim');
x=(pos(1)-dims(1))*sizeX(2)/(dims(3));
%evalin('base','delete(xline)');
evalin('base','delete(lineText)');
%Check if mouse is inside plot
if x>sizeX(1) && x<sizeX(2)
    Y=get(destPlot,'YLim');
    %xline=line([x,x],Y,'Color',[.2,.4,.8,.8],'LineWidth',2,'Parent',destPlot);
    lineText=text(x,Y(2)-1000,['T = ' num2str(round(x))],'BackGroundColor',[.2,.4,.8,.8],'Parent',destPlot);
    %assignin('base','xline',xline)
    assignin('base','lineText',lineText);
end
set(destPlot,'Units',units);

end