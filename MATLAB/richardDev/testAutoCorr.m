%% Example illustration from https://www.cs.berkeley.edu/~klein/papers/lagrange-multipliers.pdf
%Plot planes y=-1 and z=-1
% figure
% grid on
% hold on
% axis vis3d
% xlim([-2 2]);
% ylim([-2 2]);
% zlim([-2 2]);
% 
% 
% x=linspace(-1,1);
% z=linspace(-1,1);
% [x,z]=meshgrid(x,z);
% y=-1*ones(size(x,1),size(x,2));
% mesh(x,y,z,y);
% 
% x=linspace(-1,1);
% y=linspace(-1,1);
% [x,y]=meshgrid(x,y);
% z=-1*ones(size(y,1),size(y,2));
% mesh(x,y,z);
% 
% %Plot sphere of radius r
% r=sqrt(2);
% theta=linspace(0,2*pi);
% phi=linspace(0,pi);
% [phi,theta]=meshgrid(phi,theta);
% x=r*cos(theta).*sin(phi);
% y=r*sin(theta).*sin(phi);
% z=r*cos(phi);
% keyin='y';
% 
% 
% while(keyin ~='x')
%     hold on
%     sphere=mesh(x,y,z);
%     set(sphere,'facecolor','none')
%     keyin=input('Press z or s. X to exit\n','s');
%     if(keyin =='z')
%         r=r+.1;
%     end
%     
%     if(keyin =='s')
%         r=r-.1;
%     end
%     x=r*cos(theta).*sin(phi);
%     y=r*sin(theta).*sin(phi);
%     z=r*cos(phi);
%     
%     set(sphere,'Visible','off');
% end
% close all

% load officetemp
% 
% tempC = (temp-32)*5/9;
% 
% tempnorm = tempC-mean(tempC);
% 
% fs = 2*24;
% t = (0:length(tempnorm) - 1)/fs;
% 
% plot(t,tempnorm)
% xlabel('Time (days)')
% ylabel('Temperature ( {}^\circC )')
% axis tight
% 
% figure
% [autocor,lags] = xcorr(tempnorm,3*7*fs,'coeff');
% 
% plot(lags/fs,autocor)
% xlabel('Lag (days)')
% ylabel('Autocorrelation')
% axis([-21 21 -0.4 1.1])
% 
% [pksh,lcsh] = findpeaks(autocor);
% short = mean(diff(lcsh))/fs
% 
% [pklg,lclg] = findpeaks(autocor, ...
%     'MinPeakDistance',ceil(short)*fs,'MinPeakheight',0.3);
% long = mean(diff(lclg))/fs
% 
% hold on
% pks = plot(lags(lcsh)/fs,pksh,'or', ...
%     lags(lclg)/fs,pklg+0.05,'vk');
% hold off
% legend(pks,[repmat('Period: ',[2 1]) num2str([short;long],0)])
% axis([-21 21 -0.4 1.1])
% 
clear all
vidFolder='../main-stable/data/UEye/richard1-ueye/';
[gtTrace,gtHR,gtTime]=loadPPG(vidFolder);
gtTime=gtTime'/1000;
gtTrace=gtTrace';
gtHR=gtHR';
low=0;
high=gtTime(end);
% figure(1);
% plot(gtTime,gtHR);

limits=gtTime>=low & gtTime<=high;
gtHRConst=gtHR(limits);
gtTraceConst=gtTrace(limits);
tracesConst=gtTrace(limits);
gtTimeConst=gtTime(limits);
% figure(2);
% plot(gtTimeConst,gtHRConst);

nSamp=numel(gtTraceConst);
t=gtTimeConst;
Fs=1/mean(diff(t));
x=gtTraceConst;


%nSamp = 64;
% Fs = 1024;
% SNR = 40;
% rng default
% 
 %t = (0:nSamp-1)/Fs;
% 
 %x = sin(2*pi*t*50);
 %x=sin(1000*t);
% gtTimeConst=t;
% [Pxx,f] = periodogram(x,hann(nSamp),[],Fs);
% 
% meanfreq(Pxx,f);

%x=[2 3 -1];
%x=randn(size(x));
%plot(gtTimeConst,x);
close all;
x=1:.1:25;
y=sin(x);
rn=randn(1,numel(x));

% [r,r1,r2]=autocorrd(y,[]);

figure(1)
plot(x,y,'LineWidth',6);
xlabel('x','FontSize',32);
ylabel('y','FontSize',32);
saveas(gcf,'/home/richard/ownCloud/PhD/Papers/autocorr1.png');
%title('y=sin(x)');

figure(2)
plot(x,rn,'LineWidth',2);

ylabel('r_n','FontSize',32);
xlabel('x','FontSize',32)
%title('r_n=rand(1,N)');
saveas(gcf,'/home/richard/ownCloud/PhD/Papers/autocorr2.png');

figure(3)
hold on;
corr_y=xcorr(y);
plot(corr_y,'LineWidth',6,'Color',[0.55 0.35 0.3]);
mu=mean((corr_y).^2);
xlabel('k=-(N-1):N-1','FontSize',24);
ylabel(['Mean squared=' num2str(round(mu,2))],'FontSize',22);
%title('Autocorrelation of y');
hold off;
saveas(gcf,'/home/richard/ownCloud/PhD/Papers/autocorr3.png');

figure(4)
hold on;
corr_rn=xcorr(rn);
mu=mean((corr_rn).^2);
ylabel(['Mean squared=' num2str(round(mu,2))],'FontSize',22);
plot(corr_rn,'LineWidth',2,'Color',[0.5 0.35 0.3]);
%title('Autocorrelation of r_n');
xlabel('k=-(N-1):N-1','FontSize',24);
hold off;
saveas(gcf,'/home/richard/ownCloud/PhD/Papers/autocorr4.png');
%R=[r r];

% figure(2);
% newgtTime=[-gtTimeConst gtTimeConst];
% 
% %set(gca,'Color',[0.0 0.0 0.0]);
% subplot(3,1,1);
% hold on
% plot(gtTimeConst,r);
% plot(-gtTimeConst,r);
% %p2=plot(gtTimeConst,R);
% %plot(-gtTimeConst,R*5000);
% title('Rxx');
% %legend([p1 p2],['Rxx' 'Rxx estimated'])
% 
% subplot(3,1,2);
% hold on
% plot(gtTimeConst,r1);
% plot(-gtTimeConst,r1);
% title('\delta(Rxx)');
% 
% subplot(3,1,3);
% hold on
% plot(gtTimeConst,r2);
% plot(-gtTimeConst,r2);
% title('\delta^2(Rxx)');
% 
% 
% [Pxx,f] = periodogram(r,hann(numel(r)),[],Fs);
% f=meanfreq(Pxx,f);
% disp('Mean GT HR(bpm)     Mean freq from autocorr');
% disp('-------------------------------------------');
% disp([ num2str(mean(gtHR)) '           ' num2str(60*f)]);
% close all;