function [ pulse] = gen_ppg_pulse( fitresult,varargin)
%% Generate a pulse from the given fourier model

%Evaluate the model
pulse=feval(fitresult.fmodel,fitresult.pulseTime);
%Trim the trailing tail. Creates problem when generating sequence of pulses
[pk,locpks]=findpeaks(pulse);

[tr,loctr]=findpeaks(-pulse);
loctr=loctr(loctr>locpks);


% hold on
% plot(fitresult.pulseTime(1:loctr),pulse(1:loctr));
% plot(fitresult.pulseTime,newPulse);

%If time series is provided, generate the pulse for this time series
nargin=length(varargin);
if nargin>1
    %Time series should be provided here
    time=varargin{1};
    %Adjust pulse length 
    if nargin ==2
        desiredPulseTime=varargin{2};
        pulse=adjustSignalLength(fitresult.pulseTime(1:loctr),pulse(1:loctr),desiredPulseTime);
    else
        pulse=adjustSignalLength(fitresult.pulseTime(1:loctr),pulse(1:loctr),fitresult.pulseTime);
    end
    
end
end

