

close all;clear;clc;
N = 5000;

% sig(1,:)=randn(1,5000); 
% sig(2,:)=randn(1,5000);
addpath('../tools/ICA/');
setPath_ICA('../tools/ICA/');
sig=gensig_ica(N,'off');  

figure;  
subplot(4,1,4);
    plot(sig(4,[N-499:N])); ylabel('s4');
    axis([-inf,inf,-5,5]);
subplot(4,1,3); 
    plot(sig(3,[N-499:N])); ylabel('s3');
    axis([-inf,inf,-5,5]);
subplot(4,1,2); 
    plot(sig(2,[N-499:N])); ylabel('s2');
    axis([-inf,inf,-5,5]);
subplot(4,1,1); 
    plot(sig(1,[N-499:N])); ylabel('s1'); title('\bf source signals');
    axis([-inf,inf,-5,5]);
   

A=rand(size(sig,1));           
MixedSig=A*sig;                
% ????????????
figure;  
subplot(4,1,4); 
    plot(MixedSig(4,[N-499:N])); ylabel('x4');
subplot(4,1,3); 
    plot(MixedSig(3,[N-499:N])); ylabel('x3');
subplot(4,1,2);
    plot(MixedSig(2,[N-499:N])); ylabel('x2');
subplot(4,1,1); 
    plot(MixedSig(1,[N-499:N])); ylabel('x1'); title('\bf mixed signals');
    
tic
w = rand(size(MixedSig,1));w=w/norm(w);

OverValue=0.000001;  maxIter = 400;
[Sig,W,A,SelectedSignal]=simpleICA(MixedSig,maxIter,OverValue,'choosebestcomponent',false);

%[Sig,A,WeightMatrix]=fastica(MixedSig,'approach','symm','g','tanh');
toc



figure;  
subplot(4,1,4);
    plot(Sig(4,[N-499:N])); ylabel('y4');
   % axis([-inf,inf,-5,5]);
subplot(4,1,3); 
    plot(Sig(3,[N-499:N])); ylabel('y3');
   % axis([-inf,inf,-5,5]);
subplot(4,1,2); 
    plot(Sig(2,[N-499:N])); ylabel('y2');
    %axis([-inf,inf,-5,5]);
subplot(4,1,1); 
    plot(Sig(1,[N-499:N])); ylabel('y1');
    title('\bf separated sources');
    %axis([-inf,inf,-5,5]);

fprintf('==============================================\n');
fprintf('            Results of calculation            \n');
fprintf('==============================================\n');
%disp(W'*V*A);
%fprintf('Matrix of Separation Performance ...\n');
%fprintf('Performance index: %g\n',CTPI(W'*V*A,'norm'));
