function [randomPoint]= getRandomPointInPlane(position, normal, radius)
randomPoint=[0;0;0];
while  norm(randomPoint)<0.01
    rnd=rand(3,1);
    random(1) = 2.0 * rnd(1) - 1.0;
    random(2) = 2.0 * rnd(2) - 1.0;
    random(3) = 2.0 * rnd(3) - 1.0;
    randomPoint=cross(random, normal);
end
randomPoint=randomPoint';
randomPoint=randomPoint/norm(randomPoint);
randomPoint = randomPoint .* radius * sqrt(rand(1,1));
randomPoint =randomPoint + position;
end