function analyseGait(varargin)
%Analyse gait using source separation from ltmm and umwedb databases
%from physiobank
% analyseGait('db',<dbname>(umwdb),'record',[<recordNum>(1)],'physiobankFolder',<physiobank folder>)
% record=-1 analayses all the records
close all;
physiobankFolder='J:/src/physiobank/';
db='ltmm'; %ltmm or umwdb
addpath('../main-stable');
recordNum=4;%-1 for all records
i=1;
while(nargin > i)
    if(strcmp(varargin{i},'physiobankFolder')==1)
        physiobankFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'db')==1)
        db=varargin{i+1};
    end
    if(strcmp(varargin{i},'recordNum')==1)
        recordNum=varargin{i+1};
    end
    i=i+1;
end

%Tidy up the parameters
if strcmp(physiobankFolder(end),'/')~=1 && strcmp(physiobankFolder(end),'\')~=1
    physiobankFolder=[physiobankFolder '/'];
end
if recordNum<10
    recordNum=['0' num2str(recordNum)];
else
    recordNum=num2str(recordNum);
end

%% Read the records from the database
%First read the files and match with the record number
%Prefix for filenames  %CO0__ and si__.*, respectively
isLTMM=true;
if strcmp(db,'ltmm')==1
    nameprefix='LabWalks/fl0';
    exts={'dat','hea'};
    namesuffix='_base';
    isLTMM=true;
end
if strcmp(db,'umwdb')==1
    nameprefix='si';
    exts={'norm','slow','fast','metnrm','metslw','metfst'};
    isLTMM=false;
end

% list=dir([physiobankFolder db '/' nameprefix '*']);
% cells=struct2cell(list);
mu0 = 1;
lambda0 = 1;
gamma = 100;
learningRate = 1e2;
OverValue=1e-4;  maxIter = 100;
threshold = 1;

if recordNum~=-1
    %Analyse single record
    recordName=[nameprefix recordNum namesuffix]
    %Read the record
    
    if isLTMM
        [tm,sig,Fs]=rdsamp([physiobankFolder db '/' recordName]);
        sig=sig';
        n=size(sig,1);
        %First 4 channels contain the accelerometer readings
        %that we will use
        %vertical acc, medio-lateral and anterior-posterior
        %=pitch, yaw and roll
        for i=1:3
            subplot(3,1,i)
            plot(tm,sig(i,:))
        end
        w=randn(3,1);
        w=w/norm(w);
        [gaitSignal,W,initialA,selectedIndex]=...
            multiobj(sig(1:3,:), tm, threshold, w, learningRate, mu0, lambda0, gamma, maxIter, OverValue,false);
        figure;
%         plot(smooth(gaitSignal))
level=4;
    [c,l] = wavedec(gaitSignal,level,'db8');
    [d1 d2] = detcoef(c,l,1:level);
    d1up = dyadup(d1,0);
    d2up = dyadup(dyadup(d2,0),0);
    subplot(311)
    plot(gaitSignal)
    subplot(312)
    plot(d1up);ylabel('d1'),xlim([400 600])
    subplot(313),
    plot(d2up);ylabel('d2'),xlim([400 600])
    else
        
    end
else
    %Code for analysing all records at once
end

end