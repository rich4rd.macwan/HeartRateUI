function analyseCA(varargin)
WINDOW_LENGTH_SEC=30;
vidFolder='D:/IUT/subject22/vid2/';
ICAWIN=true;
DOFILTER=false;
keyword='skin';
silent=false;
extractICA=false;
extractCICA=true;
i=1;
analyseWs=true;
addpath('../main-stable');
while(nargin > i)
    if(strcmp(varargin{i},'vidFolder')==1)
        vidFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'silent')==1)
        silent=varargin{i+1};
    end
    if(strcmp(varargin{i},'extractICA')==1)
        extractICA=varargin{i+1};
    end
    if(strcmp(varargin{i},'extractCICA')==1)
        extractCICA=varargin{i+1};
    end
    if(strcmp(varargin{i},'keyword')==1)
        keyword=varargin{i+1};
    end
    i=i+1;
end

disp(['Starting analysis for ' vidFolder]);
%% Extract pulse and HR
if extractICA
    method='ICA';
    
    if strcmp(keyword,'')==1
        matFileName=['pulseTrace' method];
    else
        matFileName=['pulseTrace' method '_' keyword];
    end
    
    getPulseSignalFromTrace('vidFolder',vidFolder,'outFolder',...
        vidFolder,'silent',true,'keyword',keyword,...
        'WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC,'ICAWIN',ICAWIN,...
        'DOFILTER',DOFILTER,'method',method);
    
    
    [~,~,~,~,excel_row]=getHRFromPulse('vidFolder',vidFolder,...
        'matFileName',matFileName,'IUTMode',false,...
        'silent',true, 'WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC,...
        'ICAWIN',ICAWIN,'DOFILTER',DOFILTER);
end
%

if extractCICA
    method='CICA';
    if strcmp(keyword,'')==1
        matFileName=['pulseTrace' method];
    else
        matFileName=['pulseTrace' method '_' keyword];
    end
    getPulseSignalFromTrace('vidFolder',vidFolder,'outFolder',...
        vidFolder,'silent',true,'keyword',keyword,...
        'WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC,'ICAWIN',ICAWIN,...
        'DOFILTER',DOFILTER,'method',method);
    
    
    [~,~,~,~,excel_row]=getHRFromPulse('vidFolder',vidFolder,...
        'matFileName',matFileName,'IUTMode',false,...
        'silent',true, 'WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC,...
        'ICAWIN',ICAWIN,'DOFILTER',DOFILTER);
end

%% Perform Analysis
if strcmp(keyword,'')~=1
    suffix=['_' keyword];
else
    suffix=keyword;
end
if ~DOFILTER
    filterSuffix='_Unfilt';
end

%Load ICA and CICA data
method='ICA';
disp(['Analysing : ' vidFolder 'pulseTrace' method '-win' num2str(WINDOW_LENGTH_SEC) suffix filterSuffix]);
load([vidFolder 'pulseTrace' method '-win' num2str(WINDOW_LENGTH_SEC) suffix filterSuffix]);

welchICA=welchData;
RsICA=Rs;
HRsICA=[HR_sensor;HR_PPG;HR_RPPG;HR_RPPG_filtered];

method='CICA';
clear Rs
clear welchData
load([vidFolder 'pulseTrace' method '-win' num2str(WINDOW_LENGTH_SEC) suffix filterSuffix]);
welchCICA=welchData;
RsCICA=Rs;
HRsCICA=[HR_sensor;HR_PPG;HR_RPPG;HR_RPPG_filtered];
W=struct2array(welchICA);
X1=(meshgrid(linspace(timeTrace(1),timeTrace(end),numel(welchICA)),1:numel(welchICA(1).rPPG_freq)))';
Y1=cell2mat([arrayfun(@(x) x.rPPG_freq,welchICA,'UniformOutput',false)]');
Z1=cell2mat([arrayfun(@(x) x.rPPG_power,welchICA,'UniformOutput',false)]');


X2=(meshgrid(linspace(timeTrace(1),timeTrace(end),numel(welchCICA)),1:numel(welchCICA(1).rPPG_freq)))';
Y2=cell2mat([arrayfun(@(x) x.rPPG_freq,welchCICA,'UniformOutput',false)]');
Z2=cell2mat([arrayfun(@(x) x.rPPG_power,welchCICA,'UniformOutput',false)]');

if ~silent
    %Plot ICA 3D Welch
    method='ICA';
    fig1=figure(1);
    subplot(2,2,1);
    grid on;
    mesh(X1,Y1,Z1);
    icawelch=gca;
    title(method);
    xlabel('Time'); ylabel('Freq_{rPPG}'); zlabel('Power_{rPPG}');
    xlim auto; ylim auto; zlim auto;
    
    %Plot CICA 3D Welch
    method='CICA';
    subplot(2,2,2);
    grid on;
    mesh(X2,Y2,Z2);
    cicawelch=gca;
    title(method);
    xlabel('Time'); ylabel('Freq_{rPPG}'); zlabel('Power_{rPPG}');
    xlim auto; ylim auto; zlim auto;
end

welchData=struct('X1',X1,'Y1',Y1,'Z1',Z1,'X2',X2,'Y2',Y2,'Z2',Z2);

if ~silent
    %Plot autocorrelations of ICA, CICA and Chrom
    s=subplot(2,2,3:4);
    %xline=line([0 0],[0 1]);
    lineText=text(0,0,''); %Display hovering text for T=t
    assignin('base','lineText',lineText);
    set(s,'ButtonDownFcn',{@ica_cica_click_hndlr,welchData,[icawelch cicawelch],[RsICA;RsCICA]});
  
    
    hold on; grid on;
    p1=plot(X1(:,1),[RsICA(:).ICA],'s','MarkerSize',12,'Color',[.5 .25 .25]);
    p2=plot(X1(:,1),[RsICA(:).ICA1],'ro');
    p3=plot(X1(:,1),[RsICA(:).ICA2],'go');
    p4=plot(X1(:,1),[RsICA(:).ICA3],'bo');
    p5=plot(X1(:,1),[RsCICA(:).CICA],'o','MarkerSize',12,'Color',[.7 .6 .15]);
    p6=plot(X1(:,1),[RsICA(:).Chrom],'x','MarkerSize',12,'Color',[.15 .3 .15]);
    legend([p1,p2,p3,p4,p5,p6],...
        'ICA','ICA1','ICA2','ICA3','CICA','Chrom')
      set(fig1,'WindowButtonMotionFcn',{@ica_cica_move_hndlr,s});
end
if ~silent
    %Plot HR data also
    method='ICA';
    fig2 = figure(2);
    subplot(2,1,1)
    hold on;
    grid on;
    p1 = plot(welchData.X1(:,1), HRsICA(1,:), 'b-*');
    p2 = plot(welchData.X1(:,1), HRsICA(2,:), 'g-*');
    p3 = plot(welchData.X1(:,1), HRsICA(3,:), 'c-*');
    p4 = plot(welchData.X1(:,1), HRsICA(4,:), 'r-*');
    ylim([40 210]);
    legend([p1, p2, p3, p4], 'HR sensor', 'HR PPG', 'HR rPPG', 'HR rPPG filtered');
    title(method);
    
    method='CICA';
    subplot(2,1,2)
    hold on;
    grid on;
    p1 = plot(welchData.X2(:,1), HRsCICA(1,:), 'b-*');
    p2 = plot(welchData.X2(:,1), HRsCICA(2,:), 'g-*');
    p3 = plot(welchData.X2(:,1), HRsCICA(3,:), 'c-*');
    p4 = plot(welchData.X2(:,1), HRsCICA(4,:), 'r-*');
    ylim([40 210]);
    legend([p1, p2, p3, p4], 'HR sensor', 'HR PPG', 'HR rPPG', 'HR rPPG filtered');
    title(method);
%     close(fig2)
end

%Analyse Ws
analyseWs=true;
display3DW=false;
if analyseWs
    figw=figure(3);
    color=[.9 .3 .3 .5;.3 .9 .3 .5;.3 .3 .9 .5];
    colormap(color(:,1:3));
    
    for i=1:numel(RsICA)
        %Display the RGB weights of the selected component
        %ICA
        for j=1:size(RsICA(i).A,1)
            w=RsICA(i).A(j,:);
            w=w/norm(w);
            if j==RsICA(i).CICA
                %selected component
                if(display3DW)
                    subplot(5,2,1);
                    hold on;grid on
                    quiver3(0,0,0,w(1),w(2),w(3),'Color',[.4 .4 .4],'LineWidth',3);
                    hold off;
                end
                selectedWs(i,:)=w;
            else
                if(display3DW)
                    subplot(5,2,1);
                    hold on;grid on
                    quiver3(0,0,0,w(1),w(2),w(3),'Color',color(j,:),'LineWidth',2);
                    hold off;
                end
                subplot(5,2,3:4);
                hold on;grid on
                %             plot(i,abs(w),'x','Color',color(j,:));
                %             hold off;
            end
        end
        %CICA
        %quiver3(0,0,0,RsCICA(i).W(1),RsCICA(i).W(2),RsCICA(i).W(3),'Color',[.4 .8 .8],'LineWidth',3,'LineStyle','-.');
        cicaWs(i,:)=RsCICA(i).W/norm(RsCICA(i).W);
        cicaAs(i,:)=RsCICA(i).A/norm(RsCICA(i).A);
    end
    subplot(5,2,1);
    xlabel('R');ylabel('G');zlabel('B');
    set(figw,'Units','normalized','Position',[0 0 1 1]);
    tokens=strsplit(vidFolder,'/');
    if strcmp(tokens(end-1),'vid2')==1
        %IUT dataset
        filename=tokens{end-2};
    else
        filename=tokens{end-1};
    end
    title(['Window wise Ws : ' filename] );
    
    subplot(5,2,3:4);
    bar(welchData.X1(:,1),abs(selectedWs),'EdgeColor','none');
    xlim([-1 welchData.X1(end,1)+1]);
    title('Selected weights magnitudes');
    subplot(5,2,5:6);
    hold on;
    grid on;
    %p1 = plot(welchData.X1(1,:), HRsICA(1,:), 'b-*');
    p2 = plot(welchData.X1(:,1), HRsICA(2,:), 'g-*');
    %p3 = plot(welchData.X1(1,:), HRsICA(3,:), 'c-*');
    p4 = plot(welchData.X1(:,1), HRsICA(4,:), 'r-*');
    xlim([-1 welchData.X1(end,1)+1]);
    ylim([min(HRsICA(4,:)) max(HRsICA(4,:))]);
    legend([ p2, p4], 'HR PPG', 'HR rPPG filtered');
    title('ICA');
    
    subplot(5,2,7:8);
%    bar(abs(cicaWs),'EdgeColor','none');
     bar(welchData.X2(:,1),abs(cicaWs),'EdgeColor','none');
     xlim([-1 welchData.X2(end,1)+1]);
    title('Selected weights magnitudes');
    subplot(5,2,9:10);
    hold on;
    grid on;
    %p1 = plot(welchData.X1(1,:), HRsICA(1,:), 'b-*');
    p2 = plot(welchData.X2(:,1), HRsCICA(2,:), 'g-*');
    %p3 = plot(welchData.X1(1,:), HRsICA(3,:), 'c-*');
    p4 = plot(welchData.X2(:,1), HRsCICA(4,:), 'r-*');
    xlim([-1 welchData.X2(end,1)+1]);
    ylim([min(HRsCICA(4,:)) max(HRsCICA(4,:))]);
    legend([ p2, p4], 'HR PPG', 'HR rPPG filtered');
    title('CICA');
end


%Save W figure data
%resultFolder = [vidFolder 'results/'];
%saveas(figw,[resultFolder method '_win' num2str(WINDOW_LENGTH_SEC) suffix  filterSuffix '_Ws.fig']);

%Save all to temp folder
% filename=['C:/Users/sager-np4658/Documents/HeartRateUI/WAnalysisResults/' filename '.png'];
% saveas(figw,filename);
% close(figw);
end