function [pos]=ica_cica_click_hndlr(src,evt,welchData,destPlot,Rs)
pos=get(src,'currentpoint');
pos=(pos(1,:));
%text(pos(1,1),pos(1,2),num2str(pos(1,1)));
%index of time
x=pos(1);
%Find index of nearest value in X1
[M,I]=min(abs(welchData.X1(1,:)-x));
x=I;
if x>0 && x<numel(welchData.X1)
    fig=figure(42);
    cla;
    RsCICA=Rs(2,:);
    W=RsCICA(x).W;
    c1=[1 0 -1]; c1c=[1 0 1]';
    c2=[0 1 -1]; c2c=[0 1 1]';
    g1=c1*(W*W')*c1c;
    g2=c2*(W*W')*c2c;
    text(.3,.3,['g1 = ' num2str(g1) ', g2 = ' num2str(g2)],'BackGroundColor',[.4 .7 .4]);

    hold on;
    p1=plot(welchData.Y1(:,x),welchData.Z1(:,x));
    p2=plot(welchData.Y2(:,x),welchData.Z2(:,x));
    legend([p1,p2],'ICA','CICA');
    [pksICA,locsICA]=findpeaks(welchData.Z1(:,x),welchData.Y1(:,x),'SortStr','descend');
    [pksCICA,locsCICA]=findpeaks(welchData.Z2(:,x),welchData.Y2(:,x),'SortStr','descend');
    
    %Display bpm
    if numel(locsICA)>0
        text(locsICA(1)*1.1,pksICA(1)*1.05,[num2str(round(60*locsICA(1))) ' bpm'],'BackGroundColor',[get(p1,'Color') .6]);
    end
    if numel(locsCICA)>0
        text(locsCICA(1)*.8,pksCICA(1)*1.05,[num2str(round(60*locsCICA(1))) ' bpm'],'BackGroundColor',[get(p2,'Color') .6]);
    end
    
    
    %Highlight ica mesh
    hold(destPlot(1),'on');
    cla(destPlot(1));
    xdata=welchData.X1;
    xi=xdata(:,x);
    xdata(:)=nan;
    xdata(:,x)=xi;
    mesh(destPlot(1),welchData.X1,welchData.Y1,welchData.Z1);
    plot3(destPlot(1),xdata,welchData.Y1,welchData.Z1,'LineWidth',3,'Color',[.7,.3,.3]);
    hold(destPlot(1),'off');
    
    %Highlight cica mesh
    hold(destPlot(2),'on');
    cla(destPlot(2));
    xdata=welchData.X2;
    xi=xdata(:,x);
    xdata(:)=nan;
    xdata(:,x)=xi;
    mesh(destPlot(2),welchData.X2,welchData.Y2,welchData.Z2);
    plot3(destPlot(2),xdata,welchData.Y2,welchData.Z2,'LineWidth',3,'Color',[.7,.3,.3]);
    
    
    

    title(['T = ' num2str(round(welchData.X1(1,x)))]);
    %         if numel(pks)>0
    %             hold(destPlot,'on')
    %             plot3(destPlot,x,welchData(x).rPPG_freq(locs(1)),welchData(x).rPPG_power(locs(1))*1.1,'^');
    %         end
end
end