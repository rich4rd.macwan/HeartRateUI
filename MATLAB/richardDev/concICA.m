function [c,ceq,gradc,gradceq] = concICA(w,X,zeta,Fs,Wchrom)
%For multiple constraints, create a vector
%Autocorrelation ineq constraint
%zeta<mean(r^2)
[r,r1,~]=autocorrd(w'*X,X,Fs);
c(1)=zeta(1)-mean(r.*r);
% c=0;
% c(1)=zeta(1)-mean(r.*r);
%Inequality constraint |w2|>|w3|+zeta
%or |w3|+zeta-|w2|<0
% c(2)=abs(w(3))-abs(w(1))+zeta(2);

%CHROM
%  C=abs(w)-abs(Wchrom);
C=w-Wchrom;
c(2)=norm(C)-zeta(2);
% c(2)=abs(w(3))-abs(w(1))+zeta(1);
%Equality constraint h(w)=1
ceq=mean((w'*X).^2)-1;
if nargout >2
    %For multiple constraints, add gradients columnwise
%      gradc=r1;
%      gradc=zeros(3,1);
%       gradc=[r1,[0;-sign(w(2));sign(w(3))]];
%          gradc=[r1,[-sign(w(1));0;sign(w(3))]];
     %CHROM
     gradc=[r1,C/c(1)];
    gradceq=2*X*(X'*w);
end
end