function initxlWrite(root)
%% Initialisation of POI Libs
% Add Java POI Libs to matlab javapath
javaaddpath([root '/poi_library/poi-3.8-20120326.jar']);
javaaddpath([root '/poi_library/poi-ooxml-3.8-20120326.jar']);
javaaddpath([root '/poi_library/poi-ooxml-schemas-3.8-20120326.jar']);
javaaddpath([root '/poi_library/xmlbeans-2.3.0.jar']);
javaaddpath([root '/poi_library/dom4j-1.6.1.jar']);
javaaddpath([root '/poi_library/stax-api-1.0.1.jar']);
end