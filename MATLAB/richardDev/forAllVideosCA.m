function forAllVideosCA()

method='ICA';
%method='Green';
%method='Chrom';
%% Stuff for writing to spreadsheet
addpath('xlwrite');
initxlWrite([pwd '/xlwrite']);
addpath('../tools/');
%% Data Generation for XLSX: IMP to verify this!!!!
% Define an xls name
fileName = [method 'newresults.xlsx'];
sheetName = 'SkinThresh=-1';
startRange = 'B4';

%% Le2i dataset
% Le2iFolder='/run/media/richard/Le2iHDD/Le2i/';
% %cd(Le2iFolder);
% dirs=dir([Le2iFolder '/*gt']);
% d=dir([Le2iFolder '/aft*']);
% dirs=[dirs;d];
% %Iterate through all directories, perform ICA and save traces
% for i=1:size(dirs)
%     %cd(dirs(i).name);
%     xlsRow={dirs(i).name};
%     vidFolder=[Le2iFolder dirs(i).name '/'];
%     %matFileNames={'rgbTraces', 'rgbTraces_crop', 'rgbTraces_skin'};
%     matFileNames={'rgbTraces_skin'};%, 'pulseTraceRGB_crop', 'pulseTraceRGB_skin'};
%     if strcmp(method,'Chrom')==1
%         matFileNames={'pulseTraceChrom_skin'};
%     end
%     if strcmp(method,'Green')==1
%         matFileNames={'pulseTraceGreen_skin'};
%     end
%     for j=1:size(matFileNames,2)
%         matFileName=matFileNames{j};
%         %excel_row=windowedCA('vidFolder',vidFolder,'matFileName',matFileName,'method',method,'silentMode',true);
%         %excel_row=ica_rppg('vidFolder',vidFolder,'matFileName',matFileName,'method',method,'IUTMode',false,'silentMode',true);
% 
%         %excel_row={2.3	0.2725	0.24638	0.55072	7.5521	2.4111	2.7146	17	22};
% %         getTraceFromFileSeq('vidFolder',vidFolder,'outFolder',vidFolder,'matFileName',matFileName,'silent',true,'keyword','skin');
%         getPulseSignalFromTrace('vidFolder',vidFolder,'outFolder',vidFolder,'matFileName',matFileName,'silent',true,'keyword','skin');
%         [HR_sensor,HR_PPG,HR_RPPG,HR_RPPG_filtered,excel_row]=getHRFromPulse('vidFolder',vidFolder,'matFileName',matFileName,'method',method,'IUTMode',false,'silent',true);         
%         if j==1
%             xlsRow(1,j+1:j+9)=excel_row;
%         else
%             xlsRow=excel_row;
%         end
%         xlwrite(fileName, xlsRow, sheetName, startRange);
%         %modify startRange
%         if j==2
%             startRange(1)=char(startRange(1)+10);
%         else
%             startRange(1)=char(startRange(1)+11);
%         end
%         
%     end
%     %Reset excel column, carriage return
%     startRange(1)='B';
%     %modify startRange row
%     startRange=[startRange(1) num2str(str2double(startRange(2:end))+1)]
%     
% end

%% IUT Dataset
IUTFolder='/run/media/richard/Le2iHDD/IUT/';

%for i=[1 3:27 30:49]
startRange = 'B21';
for i=[1 3:27 30:49]
    if i<10
        vidFolder=[IUTFolder 'subject' num2str(0) num2str(i) '/vid2/'];
        vidName=['subject' num2str(0) num2str(i)];
    else
        vidFolder=[IUTFolder 'subject' num2str(i) '/vid2/'];
        vidName=['subject' num2str(i)];
    end
    xlsRow={vidName};
    %matFileNames={'rgbTraces', 'rgbTraces_crop', 'rgbTraces_skin'};
    %matFileNames={'pulseTraceRGB', 'pulseTraceRGB_crop', 'pulseTraceRGB_skin'};
    matFileNames={'rgbTraces_skin'};
     if strcmp(method,'Chrom')==1
        matFileNames={'pulseTraceChrom_skin'};
     end
    if strcmp(method,'Green')==1
        matFileNames={'pulseTraceGreen_skin'};
    end
    for j=1:size(matFileNames,2)
        matFileName=matFileNames{j};
        %excel_row=windowedCA('vidFolder',vidFolder,'matFileName',matFileName,'method',method);
        %excel_row=cica_rppg('vidFolder',vidFolder,'matFileName',matFileName,'method',method,'IUTMode',true,'silentMode',true)
        
       % getTraceFromFileSeqIUTDataset('vidFolder',vidFolder,'matFileName',matFileName,'silent',true);
        getPulseSignalFromTrace('vidFolder',vidFolder,'outFolder',vidFolder,'matFileName',matFileName,'silent',true,'keyword','skin');
        [HR_sensor,HR_PPG,HR_RPPG,HR_RPPG_filtered,excel_row]=getHRFromPulse('vidFolder',vidFolder,'matFileName',matFileName,'method',method,'IUTMode',true,'silent',true);
        excel_row
        %excel_row={2.3	0.2725	0.24638	0.55072	7.5521	2.4111	2.7146	17	22};
        if j==1
            xlsRow(1,j+1:j+9)=excel_row;
        else
            xlsRow=excel_row;
        end
        xlsRow
        xlwrite(fileName, xlsRow, sheetName, startRange);
        %modify startRange
        if j==2
            startRange(1)=char(startRange(1)+10);
        else
            startRange(1)=char(startRange(1)+11);
        end
        
    end
    %Reset excel column, carriage return
    startRange(1)='B';
    %modify startRange row
    startRange=[startRange(1) num2str(str2double(startRange(2:end))+1)]
    
end
end