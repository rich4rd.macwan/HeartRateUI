% Test EMD and HHT on rPPG 
% yannick.benezeth@u-bourgogne.fr 
% 07/06/2016 
 
clear all; 
close all; 
clc; 
 
addpath(genpath('../tools/tftb-0.2')); 
 
%%%%%%%%%%%%%%%%% 
%% Initialisation 
%%%%%%%%%%%%%%%%% 
SHOW_PLOTS = 1; 
RPPG = 1; % if 0 then work with PPG 
folderName = '../main-stable/data/RichardSummer2015/after-exercise/'; 
 
% select time to analyse 
% use endT=-1 to analyse until the end 
startT = 0; 
endT = -1; 
 
Fs = 30; 
inc = 1/Fs; 
 
%%%%%%%%%%%%%%%%% 
%% load (r)PPG 
%%%%%%%%%%%%%%%% 
if(~RPPG) 
    % load PPG data 
    [trace, gtHR, time] = loadPPG(folderName); 
    time = time / 1000;  % in seconds 
else 
    % load rPPG data 
    [trace, time] = loadrPPG(folderName); 
    trace = trace'; 
    time = time'; 
end; 
 
% select the part to analyse 
if(endT == -1) 
    endT = time(end); 
end 
trace = trace(((time>startT) & (time<endT))); 
time =  time(((time>startT) & (time<endT))); 
 
% interpolate PPG or rPPG traces 
newTime = time(1) : inc : time(end); 
traceInterp = interp1(time,trace,newTime, 'pchip'); 
 
% find peaks 
%[pks, locs]=findpeaks(traceInterp, newTime, 'MinPeakProminence',1); 
 
if (0) 
    % Plot PPG data 
    figure,hold on; 
    title(' (r)PPG trace') 
    xlabel('Time(s)') 
    p1 = plot(newTime,traceInterp); 
    %plot(locs,pks,'X','MarkerSize',12) 
     
    %legend(p1,'rPPG'); 
    %xlim([0 15]); 
    hold off; 
end 
 
 
%%%%%%%%%%%%%%%%%%%%%%%%% 
%% Apply EMD 
%%%%%%%%%%%%%%%%%%%%%%%% 
x = traceInterp; 
Nx = length(x); 
t = 1:Nx; 
[imf,ort,nbits] = emd(x); 
 
% get nrj of each imf in HR range 
for i=1:size(imf,1) 
        x = imf(i,:)'; 
        nfft = Fs*20; % nb of points for FFT 
        shift = round(0.995*nfft); % sliding window shift for spectrogram 
        window = hanning(nfft); 
        [s,f,t,p] = spectrogram(x,window,shift, linspace(0.3, 3.5,nfft), Fs,'yaxis'); 
     
        HR_nrj(i) = sum(sum(abs(s(f>0.3 & f<3.5,:)))); 
     
        % show (r)PPG spectrogram 
        if (1) 
            figure; 
            imagesc(t,f,abs(s).^2); 
            axis xy,colormap('jet'),colorbar, xlabel('Time (s)'), ylabel('Frequency (Hz)'),ylim([0.3 3.5]); 
            title('(r)PPG spectrogram'); 
        end 
%     x = imf(i,:)'; 
%     Nx = length(x); 
%     [tfr, t, f] = tfrsp(x, 1:Nx, Nx, tftb_window(odd(Nx/4),'Hamming')); 
%      
%     HR_nrj(i) = sum(sum(abs(tfr(f>(0.3/Fs) & f<(3.5/Fs),:)))); 
     
    
end 
 
% select best IMF 
[val, ind] = max(HR_nrj); 
x = imf(ind,:)'; 
nfft = Fs*20; % nb of points for FFT 
shift = round(0.995*nfft); % sliding window shift for spectrogram 
window = hanning(nfft); 
[s,f,t,p] = spectrogram(x,window,shift, linspace(0.3, 3.5,nfft), Fs,'yaxis'); 
 
%emd_visu(x,1:Nx,imf,1); 
% [A,f,tt] = hhspectrum(imf(2,:),1:Nx); 
% [im,tt,ff] = toimage(A,f,tt); 
% disp_hhs(im,t); 
 
 
% % time-frequency distributions 
% Nf = 256;% # of frequency bins 
% Nh = 127;% short-time window length 
% w = tftb_window(Nh,'Kaiser'); 
% 
% [s,rs] = tfrrsp(x,T,Nf,w,1); 
% [s,rs1] = tfrrsp(imf(1,:)',T,Nf,w,1); 
% [s,rs2] = tfrrsp(imf(2,:)',T,Nf,w,1); 
% [s,rs3] = tfrrsp(imf(3,:)',T,Nf,w,1); 
% 
% figure(1) 
% 
% subplot(221) 
% imagesc(flipud(rs(1:128,:))) 
% set(gca,'YTick',[]);set(gca,'XTick',[]) 
% xlabel('time') 
% ylabel('frequency') 
% title('signal') 
% 
% subplot(222) 
% imagesc(flipud(rs1(1:128,:))) 
% set(gca,'YTick',[]);set(gca,'XTick',[]) 
% xlabel('time') 
% ylabel('frequency') 
% title('mode #1') 
% 
% subplot(223) 
% imagesc(flipud(rs2(1:128,:))) 
% set(gca,'YTick',[]);set(gca,'XTick',[]) 
% xlabel('time') 
% ylabel('frequency') 
% title('mode #2') 
% 
% subplot(224) 
% imagesc(flipud(rs3(1:128,:))) 
% set(gca,'YTick',[]);set(gca,'XTick',[]) 
% xlabel('time') 
% ylabel('frequency') 
% title('mode #3') 
 
 
%%%%%%%%%%%%%%%%%%%%%%%%% 
%% Freq analysis on PPG (spectrogram matlab) 
%%%%%%%%%%%%%%%%%%%%%%%% 
% x = traceInterp; 
% nfft = Fs*20; % nb of points for FFT 
% shift = round(0.995*nfft); % sliding window shift for spectrogram 
% window = hanning(nfft); 
% [s,f,t,p] = spectrogram(x,window,shift, linspace(0.3, 3.5,nfft), Fs,'yaxis'); 
% 
% % show (r)PPG spectrogram 
% if (SHOW_PLOTS) 
%     figure; 
%     imagesc(t,f,abs(s)); 
%     axis xy,colormap('jet'),colorbar, xlabel('Time (s)'), ylabel('Frequency (Hz)');%,ylim([0.3 3.5]); 
%     title('(r)PPG spectrogram'); 
% end 
% 
% % Get HR from spectrogram 
% [q,nd] = max(10*log10(p)); 
% HR_spec = 60*f(nd); 
% HR_specTime = t; 
% 
% % show HRs 
% if (SHOW_PLOTS) 
%     figure, hold on; 
%     title('Heart Rate'); 
%     plot(HR_specTime,HR_spec,'r'); 
%     ylim([40 210]); 
%     hold off; 
% end 
 
%%%%%%%%%%%%%%%%%%%%%%%%% 
%% Freq analysis on PPG (STFT TFTB) 
%%%%%%%%%%%%%%%%%%%%%%%% 
% x = traceInterp'; 
% Nx = length(x); 
% x = hilbert(x); 
% 
% % spectrogram |STFT|.^2 
% [tfr, t, f] = tfrsp(x, 1:Nx, Nx, tftb_window(odd(Nx/4),'Hamming')); 
% 
% % Get HR from spectrogram 
% [q,nd] = max(10*log10(tfr)); 
% HR_spec = 60*f(nd)*Fs; 
% HR_specTime = t/Fs; 
% 
% % show HRs 
% if (SHOW_PLOTS) 
%     figure, hold on; 
%     title('Heart Rate'); 
%     plot(HR_specTime,HR_spec,'r'); 
%     ylim([40 210]); 
%     hold off; 
% end 
 
% First order moment = instantaneous frequency 
% [fm, B2] = momttfr(tfr, 'tfrsp'); 
% plot(t/Fs, fm*Fs); 
% figure; 
% tfrqview(tfr, x, 1:Nx, 'tfrsp'); 
 
% reassigned spectrogram 
% [tfr, rtfr, hat] = tfrrsp(x, 1:Nx, Nx, tftb_window(odd(Nx/4),'Hamming'), 1); 
% ridges(tfr,hat,1:Nx,'tfrrsp',1); 
% tfrqview(tfr, x, 1:Nx, 'tfrrsp'); 
 
% tfr=tfrstft(x, 1:length(x), length(x), window, 0); 
% tfr=tfrstft(x); 
% load('tfrstft_options.mat'); 
% figure; 
% clf;colormap(SavedColorMap); 
% tfrview(abs(tfr).^2,x,t,method,param); 
%tfrqview(abs(tfr).^2, x, t); 
 
%%%%%%%%%%%%%%%%%%%%%%%%% 
%% Freq analysis on (r)PPG (Scalo - wavelets TFTB) 
%%%%%%%%%%%%%%%%%%%%%%%% 
% x = traceInterp'; 
% x = hilbert(x); 
% Nx = length(x); 
% 
% %scalogram (Morlet wavelets) 
% [tfr,t,f,wt] = tfrscalo(x, 1:length(x), sqrt(length(x))*5, 0.1/Fs, 3.5/Fs, 512); 
 
% reassigned scalogram (very very slow + wrong), why ? 
% tfrrmsc(x, 1:Nx, Nx, 0.5, 1); 
%[tfr, tt, f, wt] = tfrscalo(x, 1:length(x), sqrt(length(x)), 0.1/Fs, 3.5/Fs, 256); 
 
%%%%%%%%%%%%%%%%%%%%%%%%% 
%% Freq analysis on (r)PPG (WVD TFTB) 
%%%%%%%%%%%%%%%%%%%%%%%% 
% x = traceInterp'; 
% x = hilbert(x); 
% Nx = length(x); 
% 
% tfrspwv(x, 1:Nx, Nx, tftb_window(odd(Nx/10),'Kaiser'), tftb_window(odd(Nx/4),'Kaiser'), 1); 
% tfrrspwv(x, 1:Nx, Nx, tftb_window(odd(Nx/10),'Kaiser'), tftb_window(odd(Nx/4),'Kaiser'), 1); 
 