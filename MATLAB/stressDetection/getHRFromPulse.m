function getHRFromPulse(varargin)
% Extract HR from pulse trace
% sliding window based max peak PSD
% arguments can be path of mat file with pulse
% yannick.benezeth@u-bourgogne.fr
% 18/02/2016 (do not use that one -> use main-stable instead)

clear all;
close all;
clc;

disp('Get HR from pulse trace');

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
WINDOW_LENGTH_SEC = 30; % length of the sliding window (in seconds)
STEP_SEC = 1; % step between 2 sliding window position (in seconds)
LOW_F=.3; % low freq for PSD range
UP_F=3; % high freq for PSD range

matFileName = 'pulseTraceChrom';
%matFileName = 'pulseTraceGreen';
%vidFolder = './data/besac/exp1/vid1/';
vidFolder = '/media/yannick/Data/Recherche/Projects/synced/HeartRate/Code/HeartRateUI/MATLAB/main-stable/data/RichardSummer2015/after-exercise/byRomain/';

pulseFile = [vidFolder matFileName '.mat'];
if(nargin==1)
    pulseFile=varargin{1};
end
disp(['Working with  ' vidFolder matFileName '.mat']);

if(exist(pulseFile, 'file') == 2)
    load(pulseFile);
else
    disp('oops, no input file');
    return;
end

traceSize = length(pulseTrace);

% get exact Fs
Fs = 1/mean(diff(timeTrace));

% load PPG
% TODO: in case of mat file in argument of the function -> extract the name
% of the folder from the string
[gtTrace, gtHR, gtTime] = loadPPG(vidFolder);
gtTime = gtTime / 1000;  % in seconds

% figure(1),plot(gtTime, gtTrace, 'b', timeTrace, pulseTrace, 'g', 'Linewidth', 2);
% legend('PPG', 'Pulse trace'),
% title('PPG and Pulse trace');

winLength = round(WINDOW_LENGTH_SEC*Fs); % length of the sliding window for FFT
step = round(STEP_SEC*Fs);

x = pulseTrace;
nfft = Fs*30; % nb of points for FFT
shift = round(0.999*nfft); % sliding window shift for spectrogram
window = hanning(nfft);
[s,f,t,p] = spectrogram(x,window,shift, linspace(0.3, 3.5,nfft), Fs,'yaxis');
if (1)
    figure;
    imagesc(t,f,abs(s));
    axis xy,colormap('jet'),colorbar, xlabel('Time (s)'), ylabel('Frequency (Hz)'),ylim([0.3 3.5]);
    title('remote PPG spectrogram');
end

ind = 0;
for i=1:step:traceSize-winLength
    ind = ind+1;
    
%     if(mod(i,10)==0)
%         disp(['processing window ' num2str(i) ' on ' num2str(traceSize-winLength)]);
%     end
    
    % get start/end index of current window
    startInd = i;
    endInd = i+winLength-1;
    startTime = timeTrace(startInd);
    endTime = timeTrace(endInd);
    
    % get current pulse window
    crtPulseWin = pulseTrace(startInd:endInd);
    crtTimeWin = timeTrace(startInd:endInd);
    
    % get current PPG window
    [~, startIndGt] = min(abs(gtTime-startTime));
    [~, endIndGt] = min(abs(gtTime-endTime));
    
    crtPPGWin = gtTrace(startIndGt:endIndGt);
    crtTimePPGWin = gtTime(startIndGt:endIndGt);
    
    % get exact PPG Fs
    Fs_PPG = 1/mean(diff(crtTimePPGWin));
    
    %     figure(2),plot(crtTimePPGWin, crtPPGWin, 'b', crtTimeWin, crtPulseWin, 'g', 'Linewidth', 2);
    %     legend('PPG', 'Pulse trace'),
    %     title('PPG and Pulse trace');
    
    %% Spectral analysis
    
    %%%%%%%%%%%%
    % with FFT
    %%%%%%%%%%%
    %     % on rPPG
    %     x = crtPulseWin;
    %     N = length(x)*3;
    %     rPPG_freq = [0 : N-1]*Fs/N;
    %     rPPG_spec = abs(fft(x,N)).^2;
    %
    %     % on PPG
    %     x = crtPPGWin;
    %     N = length(x)*3;
    %     PPG_freq = [0 : N-1]*Fs_PPG/N;
    %     PPG_spec = abs(fft(x,N)).^2;
    %
    %     fig3 = figure(3);
    %     clf(fig3);
    %     subplot(1,2,1), hold on; grid on;
    %     plot(rPPG_freq, rPPG_spec), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
    %     title('rPPG FFT')
    %     hold off;
    %     subplot(1,2,2); hold on; grid on;
    %     plot(PPG_freq, PPG_spec), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
    %     title('PPG FFT');
    %     hold off;
    
    
    %%%%%%%%%%%%
    % with periodogram function
    %%%%%%%%%%%
    %     [rPPG_power,rPPG_freq] = periodogram(crtPulseWin,hamming(length(crtPulseWin)),[],Fs,'power');
    %     [PPG_power,PPG_freq] = periodogram(crtPPGWin,hamming(length(crtPPGWin)),[],Fs_PPG,'power');
    
    %     figure(4),
    %     subplot(1,2,1),  grid
    %     plot(rPPG_freq,rPPG_power)
    %     xlim([0.3 3.5]), title('rPPG periodogram'), xlabel('Heart Rate (Hz)')
    %     subplot(1,2,2), grid
    %     plot(PPG_freq,PPG_power)
    %     xlim([0.3 3.5]), title('PPG periodogram'), xlabel('Heart Rate (Hz)');
    
    
    %%%%%%%%%%%%
    % with Welch
    %%%%%%%%%%%
    % RPPG
    [rPPG_power,rPPG_freq] = pwelch(crtPulseWin,[],[],length(crtPulseWin)*3,Fs, 'psd'); % zero pading to increase the nb of points in the PSD
    rangeRPPG = (rPPG_freq>LOW_F & rPPG_freq < UP_F);   % frequency range to find PSD peak
    rPPG_power = rPPG_power(rangeRPPG);
    rPPG_freq = rPPG_freq(rangeRPPG);
    [pksrPPG,loc] = findpeaks(rPPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks (only one is used now but will be useful for postpreocessing
    rPPG_peaksLoc = rPPG_freq(loc);
      
    % PPG
    [PPG_power,PPG_freq] = pwelch(crtPPGWin,[],[],length(crtPPGWin)*3,Fs_PPG, 'psd');
    rangePPG = (PPG_freq>LOW_F & PPG_freq < UP_F); % frequency range to find PSD peak
    PPG_power = PPG_power(rangePPG);
    PPG_freq = PPG_freq(rangePPG);
    [pksPPG,loc] = findpeaks(PPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks
    PPG_peaksLoc = PPG_freq(loc);
    
%     fprintf('rPPG HR: %.2f \t',rPPG_peaksLoc(1)*60);
%     fprintf('PPG HR: %.2f \n',PPG_peaksLoc(1)*60);
    
% 
    fig5 = figure(5);
    clf(fig5);
    pos = get(gcf, 'Position');
    set(gcf, 'Position', [pos(1) pos(2) 800, 300]); %<- Set size
    subplot(1,2,1);
    set(gca, 'FontSize', 11, 'LineWidth', 1.5); %<- Set properties
    hold on;grid on;
    plot(rPPG_freq,rPPG_power, 'LineWidth',1.5);
    plot(rPPG_peaksLoc(1), pksrPPG(1), 'r*','MarkerSize',8);
    xlim([0.1 3.5]), title('rPPG Welch periodogram'), xlabel('Heart Rate (Hz)');
    ylim([0 3]);
    hold off;
    
    subplot(1,2,2);
    set(gca, 'FontSize', 11, 'LineWidth', 1.5); %<- Set properties
    hold on; grid on;
    plot(PPG_freq,PPG_power, 'LineWidth',1.5)
    plot(PPG_peaksLoc(1), pksPPG(1), 'r*' ,'MarkerSize',8);
    xlim([0.1 3.5]), title('PPG Welch periodogram'), xlabel('Heart Rate (Hz)');
    ylim([0 5]);
    %print(['example' num2str(i)], '-dpng', '-r300'); %<-Save as PNG with 300 DPI
    hold off;
    
    
    % % Here we preserve the size of the image when we save it.
    % set(gcf,'InvertHardcopy','on');
    % set(gcf,'PaperUnits', 'inches');
    % papersize = get(gcf, 'PaperSize');
    % left = (papersize(1)- width)/2;
    % bottom = (papersize(2)- height)/2;
    % myfiguresize = [left, bottom, width, height];
    % set(gcf,'PaperPosition', myfiguresize);
    % 
    % % Save the file as PNG
    % print(['example' num2str(i)],'-dpng','-r300');
    
    %%%%%%%%%%%%
    % with Multitaper method
    %%%%%%%%%%%
    %     [rPPG_power,rPPG_freq] = pmtm(crtPulseWin,[],[], Fs);
    %     [PPG_power,PPG_freq] = pmtm(crtPPGWin,[], [], Fs_PPG);
    %
    %     figure(6),
    %     subplot(1,2,1),  grid
    %     plot(rPPG_freq,rPPG_power)
    %     xlim([0.3 3.5]), title('rPPG PMTM periodogram'), xlabel('Heart Rate (Hz)')
    %     subplot(1,2,2), grid
    %     plot(PPG_freq,PPG_power)
    %     xlim([0.3 3.5]), title('PPG PMTM periodogram'), xlabel('Heart Rate (Hz)');
    
    % save final results
    crtTime =  (startTime + endTime)/2;
    time(ind) = crtTime;
    HR_PPG(ind) = PPG_peaksLoc(1)*60;
    HR_RPPG(ind) = rPPG_peaksLoc(1)*60;
    % get current PPG window
    [~, crtTimeInd] = min(abs(gtTime-crtTime));
    HR_sensor(ind) = gtHR(crtTimeInd);
    
%     fig7 = figure(7);
%     clf(fig7);
%     hold on;
%     grid on;
%     p1 = plot(time, HR_sensor, 'b-*');
%     p2 = plot(time, HR_PPG, 'g-*');
%     p3 = plot(time, HR_RPPG, 'r-*');
%     legend([p1, p2, p3], 'HR sensor', 'HR PPH', 'HR rPPG');
%     title('raw results HR estimation');
%     ylim([40 120]);
%     hold off;
    
    pause(0.0001);
    
end

fig8 = figure(8);
clf(fig8);
hold on;
grid on;
%p1 = plot(time, HR_sensor, 'b-*');
p2 = plot(time, HR_PPG, 'g-*');
p3 = plot(time, HR_RPPG, 'r-*');
%legend([p1, p2, p3], 'HR sensor', 'HR PPG', 'HR rPPG');
legend([p2, p3], 'HR sensor', 'HR PPG', 'HR rPPG');
title('raw results HR estimation');
xlabel('Time (second)'), ylabel('Heart Rate (bpm)')
ylim([40 160]);
hold off;
