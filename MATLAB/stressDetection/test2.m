% Matlab example for Spectral Analysis of Nonuniformly Sampled Signals
clear all;
close all;
% if data nonuniforlmy sampled -> interpolate or Lomb­Scargle method
% (plomb function)

%% 1 load and plot
load('nonuniformdata.mat','roomtemp','t1')
figure
plot(t1/(60*60*24*7),roomtemp,'LineWidth',1.2)
grid
xlabel('Time (weeks)')
ylabel('Temperature (\circF)')

%% is uniformly sampled ?
tAtPoints = t1(~isnan(roomtemp))/60;
TimeIntervalDiff = diff(tAtPoints);
figure
hist(TimeIntervalDiff,0:100)
grid
xlabel('Sampling intervals (minutes)')
ylabel('Occurrences')
xlim([10 100])

%% Lomb­Scargle
[Plomb,flomb] = plomb(roomtemp,t1,2e-5,'power'); % t1 en ms
figure
plot(flomb*60*60*24*7,Plomb)
grid
xlabel('Frequency (cycles/week)')
ylabel('Power (dBW)')

%% Example 2 HRV
% Load the signal, the timestamps, and the sample rate
load('nonuniformdata.mat','ecgsig','t2','Fs')
% Find the ECG peaks
[pks,locs] = findpeaks(ecgsig,Fs, 'MinPeakProminence',0.3,'MinPeakHeight',0.2);
% Determine the RR intervals
RLocsInterval = diff(locs);
% Derive the HRV signal
tHRV = locs(2:end);
HRV = 1./RLocsInterval;
% Plot the signals
figure
a1 = subplot(2,1,1);
plot(t2,ecgsig,'b',locs,pks,'*r')
grid
a2 = subplot(2,1,2);
plot(tHRV,HRV)
grid
xlabel(a2,'Time(s)')
ylabel(a1,'ECG (mV)')
ylabel(a2,'HRV (Hz)')

%% frequency analysis
figure
plomb(HRV,tHRV,'Pd',[0.95, 0.5])

figure
[Plomb,flomb] = plomb(HRV,tHRV); % t1 en ms
figure
plot(flomb,Plomb)
grid
% ylabel('Power (dBW)')

%% 3
   x = randn(1000,1);
   [y,f,t,p] = spectrogram(x,256,250,256,1000);
   % now to create an equivalence between the column vectors
   % in p
   win = hamming(256);
   xdft = fft(x(1:256).*win);
   psdx = abs(xdft(1:length(xdft)/2+1)).^2;
   psdx = 1/(1000*norm(win,2)^2)*psdx;
   psdx(2:end-1) = 2*psdx(2:end-1);
   % now compare
   subplot(211)
   plot(psdx)
   subplot(212)
   plot(p(:,1))
   
  %% 5
clear all;
close all;
rng default
Fs = 1000;
t = 0:1/Fs:1-1/Fs;
x = cos(2*pi*100*t) + randn(size(t));

N = length(x);
xdft = fft(x);
xdft = xdft(1:N/2+1);
psdx = (1/(Fs*N)) * abs(xdft).^2;
psdx(2:end-1) = 2*psdx(2:end-1);
freq = 0:Fs/length(x):Fs/2;
figure();
plot(freq,10*log10(psdx))
grid on
title('Periodogram Using FFT')
xlabel('Frequency (Hz)')
ylabel('Power/Frequency (dB/Hz)')

figure();
periodogram(x,rectwin(length(x)),length(x),Fs)
mxerr = max(psdx'-periodogram(x,rectwin(length(x)),length(x),Fs))

%% 6
clear all;
close all;
Fs = 44100;
y = audioread('guitartune.wav');

NFFT = length(y);
Y = fft(y,NFFT);
F = ((0:1/NFFT:1-1/NFFT)*Fs).';

magnitudeY = abs(Y);        % Magnitude of the FFT
phaseY = unwrap(angle(Y));  % Phase of the FFT

helperFrequencyAnalysisPlot1(F,magnitudeY,phaseY,NFFT)

y1 = ifft(Y,NFFT,'symmetric');
norm(y-y1)

%hplayer = audioplayer(y1, Fs);
%play(hplayer);

Ylp = Y;
Ylp(F>=1000 & F<=Fs-1000) = 0;

helperFrequencyAnalysisPlot1(F,abs(Ylp),unwrap(angle(Ylp)),NFFT,...
  'Frequency components above 1 kHz have been zeroed')

ylp = ifft(Ylp,'symmetric');

% hplayer = audioplayer(ylp, Fs);
% play(hplayer);

% Take the magnitude of each FFT component of the signal
Yzp = abs(Y);
helperFrequencyAnalysisPlot1(F,abs(Yzp),unwrap(angle(Yzp)),NFFT,[],...
  'Phase has been set to zero')

yzp = ifft(Yzp,'symmetric');
hplayer = audioplayer(yzp, Fs);
play(hplayer);

%% 9
clear all;
close all;
load officetemp.mat
Fs = 1/(60*30);   % Sample rate is 1 sample every 30 minutes
t = (0:length(temp)-1)/Fs;

helperFrequencyAnalysisPlot2(t/(60*60*24*7),temp,...
  'Time in weeks','Temperature (Fahrenheit)')

NFFT = length(temp);              % Number of FFT points
F = (0 : 1/NFFT : 1/2-1/NFFT)*Fs; % Frequency vector

TEMP = fft(temp,NFFT);
TEMP(1) = 0; % remove the DC component for better visualization

helperFrequencyAnalysisPlot2(F*60*60*24*7,abs(TEMP(1:NFFT/2)),...
  'Frequency (cycles/week)','Magnitude',[],[],[0 10])

%% 10
load ampoutput1.mat
Fs = 3600;
NFFT = length(y);

% Power spectrum is computed when you pass a 'power' flag input
[P,F] = periodogram(y,[],NFFT,Fs,'power');

helperFrequencyAnalysisPlot2(F,10*log10(P),'Frequency in Hz',...
  'Power spectrum (dBW)',[],[],[-0.5 200])

PdBW = 10*log10(P);
power_at_DC_dBW = PdBW(F==0)   % dBW

[peakPowers_dBW, peakFreqIdx] = findpeaks(PdBW,'minpeakheight',-11);
peakFreqs_Hz = F(peakFreqIdx)
peakPowers_dBW

%% 11
load ampoutput2.mat
SegmentLength = NFFT;

% Power spectrum is computed when you pass a 'power' flag input
[P,F] = pwelch(y,ones(SegmentLength,1),0,NFFT,Fs,'power');

helperFrequencyAnalysisPlot2(F,10*log10(P),'Frequency in Hz',...
  'Power spectrum (dBW)',[],[],[-0.5 200])

%% 11
close all
clear all;
load quakevibration.mat

Fs = 1e3;                 % sample rate
NFFT = 512;               % number of FFT points
segmentLength = 64;       % segment length

% open loop acceleration power spectrum
[P1_OL,F] = pwelch(gfloor1OL,ones(segmentLength,1),0,NFFT,Fs,'power');

% closed loop acceleration power spectrum
P1_CL     = pwelch(gfloor1CL,ones(segmentLength,1),0,NFFT,Fs,'power');

% helperFrequencyAnalysisPlot2(F,10*log10([(P1_OL) (P1_CL)]),...
%   'Frequency in Hz','Acceleration Power Spectrum in dB',...
%   'Resolution bandwidth = 15.625 Hz',{'Open loop', 'Closed loop'},[0 100])

 % simple FFT on rPPG
x = gfloor1OL;
N = length(x);
freq = [0 : N-1]*Fs/N;
spec = abs(fft(x)).^2;
plot(freq, spec), xlim([0 100]);

NFFT = 512;            % number of FFT points
segmentLength = 512;   % segment length

[P1_OL,F] = pwelch(gfloor1OL,ones(segmentLength,1),0,NFFT,Fs);
P1_CL     = pwelch(gfloor1CL,ones(segmentLength,1),0,NFFT,Fs);

helperFrequencyAnalysisPlot2(F,10*log10([(P1_OL) (P1_CL)]),...
  'Frequency in Hz','Acceleration Power Spectrum in dB',...
  'Resolution bandwidth = 1.95 Hz',{'Open loop', 'Closed loop'},[0 100])

%% 12
clear all;
close all;

Fs = 1e3;
t = 0:0.001:1-0.001;
x = cos(2*pi*100*t)+sin(2*pi*202.5*t);

xdft = fft(x);
xdft = xdft(1:length(x)/2+1);
xdft = xdft/length(x);
xdft(2:end-1) = 2*xdft(2:end-1);
freq = 0:Fs/length(x):Fs/2;

plot(freq,abs(xdft))
hold on
plot(freq,ones(length(x)/2+1,1),'LineWidth',2)
xlabel('Hz')
ylabel('Amplitude')

%% 12
xdft = fft(x,2000);
xdft = xdft(1:length(xdft)/2+1);
xdft = xdft/length(x);
xdft(2:end-1) = 2*xdft(2:end-1);
freq = 0:Fs/(2*length(x)):Fs/2;

figure
plot(freq,abs(xdft));
hold on
plot(freq,ones(2*length(x)/2+1,1),'LineWidth',2)
xlabel('Hz')
ylabel('Amplitude')


%% 13
clear all;
close all;

rng default

n = 0:319;
x = cos(pi/4*n)+randn(size(n));
pxx = pwelch(x);
plot(10*log10(pxx))

