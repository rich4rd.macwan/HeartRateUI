% Arousal Estimation project main file
% yannick.benezeth@u-bourgogne.fr
% 15/02/2016

clear all;
close all;
clc;

%%%%%%%%%%%%%%%%%
%% Initialisation
%%%%%%%%%%%%%%%%%
SHOW_PLOTS = 1;
RPPG = 0; % if 0 then work with PPG
folderName = '/media/yannick/Data/Recherche/Projects/synced/HeartRate/Code/HeartRateUI/MATLAB/stressDetection/data/stress/Richard_2016_03_02/3_disturbing/';
folderName='../main-stable/data/RichardSummer2015/12-gt/';

% select time to analyse
% use endT=-1 to analyse until the end
startT = 0;
endT = -1;

Fs = 256;
inc = 1/Fs;

%%%%%%%%%%%%%%%%%
%% load (r)PPG & and get HRV
%%%%%%%%%%%%%%%%
if(~RPPG)
    % load PPG data
    [trace, gtHR, time] = loadPPG(folderName);
    time = time / 1000;  % in seconds
else
    % load rPPG data
    [trace, time] = loadrPPG(folderName);
    trace = trace';
    time = time';
end;

% select the part to analyse
if(endT == -1)
    endT = time(end);
end
trace = trace(((time>startT) & (time<endT)));
time =  time(((time>startT) & (time<endT)));

% interpolate PPG or rPPG traces
newTime = time(1) : inc : time(end);
traceInterp = interp1(time,trace,newTime, 'pchip');

% find peaks
[pks, locs]=findpeaks(traceInterp, newTime, 'MinPeakProminence',1);

% get interbeat interval (IBI = HRV)
HRV = diff(locs);
HRVTime = locs(2:end);

% remove outliers
m = mean(HRV);
v = std(HRV);
[i,j,val] = find(abs(HRV-m)<2*v);
HRV = HRV(j);
HRVTime = HRVTime(j);

% HRV not evenly spaced -> resample
FsHRV = 30;
incHRV = 1/FsHRV;
HRVInterpTime = HRVTime(1) : incHRV : HRVTime(end);
HRVInterp = interp1(HRVTime,HRV,HRVInterpTime, 'pchip');

if (1)
    % Plot PPG data
    figure,hold on;
    title('Peak detection on (r)PPG trace')
    xlabel('Time(s)')
    p1 = plot(newTime,traceInterp);
    plot(locs,pks,'X','MarkerSize',12)
    
    % Plot HRV
    p2 = plot(HRVInterpTime,HRVInterp*10-mean(HRVInterp*10), 'r', 'LineWidth',2);
    %p3 = plot(HRVTime,HRV*10-mean(HRV*10), 'g--*', 'LineWidth',2);
    
    %legend([p1, p2 p3],'PPG', 'HRV interp', 'HRV');
    legend([p1, p2],'PPG', 'HRV interp');
    %legend('PPG');
    %xlim([0 15]);
    hold off;
end

%%%%%%%%%%%%%%%%%%%%%%%%
%% HRV temporal features
%%%%%%%%%%%%%%%%%%%%%%%%
RRmean = mean(HRV);
RRstd = std(HRV);
% square root of the mean of the sum of the squares of differences between subsequent NN intervals
x = diff(HRV);
x = x.^2;
x = sum(x)/(length(x));
RMSSD = sqrt(x);
clear x;
fprintf('RRmean %.2f \n', RRmean);
fprintf('RRstd %.2f \n', RRstd);

%%%%%%%%%%%%%%%%%%%%%%%%%
%% Freq analysis on PPG
%%%%%%%%%%%%%%%%%%%%%%%%
x = traceInterp;
nfft = Fs*20; % nb of points for FFT
shift = round(0.995*nfft); % sliding window shift for spectrogram
%shift = nfft - Fs; % sliding window shift for spectrogram
window = hanning(nfft);
[s,f,t,p] = spectrogram(x,window,shift, linspace(0.3, 3.5,nfft), Fs,'yaxis');
% show PPG spectrogram
if (1)
    figure;
    imagesc(t,f,abs(s));
    axis xy,colormap('jet'),colorbar, xlabel('Time (s)'), ylabel('Frequency (Hz)'),ylim([0.3 3.5]);
    title('(r)PPG spectrogram');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Get HR
%%%%%%%%%%%%%%%%%%%%%%%%%

% HR from spectrogram
[q,nd] = max(10*log10(p));
HR_spec = 60*f(nd);
HR_specTime = t;

% HR from HRV
HR_hrv = 60*1./HRV;
%HR_hrv = smooth(HR_hrv, 15);
%HR_hrv = detrendsignal(HR_hrv');

% show HRs
if (1)
    figure, hold on;
    title('Heart Rate');
    p1 = plot(HRVTime,HR_hrv);
    legend(p1,'HR from HRV');
    %     p2 = plot(HR_specTime,HR_spec,'r');
    %     legend([p1, p2], 'HR from spectrogram', 'HR from HRV');
    ylim([40 140]);
    hold off;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HRV frequency features with spectrogram
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x = HRVInterp;
%x = detrendsignal(x');
x = x-mean(x);
nfft = FsHRV*60; % nb of points for FFT
%shift = nfft - Fs; % sliding window shift for spectrogram
shift = round(0.995*nfft); % sliding window shift for spectrogram
window = hanning(nfft);
[s,f,t,p] = spectrogram(x,window,shift,linspace(0.01, 0.4, nfft),FsHRV,'yaxis');
%[s,f,t,p] = spectrogram(x,window,shift,0.01:0.0001:0.4,Fs,'yaxis');
LF = sum(sum(abs(s(f>0.04 & f<0.15,:))));
HF = sum(sum(abs(s(f>0.15 & f<0.4,:))));
totalNrj = sum(sum(abs(s)));
LF = LF / totalNrj;
HF = HF / totalNrj;
LFoverHF = LF/HF;
fprintf('LFoverHF %.2f \n', LFoverHF);

if (1)
    % we should observe more LF when stress
    figure, hold on;
    title('HRV spectrogram (Matlab toolbox)');
    imagesc(t,f,abs(s)),axis xy,colormap('jet'),colorbar, xlabel('Time (s)'), ylabel('Frequency (Hz)');
    ylim([0.01 0.4]);
    hold off;
end

%% HRV frequency features with periodogram
% integration de spectrogram
nrjIntegrated = sum(abs(s),2);
if (SHOW_PLOTS)
    figure, hold on;
    plot(f, nrjIntegrated);
    title('HRV periodogram (spectrogram integration)');
    xlabel('Frequency (Hz)');
    hold off;
end

% lomb periodogram
% figure;
% x = HRVInterp;
% x = x-mean(x);
% plomb(x,HRVInterpTime,'power')
% xlim([0.01, 0.4]);

% Welch
x = HRVInterp;
%x = detrendsignal(x');
x = x-mean(x);
nfft = FsHRV*60; % nb of points for FFT
%shift = nfft - Fs; % sliding window shift for spectrogram
shift = round(0.995*nfft); % sliding window shift for spectrogram
window = hanning(nfft);
%[x_spec,x_freq] = pwelch(x,window,shift,linspace(0.01, 0.4, nfft),FsHRV, 'psd');
[x_spec,x_freq] = pwelch(x,window,[],linspace(0.01, 0.4, nfft),FsHRV, 'psd');
figure;
hold on;grid on;
plot(x_freq,x_spec);
xlim([0.01 0.4]), title('HRV periodogram (Welch)'), xlabel('Hz');
hold off;

%% Poincar?? plot geometric features
x = HRV;
x(end)=[];
y=HRV;
y(1)=[];

SD1 = std(x-y)/sqrt(2);
SD2 = std(x+y)/sqrt(2);
SDRR=sqrt(SD1^2+SD2^2)/sqrt(2);

if (1)
    % we should observe round shape when stress and elongated when zen
    figure, hold on;
    title('Poincar?? plot');
    xlabel('RR_i'), ylabel('RR_{i+1}');
    plot(x,y,'r*'), xlim([0.1 1.5]), ylim([0.1 1.5]);
    hold off;
end

fprintf('SD1 %.2f \n', SD1);
fprintf('SD2 %.2f \n', SD2);
