function [z_detrended] = detrendsignal(z)
%Detrending method based on smoothness priors approach
%z has to be a column vector
%based on: Tarvainen et al., “An advanced detrending method with application to hrv analysis,” IEEE Trans. on Biom. Eng., 2002.

T = length(z); 
%Regularizer
lambda = 10; 
I = speye(T);
%The second order difference matrix
D2 = spdiags(ones(T-2,1)*[1 -2 1],(0:2),T-2,T); 
%The detrended signal
z_detrended = (I-inv(I+lambda^2.*(D2'*D2)))*z;

end

