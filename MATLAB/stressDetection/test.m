% Matlab examples for time/frequency analysis

clear all;
close all;
% Exemple 1
%% Generate a DTMF signal and listen to it.
[tones, Fs] = helperDTMFToneGenerator();
audid = audiodevinfo(0,Fs,16,1);
if audid ~= -1
    p = audioplayer(tones,Fs,16,audid);
    play(p)
end

%%  visualize signal
N = numel(tones);
t = (0:N-1)/Fs;
subplot(2,1,1)
plot(1e3*t,tones)
xlabel('Time (ms)')
ylabel('Amplitude')
title('DTMF Signal')
subplot(2,1,2)
periodogram(tones,[],[],Fs)
xlim([0.65 1.5])

%% Locate the frequency peaks
f = [meanfreq(tones,Fs,[700 800]), ...
meanfreq(tones,Fs,[800 900]), ...
meanfreq(tones,Fs,[900 1000]), ...
meanfreq(tones,Fs,[1300 1400])];
round(f)

%% spectrogram
figure,
spectrogram(tones,[],[],[],Fs,'yaxis')
ylim([0.7 1.4])

%% Exemple 2 (time vs spectral resolution)
load splat
audid = audiodevinfo(0,Fs,16,1);
if audid ~= -1
    p = audioplayer(y,Fs,16,audid);
    play(p)
end

%% 1
segmentLength = round(numel(y)/4.5); % Equivalent to setting segmentLength = [] in the next line
spectrogram(y,segmentLength,[],[],Fs,'yaxis')

%% 2
spectrogram(y,round(segmentLength/5),[],[],Fs,'yaxis')

%% 3
spectrogram(y,round(segmentLength/5), ...
round(80/100*segmentLength/5),[],Fs,'yaxis')

%% 4
spectrogram(y,round(segmentLength/5), round(80/100*segmentLength/5),[],Fs,'yaxis');
%helperTimeFrequencyAnalysisPlotReassignment(F,T,P,Fc,Tc,'yaxis');

%% Exemple 3 : 3D
Fs = 10e3;
t = 0:1/Fs:2;
x1 = vco(sawtooth(2*pi*t,0.5),[0.1 0.4]*Fs,Fs);
spectrogram(x1,kaiser(256,5),220,512,Fs,'yaxis')
view(-45,65)
colormap bone

%% Exemple 4
clear all;
Fs = 1000;
t = 0:1/Fs:2-1/Fs;
y = chirp(t,100,1,200,'quadratic');
spectrogram(y,100,80,100,Fs,'yaxis')
view(-77,72)
shading interp
colorbar off

[s,f,t,p] = spectrogram(y,100,80,100,Fs);

[q,nd] = max(10*log10(p));

hold on
plot3(t,f(nd),q,'r','linewidth',4)
hold off
colorbar
view(2)
