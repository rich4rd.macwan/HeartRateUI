% Analysis of Besancon data (19/01/2016)
clear all;
close all;
clc;

%% Initialisation

SHOW_PLOTS = 1;

mainFolder = './data/besac/';
expNumber = 2;
vidNumber = 2;
vidFolder = [mainFolder 'exp' num2str(expNumber) '/vid' num2str(vidNumber)];

Fs = 75;
winLength = Fs * 45; % analyzed window length (nb of points)

%%%%%%%%%%%%%%%%%%%%%%%
%% PPG & HRV
%%%%%%%%%%%%%%%%%%%%%%%
% load PPG data
[gtTrace, gtHR, gtTime] = loadPPG(vidFolder);
gtTime = gtTime / 1000; % in second

% find peaks
[pks, locs]=findpeaks(gtTrace, gtTime, 'MinPeakProminence',1);

% get interbeat interval
HRV = diff(locs);
HRVTime = locs(2:end);

% not evenly spaced -> resample
inc = 1/Fs;
endTime = gtTime(end);
newTime = 0 : inc : endTime;
gtTraceInterp = interp1(gtTime,gtTrace,newTime, 'pchip');
HRVInterp = interp1(HRVTime,HRV,newTime, 'pchip');

% HRVInterp = HRVInterp -mean(HRVInterp); % for spctrogram

if (SHOW_PLOTS)
    % Plot PPG data
    figure(1), hold on;
    title('PPG & HRV trace')
    xlabel('Time(s)')
    p1 = plot(newTime,gtTraceInterp);
    plot(locs,pks,'X','MarkerSize',12)
    
    % Plot HRV
    p2 = plot(newTime,10*HRVInterp-mean(10*HRVInterp), 'r', 'LineWidth',2);
end

if (expNumber == 1)
    motsOK = [20, 50, 60, 70, 90];
    motsKO = [30, 40, 80, 100, 110];
    stimuli = [120, 135, 150, 165];
    ylimit=get(gca,'ylim');
    % Plotting vertical lines from x-axis to points
    for i=1:5
        p3 = plot([motsOK(i), motsOK(i)],[ylimit(1),ylimit(2)],'g--');
        p4 = plot([motsKO(i), motsKO(i)],[ylimit(1),ylimit(2)],'r--');
    end;
    for i=1:4
        p5 = plot([stimuli(i), stimuli(i)],[ylimit(1),ylimit(2)],'k--');
    end;
    legend([p1, p2, p3, p4, p5], 'PPG', 'IBI', 'mots neutres', 'mots forts', 'autres stimuli');
elseif (expNumber == 2)
    stimuli = [20, 40, 60];
    ylimit=get(gca,'ylim');
    % Plotting vertical lines from x-axis to points
    for i=1:3
        p3 = plot([stimuli(i), stimuli(i)],[ylimit(1),ylimit(2)],'r--');
    end;
    legend([p1, p2, p3],'PPG', 'IBI', 'stimuli');
end;
hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Spectrogram on PPG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x = gtTraceInterp;
nfft = 30*Fs;%2^nextpow2(Fs*40); 
window= hanning(nfft);
[s,f,t,p] = spectrogram(x,window,length(window)-Fs, length(window)*3,Fs,'yaxis'); 
% fig3 = figure(3);
% imagesc(t,f,abs(s)),axis xy,colormap('jet'),colorbar, 
% title('spectrogram on PPG');
% xlabel('Time (s)'), ylabel('Frequency (Hz)');ylim([0.3 3.5]); 

% HR from spectrogram
[q,nd] = max(10*log10(p));
HRspec = f(nd);

%% Freq analysis on HRV
% x = HRVInterp';
% fig4 = figure(4);
% [s,f,t,p] = spectrogram(x,window,length(window)-Fs,linspace(0.04, 0.4, length(window)),Fs,'yaxis');%, ylim([0.03 0.4]);
% imagesc(t,f,abs(s)),axis xy,colormap('jet'),colorbar, 
% xlabel('Time (s)'), ylabel('Frequency (Hz)');ylim([0.04 0.4]); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Heart Rate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get heart rate from IBI
HR_hrv = 60*(1./HRVInterp);
HR_hrv = smooth(HR_hrv, 500);

% get HR from PPG FFT analysis (sliding window)
WINDOW_LENGTH_SEC = 10; % length of the sliding window (in seconds)
STEP_SEC = 0.4; % step between 2 sliding window position (in seconds)
LOW_F=.2; % low freq for PSD range
UP_F=3; % high freq for PSD range

winLength = round(WINDOW_LENGTH_SEC*Fs); % length of the sliding window for FFT
step = round(STEP_SEC*Fs);


ind = 0;
for i=1:step:length(gtTrace)-winLength
    ind = ind+1;
   
    % get start/end index of current window
    startInd = i;
    endInd = i+winLength-1;
    startTime = gtTime(startInd);
    endTime = gtTime(endInd);
    
    % get current pulse window
    crtPPGWin = gtTrace(startInd:endInd);
    crtTimePPGWin = gtTime(startInd:endInd);
    
    % get exact PPG Fs
    Fs_PPG = 1/mean(diff(crtTimePPGWin));
    
%     figure(4),plot(crtTimePPGWin, crtPPGWin, 'b');
%     legend('PPG'),
%     title('PPG window');
    
    %% Spectral analysis
    [PPG_power,PPG_freq] = pwelch(crtPPGWin,[],[],length(crtPPGWin)*3,Fs_PPG, 'psd');
    rangePPG = (PPG_freq>LOW_F & PPG_freq < UP_F); % frequency range to find PSD peak
    PPG_power = PPG_power(rangePPG);
    PPG_freq = PPG_freq(rangePPG);
    [pksPPG,loc] = findpeaks(PPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks
    PPG_peaksLoc = PPG_freq(loc);
       
%     fig5 = figure(5);
%     clf(fig5);
%     hold on;grid on;
%     plot(PPG_freq,PPG_power)
%     plot(PPG_peaksLoc(1), pksPPG(1), 'r*');
%     xlim([0.1 3.5]), title('PPG Welch periodogram'), xlabel('Heart Rate (Hz)');
%     hold off;
        
    % save final results
    crtTime =  (startTime + endTime)/2;
    time(ind) = crtTime;
    HR_PPG(ind) = PPG_peaksLoc(1)*60;
   
    % get current PPG window
    [~, crtTimeInd] = min(abs(gtTime-crtTime));
    HR_sensor(ind) = gtHR(crtTimeInd);
    
%     fig6 = figure(6);
%     clf(fig6);
%     hold on;
%     grid on;
%     p1 = plot(time, HR_sensor, 'b-*');    p2 = plot(time, HR_PPG, 'g-*');
%     legend([p1, p2], 'HR sensor', 'HR PPG');
%     title('raw results HR estimation');
%     ylim([40 120]);
%     hold off;
%     
%     pause(0.0001);
    
end

HR_PPG = smooth(HR_PPG);
HR_sensor = smooth(HR_sensor);

fig8 = figure(8);
clf(fig8);
hold on;
grid on;
ylim([60 110]);
p1 = plot(time, HR_sensor, 'b-');
p2 = plot(time, HR_PPG, 'g-');

% Plot stimuli
if (expNumber == 1)
    motsOK = [20, 50, 60, 70, 90];
    motsKO = [30, 40, 80, 100, 110];
    stimuli = [120, 135, 150, 165];
    ylimit=get(gca,'ylim');
    % Plotting vertical lines from x-axis to points
    for i=1:5
        p3 = plot([motsOK(i), motsOK(i)],[ylimit(1),ylimit(2)],'g--');
        p4 = plot([motsKO(i), motsKO(i)],[ylimit(1),ylimit(2)],'r--');
    end;
    for i=1:4
        p5 = plot([stimuli(i), stimuli(i)],[ylimit(1),ylimit(2)],'k--');
    end;
    legend([p1, p2, p3, p4, p5], 'HR sensor', 'HR PPG Welch', 'mots neutres', 'mots forts', 'autres stimuli');
elseif (expNumber == 2)
    stimuli = [20, 40, 60];
    ylimit=get(gca,'ylim');
    % Plotting vertical lines from x-axis to points
    for i=1:3
        p3 = plot([stimuli(i), stimuli(i)],[ylimit(1),ylimit(2)],'r--');
    end;
    legend([p1, p2, p3] ,'HR sensor', 'HR PPG Welch', 'stimuli');
end;
hold off;