function getTraceFromFileSeq(varargin)
% extract rPPG trace from image sequences
% face detection, tracking, skin detection, and spatial average
% argument can be a video folder
% yannick.benezeth@u-bourgogne.fr
% 17/02/2016

close all;
clear all;
clc;

addpath('./tools/skindetector');

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
DO_FACECROP = 0; % 0 or 1
DO_SKINDETECTION = 1; % 0 or 1
SKIN_THRESH = 0; % threshold for skin detection (default: 0 - the lower the more skin we detect)

%vidFolder = '/home/yannick/vidStress/Yannick_2016_02_17_1/stress/';
vidFolder = '/media/yannick/Data/Dataset/HeartRate/vidStress/Yannick_2016_02_17_1/stress/';
if(nargin==1)
    vidFolder=varargin(1);
end
disp(['Get RGB trace from image sequence here: ' vidFolder]);
outFileName = 'rgbTraces_skin';

% Create a cascade detector object.
faceDetector = vision.CascadeObjectDetector('ClassificationModel','FrontalFaceCART','MinSize',[200 200]);

% Create the point tracker object.
pointTracker = vision.PointTracker('MaxBidirectionalError', 5);

% nb of frames
nbFiles=length(dir([vidFolder 'img/*.ppm']));

nChannels=3;
rgbTraces = zeros(1+nChannels, nbFiles-1); % why minus 1 ?

figure, hold on;

numPts = 0;
manualbbox=[];
for n=1:nbFiles
    % read frame by frame number (maybe not ordered)
    imgName = dir([vidFolder '/img/frame_' num2str(n-1) '_*.ppm']);
    if(size(imgName,1)==0)
        fprintf('oops, no image file \n');
        continue;
    end
    imgName = imgName.name;
    img = imread([vidFolder '/img/' imgName]);
    img_copy = img;
    
    % face localisation by detection then tracking
    if numPts < 10
        % Detection mode.
        bbox  = step(faceDetector, img);
        
        if numel(bbox)==0
            if ~isempty(manualbbox)
                bbox=manualbbox;
            end
        end
        
        if numel(bbox)~=0
            % Find corner points inside the detected region.
            points = detectMinEigenFeatures(rgb2gray(img), 'ROI', bbox(1, :));
            
            % Re-initialize the point tracker.
            xyPoints = points.Location;
            numPts = size(xyPoints,1);
            release(pointTracker);
            initialize(pointTracker, xyPoints, img);
            
            % Save a copy of the points.
            oldPoints = xyPoints;
            
            % Convert the rectangle represented as [x, y, w, h] into an
            % M-by-2 matrix of [x,y] coordinates of the four corners. This
            % is needed to be able to transform the bounding box to display
            % the orientation of the face.
            x = bbox(1, 1);
            y = bbox(1, 2);
            w = bbox(1, 3);
            h = bbox(1, 4);
            bboxPoints = [x,y;x,y+h;x+w,y+h;x+w,y];
            
            % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
            % format required by insertShape.
            bboxPolygon = reshape(bboxPoints', 1, []);
            
            % Display a bounding box around the detected face.
            img = insertShape(img, 'Polygon', bboxPolygon);
            
            % Display detected corners.
            img = insertMarker(img, xyPoints, '+', 'Color', 'white');
        else
            %Face wasn't detected, manual roi mode
            imshow(img);
            title('Face detector did not detect a face. Please select one manually');
            
            rect= ginput(2);
            rect=round(rect);
            rect(2,:)=rect(2,:)-rect(1,:);
            manualbbox=reshape(rect',[1 4]);    
        end        
    else
        % Tracking mode.
        [xyPoints, isFound] = step(pointTracker, img);
        visiblePoints = xyPoints(isFound, :);
        oldInliers = oldPoints(isFound, :);
        
        numPts = size(visiblePoints, 1);
        
        if numPts >= 10
            % Estimate the geometric transformation between the old points
            % and the new points.
            [xform, oldInliers, visiblePoints] = estimateGeometricTransform(...
                oldInliers, visiblePoints, 'similarity', 'MaxDistance', 4);
            
            % Apply the transformation to the bounding box.
            bboxPoints = transformPointsForward(xform, bboxPoints);
            
            % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
            % format required by insertShape.
            bboxPolygon = reshape(bboxPoints', 1, []);
            
            % Display a bounding box around the face being tracked.
            img = insertShape(img, 'Polygon', bboxPolygon);
            
            % Display tracked points.
            img = insertMarker(img, visiblePoints, '+', 'Color', 'white');
            
            % Reset the points.
            oldPoints = visiblePoints;
            setPoints(pointTracker, oldPoints);
        end
    end
    
    % once we have the face location -> we extract the RGB value
    %pause(0.00000001)
    if ~isempty(bbox)
        IFaces = insertObjectAnnotation(img, 'rectangle', bbox, 'Face');
        imshow(IFaces)
        if(DO_FACECROP)
            % crop the face
            % horizontal cropping
            scaleW = 0.6;
            bbox(1,3) = bbox(1,3)*scaleW;
            bbox(1,1) = bbox(1,1) + bbox(1,3)*((1-scaleW)/2);
            % vertical cropping
            scaleV = 0.6;
            bbox(1,4) = bbox(1,4)*scaleV;
            bbox(1,2) = bbox(1,2) + bbox(1,4)*((1-scaleV)/2);
        end;
        
        imgROI = imcrop(img_copy,bbox(1,:));
        %figure(2), imshow(imgROI), title('Detected Face');
        
        if (DO_SKINDETECTION)   
            % average RGB value of skin pixels
            imgD = double(imgROI);
            skinprob = computeSkinProbability(imgD);
            mask = skinprob>SKIN_THRESH;
            maskRGB = zeros(size(skinprob,1),size(skinprob,2),3);
            maskRGB(:,:,1) = mask;
            maskRGB(:,:,2) = mask;
            maskRGB(:,:,3) = mask;
            imgD = imgD.*maskRGB;
            imgROI(imgD==0)=nan;
            
            figure(3), imshow(imgROI), title('Detected Face + skin');
            
        end;
        
        % Better way to get mean of non-zero values ? this is quite slow...
        % TODO: double check this...
        [~, ~, vals] = find(imgD(:,:,1));
        avg(1) = mean(vals);
        [~, ~, vals] = find(imgD(:,:,2));
        avg(2) = mean(vals);
        [~, ~, vals] = find(imgD(:,:,3));
        avg(3) = mean(vals);
        
        rgbTraces(1:nChannels,n) = avg;
       
        %get timestamp from name
        [startIndex,endIndex] = regexp(imgName,'\_\d*\.');
        time = imgName(startIndex+1:endIndex-1);
               
        rgbTraces(1+nChannels,n) = str2double(time);
        
        if (mod(n,10) == 0)
            fprintf('%i sur %i %s, timestamp %s \n', n, nbFiles, imgName, time);
        end
    end
end
save([vidFolder '/' outFileName '.mat'], 'rgbTraces');

clear all
close all
end
