function getPulseSignalFromTrace(varargin)
% preprocess RGB trace to get pulse trace
% Green method: sliding window, detrend, band-pass on green channel
% Chrom method: "Robust Pulse Rate From Chrominance-Based rPPG" Haan et al. 2013
% yannick.benezeth@u-bourgogne.fr
% 17/02/2016

clear all;
close all;
clc;

disp('preprocess RGB trace to get pulse trace');

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
WINDOW_LENGTH_SEC = 30; % length of the sliding window (in seconds)
STEP_SEC = 0.5; % step between 2 sliding window position (in seconds)
LOW_F=.3; % low freq for BP filtering
UP_F=3; % high freq for BP filtering
FILTER_ORDER = 4; % Order of the butterworth BP filter

vidFolder = '/media/yannick/Data/Dataset/HeartRate/RichardSummerVideos/10-gt/';
%vidFolder = '/home/yannick/vidYannick/1/';

matFileName = 'rgbTraces_skin'; % input file
if(nargin==1)
    vidFolder=varargin(1);
end
disp(['Working with  ' vidFolder matFileName '.mat']);

if(exist([vidFolder matFileName '.mat'], 'file') == 2)
    load([vidFolder matFileName '.mat']);
else
    disp('oops, no input file');
    return;
end

% load PPG
[gtTrace, gtHR, gtTime] = loadPPG(vidFolder);
gtTime = gtTime / 1000;  % in seconds

% get current traces
timeTrace = rgbTraces(end,:);
timeTrace = timeTrace/1000; % in second
crtTrace = rgbTraces(1:3,:);
traceSize = size(crtTrace,2);

% Uniform sampling
timeTraceUnif = linspace(timeTrace(1), timeTrace(end), traceSize);
for k=1:3
    crtTrace(k,:) = interp1(timeTrace,crtTrace(k,:), timeTraceUnif);
end
timeTrace = timeTraceUnif;

% detrend the whole signal (in case we can work on the whole signal)
% for k=1:3
%     crtTrace(k,:) = detrendsignal(crtTrace(k,:)')';
% end

% get exact Fs
Fs = 1/mean(diff(timeTrace));

% get window length in frames
winLength = round(WINDOW_LENGTH_SEC*Fs);
step = round(STEP_SEC*Fs);

% get coef of butterworth filter
[b, a]= butter(FILTER_ORDER,[LOW_F UP_F]/(Fs/2));

pulseTraceGreen=zeros(1,traceSize);
pulseTraceChrom=zeros(1,traceSize);
for i=1:step:traceSize-winLength
    
    %     if(mod(i,10)==0)
    %         disp(['processing window ' num2str(i) ' on ' num2str(traceSize-winLength)]);
    %     end
    %
    % get start/end index of current window
    startInd = i;
    endInd = i+winLength-1;
    
    % get current trace window
    crtTraceWin = crtTrace(1:3,startInd:endInd);
    crtTimeWin = timeTrace(startInd:endInd);
    
    % figure(1), plot(crtTimeWin, crtTraceWin(2,:)),title('raw green trace'), xlabel('seconds'), ylabel('pixel values');
    
    % normalize each signal to have zero mean
    crtTraceWin = crtTraceWin - repmat(mean(crtTraceWin,2),[1 winLength]);
    % and also unit variance
    crtTraceWin = crtTraceWin ./ repmat(std(crtTraceWin,0,2),[1 winLength]);
    
    %     fig2 = figure(2);
    %     clf(fig2);
    %     hold on;
    %     p1 = plot(crtTimeWin, crtTraceWin(1,:),'r');
    %     p2 = plot(crtTimeWin, crtTraceWin(2,:),'g');
    %     p3 = plot(crtTimeWin, crtTraceWin(3,:),'b');
    %     title('normalized traces'), xlabel('seconds'), ylabel('normalized pixel values');
    %     hold off;
    
    
    % detrend
    for k=1:size(crtTraceWin,1)
        crtTraceWin(k,:) = detrendsignal(crtTraceWin(k,:)')';
    end
    % figure(3), plot(crtTimeWin, crtTraceWin(2,:)), title('detrended green trace');
    
    % band pass filter
    crtTraceWinFilt = zeros(size(crtTraceWin));
    for k=1:size(crtTraceWin,1)
        crtTraceWinFilt(k,:) = filter(b,a,crtTraceWin(k,:)')';
    end
    % figure(4), plot(crtTimeWin, crtTraceWinFilt(2,:)), title('filtered green trace');
    
    % Method 1: Green channel
    crtPulseGreen = crtTraceWinFilt(2,:);
    % figure(5), plot(crtTimeWin, crtPulseGreen), title('crtPulseGreen');
    
    % Method 2: CHROM
%     Rn = crtTraceWin(1,:);
%     Gn = crtTraceWin(2,:);
%     Bn = crtTraceWin(3,:);
%     Xs = 3*Rn-2*Gn;
%     Ys = 1.5*Rn+Gn-1.5*Bn;
%     Xf = filter(b,a,Xs')';
%     Yf = filter(b,a,Ys')';
%     alpha = std(Xf)/std(Yf);
%     crtPulseChrom = Xf - alpha*Yf;

    Rf = crtTraceWinFilt(1,:);
    Gf = crtTraceWinFilt(2,:);
    Bf = crtTraceWinFilt(3,:);
    Xf = 3*Rf-2*Gf;
    Yf = 1.5*Rf+Gf-1.5*Bf;
    %Xf = filter(b,a,Xs')';
    %Yf = filter(b,a,Ys')';
    alpha = std(Xf)/std(Yf);
    crtPulseChrom = Xf - alpha*Yf;
    %figure(6), plot(crtTimeWin, crtPulseChrom), title('crtPulseChrom');
    
    % compare PPG with pulse traces
    %     fig7 = figure(7);
    %     clf(fig7), hold on, title('PPG vs rPPG'), xlim([timeTrace(startInd), timeTrace(endInd)]);
    %     p1 = plot(gtTime, gtTrace, 'b');
    %     p2 = plot(crtTimeWin, crtPulseGreen, 'g');
    %     p3 = plot(crtTimeWin, crtPulseChrom, 'r');
    %     legend([p1,p2,p3],'PPG', 'Green', 'Chrom');
    %     hold off;
    
    % overlap-add to build the final pulse trace
    pulseTraceGreen(startInd:endInd)=crtPulseGreen;
    pulseTraceChrom(startInd:endInd)=crtPulseChrom;
    
    % pause();
end

% scale amplitudes
pulseTraceGreen = pulseTraceGreen/std(pulseTraceGreen);
pulseTraceChrom = pulseTraceChrom/std(pulseTraceChrom);

% plot final traces
figure(8),hold on;
p1 = plot(gtTime, gtTrace, 'b');
p2 = plot(timeTrace, pulseTraceGreen, 'g', 'Linewidth', 2);
p3 = plot(timeTrace, pulseTraceChrom, 'r');
xlim([1 15]),
legend([p1, p2, p3], 'PPG', 'Green', 'Chrom');

% save to mat files
pulseTrace = pulseTraceGreen;
save([vidFolder 'pulseTraceGreen.mat'], 'pulseTrace', 'timeTrace');
pulseTrace = pulseTraceChrom;
save([vidFolder 'pulseTraceChrom.mat'], 'pulseTrace', 'timeTrace');