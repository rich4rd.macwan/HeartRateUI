function [trace, time] = loadrPPG(vidFolder)
% load PPG recorded with pulse oximeter

trace = [];
time = [];

filename=[vidFolder '/' 'pulseTraceChrom.mat'];

if exist(filename)==2
    data=load(filename);
    trace=data.pulseTrace;
    time=data.timeTrace;
    
    % normalize data (zero mean and unit variance)
    trace = trace - mean(trace);
    trace = trace / std(trace);    
else
    fprintf('oops, no PPG file...\n');
end

