%clear all;
close all;
clc;

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
filterSize = 10;
resizeFactor = 0.25;
h = ones(filterSize,filterSize) / (filterSize^2); % avegarge filter
LOW_F=.7; % low freq for BP filtering
UP_F=4; % high freq for BP filtering
FILTER_ORDER = 4; % Order of the butterworth BP filter

vidFolder ='/home/yannick/ownCloud/PPGImaging/vid1/';
outFolder='/home/yannick/ownCloud/PPGImaging/vid1/';

% load pulse traces
filename = [vidFolder 'rgbMultipleTraces_' num2str(filterSize) 'x' num2str(filterSize)];
%load(filename);
%timeTrace = timeTrace/1000; % in second
Fs = 1/mean(diff(timeTrace)); % get exact Fs
[nR, nC, nChannel, traceSize] = size(rgbMultipleTraces);

% Create filter
d = designfilt('bandpassfir','FilterOrder', FILTER_ORDER, 'CutoffFrequency1',LOW_F,'CutoffFrequency2',UP_F, 'SampleRate',Fs);
delay = mean(grpdelay(d)); % filter delay in samples

% load PPG
[gtTrace, gtHR, gtTime] = loadPPG(vidFolder);
gtTime = gtTime / 1000;  % in seconds
Fs_PPG = 1/mean(diff(gtTime)); % get exact Fs

load([vidFolder 'mask.mat']);
mask = imresize(mask, resizeFactor);


%% Display one trace
imgName = dir([vidFolder '/img/frame_' num2str(1) '_*.bmp']);
imgName = imgName.name;
img = imread([vidFolder '/img/' imgName]);
img = imfilter(img,h);
img = imresize(img, resizeFactor);

while(1);
    fig1 = figure(1);
%     clf(fig1);
%     pos = get(fig1,'position');
%     set(fig1,'position',[213 703 pos(3) pos(4)])
    imshow(img);
    [c,r]=ginput(1);
    r=round(r); c=round(c);
    fprintf('plot traces for pixel r=%i and c=%i \n', r, c);
    
    % get current traces
    crtTrace = rgbMultipleTraces(r,c,:,:);
    crtTrace = squeeze(crtTrace);
    
    % Uniform sampling
    timeTraceUnif = linspace(timeTrace(1), timeTrace(end), traceSize);
    for k=1:nChannel
        crtTrace(k,:) = interp1(timeTrace,crtTrace(k,:), timeTraceUnif, 'spline');
    end
    gtTraceUnif =  interp1(gtTime,gtTrace, timeTraceUnif, 'spline');
    timeTrace = timeTraceUnif;
    
   
    
    
    % normalize each signal to have zero mean and also unit variance
%                     crtTrace = crtTrace - repmat(mean(crtTrace,2),[1 traceSize]);
%                     crtTrace = crtTrace ./ repmat(std(crtTrace,0,2),[1 traceSize]);
    
    crtTrace = crtTrace ./ repmat(mean(crtTrace,2),[1 traceSize]) - 1;

    fig2 = figure(2);
    clf(fig2);
    set(fig2,'position',[66 1 600 282])
    plot(timeTrace, crtTrace(2,:)),title('raw normalized green trace'), xlabel('seconds'), ylabel('pixel values');
    xlim([10 30]);

    % smooth
    for k=1:3
        crtTrace(k,:)=smooth(crtTrace(k,:),5);
    end
%     fig3 = figure(3);
%     clf(fig3);
%     title('smoothed traces'), xlabel('seconds'), ylabel('pixel values'), xlim([1 20]);
%     hold on;
%     plot(gtTime, gtTrace/max(gtTrace), 'k');
%     plot(timeTrace, crtTrace(2,:)/max(crtTrace(2,:)), 'b');
%     hold off;
    
    % detrend
    for k=1:3
        crtTrace(k,:) = detrendsignal(crtTrace(k,:)')';
    end
%     fig4 = figure(4);
%     clf(fig4);
%     title('detrended traces'), xlabel('seconds'), ylabel('pixel values'), xlim([1 20]);
%     hold on;
%     plot(gtTime, gtTrace/max(gtTrace), 'k');
%     plot(timeTrace, crtTrace(2,:)/max(crtTrace(2,:)),'b');
%     hold off;
   
    % filter
    for k=1:3
        tmp = filter(d,[crtTrace(k,:)'; zeros(delay,1)]); % Append D zeros to the input data
        crtTrace(k,:) = tmp(delay+1:end)';                  % Shift data to compensate for delay
    end
    fig5 = figure(5);
    set(fig5,'position',[681 37 1221 375])
    clf(fig5);
    title('Pulse Green trace'), xlabel('seconds'), ylabel('pixel values'), xlim([10 30]);
    hold on;
    plot(timeTrace, gtTrace/max(gtTrace), 'k');
    plot(timeTrace, crtTrace(2,:)/max(crtTrace(2,:)), 'b');
    hold off;
        
     % Method 1: Green channel
    pulseTraceGreen = crtTrace(2,:);
        
    % Method 2: CHROM
    Rf = crtTrace(1,:);
    Gf = crtTrace(2,:);
    Bf = crtTrace(3,:);
    Xf = 3*Rf-2*Gf;
    Yf = 1.5*Rf+Gf-1.5*Bf;
    alpha = std(Xf)/std(Yf);
    pulseTraceChrom = Xf - alpha*Yf;
    
    fig6 = figure(6);
    clf(fig6);
    set(fig6,'position',[684 495 1195 469])
    title('Chrom trace'), xlabel('seconds'), ylabel('pixel values'), xlim([10 30]);
    hold on;
    plot(timeTrace, gtTrace/max(gtTrace), 'k');
    plot(timeTrace, -pulseTraceChrom/max(pulseTraceChrom), 'b');
    hold off;
        
    % Spectral analysis with FFT
    % on rPPG
    x = pulseTraceGreen;
    N = length(x)*3;
    rPPG_freq = [0 : N-1]*Fs/N;
    rPPG_power = abs(fft(x,N)).^2;
    
     % Get peaks rPPG
    rangeRPPG = (rPPG_freq>LOW_F & rPPG_freq < UP_F);   % frequency range to find PSD peak
    rPPG_power = rPPG_power(rangeRPPG);
    rPPG_freq = rPPG_freq(rangeRPPG);
    [pksrPPG,loc] = findpeaks(rPPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks (only one is used now but will be useful for postpreocessing
    rPPG_peaksLoc = rPPG_freq(loc);
    
    % on PPG
    x = gtTrace;
    N = length(x)*3;
    PPG_freq = [0 : N-1]*Fs_PPG/N;
    PPG_power = abs(fft(x,N)).^2;
    
    rangePPG = (PPG_freq>LOW_F & PPG_freq < UP_F); % frequency range to find PSD peak
    PPG_power = PPG_power(rangePPG);
    PPG_freq = PPG_freq(rangePPG);
    [pksPPG,loc] = findpeaks(PPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks
    PPG_peaksLoc = PPG_freq(loc);
    
    fig7 = figure(7);
    clf(fig7);
    set(fig7,'position',[66 370 595 306])
    subplot(1,2,1), hold on; grid on;
    plot(rPPG_freq, rPPG_power), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
    plot(rPPG_peaksLoc(1), pksrPPG(1), 'r*');
    text(rPPG_peaksLoc(1)+0.1, pksrPPG(1),[num2str(rPPG_peaksLoc(1)*60,3) ' bpm']);
    title('rPPG FFT')
    hold off;
    subplot(1,2,2); hold on; grid on;
    plot(PPG_freq, PPG_power), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
    plot(PPG_peaksLoc(1), pksPPG(1), 'r*');
    text(PPG_peaksLoc(1)+0.1, pksPPG(1),[num2str(PPG_peaksLoc(1)*60,3) ' bpm']);
    title('PPG FFT');
    hold off;
end
    
    
    
    
