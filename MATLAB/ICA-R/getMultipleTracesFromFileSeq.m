function getMultipleTracesFromFileSeq(varargin)
% extract multiple RGB traces from image sequences
% first version: 23/11/2016
tic
close all;
clear all;
clc;

vidFolder='/home/yannick/ownCloud/PPGImaging/vid1/';%  /media/yannick/Data/Dataset/HeartRate/phaseAnalysis/
outFolder='/home/yannick/ownCloud/PPGImaging/vid1/';

filterSize = 20;
resizeFactor = 0.25;
h = ones(filterSize,filterSize) / (filterSize^2); % avegarge filter

outFileName=['rgbMultipleTraces_' num2str(filterSize) 'x' num2str(filterSize)];

disp(['Get Multiple RGB traces from image sequence here: ' vidFolder]);
disp(['outFolder: ' outFolder]);

nbFiles=length(dir([vidFolder 'img/*.bmp']));

imgName = dir([vidFolder '/img/frame_' num2str(1) '_*.bmp']);
imgName = imgName.name;
img = imread([vidFolder '/img/' imgName]);

%mask = roipoly(img);
load([vidFolder 'mask.mat']); 
mask = imresize(mask, resizeFactor);

[nR, nC, nChannels]=size(img);
rgbMultipleTraces = zeros(nR*resizeFactor, nC*resizeFactor, nChannels, nbFiles);
timeTrace = zeros(1,nbFiles);

for n=1:nbFiles
    % read frame by frame number (maybe not ordered)
    imgName = dir([vidFolder '/img/frame_' num2str(n-1) '_*.bmp']);
    
    if(size(imgName,1)==0)
        fprintf('oops, no image file \n');
        continue;
    end
    imgName = imgName.name;
    img = double(imread([vidFolder '/img/' imgName]));
       
    img = imfilter(img,h);
    img = imresize(img, resizeFactor);
    %imshow(uint8(img));
    
    img_copy(:,:,1) = img(:,:,1).*mask;    
    img_copy(:,:,2) = img(:,:,2).*mask;    
    img_copy(:,:,3) = img(:,:,3).*mask;    
    
    %imshow(uint8(img_copy));
    
    rgbMultipleTraces(:,:,:,n) = img_copy;
    %get timestamp from name
    [startIndex,endIndex] = regexp(imgName,'\_\d*\.');
    time = imgName(startIndex+1:endIndex-1);
    
    timeTrace(n) = str2double(time);
    if (mod(n,50) == 0)
        fprintf('%i sur %i %s, timestamp %s \n', n, nbFiles, imgName, time);
    end
end
  fprintf('saving %s ...\s', [outFolder '/' outFileName '.mat']);
save([outFolder '/' outFileName '.mat'], 'rgbMultipleTraces', 'timeTrace', '-v7.3');

clear all
close all
disp(toc);
end

