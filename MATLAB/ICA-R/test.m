%% 1
w = 2;
t = 0:0.01:10;
Y = pi/2;
s1 = sin(w*t)+cos(w*t);
s2 = sin(w*t+Y)+cos(w*t+Y);

figure(1), clf, hold on;
plot(t, s1);
plot(t, s2);
hold off;
mean(s1.*s2)
%acos(dot(s1,s2)/(norm(s1)*norm(s2))) 

%% 2
Fcut1 = 0.7;
Fcut2 = 7;
%FiltOrder = 4;
Fs = 30;
Ap = 1;
Ast = 30;

%d = designfilt('bandpassfir','FilterOrder',FiltOrder, 'CutoffFrequency1',Fcut1,'CutoffFrequency2',Fcut2, 'SampleRate',Fs);
%d = designfilt('bandpassfir','PassbandRipple',Ap,'StopbandAttenuation',Ast, 'CutoffFrequency1',Fcut1,'CutoffFrequency2',Fcut2, 'SampleRate',Fs);
d = designfilt('bandpassiir','FilterOrder', 8, 'PassbandFrequency1',0.7, 'PassbandFrequency2',4, 'SampleRate',Fs);
hfvt = fvtool(d);

%% 3
[b, a]= butter(8,[0.7 4]/(30/2));
h = fvtool(b,a);