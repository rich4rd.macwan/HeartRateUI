clear all;
close all;
clc;

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
filterSize = 10;
resizeFactor = 0.25;
h = ones(filterSize,filterSize) / (filterSize^2); % avegarge filter

vidFolder ='/home/yannick/ownCloud/PPGImaging/vid1/';
outFolder='/home/yannick/ownCloud/PPGImaging/vid1/';

% load pulse traces
filename = [outFolder 'multiplePulseTracesChrom_' num2str(filterSize) 'x' num2str(filterSize)];
load(filename);

% load PPG
[gtTrace, gtHR, gtTime] = loadPPG(vidFolder);
gtTime = gtTime / 1000;  % in seconds

load([vidFolder 'mask.mat']); 
mask = imresize(mask, resizeFactor);


%% Display one trace
imgName = dir([vidFolder '/img/frame_' num2str(1) '_*.bmp']);
imgName = imgName.name;
img = imread([vidFolder '/img/' imgName]);
img = imfilter(img,h);
img = imresize(img, resizeFactor);

while(1);
    figure(1);
    imshow(img);
    [c,r]=ginput(1);
    r=round(r); c=round(c);
    fprintf('plot traces for pixel r=%i and c=%i \n', r, c);

    %get the pulse 
    pulse = squeeze(pulseTracesMat(r,c,:));

    % filter
    % get coef of butterworth filter
    LOW_F=.7; % low freq for BP filtering
    UP_F=4; % high freq for BP filtering
    % get exact Fs
    Fs = 1/mean(diff(timeTrace));
    %[b, a]= butter(4,[LOW_F UP_F]/(Fs/2));
    FiltOrder = 4;
    
    %d = designfilt('bandpassiir','FilterOrder', FiltOrder, 'PassbandFrequency1',Fcut1, 'PassbandFrequency2',Fcut2, 'SampleRate',Fs);
    d = designfilt('bandpassfir','FilterOrder', FiltOrder, 'CutoffFrequency1',LOW_F,'CutoffFrequency2',UP_F, 'SampleRate',Fs);
    %hfvt = fvtool(d);

    pulseFiltered = filter(d,pulse')';

    figure(2), clf, title('Pulse trace'), xlabel('seconds'), ylabel('pixel values'), xlim([1 20]);
    hold on;
    p1 = plot(gtTime, gtTrace/max(gtTrace), 'k');
    p2 = plot(timeTrace, 1+pulse/max(pulse), 'm');
    p3 = plot(timeTrace, 2+pulseFiltered/max(pulseFiltered), 'b');
    legend([p1,p2],'PPG', 'rPPG');
    hold off;
end



