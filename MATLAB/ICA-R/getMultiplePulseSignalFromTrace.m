function getMultiplePulseSignalFromTrace(varargin)
% preprocess RGB traces to get pulse trace
% Pixel-wise rPPG methods
% yannick.benezeth@u-bourgogne.fr
% 26/11/2016

clear all;
close all;
clc;

addpath('../main-stable/');
%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
LOW_F=.7; % low freq for BP filtering
UP_F=4; % high freq for BP filtering
FILTER_ORDER = 4; % Order of the butterworth BP filter
SHOWPLOTS = 1;
filterSize = 10;

vidFolder ='/home/yannick/ownCloud/PPGImaging/vid1/';
outFolder='/home/yannick/ownCloud/PPGImaging/vid1/';

matFileName = ['rgbMultipleTraces_' num2str(filterSize) 'x' num2str(filterSize)]; % input file

disp(['Get Pulse from ' vidFolder matFileName '.mat']);

disp(['Loading stuff...']);
% load mat file
load([vidFolder matFileName '.mat']);
[nR, nC, nChannel, traceSize] = size(rgbMultipleTraces);
timeTrace = timeTrace/1000; % in second
Fs = 1/mean(diff(timeTrace)); % get exact Fs

pulseTracesMat = zeros(nR, nC, traceSize);
pulseTracesChrom = pulseTracesMat;
pulseTracesGreen = pulseTracesMat;
clear pulseTracesMat;

% Create filter
d = designfilt('bandpassfir','FilterOrder', FILTER_ORDER, 'CutoffFrequency1',LOW_F,'CutoffFrequency2',UP_F, 'SampleRate',Fs);
delay = mean(grpdelay(d)); % filter delay in samples
%hfvt = fvtool(d);
    
% load PPG
[gtTrace, gtHR, gtTime] = loadPPG(vidFolder);
gtTime = gtTime / 1000;  % in seconds

disp(['Starting the real work...']);
% get pulse trace for each pixel
for r=1:nR
    fprintf('\n r=%i, c=', r);
         
    for c=1:nC
        if (mod(c,50)==0)
            fprintf('%i, ', c);
        end
        % get current traces
        crtTrace = rgbMultipleTraces(r,c,:,:);  
        crtTrace = squeeze(crtTrace);
                
        % if 0 (out of the mask) -> do nothing
        if (sum(crtTrace(1,:)) ~= 0)
            % Uniform sampling
            timeTraceUnif = linspace(timeTrace(1), timeTrace(end), traceSize);
            for k=1:nChannel
                crtTrace(k,:) = interp1(timeTrace,crtTrace(k,:), timeTraceUnif, 'spline');
            end
            timeTrace = timeTraceUnif;
                               
               
                  % normalize each signal to have zero mean and also unit variance
%                 crtTrace = crtTrace - repmat(mean(crtTrace,2),[1 traceSize]);
%                 crtTrace = crtTrace ./ repmat(std(crtTrace,0,2),[1 traceSize]);

                crtTrace = crtTrace ./ repmat(mean(crtTrace,2),[1 traceSize]) - 1;
                if(0)
                    figure(1), plot(timeTrace, crtTrace(2,:)),title('raw normalized green trace'), xlabel('seconds'), ylabel('pixel values');
                     xlim([1 20]);
                end
                
%                 % smooth
%                 for k=1:3
%                     crtTrace(k,:)=smooth(crtTrace(k,:),10);                                   
%                 end
%                 if(SHOWPLOTS)
%                     figure(), title('smoothed traces'), xlabel('seconds'), ylabel('pixel values'), xlim([1 20]);
%                     hold on;
%                     p1 = plot(gtTime, gtTrace/max(gtTrace), 'k');
%                     %p2 = plot(timeTrace, crtTrace(1,:)/max(crtTrace(1,:)), 'r');
%                     p3 = plot(timeTrace, crtTrace(2,:)/max(crtTrace(2,:)), 'g');
%                     %p4 = plot(timeTrace, crtTrace(3,:)/max(crtTrace(3,:)), 'b');
%                     hold off;
%                 end

                % detrend
                for k=1:3
                    crtTrace(k,:) = detrendsignal(crtTrace(k,:)')';
                end
                if(SHOWPLOTS)
                    figure(2),title('detrended traces'), xlabel('seconds'), ylabel('pixel values'), xlim([1 20]);
                    hold on;
                    p1 = plot(gtTime, gtTrace/max(gtTrace), 'k');
                    %plot(timeTrace, crtTrace(1,:),'r');
                    p2 = plot(timeTrace, crtTrace(2,:)/max(crtTrace(2,:)),'g');
                    %plot(timeTrace, crtTrace(3,:),'b');
                    hold off;
                end
               
                % filter
                for k=1:3
                    tmp = filter(d,[crtTrace(k,:)'; zeros(delay,1)]); % Append D zeros to the input data
                    crtTrace(k,:) = tmp(delay+1:end)';                  % Shift data to compensate for delay
                end
                if(SHOWPLOTS)
                    figure(3), title('filtered traces'), xlabel('seconds'), ylabel('pixel values'), xlim([1 20]);
                    hold on;
                    p1 = plot(gtTime, gtTrace/max(gtTrace), 'k');
                    %p2 = plot(timeTrace, crtTrace(1,:)/max(crtTrace(1,:)), 'r');
                    p3 = plot(timeTrace, crtTrace(2,:)/max(crtTrace(2,:)), 'g');
                    %p4 = plot(timeTrace, crtTrace(3,:)/max(crtTrace(3,:)), 'b');
                    hold off;
                end
                
%                % Method 1: Green channel
%                pulseTraceRed = crtTrace(1,:);
                 pulseTraceGreen = crtTrace(2,:);
%                pulseTraceBlue = crtTrace(3,:);
                
                % Method 2: CHROM
                Rf = crtTrace(1,:);
                Gf = crtTrace(2,:);
                Bf = crtTrace(3,:);
                Xf = 3*Rf-2*Gf;
                Yf = 1.5*Rf+Gf-1.5*Bf;
                alpha = std(Xf)/std(Yf);
                pulseTraceChrom = Xf - alpha*Yf;
                
                if(SHOWPLOTS)
                    figure(4), title('Pulse trace'), xlabel('seconds'), ylabel('pixel values'), xlim([1 20]);
                    hold on;
                    p1 = plot(gtTime, gtTrace/max(gtTrace), 'k');
                    p2 = plot(timeTrace, pulseTraceChrom/max(pulseTraceChrom), 'm');
                    legend([p1,p2],'PPG', 'Chrom');
                    hold off;
                    pause();
                    close all;
                end
                
                % save pulse traces
                % for Chrom
                pulseTrace = pulseTraceChrom;
                pulseTrace = reshape(pulseTrace, [1, size(pulseTrace)]);
                pulseTracesChrom(r,c,:)=pulseTrace;
                % for Green
                pulseTrace = pulseTraceGreen;
                pulseTrace = reshape(pulseTrace, [1, size(pulseTrace)]);
                pulseTracesGreen(r,c,:)=pulseTrace;
        end
    end % end r
end % end c

% save Mat files
filename = [outFolder 'multiplePulseTracesChrom_' num2str(filterSize) 'x' num2str(filterSize)];
pulseTracesMat = pulseTracesChrom;
save(filename, 'pulseTracesMat','timeTrace', '-v7.3');

filename = [outFolder 'multiplePulseTracesGreen_'   num2str(filterSize) 'x' num2str(filterSize)];
pulseTracesMat = pulseTracesGreen;
save(filename, 'pulseTracesMat','timeTrace', '-v7.3');
