close all;
clc;

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
filterSize = 10;
resizeFactor = 0.25;
h = ones(filterSize,filterSize) / (filterSize^2); % avegarge filter

vidFolder ='C:\Users\yanni\ownCloud\PPGImaging\vid1\';
outFolder='C:\Users\yanni\ownCloud\PPGImaging\vid1\';

% load one pulse trace
filename = [outFolder 'pulse.mat'];
load(filename);

% load PPG
[gtTrace, gtHR, gtTime] = loadPPG(vidFolder);
gtTime = gtTime / 1000;  % in seconds

% Display one trace

% filter
% get coef of butterworth filter
LOW_F=.7; % low freq for BP filtering
UP_F=4; % high freq for BP filtering
% get exact Fs
Fs = 1/mean(diff(timeTrace));

Fcut1 = 0.7;
Fcut2 = 4;
width = 0.2;
FiltOrder = 4;

%[b, a]= butter(4,[LOW_F UP_F]/(Fs/2));


%hfvt = fvtool(d);
%% IIR without phase shift compensation
d = designfilt('bandpassiir','FilterOrder', FiltOrder, 'PassbandFrequency1',Fcut1, 'PassbandFrequency2',Fcut2, 'SampleRate',Fs);
pulseFiltered1 = filter(d,pulse);
pulseFiltered2 = filtfilt(d,pulse);
grpdelay(d,2048,Fs);   % plot group delay -> useful to check if delay is constant over frequencies

% %% FIR with phase compensation
d = designfilt('bandpassfir','FilterOrder', FiltOrder, 'CutoffFrequency1',Fcut1,'CutoffFrequency2',Fcut2, 'SampleRate',Fs);
%grpdelay(d,2048,Fs);   % plot group delay -> useful to check if delay is constant over frequencies
D = mean(grpdelay(d)); % filter delay in samples
pulseFiltered1 = filter(d,[pulse; zeros(D,1)]); % Append D zeros to the input data
pulseFiltered1 = pulseFiltered1(D+1:end);                  % Shift data to compensate for delay
% without phase compensation
pulseFiltered2 = filter(d,pulse);

%% Plot
figure(2), clf, title('Pulse trace'), xlabel('seconds'), ylabel('pixel values'), xlim([1 20]);
hold on;
p1 = plot(gtTime, gtTrace/max(gtTrace), 'k');
p2 = plot(timeTrace, 1+pulse/max(pulse), 'b');
p3 = plot(timeTrace, 1+pulseFiltered1/max(pulseFiltered1), 'r', 'linewidth',1.5);
p3 = plot(timeTrace, 1+pulseFiltered2/max(pulseFiltered2), 'g', 'linewidth',1.5);
legend([p1,p2],'PPG', 'rPPG');
hold off;



