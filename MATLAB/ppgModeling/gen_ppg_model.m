% Constrained ICA with PPG modeling
% rich4rd.macwan@gmail.com
% 15/02/2016

clear all;
close all;
clc;

%% Initialisation
SHOW_PLOTS = 1;
PPG_OUTLIER_REJECTION_THRESHOLD=.7;
RPPG = 0; % if 0 then work with PPG
folderName = '../main-stable/data/RichardSummer2015/5-gt';

Fs = 256;
nfft = Fs*60; % nb of points for FFT
shift = nfft - Fs; % sliding window shift for spectrogram

%% PPG & HRV
if(~RPPG)
    % load PPG data
    [trace, gtHR, time] = loadPPG(folderName);
    time = time / 1000;  % in seconds
else
    % load rPPG data
    % [trace, time] = loadrPPG(folderName);
    % trace = trace';
    % time = time';
end;

% find peaks
[pks, locs]=findpeaks(trace, time, 'MinPeakProminence',1);
[troughs, locs_troughs]=findpeaks(-trace, time, 'MinPeakProminence',1);

%SEGMENTATION
%Diastoles: regions of the trace between each peak and the next trough
%Systoles: regions of the trace between each trough and the next peak


%Calculate average amplitude. Pulse height is avg height of TPT or PTP
%If a trough appears first, find TPT
nPeaks=numel(locs);
nTroughs=numel(locs_troughs);
nPulses=nTroughs-1;

if(nTroughs>nPeaks)
    avgTroughs=0.5*(troughs(1:end-1)+troughs(2:end));
    amp=mean(pks-avgTroughs);
    Wsys=locs-locs_troughs(1:end-1);
    Wdia=locs_troughs(2:end)-locs;
elseif (nTroughs<nPeaks)
    %Find PTP
    avgPeaks=.5*(pks(1:end-1)+pks(2:end));
    amp=mean(avgPeaks-troughs);
    Wsys=locs(2:end)-locs_troughs;
    Wdia=locs_troughs-locs(1:end-1);
else
    avgPeaks=.5*(pks(1:end-1)+pks(2:end));
    amp=mean(avgPeaks-troughs(2:end));
    if(locs(1)<locs_troughs(1))
        Wsys=locs(2:end)-locs_troughs(1:end-1);
        Wdia=locs_troughs-locs;
    else
        Wsys=locs-locs_troughs;
        Wdia=locs_troughs(2:end)-locs(1:end-1);
    end
end

% get interbeat interval (IBI = HRV)
IBI = diff(locs);
IBITime = locs(2:end);

% not evenly spaced -> resample
inc = 1/Fs;
endTime = time(end);
newTime = 0 : inc : endTime;
traceInterp = interp1(time,trace,newTime, 'pchip');
IBIInterp = interp1(IBITime,IBI,newTime, 'pchip');
gtHRInterp=interp1(time,gtHR,newTime, 'pchip');

if (SHOW_PLOTS)
    % Plot PPG data
    figure,hold on;
    title('PPG & HRV trace')
    xlabel('Time(s)')
    p1 = plot(newTime,traceInterp);
    plot(Wsys);
    plot(locs,pks,'X','MarkerSize',12)
    plot(locs_troughs,-troughs,'o','MarkerSize',5)
    
    % Plot HRV
    p2 = plot(newTime,IBIInterp, 'r', 'LineWidth',2);
    %p3 = plot(HRVTime,HRV, 'g--', 'LineWidth',2);
    
    legend([p1, p2],'PPG', 'IBI');
%     xlim([0 30]);
    hold off;
end


%% HRV temporal features
RRmean = mean(IBI);
RRstd = std(IBI);
% square root of the mean of the sum of the squares of differences between subsequent NN intervals
x = diff(IBI);
x = x.^2;
x = sum(x)/(length(x));
RMSSD = sqrt(x);
clear x;

%% Freq analysis on PPG

x = traceInterp;
window = hanning(nfft);
[s,f,t,p] = spectrogram(x,window,shift, linspace(0.3, 3.5,nfft), Fs,'yaxis');


% show PPG spectrogram
% if (SHOW_PLOTS)
%     figure;
%     imagesc(t,f,abs(s));
%     axis xy,colormap('jet'),colorbar, xlabel('Time (s)'), ylabel('Frequency (Hz)'),ylim([0.3 3.5]);
%     title('PPG spectrogram (Matlab toolbox)');
% end

% HR from spectrogram
[q,nd] = max(10*log10(p));
HR_spec = 60*f(nd);
HR_specTime = t;


% HR from HRV
HR_ibi = 60*1./IBI;
HR_ibi = smooth(HR_ibi, 15);

hrmean=mean(HR_ibi);
hrstd=std(HR_ibi);
%Obtain Single average pulse
IBI_avg=mean(IBI);
Wsys_avg=mean(Wsys);
Wdia_avg=mean(Wdia);

avgPulseTime=0:inc:IBI_avg;
avgSysTime=0:inc:Wsys_avg;
avgDiaTime=Wsys_avg:inc:IBI_avg;

accumPulses=zeros(nPulses,numel(avgPulseTime));
accumDia=zeros(nPulses,numel(avgDiaTime));
accumSys=zeros(nPulses,numel(avgSysTime));
%Interpolate each pulse to avgPulse length and then average them 
X=zeros(1,numel(avgPulseTime)*nPulses);
Y=X;
n=numel(avgPulseTime);
pks_btwn_troughs=locs(locs>locs_troughs(1) & locs < locs_troughs(end));
figure
pulPkPos=zeros(nPulses,1); %Store peak positions of each pulse
for i=1:nPulses-1
    hold on
    
    t=time(time>=locs_troughs(i) & time<=locs_troughs(i+1));
    
    %pchip does not interpolate correctly for all pulses, therefore v5cubic
    %pulse=interp1(t-t(1),trace(time>=locs_troughs(i) & time<=locs_troughs(i+1)),avgPulseTime,'v5cubic');
    
    %Smooth out the Nans given by interpolation. 
    %nextTrough=trace(time==locs_troughs(i+1));
    %nanIndexes=isnan(pulse);
    %nonNans=pulse(~nanIndexes);
    %pulse(nanIndexes)=linspace(nonNans(end),nextTrough,nnz(nanIndexes));
    
    %adjustSignalLength handles the interpolation if avgPulseTime is
    %greater then the time of the current pulse
    accumPulses(i,:)=adjustSignalLength(t-t(1),trace(time>=locs_troughs(i) & time<=locs_troughs(i+1)),avgPulseTime);
%   tsys=time(time>=locs_troughs(i) & time<=pks_btwn_troughs(i));
%   tdia=time(time>=pks_btwn_troughs(i) & time<=locs_troughs(i+1));
%   accumSys(i,:)=interp1(tsys-tsys(1),trace(time>=locs_troughs(i) & time<=pks_btwn_troughs(i)),avgSysTime,'pchip');
%   accumDia(i,:)=interp1(tdia-tdia(1)+tsys(end)-tsys(1),trace(time>=pks_btwn_troughs(i) & time<=locs_troughs(i+1)),avgDiaTime,'pchip');
    [p,l]=findpeaks(accumPulses(i,:),avgPulseTime);
    pulPkPos(i)=l(p==max(p));
    plot(avgPulseTime,accumPulses(i,:));
end

figure
%Align each pulse keeping peak as the focus. Better morphology
mP=mean(pulPkPos); %Mean pulse peak position
%Index of mean Pulse peak position
edges = [-Inf, mean([avgPulseTime(2:end); avgPulseTime(1:end-1)]), +Inf];
I = discretize(mP, edges);

for i=1:size(accumPulses,1)
    %hold on
    %plot(avgPulseTime,accumPulses(i,:));
    currPulse=accumPulses(i,:);
    %Find index of nearest time position in avgPulseTime
    pI=discretize(pulPkPos(i), edges);
    pkDiff=I-pI;
    
    %Shift the pulse right if peakDisplacement>0
    if(pkDiff>0)
        accumPulses(i,:)=[currPulse(end-pkDiff:end-1) currPulse(1:end-pkDiff)];
    end
    %Shift the pulse left if peakDisplacement<0
    if(pkDiff<0)
        accumPulses(i,:)=[currPulse(-pkDiff:end-1) currPulse(1:-pkDiff)];
    end
   % plot(avgPulseTime,accumPulses(i,:));
    
end

avgPulse=mean(accumPulses);

%Outlier Rejection
diffPulse=abs(bsxfun(@minus,accumPulses,avgPulse));
meanDiffPulse=mean(abs(diffPulse),2);

%Normalize the difference measure of each pulse and eliminate those which
%are outliers
meanDiffPulseN=mat2gray(meanDiffPulse);

%Remove the pulses that have a difference higher than .7
filteredPulses=accumPulses(meanDiffPulseN<=PPG_OUTLIER_REJECTION_THRESHOLD,:);

%Average pulse after removing outliers
avgFilteredPulse=mean(filteredPulses);
avgFilteredPulse=mat2gray(avgFilteredPulse);

WsysY=avgFilteredPulse(avgPulseTime<=Wsys_avg);
avgPulseTime_sys=avgPulseTime(avgPulseTime<=Wsys_avg);
avgPulseTime_dia=avgPulseTime(avgPulseTime>Wsys_avg);
Wsys_nearest=avgPulseTime_sys(end);

%Final average systole and diastole
avgSystole=avgFilteredPulse(avgPulseTime<=Wsys_nearest);
avgDiastole=avgFilteredPulse(avgPulseTime>=Wsys_nearest);

% hold on
% plot(avgPulseTime,avgFilteredPulse,'LineWidth',3);
% plot(avgPulseTime,[avgSystole avgDiastole])


%% Fourier fitting
%fun_pulse= @(a0,a1,b1,a2,b2,a3,b3,w,x) a0 + a1*cos(x*w) + b1*sin(x*w) + ...
%               a2*cos(2*x*w) + b2*sin(2*x*w) + a3*cos(3*x*w) + b3*sin(3*x*w)

[fitresult,gof]=createFit(avgPulseTime,avgFilteredPulse);
calculatedPulse=feval(fitresult.fmodel,avgPulseTime);

%calculatedPulse=feval(fitresult,[avgPulseTime avgPulseTime+avgPulseTime(end)]);
% plot([avgPulseTime avgPulseTime+avgPulseTime(end)],calculatedPulse);
figure
hold on
h=plot(avgPulseTime,avgFilteredPulse,'.',avgPulseTime,calculatedPulse);

legend( h, 'avgFilteredPulse vs. avgPulseTime', 'FitFourier', 'Location', 'NorthEast' );

xlabel avgPulseTime
ylabel avgFilteredPulse
grid on
save('fitresult','fitresult');

% show HRs
if (SHOW_PLOTS)
    figure, hold on;
    title('Heart Rate');
    p1 = plot(IBITime,HR_ibi);
    %p2 = plot(HR_specTime,HR_spec,'r');
    p4=plot(newTime,gtHRInterp);
    legend([p1, p4],'HR from IBI','HR from GT');
    ylim([50 110]);
    hold off;
end

%% HRV frequency features with spectrogram

x = IBIInterp;
x = x-mean(x);
window = hanning(nfft);
[s,f,t,p] = spectrogram(x,window,shift,linspace(0.04, 0.4, nfft),Fs,'yaxis');
LF = sum(sum(abs(s(f>0.04 & f<0.15,:))));
HF = sum(sum(abs(s(f>0.15 & f<0.4,:))));
totalNrj = sum(sum(abs(s)));
LF = LF / totalNrj;
HF = HF / totalNrj;
LFoverHF = LF/HF;

% if (SHOW_PLOTS)
%     % we should observe more LF when stress
%     figure, hold on;
%     title('HRV spectrogram (Matlab toolbox)');
%     imagesc(t,f,abs(s)),axis xy,colormap('jet'),colorbar, xlabel('Time (s)'), ylabel('Frequency (Hz)');
%     ylim([0.04 0.4]);
%     hold off;
% end

%% HRV frequency features with periodogram
% plomb(HRV,HRVTime,'power')

