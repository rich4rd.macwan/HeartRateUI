function resultStruct=getHRFromPulse(varargin)
    % Extract HR from pulse trace
    % sliding window based max peak PSD
    % contact: yannick.benezeth@u-bourgogne.fr
    % first version:  18/02/2016
    
    tic
    close all;
    clc;

    addpath('../BlandAltman/');
    addpath('../tools/DP/');

    %%%%%%%%%%%
    % Parameters
    %%%%%%%%%%%
    VERBOSE = 1; % 0 -> no plot, 1 -> some intermediate plots, 2 -> debug mode
    VIDFOLDER='D:\Dataset\HeartRate\RichardSummerVideos\5-gt\';
    POSTPROC = 'Kalman'; % DP, Heuristic, Kalman, None

    i = 1;
    while(nargin > i)
        if(strcmp(varargin{i},'VIDFOLDER')==1)
            VIDFOLDER=varargin{i+1};
        end
        if(strcmp(varargin{i},'VERBOSE')==1)
            VERBOSE=varargin{i+1};
        end
        if(strcmp(varargin{i},'POSTPROC')==1)
            POSTPROC=varargin{i+1};
        end
        i = i+2;
    end

    matFileName = 'pulseTrace'; % input file
    winLengthSec = 20; % length of the sliding window (in seconds)
    stepSec = 0.5; % step between 2 sliding window position (in seconds)
    lowF=.7; % low freq for PSD range
    upF=3.5; % high freq for PSD range
    saveResults = 1;
    lagAtBeginning = 0; % Begin after x seconds
    nbMaxCandidates = 3 ; % for post-proc
    waitTime = 0.1;

    fprintf('Get HR from pulse : %s%s.mat \n',VIDFOLDER, matFileName);
    fprintf('winLengthSec=%i sec\n',winLengthSec);
    fprintf('stepSec=%.2f \n',stepSec);
    fprintf('lowF=%.2f and upF=%.2f \n',lowF, upF);
    fprintf('saveResults=%i \n',saveResults);
    fprintf('VERBOSE=%i \n',VERBOSE);
    fprintf('POSTPROC=%s \n',POSTPROC);
    

    pulseFile = [VIDFOLDER matFileName '.mat'];
    if(exist(pulseFile, 'file') == 2)
        load(pulseFile);
    else
        disp('oops, no input file');
        return;
    end
    
    if(size(pulseTrace,1) > 1)
        overlapAdd = 0; % dirty trick to manage no overlap mode with ICA-like methods
        fprintf('no overlap mode (ICA-like methods)\n');
    else
        overlapAdd = 1; % normal mode
    end
    fprintf('Processing...');
        
    
    resultStruct = [];
    
    traceSize = length(timeTrace);
    % get exact Fs
    Fs = 1/mean(diff(timeTrace));

    % load PPG
    [gtTrace, gtHR, gtTime] = loadPPG(VIDFOLDER);
    gtTime = gtTime / 1000;  % in seconds

    % Uniform sampling
    gtTimeUnif = linspace(gtTime(1), gtTime(end), length(gtTrace));
    gtTrace = interp1(gtTime,gtTrace, gtTimeUnif);
    gtTime = gtTimeUnif;

    if((VERBOSE == 2) && overlapAdd)
        figure(1);
        hold on;
        p1 = plot(gtTime, gtTrace, 'k');
        p2 = plot(timeTrace, -pulseTrace, 'b', 'Linewidth', 1.5);
        xlim([5 25]),
        title('Final rPPG trace');
        legend([p1 p2], 'PPG', 'rPPG');
        hold off;
        pause(waitTime)
    end

    winLength = round(winLengthSec*Fs); % length of the sliding window for FFT
    step = round(stepSec*Fs);
    if(lagAtBeginning == 0)
        beg = 1;
    else
        beg = round(lagAtBeginning*Fs);
    end

    ind = 1;
    halfWin = (winLength/2);
    for i=halfWin:step:traceSize-halfWin

        %%%%%%%%%%%%%%
        % Get current windows
        %%%%%%%%%%%%%%
        % get start/end index of current window
        startInd = i-halfWin+1;
        endInd = i+halfWin;
        startTime = timeTrace(startInd);
        endTime = timeTrace(endInd);

        % get current pulse window
        if(overlapAdd)
            crtPulseWin = pulseTrace(startInd:endInd);
        else
            crtPulseWin = pulseTrace(ind,:); % dirty trick -> winLengthSec should be similar in getHR and in getPulse
        end
        crtTimeWin = timeTrace(startInd:endInd);

        % get current PPG window
        [~, startIndGt] = min(abs(gtTime-startTime));
        [~, endIndGt] = min(abs(gtTime-endTime));
        crtPPGWin = gtTrace(startIndGt:endIndGt);
        crtTimePPGWin = gtTime(startIndGt:endIndGt);

        % get exact PPG Fs
        Fs_PPG = 1/mean(diff(crtTimePPGWin));

        if (VERBOSE == 2)
            fig2 = figure(2);
            clf(fig2);
            hold on;
            plot(crtTimePPGWin, crtPPGWin, 'k')
            plot(crtTimeWin, crtPulseWin, 'b', 'Linewidth', 1.5);
            legend('PPG', 'Pulse trace'),
            hold off;
            pause(waitTime);
        end

        %%%%%%%%%%%%%%
        % Spectral analysis
        %%%%%%%%%%%%%%
        % rPPG
        [rPPG_freq, rPPG_power] = getSpectrum(crtPulseWin, Fs, 'FFT');
        rangeRPPG = (rPPG_freq>lowF & rPPG_freq < upF);   % frequency range to find PSD peak
        rPPG_power = rPPG_power(rangeRPPG);
        rPPG_freq = rPPG_freq(rangeRPPG);
        [pksrPPG,loc] = findpeaks(rPPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks (only one is used now but will be useful for postpreocessing
        if (isempty(pksrPPG))
            pksrPPG = 1;
            loc = 1;
        end
        rPPG_peaksLoc = rPPG_freq(loc);

        % PPG
        if(~isempty(crtPPGWin)) % sometime there is no PPG
            [PPG_freq, PPG_power] = getSpectrum(crtPPGWin, Fs_PPG, 'FFT');
            rangePPG = (PPG_freq>lowF & PPG_freq < upF); % frequency range to find PSD peak
            PPG_power = PPG_power(rangePPG);
            PPG_freq = PPG_freq(rangePPG);
            [pksPPG,loc] = findpeaks(PPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks
            if (isempty(pksPPG))
                pksPPG = 1;
                loc = 1;
            end
            PPG_peaksLoc = PPG_freq(loc);
        end
        
        if (VERBOSE == 2)
            % Plot intermediate spectra
            fprintf('rPPG HR: %.2f \t',rPPG_peaksLoc(1)*60);
            fprintf('PPG HR: %.2f \n',PPG_peaksLoc(1)*60);

            maxy = max(pksrPPG(1), pksPPG(1));
            fig4 = figure(4);
            clf(fig4);
            subplot(1,2,1);
            hold on;grid on;
            plot(rPPG_freq,rPPG_power);
            plot(rPPG_peaksLoc(1), pksrPPG(1), 'r*');
            xlim([0.1 3.5]), ylim([0, maxy*1.1]), title('rPPG periodogram'), xlabel('Heart Rate (Hz)');
            hold off;

            subplot(1,2,2);
            hold on; grid on;
            plot(PPG_freq,PPG_power)
            plot(PPG_peaksLoc(1), pksPPG(1), 'r*');
            xlim([0.1 3.5]),  ylim([0, maxy*1.1]), title('PPG periodogram'), xlabel('Heart Rate (Hz)');
            hold off;
            pause(waitTime)
        end

        % will be used for post proc
        tfrFull(:,ind)=rPPG_power;

        % get SNR for evaluation
        width = 0.3;
        if (~isempty(PPG_peaksLoc))
            % SNR is more intersting with FFT so we get spectra again (I don't care about processing cost :)
            [rPPG_freq, rPPG_power] = getSpectrum(crtPulseWin, Fs, 'FFT');
            rangeRPPG = (rPPG_freq>lowF & rPPG_freq < upF);
            rPPG_power = rPPG_power(rangeRPPG);
            rPPG_freq = rPPG_freq(rangeRPPG);
            [pksrPPG,~] = findpeaks(rPPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks (only one is used now but will be useful for postpreocessing
                        
            [PPG_freq, PPG_power] = getSpectrum(crtPPGWin, Fs_PPG, 'FFT');
            rangePPG = (PPG_freq>lowF & PPG_freq < upF);
            PPG_power = PPG_power(rangePPG);
            PPG_freq = PPG_freq(rangePPG);
            [pksPPG,~] = findpeaks(PPG_power,'SortStr','descend','NPeaks',1);
            
            range1 = (rPPG_freq>(PPG_peaksLoc(1)-width/2) & rPPG_freq < (PPG_peaksLoc(1)+width/2));
            range2 = (rPPG_freq>(PPG_peaksLoc(1)*2-width/2) & rPPG_freq < (PPG_peaksLoc(1)*2+width/2));
            range = range1 + range2;
            signal = rPPG_power.*range;
            noise = rPPG_power.*(~range);
            n = sum(noise);
            s = sum(signal);
            snr(ind) = 10*log10(s/n);
            
            if(VERBOSE == 2)
                fig3 = figure(3);
                clf(fig3);
                hold on;
                plot(rPPG_freq,noise,'r');
                plot(rPPG_freq,signal,'b');
                legend (['SNR (dB) = ' num2str(snr(ind))])
                hold off;
                pause(waitTime);
            end                     
        end

        %%%%%%%%%%%%%%%%%%
        % save estimations
        %%%%%%%%%%%%%%%%%%
        % time
        crtTime =  (startTime + endTime)/2;
        time(ind) = crtTime;

        % HR from sensor
        if(~isempty(gtTime))
            [~, crtTimeInd] = min(abs(gtTime-crtTime));
            HR_sensor(ind) = gtHR(crtTimeInd);
        end

        % HR from PPG
        if(~isempty(gtTime))
            HR_PPG(ind) = PPG_peaksLoc(1)*60;
        else
            HR_PPG(ind) = 1;
        end

        % HR from rPPG
        if (pksrPPG ~= 1) % at least one peak
            HR_RPPG(ind) = rPPG_peaksLoc(1)*60;
        else
            % no detection, we use the last one
            if ind > 1
                HR_RPPG(ind)= HR_RPPG(ind-1);
            else
                %First RPPG win has no peak
                HR_RPPG(ind)= 0;
            end
        end
        
        % Will be used during post-processing
        for j=1:nbMaxCandidates
            if j>length(rPPG_peaksLoc)
                tfr(ind,j,1) = 0;
                tfr(ind,j,2) = 0;
            else
                tfr(ind,j,1) = rPPG_peaksLoc(j)*60;
                tfr(ind,j,2) = pksrPPG(j);
            end
        end
        ind=ind+1;
    end

    %%%%%%%%%%%%%%%%%
    % Post processing
    %%%%%%%%%%%%%%%%%%
    switch POSTPROC
        case 'None'
            HR_RPPG_filtered = HR_RPPG;
            
        case 'Kalman'
            %Kalman parameters
            trendTolerance=10; %Size of window to calculate trend and trend difference
            threshold=10;
            kalmanFilter= configureKalmanFilter('ConstantVelocity',HR_RPPG(1), [1 1]*1e5, [10,10], 1e5);
            HR_RPPG_filtered(1) = HR_RPPG(1);

            for i=2:length(HR_RPPG)
                %Reconfigure kalman to compensate for changing HR
                if (mod(i,6)==0) %% ??
                    kalmanFilter= configureKalmanFilter('ConstantVelocity',HR_RPPG_filtered(i-1), [1 1]*1e5, [10,10], 1e5);
                end

                currDiff=abs(HR_RPPG(i-1)-HR_RPPG(i));
                trendDiff=currDiff;

                if i>trendTolerance
                    trend=mean(HR_RPPG(i-trendTolerance:i-1));
                    trendDiff=abs(trend-HR_RPPG(i));
                    %stem(ind,trendDiff,'b+');
                end

                if(currDiff > threshold && trendDiff>threshold)
                    %Also check if the new values are consistent
                    if i>trendTolerance/2
                        newTrend=mean(HR_RPPG(i-round(trendTolerance/3):i-1));
                        newTrendDiff=abs(newTrend-HR_RPPG(i));
                        if(newTrendDiff<threshold)
                            %Seems the new values are consistent, choose, and
                            %update kalman
                            HR_RPPG_filtered(i)=HR_RPPG(i);
                            predict(kalmanFilter);
                            correct(kalmanFilter,HR_RPPG_filtered(i));

                        else
                            %New value is very different from previous as well as trend
                            %predict
                            HR_RPPG_filtered(i)=predict(kalmanFilter);
                            %Probability of HR being less than 50 is quite
                            %low. Maybe prediction is on the wrong trend
                            if HR_RPPG_filtered(i)<50 && HR_RPPG(i)>50
                                HR_RPPG_filtered(i)=HR_RPPG(i);
                                predict(kalmanFilter);
                                correct(kalmanFilter,HR_RPPG_filtered(i));
                            end
                        end
                    else
                        %New value is very different from previous as well as trend
                        %predict
                        HR_RPPG_filtered(i)=predict(kalmanFilter);
                        %Probability of HR being less than 50 is quite
                        %low. Maybe prediction is on the wrong trend
                        if HR_RPPG_filtered(i)<50 && HR_RPPG(i)>50
                            HR_RPPG_filtered(i)=HR_RPPG(i);
                            predict(kalmanFilter);
                            correct(kalmanFilter,HR_RPPG_filtered(i));
                        end
                    end
                elseif currDiff>threshold && trendDiff<threshold
                    %New value is close to the trend
                    HR_RPPG_filtered(i)=HR_RPPG(i);
                    %predict(kalmanFilter);
                    correct(kalmanFilter,HR_RPPG_filtered(i));
                elseif currDiff<threshold && trendDiff>threshold
                    %New value is close to previous value,wrong trend
                    %correct
                    HR_RPPG_filtered(i)=HR_RPPG(i);
                    predict(kalmanFilter);
                    correct(kalmanFilter,HR_RPPG_filtered(i));
                else
                    HR_RPPG_filtered(i)=HR_RPPG(i);
                    %predict(kalmanFilter);
                    correct(kalmanFilter,HR_RPPG_filtered(i));
                end
            end

        case 'DP'
            % Dynamic programming
            rahrt_data_inst=rahrt_data(tfr, stepSec);
            i=size(tfr,1);
            result=rahrt(i,rahrt_data_inst);

            % we reconstruct the optimal path
            % bestPath(1,:) -> Hz
            % bestPath(2,:) -> Nrj
            [~, idx]= max(result.D(:,i)) ;
            bestPath(1,i) = result.locs(idx,i);
            bestPath(2,i) = result.pks(idx,i);
            nextIdx = result.q(idx,i);
            for k=size(tfr,1)-1:-1:1
                idx = nextIdx;
                nextIdx = result.q(idx,k);
                bestPath(1,k) = result.locs(idx,k);
                bestPath(2,k) = result.pks(idx,k);
            end

            HR_RPPG_filtered = bestPath(1,:);

            if(VERBOSE == 2)
                figure(5);
                hold on
                xlabel('T');
                ylabel('F');
                zlabel('abs(TFR)');
                mesh(time,rPPG_freq,tfrFull),axis xy,colormap('jet'),colorbar, xlabel('Time (s)'), ylabel('Frequency (Hz)'),xlim([time(1) time(end)]), ylim([rPPG_freq(1) rPPG_freq(end)]);
                view(-50,70);
                plot3(time,bestPath(1,:)/60,bestPath(2,:),'r^','MarkerSize',10)
                plot3(time,tfr(:,1,1)/60,tfr(:,1,2),'b*','MarkerSize',10)
                pause()
            end

        case 'Heuristic'
            % Homemade heuristic
            % If diff between actual and predicted peak is large,
            % Choose the 2nd peak (if better)
            crtDelay = 0;
            HR_RPPG_filtered(1)=HR_RPPG(1);
            for i=2:length(HR_RPPG)
                HR_RPPG_filtered(i)=HR_RPPG(i);
                d = abs(HR_RPPG_filtered(i)-HR_RPPG_filtered(i-1));
                if(d > 10)
                    new_HR = tfr(i,2,1); % 2nd peak
                    new_d = abs(new_HR-HR_RPPG_filtered(i-1));
                    if(new_d < d && crtDelay < 15)
                        HR_RPPG_filtered(i) = new_HR;
                        crtDelay = crtDelay + 1;
                    else
                        crtDelay = 0;
                    end
                end
            end
        otherwise
            warning('oops there is something somewhere');
 
    end

    %%%%%%%%%%%%%%%%%%
    % Get metrics and save
    %%%%%%%%%%%%%%%%%
    resultFolder = [VIDFOLDER 'results/'];
    if (exist(resultFolder, 'dir') == 0)
        mkdir(resultFolder);
    end

    % Get metrics
    if(~isempty(gtTime))        
        nb = length(HR_RPPG_filtered);        
        absError = abs(HR_PPG-HR_RPPG_filtered);
        absError_2_5 = absError(absError <= 2.5);
        absError_5 = absError(absError <= 5);
        precision_2_5 = double(length(absError_2_5))/nb;
        precision_5 = double(length(absError_5))/nb;        
        MAE = mean(absError);
        RMSE = sqrt((1/nb)*sum((absError).^2));
        MAE_5 = mean(absError_5);
        meanSNR = mean(snr);

        if (VERBOSE >= 1)
            disp(['MAE : ' num2str(MAE)]);
            disp(['SNR : ' num2str(meanSNR)]);
            disp(' ');
        end

        if(saveResults)
            save([resultFolder 'metrics.mat' ],'precision_2_5','precision_5','MAE','MAE_5','RMSE','meanSNR', 'HR_PPG','HR_RPPG_filtered');
        end
    end

    % save temporal traces
    if(saveResults || VERBOSE>=1)
        fig6 = figure(6);
        clf(fig6);
        hold on;
        grid on;
        p1 = plot(time, HR_sensor, 'b-*');
        p2 = plot(time, HR_PPG, 'g-*');
        p3 = plot(time, HR_RPPG, 'c-*');
        p4 = plot(time, HR_RPPG_filtered, 'r-*');
        legend([p1, p2, p3, p4], 'HR sensor', 'HR PPG', 'HR rPPG', 'HR rPPG filtered');
        title('Results');
        xlabel('seconds'), ylabel('Heart Rate (bpm)');
        ylim([40 210]);
        hold off;

        if(saveResults)
            saveas(gcf,[resultFolder 'temporalHR.png']);
        end
        if(VERBOSE == 0)
            close(fig6);
        else
            pause(1);
        end
    end

    % save Bland Altman Plots
    if(~isempty(gtTime))

        gnames = {'HR analysis'}; % names of groups in data {dimension 1 and 2}
        %corrinfo = {'n','SSE','r2','eq'}; % stats to display of correlation scatter plot
        corrinfo = {'n','r','eq'}; % stats to display of correlation scatter plot
        BAinfo = {'RPC(%)'}; % stats to display onavg Bland-ALtman plot
        limits = 'auto';%[50 120]; % how to set the axes limits
        symbols = 'Num'; % symbols for the data sets (default)
        colors = [.75, .5, .3]; % colors for the data sets
        tit='';
        label = {'EstimatedHR', 'HR'};
        %fig=figure('Name','Bland Altman Analysis');
        [cr, fig, statsStruct] = BlandAltman(HR_RPPG_filtered(1,:),HR_PPG(1,:),label,tit,gnames,corrinfo,BAinfo,limits,colors,symbols);
        %set (fig, 'Units', 'normalized', 'Position', [0,0,1,1]);
        str=['PRECIS 2.5 : ' num2str(precision_2_5) ' | PRECIS 5 : ' num2str(precision_5) ' | MAE : ' num2str(MAE) ];

        a=annotation('textbox', [0.2 0.85 .1 .1], 'FitHeightToText', 'ON', 'Fontsize', 12, ...
            'String',str );
        %Center annotation
        %     set(a,'Units','pixels');
        %     set(fig,'Units','pixels');
        %     aPos=get(a,'Position');
        %     figPos=get(fig,'Position');
        %     set(a,'Position',[(figPos(3)-aPos(3))/2 aPos(2) aPos(3) aPos(4)]);
        %

        resultStruct={round(statsStruct.SSE,4) round(statsStruct.r,4)...
            round(precision_2_5,4) round(precision_5,4)...
            round(MAE,4) round(MAE_5,4) round(RMSE,4) round(statsStruct.CR) ...
            round(meanSNR,4)};
        %excel_row_disp=[statsStruct.SSE statsStruct.r precision_2_5 precision_5 MAE MAE_5 RMSE round(statsStruct.CR) round(meanSNR,4)]
        
         if(saveResults)
             saveas(fig,[resultFolder 'BA.png']);
         end
    end
    
    %close all
    fprintf('\n');
    fprintf('done in %i seconds\n', round(toc));
end

function [freq, power] = getSpectrum(x, Fs, method)

% method can be FFT, Welch, Periodogram, Multitaper
switch method
    case 'FFT'
        N = length(x)*3;
        freq = [0 : N-1]*Fs/N;
        power = abs(fft(x,N)).^2;
        
    case 'Welch'
        [power,freq] = pwelch(x,[],[],length(x)*3,Fs, 'psd'); % zero pading to increase the nb of points in the PSD
        
    case 'Periodogram'
        [power,freq] = periodogram(x,hamming(length(x)),[],Fs,'power');
        
    case 'Multitaper'
        [power,freq] = pmtm(x,[],[],Fs);
        
    otherwise
        warning('oops, there is something somewhere...');
end
end
