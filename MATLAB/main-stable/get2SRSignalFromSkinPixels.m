function get2SRSignalFromSkinPixels(varargin)
% Compute 2SR on a video
% 2SR method: "A Novel Algorithm for Remote Photoplethysmography: Spatial
% Subspace Rotation" Wenjin Wang, Sander Stuijk, and Gerard de Haan 2015
% yannick.benezeth@u-bourgogne.fr
% 08/03/2016

close all;
%     clear all;
%     clc;

outFolder = 'D:\Dataset\HeartRate\IUT_Dataset\subject06\vid2\';
vidFolder = 'D:\Dataset\HeartRate\IUT_Dataset\subject06\vid2\';

matFileName = 'allSkinPixels'; % input file

WINDOW_LENGTH_SEC = 2;
BEGIN_LENGTH_SEC = 0;
FILTER_ORDER = 8;
LOW_F=.7; % low freq for BP filtering
UP_F=3; % high freq for BP filtering
STEP_SEC = 0.5; % step between 2 sliding window position (in seconds)  

i = 1;
while(nargin > i)
    if(strcmp(varargin{i},'vidFolder')==1)
        vidFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'outFolder')==1)
        outFolder=varargin{i+1};
    end
    i = i+2;
end

% Disp working repositories
disp(['Working with  ' vidFolder ', Window Length : ' num2str(WINDOW_LENGTH_SEC)]);
disp(['Output in :  ' outFolder]);


if(exist([vidFolder matFileName '.mat'], 'file') == 2)
    load([vidFolder matFileName '.mat']);
else
    disp('oops, no input file');
    return;
end

% load PPG
[gtTrace, gtHR, gtTime] = loadPPG(vidFolder);
gtTime = gtTime / 1000;  % in seconds

timeTrace = cell2mat(skinPixelsTraces(:,2));
timeTrace = timeTrace/1000; % in second

% get exact Fs
Fs = 1/mean(diff(timeTrace));

% nb of frames
nbFrame = length(timeTrace);

%Window size (number of frames)
l = round(WINDOW_LENGTH_SEC*Fs);

if(BEGIN_LENGTH_SEC == 0)
    beg = 1;
else
    beg = round(BEGIN_LENGTH_SEC*Fs);
end


%Initialization
P = zeros(1,nbFrame);
array_lambda = zeros(3,3,nbFrame);
array_eigenvectors = zeros(3,3,nbFrame);

for k=beg:nbFrame
    if (mod(k,50) == 0)
        fprintf('%i sur %i\n', k, nbFrame);
    end
    
    Vk =cell2mat(skinPixelsTraces(k,1));
    N = length(Vk);
    Ck = (Vk'*Vk)/N;
    [Uk,Ak] = eigs(Ck);
    
    % Sort eigenvalues
    [~,permutation]=sort(diag(Ak),'descend');
    Ak = Ak(permutation,permutation);
    Uk = Uk(:,permutation);
    
    array_lambda(:,:,k) = Ak;
    array_eigenvectors(:,:,k) = Uk;
    
    %     disp('Uk :');
    %     disp(Uk);
    %     disp(array_eigenvectors(:,:,k));
    %     disp('Ak :');
    %     disp(Ak);
    %     disp(array_lambda(:,:,k));
    %     disp('-----------');
    
    tho = k-l+1;
    
    %         if(tho > 0)
    if(tho > beg)
        array_SR = zeros(k+1-tho,3);
        iSR = 1;
        
        for t=tho:k
            first_part = sqrt(array_lambda(1,1,t)/array_lambda(2,2,tho)) * array_eigenvectors(:,1,t)' * array_eigenvectors(:,2,tho) * array_eigenvectors(:,2,tho)';
            second_part = sqrt(array_lambda(1,1,t)/array_lambda(3,3,tho)) * array_eigenvectors(:,1,t)' * array_eigenvectors(:,3,tho) * array_eigenvectors(:,3,tho)';
            
            array_SR(iSR,:) = first_part + second_part;
            
            iSR = iSR + 1;
            %             disp(iSR);
            %             disp(first_part);
            %             disp('---');
            %             disp(second_part);
            %             disp('***************************');
        end
        
        p = array_SR(:,1) - ((std(array_SR(:,1))/std(array_SR(:,2))) * array_SR(:,2));
        
        p = p';
        P(tho:k) = P(tho:k) + (p - mean(p));
        
    end
end
%     plot(timeTrace,P);

pulseTrace = P';

% figure(1);
% plot(timeTrace,pulseTrace);
% disp(size(timeTrace));
% disp(size(pulseTrace));
% 
% figure(2);
% plot(timeTrace,pulseTraceFiltered);

% for fair comparison - do sliding window BP filtering
 
% get window length in frames  
winLength = round(WINDOW_LENGTH_SEC*Fs);  
step = round(STEP_SEC*Fs);  
halfWin = (winLength/2);  
[b, a]= butter(FILTER_ORDER,[LOW_F UP_F]/(Fs/2));  

pulseTrace2SR=zeros(nbFrame,1);  

for i=halfWin:step:nbFrame-halfWin  
          
        % get start/end index of current window  
        startInd = i-halfWin+1;  
        endInd = i+halfWin;  
          
        % get current trace window  
        crtTraceWin = pulseTrace(startInd:endInd);  
          
        % band pass filter  
        crtTraceWinFilt = filter(b,a,crtTraceWin')';  
          
        % Overlap add
        pulseTrace2SR(startInd:endInd)=pulseTrace2SR(startInd:endInd)+crtTraceWinFilt;  

end  
 
% scale amplitudes  
pulseTrace = pulseTrace2SR/std(pulseTrace2SR); 

save([outFolder 'pulseTrace2SR_skin.mat'], 'pulseTrace', 'timeTrace');
end
