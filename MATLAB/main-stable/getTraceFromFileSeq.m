function getTraceFromFileSeq(varargin)
% extract RGB traces from image sequences
% face detection, tracking, skin detection, and spatial average
% contact: yannick.benezeth@u-bourgogne.fr
% first version: 17/02/2016
tic
close all;
clc;

addpath('../tools/skindetector');

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
VERBOSE = 1; % 0 -> no plot, 1 -> some intermediate plots, 2 -> debug mode
VIDFOLDER='D:\Dataset\HeartRate\RichardSummerVideos\5-gt\';

i = 1;
while(nargin > i)
    if(strcmp(varargin{i},'VIDFOLDER')==1)
        VIDFOLDER=varargin{i+1};
    end
   if(strcmp(varargin{i},'VERBOSE')==1)
        VERBOSE=varargin{i+1};
    end
    i = i+2;
end

outFolder = VIDFOLDER;
isBmp=0; % 0 is ppm and 1 is bmp
useFull=0; % 0 or 1 - use full frame (no detection, tracking, etc...)
faceCrop = 0; % 0 or 1 - use 60% of the detected face
skinDetection = 1; % 0 or 1
skinThresh = -2; % threshold for skin detection (default: 0 - the lower the more skin we detect)
forceManualDetection = 0;
saveSkinPixels = 0; % for 2SR (quite slow...)

fprintf('Get RGB traces from image seq folder: %s \n',VIDFOLDER);
fprintf('useFull=%i \n',useFull);
fprintf('faceCrop=%i \n',faceCrop);
fprintf('skinDetection=%i \n',skinDetection);
fprintf('skinThresh=%i\n',skinThresh);
fprintf('saveSkinPixels=%i \n',saveSkinPixels);
fprintf('VERBOSE=%i \n',VERBOSE);
fprintf('forceManualDetection=%i \n',forceManualDetection);
fprintf('Processing...');

%Default filename for the full face without skin or crop
outFileName ='rgbTraces';

% Create a cascade detector object.
faceDetector = vision.CascadeObjectDetector('ClassificationModel','FrontalFaceCART','MinSize',[100 100]);
%faceDetector = vision.CascadeObjectDetector('ClassificationModel','ProfileFace','MinSize',[100 100]);
% Create the point tracker object.
pointTracker = vision.PointTracker('MaxBidirectionalError', 5);

% nb of frames
if isBmp
    nbFiles=length(dir([VIDFOLDER 'img/*.bmp']));
else
    nbFiles=length(dir([VIDFOLDER 'img/*.ppm']));
end

nChannels=3;
rgbTraces = zeros(1+nChannels, nbFiles-1); % why minus 1 ?

numPts = 0;
manualbbox=[];
for n=1:nbFiles
    % read frame by frame number (maybe not ordered)
    if isBmp
        imgName = dir([VIDFOLDER '/img/frame_' num2str(n-1) '_*.bmp']);
    else
        imgName = dir([VIDFOLDER '/img/frame_' num2str(n-1) '_*.ppm']);
    end
    if(size(imgName,1)==0)
        fprintf('oops, no image file \n');
        continue;
    end
    imgName = imgName.name;
    img = imread([VIDFOLDER '/img/' imgName]);
    
    img_copy = img;
    
    if ~ useFull
        % face localisation by detection then tracking
        if numPts < 10
            % Detection mode.
            if(~forceManualDetection)
                bbox  = step(faceDetector, img);
            else
                bbox = [];
            end
            
            if isempty(bbox)
                if ~isempty(manualbbox)
                    bbox=manualbbox;
                end
            end
            
            if (numel(bbox)~=0)
                % Find corner points inside the detected region.
                points = detectMinEigenFeatures(rgb2gray(img), 'ROI', bbox(1, :));
                
                % Re-initialize the point tracker.
                xyPoints = points.Location;
                numPts = size(xyPoints,1);
                release(pointTracker);
                initialize(pointTracker, xyPoints, img);
                
                % Save a copy of the points.
                oldPoints = xyPoints;
                
                % Convert the rectangle represented as [x, y, w, h] into an
                % M-by-2 matrix of [x,y] coordinates of the four corners. This
                % is needed to be able to transform the bounding box to display
                % the orientation of the face.
                x = bbox(1, 1);
                y = bbox(1, 2);
                w = bbox(1, 3);
                h = bbox(1, 4);
                bboxPoints = [x,y;x,y+h;x+w,y+h;x+w,y];
                
                % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
                % format required by insertShape.
                bboxPolygon = reshape(bboxPoints', 1, []);
                
                % Display a bounding box around the detected face.
                img = insertShape(img, 'Polygon', bboxPolygon);
                
                % Display detected corners.
                img = insertMarker(img, xyPoints, '+', 'Color', 'white');
            else
                %Face wasn't detected, manual roi mode
                imshow(img);
                title('Face detector did not detect a face. Please select one manually');
                
                rect= ginput(2);
                rect=round(rect);
                rect(2,:)=rect(2,:)-rect(1,:);
                manualbbox=reshape(rect',[1 4]);
            end
        else
            % Tracking mode.
            [xyPoints, isFound] = step(pointTracker, img);
            %fprintf('%i %f\n', n, xyPoints(1))
            visiblePoints = xyPoints(isFound, :);
            oldInliers = oldPoints(isFound, :);
            
            numPts = size(visiblePoints, 1);
            %fprintf('%i %f\n', n, numPts)
            
            if numPts >= 10
                % Estimate the geometric transformation between the old points
                % and the new points.
                [xform, oldInliers, visiblePoints] = estimateGeometricTransform(...
                    oldInliers, visiblePoints, 'similarity', 'MaxDistance', 4);
                
                % Apply the transformation to the bounding box. [xi yi], 4x2
                bboxPoints = transformPointsForward(xform, bboxPoints);
                Min=min(bboxPoints);
                Max=max(bboxPoints);
                
                bbox=[Min(1) Min(2) Max(1)-Min(1) Max(2)-Min(2)];
                % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
                % format required by insertShape.
                bboxPolygon = reshape(bboxPoints', 1, []);
                
                %Save to bbox [x1 y1 w h]
                % Display a bounding box around the face being tracked.
                img = insertShape(img, 'Polygon', bboxPolygon);
                
                % Display tracked points.
                img = insertMarker(img, visiblePoints, '+', 'Color', 'white');
                
                % Reset the points.
                oldPoints = visiblePoints;
                setPoints(pointTracker, oldPoints);
            end
        end
        
        % once we have the face location -> we extract the RGB value
        if ~isempty(bbox)
            
            if(VERBOSE == 2)
                figure(1), imshow(img), title('Detected Face 1');
            end
            
             if(faceCrop)
                % crop the face
                % horizontal cropping
                scaleW = 0.6;
                bbox(1,3) = bbox(1,3)*scaleW;
                bbox(1,1) = bbox(1,1) + bbox(1,3)*scaleW/2;
                % vertical cropping
                scaleV = 0.6;
                bbox(1,4) = bbox(1,4)*scaleV;
                bbox(1,2) = bbox(1,2) + bbox(1,4)*scaleV/2;
            end;
            
            imgROI = imcrop(img_copy,bbox(1,:));
            if(VERBOSE == 2)
                figure(2), imshow(imgROI), title('Detected Face 2');
            end
            
            if (skinDetection)
                % average RGB value of skin pixels
                imgD = double(imgROI);
                skinprob = computeSkinProbability(imgD);
                mask = skinprob>skinThresh;
                %mask = imfill(mask, 'holes');
                maskRGB = zeros(size(skinprob,1),size(skinprob,2),3);
                maskRGB(:,:,1) = mask;
                maskRGB(:,:,2) = mask;
                maskRGB(:,:,3) = mask;
                imgD = imgD.*maskRGB;
                skinPixels = imgD; %for 2SR
                imgROI(imgD==0)=nan;
                
                imgROI_nan = double(imgROI);
                imgROI_nan(imgD==0)=nan;
                
                [r,c,w]=size(imgROI_nan);
                imgROI_nan_reshaped = reshape(imgROI_nan(:),r*c,[]);
                
                avg = nanmean(imgROI_nan_reshaped);
            else
                avg = nanmean(nanmean(imgROI));
                % for 2SR copy imgROI to skinPixels
                skinPixels = imgROI;
            end
            
            % save skin pixels for 2SR
            skinPixels = double(skinPixels);
            [r,c,w]=size(skinPixels);
            skinPixels = reshape(skinPixels(:),r*c,[]);
            % Remove the 0s
            skinPixels(all(skinPixels==0,2),:)=[];
            % end of save skin pixels for 2SR
            
            
            if(VERBOSE == 2)
                imshow(imgROI), title('Detected Face + skin');
                pause();
            elseif ((mod(n,50) == 0)||(n==2))
                if (VERBOSE == 1)
                    figure(1), imshow(imgROI), title('Detected Face + skin');
                    pause(.01);
                end
            end
        end
    else
        % USEFULL case
        avg=mean(mean(img));    
        if (saveSkinPixels)
             % save skin pixels for 2SR
            skinPixels = double(img);
            [r,c,w]=size(skinPixels);
            skinPixels = reshape(skinPixels(:),r*c,[]);
            % Remove the 0s
            skinPixels(all(skinPixels==0,2),:)=[];
            % end of save skin pixels for 2SR
        end
    end
    
    % get timestamps from name
    [startIndex,endIndex] = regexp(imgName,'\_\d*\.');
    time = imgName(startIndex+1:endIndex-1);
    rgbTraces(1:3,n) = avg;
    rgbTraces(4,n) =  str2double(time);   
    
    if (saveSkinPixels)
        skinPixelsTraces{n,1,:} = skinPixels ; % save all pixels (slow)
        skinPixelsTraces{n,2,:} =  str2double(time) ; % in case save all pixels (slow)
    end
    
    % verbose stuff
    if (mod(n,50) == 0)
        if (VERBOSE >= 1)
            fprintf('\n %i sur %i frames', n,nbFiles);
        else
            fprintf('.');
        end
    end
end
save([outFolder '/' outFileName '.mat'], 'rgbTraces');

if(saveSkinPixels)
    save([VIDFOLDER 'allSkinPixels.mat'], 'skinPixelsTraces', '-v7.3');
end


close all
fprintf('\n');
fprintf('done in %i seconds\n', round(toc));
end

 
            
            
            
        