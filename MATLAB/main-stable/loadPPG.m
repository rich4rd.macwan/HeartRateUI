function [gtTrace, gtHR, gtTime] = loadPPG(vidFolder)
% load PPG recorded with pulse oximeter

gtHR = [];
gtTrace = [];
gtTime = [];

gtfilename=[vidFolder 'gtdump.xmp'];
skipgt=0;
if ~skipgt && exist(gtfilename)==2
    gtdata=csvread(gtfilename);
%     gtTrace=gtdata(:,2);%4
%     gtTime=gtdata(:,1);
%     gtHR = gtdata(:,3);%2
        gtTrace=gtdata(:,4);
        gtTime=gtdata(:,1);
        gtHR = gtdata(:,2);
    
    
    % normalize data (zero mean and unit variance)
    gtTrace = gtTrace - mean(gtTrace,1);
    gtTrace = gtTrace / std(gtTrace);
    
else
    fprintf('oops, no PPG file...\n');
end

