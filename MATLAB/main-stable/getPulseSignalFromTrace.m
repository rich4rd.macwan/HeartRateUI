function getPulseSignalFromTrace(varargin)
% Get pulse trace from RGB traces
% Optional sliding window, detrend, filtering and several methods
% Green method: "Remote plethysmo- graphic imaging..." Verkruysse et al. 2008
% Chrom method: "Robust Pulse Rate From Chrominance-Based rPPG" Haan et al. 2013
% ICA: MacDuff et al. "ďAdvancements in noncontact..." 2011
% PCA: Lewandowska et al. "Measuring pulse rate with a webcam ..." 2011
% contact: yannick.benezeth@u-bourgogne.fr
% first version:  17/02/2016

tic
close all;
clc;

addpath('../tools');
addpath('../tools/PCA');
addpath('../tools/FastICA');

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
VERBOSE = 1; % 0 -> no plot, 1 -> some intermediate plots, 2 -> debug mode
METHOD='ICA'; % can be: Green, Chrom, PCA, ICA
VIDFOLDER='D:\Dataset\HeartRate\RichardSummerVideos\5-gt\';

i = 1;
while(nargin > i)
    if(strcmp(varargin{i},'VIDFOLDER')==1)
        VIDFOLDER=varargin{i+1};
    end
    if(strcmp(varargin{i},'VERBOSE')==1)
        VERBOSE=varargin{i+1};
    end
    if(strcmp(varargin{i},'METHOD')==1)
        METHOD=varargin{i+1};
    end
    
    i = i+2;
end
outFolder = VIDFOLDER;

matFileName = 'rgbTraces'; % input file
winLengthSec = 20; % length of the sliding window (in seconds) (use -1 to work on the whole signal at once)
stepSec = 0.5; % step between 2 sliding window position (in seconds)
lowF=.7; % low freq for BP filtering
upF=3.5; % high freq for BP filtering
filtOrder = 8; % Order of the butterworth BP filter
saveResults = 1;
doFilter=1;
OverValue=1e-4;  maxIter = 10; % For ICA

fprintf('Get pulse from RGB trace: %s%s.mat \n',VIDFOLDER, matFileName);
fprintf('METHOD=%s \n',METHOD);
fprintf('winLengthSec=%i sec\n',winLengthSec);
fprintf('stepSec=%.2f \n',stepSec);
fprintf('lowF=%.2f and upF=%.2f \n',lowF, upF);
fprintf('filtOrder=%i \n',filtOrder);
fprintf('saveResults=%i \n',saveResults);
fprintf('VERBOSE=%i \n',VERBOSE);
fprintf('Processing...');

% load PPG
[gtTrace, gtHR, gtTime] = loadPPG(VIDFOLDER);
gtTime = gtTime / 1000;  % in seconds

% Uniform sampling
% gtTimeUnif = linspace(gtTime(1), gtTime(end), length(gtTrace));
% gtTrace = interp1(gtTime,gtTrace, gtTimeUnif);
% gtTime = gtTimeUnif;

% load RGB traces
if(exist([VIDFOLDER matFileName '.mat'], 'file') == 2)
    load([VIDFOLDER matFileName '.mat']);
else
    disp('oops, no input file');
    return;
end

% remove 0s if any (happens for example on CHU data)
nbChannel=3;
rgbTraces(:,rgbTraces(nbChannel+1,:)<=0)=[];

% get current traces
timeTrace = rgbTraces(end,:);
timeTrace = timeTrace/1000; % in second
crtTrace = rgbTraces(1:nbChannel,:);
traceSize = size(crtTrace,2);

% Uniform sampling
% timeTraceUnif = linspace(timeTrace(1), timeTrace(end), traceSize);
% for k=1:nbChannel
%     crtTrace(k,:) = interp1(timeTrace,crtTrace(k,:), timeTraceUnif);    
% end
% timeTrace = timeTraceUnif;

if (VERBOSE == 2)
    figure();
    hold on
    plot(timeTrace,crtTrace(1,:),'Color',[.8,.1,.3]);
    plot(timeTrace,crtTrace(2,:),'Color',[.1,.8,.3]);
    plot(timeTrace,crtTrace(3,:),'Color',[.3,.1,.8]);
    title('Full RGB traces');
end

% get exact Fs
Fs = 1/mean(diff(timeTrace));

% get window length in frames
winLength = round(winLengthSec*Fs);
step = round(stepSec*Fs);

% get coef of butterworth filter
[b, a]= butter(filtOrder/2,[lowF upF]/(Fs/2)); % filtOrder/2 because of filtfilt

if(winLengthSec == -1)
    % detrend and filter the whole signal (no sliding window - in case we can work on the whole signal)
    for k=1:nbChannel
        crtTrace(k,:) = detrendsignal(crtTrace(k,:)')';
    end
    
    if (VERBOSE == 2)
        figure();
        hold on
        p1 = plot(gtTime, gtTrace/max(gtTrace), 'k');
        p2 = plot(timeTrace,-crtTrace(2,:)/max(crtTrace(2,:)),'b', 'Linewidth', 1.5);
        legend([p1,p2],'PPG', 'remote');
        xlim([5, 25]);
        title('Detrended Green traces');
    end
    
    if doFilter
        % filter the whole signal (in case we can work on the whole signal)
        for k=1:nbChannel
            crtTrace(k,:) = filter(b,a,crtTrace(k,:)')';
        end
    end
    
    if (VERBOSE == 2)
        figure();
        hold on
        p1 = plot(gtTime, gtTrace/max(gtTrace), 'k');
        p2 = plot(timeTrace,-crtTrace(2,:)/max(crtTrace(2,:)),'b', 'Linewidth', 1.5);
        legend([p1,p2],'PPG', 'remote');
        xlim([5, 25]);
        title('Filtered Green traces');
    end
    
    switch METHOD
        case 'Green'
            % Method 1: Green channel
            pulseTrace = crtTrace(2,:);
            
        case 'Chrom'
            % Method 2: CHROM
            Rf = crtTrace(1,:);
            Gf = crtTrace(2,:);
            Bf = crtTrace(3,:);
            Xf = 3*Rf-2*Gf;
            Yf = 1.5*Rf+Gf-1.5*Bf;
            alpha = std(Xf)/std(Yf);
            pulseTrace = Xf - alpha*Yf;
                        
        case 'PCA'
             % Method 3 : PCA
             RGB_input = [ crtTrace(1,:); crtTrace(2,:); crtTrace(3,:)];
             tmp= PCA(RGB_input);
             pulseTrace = getMostPeriodicSignal(tmp, Fs);
             
       case 'ICA'
             % Method 4 : ICA
             RGB_input = [ crtTrace(1,:); crtTrace(2,:); crtTrace(3,:)];
             % [Sig,W,initialA,crtPulseICA,selectedIndex]=simpleICA2(RGB_input,maxIter,OverValue,'choosebestcomponent',true,'func','negentropy','timeTrace',timeTrace);
             tmp = fastica(RGB_input,  'verbose', 'off', 'g', 'tanh');
             pulseTrace = getMostPeriodicSignal(tmp, Fs); 
             
        otherwise
            warning('invalid method name');
    end    
else

    ind=1;
    if ~strcmp(METHOD, 'ICA') % for ICA, no overlap-add
        pulseTrace=zeros(1,traceSize);
    end
    halfWin = (winLength/2);

    for i=halfWin:step:traceSize-halfWin
        
        % get start/end index of current window
        startInd = i-halfWin+1;
        endInd = i+halfWin;
        
        % get current trace window
        crtTraceWin = crtTrace(1:nbChannel,startInd:endInd);
        crtTimeWin = timeTrace(startInd:endInd);
        
        if(VERBOSE == 2)
            fig1 = figure(1);
            clf(fig1);
            plot(crtTimeWin, crtTraceWin(2,:)),title('raw green trace'), xlabel('seconds'), ylabel('pixel values');
        end
        
        % normalize
        crtTraceWin = crtTraceWin ./ repmat(mean(crtTraceWin,2),[1 winLength]) -1;
        % crtTraceWin = crtTraceWin - repmat(mean(crtTraceWin,2),[1 winLength]);
        % crtTraceWin = crtTraceWin ./ repmat(std(crtTraceWin,0,2),[1 winLength]);
                
        if(VERBOSE == 3)
            fig2 = figure(2);
            clf(fig2);
            hold on;
            plot(crtTimeWin, crtTraceWin(1,:),'r');
            plot(crtTimeWin, crtTraceWin(2,:),'g');
            plot(crtTimeWin, crtTraceWin(3,:),'b');
            title('normalized traces'), xlabel('seconds'), ylabel('normalized pixel values');
            hold off;
        end
        
        % detrend
        for k=1:size(crtTraceWin,1)
            crtTraceWin(k,:) = detrendsignal(crtTraceWin(k,:)')';
        end     
        
        if(VERBOSE == 2)
           fig3 = figure(3);
           clf(fig3);
           plot(crtTimeWin, crtTraceWin(2,:)), title('detrended green trace');
        end
        
         if doFilter
            % band pass filter
            for k=1:size(crtTraceWin,1)
                crtTraceWin(k,:) = filtfilt(b,a,crtTraceWin(k,:)')'; % filtfilt for zero-phase filtering
            end
         end
         
         if(VERBOSE == 2)
           fig4 = figure(4);
           clf(fig4);
           plot(crtTimeWin, crtTraceWin(2,:)), title('detrended and filtered green trace');
           pause(0.05);
         end
        
        switch METHOD
            case 'Green'
                % Method 1: Green channel
                crtPulse = crtTraceWin(2,:);
                
            case 'Chrom'
                % Method 2: CHROM
                Rf = crtTraceWin(1,:);
                Gf = crtTraceWin(2,:);
                Bf = crtTraceWin(3,:);
                Xf = 3*Rf-2*Gf;
                Yf = 1.5*Rf+Gf-1.5*Bf;
                
                alpha = std(Xf)/std(Yf);
                crtPulse = Xf - alpha*Yf;  
                crtPulse = -crtPulse;

            case 'PCA'
                 % Method 3 : PCA
                 RGB_input = [crtTraceWin(1,:);crtTraceWin(2,:);crtTraceWin(3,:)];
                 tmp = PCA(RGB_input)';
                 crtPulse = getMostPeriodicSignal(tmp, Fs);        
                 
            case 'ICA'
             % Method 4 : ICA
             RGB_input = [ crtTraceWin(1,:); crtTraceWin(2,:); crtTraceWin(3,:)];
             % [Sig,W,initialA,crtPulseICA,selectedIndex]=simpleICA2(RGB_input,maxIter,OverValue,'choosebestcomponent',true,'func','negentropy','timeTrace',timeTrace);
             tmp = fastica(RGB_input,  'verbose', 'off', 'g', 'tanh');
             crtPulse = getMostPeriodicSignal(tmp, Fs); 
             

            otherwise
                warning('invalid method name');
        end
       
        % compare PPG with pulse traces
        if(VERBOSE == 2)
            fig5 = figure(5);
            clf(fig5), hold on, title('PPG vs rPPG (not final)'), xlim([timeTrace(startInd), timeTrace(endInd)]);
            p1 = plot(gtTime, gtTrace/max(gtTrace), 'b');
            p2 = plot(crtTimeWin, crtPulse/max(crtPulse), 'r');
            legend([p1,p2],'PPG', 'rPPG');
            hold off;
        end
        
        % Overlap add + smooth        
        crtPulse = smooth(crtPulse)';

        if strcmp(METHOD, 'ICA')
            pulseTrace(ind,:)=crtPulse/std(crtPulse);
        else
            pulseTrace(startInd:endInd)=pulseTrace(startInd:endInd)+crtPulse;
        end

        if((VERBOSE == 2) && (~strcmp(METHOD, 'ICA')))
            fig6 = figure(6);
            clf(fig6), hold on, title('PPG vs rPPG - final'), xlim([timeTrace(startInd), timeTrace(endInd)]);
            p1 = plot(gtTime, gtTrace/max(gtTrace), 'k');
            p2 = plot(timeTrace(1:endInd), pulseTrace(1:endInd)/max(pulseTrace(1:endInd)), 'b', 'Linewidth', 1.5);
            legend([p1,p2],'PPG', METHOD);
            hold off;
            pause(0.05);
        end
        ind=ind+1;
    end
end

% scale amplitudes
if (~strcmp(METHOD, 'ICA'))
    pulseTrace = pulseTrace/std(pulseTrace);
end

% plot final traces
if((VERBOSE >= 1) && (~strcmp(METHOD, 'ICA')))
    figure(7),hold on;  
    p1 = plot(gtTime, gtTrace, 'k');  
    p2 = plot(timeTrace, pulseTrace, 'b', 'Linewidth', 1.5);  
    xlim([5 25]),  
    title('Final rPPG trace');
    legend([p1 p2], 'PPG', 'rPPG');  
end;

if(saveResults)
    % save to mat files
    save([outFolder 'pulseTrace.mat'], 'pulseTrace', 'timeTrace');
end;

%close all
fprintf('\n');
fprintf('done in %i seconds\n', round(toc));
