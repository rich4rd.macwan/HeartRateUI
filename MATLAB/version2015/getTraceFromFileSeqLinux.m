function getTraceFromFileSeqLinux(varargin)

close all;
clc;
vidFolder = '/run/media/richard/Le2iHDD/IUT/subject012.vid';
subjectnum=-1;
if(nargin==1)
    if(isnumeric(varargin(1)))
        subjectnum=varargin(1);
    else
        vidFolder=varargin(1);
    end
end

addpath('./tools/skindetector');
%mode can be 0 or 1. 0 for ppm image mode, 1 for custom file mode
DO_FACECROP = 1;
DO_SKINDETECTION = 0;
%vidFolder = 'E:\RPPGVideos\videoTest\4';

%vidFolder = '/home/richard/rppgVideos/CHU/patient4-normal3/';
%Custom file mode
%subjectnum=12;
if(subjectnum>0)
    if(subjectnum<10)
        prefix='0';
    else
        prefix=''
    end
end
% vidFolder=['/run/media/richard/Newer/RPPGData/subject' prefix  num2str(subjectnum) '2.vid'];
%vidFolder=['/run/media/richard/544473F41F2B707A/rppgVideos-1/subject' prefix  num2str(subjectnum) '2.vid'];
fileid=[];
nRows=0;
nCols=0;
nChannels=0;


disp(['Using source: ' vidFolder]);
%keyin=input('Is this correct? ','s');
keyin='y';
if(keyin=='y' || keyin =='Y')
    disp('');
else
    return;
end
if DO_FACECROP==1
    outFileName = 'rgbTraces_crop';
elseif DO_SKINDETECTION==1
    outFileName = 'rgbTraces_skin';
else
    outFileName = 'rgbTraces';
end

% Create a cascade detector object.
%faceDetector = vision.CascadeObjectDetector('ClassificationModel','ProfileFace','MinSize',[200 200]);
faceDetector = vision.CascadeObjectDetector('ClassificationModel','FrontalFaceCART','MinSize',[100 100]);
%faceDetector = vision.CascadeObjectDetector();

% Create the point tracker object.
pointTracker = vision.PointTracker('MaxBidirectionalError', 5);

% nb of frames
%nbFiles=length(dir([vidFolder '\img\*.ppm']));
if(strcmp(vidFolder(end-2:end),'vid')~=1)
    ppmMode=true;
    imgpath=[vidFolder '/img/*.ppm'];
    %Hack to get the first index of the frame number since the frame numbers dont start at 1
    nbFiles=length(dir(imgpath));
    nChannels=3;
else
    ppmMode=false;
    %Get number of frames from the file
    fileid=fopen(vidFolder,'rb');
    %Read file header
    A=fread(fileid,[1 5],'uint32');
    %The second value is the number of frames. A(1) should be 43200 which
    %we are not checking right now.
    nbFiles=A(2);
    nRows=A(3);
    nCols=A(4);
    nChannels=A(5);
end
% imgNames=dir(imgpath);
% imgNames={imgNames.name}';
% expr1='_[0-9]*_';
% expr2='_[0-9]*\.';
% minIndex=1000000;
% maxIndex=0;
% for i=1:numel(imgNames)
%     nm=imgNames{i};
%     si=regexp(nm,expr1);
%     ei=regexp(nm,expr2);
%     index=str2num(nm(si+1:ei-1));
%     if(index>maxIndex)
%         maxIndex=index;
%     end
%     if(index<minIndex)
%         minIndex=index;
%     end
% end
% %minIndex contains the first frame number
% [minIndex maxIndex]

rgbTraces = zeros(1+nChannels, nbFiles-1);
gtdata = zeros(3,nbFiles-1);
numPts = 0;
hold on;
manualbbox=[];
list=dir([vidFolder '/img/*.ppm']);

for n=1:nbFiles
    %Hack for disturbance
%     if(n>2120 && n<2140)
%           avg=zeros(3,1);
%           continue;
%     end
%     if n==2140
%         bbox=[];
%         numPts=0;
%     end
    % read frame by frame number (maybe not ordered)
    if(ppmMode)
       imgName = dir([vidFolder '/img/frame_' num2str(n-1) '_*.ppm']);
        %imgName=list(n);
        if(size(imgName,1)==0)
            continue;
        end
        imgName = imgName.name;
        img = imread([vidFolder '/img/' imgName]);
    else
        %Read next frame from custom file
        %First read frame header
        frameHeader=fread(fileid,[1 3],'uint32');
        %Then read frame data
        img=fread(fileid,[nCols*nChannels nRows] ,'*uchar');
        img=cat(3,img(3:3:end,:)',img(2:3:end,:)',img(1:3:end,:)');
    end
    img_copy = img;
   
    if numPts < 10
        % Detection mode.
        bbox  = step(faceDetector, img);
        if numel(bbox)==0
            if ~isempty(manualbbox)
                bbox=manualbbox;
            end
        end
     
        if numel(bbox)~=0
            % Find corner points inside the detected region.
            points = detectMinEigenFeatures(rgb2gray(img), 'ROI', bbox(1, :));
            
            % Re-initialize the point tracker.
            xyPoints = points.Location;
            numPts = size(xyPoints,1);
            release(pointTracker);
            initialize(pointTracker, xyPoints, img);
            
            % Save a copy of the points.
            oldPoints = xyPoints;
            
            % Convert the rectangle represented as [x, y, w, h] into an
            % M-by-2 matrix of [x,y] coordinates of the four corners. This
            % is needed to be able to transform the bounding box to display
            % the orientation of the face.
            x = bbox(1, 1);
            y = bbox(1, 2);
            w = bbox(1, 3);
            h = bbox(1, 4);
            bboxPoints = [x,y;x,y+h;x+w,y+h;x+w,y];
            
            % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
            % format required by insertShape.
            bboxPolygon = reshape(bboxPoints', 1, []);
            
            % Display a bounding box around the detected face.
            img = insertShape(img, 'Polygon', bboxPolygon);
            
            % Display detected corners.
            img = insertMarker(img, xyPoints, '+', 'Color', 'white');
        else
            %Face wasn't detected, manual roi mode
            imshow(img);
            title('Face detector did not detect a face. Please select one manually');
            
            rect= ginput(2);
            rect=round(rect);
            rect(2,:)=rect(2,:)-rect(1,:);
            manualbbox=reshape(rect',[1 4]);
            
            
        end
        
    else
        % Tracking mode.
        [xyPoints, isFound] = step(pointTracker, img);
        visiblePoints = xyPoints(isFound, :);
        oldInliers = oldPoints(isFound, :);
        
        numPts = size(visiblePoints, 1);
        
        if numPts >= 10
            % Estimate the geometric transformation between the old points
            % and the new points.
            [xform, oldInliers, visiblePoints] = estimateGeometricTransform(...
                oldInliers, visiblePoints, 'similarity', 'MaxDistance', 4);
            
            % Apply the transformation to the bounding box.
            bboxPoints = transformPointsForward(xform, bboxPoints);
            
            % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
            % format required by insertShape.
            bboxPolygon = reshape(bboxPoints', 1, []);
            
            % Display a bounding box around the face being tracked.
            img = insertShape(img, 'Polygon', bboxPolygon);
            
            % Display tracked points.
            img = insertMarker(img, visiblePoints, '+', 'Color', 'white');
            
            % Reset the points.
            oldPoints = visiblePoints;
            setPoints(pointTracker, oldPoints);
        end
    end
    
  
    
    
    pause(0.00000001)
    if ~isempty(bbox)
        IFaces = insertObjectAnnotation(img, 'rectangle', bbox, 'Face');    
        imshow(IFaces)
        if(DO_FACECROP)
            % crop the face
            % horizontal cropping
            scaleW = 0.6;
            bbox(1,3) = bbox(1,3)*scaleW;
            bbox(1,1) = bbox(1,1) + bbox(1,3)*((1-scaleW)/2);
            % vertical cropping
            scaleV = 0.6;
            bbox(1,4) = bbox(1,4)*scaleV;
            bbox(1,2) = bbox(1,2) + bbox(1,4)*((1-scaleV)/2);
        end;
        
        imgROI = imcrop(img_copy,bbox(1,:));
      %  figure(2), imshow(imgROI), title('Detected Face');
        imgD = double(imgROI);
        if (DO_SKINDETECTION)
            
            % average RGB value of skin pixels
            
            skinprob = computeSkinProbability(imgD);
            mask = skinprob>0;
            maskRGB = zeros(size(skinprob,1),size(skinprob,2),3);
            maskRGB(:,:,1) = mask;
            maskRGB(:,:,2) = mask;
            maskRGB(:,:,3) = mask;
            imgD = imgD.*maskRGB;
            imgROI(imgD==0)=nan;
            
            %figure(3), imshow(imgROI), title('Detected Face + skin');
            
        end;
        
        %avg=nanmean(nanmean(imgROI));
        
        % Is there a better way to get mean of non-zero values ? this is quite slow...
        [~, ~, vals] = find(imgD(:,:,1));
        avg(1) = mean(vals);
        [~, ~, vals] = find(imgD(:,:,2));
        avg(2) = mean(vals);
        [~, ~, vals] = find(imgD(:,:,3));
        avg(3) = mean(vals);
            
        rgbTraces(1:nChannels,n) = avg;
        
        if(ppmMode)
            %get timestamp from name
            [startIndex,endIndex] = regexp(imgName,'\_\d*\.');
            time = imgName(startIndex+1:endIndex-1);
        else
            %Get the timestamp from oximeter data in the file
            %Get number of oximeter rows
            time=fread(fileid,[1 1],'uint64');
            nOxi=fread(fileid,[1 1],'uint32');
            for o=1:nOxi
                oxirow=fread(fileid,[1 4],'uint32');
                oxits=fread(fileid,[1 1],'uint64');
                
            end
        end
        
        if(ppmMode)
            rgbTraces(1+nChannels,n) = str2double(time);
        else
            rgbTraces(1+nChannels,n) = time;
        end
        %figure(1), imshow(img);
        
        
        if (mod(n,10) == 0)
            if(ppmMode)
                fprintf('%i sur %i %s, timestamp %s \n', n, nbFiles, imgName, time);
            else
                fprintf('%i sur %i , timestamp %13i \n', n, nbFiles, time);
            end
        end
%         if (n>4000)
%             break;
%         end
    end
end
if(ppmMode)
    save([vidFolder '/' outFileName '.mat'], 'rgbTraces');
else
    save([vidFolder(1:end-4) '-' outFileName '.mat'], 'rgbTraces');
end

clear all
close all
end
