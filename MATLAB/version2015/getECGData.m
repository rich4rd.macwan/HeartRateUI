function [ ecgdata] = getECGData( vidFolder)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
ecgdata=struct;
tokens=strsplit(vidFolder,'/');
ecgdata.name=tokens(length(tokens));

%Remove last '/' if present
if strcmp(vidFolder(end),'/')==1
    vidFolder(end)=[];
end

if exist([vidFolder '/ecg'])
    disp('ECG data found. Parsing...');
    %Read all the files from the ecg folder
    ecgfiles=dir([vidFolder '/ecg/*.txt']);
    for i=1:numel(ecgfiles)
        %create a structure for each minute. This will contain the ecg
        %waveform for that minute
        ecgdata.minutes(i)=struct;
        minuteitext=fileread([vidFolder '/ecg/' ecgfiles(i).name]);
        
    end
end

end

