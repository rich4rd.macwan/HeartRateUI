function metrics(varargin)
%Metrics evaluation
% Syntax: metrics('load'|'evaluate',options)
% options can be one key value pairs of one or more of the following
%
% 'interpolation' :'spline', 'pchip'(default),'nearest'
%
% 'smoothing' : 'none', 'frequency-bandpass'(default), 'temporal-bandpass'
% 'winLength' : TFR window analysis length in seconds (multiplied with Fs to get number of frames).
% 'tfrgaussianfilter': true(defalt)|false. Use gaussian filter to smooth the tfr
% 'metricsmode': true|false(default). Disable displaying of figures
% e.g.
% metrics('evaluate','interpolation','spline','smoothing','frequency-bandpass','winLength',20,'metricsmode',true);
mode=0;
metric_prefix='/metrics';
metric_suffix='';
extraargs=0;
interpolation='pchip';
winLength=15;
smoothing='frequency-bandpass';
metricsmode=1;
tfrgaussianfilter=1;
for i=1:nargin
    if(strcmp(varargin{i},'load')==1)
        mode=1;
    end
    if(strcmp(varargin{i},'evaluate')==1)
        mode=0;
    end
  
    if(strcmp(varargin{i},'interpolation')==1)
        interpolation=varargin{i+1};
        metric_suffix=[metric_suffix 'interp-' varargin{i+1}];
    end
    if(strcmp(varargin{i},'winLength')==1)
        winLength=varargin{i+1};
        metric_suffix=[metric_suffix 'winLen-' num2str(varargin{i+1})];
    end
    if(strcmp(varargin{i},'smoothing')==1)
        smoothing=varargin{i+1};
        if(strcmp(smoothing,'none')==1)
            metric_suffix=[metric_suffix 'smth-none'];
        end
        if(strcmp(smoothing,'temporal-bandpass')==1)
            metric_suffix=[metric_suffix 'smth-TBP'];
        end
    end
    if(strcmp(varargin{i},'tfrgaussianfilter')==1)
        tfrgaussianfilter=varargin{i+1};
    end
   
end

if(tfrgaussianfilter)
        if(strcmp(metric_suffix,'')==1)
            metric_suffix=[metric_suffix 'gausstfr'];
        else
            metric_suffix=[metric_suffix '-gausstfr'];    
        end
    
end
clc

overwriteAll=0;
if(mode==1)
%Overwrite behaviour is undefined. Let the user choose.
overwriteAll=2;
end

%% Evaluation mode
%We perform the evaluation in this mode.

videoFolder='/run/media/richard/841A-95A7/Le2i/';
dirs=dir([videoFolder '*-gt*']);
overallMetrics=[];
%N rows of the following format
%Mean_HRGT Mean_HRGT_PPG Mean_RPPG  Std_HRGT Std_HRGT_PPG Std_RPPG CorrCoef_HRGT_vs_RPPG CorrCoef_HRGTPPG_vs_RPPG
overallMetrics=zeros(numel(dirs),8);

for i=1:numel(dirs)
    overwrite=false;
    disp(['Evaluating ' videoFolder dirs(i).name]);

    if(strcmp(metric_suffix,'')==1)
        metricsfilename=[videoFolder dirs(i).name metric_prefix '.mat'];
    else
        metricsfilename=[videoFolder dirs(i).name metric_prefix '-' metric_suffix '.mat'];
    end
    
    
    message=sprintf('Previous metrics file found!\n%s%s%s \nOverwrite(y/n),Overwrite All(a),Overwrite none(o)?  \n',...
        [videoFolder dirs(i).name ],metric_prefix,metric_suffix);
    
    if(overwriteAll==0 || overwriteAll==2)
        if(exist(metricsfilename,'file')==2)
            if(overwriteAll==2)
                %Don't overwrite any metrics
                load(metricsfilename);
                overallMetrics(i,:)=[Mean_HRGT Mean_HRGT_PPG Mean_RPPG  Std_HRGT Std_HRGT_PPG Std_RPPG CorrCoef_HRGT_vs_RPPG CorrCoef_HRGTPPG_vs_RPPG];
                continue;
            end
            %Metrics file exists. Confirm with user to run the metrics again
            str=input(message,'s');
            if(~isempty(str) )
                if(str=='y' || str=='Y')
                    overwrite=true;
                end
                if(str=='n' || str=='N')
                    overwrite=false;
                end
                if(str=='a' || str=='A')
                    overwriteAll=1;
                    overwrite=false;
                end
                if(str=='o' || str=='O')
                    overwriteAll=2;
                end
            end
        else
            disp('Calculating metrics...');
            getPulseFromTracesLinux([videoFolder dirs(i).name],varargin(2:end));
        end
        
        if(overwriteAll==2)
            %Don't overwrite any metrics
            continue;
        end
        if(overwrite)
            disp('Calculating metrics...');
            getPulseFromTracesLinux([videoFolder dirs(i).name],varargin(2:end));
        end
    end
    
    if(overwriteAll==1)
        disp('Calculating metrics...');
        getPulseFromTracesLinux([videoFolder dirs(i).name],varargin(2:end));
    end
end

%% Visualization mode
%Display overall analysis
    
    addpath('../BlandAltman/');
    disp(['Bland Altman Analysis']);
    
    tit = ['Average HR vs GT Signal']; % figure title
    label = {'Avg HR', 'Avg HR from GT Signal'};
    gnames = {'Avg HR analysis'}; % names of groups in data {dimension 1 and 2}
    corrinfo = {'n','SSE','r2','eq'}; % stats to display of correlation scatter plot
    BAinfo = {'RPC(%)'}; % stats to display onavg Bland-ALtman plot
    limits = 'auto'; % how to set the axes limits
    
    symbols = ''; % symbols for the data sets (default)
    if 1
        colors = 'brg'; % colors for the data sets
    else
        colors = [0 0 1;...
            1 0 0; 0 1 0];
    end
    
    %Remove corrupt data
    % avgHRs(:,2)=[];
    %avgHRs(:,3)=[];
    % avgHRGTs(:,2)=[];
    %avgHRGTs(:,3)=[];
    
    % Generat figure with symbols
    [cr, fig, statsStruct] = BlandAltman(overallMetrics(:,1), overallMetrics(:,3),label,tit,gnames,corrinfo,BAinfo,limits,colors,symbols);
    
    tit = ['Average HR vs GT from PPG']; % figure title
    label = {'Avg HR', 'Avg HR from GT from PPG'};
    
    [cr, fig, statsStruct] = BlandAltman(overallMetrics(:,2), overallMetrics(:,3),label,tit,gnames,corrinfo,BAinfo,limits,colors,symbols);
    
    pause
    close all

end
