clear all;
close all;
clc;

addpath(genpath('../tools/tftb-0.2'));
addpath(genpath('../tools'));
%% Get traces

vidFolder = '/run/media/richard/Le2iHDD/Le2i/5-gt';
outFileName = 'rgbTraces_crop';
load([vidFolder '/' outFileName '.mat']);

Fs = 30; %new sampling rate
winLength = Fs * 15; % analyzed window length (nb of points)

time = rgbTraces(4,:);
rgbTraces = rgbTraces(1:3,:);

%% Normalize and resample
traceSize = size(rgbTraces,2);

% normalize each trace window to have zero mean and unit variance
rgbTraces = rgbTraces - repmat(mean(rgbTraces,2),[1 traceSize]);
rgbTraces = rgbTraces ./ repmat(std(rgbTraces,0,2),[1 traceSize]);

% resample
time=time-time(1);
inc = 1/Fs * 1000;
endTime = time(length(time));
newTime = 0 : inc : endTime;

for c=1:3
    rgbTracesSmoothed(c,:) = smooth(rgbTraces(c,:), 5, 'moving');
%     figure(),  plot(time/1000,rgbTraces(c,:),'-', time/1000,rgbTracesSmoothed(c,:), '-'), title('raw and smoothed trace');
%     figure(),  plot(time/1000,rgbTraces(c,:),'-'), title('raw trace');
%     figure(),  plot( time/1000,rgbTracesSmoothed(c,:), '-'), title(' smoothed trace');;

    newRgbTraces(c,:) = interp1(time,rgbTracesSmoothed(c,:),newTime, 'pchip')';
    %plot(timeStamps,RGBTraces(1,:),'bo',times,traces(1,:),'ro');  
   % figure(c),  plot(time/1000,rgbTracesSmoothed(c,:),'-x', newTime/1000,newRgbTraces(c,:), ':.');    
end

%% FFT v1
% figure();
% x = newRgbTraces(2,:)';
% N = length(x);
% fax_Hz = 60 * [0 : N-1]*Fs/N;
% spec = abs(fft(x)).^2;
% plot(fax_Hz, spec), xlim(60*[0.6 3.5]), xlabel('Heart Rate (bpm)');

%% FFT v2
% figure();
% x = newRgbTraces(2,:)';
% N = length(x);
% spec=fftshift(abs(fft(x)).^2);
% fax_Hz = 60 * [-N/2 : (N-1)/2] *Fs/N ;
% plot(fax_Hz,spec), xlim(60 * [0.6 3.5]), xlabel('Heart Rate (bpm)');

%% short time Fourrier Transform v1 (manual with fftshift)
% figure();
% timeShift = 35 * Fs;
% x = newRgbTraces(2,:)';
% N = length(x);
% h = hamming(odd(winLength));
% newH = zeros(N,1);
% newH(timeShift : timeShift + length(h)-1) = h; 
% newX = x .* newH;
% spec=fftshift(abs(fft(newX)).^2);
% fax_Hz = 60 * [-N/2 : (N-1)/2] *Fs/N ;
% plot(fax_Hz,spec), xlim(60*[0.6 3.5]), xlabel('Heart Rate (bpm)');

%% short time Fourrier Transform v2 (manual without fftshift)
% figure();
% timeShift = 35 * Fs;
% x = newRgbTraces(2,:)';
% N = length(x);
% h = hamming(odd(winLength));
% newH = zeros(N,1);
% newH(timeShift : timeShift + length(h)-1) = h; 
% newX = x .* newH;
% spec=abs(fft(newX)).^2;
% fax_Hz =  60 * [0 : N-1]*Fs/N ;
% plot(fax_Hz,spec), xlim(60*[0.6 3.5]), xlabel('Heart Rate (bpm)');

%% short time Fourrier Transform v3 (whole signal)
figure();
x = newRgbTraces(2,15*Fs:end-10*Fs)';
% method='cICA';
% WINDOW_LENGTH_SEC=15;
% load([vidFolder '/pulseTrace' method  '-win' num2str(WINDOW_LENGTH_SEC)]);
% x=pulseTrace';
N = length(x);
h = hamming(odd(winLength));
%h = hamming(odd(winLength));
tfr=tfrstft(x, 1:length(x), length(x), h, 1);

tfr2 = tfr(round(0.6*N/Fs):round(3.5*N/Fs),:);
tfr2abs=abs(tfr2);
%imagesc(newTime,f,tfr2abs), axis('xy');
X=1:size(tfr2abs,2);
f = linspace(0.6*60,3.5*60,size(tfr2abs,1));
T=interp1(newTime,newTime,X);
%T=timeTrace;
%mesh(T,f,tfr2abs);
axis tight

%% Dynamic programming
figure(1);
first=50;
last=size(tfr2abs,2);
last=60;
tfr2abs=tfr2abs(:,first:last);
%tfr2abs=normalise(tfr2abs);
mesh(T(first:last),f,tfr2abs);
q=zeros(1,size(tfr2abs,2));
nMaxCandidates=4;
i=size(tfr2abs,2);

rahrt_data_inst=rahrt_data(nMaxCandidates,tfr2abs);

result=rahrt(i,rahrt_data_inst);
figure(1);
hold on
xlabel('T');
ylabel('F');
zlabel('abs(TFR)');
finalpks=result.pks(sub2ind(size(result.locs),result.q,1:result.nFrames));
finallocs=result.locs(sub2ind(size(result.locs),result.q,1:result.nFrames));
plot3([first:last]',finallocs(1,:),finalpks(1,:),'^','Color','Red')
view(-90,60);
result.D
%fax_Hz = 60 * [0 : N-1]*Fs/N ;
%spec = abs(tfr(:,Fs * 42.5)).^2;
%plot(fax_Hz,abs(spec)), xlim(60*[0.6 3.5]), xlabel('Heart Rate (bpm)');
%imagesc(abs(tfr)), axis('xy'), ylim([0.6*N/Fs 3.5*N/Fs])

% figure()
% x = newRgbTraces(2,:)';
% %newTime = 0 : inc : endTime;
% %x_an = newRgbTraces(2,:)' + hilbert(newRgbTraces(2,:)' );
% h = hamming(odd(winLength));
% %x_an = hilbert(newRgbTraces(2,:)' );
% tfr=tfrstft(x, 1:length(newTime), length(newTime), h, 1);
% imagesc(abs(tclose all;fr));
% 
% 
% [tfr,t,f]=tfrwv(x_an);
% %contour(tfr,5); grid
% a = tfr(1:10:end,1:10:end);
% step = 10;
% contour(t(1:step:end),f(1:step:end),tfr(1:step:end,1:step:end));
