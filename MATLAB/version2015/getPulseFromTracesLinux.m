function getPulseFromTracesLinux(varargin)
%Extract pulses from existing traces with the given parameters
% getPulseFromTracesLinux(subjectnum|vidFolder,option)
% option can be any of the following:
%
% interpolation : interpolation-spline, interpolation-pchip(default),
% interpolation-nearest
%
% smoothing : smoothing-none, smoothing-frequency-bandpass,
% smoothing-temporal-bandpass
% winLength : TFR window analysis length
% metricsmode: true|false. Disable displaying of figures
close all;
clc;
subjectnum=1;
vidFolder =  '/run/media/richard/Le2iHDD/IUT';
%vidFolder = '/media/yannick/Data/Dataset/HeartRate/RichardSummerVideos/5-gt';
%vidFolder = '/media/yannick/Data/Recherche/Projects/synced/HeartRate/Code/HeartRateUI/MATLAB/currentMatlabCode/evaluation/5-gt';

metric_suffix='';
%% Global Flags
%Default values
interpolation='pchip'; %Same as cubic
smoothing='frequency-bandpass';
tfrgaussianfilter=1;
metricsmode=false;
Fs = 30; %new sampling rate
winLength = Fs * 20; % analyzed window length (nb of points)
nChannels=3; %No of channels
if(nargin>=1)
    if(isnumeric(varargin(1))==1)
        subjectnum=varargin{1};
    else
        vidFolder=varargin{1};
        
        if(nargin>1)
            extra_args=varargin{2};
            for i=1:numel(extra_args)
                if(strcmp(extra_args{i},'interpolation')==1)
                    interpolation=extra_args{i+1};
                    metric_suffix=[metric_suffix 'interp-' extra_args{i+1}];
                end
                if(strcmp(extra_args{i},'winLength')==1)
                    winLength=Fs*extra_args{i+1};
                    metric_suffix=[metric_suffix 'winLen-' num2str(extra_args{i+1})];
                end
                if(strcmp(extra_args{i},'smoothing')==1)
                    smoothing=extra_args{i+1};
                    if(strcmp(smoothing,'none')==1)
                        metric_suffix=[metric_suffix 'smth-none'];
                    end
                    if(strcmp(smoothing,'temporal-bandpass')==1)
                        metric_suffix=[metric_suffix 'smth-TBP'];
                    end
                end
                if(strcmp(extra_args{i},'metricsmode')==1)
                    metricsmode=extra_args{i+1};
                end
                 if(strcmp(extra_args{i},'tfrgaussianfilter')==1)
                    tfrgaussianfilter=extra_args{i+1};
                    
                end
            end
        end
        
    end
end

if(tfrgaussianfilter)
        if(strcmp(metric_suffix,'')==1)
            metric_suffix=[metric_suffix 'gausstfr'];
        else
            metric_suffix=[metric_suffix '-gausstfr'];    
        end
end
       

addpath(genpath('./tools/tftb-0.2'));

if(strcmp(metric_suffix,'')~=1)
    metric_suffix=['-' metric_suffix];
end


%Skip calculation and display of gt traces
skipgt=1;
%This flag is for tentativeMeanHR vs windowedMeanHR. For calculating the
%tentative mean in the first pass, we will use the global tentative mean if
%this flag is set to 1 and windowed mean if it is set to 0.
globalMean=1;
individualFrameAnalysis=0;
minHR=42;

%% Get traces

%vidFolder = '/home/richard/rppgVideos/CHU/tv1-patient2';
%vidFolder = '/home/richard/rppgVideos/12-gt';
%vidFolder='/run/media/richard/841A-95A7/CHU/inducedflutter2-patient1';
ecgdata=getECGData(vidFolder);
if(subjectnum>0)
    if(subjectnum<10)
        prefix='0';
    else
        prefix=''
    end
    vidFolder=[vidFolder '/subject' prefix  num2str(subjectnum) '2.vid'];
end
outFileName = 'rgbTraces_skin';
disp(['Using source: ' vidFolder]);

% keyin=input('Is this correct? ','s');
keyin='y';
if(keyin=='y' || keyin =='Y')
    disp('');
else
    return;
end


% [vidFolder(1:end-4) '-' outFileName '.mat']
%This loads rgbTraces
if(strcmp(vidFolder(end-2:end),'vid')==1)
    load([vidFolder(1:end-4) '-' outFileName '.mat']);
else
    load([vidFolder '/' outFileName '.mat']);
end



if(strcmp(vidFolder(end-3:end-3),'9')==1)
    START=100; %To fix the problem of overwritten frames of nicolas
else
    %START=34; %Skipping first 50 frames to match old dump data
    START=1;
end

END=numel(rgbTraces(1+nChannels,:));
%END=3000;
time = rgbTraces(1+nChannels,START:END);
rgbTraces = rgbTraces(1:nChannels,START:END);
HRGT=[];
%Eliminate erronous time values
rgbTraces(:,time<=0)=[];
time(time<=0)=[];

if(~metricsmode)
    if(subjectnum>0)
        mainfigure = figure(subjectnum);
    else
        mainfigure = figure;
        set(gcf,'Name',['Time Frequency FFT : ' vidFolder]);
    end
    set(mainfigure,'units','normalized','outerposition',[0 0 1.1 1.1]);
    set(gcf,'NumberTitle','off')
    hold on
end



%% Normalize and resample
traceSize = size(rgbTraces,2);

% normalize each trace window to have zero mean and unit variance
rgbTraces = rgbTraces - repmat(mean(rgbTraces,2),[1 traceSize]);
rgbTraces = rgbTraces ./ repmat(std(rgbTraces,0,2),[1 traceSize]);

% resample
time=time-time(1);
inc = 1/Fs * 1000;
endTime = time(length(time));
newTime = 0 : inc : endTime;

if(~metricsmode)
    s1=subplot(2,2,1);
    title('Groundtruth trace')
    xlabel('Time(s)')
end

if(strcmp(vidFolder(end-2:end),'vid')==1)
    %Test with old data. Comment this block to disable this
    %load ('../fullrppgDataPYR.mat');
    
    vidNum=subjectnum;
    dataset=2;
    
    %Video number 3, second dataset, green trace
    
    %gtdata=[fullrppgData(vidNum,dataset).rppgData(:,1) fullrppgData(vidNum,dataset).rppgData(:,6)];
    gttime=gtdata(:,1);
    [gttime gtindex]=unique(gttime);
    gtdata=gtdata(gtindex,:);
    gtTrace=gtdata(:,2);
   
    if(strcmp(smoothing,'frequency-bandpass')==1)
        gtTracesSmoothed = smooth(gtTrace, 5, 'moving');
    else
        %Raw signal without smoothing
        gtTracesSmoothed=gtTrace;
    end
    
    gttime=gttime-gttime(1);
    if(~metricsmode)
        plot(gttime/1000,gtTracesSmoothed);
        title('Groundtruth trace')
    end
    
    
    x = gtTrace(15*Fs:end-10*Fs);
    x=gtTrace;
    N = length(x);
    h = hamming(odd(winLength));
    tfr=tfrstft(x, 1:length(x), length(x), h, 1);
    tfr2 = tfr(round(0.3*N/Fs):round(3.5*N/Fs),:);
    f = linspace(0.3*60,3.5*60);
    if(~metricsmode)
        s2=subplot(2,2,2);
        imagesc(newTime,f,abs(tfr2)), axis('xy');
        title('Groundtruth sfft')
    end
else
    
    if ~skipgt
        gtfilename=[vidFolder '/' 'gtdump.xmp'];
        %Groundtruth calculations
        if exist(gtfilename,'file')==2
            gtdata=csvread(gtfilename);
            gtTrace=gtdata(:,4);
            HRGT_PPG=gtdata(:,2);
            gttime=gtdata(:,1);
       
            if(strcmp(smoothing,'frequency-bandpass')==1)
                gtTracesSmoothed = smooth(gtTrace, 5, 'moving');
            else
                %Raw signal without smoothing
                gtTracesSmoothed=gtTrace;
            end
            
            
            x = gtTrace(15*Fs:end-10*Fs);
            x=gtTrace;
            N = length(x);
            h = hamming(odd(winLength));
            g=tftb_window(odd(winLength/4));
            
            tfr=tfrstft(x, 1:length(x), length(x), h, 1);
            tfr2 = tfr(round(0.3*N/Fs):round(3.5*N/Fs),:);
            
            %[tfr2,T,F]=tfrspwv(x, 1:length(x), 10 , h, 1);
            %tfrspwvmine(x,1:length(x),N,g,h);
            % plot(T',abs(tfr2(2,:)),'g.')
            
            f = linspace(0.3*60,3.5*60,size(tfr2,1));
            %         imagesc(newTime,f,abs(tfr2)), axis('xy');
            tfrabs=abs(tfr2);
            HRGT=zeros(1,size(tfrabs,2));
            
            if(~metricsmode)
                plot(gttime/1000,gtTracesSmoothed);
                s2=subplot(2,2,2);
                mesh(gttime,f,tfrabs);
                hold on
                h = waitbar(0,'Analyzing GT signal...');
            end
            loopcount=size(tfrabs,2);
            
            for i=1:loopcount
                if(~metricsmode && mod(i,100)==0)
                    %                     clc
                    %                     perc=round(100*i/size(tfrabs,2));
                    waitbar(i/loopcount,h);
                    %                    disp(['Analyzing GT signal : ' repmat('*',[1 round(perc/10)])  ' ' num2str(perc) '%' ]);
                end
                % plot(f,tfrabs(:,i));
                [pks locs]=findpeaks(tfrabs(:,i));
                %plot(f(locs),pks+0.5,'k^');
                sortedpks=sort(pks,'descend');
                pkpos1=locs(pks==sortedpks(1));
                pkpos2=locs(pks==sortedpks(2));
                if(f(pkpos2)>minHR)
                    HRGT(i)=f(pkpos2);
                    if(~metricsmode)
                        plot3(gttime(i),f(pkpos2),tfrabs(pkpos2,i),'k^','markerfacecolor',[1 0 0]);
                    end
                elseif(f(pkpos1)>minHR)
                    HRGT(i)=f(pkpos1);
                    if(~metricsmode)
                        plot3(gttime(i),f(pkpos1),tfrabs(pkpos1,i),'k^','markerfacecolor',[0 1 0]);
                    end
                end
                
                
                %plot3(repmat(gttime(i),size(locs)),f(locs),tfrabs(locs,i),'k^','markerfacecolor',[0 1 0]);
                
            end
            if(~metricsmode)
                close(h);
                title('Groundtruth sfft')
            end
        end
    else
        if(~metricsmode)
            s2=subplot(2,2,2);
        end
    end
end

if(~metricsmode)
    s3=subplot(2,2,3);
    hold on
    xlabel('Time(s)');
end

for c=1:nChannels
    if(strcmp(smoothing,'frequency-bandpass')==1)
        rgbTracesSmoothed(c,:) = smooth(rgbTraces(c,:), 5, 'moving');
    else
        %Raw signal without smoothing
        rgbTracesSmoothed(c,:) = rgbTraces(c,:);
    end
    
    newRgbTraces(c,:) = interp1(time,rgbTracesSmoothed(c,:),newTime, interpolation)';
    if(~metricsmode)
        if(c==1)
            plot(newTime/1000,newRgbTraces(c,:),'r');
        elseif(c==2)
            plot(newTime/1000,newRgbTraces(c,:),'g-');
        else
            plot(newTime/1000,newRgbTraces(c,:),'b');
        end
        
        title('RPPG trace');
    end
    %plot(gttime/1000,gtTracesSmoothed/60,'m');
    %plot(timeStamps,RGBTraces(1,:),'bo',times,traces(1,:),'ro');
    % figure(c),  plot(time/1000,rgbTracesSmoothed(c,:),'-x', newTime/1000,newRgbTraces(c,:), ':.');
end
pulseTraceRGB=newRgbTraces;

save(['pulseTraceRGB' vidFolder(1:end-4) '-' outFileName '.mat'] ,'pulseTraceRGB');
%% FFT v1
% figure();
% x = newRgbTraces(2,:)';
% N = length(x);
% fax_Hz = 60 * [0 : N-1]*Fs/N;
% spec = abs(fft(x)).^2;
% plot(fax_Hz, spec), xlim(60*[0.6 3.5]), xlabel('Heart Rate (bpm)');

%% FFT v2
% figure();
% x = newRgbTraces(2,:)';
% N = length(x);
% spec=fftshift(abs(fft(x)).^2);
% fax_Hz = 60 * [-N/2 : (N-1)/2] *Fs/N ;
% plot(fax_Hz,spec), xlim(60 * [0.6 3.5]), xlabel('Heart Rate (bpm)');

%% short time Fourrier Transform v1 (manual with fftshift)
% figure();
% timeShift = 35 * Fs;
% x = newRgbTraces(2,:)';
% N = length(x);
% h = hamming(odd(winLength));
% newH = zeros(N,1);
% newH(timeShift : timeShift + length(h)-1) = h;
% newX = x .* newH;
% spec=fftshift(abs(fft(newX)).^2);
% fax_Hz = 60 * [-N/2 : (N-1)/2] *Fs/N ;
% plot(fax_Hz,spec), xlim(60*[0.6 3.5]), xlabel('Heart Rate (bpm)');

%% short time Fourrier Transform v2 (manual without fftshift)
% figure();
% timeShift = 35 * Fs;
% x = newRgbTraces(2,:)';
% N = length(x);
% h = hamming(odd(winLength));
% newH = zeros(N,1);
% newH(timeShift : timeShift + length(h)-1) = h;
% newX = x .* newH;
% spec=abs(fft(newX)).^2;
% fax_Hz =  60 * [0 : N-1]*Fs/N ;
% plot(fax_Hz,spec), xlim(60*[0.6 3.5]), xlabel('Heart Rate (bpm)');

%% Short time Fourrier Transform v3 (whole signal)
if(nChannels==1)
        x = newRgbTraces(1,15*Fs:end-10*Fs)';
    else
        x = newRgbTraces(2,15*Fs:end-10*Fs)';
end
t=newTime(15*Fs:end-10*Fs);
N = length(x);
h = hamming(odd(winLength));
tfr=tfrstft(x, 1:length(x), length(x), h, 1);
tfr2 = tfr(round(0.3*N/Fs):round(3.5*N/Fs),:);
f = linspace(0.3*60,3.5*60,size(tfr2,1));
% [tfr2,T,F]=tfrspwv(x, 1:length(x), 10 , h, 1);

% plot(T',abs(tfr2(10,:)),'g.')
tfrabs=abs(tfr2);
if(~metricsmode)
    s4=subplot(2,2,4);
    mesh(t,f,tfrabs);
end

%% Extract heart rate signal

HRs=zeros(1,size(tfrabs,2));
tfrabs_smoothed=zeros(size(tfrabs));
sortedPeakLocs=ones(size(tfrabs));
sortedPeaks=zeros(size(tfrabs));
confidence=zeros(1,size(tfrabs,2));
%Number of peaks to choose the HR from. For instance, if this number is 10,
%we will choose our final HR from the peaks nearest to the tentative HR
%from the first highest 10 peaks in the spectrum
nPeaksThreshold=10;

% First extract the frequency peaks at each time interval
if(~metricsmode)
    hold on
    h = waitbar(0,'Preprocessing RPPG signal...');
end
loopcount=size(tfrabs,2);

for i=1:loopcount
    if(mod(i,100)==0 && ~metricsmode)
        %Progress bar
        waitbar(i/loopcount,h);
    end
    
    [pks locs]=findpeaks(tfrabs(:,i));
    
    sortedpks=sort(pks,'descend');
    %Save first 10 peaks for use later in pruning the HRs
    nPeaks=numel(sortedpks);
    confidence(i)=nPeaks;
    for idx=1:nPeaks
        sortedPeakLocs(idx,i)=locs(pks==sortedpks(idx));
        sortedPeaks(idx,i)=pks(pks==sortedpks(idx));
    end
    pkpos1=locs(pks==sortedpks(1));
    pkpos2=locs(pks==sortedpks(2));
    
    %Choose the most likely heart rate
    %peakfound=false;
    
    if(f(pkpos1)>minHR)
        sprintf('%f ',f(pkpos1));
        %plot3(t(i),f(pkpos1),tfrabs(pkpos1,i),'k^','markerfacecolor',[0 1 0]);
        HRs(i)=f(pkpos1);
        %peakfound=true;
        %text(f(pkpos1),sortedpks(1)+.01,num2str(f(pkpos1)),'Color','Red');
        
    else
        if(f(pkpos2)>minHR)
            %text(f(pkpos2),sortedpks(2)+.01,num2str(f(pkpos2)),'Color','Red');
            %HRs(i)=f(pkpos2);
        end
    end
    
end
if(~metricsmode)
    close(h);
end
%     clc
%     perc=round(100*i/size(tfrabs,2));
%     disp(['Analyzing RPPG : ' repmat('*',[1 10])  ' 100%' ]);

%Now we have the inital HR estimates. Prune them to get a more accurate
%estimate. HRs(1,i) contains peaks with the highest probability and HRs(2,i) contains second
%highest. UPDATE: HRs(i) now contains the peaks since we are eliminating
%all HRs below 40 for now.
%We choose the one which is closest to the average HR of possibly
%correct HRs
finalHR=zeros(1,size(HRs,2));
%Tentative mean is the mean of the highest definite peaks. Positions having
%a value of 10 means no peak was found, so we use the average HR as the HR
%for that point
tentativeMeanHR=mean(HRs(1,(HRs(1,:)~=0)))
%Normally, hr cannot change drastically. To avoid such changes,
meanThreshold=10;
meanCalculationWindowSize=300;


loopcount=size(HRs,2);


if(individualFrameAnalysis==true)
    if(~metricsmode)
        auxfigure=figure;
        set(gcf,'NumberTitle','off')
        set(gcf,'Name','Individual Frame HR Analysis');
        
        hold on
        
        plot(f,tfrabs(:,i));
        
        plot(f(locs),pks+.01,'k^');
    end
end
%Smooth around the peak frequency
sig=10;
gaussFilter=exp(-((f-tentativeMeanHR)/sig).^2)/(sqrt(2*pi)*sig);
gaussFilter=gaussFilter/sum(gaussFilter);

prevHR=0;
if(~metricsmode)
    h = waitbar(0,'Analyzing RPPG signal...');
    figure(mainfigure);
    set(mainfigure,'visible','off');
    cla;
end
for i=1:loopcount
    %Find the peak from the first 10 peaks that is closest to the
    tentativeWindowMean=tentativeMeanHR;
    if(~metricsmode && mod(i,100)==0)
        %Progress bar
        waitbar(i/loopcount,h);
    end
    %Windowed mean
    if(~globalMean)
        if(i>meanCalculationWindowSize/2 && i<=loopCount-meanCalculationWindowSize/2)
            hrwindow=HRs(1,i-meanCalculationWindowSize/2:i+meanCalculationWindowSize/2);
            tentativeWindowMean=mean(hrwindow(hrwindow~=0));
            % plot(HRs(1,i-meanCalculationWindowSize/2:i+meanCalculationWindowSize/2));
            
            %For time points where original HR was not able to be
            %calculated, use the mean HR.
            if(isnan(tentativeWindowMean) || tentativeWindowMean==0)
                tentativeWindowMean=tentativeMeanHR;
            end
            
        end
    end
    [~, idx]=min(abs(f(sortedPeakLocs(1:nPeaksThreshold,i))-tentativeWindowMean));
    finalHR(i)=f(sortedPeakLocs(idx,i));
    %% Gaussian filter on the tfr with peak at tentativeMean
    %Scaled and smoothed tfr
    tfrabs_smoothed(:,i)=20*gaussFilter'.* (tfrabs(:,i));
    [~, locs]=findpeaks(tfrabs_smoothed(:,i));
    %Find the peak nearest to the mean
    [~, idx]=min(abs(f(locs)-tentativeWindowMean));
    finalHR(i)=f(locs(idx));
    
    %Eliminate outliers. HR is unlikely to change rapidly between
    %successive frames
    if(prevHR~=0 && abs(finalHR(i)-prevHR)>5)
        %finalHR(i)=prevHR;
    end
    prevHR=finalHR(i);
    %%
    if(individualFrameAnalysis==true)
        figure(auxfigure)
        cla
        title([num2str(round(t(i)/1000)) 'th second']);
        text(f(locs(idx)),peaks(idx)+.001,num2str(f(locs(idx))));
        %plot(f,gaussFilter);
        plot(f,tfrabs(:,i));
        plot(f(locs),peaks,'k^');
        %plot(f(sortedPeakLocs(:,i)),sortedPeaks(:,i)+.01,'k^');
        plot(f,tfrabs_smoothed(:,i));
        pause(0.001);
        
    end
    if(~metricsmode)
        figure(mainfigure)
        plot3(t(i),finalHR(i),tfrabs_smoothed(locs(idx),i),'k^','markerfacecolor',[0 1 0]);
    end
    %plot3(t(:,i),f(:,i),tfrabs_smoothed(:,i));
    
end
if(~metricsmode)
    close(h);
    mesh(t,f,tfrabs_smoothed);
    
    title('STFT RPPG');
    xlabel('Time(s)');
    
    figure(mainfigure)
    d=0.02; %distance between subplots
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 10])
    
    set(s1,'position',[d .51 0.48 0.45],'Units','normalized')
    set(s2,'position',[0.5+d .51 0.48 0.45],'Units','normalized')
    set(s3,'position',[d d*2 0.48 0.45],'Units','normalized')
    set(s4,'position',[.5+d d*2 0.48 0.45],'Units','normalized')
    
end

finalHR_smoothed=sgolayfilt(finalHR,3,51);


Mean_RPPG=mean(finalHR);

Std_RPPG=std(finalHR);


%GT HR has much more samples than finalHR, Interpolate so that the metrics
%are more meaningful
if skipgt ~=1
    HRGT_interpolated=interp1(gttime,HRGT,t,interpolation);
    HRGT_PPG_interpolated=interp1(gttime,HRGT_PPG,t,interpolation);
    
    Mean_HRGT=mean(HRGT_interpolated);
    Mean_HRGT_PPG=mean(HRGT_PPG_interpolated);

    Std_HRGT=std(HRGT_interpolated);
    Std_HRGT_PPG=std(HRGT_PPG_interpolated);

    [r1,~]=corrcoef([HRGT_interpolated;finalHR]');
    [r2,~]=corrcoef([HRGT_PPG_interpolated;finalHR]');
    CorrCoef_HRGT_vs_RPPG=r1(1,2);
    CorrCoef_HRGTPPG_vs_RPPG=r2(1,2);

disp('MEAN-GT    MEAN-GT_PPG     MEAN-RPPG       DIFFERENCE');
disp('-----------------------------------------------------');
disp([num2str(Mean_HRGT) '       ' num2str(Mean_HRGT_PPG) '       ' num2str(Mean_RPPG) '       ' num2str(Mean_RPPG-Mean_HRGT_PPG)]);
disp(' ');

disp('STDDEV-GT    STDDEV-GT_PPG     STDDEV-RPPG');
disp('---------------------------------------------');
disp([num2str(Std_HRGT) '       ' num2str(Std_HRGT_PPG) '       ' num2str(Std_RPPG)]);
disp(' ');


disp('CorrCoeff-GT-vs-RPPG   CorrCoeff-GT_PPG-vs-RPPG');
disp('--------------------------------------------------');

disp([num2str(CorrCoef_HRGT_vs_RPPG) '       ' num2str(CorrCoef_HRGTPPG_vs_RPPG)]);
disp(' ');


save([vidFolder '/metrics' metric_suffix '.mat' ],'Mean_HRGT','Mean_HRGT_PPG','Mean_RPPG',...
    'Std_HRGT','Std_HRGT_PPG','Std_RPPG',...
    'CorrCoef_HRGT_vs_RPPG','CorrCoef_HRGTPPG_vs_RPPG');

end






% disp( [ 'Mean GT = ' num2str(mean(HRGT_interpolated))]);
% disp( [ 'Mean GT PPG= ' num2str(mean(HRGT_PPG_interpolated))]);
% disp( [ 'Mean RPPG = ' num2str(mean(finalHR))]);
% disp( [ 'Mean RPPG smoothed= ' num2str(mean(finalHR_smoothed))]);


if(~metricsmode)
    figure
    set(gcf,'NumberTitle','off')
    hold on
    if skipgt ~=1
    plot(HRGT_interpolated,'r');
    plot(HRGT_PPG_interpolated,'b');
    end
    plot(finalHR,'g');
    %plot(finalHR_smoothed,'Color',[.1 .1 .5]);
    
    legend('HRGT\_interpolated','finalHR','HRGT\_PPG\_interpolated');
    set(gcf,'Name','Estimated Heart Rate');
end
%         clc
%         disp(['Processing : ' repmat('*',[1 10])  ' ' num2str(100) '%' ]);
%imagesc(time/1000,f,abs(tfr2)), axis('xy');






%fax_Hz = 60 * [0 : N-1]*Fs/N ;
%spec = abs(tfr(:,Fs * 42.5)).^2;
%plot(fax_Hz,abs(spec)), xlim(60*[0.6 3.5]), xlabel('Heart Rate (bpm)');
%imagesc(abs(tfr)), axis('xy'), ylim([0.6*N/Fs 3.5*N/Fs])

% figure()
% x = newRgbTraces(2,:)';
% %newTime = 0 : inc : endTime;
% %x_an = newRgbTraces(2,:)' + hilbert(newRgbTraces(2,:)' );
% h = hamming(odd(winLength));
% %x_an = hilbert(newRgbTraces(2,:)' );
% tfr=tfrstft(x, 1:length(newTime), length(newTime), h, 1);
% imagesc(abs(tclose all;fr));
%
%
% [tfr,t,f]=tfrwv(x_an);
% %contour(tfr,5); grid
% a = tfr(1:10:end,1:10:end);
% step = 10;
% contour(t(1:step:end),f(1:step:end),tfr(1:step:end,1:step:end));



% print(['/home/richard/Studies/Sem4/Internship/HeartRateUI/MATLAB/currentMatlabCode/results/subject' prefix  num2str(subjectnum)],'-dpng');
disp(['Finished metrics for ' vidFolder]);

pause

clc
close all

end
