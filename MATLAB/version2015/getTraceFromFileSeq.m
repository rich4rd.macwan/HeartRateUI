clear all;
close all;
clc;

addpath('./tools/skindetector');

DO_FACECROP = 0;
DO_SKINDETECTION = 1;
vidFolder = 'E:\RPPGVideos\videoTest\4';
%vidFolder = '/home/richard/Studies/Sem4/Internship/Summer2015/videos_rPPG/4';
outFileName = 'rgbTraces_skin';

% Create a cascade detector object.
faceDetector = vision.CascadeObjectDetector();

% Create the point tracker object.
pointTracker = vision.PointTracker('MaxBidirectionalError', 2);

% nb of frames
nbFiles=length(dir([vidFolder '\img\*.ppm']));
%nbFiles=length(dir([vidFolder '/img/*.ppm']));

rgbTraces = zeros(4, nbFiles-1);

numPts = 0;
for n=1:nbFiles-1
    
    % read frame by frame number (maybe not ordered)
    imgName = dir([vidFolder '\img\frame_' num2str(n) '_*.ppm']);
    imgName = imgName.name;
    img = imread([vidFolder '\img\' imgName]);
    img_copy = img;
    
    if numPts < 10
        % Detection mode.
        bbox  = step(faceDetector, img);

        if ~isempty(bbox)
            % Find corner points inside the detected region.
            points = detectMinEigenFeatures(rgb2gray(img), 'ROI', bbox(1, :));

            % Re-initialize the point tracker.
            xyPoints = points.Location;
            numPts = size(xyPoints,1);
            release(pointTracker);
            initialize(pointTracker, xyPoints, img);

            % Save a copy of the points.
            oldPoints = xyPoints;

            % Convert the rectangle represented as [x, y, w, h] into an
            % M-by-2 matrix of [x,y] coordinates of the four corners. This
            % is needed to be able to transform the bounding box to display
            % the orientation of the face.
            x = bbox(1, 1);
            y = bbox(1, 2);
            w = bbox(1, 3);
            h = bbox(1, 4);
            bboxPoints = [x,y;x,y+h;x+w,y+h;x+w,y];

            % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
            % format required by insertShape.
            bboxPolygon = reshape(bboxPoints', 1, []);

            % Display a bounding box around the detected face.
            img = insertShape(img, 'Polygon', bboxPolygon, 'LineWidth', 3);

            % Display detected corners.
            img = insertMarker(img, xyPoints, '+', 'Color', 'white');
        end

    else
        % Tracking mode.
        [xyPoints, isFound] = step(pointTracker, img);
        visiblePoints = xyPoints(isFound, :);
        oldInliers = oldPoints(isFound, :);

        numPts = size(visiblePoints, 1);

        if numPts >= 10
            % Estimate the geometric transformation between the old points
            % and the new points.
            [xform, oldInliers, visiblePoints] = estimateGeometricTransform(...
                oldInliers, visiblePoints, 'similarity', 'MaxDistance', 4);

            % Apply the transformation to the bounding box.
            bboxPoints = transformPointsForward(xform, bboxPoints);

            % Convert the box corners into the [x1 y1 x2 y2 x3 y3 x4 y4]
            % format required by insertShape.
            bboxPolygon = reshape(bboxPoints', 1, []);

            % Display a bounding box around the face being tracked.
            img = insertShape(img, 'Polygon', bboxPolygon, 'LineWidth', 3);

            % Display tracked points.
            img = insertMarker(img, visiblePoints, '+', 'Color', 'white');

            % Reset the points.
            oldPoints = visiblePoints;
            setPoints(pointTracker, oldPoints);
        end
    end
    
    if(DO_FACECROP)
        % crop the face
        % horizontal cropping
        scaleW = 0.6;
        bbox(1,3) = bbox(1,3)*scaleW; 
        bbox(1,1) = bbox(1,1) + bbox(1,3)*((1-scaleW)/2); 
        % vertical cropping
        scaleV = 0.6;
        bbox(1,4) = bbox(1,4)*scaleV; 
        bbox(1,2) = bbox(1,2) + bbox(1,4)*((1-scaleV)/2); 
    end;
    
    imgROI = imcrop(img_copy,bbox(1,:));
    %figure(2), imshow(imgROI), title('Detected Face');
    
    if (DO_SKINDETECTION)
        
        % average RGB value of skin pixels
        imgD = double(imgROI);
        skinprob = computeSkinProbability(imgD);
        mask = skinprob>0;
        maskRGB = zeros(size(skinprob,1),size(skinprob,2),3);
        maskRGB(:,:,1) = mask;
        maskRGB(:,:,2) = mask;
        maskRGB(:,:,3) = mask;
        imgD = imgD.*maskRGB;
        imgROI(imgD==0)=nan;
        
        %figure(3), imshow(imgROI), title('Detected Face + skin');
        
    end;
    
    avg=nanmean(nanmean(imgROI));    
    
    rgbTraces(1:3,n) = avg;

    %get timestamp from name
    [startIndex,endIndex] = regexp(imgName,'\_\d*\.');
    time = imgName(startIndex+1:endIndex-1);
    rgbTraces(4,n) = str2double(time);
    
    %figure(1), imshow(img);
    
    if (mod(n,10) == 0)
        fprintf('%i sur %i %s, timestamp %s \n', n, nbFiles, imgName, time);
    end
end    

save([vidFolder '\' outFileName '.mat'], 'rgbTraces');