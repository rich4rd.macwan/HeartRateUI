function [gtTrace, gtHR, gtTime] = loadPPG(vidFolder,varargin)
% load PPG recorded with pulse oximeter

gtHR = [];
gtTrace = [];
gtTime = [];
i=1;
while(nargin > i)
    if(strcmp(varargin{i},'database')==1)
        dbase=varargin{i+1};
    end
    i=i+1;
end

if strcmp(dbase,'MMSE')
    tokens=strsplit(vidFolder,'/');
    if strcmp(tokens{end},'')==1
        %Last element is a blank
        taskid=tokens{end-1};
        subjectid=tokens{end-2};
        subdb=tokens{end-3};
        prefix=strjoin(tokens(1:5),'/');
    else
        taskid=tokens{end};
        subjectid=tokens{end-1};
        subdb=tokens{end-2};
        prefix=strjoin(tokens(1:3),'/');
    end
    if strcmp(subdb,'first_10_subjects 2D')==1
        physdata_path=[prefix ...
            '/first 10 subjects Phydata released/Phydata/' ...
            subjectid '/' taskid '/'];
    elseif strcmp(subdb,'T10_T11_30Subjects')==1
        physdata_path=[prefix ...
            '/T10_T11_30PhyBPHRData/' ...
            subjectid '/' taskid '/'];
    end
    physdata_filename=[physdata_path 'Pulse Rate_BPM.txt'];
    %Load physdata_filename in gtHR, 1000 frames/sec
    fileID=fopen(physdata_filename);
    gtHR=cell2mat(textscan(fileID,'%f'));
    gtTime=1:numel(gtHR);
    fclose(fileID);
    
else
    
    gtfilename=[vidFolder 'gtdump.xmp'];
    
    if exist(gtfilename)==2
        gtdata=csvread(gtfilename);
        gtTrace=gtdata(:,4);
        gtTime=gtdata(:,1);
        gtHR = gtdata(:,2);
        
        % normalize data (zero mean and unit variance)
        gtTrace = gtTrace - mean(gtTrace,1);
        gtTrace = gtTrace / std(gtTrace);
        
    else
        fprintf('oops, no PPG file...\n');
    end
    
end