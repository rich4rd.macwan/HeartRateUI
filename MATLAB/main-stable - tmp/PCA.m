function selectedSig = PCA(rgb, Fs)

%Input: each row of matrix rgb is one observed signal (i.e., r,g,b signals)
%Output: most periodic component (based on SNR)
%fprintf('Extracting PCA trace\n');
    LOW_F=.7; % low freq for BP filtering  
    UP_F=3; % high freq for BP filtering 
    WIDTH = 0.1; % Width for the door step function to compute SNR (in Hz)

    testR=rgb(1,:);
    testG=rgb(2,:);
    testB=rgb(3,:);

    meanR=mean(testR);
    meanG=mean(testG);
    meanB=mean(testB);
    newR=(testR-meanR);
    newG=(testG-meanG);
    newB=(testB-meanB);

    testrgb=[newR; newG; newB];

    [E,D]=pcamat(testrgb);
    result=E'*rgb;
    result=result';

    maxsnr = 0;
    for index=1:size(result,2)

        x = result(:,index);
        N = length(x)*3;
        sig_freq = [0 : N-1]*Fs/N;
        sig_power = abs(fft(x,N)).^2;

%         fig3 = figure(3);
%         clf(fig3);
%         plot(sig_freq, sig_power), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
%         title('rPPG FFT')

        range = (sig_freq>LOW_F & sig_freq < UP_F);   % frequency range to find PSD peak
        sig_power = sig_power(range);
        sig_freq = sig_freq(range);
        [sig_peakPower,loc] = max(sig_power); % findpeaks(sig_power,'SortStr','descend','NPeaks',1); % get first peak
        sig_peakLoc = sig_freq(loc);    
        peaksPower(index) = sig_peakPower;

        range = (sig_freq>(sig_peakLoc-WIDTH/2) & sig_freq < (sig_peakLoc+WIDTH/2))';
        signal = sig_power.*range;
        noise = sig_power.*(~range);

%         fig4 = figure(4);
%         clf(fig4);
%         hold on;
%         plot(sig_freq, signal, 'r'), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
%         plot(sig_freq, noise, 'b'), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
%         title('rPPG FFT')

        n = sum(noise);
        s = sum(signal);
        snr = s/n;

        if snr>maxsnr
             maxsnr=snr;
             selectedSig=x';
        end  
    end
end

