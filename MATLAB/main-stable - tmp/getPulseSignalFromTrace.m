function getPulseSignalFromTrace(varargin)
% preprocess RGB trace to get pulse trace
% Green method: sliding window, detrend, band-pass on green channel
% Chrom method: "Robust Pulse Rate From Chrominance-Based rPPG" Haan et al. 2013
% yannick.benezeth@u-bourgogne.fr
% 17/02/2016

%clear all;
 close all;
%clc;

%addpath('../tools/PCA');
addpath('../richardDev');

%disp('preprocess RGB trace to get pulse trace');

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
WINDOW_LENGTH_SEC = 30; % length of the sliding window (in seconds) (use -1 to work on the whole signal at once)
STEP_SEC = 0.5; % step between 2 sliding window position (in seconds)
LOW_F=.7; % low freq for BP filtering
UP_F=3; % high freq for BP filtering
FILTER_ORDER = 8; % Order of the butterworth BP filter
SHOWPLOTS = 0;
SAVEFILES = 1;
ICAWIN=true;
method='CICA';
dbase='LE2I';
overwrite=true;
% vidFolder='/home/richard/rppgVideos/testvideo/';
% outFolder='/home/richard/rppgVideos/testvideo/';
% vidFolder='./data/RichardSummer2015/5-gt/';
% outFolder='./data/RichardSummer2015/5-gt/';
% vidFolder ='/run/media/richard/Le2iHDD/CHU/patient9-sinus3/';
% outFolder='/run/media/richard/Le2iHDD/CHU/patient9-sinus3/';

% Richard's Path
vidFolder ='D:/Le2i/10-gt/';
outFolder='D:/Le2i/10-gt/';

% Yannick's Path
% vidFolder ='D:\Dataset\HeartRate\RichardSummerVideos\5-gt\';
% outFolder='D:\Dataset\HeartRate\RichardSummerVideos\5-gt\';

matFileName = 'rgbTraces'; % input file
%matFileName = 'rgbTraces_crop'; % input file
% matFileName = 'rgbTraces_skin'; % input file


i = 1;
silentMode=false;
DOFILTER=false;
while(nargin > i)
    if(strcmp(varargin{i},'vidFolder')==1)
        vidFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'outFolder')==1)
        outFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'WINDOW_LENGTH_SEC')==1)
        WINDOW_LENGTH_SEC=varargin{i+1};
    end
    if(strcmp(varargin{i},'FILTER_ORDER')==1)
        FILTER_ORDER=varargin{i+1};
    end
    if(strcmp(varargin{i},'keyword')==1)
        keyword=varargin{i+1};
        if ~isempty(keyword)
            matFileName=['rgbTraces_' varargin{i+1}];
        end
    end
    if(strcmp(varargin{i},'silent')==1)
        silentMode=varargin{i+1};
    end
    if(strcmp(varargin{i},'ICAWIN')==1)
        ICAWIN=varargin{i+1};
    end
    if(strcmp(varargin{i},'DOFILTER')==1)
        DOFILTER=varargin{i+1};
    end
    if(strcmp(varargin{i},'method')==1)
        method=varargin{i+1};
    end
    if(strcmp(varargin{i},'database')==1)
        dbase=varargin{i+1};
    end
    if(strcmp(varargin{i},'overwrite')==1)
        overwrite=varargin{i+1};
    end
    i = i+2;
end
% disp(WINDOW_LENGTH_SEC);
% disp(FILTER_ORDER);
% disp(outFolder);




disp(['Get Pulse from ' vidFolder matFileName '.mat']);

if(exist([vidFolder matFileName '.mat'], 'file') == 2)
    load([vidFolder matFileName '.mat']);
else
    disp('oops, no input file');
    return;
end
% load PPG
[gtTrace, gtHR, gtTime] = loadPPG(vidFolder,'database',dbase);
gtTime = gtTime / 1000;  % in seconds


nTraces=3;
%rgbTraces=[traces(1:nTraces,:);traces(end,:)];
% remove 0s if any (happens for example on CHU data)
rgbTraces(:,rgbTraces(nTraces+1,:)<=0)=[];

% just for bodySync v1 (duplicate images)
% rgbTraces=rgbTraces(:,1:2:end);

% get current traces
if strcmp(dbase,'MMSE')==1
    %Frame rate of 25/s
    Fs=25;
    timeTrace=linspace(0, size(rgbTraces,2)/Fs, size(rgbTraces,2));
else
    timeTrace = rgbTraces(end,:);
    timeTrace = timeTrace/1000; % in second
end

crtTrace = rgbTraces(1:nTraces,:);
traceSize = size(crtTrace,2);

% Uniform sampling
timeTraceUnif = linspace(timeTrace(1), timeTrace(end), traceSize);

for k=1:nTraces
    crtTrace(k,:) = interp1(timeTrace,crtTrace(k,:), timeTraceUnif);
    
end
% f=figure(1);
% hold on
% plot(timeTraceUnif,crtTrace(1,:),'Color',[.8,.1,.3]);
% plot(timeTraceUnif,crtTrace(2,:),'Color',[.1,.8,.3]);
% plot(timeTraceUnif,crtTrace(3,:),'Color',[.3,.1,.8]);
% % close(f);
% pause(.01)
timeTrace = timeTraceUnif;

%Display traces

if strcmp(dbase,'MMSE')~=1
    % get exact Fs
    Fs = 1/mean(diff(timeTrace));
end
% get window length in frames
winLength = round(WINDOW_LENGTH_SEC*Fs);
step = round(STEP_SEC*Fs);

% get coef of butterworth filter
[b, a]= butter(FILTER_ORDER,[LOW_F UP_F]/(Fs/2));
%Hamming
initialA=[];
%Create struct with 3 dim and 2 dim w. alternate between them based on
%the num of dimensions (if reduced) during optimization
cICAFirstPass=true;
wcICA=rand(3,1);
wcICA=wcICA/norm(wcICA);
wMAICA=rand(3,1);
wMAICA=wMAICA/norm(wMAICA);
% wcICA.w2 = rand(2,1);
% wcICA.w2=wcICA.w2/norm(wcICA.w2);
% wcICA.dimRedWinInds=[];
if(WINDOW_LENGTH_SEC == -1)
    
    % detrend and filter the whole signal (no sliding window - in case we can work on the whole signal)
    for k=1:3
        crtTrace(k,:) = detrendsignal(crtTrace(k,:)')';
    end
    if DOFILTER
        % filter the whole signal (in case we can work on the whole signal)
        for k=1:3
            crtTrace(k,:) = filter(b,a,crtTrace(k,:)')';
        end
    end
    % Method 1: Green channel
    pulseTraceRed = crtTrace(1,:);
    pulseTraceGreen = crtTrace(2,:);
    pulseTraceBlue = crtTrace(3,:);
    
    % Method 2: CHROM
    Rf = crtTrace(1,:);
    Gf = crtTrace(2,:);
    Bf = crtTrace(3,:);
    Xf = 3*Rf-2*Gf;
    Yf = 1.5*Rf+Gf-1.5*Bf;
    alpha = std(Xf)/std(Yf);
    pulseTraceChrom = Xf - alpha*Yf;
    
    % Method 3 : PCA
    RGB_input = [ crtTrace(1,:); crtTrace(2,:); crtTrace(3,:)];
    pulseTracePCA = PCA(RGB_input, Fs);
    
    % Method 4 : ICA: works better with overlap added signal
    %     if ICAWIN==true
    %         OverValue=1e-4;  maxIter = 400;
    %         [Sig,W,initialA,pulseTraceICA]=simpleICA(RGB_input,maxIter,OverValue,'choosebestcomponent',true,'func','tanh');
    %     end
else
    %Chrom weights for the whole signal
     % detrend and filter the whole signal (no sliding window - in case we can work on the whole signal)
    for k=1:3
        crtTrace(k,:) = detrendsignal(crtTrace(k,:)')';
    end
    if DOFILTER
        % filter the whole signal (in case we can work on the whole signal)
        for k=1:3
            crtTrace(k,:) = filter(b,a,crtTrace(k,:)')';
        end
    end
    % Method 2: CHROM
    Rf = crtTrace(1,:);
    Gf = crtTrace(2,:);
    Bf = crtTrace(3,:);
    Xf = 3*Rf-2*Gf;
    Yf = 1.5*Rf+Gf-1.5*Bf;
    alpha = std(Xf)/std(Yf);
    wchromfull=[3*(1-.5*alpha) -2*(1+.5*alpha)  1.5*alpha]';
    wchromfull=wchromfull/norm(wchromfull);
    
    halfWin = (winLength/2);
    
    pulseTraceGreen=zeros(1,traceSize);
    pulseTraceBlue=zeros(1,traceSize);
    pulseTraceRed=zeros(1,traceSize);
    pulseTraceChrom=zeros(1,traceSize);
    pulseTracePCA=zeros(1,traceSize);
    pulseTraceG_R=zeros(1,traceSize);
    pulseTracePOS=zeros(1,traceSize);
    if ICAWIN
        pulseTraceICA=zeros(numel(halfWin:step:traceSize-halfWin),winLength);
        pulseTraceCICA=zeros(numel(halfWin:step:traceSize-halfWin),winLength);
        pulseTraceMAICA=zeros(numel(halfWin:step:traceSize-halfWin),winLength);
    end
    ind=1;
    Rs=struct('ICA',{},'ICA1',{},'ICA2',{},'ICA3',{},'CICA',{},'Chrom',{},'W',{},'A',{});
    for i=halfWin:step:traceSize-halfWin
        
        % get start/end index of current window
        startInd = i-halfWin+1;
        endInd = i+halfWin;
        
        % get current trace window
        crtTraceWin = crtTrace(1:nTraces,startInd:endInd);
        crtTimeWin = timeTrace(startInd:endInd);
        
        if(SHOWPLOTS)
            figure(1), plot(crtTimeWin, crtTraceWin(2,:)),title('raw green trace'), xlabel('seconds'), ylabel('pixel values');
        end
        
        % detrend
        for k=1:size(crtTraceWin,1)
            crtTraceWin(k,:) = detrendsignal(crtTraceWin(k,:)')';
        end
        % normalize each signal to have zero mean and also unit variance
        crtTraceWin = crtTraceWin - repmat(mean(crtTraceWin,2),[1 winLength]);
        crtTraceWin = crtTraceWin ./ repmat(std(crtTraceWin,0,2),[1 winLength]);
        
        %crtTraceWin = crtTraceWin ./ repmat(mean(crtTraceWin,2),[1 winLength]);
        
        if(SHOWPLOTS)
            fig2 = figure(2);
            clf(fig2);
            hold on;
            p1 = plot(crtTimeWin, crtTraceWin(1,:),'r');
            p2 = plot(crtTimeWin, crtTraceWin(2,:),'g');
            p3 = plot(crtTimeWin, crtTraceWin(3,:),'b');
            title('normalized traces'), xlabel('seconds'), ylabel('normalized pixel values');
            hold off;
        end
        
        
        if(SHOWPLOTS)
            figure(3), plot(crtTimeWin, crtTraceWin(2,:)), title('detrended green trace');
        end
        crtPulseRed = crtTraceWin(1,:);
        crtPulseGreen = crtTraceWin(2,:);
        crtPulseBlue = crtTraceWin(3,:);
%         wVsG(crtTrace,wchromfull);
        
        if ICAWIN==true
            
            %ICA or cICA
            crtRGB=cat(1,crtPulseGreen,crtPulseRed,crtPulseBlue);
            if strcmp(method,'ICA')==1
                OverValue=1e-4;  maxIter = 400;
                if isempty(initialA)
                    [Sig,W,initialA,crtPulseICA,selectedIndex]=simpleICA(crtRGB,maxIter,OverValue,'choosebestcomponent',true,'func','tanh','timeTrace',timeTrace);
                else
                    [Sig,W,initialA,crtPulseICA,selectedIndex]=simpleICA(crtRGB,maxIter,OverValue,'initguess',initialA,'choosebestcomponent',true,'func','tanh','timeTrace',timeTrace);
                end
                pulseTraceICA(ind,:)=smooth(crtPulseICA'/std(crtPulseICA));
                %Plot autocorr of each component
                %figure(11);
                %hold on;
                %col='rgbcmyk';
                Rs(ind).W=W;
                Rs(ind).A=initialA; %Dewhitened separation matrix
                [R,~,~]=autocorrd(pulseTraceICA(ind,:),1,1/mean(diff(crtTimeWin)));
                Rs(ind).ICA=mean(R.*R);
                %CICA contains selectedIndex for ICA
                Rs(ind).CICA=selectedIndex;
                %plot(ind,mean(R.*R),'s','MarkerSize',12,'Color',[.5 .25 .25]);
                fNames=fieldnames(Rs(ind));
                for s=1:size(Sig,1)
                    Sig(s,:)=smooth(Sig(s,:)/std(Sig(s,:)));
                    [R,~,~]=autocorrd(Sig(s,:),1);
                    Rs(ind).(fNames{s+1})=mean(R.*R);
                    %scatter(ind,mean(r.*r),col(s));
                end
                if size(Sig,1)<3
                    if size(Sig,1)<2
                        Rs(ind).ICA2=0;
                    end
                    Rs(ind).ICA3=0;
                end
                
            end
            if strcmp(method,'CICA')==1
                mu0 = 1;
                lambda0 = 1;
                gamma = 1e3;
                learningRate = 10;
                OverValue=1e-4;  maxIter = 100;
                threshold = 1;
                %                 figure(3);
                %                 cla;
                %                 hold on
                %                 plot(crtTimeWin,crtRGB(1,:));
                %                 plot(crtTimeWin,crtRGB(2,:));
                %                 plot(crtTimeWin,crtRGB(3,:));
                %                 pause(.01)
                %                 continue;
                %             (crtTimeWin(1)+crtTimeWin(end))/2
                %                 if (crtTimeWin(1)+crtTimeWin(end))/2>50
                %                      wcICA=rand(3,1);
                %                      wcICA=wcICA/norm(wcICA);
                %                 end
                %                  wVsG(vidFolder,crtRGB);
                if cICAFirstPass
                    cICAFirstPass=false;
                    [crtPulseCICA,wcICA,A]=cICAfmincon(crtRGB, crtTimeWin, threshold, wcICA, learningRate, mu0, lambda0, gamma, maxIter, OverValue,false);
                else
                    [crtPulseCICA,wcICA,A]=cICAfmincon(crtRGB, crtTimeWin, threshold, wcICA, learningRate, mu0, lambda0, gamma, maxIter, OverValue,false);
                end
                %                 plot(crtTimeWin,crtPulseCICA,'-x');
                
                %[crtPulseCICA,wcICA]=GAMulti(crtRGB,crtTimeWin);
                %disp(wcICA.w3), disp(wcICA.w2);
                pulseTraceCICA(ind,:)=smooth(crtPulseCICA'/std(crtPulseCICA));
                [R,~,~]=autocorrd(pulseTraceCICA(ind,:),1/mean(diff(crtTimeWin)));
                Rs(ind).CICA=mean(R.*R);
                %Rs(ind).W=wcICA(selectedIndex,:);
                Rs(ind).W=wcICA;
                Rs(ind).A=A;
                %if ind==5
                %   rankTest(crtRGB,crtTimeWin);
                %end
                %plot(ind,mean(R.*R),[col(6) 'o'],'MarkerSize',12,'Color',[.7 .6 .15]);
                
            end
            if strcmp(method,'MAICA')==1
                mu0 = 1;
                lambda0 = 1;
                gamma = 1e1;
                learningRate = 1e3;
                OverValue=1e-5;  maxIter = 100;
                threshold = 1;
                onlyAutocorr=false;
                if cICAFirstPass
                    cICAFirstPass=false;
                    [crtPulseMAICA,wMAICA,A]=multiobj(crtRGB, crtTimeWin, threshold, wMAICA, learningRate, mu0, lambda0, gamma, maxIter, OverValue,onlyAutocorr);
                else
                    [crtPulseMAICA,wMAICA,A]=multiobj(crtRGB, crtTimeWin, threshold, wMAICA, learningRate, mu0, lambda0, gamma, maxIter, OverValue,onlyAutocorr);
                end
                %[crtPulseCICA,wcICA]=GAMulti(crtRGB,crtTimeWin);
                %disp(wcICA.w3), disp(wcICA.w2);
                pulseTraceMAICA(ind,:)=smooth(crtPulseMAICA'/std(crtPulseMAICA));
                [R,~,~]=autocorrd(pulseTraceMAICA(ind,:),1/mean(diff(crtTimeWin)));
                Rs(ind).MAICA=mean(R.*R);
                %Rs(ind).W=wcICA(selectedIndex,:);
                Rs(ind).W=wMAICA;
                Rs(ind).A=A;
                %plot(ind,mean(R.*R),[col(6) 'o'],'MarkerSize',12,'Color',[.7 .6 .15]);
                
            end
            
            %pulseTraceICA(startInd:endInd)=pulseTraceICA(startInd:endInd)+crtPulseICA';
            %Scale and smooth it here itself
            
            
            %             figure(3);cla;
            %             plot(pulseTraceICA(i,:));
            %             pause(.001);
        end
        
        if DOFILTER
            % band pass filter
            crtTraceWinFilt = zeros(size(crtTraceWin));
            for k=1:size(crtTraceWin,1)
                crtTraceWinFilt(k,:) = filter(b,a,crtTraceWin(k,:)')';
            end
        else
            crtTraceWinFilt=crtTraceWin;
        end
        
        if(SHOWPLOTS)
            figure(4), plot(crtTimeWin, crtTraceWinFilt(2,:)), title('filtered green trace');
        end
        
        % Method 1: Green channel
        crtPulseRed = crtTraceWinFilt(1,:);
        crtPulseGreen = crtTraceWinFilt(2,:);
        crtPulseBlue = crtTraceWinFilt(3,:);
        
        if(SHOWPLOTS)
            figure(5), plot(crtTimeWin, crtPulseGreen), title('crtPulseGreen');
        end
        
        % Method 2: CHROM
        Rf = crtTraceWinFilt(1,:);
        Gf = crtTraceWinFilt(2,:);
        Bf = crtTraceWinFilt(3,:);
        Xf = 3*Rf-2*Gf;
        Yf = 1.5*Rf+Gf-1.5*Bf;
        
        alpha = std(Xf)/std(Yf);
        crtPulseChrom = Xf - alpha*Yf;
        %Save Rs of chrom as well for analysis
        crtPulseChromScaled=smooth(crtPulseChrom/std(crtPulseChrom));
        [R,~,~]=autocorrd(crtPulseChromScaled',1,1/mean(diff(crtTimeWin)));
        Rs(ind).Chrom=mean(R.*R);
        
        if(SHOWPLOTS)
            figure(6), plot(crtTimeWin, crtPulseChrom), title('crtPulseChrom');
        end
        
        % Method 5 : PCA
        RGB_input = [crtTraceWinFilt(1,:);crtTraceWinFilt(2,:);crtTraceWinFilt(3,:)];
        crtPulsePCA = PCA(RGB_input, Fs);
        
        % Method 6: G_R
        crtPulseG_R = crtTraceWinFilt(2,:)-crtTraceWinFilt(1,:);
        
        % Method 7: POS
        Rf = crtTraceWinFilt(1,:);
        Gf = crtTraceWinFilt(2,:);
        Bf = crtTraceWinFilt(3,:);
        S1 = Gf - Bf;
        S2 = Gf + Bf - 2*Rf;
                
        alpha = std(S1)/std(S2);
        crtPulsePOS = S1 + alpha*S2;
        
        if(SHOWPLOTS)
            figure(7), plot(crtTimeWin, crtPulsePCA), title('crtPulsePCA');
        end
        
        % compare PPG with pulse traces
        if(SHOWPLOTS && strcmp(dbase,'MMSE')~=1)
            fig8 = figure(8);
            clf(fig8), hold on, title('PPG vs rPPG (not final)'), xlim([timeTrace(startInd), timeTrace(endInd)]);
            p1 = plot(gtTime, gtTrace/max(gtTrace), 'b');
            p2 = plot(crtTimeWin, crtPulseGreen/max(crtPulseGreen), 'g');
            p3 = plot(crtTimeWin, crtPulseBlue/max(crtPulseBlue), 'c');
            p4 = plot(crtTimeWin, crtPulseRed/max(crtPulseRed), 'r');
            %p3 = plot(crtTimeWin, crtPulseChrom/max(crtPulseChrom), 'r');
            %p4 = plot(crtTimeWin, crtPulsePCA/max(crtPulsePCA), 'k');
            %legend([p1,p2,p3,p4],'PPG', 'Green', 'Chrom', 'PCA');
            %legend([p1,p3],'PPG', 'Chrom');
            hold off;
        end
        
        % Overlap add
        
        crtPulseGreen = smooth(crtPulseGreen)';
        crtPulseBlue = smooth(crtPulseBlue)';
        crtPulseRed = smooth(crtPulseRed)';
        crtPulseChrom = smooth(crtPulseChrom)';
        crtPulsePCA = smooth(crtPulsePCA)';
        crtPulseG_R = smooth(crtPulseG_R)';
        crtPulsePOS = smooth(crtPulsePOS)';
        
        pulseTraceGreen(startInd:endInd)=pulseTraceGreen(startInd:endInd)+crtPulseGreen;
        pulseTraceBlue(startInd:endInd)=pulseTraceBlue(startInd:endInd)+crtPulseBlue;
        pulseTraceRed(startInd:endInd)=pulseTraceRed(startInd:endInd)+crtPulseRed;
        pulseTraceChrom(startInd:endInd)=pulseTraceChrom(startInd:endInd)+crtPulseChrom;
        pulseTraceG_R(startInd:endInd)=pulseTraceG_R(startInd:endInd)+crtPulseG_R;
        pulseTracePOS(startInd:endInd)=pulseTracePOS(startInd:endInd)+crtPulsePOS;
        pulseTracePCA(:,startInd:endInd)=pulseTracePCA(:,startInd:endInd)+crtPulsePCA;
        
        
        %pulseTraceICA(:,startInd:endInd)=pulseTraceICA(:,startInd:endInd)+crtPulseICA;
        
        if(SHOWPLOTS && strcmp(dbase,'MMSE')~=1)
            fig9 = figure(9);
            clf(fig9), hold on, title('PPG vs rPPG - final'), xlim([0, timeTrace(endInd)]);
            p1 = plot(gtTime, gtTrace/max(gtTrace), 'b');
            %p2 = plot(timeTrace(1:endInd),  pulseTraceGreen(1:endInd)/max(pulseTraceGreen(1:endInd)), 'g');
            %p3 = plot(timeTrace(1:endInd),  pulseTraceBlue(1:endInd)/max(pulseTraceBlue(1:endInd)), 'c');
            %p4 = plot(timeTrace(1:endInd),  pulseTraceRed(1:endInd)/max(pulseTraceRed(1:endInd)), 'r');
            p5 = plot(timeTrace(1:endInd), pulseTraceChrom(1:endInd)/max(pulseTraceChrom(1:endInd)), 'r');
            %p6 = plot(timeTrace(1:endInd), pulseTracePCA(1:endInd)/max(pulseTracePCA(1:endInd)), 'k');
            %legend([p1,p2,p3,p4, p5, p6],'PPG', 'Green', 'Blue', 'Red', Chrom', 'PCA');
            %legend([p1,p5],'PPG', 'Chrom');
            hold off;
            pause();
        end
        ind=ind+1;
    end
end

% scale amplitudes
pulseTraceGreen = pulseTraceGreen/std(pulseTraceGreen);
pulseTraceBlue = pulseTraceBlue/std(pulseTraceBlue);
pulseTraceRed = pulseTraceRed/std(pulseTraceRed);
pulseTraceChrom = pulseTraceChrom/std(pulseTraceChrom);
pulseTracePCA = pulseTracePCA/std(pulseTracePCA);
pulseTraceG_R = pulseTraceG_R/std(pulseTraceG_R);
pulseTracePOS = pulseTracePOS/std(pulseTracePOS);


%pulseTraceGreen = smooth(pulseTraceGreen);
%pulseTraceBlue = smooth(pulseTraceBlue);
%pulseTraceRed = smooth(pulseTraceRed);
pulseTraceRGB=cat(2,pulseTraceRed,pulseTraceGreen,pulseTraceBlue);

%pulseTraceChrom = smooth(pulseTraceChrom);
%pulseTracePCA = smooth(pulseTracePCA);

% plot final traces
if(~silentMode && strcmp(dbase,'MMSE')~=1)
    figure(10),hold on;
    p1 = plot(gtTime, gtTrace, 'b', 'Linewidth', 2);
    p2 = plot(timeTrace, pulseTraceGreen, 'Color',[.4,.8,.4], 'Linewidth', 1.5);
    p3 = plot(timeTrace, pulseTraceBlue, 'Color',[.4,.4,.8],'Linewidth', 1.5);
    p4 = plot(timeTrace, pulseTraceRed, 'Color',[.8,.4,.4], 'Linewidth', 1.5);
    %p5 = plot(timeTrace, pulseTraceChrom, 'm', 'Linewidth', 2);
    %p6 = plot(timeTrace, pulseTracePCA, 'k');
    xlim([15 35]),
    %legend([p1, p2, p3, p4], 'PPG', 'Green', 'Chrom', 'PCA');
    %pause();
end;

if(SAVEFILES)
    % save to mat files
        suffix='';
        switch matFileName
            case 'rgbTraces_skin'
                suffix='_skin';
            case 'rgbTraces_crop'
                suffix='_crop';
            case 'rgbTraces_face'
                suffix='_face';
        end
%         if ~DOFILTER
%             filterSuffix='_Unfilt';
%         else
             filterSuffix='';
%         end
    pulseTrace = pulseTraceChrom;
    save([outFolder 'pulseTraceChrom' suffix filterSuffix '.mat'], 'pulseTrace', 'pulseTraceChrom','timeTrace');
    pulseTrace = pulseTraceRGB;
    save([outFolder 'pulseTraceRGB' suffix filterSuffix '.mat'], 'pulseTrace','pulseTraceRGB', 'timeTrace');
    pulseTrace = pulseTraceGreen;
    save([outFolder 'pulseTraceGreen' suffix filterSuffix '.mat'], 'pulseTrace', 'pulseTraceGreen','timeTrace');
    pulseTrace = pulseTraceG_R;
    save([outFolder 'pulseTraceG_R' suffix filterSuffix '.mat'], 'pulseTrace', 'pulseTraceG_R','timeTrace');
    pulseTrace = pulseTracePOS;
    save([outFolder 'pulseTracePOS' suffix filterSuffix '.mat'], 'pulseTrace', 'pulseTracePOS','timeTrace');
    pulseTrace = pulseTracePCA;
    save([outFolder 'pulseTracePCA' suffix filterSuffix '.mat'], 'pulseTrace', 'timeTrace');
    if ICAWIN==true
        if strcmp(method,'ICA')==1
            pulseTrace = pulseTraceICA;
            traceName='pulseTraceICA';
        end
        if strcmp(method,'CICA')==1
            pulseTrace = pulseTraceCICA;
            traceName='pulseTraceCICA';
        end
        if strcmp(method,'MAICA')==1
            pulseTrace = pulseTraceMAICA;
            traceName='pulseTraceMAICA';
        end
        if WINDOW_LENGTH_SEC==-1
            save([outFolder traceName suffix filterSuffix '.mat'], 'pulseTrace', 'timeTrace','Rs');
        else
            save([outFolder traceName '-win' num2str(WINDOW_LENGTH_SEC)...
                suffix filterSuffix '.mat'], 'pulseTrace', 'timeTrace','Rs','-append');
        end
        %         figure(11);
        %         legend('ICA','ICA1','ICA2','ICA3','CICA')
    end
end
end
