function [HR_sensor,HR_PPG,HR_RPPG,HR_RPPG_filtered,excel_row]=getHRFromPulse(varargin)
% Extract HR from pulse trace
% sliding window based max peak PSD
% arguments can be path of mat file with pulse
% yannick.benezeth@u-bourgogne.fr
% 18/02/2016

%clear all;
close all;
%clc;

addpath('../BlandAltman/');
addpath('../tools/DP/');

%disp('Get HR from pulse trace');

%%%%%%%%%%%
% Parameters
%%%%%%%%%%%
WINDOW_LENGTH_SEC = 20; % length of the sliding window (in seconds)
STEP_SEC = 0.1; % step between 2 sliding window position (in seconds)
LOW_F=.7; % low freq for PSD range
UP_F=3; % high freq for PSD range
BEGIN_LENGTH_SEC = 0; % Begin after x seconds
SAVEFILES = 0;
NBMAXCANDIDATES = 3 ; % for post-proc
ICAWIN=false;
%matFileName = 'pulseTrace2SR';
matFileName = 'pulseTraceChrom_skin';
%matFileName = 'pulseTraceGreen';
%matFileName = 'pulseTraceCICA';
% vidFolder = './data/RichardSummerVideos/';
% vidFolder ='/run/media/richard/Le2iHDD/IUT/subject41/vid2/';
vidFolder='D:/IUT/subject26/vid2/';

% postProcMethod gives post processing method, can be
% 'DP' for Dynamic Programing
% 'Heuristic'
% 'Kalman'
% 'none'
postProc = 'Kalman';

%nameFolder = 'roberto1-ueye';
suffix='';
keyword='';
i = 1;
silentMode=false;
IUTMode=true;
DOFILTER=true;
dbase='IUT';
while(nargin > i)
    if(strcmp(varargin{i},'vidFolder')==1)
        vidFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'matFileName')==1)
        matFileName=varargin{i+1};
    end
    if(strcmp(varargin{i},'WINDOW_LENGTH_SEC')==1)
        WINDOW_LENGTH_SEC=varargin{i+1};
    end
    if(strcmp(varargin{i},'nameFolder')==1)
        nameFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'keyword')==1)
        keyword=varargin{i+1};
    end
    if(strcmp(varargin{i},'silent')==1)
        silentMode=varargin{i+1};
    end
    if(strcmp(varargin{i},'database')==1)
        dbase=varargin{i+1};
    end
    if(strcmp(varargin{i},'ICAWIN')==1)
        ICAWIN=varargin{i+1};
    end
    if(strcmp(varargin{i},'DOFILTER')==1)
        DOFILTER=varargin{i+1};
    end
    if(strcmp(varargin{i},'postProc')==1)
        postProc=varargin{i+1};
    end
    if(strcmp(varargin{i},'method')==1)
        method=varargin{i+1};
    end
    if(strcmp(varargin{i},'freqRange')==1)
        freqRange=varargin{i+1};
        if numel(freqRange)==2
            LOW_F=freqRange(1);
            UP_F=freqRange(2);
        else
            warning('Frequency range has to be a 2 element vector!');
        end
    end
    i = i+2;
end

%Build suffix from matFileName
suffix=matFileName(end-3:end);
if strcmp(suffix,'crop')~=1 && strcmp(suffix,'skin')~=1
    suffix='';
end
if ~isempty(suffix)
    suffix=['_' suffix];
    matFileName=matFileName(1:end-5);
elseif strcmp(keyword,'crop')==1 ||strcmp(keyword,'skin')==1
    suffix=['_' keyword];
end


if ~exist('nameFolder','var')
    %Extract nameFolder from the vidFolder
    tokens=strsplit(vidFolder,'/');
    if ~isempty(tokens{end})
        nameFolder=tokens{end};
    else
        nameFolder=tokens{end-1};
        %For IUT videos use this:
        if strcmp(dbase,'IUT')==1
            nameFolder=[tokens{end-2} '/' tokens{end-1}];
        end
    end
end
if ~DOFILTER
    filterSuffix='_Unfilt';
else
    filterSuffix='';
end

if ICAWIN==true
    %Check if we are working with skin or crop
    if strcmp(suffix,'_skin')==1 || strcmp(suffix,'_crop')==1
        pulseFile = [vidFolder matFileName '-win' num2str(WINDOW_LENGTH_SEC) suffix filterSuffix '.mat'];
        %pulseFile = [vidFolder matFileName(1:end-4) '-win' num2str(WINDOW_LENGTH_SEC) '_' matFileName(end-3:end) filterSuffix '.mat'];
    else
        pulseFile = [vidFolder matFileName '-win' num2str(WINDOW_LENGTH_SEC) filterSuffix '.mat'];
    end
    
else
   pulseFile = [vidFolder 'pulseTrace'  method '.mat'];
end


% disp(WINDOW_LENGTH_SEC);
% disp(vidFolder);
disp(['get HR from pulse with  ' pulseFile]);

if(exist(pulseFile, 'file') == 2)
    load(pulseFile);
else
    disp('oops, no input file');
    return;
end
welchData=[];
%Check if the name starts with pulseTraceRGB
% if numel(matFileName)>15
%     if strcmp(matFileName(1:15),'pulseTraceGreen')==1
%         pulseTrace=pulseTraceGreen;
%     end
%     if strcmp(matFileName(1:15),'pulseTraceChrom')==1
%         pulseTrace=pulseTraceChrom;
%     end
% end
traceSize = length(timeTrace);

% get exact Fs
Fs = 1/mean(diff(timeTrace));

% load PPG
% TODO: in case of mat file in argument of the function -> extract the name
% of the folder from the string : see line 31 to 38
[gtTrace, gtHR, gtTime] = loadPPG(vidFolder,'database',dbase);
gtTime = gtTime / 1000;  % in seconds

if strcmp(dbase,'MMSE')==1
    %Interpolate gtTime and gtHR to timeTrace
    gtHR=interp1(gtTime,gtHR,timeTrace,'spline');
end

%Kalman heuristic parameters
isTrackInitialized=false;
trendTolerance=10; %Size of window to calculate trend and trend difference
threshold=10;


% figure(1),plot(gtTime, gtTrace, 'b', timeTrace, pulseTrace, 'g', 'Linewidth', 2);
% legend('PPG', 'Pulse trace'),
% title('PPG and Pulse trace');

winLength = round(WINDOW_LENGTH_SEC*Fs); % length of the sliding window for FFT
step = round(STEP_SEC*Fs);
if(BEGIN_LENGTH_SEC == 0)
    beg = 1;
else
    beg = round(BEGIN_LENGTH_SEC*Fs);
end

% figure(2),plot(gtTime, gtTrace, 'b', timeTrace, pulseTrace, 'g', 'Linewidth', 2);
% legend('PPG', 'Pulse trace'),
% title('PPG and Pulse trace');
% pause(1);
secHeuristic = 0;

ind = 1;
halfWin = (winLength/2);
for i=halfWin:step:traceSize-halfWin
    
    %     if(mod(i,10)==0)
    %         disp(['processing window ' num2str(i) ' on ' num2str(traceSize-winLength)]);
    %     end
    
    % get start/end index of current window
    startInd = i-halfWin+1;
    endInd = i+halfWin;
    startTime = timeTrace(startInd);
    endTime = timeTrace(endInd);
    
    % get current pulse window
    if ICAWIN==true
        if ind>size(pulseTrace,1)
            break;
        end
        crtPulseWin = pulseTrace(ind,:);
        if strcmp(dbase,'MMSE')==1
            HR_PPG(ind)=median(gtHR(startInd:endInd));
        end
    else
        crtPulseWin = pulseTrace(startInd:endInd);
        if strcmp(dbase,'MMSE')==1
            HR_PPG(ind)=mean(gtHR(startInd:endInd));
        end
    end
    crtTimeWin = timeTrace(startInd:endInd);
    
    % get current PPG window
    [~, startIndGt] = min(abs(gtTime-startTime));
    [~, endIndGt] = min(abs(gtTime-endTime));
    
    
    crtTimePPGWin = gtTime(startIndGt:endIndGt);
    
    % get exact PPG Fs
    Fs_PPG = 1/mean(diff(crtTimePPGWin));
    if strcmp(dbase,'MMSE')~=1
        crtPPGWin = gtTrace(startIndGt:endIndGt);
    end
    
    %%%% resampling for BayesHeart
    %     [P,Q] = rat(30/Fs_PPG);
    %
    %     crtPPGWin = resample(crtPPGWin,P,Q);
    %     crtTimePPGWin = (crtTimePPGWin(1):1/30:crtTimePPGWin(end))';
    %
    %     if(size(crtTimePPGWin,1) ~= size(crtPPGWin,1))
    %        crtTimePPGWin = [crtTimePPGWin; crtTimePPGWin(end) + 1/30];
    %     end
    %     [P,Q] = rat(30/Fs);
    %
    %     crtPulseWin = resample(crtPulseWin,P,Q);
    %     crtTimeWin = (crtTimeWin(1):1/30:crtTimeWin(end))';
    %
    %     while(size(crtTimeWin,1) ~= size(crtPulseWin,1))
    %         crtTimeWin = [crtTimeWin; crtTimeWin(end) + 1/30];
    %     end
    %%%% END resampling
    %     figure(2),plot(crtTimePPGWin, crtPPGWin, 'b', crtTimeWin, crtPulseWin, 'g', 'Linewidth', 2);
    %     legend('PPG', 'Pulse trace'),
    %     title('PPG and Pulse trace');
    %     pause();
    
    %% Spectral analysis
    
    %%%%%%%%%%%%
    % with FFT
    %%%%%%%%%%%
    % on rPPG
    x = crtPulseWin;
    N = length(x)*3;
    rPPG_freq = [0 : N-1]*Fs/N;
    rPPG_power = abs(fft(x,N)).^2;
         
    if strcmp(dbase,'MMSE')~=1
        % on PPG
        x = crtPPGWin;
        N = length(x)*3;
        PPG_freq = [0 : N-1]*Fs_PPG/N;
        PPG_power = abs(fft(x,N)).^2;
    end
   
    
    
    %     PPG
%     if(length(crtPPGWin) > 0) % sometime there is no PPG
%         [PPG_power,PPG_freq] = pwelch(crtPPGWin,[],[],length(crtPPGWin)*3,Fs_PPG, 'psd');
%         rangePPG = (PPG_freq>LOW_F & PPG_freq < UP_F); % frequency range to find PSD peak
%         PPG_power = PPG_power(rangePPG);
%         PPG_freq = PPG_freq(rangePPG);
%         [pksPPG,loc] = findpeaks(PPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks
%         PPG_peaksLoc = PPG_freq(loc);
%     end

    
    
    %
    %     fig3 = figure(3);
    %     clf(fig3);
    %     subplot(1,2,1), hold on; grid on;
    %     plot(rPPG_freq, rPPG_spec), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
    %     title('rPPG FFT')
    %     hold off;
    %     subplot(1,2,2); hold on; grid on;
    %     plot(PPG_freq, PPG_spec), xlim([0.3 3.5]), xlabel('Heart Rate (Hz)');
    %     title('PPG FFT');
    %     hold off;
    
    
    %%%%%%%%%%%%
    % with periodogram function
    %%%%%%%%%%%
    %         [rPPG_power,rPPG_freq] = periodogram(crtPulseWin,hamming(length(crtPulseWin)),[],Fs,'power');
    %         [PPG_power,PPG_freq] = periodogram(crtPPGWin,hamming(length(crtPPGWin)),[],Fs_PPG,'power');
    
    %     figure(4),
    %     subplot(1,2,1),  grid
    %     plot(rPPG_freq,rPPG_power)
    %     xlim([0.3 3.5]), title('rPPG periodogram'), xlabel('Heart Rate (Hz)')
    %     subplot(1,2,2), grid
    %     plot(PPG_freq,PPG_power)
    %     xlim([0.3 3.5]), title('PPG periodogram'), xlabel('Heart Rate (Hz)');
    
    
    %%%%%%%%%%%%
    % with Welch
    %%%%%%%%%%%
    %     RPPG
%     [rPPG_power,rPPG_freq] = pwelch(crtPulseWin,[],[],length(crtPulseWin)*3,Fs, 'psd'); % zero pading to increase the nb of points in the PSD
%     rangeRPPG = (rPPG_freq>LOW_F & rPPG_freq < UP_F);   % frequency range to find PSD peak
%     rPPG_power = rPPG_power(rangeRPPG);
%     rPPG_freq = rPPG_freq(rangeRPPG);
%     [pksrPPG,loc] = findpeaks(rPPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks (only one is used now but will be useful for postpreocessing
%     rPPG_peaksLoc = rPPG_freq(loc);
%     
%     
%     
%     
%     %     PPG
%     if(length(crtPPGWin) > 0) % sometime there is no PPG
%         [PPG_power,PPG_freq] = pwelch(crtPPGWin,[],[],length(crtPPGWin)*3,Fs_PPG, 'psd');
%         rangePPG = (PPG_freq>LOW_F & PPG_freq < UP_F); % frequency range to find PSD peak
%         PPG_power = PPG_power(rangePPG);
%         PPG_freq = PPG_freq(rangePPG);
%         [pksPPG,loc] = findpeaks(PPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks
%         PPG_peaksLoc = PPG_freq(loc);
%     end

    % Get peaks rPPG
    rangeRPPG = (rPPG_freq>LOW_F & rPPG_freq < UP_F);   % frequency range to find PSD peak
    rPPG_power = rPPG_power(rangeRPPG);
    rPPG_freq = rPPG_freq(rangeRPPG);
    [pksrPPG,loc] = findpeaks(rPPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks (only one is used now but will be useful for postpreocessing
    rPPG_peaksLoc = rPPG_freq(loc);

    if strcmp(dbase,'MMSE')~=1
        % Get peaks PPG
        rangePPG = (PPG_freq>LOW_F & PPG_freq < UP_F); % frequency range to find PSD peak
        PPG_power = PPG_power(rangePPG);
        PPG_freq = PPG_freq(rangePPG);
        [pksPPG,loc] = findpeaks(PPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks
        PPG_peaksLoc = PPG_freq(loc);
    else
        %Create a gaussian around the PPG freq
    end
    tfrFull(:,ind)=rPPG_power;
    % get SNR for evaluation
    WIDTH = 0.15;
    if strcmp(dbase,'MMSE')==1
        %No gtTrace, use rPPG itself for SNR calculation
        if numel(rPPG_peaksLoc)>0
            range1 = (rPPG_freq>(rPPG_peaksLoc(1)-WIDTH/2) & rPPG_freq < (rPPG_peaksLoc(1)+WIDTH/2));
            range2 = (rPPG_freq>(rPPG_peaksLoc(1)*2-WIDTH/2) & rPPG_freq < (rPPG_peaksLoc(1)*2+WIDTH/2));
        else
            
        end
        
    else
        range1 = (rPPG_freq>(PPG_peaksLoc(1)-WIDTH/2) & rPPG_freq < (PPG_peaksLoc(1)+WIDTH/2));
        range2 = (rPPG_freq>(PPG_peaksLoc(1)*2-WIDTH/2) & rPPG_freq < (PPG_peaksLoc(1)*2+WIDTH/2));
    end
    
    range = range1 + range2;
    signal = rPPG_power.*range;
    noise = rPPG_power.*(~range);
%     figure(1),
%     cla;
%     hold on;
%     plot(rPPG_freq,noise,'r')
%     plot(rPPG_freq,signal,'b')
%     hold off;
%     pause(0.1)

    n = sum(noise);
    s = sum(signal);
    snr(ind) = 10*log10(s/n);
    
       
    %%%%%%%%%%%%%%%%%
    % With BayesHeart
    %%%%%%%%%%%%%%%%%
    
    %     if(i==beg)
    %         % PPG
    %         end5seconds = timeTrace(round(5*Fs));
    %         [~, endInd5sec] = min(abs(gtTime-end5seconds));
    %
    %         disp('PPG');
    %         [TRANSITION_2_PPG,EMISSION_2_PPG,TRANSITION_4_PPG,EMISSION_4_PPG] = getProbabilities(crtTimePPGWin,crtPPGWin,1,[],[],[],[]);
    %         [~, numberStatesPPG] = getObservationFromSignal(crtTimePPGWin,crtPPGWin,TRANSITION_2_PPG,EMISSION_2_PPG,TRANSITION_4_PPG,EMISSION_4_PPG,0);
    % %         TRANSITION_2_PPG,EMISSION_2_PPG,TRANSITION_4_PPG,EMISSION_4_PPG
    %
    %         % rPPG
    %         disp('rPPG');
    %         [TRANSITION_2,EMISSION_2,TRANSITION_4,EMISSION_4] = getProbabilities(crtTimeWin,crtPulseWin,2,[],[],[],[]);
    %         [~, numberStates] = getObservationFromSignal(crtTimeWin,crtPulseWin,TRANSITION_2,EMISSION_2,TRANSITION_4,EMISSION_4,0);
    % %         TRANSITION_2,EMISSION_2,TRANSITION_4,EMISSION_4
    %     end
    %
    % 	% PPG
    %     [HR_PPG(ind),~] = getObservationFromSignal(crtTimePPGWin,crtPPGWin,TRANSITION_2_PPG,EMISSION_2_PPG,TRANSITION_4_PPG,EMISSION_4_PPG,numberStatesPPG);
    %     % rPPG
    %     numberStates = 4;
    %     [HR_RPPG(ind),~] = getObservationFromSignal(crtTimeWin,crtPulseWin,TRANSITION_2,EMISSION_2,TRANSITION_4,EMISSION_4,numberStates);
    %     HR_RPPG_filtered(ind) = HR_RPPG(ind);
    
    %     fprintf('rPPG HR: %.2f \t',rPPG_peaksLoc(1)*60);
    %     fprintf('PPG HR: %.2f \n',PPG_peaksLoc(1)*60);
    if ~silentMode
        fig5 = figure(5);
        clf(fig5);
        subplot(1,2,1);
        hold on;grid on;
        plot(rPPG_freq,rPPG_power);
        if ~isempty(pksrPPG)
            plot(rPPG_peaksLoc(1), pksrPPG(1), 'r*');
        end
        xlim([0.1 3.5]), title('rPPG Welch periodogram'), xlabel('Heart Rate (Hz)');
        hold off;
        if strcmp(dbase,'MMSE')~=1
            subplot(1,2,2);
            hold on; grid on;
            plot(PPG_freq,PPG_power)
            plot(PPG_peaksLoc(1), pksPPG(1), 'r*');
            xlim([0.1 3.5]), title('PPG Welch periodogram'), xlabel('Heart Rate (Hz)');
            hold off;
        end
    end
    
    %%%%%%%%%%%%
    % with Lomb method
    %%%%%%%%%%%%
    %         [rPPG_freq,rPPG_power,prob1] = lomb(crtTimeWin',crtPulseWin,4,1);
    %         [PPG_freq,PPG_power,prob2] = lomb(crtTimePPGWin,crtPPGWin,4,1);
    
    %%%%%%%%%%%%
    % with Multitaper method
    %%%%%%%%%%%%
    %         [rPPG_power,rPPG_freq] = pmtm(crtPulseWin,[],[], Fs);
    %         [PPG_power,PPG_freq] = pmtm(crtPPGWin,[], [], Fs_PPG);
    
    %         [pksrPPG,loc] = findpeaks(rPPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks (only one is used now but will be useful for postpreocessing
    %         rPPG_peaksLoc = rPPG_freq(loc);
    
    %         [pksPPG,loc] = findpeaks(PPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks
    %         PPG_peaksLoc = PPG_freq(loc);
    %
    %     figure(6),
    %     subplot(1,2,1),  grid
    %     plot(rPPG_freq,rPPG_power)
    %     xlim([0.3 3.5]), title('rPPG PMTM periodogram'), xlabel('Heart Rate (Hz)')
    %     subplot(1,2,2), grid
    %     plot(PPG_freq,PPG_power)
    %     xlim([0.3 3.5]), title('PPG PMTM periodogram'), xlabel('Heart Rate (Hz)');
    
    % save final results
    crtTime =  (startTime + endTime)/2;
    time(ind) = crtTime;
    if isempty(welchData)
        welchData=struct('rPPG_freq',{},'rPPG_power',{});
    end
    welchData(ind).rPPG_freq=rPPG_freq;
    welchData(ind).rPPG_power=rPPG_power;
    if(~isempty(gtTime) && strcmp(dbase,'MMSE')~=1) % sometime there is no PPG
        HR_PPG(ind) = PPG_peaksLoc(1)*60;
    end
    if ~isempty(pksrPPG)
        HR_RPPG(ind) = rPPG_peaksLoc(1)*60;
        HR_RPPG_filtered(ind) = rPPG_peaksLoc(1)*60;
    else
        if ind >1
            HR_RPPG(ind)= HR_RPPG(ind-1);
            HR_RPPG_filtered(ind)= HR_RPPG_filtered(ind-1);
        else
            %First RPPG win has no peak
            HR_RPPG(ind)= 0;
            HR_RPPG_filtered(ind)=0;
        end
    end
    
    %Predict peak based on previous trend
    %If diff between actual and predicted peak is large,
    %Choose the peak closest to the predicted peak
    
    %Heuristic based on kalman filter
    if ~isTrackInitialized
        if HR_RPPG(ind)~=0
            kalmanFilter= configureKalmanFilter('ConstantVelocity',HR_RPPG(1), [1 1]*1e0, [1,1], 1e1);
            isTrackInitialized=true;
        end
    else
        
        if ind >1
            %Reconfigure kalman to compensate for changing HR
            if mod(ind,6)==0
                kalmanFilter= configureKalmanFilter('ConstantVelocity',HR_RPPG_filtered(ind-1), [1 1]*1e0, [1,1], 1e1);
            end
            currDiff=abs(HR_RPPG_filtered(ind-1)-HR_RPPG(ind));
            trendDiff=currDiff;
            
            if ind>trendTolerance
                trend=mean(HR_RPPG_filtered(ind-trendTolerance:ind-1));
                trendDiff=abs(trend-HR_RPPG(ind));
                %stem(ind,trendDiff,'b+');
            end
            
            if(currDiff > threshold && trendDiff>threshold)
                %Also check if the new values are consistent
                if ind>trendTolerance/2
                    newTrend=mean(HR_RPPG(ind-round(trendTolerance/3):ind-1));
                    newTrendDiff=abs(newTrend-HR_RPPG(ind));
                    if(newTrendDiff<threshold)
                        %Seems the new values are consistent, choose, and
                        %update kalman
                        HR_RPPG_filtered(ind)=HR_RPPG(ind);
                        predict(kalmanFilter);
                        correct(kalmanFilter,HR_RPPG_filtered(ind));
                        
                    else
                        %New value is very different from previous as well as trend
                        %predict
                        HR_RPPG_filtered(ind)=predict(kalmanFilter);
                        %Probability of HR being less than 50 is quite
                        %low. Maybe prediction is on the wrong trend
                        if HR_RPPG_filtered(ind)<50 && HR_RPPG(ind)>50
                            HR_RPPG_filtered(ind)=HR_RPPG(ind);
                            predict(kalmanFilter);
                            correct(kalmanFilter,HR_RPPG_filtered(ind));
                        end
                    end
                else
                    if ind>trendTolerance
                        %New value is very different from previous as well as trend
                        %predict
                        HR_RPPG_filtered(ind)=predict(kalmanFilter);
                        %Probability of HR being less than 50 is quite
                        %low. Maybe prediction is on the wrong trend
                        if HR_RPPG_filtered(ind)<50 && HR_RPPG(ind)>50
                            HR_RPPG_filtered(ind)=HR_RPPG(ind);
                            predict(kalmanFilter);
                            correct(kalmanFilter,HR_RPPG_filtered(ind));
                        end
                    else
                        %Not sure about the values, reconfigure kalman with
                        %the current value
                        kalmanFilter= configureKalmanFilter('ConstantVelocity',HR_RPPG_filtered(ind), [1 1]*1e1, [1,1], 1e1);
                    end
                end
            elseif currDiff>threshold && trendDiff<threshold
                %New value is close to the trend
                HR_RPPG_filtered(ind)=HR_RPPG(ind);
                %predict(kalmanFilter);
                correct(kalmanFilter,HR_RPPG_filtered(ind));
            elseif currDiff<threshold && trendDiff>threshold
                %New value is close to previous value,wrong trend
                %correct
                HR_RPPG_filtered(ind)=HR_RPPG(ind);
                predict(kalmanFilter);
                correct(kalmanFilter,HR_RPPG_filtered(ind));
            else
                HR_RPPG_filtered(ind)=HR_RPPG(ind);
                %predict(kalmanFilter);
                correct(kalmanFilter,HR_RPPG_filtered(ind));
            end
        end
    end
    
    
    % Will be used during post-processing
    for j=1:NBMAXCANDIDATES
        if j>length(rPPG_peaksLoc)
            tfr(ind,j,1) = 0;
            tfr(ind,j,2) = 0;
        else
            tfr(ind,j,1) = rPPG_peaksLoc(j)*60;
            tfr(ind,j,2) = pksrPPG(j);
        end
    end
    
    % get current PPG window
    if(~isempty(gtTime)) % sometime there is no PPG
        [~, crtTimeInd] = min(abs(gtTime-crtTime));
        if strcmp(dbase,'MMSE')~=1
            HR_sensor(ind) = gtHR(crtTimeInd);
        else
            HR_sensor(ind)=HR_PPG(ind);
        end
        
    end
    
    if ~silentMode
        pause(0.1);
    end
    ind=ind+1;
end

%%%%%%%%%%%%%%%%%%
% Post processing....
switch postProc
    case 'none'
        HR_RPPG_filtered = HR_RPPG;
    case 'Kalman'
        HR_RPPG_filtered = HR_RPPG_filtered;
    case 'DP'
        filterSuffix=[filterSuffix '-' 'DP'];
        rahrt_data_inst=rahrt_data(tfr, STEP_SEC);
        i=size(tfr,1);
        result=rahrt(i,rahrt_data_inst);
        
        % we reconstruct the optimal path given result.q
        % bestPath(1,:) -> Hz
        % bestPath(2,:) -> Nrj
        [~, idx]= max(result.D(:,i)) ;
        bestPath(1,i) = result.locs(idx,i);
        bestPath(2,i) = result.pks(idx,i);
        nextIdx = result.q(idx,i);
        for k=size(tfr,1)-1:-1:1
            idx = nextIdx;
            nextIdx = result.q(idx,k);
            bestPath(1,k) = result.locs(idx,k);
            bestPath(2,k) = result.pks(idx,k);
        end
        
        HR_RPPG_filtered = bestPath(1,:);
        
        if(0)      
            figure(1);
            hold on
            xlabel('T');
            ylabel('F');
            zlabel('abs(TFR)');
            figure(1), mesh(time,rPPG_freq,tfrFull),axis xy,colormap('jet'),colorbar, xlabel('Time (s)'), ylabel('Frequency (Hz)'),xlim([time(1) time(end)]), ylim([rPPG_freq(1) rPPG_freq(end)]);
            view(-50,70);
            hold on;
            plot3(time,bestPath(1,:)/60,bestPath(2,:),'r^','MarkerSize',10)
            plot3(time,tfr(:,1,1)/60,tfr(:,1,2),'b*','MarkerSize',10)
            pause()
        end


        
    case 'Heuristic'
        % Homemade heuristic
        % If diff between actual and predicted peak is large,
        % Choose the 2nd peak
        HR_RPPG_filtered = [];
        HR_RPPG_filtered(1)=HR_RPPG(1);
        for i=2:length(HR_RPPG)
            HR_RPPG_filtered(i)=HR_RPPG(i);
            d = abs(HR_RPPG_filtered(i)-HR_RPPG_filtered(i-1));
            if(d > 10)
                new_HR = tfr(i,2,1); % 2nd peak
                new_d = abs(new_HR-HR_RPPG_filtered(i-1));
                if(new_d < d && secHeuristic < 10)
                    HR_RPPG_filtered(i) = new_HR;
                    secHeuristic = secHeuristic + 1;
                else
                    secHeuristic = 0;
                end
            end
        end
end

%%%%%%%%%%%%%%%%%%
% Save results....
method=matFileName(11:end);
resultFolder = [vidFolder 'results/'];
if (exist(resultFolder, 'dir') == 0)
    mkdir(resultFolder);
end

if(~isempty(gtTime))
    %Metrics
    method=matFileName(11:end);
    metric_suffix = ['result_' method];
    
    Array_Difference = abs(HR_PPG-HR_RPPG_filtered);
    Selected_Array_Difference_2_5 = Array_Difference(Array_Difference <= 2.5);
    Selected_Array_Difference_5 = Array_Difference(Array_Difference <= 5);
    
    Estimation_percentage_2_5 = double(length(Selected_Array_Difference_2_5))/length(HR_RPPG_filtered);
    Estimation_percentage_5 = double(length(Selected_Array_Difference_5))/length(HR_RPPG_filtered);
    
    M = length(Selected_Array_Difference_5);
    
    MAE = mean(Array_Difference);
    RMSE = sqrt((1/M)*sum((Array_Difference).^2));
    MAE_5 = mean(Selected_Array_Difference_5);
    
    meanSNR = mean(snr);
    
    %disp(['Estimation_percentage_2_5 : ' num2str(Estimation_percentage_2_5)]);
    %disp(['Estimation_percentage_5 : ' num2str(Estimation_percentage_5)]);
    disp(['MAE : ' num2str(MAE)]);
    disp(['SNR : ' num2str(meanSNR)]);
    %disp(['MAE_5 : ' num2str(MAE_5)]);
    %disp(['RMSE : ' num2str(RMSE)]);
    disp(' ');
    %pause();
    
    save([resultFolder metric_suffix filterSuffix '.mat' ],'Estimation_percentage_2_5','Estimation_percentage_5','MAE','MAE_5','RMSE','meanSNR', 'HR_PPG','HR_RPPG_filtered');
end

% save temporal traces
if(SAVEFILES || ~silentMode)
    fig8 = figure(8);
    clf(fig8);
    hold on;
    grid on;
    p1 = plot(time, HR_sensor, 'b-*');
    p2 = plot(time, HR_PPG, 'g-*');
    p3 = plot(time, HR_RPPG, 'c-*');
    p4 = plot(time, HR_RPPG_filtered, 'r-*');
    legend([p1, p2, p3, p4], 'HR sensor', 'HR PPG', 'HR rPPG', 'HR rPPG filtered');
    title('raw results HR estimation');
    
    title(strrep(method, '_', ' '));
    xlabel('seconds'), ylabel('Heart Rate (bpm)');
    ylim([40 210]);
    hold off;
    
    if(SAVEFILES)
        saveas(gcf,[resultFolder method suffix '.png']);
    end
    if(silentMode)
        close(fig8);
    else
        pause(1);
    end
end

% save Bland Altman Plots
if(~isempty(gtTime))
    
    gnames = {'HR analysis'}; % names of groups in data {dimension 1 and 2}
    %corrinfo = {'n','SSE','r2','eq'}; % stats to display of correlation scatter plot
    corrinfo = {'n','r','eq'}; % stats to display of correlation scatter plot
    BAinfo = {'RPC(%)'}; % stats to display onavg Bland-ALtman plot
    limits = 'auto';%[50 120]; % how to set the axes limits
    symbols = 'Num'; % symbols for the data sets (default)
    colors = [.75, .5, .3]; % colors for the data sets
    tit='';
    label = {'EstimatedHR', 'HR'};
    %fig=figure('Name','Bland Altman Analysis');
    [cr, fig, statsStruct] = BlandAltman(HR_RPPG_filtered(1,:),HR_PPG(1,:),label,tit,gnames,corrinfo,BAinfo,limits,colors,symbols);
    %set (fig, 'Units', 'normalized', 'Position', [0,0,1,1]);
    str=['PRECIS 2.5 : ' num2str(Estimation_percentage_2_5) ' | PRECIS 5 : ' num2str(Estimation_percentage_5) ' | MAE : ' num2str(MAE) ];
    
    a=annotation('textbox', [0.2 0.85 .1 .1], 'FitHeightToText', 'ON', 'Fontsize', 12, ...
        'String',str );
    %Center annotation
    %     set(a,'Units','pixels');
    %     set(fig,'Units','pixels');
    %     aPos=get(a,'Position');
    %     figPos=get(fig,'Position');
    %     set(a,'Position',[(figPos(3)-aPos(3))/2 aPos(2) aPos(3) aPos(4)]);
    %
    
    excel_row={round(statsStruct.SSE,4) round(statsStruct.r,4)...
        round(Estimation_percentage_2_5,4) round(Estimation_percentage_5,4)...
        round(MAE,4) round(MAE_5,4) round(RMSE,4) round(statsStruct.CR) ...
        round(meanSNR,4)};
    excel_row_disp=[statsStruct.SSE statsStruct.r Estimation_percentage_2_5 Estimation_percentage_5 MAE MAE_5 RMSE round(statsStruct.CR) round(meanSNR,4)]
    
    if ~ICAWIN
        saveas(fig,[resultFolder method suffix filterSuffix '_BA.png']);
        save([vidFolder 'pulseTrace' method suffix filterSuffix],'pulseTrace','timeTrace',...
            'HR_sensor','HR_PPG','HR_RPPG','HR_RPPG_filtered','welchData');
    else
        %saveas(fig,[resultFolder method '_win' num2str(WINDOW_LENGTH_SEC) suffix  filterSuffix '_BA.png']);
        %save([vidFolder 'pulseTrace' method '-win' num2str(WINDOW_LENGTH_SEC) ...
        %    suffix filterSuffix],'pulseTrace','timeTrace',...
        %    'HR_sensor','HR_PPG','HR_RPPG','HR_RPPG_filtered','welchData','-append');
%           'HR_sensor','HR_PPG','HR_RPPG','HR_RPPG_filtered','welchData','Rs');
        
    end
    %Save the results for global BA later
    
    
end
if silentMode==true
    close all
    pause(0.01);
    %excel_row_disp
end
