function forAllVideosCA(method,ICAWIN,varargin)

%% Stuff for writing to spreadsheet
addpath('../tools/xlwrite');
addpath('../tools');
initxlWrite('../tools/xlwrite');

addpath('../main-stable/');
%% Data Generation for XLSX: IMP to verify this!!!!
% Define an xls name
fileName = [method 'newresults.xlsx'];
sheetName = 'SkinThresh=-1';
startRange = 'B4';
dbase='MMSE';
saveExcel=true;
i=3;
countFrames=false;
DOFILTER=true;
WINDOW_LENGTH_SEC=30;
while(nargin > i)
    if(strcmp(varargin{i-2},'database')==1)
        dbase=varargin{i+1-2};
    end
    if(strcmp(varargin{i-2},'saveExcel')==1)
        saveExcel=varargin{i+1-2};
    end
    if(strcmp(varargin{i-2},'countFrames')==1)
        countFrames=varargin{i+1-2};
    end
    i=i+2;
end
totalLe2iFrames=0;
totalIUTFrames=0;

%% Le2i dataset
if strcmp(dbase,'LE2I')
    Le2iFolder='D:\Dataset\HeartRate\RichardSummerVideos\';
    %cd(Le2iFolder);
    dirs=dir([Le2iFolder '/*gt']);
    d=dir([Le2iFolder '/aft*']);
    dirs=[dirs;d];
    %Iterate through all directories, perform ICA and save traces
    for i=1:size(dirs)
        if (i==8)
            continue;
        end
        %cd(dirs(i).name);
        xlsRow={dirs(i).name};
        vidFolder=[Le2iFolder dirs(i).name '/'];
        %matFileNames={'rgbTraces', 'rgbTraces_crop', 'rgbTraces_skin'};
        matFileNames={'rgbTraces'};%, 'pulseTraceRGB_crop', 'pulseTraceRGB_skin'};
%         if strcmp(method,'Chrom')==1
%             matFileNames={'pulseTraceChrom_skin'};
%         end
%         if strcmp(method,'Green')==1
%             matFileNames={'pulseTraceGreen_skin'};
%         end
        for j=1:size(matFileNames,2)
            matFileName=matFileNames{j};
            %excel_row=windowedCA('vidFolder',vidFolder,'matFileName',matFileName,'method',method,'silentMode',true);
            %excel_row=ica_rppg('vidFolder',vidFolder,'matFileName',matFileName,'method',method,'IUTMode',false,'silentMode',true);
    
            %excel_row={2.3	0.2725	0.24638	0.55072	7.5521	2.4111	2.7146	17	22};
            nFrames=getTraceFromFileSeq('vidFolder',vidFolder,'outFolder',vidFolder,'matFileName',matFileName,'silent',true,'keyword','skin','countFrames',true, 'bmp_ppm_jpg', 0);
            if countFrames
                
                totalLe2iFrames=totalLe2iFrames+nFrames;
                continue;
            end
            
            if (strcmp(method, '2SR'))
                get2SRSignalFromSkinPixels('vidFolder',vidFolder,'outFolder',vidFolder, 'database','LE2I');
            else
                getPulseSignalFromTrace('vidFolder',vidFolder,'outFolder',vidFolder,'matFileName',matFileName,'silent',true,'method',method,'database','LE2I','ICAWIN',ICAWIN);
            end
            
            [HR_sensor,HR_PPG,HR_RPPG,HR_RPPG_filtered,excel_row]=getHRFromPulse('vidFolder',vidFolder,'matFileName',matFileName,'method',method,'IUTMode',false,'silent',true,'database','LE2I','ICAWIN',ICAWIN);
            if j==1
                xlsRow(1,j+1:j+9)=excel_row;
            else
                xlsRow=excel_row;
            end
            if saveExcel
                xlwrite(fileName, xlsRow, sheetName, startRange);
            end
            %modify startRange
            if j==2
                startRange(1)=char(startRange(1)+10);
            else
                startRange(1)=char(startRange(1)+11);
            end
    
        end
        %Reset excel column, carriage return
        startRange(1)='B';
        %modify startRange row
        startRange=[startRange(1) num2str(str2double(startRange(2:end))+1)]
    
    end
end
totalLe2iFrames
%% IUT Dataset
if strcmp(dbase,'IUT')
    IUTFolder='D:\Dataset\HeartRate\IUT_Dataset\';
    %IUTFolder='D:\Dataset\HeartRate\TracesIUT\';
    
    %for i=[1 3:27 30:49]
    startRange = 'B21';
    %for i=[1 3:27 30:49]
    for i=[1 3:27 30:49]
        if i<10
            vidFolder=[IUTFolder 'subject' num2str(0) num2str(i) '/vid2/'];
            vidName=['subject' num2str(0) num2str(i)];
        else
            vidFolder=[IUTFolder 'subject' num2str(i) '/vid2/'];
            vidName=['subject' num2str(i)];
        end
        xlsRow={vidName};
        %matFileNames={'rgbTraces', 'rgbTraces_crop', 'rgbTraces_skin'};
        %matFileNames={'pulseTraceRGB', 'pulseTraceRGB_crop', 'pulseTraceRGB_skin'};
        matFileNames={'rgbTraces'};
%          if strcmp(method,'Chrom')==1
%             matFileNames={'pulseTraceChrom_skin'};
%          end
%         if strcmp(method,'Green')==1
%             matFileNames={'pulseTraceGreen_skin'};
%         end
        for j=1:size(matFileNames,2)
            matFileName=matFileNames{j};
            keyword=matFileName(end-3:end);
            %excel_row=windowedCA('vidFolder',vidFolder,'matFileName',matFileName,'method',method);
            %excel_row=cica_rppg('vidFolder',vidFolder,'matFileName',matFileName,'method',method,'IUTMode',true,'silentMode',true)
            %nFrames=getTraceFromFileSeqIUTDataset('vidFolder',vidFolder,'matFileName',matFileName,'silent',true,'countFrames',false);
            if countFrames==true
            
                totalIUTFrames=totalIUTFrames+nFrames;
                continue;
            end
            if strcmp(keyword,'')==1
                matFileName=['pulseTrace' method];
            else
                matFileName=['pulseTrace' method '_' keyword];
            end
        
            
            if (strcmp(method, '2SR'))
                get2SRSignalFromSkinPixels('vidFolder',vidFolder,'outFolder',vidFolder, 'database','IUT');
            else
                %getPulseSignalFromTrace('vidFolder',vidFolder,'outFolder',vidFolder,'matFileName',matFileName,'silent',true,'method',method,'database','IUT','ICAWIN',ICAWIN);
            end
            
            [HR_sensor,HR_PPG,HR_RPPG,HR_RPPG_filtered,excel_row]=getHRFromPulse('vidFolder',vidFolder,'matFileName',matFileName,'method',method,'IUTMode',false,'silent',true,'database','LE2I','ICAWIN',ICAWIN, 'postProc', 'Kalman');
            
%                 getPulseSignalFromTrace('vidFolder',vidFolder,'outFolder',...
%         vidFolder,'silent',true,'keyword',keyword,...
%         'WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC,'ICAWIN',ICAWIN,...
%         'DOFILTER',DOFILTER,'method',method);
%     
%             [~,~,~,~,excel_row]=getHRFromPulse('vidFolder',vidFolder,...
%     'matFileName',matFileName,'database',database,...
%     'silent',true, 'WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC,...
%     'ICAWIN',ICAWIN,'DOFILTER',DOFILTER, 'method', method);
%             excel_row
            %excel_row={2.3	0.2725	0.24638	0.55072	7.5521	2.4111	2.7146	17	22};
            if j==1
                xlsRow(1,j+1:j+9)=excel_row;
            else
                xlsRow=excel_row;
            end
            xlsRow
            if saveExcel
                xlwrite(fileName, xlsRow, sheetName, startRange);
            end
            %modify startRange
            if j==2
                startRange(1)=char(startRange(1)+10);
            else
                startRange(1)=char(startRange(1)+11);
            end
    
        end
        %Reset excel column, carriage return
        startRange(1)='B';
        %modify startRange row
        startRange=[startRange(1) num2str(str2double(startRange(2:end))+1)]
    
    end
end
totalIUTFrames
%% MMSE-HR Dataset
if strcmp(dbase,'MMSE')
    %method='MAICA';
    %ICAWIN=true;
    mmsedb='D:/Dataset/HeartRate/MMSE-HR 2/MMSE-HR/';
    %mmsedb='D:/MMSE-HR/MMSE-HR/';
    subdbs={'first_10_subjects 2D','T10_T11_30Subjects'};
    overwriteTraces=false;
    overwritePulseTraces=true;
    addpath('../main-stable');
    %fileName = ['MMSE' method datestr(now,'dd-mm-yy') '.xlsx'];
    sheetName = 'SkinThresh=-1';
    startRange = 'B74';
    saveExcel=true;
    DOFILTER=true;
    WINDOW_LENGTH_SEC=15;
%     ignoreList={'F014','T10';'F014','T1';'F014','T8';'F014','T11';'F015','T11';'F022','T10';'M013','T11';'F006','T11';'F013','T11';'F013','T8' ; 'F005', 'T10' ; 'F009', 'T10' ;'F008', 'T10' ;...
%         'F007', 'T10' ;'F007', 'T11' ;'F008', 'T11' ;'F009', 'T11' ; 'F005', 'T11' ; 'F006', 'T10'}; %  'F005', 'T10' ; 'F005', 'T11' ; 'F006', 'T10' are ok
    ignoreList={'F014','T10';'F014','T1';'F014','T8';'F014','T11';'F015','T11';'F022','T10';'M013','T11';'F006','T11';'F013','T11';'F013','T8';...
        'F016', 'T8'; 'F020', 'T11'};
    skinAdjList={'M016','T10'};
    if ~DOFILTER
        filterSuffix='_Unfilt';
    else
        filterSuffix='';
    end
    vidIndex=0;
    vidToProcess=-1; %-1 to process all videos
    SKIN_THRESH=-3;
    for d=1:size(subdbs,2)
        subjects =dir([mmsedb subdbs{d} ]);
        subjects=subjects(~ismember({subjects.name},{'.','..'}));
        for i=1:size(subjects)
            %Iterate through taskids and create those as well
            tasks=dir([mmsedb subdbs{d} '/' (subjects(i).name) ]);
            tasks=tasks(~ismember({tasks.name},{'.','..'}));
            
            
            for j=1:size(tasks)
                skipVideo=false;
                %             disp([subjects(i).name '-' tasks(j).name]);
                for ig=1:size(ignoreList,1)
                    if strcmp(ignoreList{ig,1},(subjects(i).name))==1 && strcmp(ignoreList{ig,2},(tasks(j).name))==1
                        %Dont do the traces
                        skipVideo=true;
                    end
                end
                
                if skipVideo
                    continue;
                end
                %For specific videos
                vidIndex=vidIndex+1;
                if vidToProcess~=-1 && vidIndex~=vidToProcess
                    
                    continue;
                    %             else
                    %                 disp(['Processing video: ' subjects(i).name '-' tasks(j).name]);
                end
                srcdir=[mmsedb subdbs{d} '/' (subjects(i).name) '/' tasks(j).name '/'];
                disp([subjects(i).name '-' tasks(j).name]);
                              continue;
                exists=false;
                %Full face
                matFileName= 'rgbTraces';
                if exist([srcdir '/' matFileName '.mat'],'file')==2
                    exists=true;
                end
                if overwriteTraces || ~exists
                     getTraceFromFileSeq('vidFolder',srcdir,...
                         'outFolder',srcdir, 'bmp_ppm_jpg', 2, 'silent',true,'countFrames',false);
                end
                
                %Check if we need to overwrite pulse traces
                suffix='';
                if WINDOW_LENGTH_SEC==-1
                    filename=[srcdir 'pulseTrace' method suffix filterSuffix '.mat'];
                else
                    filename=[srcdir 'pulseTrace' method suffix filterSuffix '.mat'];
                end
                if ~overwritePulseTraces && exist(filename,'file')~=0
                    %skip video
                    continue;
                end
%                 if strcmp(method,'MSSA')==1
%                     getPulseFromMSSATraces('vidFolder',srcdir,'outFolder',srcdir,...
%                         'database','MMSE','method',method,'ICAWIN',ICAWIN,...
%                         'WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC,'overwrite',false);
%                 else
%                     getPulseSignalFromTrace('vidFolder',srcdir,'outFolder',srcdir,...
%                         'database','MMSE','method',method,'ICAWIN',true,'overwrite',false);
%                 end
                
                matFileName=['pulseTrace' method];
                %getPulseSignalFromTrace('vidFolder',srcdir,'outFolder',srcdir,'matFileName',matFileName,'silent',true,'method',method,'database','MMSE','ICAWIN',ICAWIN, 'WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC);
                
                
                if (strcmp(method, '2SR'))
                    get2SRSignalFromSkinPixels('vidFolder',srcdir,'outFolder',srcdir, 'database','MMSE');
                else
                    %getPulseSignalFromTrace('vidFolder',srcdir,'outFolder',srcdir, 'matFileName',matFileName,'method',method,'silent',true,'database','MMSE','ICAWIN',ICAWIN, 'postProc', 'Kalman','WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC);
                end
               [HR_sensor,HR_PPG,HR_RPPG,HR_RPPG_filtered,excel_row]=getHRFromPulse('vidFolder',srcdir,'matFileName',matFileName,'method',method,'IUTMode',false,'silent',true,'database','MMSE','ICAWIN',ICAWIN, 'postProc', 'Kalman','WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC);
            
 
                
               % matFileName=['pulseTrace' method];
                
%                 [~,~,~,~,excel_row]=getHRFromPulse('vidFolder',srcdir,...
%                     'matFileName',matFileName,'database','MMSE',...
%                     'silent',true, 'WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC,...
%                     'ICAWIN',ICAWIN,'DOFILTER',DOFILTER,'freqRange',[1 3]);
 %                           xlsRow={};
                xlsRow={[], excel_row{1:end}};
                if saveExcel==true
                    xlwrite(fileName, xlsRow, sheetName, startRange);
                end
                
                
                %Face crop method
                %             exists=false;
                %             matFileName= 'rgbTraces_crop';
                %             if exist([srcdir '/' matFileName '.mat'],'file')==2
                %                 exists=true;
                %             end
                %             if overwriteTraces || ~exists
                %                 getTraceFromFileSeq('vidFolder',srcdir,...
                %                     'outFolder',srcdir,'DO_FACECROP',1);
                %             end
                %             if strcmp(method,'MSSA')==1
                %             getPulseFromMSSATraces('vidFolder',srcdir,'outFolder',srcdir,...
                %                 'database','MMSE','method',method,'ICAWIN',ICAWIN,'keyword','crop','WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC,'overwrite',false);
                %             else
                %             getPulseSignalFromTrace('vidFolder',srcdir,'outFolder',srcdir,...
                %                 'database','MMSE','method',method,'ICAWIN',ICAWIN,'keyword','crop','WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC,'overwrite',false);
                %             end
                %
                %             matFileName=['pulseTrace' method];
                %             [~,~,~,~,excel_row]=getHRFromPulse('vidFolder',srcdir,...
                %                 'matFileName',matFileName,'keyword','crop','database','MMSE',...
                %                 'silent',true, 'WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC,...
                %                 'ICAWIN',ICAWIN,'DOFILTER',DOFILTER);
                %
                %             xlsRow=excel_row;
                %             startRange(1)=char(startRange(1)+11);
                %             if saveExcel==true
                %                 xlwrite(fileName, xlsRow, sheetName, startRange);
                %             end
                %
                %Skin
                %             exists=false;
                %             matFileName= 'rgbTraces_skin';
                %             if exist([srcdir '/' matFileName '.mat'],'file')==2
                %                 exists=true;
                %             end
                %             if overwriteTraces || ~exists
                %
                %                 for ig=1:size(skinAdjList,1)
                %                     if strcmp(skinAdjList{ig,1},(subjects(i).name))==1 && strcmp(skinAdjList{ig,2},(tasks(j).name))==1
                %                         %Dont do the traces
                %                         SKIN_THRES=-1;
                %                     end
                %                 end
                %                 getTraceFromFileSeq('vidFolder',srcdir,...
                %                     'outFolder',srcdir,'DO_SKINDETECTION',1,'SKIN_THRESH',SKIN_THRESH);
                %             end
                %             getPulseSignalFromTrace('vidFolder',srcdir,'outFolder',srcdir,...
                %                 'database','MMSE','method',method,'ICAWIN',...
                %                 ICAWIN,'keyword','skin','WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC,'overwrite',false);
                %
                %             matFileName=['pulseTrace' method];
                %             [~,~,~,~,excel_row]=getHRFromPulse('vidFolder',srcdir,...
                %                 'matFileName',matFileName,'keyword','skin','database','MMSE',...
                %                 'silent',true, 'WINDOW_LENGTH_SEC',WINDOW_LENGTH_SEC,...
                %                 'ICAWIN',ICAWIN,'DOFILTER',DOFILTER);
                startRange(1)=char(startRange(1)+10);
                xlsRow=excel_row;
                if saveExcel==true
                    xlwrite(fileName, xlsRow, sheetName, startRange);
                end
                %Reset excel column, carriage return
                startRange(1)='B';
                %modify startRange row
                startRange=[startRange(1) num2str(str2double(startRange(2:end))+1)];
                
                
            end
            
            
        end
        
    end
end
end