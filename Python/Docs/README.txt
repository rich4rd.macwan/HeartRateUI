Comments on the files in the folder "source code"

- mainCHROM.py
	Contains the main, that will launch all the functions of the project (GUI and initialization)
	
- GUI.py
	A dynamic GUI where we can choose the folder, pause and start the process, and make the process. Display the current image, the rPPG, the current heartbeat and all the heatbeats since the beginning.
	
- advanced_mainCHROM.py
	Contains the advanced main, that will launch all the functions of the project (GUI and initialization)
	
- advanced_GUI.py
	A dynamic GUI where we can choose the folder, pause and start the process, and make the process. Display :
		-> The current image
		-> The face detection
		-> The skin detection
		-> PPG
		-> rPPG
		-> The FFT
		-> The green channel
		-> The comparison between all the heatbeats in PPG and rPPG since the beginning
		-> The current heartbeat
		-> The current file
		
- getRGBTraces.py
	It's the first librairy called during the process. The functions are :
		-> readCsv : read the RGBTraces and the timestamp from a csv file.
		-> getImgList : from a repository, give the list of all the images from the video
		-> extractAndSaveRGBTraces : Save into a csv file the RGBTraces and the timestamp
		-> all_functions_frames : make together the ROI_face, the skinDetection and the spatialAverage
		-> saveFile : save a file from a buffer. Needed for extractAndSaveRGBTraces.
		-> ROI_face : detect the face(s) from an image
		-> skinDetection : detect the skin on the face given
		-> spatialAverage : compute the mean of the RGB sequence given
		
- preProcessData.py
	Library used for making a signal from a set of RGBtraces
		-> chromProcess : main of the library, make all the process, with calling others functions
		-> compute_Xs_Ys : used for creating the input of the Xs_min_alpha_Ys function.
		-> apply_filter : used for computing the Xs_min_alpha_Ys.
		-> Xs_min_alpha_Ys : make the signal
		-> temporalAverage : compute the average of the RBGTraces (by each channel)
		-> normalize : normalize the RGBTraces by each average channel value.
		-> fft : make the fft of a signal given
		-> interpolation : interpolate a signal by a number of points given
		-> execEquations, R_over_G, X_over_Y, fixed_8, fixed_12 : depreciated, not used here
		
- getPulseFromTraces.py
	From a signal by FFT, extract the heartbeat.
		-> extractHeartBeat : main, extract the heartbeat from the signal
		-> for_y_lim : make a restriction on the signal, by the scale
		
- getPPG.py
	Used to produce a PPG signal from a file
		-> readXmp : read a xmp file
		-> loadCrtPPG : main, give the signal extract from the xmp file
		
- frameProcessing.py
	Two classes, used for the (advanced_)GUI. Make all the process with a RGB traces given to the heartbeat value.
		-> FrameProcessing : class for GUI
		-> AdvancedFrameProcessing : class for advanced_GUI
	-> getCrtFrames : function that take the current value from all the RGBTraces