import fromVideo as video;
import ROI as roi;
import skinDetection as sd;
import rgbOperations as ro;
import cv2;
import cv2.cv as cv;
import numpy as np;
import time;
import normalize as norm;
import equations as eq;
import csvManager as cm;
import matplotlib.pyplot as plt;
import IHM;
import extractXmp as ex;
import fourier as fr;
from scipy import interpolate;
import overlap as ov;
import dft;
	
def mainVideo(repo):
	sizeWindow = 400;
	currentLeft = 0;
	
	# Read the xmp file of the signals.
	array_xmp = cm.readXmp("..\\5-gt\\gtdump.xmp");
	# array_xmp = cm.readXmp("/media/yannick/Data/Dataset/HeartRate/RichardSummerVideos/5-gt/gtdump.xmp");
	
	# Read the csv file of the RGB spatial average. if not exist, create the csv
	spatial_averages_ts = cm.readCsv(repo);
	
	array_timeStamp = spatial_averages_ts[:,3];
	spatial_averages = spatial_averages_ts[:,0:3];
	
	[sa_width,sa_height] = spatial_averages.shape;
	
	#Number of windows
	numberWindows = sa_width - sizeWindow;
	
	array_HR_eq = [];
	array_HR_ppg = [];
	array_diff_HR = [];
	
	#For every temporal windows
	for indexWindow in range(0,numberWindows+1):
		# Take the frames of the window
		rgb_table_frames = spatial_averages[currentLeft:currentLeft+sizeWindow];
		
		# Take the timeStamp of the window (in secondes)
		timeStamp_frames = array_timeStamp[currentLeft:currentLeft+sizeWindow].copy();
		timeStamp_frames[:] = [x / 1000 for x in timeStamp_frames];
		
		# Take the first and last timestamp for the PPG
		first_timeStamp = array_timeStamp[currentLeft];
		last_timeStamp = array_timeStamp[currentLeft + sizeWindow - 1];

		# Temporal average of the window
		rgb_table_window = ro.temporalAverage(rgb_table_frames);
		
		# Array of normalized frames
		array_frames_normalize = [];
		array_res = [];
		
		# For all the frames of the window (Equations except min_alpha)
		for rgb_table_f in rgb_table_frames:

			#Normalisation of the frame
			rgb_table_f_normalise = norm.normalize(rgb_table_f,rgb_table_window);
			array_frames_normalize.append(rgb_table_f_normalise);
			
		# Used for the min_alpha
		Xs = [];
		Ys = [];
			
		# For all the frames of the windows (min_alpha equation)
		for rgb_normalise in array_frames_normalize:
			[X,Y] = eq.compute_Xs_Ys(rgb_normalise);
			Xs.append(X);
			Ys.append(Y);
			
		# Band-passed filtor
		res_eq14 = eq.Xs_min_alpha_Ys(Xs,Ys);
		
		# Print the result for each frames
		for i in range(0,len(res_eq14)):
			array_res.append(res_eq14[i]);

		# PPG
		PPG = ex.extractValues(first_timeStamp,last_timeStamp,array_xmp);
			
		#FFT
		
		[axis_x_f,axis_y_f,hb_eq14] = fr.fft_signal(timeStamp_frames,array_res);
		
		[axis_x_p,axis_y_p,hb_ppg] = fr.fft_signal(PPG[0],PPG[1]);
		
		# ------DFT------
		[lAmplitude, lPhase] = dft.forChrom(timeStamp_frames,array_res);
		# ---------------
		
		#------OVERLAP------
		# if(currentLeft % 29 == 0):
			# overlap_signal = ov.overlap(timeStamp_frames,array_res);
			
			# [axis_x_o,axis_y_o,hb_o] = fr.fft_signal(timeStamp_frames,overlap_signal);
			
			# plt.figure;
			# plt.subplot(2,1,1);
			# plt.plot(axis_x_f,axis_y_f);
			
			# plt.subplot(2,1,2);
			# plt.plot(axis_x_o,axis_y_o);
			
			# plt.show();
		#-------------------
		
		array_HR_eq.append(hb_eq14);
		array_HR_ppg.append(hb_ppg);
		
		# moyenne de la valeur absolue de la difference entre HR_PPG et HR_rPPG
		array_diff_HR.append(abs(hb_eq14-hb_ppg));
		
		# Final GUI
		# IHM.ihm([timeStamp_frames,array_res],PPG,repo,currentLeft+sizeWindow-1,hb_eq14);
		
		# Shift of images
		currentLeft = currentLeft + 1;
		
		print str(currentLeft) + " / " + str(numberWindows);
		
	print np.mean(array_diff_HR);
	print np.max(array_diff_HR);
	
	bland_altman_plot(array_HR_eq,array_HR_ppg);
	
	plt.plot(range(sizeWindow,numberWindows+sizeWindow+1),array_HR_eq,color="b");
	plt.plot(range(sizeWindow,numberWindows+sizeWindow+1),array_HR_ppg,color="g");
	plt.show();
	
def bland_altman_plot(data1, data2):
	data1     = np.asarray(data1)
	data2     = np.asarray(data2)
	mean      = np.mean([data1, data2], axis=0)
	diff      = data1 - data2                   # Difference between data1 and data2
	md        = np.mean(diff)                   # Mean of the difference
	sd        = np.std(diff, axis=0)            # Standard deviation of the difference
	
	print diff;
	
	plt.scatter(mean, diff)
	plt.axhline(md,           color='gray', linestyle='--')
	plt.axhline(md + 1.96*sd, color='gray', linestyle='--')
	plt.axhline(md - 1.96*sd, color='gray', linestyle='--')
	plt.title('Bland-Altman Plot')
	plt.show()

def mainWebcam():
	cap = cv2.VideoCapture(0);

	firstCap = -1;
	count = 0;

	while(True):
		# Capture frame-by-frame
		ret, frame = cap.read();
		if(firstCap == -1):
			firstCap = time.time();
		
		print time.ctime();
		print str(count)+"->"+str(int((time.time()-firstCap)*1000));
		
		count = count + 1;

		# Display the resulting frame
		cv2.imshow('frame',frame);
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break;

	# When everything done, release the capture
	cap.release();
	cv2.destroyAllWindows();
	
def main():
	isVideo = 1;

	if(isVideo) :
		repo = "img";
		mainVideo(repo);
	else:
		print "webcam : for future";
		mainWebcam();
		
main();