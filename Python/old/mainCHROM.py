import fromVideo as video;
import ROI as roi;
import skinDetection as sd;
import rgbOperations as ro;
import cv2;
import cv2.cv as cv;
import numpy as np;
import time;
import normalize as norm;
import equations as eq;
import csvManager as cm;
import matplotlib.pyplot as plt;
import IHM;
import extractXmp as ex;
import fourier as fr;
from scipy import interpolate;
	
def mainVideo(repo):
	sizeWindow = 400;
	currentLeft = 0;
	
	# Read the xmp file of the signals.
	# array_xmp = cm.readXmp("..\\5-gt\\gtdump.xmp");
	array_xmp = cm.readXmp("/media/yannick/Data/Dataset/HeartRate/RichardSummerVideos/5-gt/gtdump.xmp");
	
	# Read the csv file of the RGB spatial average. if not exist, create the csv
	spatial_averages_ts = cm.readCsv(repo);
	
	array_timeStamp = spatial_averages_ts[:,3];
	spatial_averages = spatial_averages_ts[:,0:3];
	
	[sa_width,sa_height] = spatial_averages.shape;
	
	#Number of windows
	numberWindows = sa_width - sizeWindow;
	
	#For every temporal windows
	for indexWindow in range(0,numberWindows+1):
		# Take the frames of the window
		rgb_table_frames = spatial_averages[currentLeft:currentLeft+sizeWindow];
		
		# Take the timeStamp of the window (in secondes)
		timeStamp_frames = array_timeStamp[currentLeft:currentLeft+sizeWindow].copy();
		timeStamp_frames[:] = [x / 1000 for x in timeStamp_frames];
		
		# Take the first and last timestamp for the PPG
		first_timeStamp = array_timeStamp[currentLeft];
		last_timeStamp = array_timeStamp[currentLeft + sizeWindow - 1];

		# Temporal average of the window
		rgb_table_window = ro.temporalAverage(rgb_table_frames);
		
		# Array of normalized frames
		array_frames_normalize = [];
		array_res = [];
		array_green = [];
		
		# For all the frames of the window (Equations except min_alpha)
		for rgb_table_f in rgb_table_frames:

			#Normalisation of the frame
			rgb_table_f_normalise = norm.normalize(rgb_table_f,rgb_table_window);
			array_frames_normalize.append(rgb_table_f_normalise);
	
			#Execution of the different equations
			res = eq.execEquations(rgb_table_f_normalise);
			
			array_res.append(res);
			
			array_green.append(rgb_table_f[2]);
			
		# Used for the min_alpha
		Xs = [];
		Ys = [];
			
		# For all the frames of the windows (min_alpha equation)
		for rgb_normalise in array_frames_normalize:
			[X,Y] = eq.compute_Xs_Ys(rgb_normalise);
			Xs.append(X);
			Ys.append(Y);
			
		# Band-passed filtor
		res_eq14 = eq.Xs_min_alpha_Ys(Xs,Ys);
		
		# Print the result for each frames
		for i in range(0,len(res_eq14)):
			array_res[i].append(res_eq14[i]);
		
		# Array of the result for every equation (before for every frames)
		array_res_transpose = zip(*array_res);
		
		size_transpose = len(array_res_transpose);
		
		# --------------BAND-PASSED FILTER-------------------
		array_res_transpose_filtered = [];
		
		for i in range(0,size_transpose-1):
			array_res_transpose_filtered.append(eq.StoF(array_res_transpose[i]));
			
		# NOT THE EQUATION 14 (already filtered)
		array_res_transpose_filtered.append(array_res_transpose[size_transpose-1])
		# ---------------------------------------------------

		# PPG
		PPG = ex.extractValues(first_timeStamp,last_timeStamp,array_xmp);
		
		# Title for the plotting
		titles = ["Equation 3","Equation 5","Equation 8","Equation 12","Equation 14", "Green"];
		
		if (currentLeft % 29 == 0):
			# Plotting options
			plt.figure;
			for i in range(0,size_transpose+1):
				ax1 = plt.subplot(3,2,i+1);
				ax1.plot(PPG[0],PPG[1],color="c");
					
				ax2 = ax1.twinx();
				if(i != 5):
					ax2.plot(timeStamp_frames,array_res_transpose_filtered[i],color="r");
					[new_array_time,new_array_signal] = fr.interpolation(timeStamp_frames,array_res_transpose_filtered[i],1000);
					ax2.plot(new_array_time,new_array_signal,color="b");
				else:
					ax2.plot(timeStamp_frames,array_green,color="g");
					
				plt.title(titles[i]);
				
			plt.show();
			
			array_HeartBeat = [];
			
			#FFT
			plt.figure;
			for i in range(0,size_transpose+1):
			    if(i != 5):
					 [axis_x,axis_y,hb] = fr.fft_signal(timeStamp_frames,array_res_transpose_filtered[i]);
					 array_HeartBeat.append(hb);
					 #plt.subplot(3,2,i+1);
					 #plt.plot(axis_x,axis_y);	
					 #plt.title("FFT - " + titles[i]);
      
                            # DFT
					 [freq, amplitude, phase] = fr.dft_signal(timeStamp_frames,array_res_transpose_filtered[i]);
					 plt.subplot(3,2,i+1);
					 plt.plot(freq,amplitude);	
					 plt.title("DFT - " + titles[i]);
			    else:
					 plt.subplot(3,2,i+1);
					 plt.axis([0, 10, 0, 10])

					 for cur in range(0,len(array_HeartBeat)) :
						 t = titles[cur] + " : " + str(array_HeartBeat[cur]) + " bpm";
						 plt.text(0, len(array_HeartBeat)-cur, t);
					
					 plt.text(0, -1, str(first_timeStamp/1000) + " s - " + str(last_timeStamp/1000) + " s");
					 plt.axis('off')
				
			plt.show();
		
		# Final GUI
		# IHM.ihm([timeStamp_frames,array_res_transpose[4]],PPG,repo,currentLeft+sizeWindow-1,array_HeartBeat[4]);
		
		# Shift of images
		currentLeft = currentLeft + 1;

def mainWebcam():
	cap = cv2.VideoCapture(0);

	firstCap = -1;
	count = 0;

	while(True):
		# Capture frame-by-frame
		ret, frame = cap.read();
		if(firstCap == -1):
			firstCap = time.time();
		
		print time.ctime();
		print str(count)+"->"+str(int((time.time()-firstCap)*1000));
		
		count = count + 1;

		# Display the resulting frame
		cv2.imshow('frame',frame);
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break;

	# When everything done, release the capture
	cap.release();
	cv2.destroyAllWindows();
	
def main():
	isVideo = 1;

	if(isVideo) :
		repo = "img";
		mainVideo(repo);
	else:
		print "webcam : for future";
		mainWebcam();
		
main();