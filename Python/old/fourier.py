import scipy;
import numpy as np;
import matplotlib.pyplot as plt;
from scipy.interpolate import interp1d
import dft;

def dft_signal(time,signal):

	startFreq= 0.3
	stopFreq= 3.5
	gap= 0.01
	freq = np.arange(startFreq, stopFreq, gap)
	
	# Hanning window
	hanning_window = np.hanning(len(time));
	signal2 = signal * hanning_window;

	[amplitude, phase] = dft.computeSpectrum(freq,signal2,time);
	
	return [freq, amplitude, phase];

def fft_signal(array_time,array_signal):

	for x in range(0,2):
	
		if (x == 1):
			[array_time,array_signal] = interpolation(array_time,array_signal,1000);
			
		N = len(array_time);	
		Fs = 1/(array_time[1]-array_time[0]);
			
		axis_x = 60 * np.arange(0, N) * Fs/N;
		
		# Hanning window
		window = np.hanning(N);
		acc = array_signal * window;
		
		# FFT
		f = np.fft.fft(acc);
		axis_y = abs(f);
		
		# Restriction on 36,210
		[ind1,ind2] = for_y_lim(axis_x,60*0.6, 60*3.5);
		axis_x_restrict = axis_x[ind1:ind2].copy();
		axis_y_restrict = axis_y[ind1:ind2].copy();
		
		# Compute Heart Beat
		indMax = np.argmax(axis_y_restrict);
		heartBeat = axis_x_restrict[indMax];
	
		# plt.subplot(2,1,x+1);
		# plt.plot(axis_x_restrict,axis_y_restrict);
		# s = "";
		# if(x == 0) :
			# s = "Normal";
		# else :
			# s = "Interpolate";
		# plt.title(s + " - BPM : " + str(heartBeat));
		
	# plt.show();
	
	return [axis_x_restrict,axis_y_restrict,heartBeat];
	
def interpolation(array_time,array_signal,nbNewPoints):
	# Boundary
	minTime = array_time[0];
	maxTime = array_time[len(array_time) - 1];
	
	# Total gap
	totalGap = maxTime - minTime;
	
	# new one : -1 because we have to take the first and the last one
	new_gap = totalGap/(nbNewPoints-1);
	
	# New array of time. arg2, "+new_gap", because otherwise, do not take the last one
	new_array_time = np.arange(minTime,maxTime+new_gap,new_gap);
	
	# Fix the limit at the maximum number of points
	new_array_time_Max = new_array_time[0:nbNewPoints].copy();
	
	#interp
	f = interp1d(array_time, array_signal);
	
	new_array_signal = [];
	
	for i in range(0,len(new_array_time_Max)):
		# Problem of float comparison
		if(i == len(new_array_time_Max)-1):
			val = f(maxTime);
		else :
			time = new_array_time_Max[i];
			val = f(time);
			
		val = float(val);
		new_array_signal.append(val);
	
	return [new_array_time_Max,new_array_signal];
	
# Find 
def for_y_lim(axis_x,val1,val2):
	currentIndex = 0;
	while(val1 > axis_x[currentIndex]):
		currentIndex = currentIndex + 1;
		
	res1 = currentIndex;
	
	while(val2 > axis_x[currentIndex]):
		currentIndex = currentIndex + 1;
		
	res2 = currentIndex;
		
	return [res1,res2];
	

def all_fft(array_time,array_all_signals):
	size = len(array_all_signals);
	for i in range(0,size):
		fft_signal(array_all_signals[i],array_time);