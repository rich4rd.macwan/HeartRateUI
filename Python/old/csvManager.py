import fromVideo as video;
import ROI as roi;
import skinDetection as sd;
import rgbOperations as ro;
import cv2;
import numpy as np;
import os;

def toCsv(repo):
	images_timestamp = video.ToArrayPathTimestamp("..\\5-gt\\"+repo);
	spatial_averages = [];
	
	#For every frames
	for [path,ts] in images_timestamp:
		img = cv2.imread(path);
		
		#ROI, skin, spatialAverage
		rgb_img = all_functions_frames(img,ts);
		
		rgb_img_ts = rgb_img + (ts,);
		
		spatial_averages.append(rgb_img_ts);
		
	print spatial_averages;
	
	saveFile("csv/"+repo+".csv",spatial_averages,'\n');
	
def all_functions_frames(img,timestamp):
	img_roi = roi.ROI_face(img);
	
	img_skin_threshold = sd.skinDetection(img_roi);
	img_skin_threshold_float = img_skin_threshold.astype('float');
	
	rgbAverage = ro.spatialAverage(img_skin_threshold_float);
	
	return rgbAverage;
	
def saveFile(fName,array,newLine):
	file = '';
	for line in array:
		for case in line:
			file = file + str(case) + ",";
		file = file[:-1];
		file = file + newLine;
		
	f = open(fName, 'w');
	f.write(file);
	f.close();
	
def readCsv(repository):
	file = "csv/"+repository+".csv";
	
	if not (os.path.isfile(file)):
		print "not exist";
		toCsv(repository);
		
	array = np.genfromtxt(open(file), delimiter=',', dtype='f8')[:];
	
	return array;
	
def readXmp(file):
	array = np.genfromtxt(open(file), delimiter=',', dtype=np.dtype(np.int64))[:];
	return array;