from scipy.signal import butter, lfilter
import numpy as np;
import matplotlib.pyplot as plt;
import cv2;

# Compute the Xs and the Ys of a normalized RGB
def compute_Xs_Ys(RGBn):
	[Rn,Gn,Bn] = RGBn;

	Rs = 0.7682*Rn;
	Gs = 0.5121*Gn;
	Bs = 0.3841*Bn;
	
	Xs = Rs-Gs;
	Ys = 0.5*Rs + 0.5*Gs - Bs;
	
	return [Xs,Ys];
	
# Execute all the equations, except min_alpha
def execEquations(RGBn):
	res = [];
	res.append(R_over_G(RGBn));
	res.append(X_over_Y(RGBn));
	res.append(fixed_8(RGBn));
	res.append(fixed_12(RGBn));
	
	return res;

# Equation 3
def R_over_G(RGBn):
	[Rn,Gn,Bn] = RGBn;
	
	si = (Rn/Gn) - 1;
	return si;
	
# Equation 5
def X_over_Y(RGBn):
	[Rn,Gn,Bn] = RGBn;
	
	Xn = -(Rn-Gn);
	Yn = 0.5*Rn + 0.5*Gn - Bn;
	
	si = (Xn/Yn)-1;
	return si;
	
# Equation 8
def fixed_8(RGBn):
	[Xs,Ys] = compute_Xs_Ys(RGBn);
	
	si = (Xs/Ys)-1;
	
	return si;
	
# Equation 12	
def fixed_12(RGBn):
	[Rn,Gn,Bn] = RGBn;
	
	si = 1.5*Rn - 3*Gn + 1.5*Bn;
	
	return si;
	
def StoF(data):
	fs = 30.0;
	lowcut = 0.3;
	highcut = 3.5;

	nyq = 0.5 * fs;
	low = lowcut / nyq;
	high = highcut / nyq;
	
	b, a = butter(5, [low, high], btype='band');

	y = lfilter(b, a, data);
	
	return y;
	
# Equation 14
def Xs_min_alpha_Ys(array_Xs,array_Ys):
	array_eq = [];
	
	array_Xf = StoF(array_Xs);
	array_Yf = StoF(array_Ys);
	
	alpha = np.std(array_Xf)/np.std(array_Yf);
	
	for i in range(0,len(array_Xs)):
		res = array_Xf[i] - alpha * array_Yf[i];
		array_eq.append(res);
	
	return array_eq;