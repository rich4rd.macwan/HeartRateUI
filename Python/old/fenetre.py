import os, glob;
import numpy as np;
import cv2;

def ToArrayPathTimestamp(folder):
	dir = os.path.join(folder,"*.ppm");

	files = glob.glob(dir);

	# Create an array with files sorted by number, and the timeStamp associated
	array_path_timeStamp = ["" for x in range(len(files))];
			
	for name in files:
		# 0 : path, 1 : framenumber, 2 : timeStamp".ppm"
		tabSplit = name.split('_');
		
		# 0 : timeStamp, 1 : "ppm"
		tabSplitTime = tabSplit[2].split('.');
		
		frameNumber = int(tabSplit[1]);
		
		timeStamp = int(tabSplitTime[0]);
		
		array_path_timeStamp[frameNumber] = [name,timeStamp];
		
	currentLeft = 0;
	
	sizeWindow = 5;
	numberWindows = len(array_path_timeStamp) - sizeWindow;
	print numberWindows;
	
	for indexWindow in range(0,numberWindows+1):
		for indexFile in range(currentLeft,currentLeft+sizeWindow):
			print indexFile;
			print array_path_timeStamp[indexFile];
			
		print '------------------------------------------';
		currentLeft = currentLeft + 1;
		
ToArrayPathTimestamp("..\\5-gt\\img");