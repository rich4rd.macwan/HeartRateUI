import cv2;
import cv2.cv as cv;

#---------------------------------------------------------------------------------------------------------
#Depreciated
def ROI_face_cv(img):
	faceXML = cv.Load("haarcascade_frontalface_default.xml");
	storage = cv.CreateMemStorage();
	detectROI = cv.HaarDetectObjects(img, faceXML, storage);
	
	for face in detectROI:
		# size > 10
		if(face[1] > 10):
			# 0 : x, 1 : y, 2 : height, 3 : width
			cv.Rectangle(img,(face[0][0]-1,face[0][1]-1),
				   (face[0][0]+face[0][2],face[0][1]+face[0][3]),
				   cv.RGB(155, 55, 200),1);
				   
			out_roi = img[face[0][1]:face[0][1]+face[0][3], face[0][0]:face[0][0]+face[0][2]];
	
	cv.ShowImage('CV', img);
	cv.WaitKey();
	
	cv.ShowImage('out_roi', out_roi);
	cv.WaitKey();
	
	return out_roi;
#---------------------------------------------------------------------------------------------------------
	
# Detect the ROI with opencv2
def ROI_face(img2):
	face_cascade = cv2.CascadeClassifier('./haarcascade_frontalface_default.xml');
	faces = face_cascade.detectMultiScale(img2, 1.3, 5);
	
	for face in faces:
		cv2.rectangle(img2,(face[0]-1,face[1]-1),
			   (face[0]+face[2],face[1]+face[3]),
			   cv.RGB(155, 55, 200),1);
			
		# Take all ? (if more than one face)
		out_roi = img2[face[1]:face[1]+face[3], face[0]:face[0]+face[2]];
			   
	# cv2.imshow('CV2', img2);
	# cv2.waitKey(0);
	
	# cv2.imshow('out_roi2', out_roi);
	# cv2.waitKey(0);
	
	return out_roi;