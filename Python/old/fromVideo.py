import os, glob;
import numpy as np;
import cv2;

# From a folder full of images in ppm format, create an array as each values is [path, timestamp] of the images, classified by frame number
def ToArrayPathTimestamp(folder):
	dir = os.path.join(folder,"*.ppm");

	files = glob.glob(dir);

	# Create an array with files sorted by number, and the timeStamp associated
	array_path_timeStamp = ["" for x in range(len(files))];
			
	for name in files:
		# 0 : path, 1 : framenumber, 2 : timeStamp".ppm"
		tabSplit = name.split('_');
		
		# 0 : timeStamp, 1 : "ppm"
		tabSplitTime = tabSplit[2].split('.');
		
		frameNumber = int(tabSplit[1]);
		
		timeStamp = int(tabSplitTime[0]);
		
		array_path_timeStamp[frameNumber] = [name,timeStamp];
		
	# for [file,ts] in array_path_timeStamp:
		# print ts;
		# img = cv2.imread(file);
		
		# cv2.imshow('cv2.WINDOW_NORMAL',img);
		# cv2.waitKey(0);
		
	return array_path_timeStamp