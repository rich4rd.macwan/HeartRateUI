#Normalize the current frame with the rgb average temporal window
def normalize(rgb_img, rgb_average):
	rgb_normalize = [rgb_img[0]/rgb_average[0],rgb_img[1]/rgb_average[1],rgb_img[2]/rgb_average[2]];
	return rgb_normalize;