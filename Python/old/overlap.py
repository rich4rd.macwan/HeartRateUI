import numpy as np;
import matplotlib.pyplot as plt;

def overlap(array_time,array_signal):

	#Initialisation
	interval = 256;
	numberFrames = len(array_signal);
	
	N = 1;
	
	array_res = np.zeros(numberFrames);
	currentLeft = 0;
	currentRight = 0;
	
	# While we can take 32 images each 16 images:
	while((interval*(N+1)/2) < 400):
		# First image
		currentLeft = (interval*(N-1)/2);
		# Last image
		currentRight = currentLeft+interval;
		print currentLeft;
		print currentRight;
		
		# Extract the signal
		current_signal = array_signal[currentLeft:currentRight+1];
		
		# Hanning on the signal taken
		window = np.hanning(len(current_signal));
		current_signal_hanning = current_signal * window;
		
		# For each images :
		index = 0;
		for i in range(currentLeft,currentRight):
			array_res[i] = array_res[i] + current_signal_hanning[index];
			index = index + 1;
			
		N = N+1;
	
	plt.plot(array_time,array_signal,color="b");
	plt.plot(array_time,array_res,color="g");
	plt.show();
	
	return array_res;