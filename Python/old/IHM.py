import fromVideo as video;
from Tkinter import *;

import matplotlib;
matplotlib.use('TkAgg');

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg;
# implement the default mpl key bindings
from matplotlib.backend_bases import key_press_handler;

from matplotlib.figure import Figure;

def ihm(array,PPG,repo,current,bmp):
	# Folder of images
	files = video.ToArrayPathTimestamp("..\\5-gt\\"+repo);
	
	# Current Image
	[path,ts] = files[current];
	print path;

	# --- Displays ---
	# Window
	fenetre = Tk();
	fenetre['bg']='white';

	# Photo
	photo = PhotoImage(file=path);
	canvas = Canvas(fenetre,width=640, height=480);
	canvas.create_image(0, 0, anchor=NW, image=photo);
	canvas.create_text(100, 10, text=path);
	canvas.pack(side = LEFT);
	
	# Best signal rate
	f2 = Figure(figsize=(5, 3), dpi=100);
	a2 = f2.add_subplot(111);
	a2.plot(array[0],array[1]);
	a2.set_title("Pulse Signal");
	
	canvas2 = FigureCanvasTkAgg(f2);
	canvas2.get_tk_widget().pack(side=TOP);
	
	# PPG
	f3 = Figure(figsize=(5, 3), dpi=100);
	a3 = f3.add_subplot(111);
	a3.plot(PPG[0],PPG[1]);
	a3.set_title("PPG");
	
	canvas3 = FigureCanvasTkAgg(f3);
	canvas3.get_tk_widget().pack(side=BOTTOM);
	
	canvas4 = Canvas(fenetre,width=110, height=20);
	canvas4.create_text(55, 10, text=str(bmp)+" bmp");
	canvas4.pack(side = BOTTOM);

	# Loop
	fenetre.mainloop();