import wave;
import matplotlib.pyplot as plt;
import struct;
import numpy as np;
import csvManager as cm;

def computeSpectrum(lFrequency, lVal, timeLine):

    lAmplitude = []
    lPhase = []

    for freq in lFrequency:
        amplitude, phase, sumX, sumY = computeDFT(freq, lVal, timeLine)

        if amplitude<0.01:
            phase = -10.

        lAmplitude.append(amplitude)
        lPhase.append(phase)

    return lAmplitude, lPhase
       

def computeDFT(freq, lVal, timeLine):

    xVal = np.cos(2.*np.pi*freq*timeLine)*lVal
    yVal = -np.sin(2.*np.pi*freq*timeLine)*lVal

    sumXVal = np.sum(xVal)
    sumYVal = np.sum(yVal)

    amplitude, phase = computeAmplitudeAndPhaseFromXY(sumXVal, sumYVal, len(lVal))

    #print amplitude, phase

    return amplitude, phase, sumXVal, sumYVal

def computeAmplitudeAndPhaseFromXY(sumXVal, sumYVal, nbValue=1):

    amplitude = 2.*np.sqrt(sumXVal**2 + sumYVal**2)/nbValue

    phase = np.angle(np.complex(sumXVal, sumYVal), deg=False)

    return amplitude, phase


#for Wave manipulation ##################################################################################""

def writeToWave(fileName, valLeft, valRight, framerateAudio = 44100, amplitudeAudio = 1.):

    vals = interleave(valLeft, valRight)

    wout = wave.open(fileName, "wb")
    nchan = 2
    sampwidth = 2
    #framerate = 22050#8000#22050

    comptype = "NONE"
    compname = "no compression"

    print (nchan, sampwidth, framerateAudio, len(vals), comptype, compname)

    wout.setparams((nchan, sampwidth, framerateAudio, len(vals), comptype, compname))

    data = ""
    for idValue, v in enumerate(vals):
        value = int(v*amplitudeAudio)
        if value>32767:
            value = 32767
            print "Warning idvalue " + str(idValue) + " = " + str(value)
        if value<-32767:
            value = -32767

        data = data + struct.pack("<h", value)
    wout.writeframes(data)
    wout.close()

def interleave(lValueLeft, lValueRight):

    lValueInterlaced = []

    for IDValue, value in enumerate(lValueLeft):

        lValueInterlaced.append(lValueLeft[IDValue])
        lValueInterlaced.append(lValueRight[IDValue])

    return np.array(lValueInterlaced)

def everyOther (v, offset=0):
   return [v[i] for i in range(offset, len(v), 2)]

def wavLoad (fname):
   wav = wave.open (fname, "r")
   (nchannels, sampwidth, framerate, nframes, comptype, compname) = wav.getparams ()
   frames = wav.readframes (nframes * nchannels)
   out = struct.unpack_from ("%dh" % nframes * nchannels, frames)

   # Convert 2 channels to numpy arrays
   if nchannels == 2:
       left = np.array (list (everyOther (out, 0)))
       right = np.array (list  (everyOther (out, 1)))
   else:
       left = array (out)
       right = left

   return left, right
#end for Wave manipulation ##################################################################################""

def main():
	"""
	frameRate = 48000.
	duration = 1.00
	phaseLeft = 0.1
	phaseRight = 0.3

	timeLine = np.arange(0, duration, 1./frameRate)
	left = 2*np.cos(2.*np.pi*440*timeLine + phaseLeft)
	right = np.cos(2.*np.pi*330*timeLine + phaseRight)
	"""
	repo = "img";
	spatial_averages_ts = cm.readCsv(repo);
	sign = spatial_averages_ts[000:2400,1]
	sign = sign-np.mean(sign)
    
	timeLine = spatial_averages_ts[000:2400,3]
    
	timeLine = timeLine/1000.
    
	sign =  10*np.sin(2*3.14159*2.85*timeLine)
    
	plt.plot(timeLine, sign)
	plt.show()
    
	startFreq= 0.3
	stopFreq= 3.5
	gap= 0.01
	lFrequency = np.arange(startFreq, stopFreq, gap)
    
	lAmplitude, lPhase = computeSpectrum(lFrequency, sign, timeLine)
	plt.plot(lFrequency, lAmplitude)
	plt.show()
    
    #plt.plot(timeLine, sign)
    #plt.show()

	"""
	fileName = "data.wav"
	writeToWave("data.wav", recordLeft, recordRight, frameRate)
	left, right = wavLoad("data.wav")
	"""

	"""    

	lAmplitudeLeft, lPhaseLeft = computeSpectrum(lFrequency, left, timeLine)
	lAmplitudeRight, lPhaseRight = computeSpectrum(lFrequency, right, timeLine)

	plt.plot(lFrequency, lAmplitudeLeft)
	plt.plot(lFrequency, lAmplitudeRight)

	plt.show()

	plt.plot(lFrequency, lPhaseLeft)
	plt.plot(lFrequency, lPhaseRight)

	plt.show()
	"""
	
def forChrom(array_time,array_signal):

	sign = array_signal
	sign = sign-np.mean(sign)
    
	timeLine = array_time;
    
	sign =  10*np.sin(2*3.14159*2.85*timeLine)
    
	plt.plot(timeLine, sign)
	plt.show()
    
	startFreq= 0.3
	stopFreq= 3.5
	gap= 0.01
	lFrequency = np.arange(startFreq, stopFreq, gap)
    
	lAmplitude, lPhase = computeSpectrum(lFrequency, sign, timeLine)
	plt.plot(lFrequency, lAmplitude)
	plt.show()
	
	return [lAmplitude,lPhase];

if __name__ == '__main__':
    main()