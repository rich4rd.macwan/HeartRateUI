import cv2;
import cv2.cv as cv;
import numpy;

#http://stackoverflow.com/questions/17124381/determine-skeleton-joints-with-a-webcam-not-kinect/17375222#17375222
def skinDetection(img_roi):
	# Constants for finding range of skin color in YCrCb
	min_YCrCb = numpy.array([0,133,124],numpy.uint8);
	max_YCrCb = numpy.array([255,173,255],numpy.uint8);
	
	# Convert image to YCrCb
	imageYCrCb = cv2.cvtColor(img_roi,cv2.COLOR_BGR2YCR_CB);

    # Find skin with YCrCb image
	skinRegion = cv2.inRange(imageYCrCb,min_YCrCb,max_YCrCb);
	
	thres = cv2.bitwise_and(img_roi, img_roi, mask = skinRegion);

	# Display the source image
	# cv2.imshow('skin_region',thres);
	# cv2.waitKey(0);
	
	return thres;