import cv2;
import cv2.cv as cv;
import numpy as np;

#Compute the spatial average of the skin from an image
def spatialAverage(threshold):
	
	min_th = np.array([1,1,1],np.uint8);
	max_th = np.array([255,255,255],np.uint8);
	
	# threshold
	mask = cv2.inRange(threshold,min_th,max_th);
	
	res = cv2.mean(threshold,mask);
	
	# Select BGR
	res = res[:3];
	# Reverse for RGB
	res = res[::-1];
	
	return res;

# Compute the RGB average on some images	
def temporalAverage(RGB_array):
	a = np.array(RGB_array);
	return np.mean(a, axis=0);
	
# Compute the RGB average on some images
# DEPRECIATED
def old_temporalAverage(RGB_array):
	totalR = 0;
	totalG = 0;
	totalB = 0;
	totalNumber = 0;
	
	for [R,G,B] in RGB_array:
		totalR += R;
		totalG += G;
		totalB += B;
		totalNumber += 1;
		
	return [totalR/totalNumber,totalG/totalNumber,totalB/totalNumber];