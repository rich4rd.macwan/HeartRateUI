def extractValues(first,last,array_xmp):
	# print "first : " + str(first);
	# print "last : " + str(last);
	
	res = [];
	indexes = [];
	
	currentIndex = 0;
	while(first > array_xmp[currentIndex,0]):
		currentIndex = currentIndex + 1;
		
	# print "----------";
	
	while(last > array_xmp[currentIndex,0]):
		# print "timeStamp : " + str(array_xmp[currentIndex,0]);
		# print "value : " + str(array_xmp[currentIndex,3]) + "\n";
		
		res.append(array_xmp[currentIndex,3]);
		indexes.append(array_xmp[currentIndex,0]);
		currentIndex = currentIndex + 1;
		
	indexes[:] = [float(i) for i in indexes];
	indexes[:] = [x / 1000 for x in indexes];
		
	return [indexes,res];