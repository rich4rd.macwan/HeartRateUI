import os;
import loadPPG, getTraceFromFileSeq;
import frameProcessing as fp;
import matplotlib.pyplot as plt;
import numpy as np;
from scipy import stats;
import time;

""" Compute result without displaying plot or intermediate results """
def main(repo,path,method,sizeWindow):
	
	array_hr_timestamp = [];
	array_hr_rppg = [];
	array_hr_rppg_filtered = [];
	array_hr_ppg = [];
	
	# Different process between method 1,2 and 3
	if(method == 3):
		# Initialization
		[files,signalPPG,isPPG] = initWithFolder3(repo,path);
		
		frameProcess = fp.AdvancedFrameProcessingSSR(files,repo,sizeWindow);
		# frameProcessing.run(0,signalPPG,isPPG);
		timestamps = [float(i) for i in np.transpose(files)[1]];
		
		maxIndex = len(timestamps)-1;
		
		l = 57;
		
		# for each frame
		for current in range(0,maxIndex+l):
			
			if(current % 50 == 0):
				print str(current) + "/" + str(maxIndex+l);
				
			[crtTimestamps_PPG, crtTimestamps_rPPG, rPPG, hr_rppg, hr_rppg_filtered, PPG, hr_ppg, FFT, G, PPG_FFT] = frameProcess.run(current,signalPPG,isPPG);
			
			# if we have a result, we add it to the final array
			if(hr_ppg != None):
				array_hr_ppg.append(hr_ppg);
				array_hr_rppg.append(hr_rppg);
				array_hr_rppg_filtered.append(hr_rppg_filtered);
				
				middle = ((current-sizeWindow+1-56)+current+1-56)/2;
				array_hr_timestamp.append(timestamps[middle]/1000);
				lastValue = hr_rppg_filtered;
	else:
		# Initialization
		[RGBTraces, timestamps, files, signalPPG, isPPG] = initWithFolder(repo,path);
		
		fs = 1/np.mean(np.diff(timestamps/1000));
		fs_ppg = 1/np.mean(np.diff(signalPPG[:,0]/float(1000)));
		
		maxIndex = len(timestamps);
		lastValue = -1;
		
		# for each frame
		for current in range(1,maxIndex):
			if(current % 50 == 0):
				print str(current) + "/" + str(maxIndex);
			
			frameProcess = fp.AdvancedFrameProcessing(RGBTraces,timestamps,current,sizeWindow,signalPPG,isPPG,method,fs,fs_ppg,lastValue);
			[crtTimestamps_PPG, crtTimestamps_rPPG, rPPG, hr_rppg, hr_rppg_filtered, PPG, hr_ppg, FFT, G, PPG_FFT] = frameProcess.run();
			
			# if we have a result, we add it to the final array
			if(current >= sizeWindow-1):
				array_hr_ppg.append(hr_ppg);
				array_hr_rppg.append(hr_rppg);
				array_hr_rppg_filtered.append(hr_rppg_filtered);
				
				middle = ((current-sizeWindow+1)+current+1)/2;
				array_hr_timestamp.append(timestamps[middle]/1000);
				lastValue = hr_rppg_filtered;
	
	# Display final results
	# Create result repository if not exist
	repoMethod = "";
	if(method == 1):
		repoMethod = "Chrom";
	elif(method == 2):
		repoMethod = "Green";
	elif(method == 3):
		repoMethod = "2SR";
	repoResult = "../Python-Result/" + repo;
	
	if not os.path.exists(repoResult):
		os.makedirs(repoResult);
		
	repoResult = repoResult + "/" + repoMethod;
			
	# Temporal signal
	plt.figure();
	plt.xlabel('Time (s)');
	plt.ylabel('HR');
	p1, = plt.plot(array_hr_timestamp,array_hr_ppg,'-g^',markersize=3);
	p2, = plt.plot(array_hr_timestamp,array_hr_rppg,'-b^',markersize=3);
	p3, = plt.plot(array_hr_timestamp,array_hr_rppg_filtered,'-r^',markersize=3);
	plt.ylim((40,210));
	plt.grid();
	plt.legend([p1, p2, p3], ["HR PPG", "HR rPPG", "HR rPPG filtered"]);
	
	plt.savefig(repoResult+"_Temporal.png");
	# plt.show();
	
	array_hr_rppg = np.array(array_hr_rppg);
	array_hr_rppg_filtered = np.array(array_hr_rppg_filtered);
	array_hr_ppg = np.array(array_hr_ppg);
	
	# getTraceFromFileSeq.saveFile1D(repoResult+"/res.csv",array_hr_rppg,'\n');
	
	estimation = computeEstimation(array_hr_rppg_filtered,array_hr_ppg,repoResult);
	
	#Bland-Altman Plot
	bland_altman_plot(array_hr_rppg_filtered,array_hr_ppg,repoResult,estimation);
	
	return [np.array(array_hr_ppg),np.array(array_hr_rppg_filtered)];
	
# Initialization for method 1 and 2
def initWithFolder(repo,path):
	# Read the xmp file of the signals.
	PPG_file = path + "gtdump.xmp";
	
	isPPG = True;
	
	# Check if there is a ppg file
	if not (os.path.isfile(PPG_file)):
		
		# Check if there is a ecg file otherwise
		ECG_repo = path + "ecg\\";
		
		# no : no video. Yes : extraction
		if (os.path.isfile(ECG_repo + "MIN1.txt")):
			isPPG = False;
			array_xmp = loadPPG.readECG(ECG_repo);
		else:
			return None;
	else:
		array_xmp = loadPPG.readXmp(path + "gtdump.xmp");

	# Read the csv file of the RGB spatial average. if not exist, create the csv
	file = path + "RGBtraces.csv";
	
	files = getTraceFromFileSeq.getImgList(path + "img");
	
	# load csv
	if not (os.path.isfile(file)):
		# If there's no files in the directory
		if(files == []):
			return None;
			
		print "Please wait, csv file is processing ..."
		getTraceFromFileSeq.extractAndSaveRGBTracesTracker(files,path,isPPG);

	[RGBTraces,timestamps] = getTraceFromFileSeq.readCsv(file);
	
	return [RGBTraces, timestamps, files, array_xmp, isPPG];
	
# Initialization for method 3
def initWithFolder3(repo,path):
	# Read the xmp file of the signals.
	PPG_file = path + "gtdump.xmp";
	
	isPPG = True;
	
	# Check if there is a ppg file
	if not (os.path.isfile(PPG_file)):
		
		# Check if there is a ecg file otherwise
		ECG_repo = path + "ecg\\";
		
		# no : no video. Yes : extraction
		if (os.path.isfile(ECG_repo + "MIN1.txt")):
			isPPG = False;
			array_xmp = loadPPG.readECG(ECG_repo);
		else:
			return None;
	else:
		array_xmp = loadPPG.readXmp(path + "gtdump.xmp");
	
	files = getTraceFromFileSeq.getImgList(path + "img");
	
	# all size
	K = len(files);
	
	return [files, array_xmp, isPPG];
	
# Compute the estimation values for the Blant 
def computeEstimation(data1, data2, repo):
	Array_Difference = np.abs(data1-data2);
	
	# Select values between 0 and 2.5 or 5
	Selected_Array_Difference_2_5 = [x for x in Array_Difference if x <= 2.5];
	Selected_Array_Difference_5 = [x for x in Array_Difference if x <= 5];

	Estimation_percentage_2_5 = float(len(Selected_Array_Difference_2_5))/len(data1);
	Estimation_percentage_5 = float(len(Selected_Array_Difference_5))/len(data1);
	
	M = float(len(Selected_Array_Difference_5));
	
	MAE = np.mean(Array_Difference);
	MAE_5 = np.mean(Selected_Array_Difference_5);
	
	RMSE = np.sqrt((1/M)*np.sum(np.power(Selected_Array_Difference_5, 2)));
	
	res = [Estimation_percentage_2_5,Estimation_percentage_5,MAE,MAE_5,RMSE];
	# getTraceFromFileSeq.saveFile1D(repo+"/estimation.csv",res,'\n');
	
	return res;
	
	
# Create a Blant Altman Plot
def bland_altman_plot(data1, data2, repo, estimation):
	data1     = np.asarray(data1)
	data2     = np.asarray(data2)
	mean      = np.mean([data1, data2], axis=0)
	diff      = data1 - data2                   # Difference between data1 and data2
	md        = np.mean(diff)                   # Mean of the difference
	sd        = np.std(diff, axis=0)            # Standard deviation of the difference
	
	# print diff;
	# BAP 1
	plt.figure();
	plt.scatter(mean, diff)
	plt.axhline(md,           color='gray', linestyle='--')
	plt.axhline(md + 1.96*sd, color='gray', linestyle='--')
	plt.axhline(md - 1.96*sd, color='gray', linestyle='--')
	plt.title('Bland-Altman Plot')
	plt.savefig(repo+"_Blant2.png");
	
	N = len(data1);
	gradient, intercept, r_value, p_value, std_err = stats.linregress(data1,data2);
	SSE = np.sqrt(np.sum((np.polyval([gradient,intercept],data1)-data2)**2)/(N-2));
	
	# BAP 2
	f = plt.figure();
	ax = f.add_subplot(111)
	plt.xlabel('HR');
	plt.ylabel('HR from PPG');
	plt.scatter(data1, data2)
	plt.plot(data1, (gradient*data1 + intercept));
	plt.title('Bland-Altman Plot');
	plt.text(0.01, 0.96,"n : " + str(N), transform=ax.transAxes, fontsize = 11);
	# plt.text(1,0, ,horizontalalignment='center',verticalalignment='center');
	plt.text(0.01, 0.92,"SSE : " + "{0:.2f}".format(SSE), transform=ax.transAxes, fontsize = 11);
	plt.text(0.01, 0.88,"y = " + "{0:.2f}".format(gradient) + "x+" + "{0:.2f}".format(intercept), transform=ax.transAxes, fontsize = 11);
	plt.text(0.01, 0.84,"$r^2$ : " + "{0:.2f}".format(r_value**2), transform=ax.transAxes, fontsize = 11);
	
	plt.text(0.75, 0.20,"PRECIS 2.5 : " + "{0:.2f}".format(estimation[0]), transform=ax.transAxes, fontsize = 11);
	plt.text(0.75, 0.16,"PRECIS 5 : " + "{0:.2f}".format(estimation[1]), transform=ax.transAxes, fontsize = 11);
	plt.text(0.75, 0.12,"MAE : " + "{0:.2f}".format(estimation[2]), transform=ax.transAxes, fontsize = 11);
	plt.text(0.75, 0.08,"MAE 5 : " + "{0:.2f}".format(estimation[3]), transform=ax.transAxes, fontsize = 11);
	plt.text(0.75, 0.04,"RMSE : " + "{0:.2f}".format(estimation[4]), transform=ax.transAxes, fontsize = 11);
	f.savefig(repo+"_Blant.png");
	# plt.show();
	
	plt.close('all');
	
	res = [gradient,intercept,r_value**2,p_value,std_err];
	# getTraceFromFileSeq.saveFile1D(repo+"/BAP-Python.csv",res,'\n');