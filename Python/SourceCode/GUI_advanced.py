# -*- coding: utf-8 -*-
"""
GUI for the methods Chrom, Green and 2SR

Form implementation generated from reading ui file 'GUI.ui'

Created with : PyQt4 UI code generator 4.11.4
"""

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import (QMainWindow, QApplication, QDialog)
import cv2;

from numpy import arange, sin, pi
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt;
import sys, os, random, time

import Controller_Advanced;
import getTraceFromFileSeq;
import TrackingWindow;

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)
		
""" Global variables useful for controller and for displaying in the GUI """
class GlobalVariables:
	def setVariables(self,repo,path,method,sizeWindow):
		self.repo = repo;
		self.path = path;
		self.method = method;
		self.sizeWindow = sizeWindow;
		
	def getVariables(self):
		return [self.repo, self.path, self.method, self.sizeWindow];
		
globalVariables = GlobalVariables();
"""END Global variables"""

"""A canvas where the plot will be display"""	
class myFigurePlot(FigureCanvas):
    
	def __init__(self, parent=None, width=5, height=4, dpi=100):
		fig = Figure(figsize=(width, height), dpi=dpi)
		self.axes = fig.add_subplot(111)

		# We want the axes cleared every time plot() is called
		self.axes.hold(False)

		FigureCanvas.__init__(self, fig)
		self.setParent(parent)

		FigureCanvas.setSizePolicy(self,
								   QtGui.QSizePolicy.Expanding,
								   QtGui.QSizePolicy.Expanding)
		FigureCanvas.updateGeometry(self)


"""A canvas that updates itself with a new plot."""
class myDynamicFigurePlot(myFigurePlot):
    
	def __init__(self, *args, **kwargs):
		myFigurePlot.__init__(self, *args, **kwargs)

	def update_figure(self,x,y,color,title,restrict=False):
		self.axes.plot(x,y,color);
		self.axes.set_title(title,fontsize=10);
		
		if(restrict):
			self.axes.set_xlim([40, 210]);
			
		self.draw();
		
	def update_multiple_figure(self,x1,y1,color1,x2,y2,color2,x3,y3,color3,title,restrict=False):
		self.axes.plot(x1,y1,color1,x2,y2,color2,x3,y3,color3);
		self.axes.legend(["PPG", "rPPG", "rPPG filtered"],prop={'size':7})
		self.axes.set_title(title,fontsize=10);
		
		if(restrict):
			self.axes.set_ylim([40, 210]);
			
		self.draw();
			
	def update_image(self,img,title):	
		self.axes.imshow(img);
		self.axes.axis('off');
		self.axes.set_title(title,fontsize=10);
		self.draw()

""" Display Informations Window """
class Ui_Informations(QDialog):
	def __init__(self):
		QDialog.__init__(self);
		self.setupUi();
		
	def setupUi(self):
		self.setObjectName(_fromUtf8("Informations"))
		self.resize(400, 300)
		self.label = QtGui.QLabel(self)
		self.label.setGeometry(QtCore.QRect(160, 10, 71, 16))
		self.label.setObjectName(_fromUtf8("label"))
		self.label_2 = QtGui.QLabel(self)
		self.label_2.setGeometry(QtCore.QRect(30, 70, 46, 13))
		self.label_2.setObjectName(_fromUtf8("label_2"))
		self.label_3 = QtGui.QLabel(self)
		self.label_3.setGeometry(QtCore.QRect(30, 120, 46, 16))
		self.label_3.setObjectName(_fromUtf8("label_3"))
		self.label_4 = QtGui.QLabel(self)
		self.label_4.setGeometry(QtCore.QRect(30, 170, 101, 16))
		self.label_4.setObjectName(_fromUtf8("label_4"))
		self.labelFolder = QtGui.QLabel(self)
		self.labelFolder.setGeometry(QtCore.QRect(190, 66, 181, 20))
		self.labelFolder.setObjectName(_fromUtf8("labelFolder"))
		self.labelMethod = QtGui.QLabel(self)
		self.labelMethod.setGeometry(QtCore.QRect(190, 120, 181, 16))
		self.labelMethod.setObjectName(_fromUtf8("labelMethod"))
		self.labelSize = QtGui.QLabel(self)
		self.labelSize.setGeometry(QtCore.QRect(190, 170, 181, 16))
		self.labelSize.setObjectName(_fromUtf8("labelSize"))
		self.pushButton = QtGui.QPushButton(self)
		self.pushButton.setGeometry(QtCore.QRect(300, 250, 75, 23))
		self.pushButton.setObjectName(_fromUtf8("pushButton"))
		
		self.pushButton.clicked.connect(self.toQuit)
		self.retranslateUi();
		QtCore.QMetaObject.connectSlotsByName(self)

	def retranslateUi(self):
		self.setWindowTitle(_translate("Informations", "Informations", None))
		self.label.setText(_translate("Informations", "Informations", None))
		self.label_2.setText(_translate("Informations", "Folder", None))
		self.label_3.setText(_translate("Informations", "Method", None))
		self.label_4.setText(_translate("Informations", "Size of the window", None))
		self.pushButton.setText(_translate("Informations", "OK", None))

		[repo, path, method, sizeWindow] = globalVariables.getVariables();
		self.labelFolder.setText(repo);
		self.labelMethod.setText(str(method));
		self.labelSize.setText(str(sizeWindow));
		
	def toQuit(self):
		self.reject();

""" Change Folder Window """
class Ui_Dialog(QDialog):
	def __init__(self):
		QDialog.__init__(self);
		self.setupUi();

	def setupUi(self):
		self.setObjectName(_fromUtf8("Dialog"))
		self.setWindowModality(QtCore.Qt.WindowModal)
		self.resize(400, 300)
		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setGeometry(QtCore.QRect(30, 240, 341, 32))
		self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
		self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
		self.LFolder = QtGui.QLabel(self)
		self.LFolder.setGeometry(QtCore.QRect(30, 70, 46, 16))
		self.LFolder.setObjectName(_fromUtf8("LFolder"))
		self.folderEdit = QtGui.QLineEdit(self)
		self.folderEdit.setGeometry(QtCore.QRect(160, 70, 141, 20))
		self.folderEdit.setObjectName(_fromUtf8("folderEdit"))
		self.methodEdit = QtGui.QLineEdit(self)
		self.methodEdit.setGeometry(QtCore.QRect(160, 120, 141, 20))
		self.methodEdit.setObjectName(_fromUtf8("methodEdit"))
		self.LMethod = QtGui.QLabel(self)
		self.LMethod.setGeometry(QtCore.QRect(30, 120, 46, 21))
		self.LMethod.setObjectName(_fromUtf8("LMethod"))
		self.LSize = QtGui.QLabel(self)
		self.LSize.setGeometry(QtCore.QRect(30, 170, 101, 21))
		self.LSize.setObjectName(_fromUtf8("LSize"))
		self.sizeEdit = QtGui.QLineEdit(self)
		self.sizeEdit.setGeometry(QtCore.QRect(160, 170, 141, 20))
		self.sizeEdit.setObjectName(_fromUtf8("sizeEdit"))
		self.LTitle = QtGui.QLabel(self)
		self.LTitle.setGeometry(QtCore.QRect(120, 10, 151, 16))
		self.LTitle.setObjectName(_fromUtf8("LTitle"))
		self.LError = QtGui.QLabel(self)
		self.LError.setGeometry(QtCore.QRect(30, 220, 600, 21))
		self.LError.setObjectName(_fromUtf8("LSize"))
		self.LError.setStyleSheet('color: red')

		self.retranslateUi(self)
		QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), self.changeParameters)
		QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), self.reject)
		QtCore.QMetaObject.connectSlotsByName(self)
		
	# modify the global variables with values in the fields
	def changeParameters(self):
		change = False;
		
		[repo, path, method, sizeWindow] = globalVariables.getVariables();
		if(self.folderEdit.text() != ""):
			tmpRepo = str(self.folderEdit.text())
			tmpPath = "../Videos/" + tmpRepo + "/"
			if(os.path.isdir(tmpPath)):
				change = True;
				repo = tmpRepo;
				path = tmpPath;
			
		if(self.methodEdit.text() != ""):
			try:
				method = int(self.methodEdit.text());
				change = True;
			except ValueError:
				change = False;
			
		if(self.sizeEdit.text() != ""):
			try:
				sizeWindow = int(self.sizeEdit.text());
				change = True;
			except ValueError:
				change = False;
		
		# Modify the variables
		if(change):
			globalVariables.setVariables(repo,path,method,sizeWindow);
			self.accept();
		else:
			self.LError.setText(_translate("Dialog", "Error : Please verify your parameters", None))

	def retranslateUi(self, Dialog):
		Dialog.setWindowTitle(_translate("Dialog", "New folder", None))
		self.LFolder.setText(_translate("Dialog", "Folder", None))
		self.LMethod.setText(_translate("Dialog", "Method", None))
		self.LSize.setText(_translate("Dialog", "Size of the window", None))
		self.LTitle.setText(_translate("Dialog", "Change folder or parameters", None))	

""" Main Window """		
class Ui_MainWindow(object):
		
	def setupUi(self, MainWindow):
		# UI
		MainWindow.setObjectName(_fromUtf8("MainWindow"))
		MainWindow.resize(803, 600)
		MainWindow.setLayoutDirection(QtCore.Qt.LeftToRight)
		self.centralwidget = QtGui.QWidget(MainWindow)
		self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
		self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
		self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
		self.frame = QtGui.QFrame(self.centralwidget)
		self.frame.setEnabled(True)
		self.frame.setMaximumSize(QtCore.QSize(16777215, 80))
		self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
		self.frame.setFrameShadow(QtGui.QFrame.Raised)
		self.frame.setLineWidth(1)
		self.frame.setObjectName(_fromUtf8("frame"))
		self.horizontalLayout = QtGui.QHBoxLayout(self.frame)
		self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
		self.frame_2 = QtGui.QFrame(self.frame)
		self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
		self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
		self.frame_2.setObjectName(_fromUtf8("frame_2"))
		self.horizontalLayout_3 = QtGui.QHBoxLayout(self.frame_2)
		self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
		
		# --------------- Buttons ---------------
		self.Bplay = QtGui.QToolButton(self.frame_2)
		icon = QtGui.QIcon()
		icon.addPixmap(QtGui.QPixmap(_fromUtf8("res/play.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
		self.Bplay.setIcon(icon)
		self.Bplay.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
		self.Bplay.setObjectName(_fromUtf8("Bplay"))
		self.horizontalLayout_3.addWidget(self.Bplay)
		self.Bplay.clicked.connect(self.playPause)
		
		self.Binfo = QtGui.QToolButton(self.frame_2)
		icon1 = QtGui.QIcon()
		icon1.addPixmap(QtGui.QPixmap(_fromUtf8("res/info.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
		self.Binfo.setIcon(icon1)
		self.Binfo.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
		self.Binfo.setObjectName(_fromUtf8("Binfo"))
		self.horizontalLayout_3.addWidget(self.Binfo)
		self.Binfo.clicked.connect(self.displayInfo)
		
		self.Bfolder = QtGui.QToolButton(self.frame_2)
		icon2 = QtGui.QIcon()
		icon2.addPixmap(QtGui.QPixmap(_fromUtf8("res/dossier.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
		self.Bfolder.setIcon(icon2)
		self.Bfolder.setIconSize(QtCore.QSize(20, 20))
		self.Bfolder.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
		self.Bfolder.setObjectName(_fromUtf8("Bfolder"))
		self.horizontalLayout_3.addWidget(self.Bfolder)
		self.Bfolder.clicked.connect(self.changeFolder)
		# --------------- END Buttons ---------------
		
		spacerItem = QtGui.QSpacerItem(100, 20, QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Minimum)
		self.horizontalLayout_3.addItem(spacerItem)
		self.horizontalLayout.addWidget(self.frame_2)
		self.frame_3 = QtGui.QFrame(self.frame)
		self.frame_3.setFocusPolicy(QtCore.Qt.NoFocus)
		self.frame_3.setFrameShape(QtGui.QFrame.StyledPanel)
		self.frame_3.setFrameShadow(QtGui.QFrame.Raised)
		self.frame_3.setObjectName(_fromUtf8("frame_3"))
		self.horizontalLayout_2 = QtGui.QHBoxLayout(self.frame_3)
		self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
		
		# --------------- Images ---------------
		self.img_uB = QtGui.QLabel(self.frame_3)
		self.img_uB.setText(_fromUtf8(""))
		self.img_uB.setPixmap(QtGui.QPixmap(_fromUtf8("res/uB.png")))
		self.img_uB.setAlignment(QtCore.Qt.AlignCenter)
		self.img_uB.setObjectName(_fromUtf8("img_uB"))
		self.horizontalLayout_2.addWidget(self.img_uB)
		self.img_LE2i = QtGui.QLabel(self.frame_3)
		self.img_LE2i.setText(_fromUtf8(""))
		self.img_LE2i.setPixmap(QtGui.QPixmap(_fromUtf8("res/LE2i.png")))
		self.img_LE2i.setAlignment(QtCore.Qt.AlignCenter)
		self.img_LE2i.setObjectName(_fromUtf8("img_LE2i"))
		self.horizontalLayout_2.addWidget(self.img_LE2i)
		self.img_Honda = QtGui.QLabel(self.frame_3)
		self.img_Honda.setText(_fromUtf8(""))
		self.img_Honda.setPixmap(QtGui.QPixmap(_fromUtf8("res/honda.png")))
		self.img_Honda.setAlignment(QtCore.Qt.AlignCenter)
		self.img_Honda.setObjectName(_fromUtf8("img_Honda"))
		self.horizontalLayout_2.addWidget(self.img_Honda)
		self.horizontalLayout.addWidget(self.frame_3)
		# --------------- END Images ---------------
		
		self.frame_2.raise_()
		self.frame_3.raise_()
		self.verticalLayout.addWidget(self.frame)
		self.frame_4 = QtGui.QFrame(self.centralwidget)
		self.frame_4.setFrameShape(QtGui.QFrame.StyledPanel)
		self.frame_4.setFrameShadow(QtGui.QFrame.Raised)
		self.frame_4.setObjectName(_fromUtf8("frame_4"))
		self.gridLayout = QtGui.QGridLayout(self.frame_4)
		self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
		
		#---------------- Plot ---------------
		self.W_img = myDynamicFigurePlot(self.frame_4)
		self.W_img.setObjectName(_fromUtf8("W_img"))
		self.gridLayout.addWidget(self.W_img, 0, 0, 2, 1)
		
		self.W_face = myDynamicFigurePlot(self.frame_4)
		self.W_face.setObjectName(_fromUtf8("W_face"))
		self.gridLayout.addWidget(self.W_face, 2, 0, 2, 1)
		
		self.W_skin = myDynamicFigurePlot(self.frame_4)
		self.W_skin.setObjectName(_fromUtf8("W_skin"))
		self.gridLayout.addWidget(self.W_skin, 4, 0, 2, 1)
		
		self.W_PPG = myDynamicFigurePlot(self.frame_4)
		self.W_PPG.setObjectName(_fromUtf8("W_PPG"))
		self.gridLayout.addWidget(self.W_PPG, 0, 1, 2, 1)
		
		self.W_rPPG = myDynamicFigurePlot(self.frame_4)
		self.W_rPPG.setObjectName(_fromUtf8("W_rPPG"))
		self.gridLayout.addWidget(self.W_rPPG, 2, 1, 2, 1)
		
		self.W_Green = myDynamicFigurePlot(self.frame_4)
		self.W_Green.setObjectName(_fromUtf8("W_Green"))
		self.gridLayout.addWidget(self.W_Green, 4, 1, 2, 1) # futur : FFT_PPG : 0,2
		
		self.W_FFT_PPG = myDynamicFigurePlot(self.frame_4)
		self.W_FFT_PPG.setObjectName(_fromUtf8("W_FFT_PPG"))
		self.gridLayout.addWidget(self.W_FFT_PPG, 0, 2, 2, 1) # futur : HR : 4, 2
		
		self.W_FFT_rPPG = myDynamicFigurePlot(self.frame_4)
		self.W_FFT_rPPG.setObjectName(_fromUtf8("W_FFT_rPPG"))
		self.gridLayout.addWidget(self.W_FFT_rPPG, 2, 2, 2, 1) #futur : Green : 2,2
		
		self.W_HR = myDynamicFigurePlot(self.frame_4) 
		self.W_HR.setObjectName(_fromUtf8("W_HR"))
		self.gridLayout.addWidget(self.W_HR, 4, 2, 2, 1) # futur : FFT_rPPG : 2,2
		
		
		
		self.verticalLayout.addWidget(self.frame_4)
		#---------------- END Plot ---------------
		
		self.frame.raise_()
		self.frame_4.raise_()
		MainWindow.setCentralWidget(self.centralwidget)
		self.menubar = QtGui.QMenuBar(MainWindow)
		self.menubar.setGeometry(QtCore.QRect(0, 0, 803, 21))
		self.menubar.setObjectName(_fromUtf8("menubar"))
		MainWindow.setMenuBar(self.menubar)
		self.statusbar = QtGui.QStatusBar(MainWindow)
		self.statusbar.setObjectName(_fromUtf8("statusbar"))
		MainWindow.setStatusBar(self.statusbar)

		self.retranslateUi(MainWindow)
		QtCore.QMetaObject.connectSlotsByName(MainWindow)
		
		self.toProcess();
		
	# connect the slot with the signal given by the controller 
	def toProcess(self):
		# Controller
		params = globalVariables.getVariables();
		self.controller = Controller_Advanced.Controller_Advanced(params);
		
		self.trackingWindow = TrackingWindow.TrackingWindow();
		path_img = self.controller.getFile();
		self.modifyFaceSkin(path_img,0);
		
		self.controller.return_signal.connect(self.updateFigures);
		self.controller.start();
		
	# Resume or pause a video
	def playPause(self):
		self.controller.playPause();
		
	# Display the Information window
	def displayInfo(self):
		self.controller.stopTemp();
		dialog = Ui_Informations();
		dialog.setWindowModality(QtCore.Qt.WindowModal);
		dialog.setAttribute(QtCore.Qt.WA_DeleteOnClose);
		dialog.exec_();
		self.controller.playTemp();
		
	# Update the window with new values
	def changeFolder(self):
		self.controller.stopTemp();
	
		dialog = Ui_Dialog();
		dialog.setWindowModality(QtCore.Qt.WindowModal)
		dialog.setAttribute(QtCore.Qt.WA_DeleteOnClose);
		
		# if there's a change
		if dialog.exec_():
			params = globalVariables.getVariables();
			self.controller.initialize(params);
			self.controller.playTemp();
		
	# Update all the figures in the window
	def updateFigures(self, data):
		[crtTimestamps_PPG, crtTimestamps_rPPG, rPPG, FFT, PPG, hr_rppg, hr_rppg_filtered, PPG_FFT, hr_ppg, Green, array_hr_timestamp, array_hr_rppg, array_hr_rppg_filtered, array_hr_ppg, path_img, current, maxIndex] = self.controller.getResult();
		
		# Update each figure :
		if(PPG != None):
			self.W_PPG.update_figure(PPG[0],PPG[1],'g','PPG');
			
			self.W_FFT_PPG.update_figure(PPG_FFT[0],PPG_FFT[1],'g','FFT PPG',True);
		
		self.W_rPPG.update_figure(crtTimestamps_rPPG,rPPG,'b','rPPG');
		
		print array_hr_timestamp;
		print array_hr_ppg;
		
		self.W_HR.update_multiple_figure(array_hr_timestamp,array_hr_ppg,'g',array_hr_timestamp,array_hr_rppg,'b',array_hr_timestamp,array_hr_rppg_filtered,'r','HR',True);
		
		if(not(Green is None)):
			self.W_Green.update_figure(crtTimestamps_PPG,Green,'g','Green');
			self.W_FFT_rPPG.update_figure(FFT[0],FFT[1],'b','FFT rPPG',True);
			
		if(hr_ppg is None):
			hr_ppg = 0;
		self.statusBar().showMessage("HR rPPG : "+"{0:.2f}".format(hr_rppg)+" bpm | HR PPG : "+"{0:.2f}".format(hr_ppg)+" bpm | "+str(current) + "/" + str(maxIndex));
		
		self.modifyFaceSkin(path_img,current);
		
	# Add on the GUI the current image, face and skin	
	def modifyFaceSkin(self,path_img,current):
		myImageCV = cv2.imread(path_img);
		myImageCV = cv2.cvtColor(myImageCV, cv2.COLOR_BGR2RGB);
		[face,skin] = getTraceFromFileSeq.face_skin_tracker(path_img,current,self.trackingWindow,False);
		
		self.W_img.update_image(myImageCV,'Frame');
		self.W_face.update_image(face,'Face');
		self.W_skin.update_image(skin,'Skin');
		
	def retranslateUi(self, MainWindow):
		MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
		self.Bplay.setText(_translate("MainWindow", "Play / Pause", None))
		self.Binfo.setText(_translate("MainWindow", "Informations", None))
		self.Bfolder.setText(_translate("MainWindow", "Folder", None))
	
""" Manager of the Main Window """	
class MainWindow(QMainWindow, Ui_MainWindow):
	def __init__(self, parent=None):
		super(MainWindow, self).__init__(parent)                                         
		self.setupUi(self)
		
def main(repo,path,method,sizeWindow):
	# Initialize values (from the main)
	app = QtGui.QApplication(sys.argv);
	globalVariables.setVariables(repo,path,method,sizeWindow);
	frame = MainWindow();
	
	# Show the window
	frame.show();
	app.exec_();