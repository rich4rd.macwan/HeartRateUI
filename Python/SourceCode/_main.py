import GUI_advanced;
import GUI_basic;
import quick_compute;

# Method 1 : CHROM with RGB and equations
# Method 2 : CHROM with green
# Method 3 : 2SR
method = 3;

# interface 1 : quick compute
# interface 2 : GUI Basic
# interface 3 : GUI Advanced
interface = 1;

# Repository of the video
repo = "12-gt";

# Path of the Videos folder
path = "../Videos/" + repo + "/";
# path = "/run/media/richard/841A-95A7/Le2i/" + repo + "/";
# path = "/media/yannick/Data/Dataset/HeartRate/RichardSummerVideos/" + repo + "/";

# Size of the window (number of frames)
# sizeWindow = 57; 			# 1
sizeWindow = 573; 			# 20
# sizeWindow = 859; 			# 30

# Launch appropriate function
if(interface == 1):
	quick_compute.main(repo,path,method,sizeWindow);
elif(interface == 2):
	GUI_basic.main(repo,path,method,sizeWindow);
elif(interface == 3):
	GUI_advanced.main(repo,path,method,sizeWindow);