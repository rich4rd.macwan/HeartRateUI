import GUI_advanced;
import quick_compute;
from glob import glob;
import os;
import numpy as np;
import time;

# Method 1 : Chrom
# Method 2 : Green
# Method 3 : 2SR
method = 3;

repoMethod = "";
if(method == 1):
	repoMethod = "Chrom";
elif(method == 2):
	repoMethod = "Green";
elif(method == 3):
	repoMethod = "2SR";
	
# interface 1 : quick compute
# interface 2 : GUI Advanced
interface = 1;

total_hr_ppg = [];
total_hr_rppg = [];

start_time = time.time();

# Repository of the video
for repo in os.walk('../Videos/').next()[1]:

	print "repo : " + repo;
	
	# Path of the Videos folder
	path = "../Videos/" + repo + "/";
	# path = "/run/media/richard/841A-95A7/Le2i/" + repo + "/";
	# path = "/media/yannick/Data/Dataset/HeartRate/RichardSummerVideos/" + repo + "/";

	# Size of the window (number of frames)
	sizeWindow = 573; 			# 20
	# sizeWindow = 859; 			# 30

	# Launch appropriate function
	if(interface == 1):
		[hr_ppg,hr_rppg] = quick_compute.main(repo,path,method,sizeWindow);
		if(total_hr_ppg == []):
			total_hr_ppg = hr_ppg;
			total_hr_rppg = hr_rppg;
		else:
			total_hr_ppg = np.concatenate((total_hr_ppg,hr_ppg),axis=0);
			total_hr_rppg = np.concatenate((total_hr_rppg,hr_rppg),axis=0);
			
	elif(interface == 2):
		GUI_advanced.main(repo,path,method,sizeWindow);
	
# Result for all the videos
if(interface == 1):	
	repoResult = "../Python-Result/global";
		
	if not os.path.exists(repoResult):
		os.makedirs(repoResult);
		
	repoResult = repoResult + "/" + repoMethod;
		
	estimation = quick_compute.computeEstimation(total_hr_rppg,total_hr_ppg,repoResult);
		
	#Bland-Altman Plot
	quick_compute.bland_altman_plot(total_hr_rppg,total_hr_ppg,repoResult,estimation);
	
print("--- %s seconds ---" % (time.time() - start_time));