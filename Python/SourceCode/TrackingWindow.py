import cv2;
import cv2.cv as cv;
import numpy as np;
import matplotlib.pyplot as plt;

# For manual detection
coords = [];

""" Create a Tracking window for face detection """
class TrackingWindow:

	# Constructor
	def __init__(self):
		self.track_window = None;
		self.roi_hist = None;
		global coords;
		coords = [];
		self.term_crit = ( cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1 ); # 10 -> 80
		
	# Initialize the tracking (first frame)
	def firstFrame(self,path):
		img = cv2.imread(path);
	
		self.track_window = ROI_face(img);
		# If no one face is detected : manual detection
		if(self.track_window is None):
			# if(path.split('/')[2] == "12-gt"):
			# h,w = img.shape[0:2];
			# self.track_window = 0,0,w,h;
			
			# Show the image
			fig = plt.figure()
			img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB);
			plt.imshow(img);
			
			# Draw horizontal and vertical lines at the current mouse position
			cursor = Cursor(plt.gca());
			plt.connect('motion_notify_event', cursor.mouse_move);

			# Event when clicked : top-left and bottom-right points
			def onclick(event):
				global ix, iy
				ix, iy = event.xdata, event.ydata

				global coords
				coords.append((int(ix), int(iy)));
				
				ax = plt.gca();
				ax.axhline(iy,color='b')  # the horiz line
				ax.axvline(ix,color='b')  # the vert line

				# End of the manual detection, close the figure
				if len(coords) == 2:
					fig.canvas.mpl_disconnect(cid)
					plt.close(fig);

				return coords

			# Give to the user the image and wait for the 2 clicks
			cid = fig.canvas.mpl_connect('button_press_event', onclick);
			plt.title("Please select the top-left and the bottom-right point to make the face detection");
			plt.show();
			
			# Get the final window
			self.track_window = coords[0][0],coords[0][1],coords[1][0]-coords[0][0],coords[1][1]-coords[0][1];
			global coords;
			coords = [];
			
		c,r,w,h = self.track_window;
		
		roi = img[r:r+h,c:c+w];
		
		# HSV
		hsv_roi =  cv2.cvtColor(roi, cv2.COLOR_BGR2HSV);
		mask = cv2.inRange(hsv_roi, np.array((0., 60.,32.)), np.array((180.,255.,255.))); # 60 -> 30
		
		self.roi_hist = cv2.calcHist([hsv_roi],[0],mask,[180],[0,180]);
		cv2.normalize(self.roi_hist,self.roi_hist,0,255,cv2.NORM_MINMAX);
		
		# cv2.imshow('roiFace',roi);
		# cv2.waitKey(0);
		
		return cv2.cvtColor(roi, cv2.COLOR_BGR2RGB);
	
	# For each frame (except the first one)
	def otherFrame(self,path):
		img = cv2.imread(path);
		
		# HSV
		hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV);
		dst = cv2.calcBackProject([hsv],[0],self.roi_hist,[0,180],1)

        # apply meanshift to get the new location
		ret, self.track_window = cv2.meanShift(dst, self.track_window, self.term_crit)
		
		# apply Camshift to get the new location
		# ret, self.track_window = cv2.CamShift(dst, self.track_window, self.term_crit)

		# Take the tracking window
		x,y,w,h = self.track_window;
		
		roiFace = img[y:y+h,x:x+w];
		# cv2.waitKey(0);
		
		# cv2.rectangle(img, (x,y), (x+w,y+h), 255,2)
		
		return cv2.cvtColor(roiFace, cv2.COLOR_BGR2RGB);
		
# Make the face detection without the tracker (for first frame mainly)
def ROI_face(img):
	face_cascade = cv2.CascadeClassifier('./lib/haarcascade_frontalface_default.xml');
	# faces = face_cascade.detectMultiScale(img2,minSize=(200,200)) #, 1.3, 5);
	faces = face_cascade.detectMultiScale(img, minSize=(200,200));
	face = [];

	for face in faces:
		cv2.rectangle(img,(face[0]-1,face[1]-1),
			   (face[0]+face[2],face[1]+face[3]),
			   cv.RGB(155, 55, 200),1);
			   
	if(face == []):
		return None;
	else:
		return (face[0],face[1],face[2],face[3]);
		
class Cursor(object):
    def __init__(self, ax):
        self.ax = ax
        self.lx = ax.axhline(color='w')  # the horiz line
        self.ly = ax.axvline(color='w')  # the vert line

    def mouse_move(self, event):
        if not event.inaxes:
            return

        x, y = event.xdata, event.ydata
        # update the line positions
        self.lx.set_ydata(y)
        self.ly.set_xdata(x)
		
        plt.draw()
	
