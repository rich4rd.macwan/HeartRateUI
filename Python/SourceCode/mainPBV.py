# Load image file list from the video folder, use Romain's code
import sys,os
import numpy as np
import matplotlib.pyplot as plt
import cv2
sys.path.append(os.path.realpath('../LegerRomain/SourceCode'))
#print sys.path
imagelist=fromVideo.getImgList("/run/media/richard/841A-95A7/Le2i/6-gt"+"/img")
print len(imagelist)," images to process"
#Initialize the nx3 pbv vector,and other variables 
Pbv=np.empty([3])
Cn=np.empty([3,len(imagelist)]) #rgba
mean_stddev=np.empty([3,2]) # Right now we use the temporal stddev for the whole video, can be done over a window
i=0
n=len(imagelist)

# Iterate through each file 
for tuple in imagelist:
	#Display frame works
	img=cv2.imread(tuple[0]);

	#Extract rgb averages
	#Currently we are not doing any face detection
	
	#img_roi=img
	Cn[:,i]=cv2.mean(img)[0:3]

	#cv2.imshow('image',img);
	#key=cv2.waitKey(10)
	# if key==27:
	# 	break
	if i%100==0:
		print i," of ",n
	i=i+1

#Calculate pbv
for i in range(0,3):
	mean_stddev[i]=cv2.meanStdDev(Cn[i,:])

#Mean centered normalize Cn
Cn= np.divide(Cn,np.transpose(np.tile(mean_stddev[:,0],(n,1))))-1

#Calculate Pbv denominator from eqn 7
Pbv_den=np.sqrt(mean_stddev[0,0]*mean_stddev[0,1]*mean_stddev[0,1]+mean_stddev[1,0]*mean_stddev[1,1]*mean_stddev[1,1])+mean_stddev[2,0]*mean_stddev[2,1]*mean_stddev[2,1]
Pbv=np.array([mean_stddev[0,0]*mean_stddev[0,1],mean_stddev[1,0]*mean_stddev[1,1],mean_stddev[2,0]*mean_stddev[2,1]])/Pbv_den
#print Pbv[:,i]
#for i in range (0,n):

#Calculate Wpbv
#Remove the last row of the mean to make the size 3xn
#mean=np.delete(mean,3,0)
Q=np.dot(Cn,np.transpose(Cn))

Wpbv=np.dot(Pbv,np.linalg.inv(Q))
#Scale Wpbv to 1
Wpbv=Wpbv/np.linalg.norm(Wpbv)
#TODO :choose k such that length of Wpbv=1
#Obtain S
S=np.dot(Wpbv,Cn)

#Display Signal
	
fig, ax = plt.subplots()
ax.plot(100*S,color='blue')
ax.set_ylim(-.5, .5)
plt.show()
