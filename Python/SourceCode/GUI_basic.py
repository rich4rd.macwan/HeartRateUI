# License: Creative Commons Zero (almost public domain) http://scpyce.org/cc0

"""
Example usage of matplotlibs widgets: Build a small 2d Data viewer.

Shows 2d-data as a pcolormesh, a click on the image shows the crossection
(x or y, depending on the mouse button) and draws a corresponding line in
the image, showing the location of the crossections. A reset button deletes all
crossections plots.

Works with matplotlib 1.0.1.
"""
import matplotlib;
from matplotlib.widgets import Cursor, Button, Line2D
import matplotlib.pyplot as plt
import numpy as np;
import os;
import loadPPG, getTraceFromFileSeq;
import frameProcessing as fp;
import matplotlib.pyplot as plt;
import time;
plt.rcParams['font.size']=10

class viewer_2d(object):
	def __init__(self):
		"""
		"""

		self.fig=plt.figure()
		#Doing some layout with subplots:
		self.fig.subplots_adjust(0.05,0.05,0.98,0.98,0.1)
		img = plt.imread("../Videos/5-gt/img/frame_0_0.ppm");
		
		self.overview=plt.subplot2grid((18,23),(4,0),rowspan=10,colspan=13)
		self.overview.axis('off');
		self.img = self.overview.imshow(img);
		# self.overview.autoscale(1,'both',1)
		
		# PPG SUBPLOT
		self.ppg_subplot=plt.subplot2grid((18,23),(1,14),rowspan=6,colspan=8)
		# plt.ylim(40, 210);
		
		self.ppg_line = Line2D([], [], color='g')
		self.rppg_line = Line2D([], [], color='r')
		
		self.ppg_subplot.add_line(self.ppg_line);
		self.ppg_subplot.add_line(self.rppg_line);
		
		# HR SUBPLOT
		self.hr_subplot=plt.subplot2grid((18,23),(8,14),rowspan=6,colspan=8)
		self.hr_subplot.set_ylim(40, 210);
		
		self.hr_ppg_line = Line2D([], [], color='g')
		self.hr_rppg_line = Line2D([], [], color='b')
		self.hr_rppg_filtered_line = Line2D([], [], color='r')
		
		self.hr_subplot.add_line(self.hr_ppg_line)
		self.hr_subplot.add_line(self.hr_rppg_line)
		self.hr_subplot.add_line(self.hr_rppg_filtered_line)
		
		#Adding widgets, to not be gc'ed, they are put in a list:
		cursor_ppg=Cursor(self. ppg_subplot, useblit=True, color='black', linewidth=1)
		cursor_hr=Cursor(self.hr_subplot, useblit=True, color='black', linewidth=1)
		
		but_ax=plt.subplot2grid((18,23),(0,0),rowspan=2,colspan=3)
		pause_button=Button(but_ax,'Play / Pause')
		
		but_ax2=plt.subplot2grid((18,23),(0,5),rowspan=2,colspan=3)
		info_button=Button(but_ax2,'Information')
		
		but_ax3=plt.subplot2grid((18,23),(0,10),rowspan=2,colspan=3)
		folder_button=Button(but_ax3,'Folder')
		
		self._widgets=[cursor_ppg,cursor_hr,pause_button,info_button,folder_button]
		
		#connect events
		pause_button.on_clicked(self.playPause)
		info_button.on_clicked(self.show_info)
		folder_button.on_clicked(self.choose_folder)
		
		#Text
		self.text_ax=plt.subplot2grid((18,23),(15,0),rowspan=4,colspan=23)
		self.text_ax.axis('off');
		
	# Update the ppg panel
	def update_figure_ppg(self,x1,y1,c1,x2,y2,c2):
		self.ppg_subplot.cla();
		self.ppg_subplot.plot(x1,y1,c1,x2,y2,c2);
		self.ppg_subplot.legend(["PPG", "rPPG"],prop={'size':8})
		self.ppg_subplot.set_title("PPG & rPPG",fontsize=11);
		# self.ppg_line.set_data(x1,y1);
		# self.rppg_line.set_data(x2,y2);
	
	# Update the hr panel	
	def update_figure_hr(self,x,y1,y2,y3):
		self.hr_subplot.cla();
		self.hr_subplot.set_ylim(40, 210);
		self.hr_subplot.plot(x,y1,'g',x,y2,'b',x,y3,'r');
		self.hr_subplot.legend(["PPG", "rPPG", "rPPG filtered"],prop={'size':8})
		self.hr_subplot.set_title("HR",fontsize=11);
		
	# Update the text panel
	def update_text(self,current,maxFrame,HR_PPG,HR_rPPG):
		self.text_ax.cla();
		self.text_ax.axis('off');
		self.text_ax.text(0.1, 0.6, "Frame " + str(current) + "/" + str(maxFrame),fontsize=12, color='b');
		self.text_ax.text(0.4, 0.6, "HR PPG : "+"{0:.2f}".format(HR_PPG)+" bpm",fontsize=12, color='g');
		self.text_ax.text(0.7, 0.6, "HR rPPG : "+"{0:.2f}".format(HR_rPPG)+" bpm",fontsize=12, color='r');
		
	# Processing of the video
	def process(self,repo,path,method,sizeWindow):
		array_hr_timestamp = [];
		array_hr_rppg = [];
		array_hr_rppg_filtered = [];
		array_hr_ppg = [];
		
		if(method == 3):
			# Initialization
			[files,signalPPG,isPPG] = initWithFolder3(repo,path);
			
			frameProcessing = fp.AdvancedFrameProcessingSSR(files,repo,sizeWindow);
			# frameProcessing.run(0,signalPPG,isPPG);
			timestamps = [float(i) for i in np.transpose(files)[1]];
			
			maxIndex = len(timestamps)-1;
			
			l = 57;
			
			# for each frame
			for current in range(0,maxIndex+l):
				
				if(current % 50 == 0):
					print str(current) + "/" + str(maxIndex+l);
					
				[crtTimestamps_PPG, crtTimestamps_rPPG, rPPG, hr_rppg, hr_rppg_filtered, PPG, hr_ppg, FFT, G, PPG_FFT] = frameProcessing.run(current,signalPPG,isPPG);
				
				# if we have a result, we add it to the final array
				
				if(hr_ppg != None):
					array_hr_ppg.append(hr_ppg);
					array_hr_rppg.append(hr_rppg);
					array_hr_rppg_filtered.append(hr_rppg_filtered);
					array_hr_timestamp.append(timestamps[current-l]/1000);
					
				# self.update_figure_ppg(crtTimestamps_PPG, PPG, crtTimestamps_rPPG, rPPG);
				# self.update_figure_hr(array_hr_timestamp, array_hr_ppg, array_hr_rppg, array_hr_rppg_filtered);
		else:
			# Initialization
			[RGBTraces, timestamps, files, signalPPG, isPPG] = initWithFolder(repo,path);
			fs = 1/np.mean(np.diff(timestamps/1000));
			fs_ppg = 1/np.mean(np.diff(signalPPG[:,0]/float(1000)));
			
			maxIndex = len(timestamps);
			lastValue = -1;
			
			# for each frame
			for current in range(1,maxIndex):
				img = plt.imread(files[current][0]);
				self.img.set_data(img)
				
				if(current % 50 == 0):
					print str(current) + "/" + str(maxIndex);
				
				frameProcess = fp.AdvancedFrameProcessing(RGBTraces,timestamps,current,sizeWindow,signalPPG,isPPG,method,fs,fs_ppg,lastValue);
				[crtTimestamps_PPG, crtTimestamps_rPPG, rPPG, hr_rppg, hr_rppg_filtered, PPG, hr_ppg, FFT, G, PPG_FFT] = frameProcess.run();
				
				# if we have a result, we add it to the final array
				if(current >= sizeWindow-1):
					array_hr_ppg.append(hr_ppg);
					array_hr_rppg.append(hr_rppg);
					array_hr_rppg_filtered.append(hr_rppg_filtered);
					middle = ((current-sizeWindow+1)+current+1)/2;
					array_hr_timestamp.append(timestamps[middle]/1000);
					lastValue = hr_rppg_filtered;
				
				rPPG_disp = [i * 1000 for i in rPPG]
					
				self.update_figure_ppg(PPG[0], PPG[1], 'g', crtTimestamps_rPPG, rPPG_disp, 'r');
				self.update_figure_hr(array_hr_timestamp, array_hr_ppg, array_hr_rppg, array_hr_rppg_filtered);
				self.update_text(current,maxIndex-1,hr_ppg,hr_rppg);
				start = time.clock();
				self.fig.canvas.flush_events();

				print str((time.clock() - start)*1000);
		
	def playPause(self,event):
		print "Pause";
    
	def show_info(self, event):
		print "Info";
		
	def choose_folder(self, event):
		print "Folder";
        
def main(repo,path,method,sizeWindow):
	plt.ion();
	#Build some strange looking data:
	x=np.array([0,1,2,3,4,5,6,7,8,9,10]);
	y=np.array([0,1,2,3,4,5,6,7,8,9,10]);
	#Put it in the viewer
	fig_v=viewer_2d()
	# fig_v2=viewer_2d(A)
	#Show it
	plt.show();
	fig_v.process(repo,path,method,sizeWindow);
	
# Initialization for method 1 and 2
def initWithFolder(repo,path):
	# Read the xmp file of the signals.
	PPG_file = path + "gtdump.xmp";
	
	isPPG = True;
	
	# Check if there is a ppg file
	if not (os.path.isfile(PPG_file)):
		
		# Check if there is a ecg file otherwise
		ECG_repo = path + "ecg\\";
		
		# no : no video. Yes : extraction
		if (os.path.isfile(ECG_repo + "MIN1.txt")):
			isPPG = False;
			array_xmp = loadPPG.readECG(ECG_repo);
		else:
			return None;
	else:
		array_xmp = loadPPG.readXmp(path + "gtdump.xmp");

	# Read the csv file of the RGB spatial average. if not exist, create the csv
	file = path + "RGBtraces.csv";
	
	files = getTraceFromFileSeq.getImgList(path + "img");
	
	# load csv
	if not (os.path.isfile(file)):
		# If there's no files in the directory
		if(files == []):
			return None;
			
		print "Please wait, csv file is processing ..."
		getTraceFromFileSeq.extractAndSaveRGBTracesTracker(files,path,isPPG);

	[RGBTraces,timestamps] = getTraceFromFileSeq.readCsv(file);
	
	return [RGBTraces, timestamps, files, array_xmp, isPPG];
	
# Initialization for method 3
def initWithFolder3(repo,path):
	# Read the xmp file of the signals.
	PPG_file = path + "gtdump.xmp";
	
	isPPG = True;
	
	# Check if there is a ppg file
	if not (os.path.isfile(PPG_file)):
		
		# Check if there is a ecg file otherwise
		ECG_repo = path + "ecg\\";
		
		# no : no video. Yes : extraction
		if (os.path.isfile(ECG_repo + "MIN1.txt")):
			isPPG = False;
			array_xmp = loadPPG.readECG(ECG_repo);
		else:
			return None;
	else:
		array_xmp = loadPPG.readXmp(path + "gtdump.xmp");
	
	files = getTraceFromFileSeq.getImgList(path + "img");
	
	# all size
	K = len(files);
	
	return [files, array_xmp, isPPG];