from scipy.signal import butter, lfilter, detrend
import numpy as np;
from scipy.sparse import spdiags
import scipy;
import matplotlib.pyplot as plt;

# Compute the Xs and the Ys of a normalized RGB
def compute_Xs_Ys(RGBn):
	return [3*RGBn[0] - 2*RGBn[1], 1.5*RGBn[0] + RGBn[1] - 1.5*RGBn[2]];

# Apply butter filter on a signal
def apply_filter(data,fs):
	lowcut = 0.3;
	highcut = 3.5;

	nyq = 0.5 * fs;
	low = lowcut / nyq;
	high = highcut / nyq;

	b, a = butter(8, [low, high], btype='band');

	y = lfilter(b, a, data);

	return y;

# Equation 14 : see paper for CHROM
def Xs_min_alpha_Ys(array_Xs,array_Ys,Fs):
	
	array_Xf_Yf = [apply_filter(array_Xs,Fs),apply_filter(array_Ys,Fs)];
	alpha = np.std(array_Xf_Yf[0])/np.std(array_Xf_Yf[1]);
	
	new_array_Xf_Yf = map(list, zip(*array_Xf_Yf));
	
	array_eq = [x[0] - alpha * x[1] for x in new_array_Xf_Yf];

	return array_eq;
	
# Detrend a signal given
def detrends(signal):
	T = len(signal);
	lam = 10;
	I  = np.identity(T);
	filt = [1,-2,1]* np.ones((1,T-2),dtype=np.int).T
	D2 = spdiags(filt.T, (range(0,3)),T-2,T);
	z_stat = np.dot((I-scipy.linalg.inv(I + np.power(lam,2) * np.dot((D2.T),D2))),signal);
	return z_stat;

def chromProcess(crtRGBTraces,crtTimestamps,method):
	# Temporal average of the window
	RGBTraces_mean = temporalAverage(crtRGBTraces);
	
	# Compute FS
	Fs = getFs(crtTimestamps);
	
	# CHROM : 1, Green : 2
	if(method == 2):
		green_normalize = (crtRGBTraces[:,1] - RGBTraces_mean[1])/np.std(crtRGBTraces[:,1])
		green_filtered = apply_filter(green_normalize,Fs);
		S = detrend(green_filtered);
	else:
		RGBTraces_normalize = crtRGBTraces/RGBTraces_mean;
			
		Xs_Ys = [compute_Xs_Ys(x) for x in RGBTraces_normalize];
		Xs_Ys = map(list, zip(*Xs_Ys));
		
		S = Xs_min_alpha_Ys(Xs_Ys[0],Xs_Ys[1],Fs);
	
	return S;
	
# Make the filter and the detrend on a signal given
def filter_detrend(signal,crtTimestamps):
	Fs = getFs(crtTimestamps);
	S_filtered = apply_filter(signal,Fs);
	S = detrends(S_filtered);
	return S;

# Compute the RGB average on some images
def temporalAverage(RGB_array):
	a = np.array(RGB_array);
	return np.mean(a, axis=0);
	
# Get the FS from timestamps
def getFs(crtTimestamps):
	max = 11;
	if(len(crtTimestamps) < 10):
		max = len(crtTimestamps);
		
	newTab = crtTimestamps[0:max];
		
	return np.mean([t - s for s, t in zip(newTab, newTab[1:])]);
