import numpy as np;
import scipy;
from matplotlib.mlab import psd;
from scipy.interpolate import interp1d;

# Return a second signal, with fft produced on it
def fft(array_time,array_signal):
	number_points = 1000;
	[array_time,array_signal,Fs] = interpolation(array_time,array_signal,number_points);

	N = len(array_time);

	# Hanning window
	window = np.hanning(N);
	acc = array_signal * window;
	
	number_points_fft = number_points*3;

	# FFT
	f = np.fft.fft(acc,n=number_points_fft);
	axis_y = abs(f);
	
	axis_x = 60 * np.arange(0, number_points_fft) * Fs/number_points_fft;

	return [axis_x,axis_y];

# Make the welch process on a signal
def welch_process(array_time,array_signal,fs,interp=False):

	number_points = 1000;
	
	if(interp):
		# array_signal = detrends(array_signal);
		[array_time,array_signal,Fs] = interpolation(array_time,array_signal,number_points);
		fs = Fs;
		
	length_signal = len(array_signal);
	number_points_fft = len(array_signal)*3;
		
	# [freq,power] = scipy.signal.welch(array_signal,fs,window='hamming',nfft=number_points_fft,nperseg=len(array_signal)/8);
	
	# MATLAB :[PPG_power,PPG_freq] = pwelch(crtPPGWin,[],[],length(crtPPGWin)*3,Fs_PPG, 'psd');
	[power,freq] = psd(array_signal,length_signal,fs,window=np.hamming(length_signal),noverlap=len(array_signal)/2,pad_to=number_points_fft);
	# if(length_signal < 256):
		# nfft = length_signal;
	# else:
		# nfft = 256;
	# [power,freq] = psd(array_signal,NFFT=nfft,Fs=fs,window=np.hamming(nfft),pad_to=number_points_fft);
	
	# [freq,power,fs] = interpolation(freq,power,number_points);
	
	axis_x = freq*60;
	axis_y = power;

	return [np.array(axis_x),np.array(axis_y)];

# Return a set of time and a set of the signal, with new gap and new number of points
def interpolation(array_time,array_signal,nbNewPoints):
	# Boundary
	minTime = array_time[0];
	maxTime = array_time[len(array_time) - 1];

	# Total gap
	totalGap = maxTime - minTime;

	# new one : -1 because we have to take the first and the last one
	new_gap = totalGap/(nbNewPoints-1);

	# New array of time. arg2, "+new_gap", because otherwise, do not take the last one
	new_array_time = np.arange(minTime,maxTime+new_gap,new_gap);

	# Fix the limit at the maximum number of points
	new_array_time_Max = new_array_time[0:nbNewPoints].copy();
	new_array_time_Max[len(new_array_time_Max)-1] = maxTime;

	#interp
	f = interp1d(array_time, array_signal);
		
	new_array_signal = f(new_array_time_Max);

	return [new_array_time_Max,new_array_signal,1/new_gap];

# Extract the heart beat from a graph
def extractHeartBeat(axis_x,axis_y,isFFT):
	# Restriction on 36,210
	if(isFFT):
		[ind1,ind2] = for_y_lim(axis_x,60*0.7, 60*3.5);
		axis_x_restrict = axis_x[ind1:ind2].copy();
		axis_y_restrict = axis_y[ind1:ind2].copy();
	else:
		axis_x_restrict = axis_x[:];
		axis_y_restrict = axis_y[:];
	
	# Compute Heart Beat
	if(len(axis_x_restrict) > 0):
		indMax = np.argmax(axis_y_restrict);
		heartBeat = axis_x_restrict[indMax];
	else:
		heartBeat = 0;
	
	return heartBeat;
	
def extractHeartBeatPeak(axis_x,axis_y,lastValue,isFFT):
	# Restriction on 36,210
	if(isFFT):
		[ind1,ind2] = for_y_lim(axis_x,60*0.7, 60*3.5);
		axis_x_restrict = axis_x[ind1:ind2].copy();
		axis_y_restrict = axis_y[ind1:ind2].copy();
	else:
		axis_x_restrict = axis_x[:];
		axis_y_restrict = axis_y[:];
	
	# Compute Heart Beat
	if(len(axis_x_restrict) > 0):
		if(lastValue == -1):
			# First value
			indMax = np.argmax(axis_y_restrict);
			heartBeat = axis_x_restrict[indMax];
			heartBeatNoF = heartBeat;
		else:
			[indPeak,valPeak] = findPeaks(axis_x_restrict,axis_y_restrict);
			
			heartBeat = indPeak[0];
			
			# Heuristic
			if(len(indPeak) > 1):
				[indPeak2,valPeak2] = restrictPeak(indPeak,valPeak,2);
				heartBeat = indPeak2[0];
				difference = np.abs(heartBeat-lastValue);
				
				heartBeatNoF = heartBeat;
				
				if(difference > 10):
					newHeartBeat = indPeak2[1];
					newDifference = np.abs(indPeak2[1]-lastValue)
					
					if(newDifference < difference):
						heartBeat = newHeartBeat;
						
			elif(len(indPeak) == 1):
				heartBeat = indPeak[0];
				heartBeatNoF = heartBeat;
			else :
				heartBeat = 0;
				heartBeatNoF = heartBeat;
				
	else:
		heartBeat = 0;
		heartBeatNoF = heartBeat;
	
	return [heartBeatNoF,heartBeat];
	
# Find y limits corresponding to the limits of x
def for_y_lim(axis_x,val1,val2):
	currentIndex = 0;
	while(val1 > axis_x[currentIndex]):
		currentIndex = currentIndex + 1;
		
	res1 = currentIndex;
	
	while(val2 > axis_x[currentIndex]):
		currentIndex = currentIndex + 1;
		
	res2 = currentIndex;
		
	return [res1,res2];
	
# Find peaks from a graph (order by index founded)
def findPeaks(tabInd,tabVal):
	peaksVal = [];
	peaksInd = [];
	meiVal = -1000;
	meiInd = -1000;
	valPrec = 0;
	asc = False;
	for ind,val in enumerate(tabVal):
		if (val >= meiVal):
			if(val < valPrec):
				asc = False;
			else:
				asc = True;
				
			meiVal = val;
			meiInd = ind;
			
		elif(asc==True):
			peaksVal.append(meiVal);
			peaksInd.append(tabInd[meiInd]);
			meiVal = -1000;
			meiInd = -1000;
			asc = False;
			
		else:
			asc = False;
			
		if(ind == len(tabVal)-1 and meiVal != -1000):
			peaksVal.append(meiVal);
			peaksInd.append(tabInd[meiInd]);
			
		valPrec = val;

	return (peaksInd,peaksVal);
	
# Order the array by high value and restrict number of peaks by a number given
def restrictPeak(peaksInd,peaksVal,numberOfPeaks):
	res_peaksVal = [];
	res_peaksInd = [];
	
	traite_peaksInd = peaksInd[:];
	traite_peaksVal = peaksVal[:];
	
	if(numberOfPeaks > len(peaksVal)):
		raise Exception("number of peaks lower that the restriction. Please decrease the restriction value");
	
	for i in range(0,numberOfPeaks):
		meiVal = -1000;
		meiInd = -1000;
		
		for ind,val in enumerate(traite_peaksVal):
			if (val > meiVal):
				meiVal = val;
				meiInd = ind;
				
		res_peaksVal.append(traite_peaksVal[meiInd]);
		res_peaksInd.append(traite_peaksInd[meiInd]);
		
		traite_peaksVal.remove(traite_peaksVal[meiInd]);
		traite_peaksInd.remove(traite_peaksInd[meiInd]);
		
	return (res_peaksInd,res_peaksVal);