import frameProcessing as fp;
from PyQt4 import QtCore;
import numpy as np, os;
import TrackingWindow;
import loadPPG, getTraceFromFileSeq;
import json;
import time;

""" Manager of frames """
class Controller_Advanced(QtCore.QThread):
	
	# Connection with the Qt Application
	return_signal = QtCore.pyqtSignal(object)

	# Constructor
	def __init__(self, params):
		QtCore.QThread.__init__(self, parent=None);
		
		self.initialize(params);
		
	# Initialization of the controller
	def initialize(self,params):
	
		self.repo = params[0];
		self.path = params[1];
		self.method = params[2];
		self.sizeWindow = params[3];
		
		if(self.method == 3):
			[self.files,self.signalPPG,self.isPPG] = initWithFolder3(self.repo,self.path);
			
			self.frameProcessing = fp.AdvancedFrameProcessingSSR(self.files,self.repo,self.sizeWindow);
			self.frameProcessing.run(0,self.signalPPG,self.isPPG);
			self.timestamps = [float(i) for i in np.transpose(self.files)[1]];
			
		else:
			[self.RGBTraces, self.timestamps, self.files, self.signalPPG, self.isPPG] = initWithFolder(self.repo,self.path);
		
		# My attributes -------
		self.current = 0;
		self.maxIndex = len(self.timestamps)-1;
		self.onRun = True;
		
		# To return attributes
		self.crtTimestamps_PPG = None;
		self.crtTimestamps_rPPG = None;
		self.rPPG = None;
		self.PPG = None;
		self.FFT = None;
		self.PPG_FFT = None;
		self.array_hr_timestamp = [];
		self.array_hr_rppg = [];
		self.array_hr_rppg_filtered = [];
		self.array_hr_ppg = [];
		self.path_img = self.files[0][0];
		
		self.l = 57;
		self.fs = 1/np.mean(np.diff(np.array(self.timestamps)/1000));
		self.fs_ppg = 1/np.mean(np.diff(self.signalPPG[:,0]/1000));
		
	# Manage the progression in the video (frame manager)
	def run(self):
		# Different process by methods
		if(self.method == 3):
			while (self.current < self.maxIndex+self.l):
				# if not paused
				if(self.onRun == True):
					self.current = self.current + 1;
					if(self.files != []):
						self.path_img = self.files[self.current][0];
						
					[self.crtTimestamps_PPG, self.crtTimestamps_rPPG, self.rPPG, self.hr_rppg, self.hr_rppg_filtered, self.PPG, self.hr_ppg, self.FFT, self.G, self.PPG_FFT] = self.frameProcessing.run(self.current,self.signalPPG,self.isPPG);
					# if we have a result, we add it to the final array
					if(self.hr_ppg != None):
						self.array_hr_ppg.append(self.hr_ppg);
						self.array_hr_rppg.append(self.hr_rppg);
						self.array_hr_rppg_filtered.append(self.hr_rppg_filtered);
						middle = ((self.current-self.l+1)+self.current+1)/2;
						self.array_hr_timestamp.append(self.timestamps[middle]/1000);
						
					# Waiting for GUI update
					if(self.current >= self.maxIndex):
						time.sleep(0.7);
						
					# Send signal to the application
					self.return_signal.emit("uploaded");
		else:
			lastValue = -1;
			while (self.current < self.maxIndex):
				# if not pause
				if(self.onRun == True):
					self.current = self.current + 1;
					if(self.files != []):
						self.path_img = self.files[self.current][0];
						
					# Get the result from the process of one frame
					frameProcess = fp.AdvancedFrameProcessing(self.RGBTraces,self.timestamps,self.current,self.sizeWindow,self.signalPPG,self.isPPG,self.method,self.fs,self.fs_ppg,lastValue);
					[self.crtTimestamps_PPG, self.crtTimestamps_rPPG, self.rPPG, self.hr_rppg, self.hr_rppg_filtered, self.PPG, self.hr_ppg, self.FFT, self.G, self.PPG_FFT] = frameProcess.run();
					
					# if we have a result, we add it to the final array
					if(self.current >= self.sizeWindow-1):
						self.array_hr_ppg.append(self.hr_ppg);
						self.array_hr_rppg.append(self.hr_rppg);
						self.array_hr_rppg_filtered.append(self.hr_rppg_filtered);
						middle = ((self.current-self.l+1)+self.current+1)/2;
						print middle;
						self.array_hr_timestamp.append(self.timestamps[middle]/1000);
				
					time.sleep(0.7);
							
					# Send signal to the application
					self.return_signal.emit("uploaded");
		
	# return all the array results
	def getResult(self):
		return [self.crtTimestamps_PPG, self.crtTimestamps_rPPG, self.rPPG, self.FFT, self.PPG, self.hr_rppg, self.hr_rppg_filtered, self.PPG_FFT, self.hr_ppg, self.G, self.array_hr_timestamp, self.array_hr_rppg, self.array_hr_rppg_filtered, self.array_hr_ppg, self.path_img,self.current, self.maxIndex];
	
	# Return the current frame path
	def getFile(self):
		return self.path_img;
		
	# pause or play the process
	def playPause(self):
		self.onRun = not(self.onRun);
		
	# pause the process
	def stopTemp(self):
		self.onRun = False;
		
	# continue the process
	def playTemp(self):
		self.onRun = True;
		
	# stop definitely the process
	def stop(self):
		self.onRun = False;
		self.terminate();
		
# Initialize the process by some parameters (files, PPG ...) for methods 1 and 2
def initWithFolder(repo,path):
	# Read the xmp file of the signals.
	PPG_file = path + "gtdump.xmp";
	
	isPPG = True;
	
	# Check if there is a ppg file
	if not (os.path.isfile(PPG_file)):
		
		# Check if there is a ecg file otherwise
		ECG_repo = path + "ecg\\";
		
		# no : no video. Yes : extraction
		if (os.path.isfile(ECG_repo + "MIN1.txt")):
			isPPG = False;
			array_xmp = loadPPG.readECG(ECG_repo);
		else:
			return None;
	else:
		array_xmp = loadPPG.readXmp(path + "gtdump.xmp");

	# Read the csv file of the RGB spatial average. if not exist, create the csv
	file = path + "RGBtraces.csv";
	
	files = getTraceFromFileSeq.getImgList(path + "img");
	
	# load csv
	if not (os.path.isfile(file)):
		# If there's no files in the directory
		if(files == []):
			return None;
			
		print "Please wait, csv file is processing ..."
		getTraceFromFileSeq.extractAndSaveRGBTraces(files,path,isPPG);

	[RGBTraces,timestamps] = getTraceFromFileSeq.readCsv(file);
	
	return [RGBTraces, timestamps, files, array_xmp, isPPG];
	
# Initialize the process by some parameters (files, PPG ...) for method 3
def initWithFolder3(repo,path):
	# Read the xmp file of the signals.
	PPG_file = path + "gtdump.xmp";
	
	isPPG = True;
	
	# Check if there is a ppg file
	if not (os.path.isfile(PPG_file)):
		
		# Check if there is a ecg file otherwise
		ECG_repo = path + "ecg\\";
		
		# no : no video. Yes : extraction
		if (os.path.isfile(ECG_repo + "MIN1.txt")):
			isPPG = False;
			array_xmp = loadPPG.readECG(ECG_repo);
		else:
			return None;
	else:
		array_xmp = loadPPG.readXmp(path + "gtdump.xmp");
	
	files = getTraceFromFileSeq.getImgList(path + "img");
	
	return [files, array_xmp, isPPG];