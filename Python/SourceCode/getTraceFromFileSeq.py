import cv2;
import cv2.cv as cv;
import numpy as np;
import os, glob;
import matplotlib.pyplot as plt;
import TrackingWindow;
import warnings;
# from sklearn import svm;

# ------------ Read csv ----------
def readCsv(file):
	all_data = np.genfromtxt(open(file), delimiter=',', dtype='f8')[:];
	return [all_data[:,0:3],all_data[:,3]]

# From a folder with images in ppm format, create an array as each values is [path, timestamp] of the images, classified by frame number
def getImgList(folder):
	dir = os.path.join(folder,"*.ppm");

	files = glob.glob(dir);

	# Create an array with files sorted by number, and the timeStamp associated
	array_path_timeStamp = ["" for x in range(len(files))];

	for name in files:
		# 0 : path, 1 : framenumber, 2 : timeStamp".ppm"
		tabSplit = name.split('_');

		# 0 : timeStamp, 1 : "ppm"
		tabSplitTime = tabSplit[2].split('.');

		frameNumber = int(tabSplit[1]);

		timeStamp = int(tabSplitTime[0]);

		array_path_timeStamp[frameNumber] = [name,timeStamp];

	return array_path_timeStamp;

# ------------ Extract And Save RGBTraces ----------
def extractAndSaveRGBTraces(images_timestamp,repo,isPPG):
	spatial_averages = [];
	
	#For every frames
	for [path,ts] in images_timestamp:
		print ts;
		#ROI, skin, spatialAverage
		bgr_img = all_functions_frames(path,ts,isPPG)

		rgb_img = bgr_img[::-1];
		
		rgb_img_ts = rgb_img + (ts,);

		spatial_averages.append(rgb_img_ts);

	saveFile(repo+"RGBtraces.csv",spatial_averages,'\n');
	
# ------------ Extract And Save RGBTraces ----------
def extractAndSaveRGBTracesTracker(images_timestamp,repo,isPPG):
	spatial_averages = [];
	current = 0;
	trackingWindow = TrackingWindow.TrackingWindow();
	
	#For every frames
	for [path,ts] in images_timestamp:
		
		#ROI, skin, spatialAverage
		rgb_img = all_functions_frames_tracker(path,current,trackingWindow,isPPG);

		# rgb_img = bgr_img[::-1];
		
		rgb_img_ts = rgb_img + (ts,);

		spatial_averages.append(rgb_img_ts);
		
		current = current + 1;

	saveFile(repo+"RGBtraces-Gen.csv",spatial_averages,'\n');

# ------------ For CHROM (Face, Skin Detection and spatial average) ----------
def all_functions_frames(path,timestamp,isPPG):
	if(isPPG):
		img = face_skin(path,onlySkin=True);
	else:
		img = cv2.imread(path);
		
	img = img.astype('float');

	rgbAverage = spatialAverage(img);
	return rgbAverage;
	
def all_functions_frames_tracker(path,current,trackingWindow,isPPG):
	if(isPPG):
		img = face_skin_tracker(path,current,trackingWindow,onlySkin=True);
	else:
		img = cv2.imread(path);
		
	img = img.astype('float');

	rgbAverage = spatialAverage(img);
	return rgbAverage;
	
# ------------ Face and Skin Detection ----------
# With face detection
def face_skin(file,onlySkin):
	img = cv2.imread(file);
	
	img_roi = ROI_face(img);

	if(img_roi == []):
		img_roi = img;
		
	img_res = skinDetection(img_roi.copy())
	
	if(onlySkin):
		return img_res;
	else:
		return [img_roi,img_res];
	
# With face tracker
def face_skin_tracker(file,current,trackingWindow,onlySkin):
	img_roi = ROI_face_tracker(file,current,trackingWindow);

	if(img_roi == []):
		img_roi = img;

	img_res = skinDetection(img_roi.copy());
		
	if(onlySkin):
		return img_res;
	else:
		return [img_roi,img_res];

# ------------ Face Detection ----------
# Detect the ROI with opencv2
def ROI_face(img2):
	face_cascade = cv2.CascadeClassifier('./lib/haarcascade_frontalface_default.xml');
	faces = face_cascade.detectMultiScale(img2, 1.3, 5);
	out_roi = [];

	for face in faces:
		cv2.rectangle(img2,(face[0]-1,face[1]-1),
			   (face[0]+face[2],face[1]+face[3]),
			   cv.RGB(155, 55, 200),1);

		out_roi = img2[face[1]:face[1]+face[3], face[0]:face[0]+face[2]];

	return out_roi;
	
# Detect the ROI with Tracker
def ROI_face_tracker(file,current,trackingWindow):
	if(current == 0):
		out_roi = trackingWindow.firstFrame(file);
	else:
		out_roi = trackingWindow.otherFrame(file);
	
	return out_roi;
	
# ------------ Skin detection ----------
def skinDetection(img_roi):
	skin = oc_skinProbability(img_roi);
	# eroded = cv2.erode(skin, np.ones((5, 5)))
	# skin = cv2.dilate(eroded, np.ones((3, 3)))
	return skin;

# ------------ Spatial average ----------
def spatialAverage(threshold):

	min_th = np.array([1,1,1],np.uint8);
	max_th = np.array([255,255,255],np.uint8);

	# threshold
	mask = cv2.inRange(threshold,min_th,max_th);

	res = cv2.mean(threshold,mask);
	# Select BGR
	res = res[:3];

	return res;

# ------------ Save file ----------
# 2D array
def saveFile(fName,array,newLine):
	file = '';
	for line in array:
		for case in line:
			file = file + str(case) + ",";
		file = file[:-1];
		file = file + newLine;

	f = open(fName, 'w');
	f.write(file);
	f.close();
	
# 1D array
def saveFile1D(fName,array,newLine):
	file = '';
	for line in array:
		file = file + str(line) + ",";
		file = file[:-1];
		file = file + newLine;

	f = open(fName, 'w');
	f.write(file);
	f.close();
	
"""
==========================================
One-class SVM with non-linear kernel (RBF)
==========================================
"""
# kernel = np.ones((9,9),np.float32)/81;

# def oc_svm(traindata):
	# fit the model
	# global clf;
	# clf = svm.OneClassSVM(nu=0.1, kernel="rbf", gamma=0.1,cache_size=10000,verbose=True);
	# clf.fit(traindata);
	# print('Classifier ready');
	# return clf;

#Method for skin detection. Ported from matlab (Original author: oconaire@eeng.dcu.ie)
def oc_skinProbability(img_roi):
	imgD=img_roi.astype('double');
	skinprob = computeSkinProbability(imgD);
	mask=(skinprob>0)+0;
	maskRGB = np.zeros((skinprob.shape[0],skinprob.shape[1],3));
	maskRGB[:,:,0] = mask;
	maskRGB[:,:,1] = mask;
	maskRGB[:,:,2] = mask;
	img_roi[:,:,0]=img_roi[:,:,0]*mask;
	img_roi[:,:,1]=img_roi[:,:,1]*mask;
	img_roi[:,:,2]=img_roi[:,:,2]*mask;
	# imgD[imgD==0]=np.nan
	# cv2.imshow('img_roi',img_roi)
	# cv2.waitKey(2)
	return img_roi;
	
	
smodel=[]
def computeSkinProbability(im):
	global smodel;
	if (smodel==[]):
		#load skin model
		# print 'Initialize skin model';
		skinmodfn = 'lib/skinmodel.bin';
		fid = open(skinmodfn, 'rb');
		tmp=np.fromfile(fid,np.float32);
		fid.close();
		K = 32;
		smodel = np.zeros((K,K,K));
		smodel = np.reshape(tmp.ravel(),(K,K,K),order='F');
		del tmp;
    
	if (im.shape[2] != 3):
		warnings.warn('Input image does not have 3 bands. RGB image required.');
		skinprob = [];
		return
	
	im2 = np.floor(im[:,:,0]/8)+np.floor(im[:,:,1]/8)*32+np.floor(im[:,:,2]/8)*32*32; # 32 bins

	skinprob = np.ravel(smodel,order='F')[im2.astype('int32')];
	
	return skinprob;