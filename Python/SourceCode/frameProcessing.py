import loadPPG;
import getTraceFromFileSeq;
import getPulseSignalFromTrace;
import getHRFromPulse;
import matplotlib.pyplot as plt;
import TrackingWindow;
import numpy as np;
import math;
import time;

# SIMPLE class
class FrameProcessing():

    # Processing
    def __init__(self,RGBTraces,timestamps,currentRight,sizeWindow,method):
		self.RGBTraces = RGBTraces;
		self.timestamps = timestamps;
		self.currentRight = currentRight;
		self.sizeWindow = sizeWindow;
		self.method = method;

    def run(self):
		# Extract values
		[crtRGBTraces,crtTimestamps] = getCrtFrames(self.RGBTraces,self.timestamps,self.currentRight,self.sizeWindow);

		# Pre Processing
		rPPG = getPulseSignalFromTrace.chromProcess(crtRGBTraces,crtTimestamps*1000,self.method);

		# FFT
		[rPPG_spec_axis,rPPG_spec] = getHRFromPulse.fft(crtTimestamps,rPPG);
		
		# Welch
		# [rPPG_spec_axis,rPPG_spec] = getPulseSignalFromTrace.welch(crtTimestamps,rPPG);

		# Extract Heart Rate
		hr_rppg = getHRFromPulse.extractHeartBeat(rPPG_spec_axis,rPPG_spec,isFFT=True);
		
		return [crtTimestamps,rPPG,hr_rppg];
		
		
# ADVANCED class
class AdvancedFrameProcessing():

    # Processing
    def __init__(self,RGBTraces,timestamps,currentRight,sizeWindow,signalPPG,isPPG,method,fs,fs_ppg,lastValue):
		self.RGBTraces = RGBTraces;
		self.timestamps = timestamps;
		self.currentRight = currentRight;
		self.sizeWindow = sizeWindow;
		self.signalPPG = signalPPG;
		self.isPPG = isPPG;
		self.method = method;
		self.fs = fs;
		self.fs_ppg = fs_ppg;
		self.lastValue = lastValue;

    def run(self):
		# Extract values
		[crtRGBTraces,crtTimestamps] = getCrtFrames(self.RGBTraces,self.timestamps,self.currentRight,self.sizeWindow);
		
		# Pre Processing
		rPPG = getPulseSignalFromTrace.chromProcess(crtRGBTraces,crtTimestamps*1000,self.method);

		# FFT
		# [rPPG_spec_axis,rPPG_spec] = getHRFromPulse.fft(crtTimestamps,rPPG);
		[rPPG_spec_axis,rPPG_spec] = getHRFromPulse.welch_process(crtTimestamps,rPPG,self.fs,interp=True);

		if(self.isPPG):
			PPG = loadPPG.loadCrtPPG(crtTimestamps[0]*1000,crtTimestamps[len(crtTimestamps)-1]*1000,self.signalPPG);
		else:
			PPG = loadPPG.loadCrtECG(crtTimestamps[0]*1000,crtTimestamps[len(crtTimestamps)-1]*1000,self.signalPPG);
		
		# PPG FFT
		# [PPG_spec_axis,PPG_spec] = getHRFromPulse.fft(PPG[0],PPG[1]);
		[PPG_spec_axis,PPG_spec] = getHRFromPulse.welch_process(PPG[0],PPG[1],self.fs_ppg,interp=False);

		# Extract Heart Rate
		[hr_rppg,hr_rppg_filtered] = getHRFromPulse.extractHeartBeatPeak(rPPG_spec_axis,rPPG_spec,self.lastValue,isFFT=True);
		hr_ppg = getHRFromPulse.extractHeartBeat(PPG_spec_axis,PPG_spec,isFFT=True);
		
		# [PPG_spec_axis,PPG_spec] = getHRFromPulse.restrict_axis(PPG_spec_axis,PPG_spec);
		
		G = crtRGBTraces[:,1];
		FFT = [rPPG_spec_axis,rPPG_spec];
		PPG_FFT = [PPG_spec_axis,PPG_spec];

		return [crtTimestamps, crtTimestamps, rPPG, hr_rppg, hr_rppg_filtered, PPG, hr_ppg, FFT, G, PPG_FFT];
		
		
# 2SR
# SIMPLE class
class FrameProcessingSSR():

	# Initialization of the class attributes
	def __init__(self,files,repo,sizeWindow):
		
		self.timestamps = [int(i) for i in np.transpose(files)[1]];
		self.files_name = np.transpose(files)[0];
		self.l = 57;
		self.sizeWindow = sizeWindow;
		
		self.trackingWindow = TrackingWindow.TrackingWindow();
		self.outfile = "..\\Result\\Method_3\\"+repo+".csv";
		
		self.maxIndex = len(self.files_name);
		
		self.P = np.zeros(len(self.files_name));
		
		self.array_lambda = [];
		self.array_eigenvectors = [];
		
		self.lastValue = -1;
		
		array_timestamp_float = [float(x) / 1000 for x in self.timestamps];
		self.fs = 1/np.mean(np.diff(array_timestamp_float));
		
	# Make the process
	def run(self,k):
		crtTimestamps = self.timestamps[0:k+1];
	
		# get skin pixels
		Vk = getSkinMatrixTracker(self.files_name[k],k,self.trackingWindow);
		
		# Number of skin pixels
		N = len(Vk);

		Ck = (np.dot(np.transpose(Vk),Vk))/N;
		
		#Ak : eigen values, Uk : eigen vectors
		[Ak,Uk] = np.linalg.eig(Ck);
		
		self.array_lambda.append(-Ak);
		self.array_eigenvectors.append(-Uk);
		
		tho = k-self.l+1;
		
		hr_rppg = 0;
		
		# for computing explanation : see method 2SR
		
		if(tho >= 0):
			arraySR = [];
			
			for t in range(tho,k+1):
				
				firstPart = np.dot(np.dot(np.dot(np.sqrt((self.array_lambda[t][0])/(self.array_lambda[tho][1])),np.transpose(self.array_eigenvectors[t][:,0])),self.array_eigenvectors[tho][:,1]),np.transpose(self.array_eigenvectors[tho][:,1]));
				secondPart = np.dot(np.dot(np.dot(np.sqrt((self.array_lambda[t][0])/(self.array_lambda[tho][2])),np.transpose(self.array_eigenvectors[t][:,0])),self.array_eigenvectors[tho][:,2]),np.transpose(self.array_eigenvectors[tho][:,2]));
				
				currentSR = firstPart + secondPart;
				
				arraySR.append(currentSR);
				
			RGB_arraySR = np.transpose(np.array(arraySR));
			
			p = RGB_arraySR[0] - (np.std(RGB_arraySR[0])/np.std(RGB_arraySR[1])) * RGB_arraySR[1];

			self.P[tho:tho+self.l] = self.P[tho:tho+self.l] + (p - np.mean(p));
			
			current_hr_index = tho-self.sizeWindow+1;
			
			# We can display the HR result
			if(current_hr_index >= 0):
				# print "index : " + str(current_hr_index);
				
				signal = self.P[current_hr_index : current_hr_index+self.sizeWindow];
				current_timestamp = self.timestamps[current_hr_index : current_hr_index+self.sizeWindow];
				
				# Welch for frequencies
				[rPPG_spec_axis,rPPG_spec] = getHRFromPulse.welch_process(current_timestamp,signal,self.fs);
				rPPG_spec_axis = np.array([math.fabs(x) for x in rPPG_spec_axis]);
				rPPG_spec = np.array([math.fabs(x) for x in rPPG_spec]);
				
				# Extraction of the heart beat
				[hr_rppg_filtered, hr_rppg] = getHRFromPulse.extractHeartBeatPeak(rPPG_spec_axis,rPPG_spec,self.lastValue,isFFT=True);
				self.lastValue = hr_rppg;
	
		return [crtTimestamps, self.P[0:k+1], hr_rppg];
		
	# Save the result into a file
	def saveFile(self):
		getTraceFromFileSeq.saveFile1D("..\\Result\\Method_3\\"+repo+".csv",P,'\n');
		
		
#ADVANCED
class AdvancedFrameProcessingSSR():

	# Initialization of the class attributes
	def __init__(self,files,repo,sizeWindow):
		self.timestamps = [int(i) for i in np.transpose(files)[1]];
		self.files_name = np.transpose(files)[0];
		self.l = 57;
		self.sizeWindow = sizeWindow;
		self.maxIndex = len(self.files_name);
		
		self.trackingWindow = TrackingWindow.TrackingWindow();
		self.outfile = "..\\Result\\Method_3\\"+repo+".csv";
		
		self.P = np.zeros(len(self.files_name));
		
		self.array_lambda = [];
		self.array_eigenvectors = [];
		
		self.lastValue = -1;
		
		array_timestamp_float = [float(x) / 1000 for x in self.timestamps];
		self.fs = 1/np.mean(np.diff(array_timestamp_float));
		
	# Make the process
	def run(self,k,signalPPG,isPPG):
		
		current_hr_index = -1;
		tho = -1;
		FFT = None;
		crtTimestamps_PPG = None;
		PPG = None;
		PPG_FFT = None;
		hr_ppg = None;
	
		crtTimestamps = self.timestamps[0:k+1];
	
		if(k < self.maxIndex):
			# get skin pixels
			Vk = getSkinMatrixTracker(self.files_name[k],k,self.trackingWindow);
			
			# Number of skin pixels
			N = len(Vk);

			Ck = (np.dot(np.transpose(Vk),Vk))/N;
			
			#Ak : eigen values, Uk : eigen vectors
			[Ak,Uk] = np.linalg.eig(Ck);
			
			self.array_lambda.append(-Ak);
			self.array_eigenvectors.append(-Uk);
			
			tho = k-self.l+1;
			
			hr_rppg = 0;
			hr_rppg_filtered = 0;
			hr_ppg = None;
			
			# for computing explanation : see method 2SR
			
			if(tho >= 0):
				arraySR = [];
				
				for t in range(tho,k+1):
					
					firstPart = np.dot(np.dot(np.dot(np.sqrt((self.array_lambda[t][0])/(self.array_lambda[tho][1])),np.transpose(self.array_eigenvectors[t][:,0])),self.array_eigenvectors[tho][:,1]),np.transpose(self.array_eigenvectors[tho][:,1]));
					secondPart = np.dot(np.dot(np.dot(np.sqrt((self.array_lambda[t][0])/(self.array_lambda[tho][2])),np.transpose(self.array_eigenvectors[t][:,0])),self.array_eigenvectors[tho][:,2]),np.transpose(self.array_eigenvectors[tho][:,2]));
					
					currentSR = firstPart + secondPart;
					
					arraySR.append(currentSR);
					
				RGB_arraySR = np.transpose(np.array(arraySR));
				
				p = RGB_arraySR[0] - (np.std(RGB_arraySR[0])/np.std(RGB_arraySR[1])) * RGB_arraySR[1];

				self.P[tho:tho+self.l] = self.P[tho:tho+self.l] + (p - np.mean(p));
				
				current_hr_index = tho-self.sizeWindow+1;
				
				# We can display the HR result
				if(current_hr_index >= 0):
					
					signal = self.P[current_hr_index : current_hr_index+self.sizeWindow];
					current_timestamp = self.timestamps[current_hr_index : current_hr_index+self.sizeWindow];
					
					# Welch for frequencies
					[rPPG_spec_axis,rPPG_spec] = getHRFromPulse.welch_process(current_timestamp,signal,self.fs);
					rPPG_spec_axis = np.array([math.fabs(x) for x in rPPG_spec_axis]);
					rPPG_spec = np.array([math.fabs(x) for x in rPPG_spec]);
					
					# Extraction of the heart beat
					[hr_rppg_filtered, hr_rppg] = getHRFromPulse.extractHeartBeatPeak(rPPG_spec_axis,rPPG_spec,self.lastValue,isFFT=True);
					self.lastValue = hr_rppg_filtered;
					
		else:
			tho = k-self.l+1;
			current_hr_index = tho-self.sizeWindow+1;
			
			# print "index : " + str(current_hr_index);
			# print "index+window : " + str(current_hr_index+self.sizeWindow);
			
			signal = self.P[current_hr_index : current_hr_index+self.sizeWindow];
			current_timestamp = self.timestamps[current_hr_index : current_hr_index+self.sizeWindow];
			
			# Welch for frequencies
			[rPPG_spec_axis,rPPG_spec] = getHRFromPulse.welch_process(current_timestamp,signal,self.fs);
			rPPG_spec_axis = np.array([math.fabs(x) for x in rPPG_spec_axis]);
			rPPG_spec = np.array([math.fabs(x) for x in rPPG_spec]);
			
			# Extraction of the heart beat
			[hr_rppg_filtered, hr_rppg] = getHRFromPulse.extractHeartBeatPeak(rPPG_spec_axis,rPPG_spec,self.lastValue,isFFT=True);
			self.lastValue = hr_rppg_filtered;
		
		crtTimestamps_rPPG = self.timestamps[0:k+1];
		
		#PPG
		if(tho > 0):
			crtTimestamps_PPG = getCrtTimestampPPG(self.timestamps,tho,self.sizeWindow);
			
			if(isPPG):
				PPG = loadPPG.loadCrtPPG(crtTimestamps_PPG[0]*1000,crtTimestamps_PPG[len(crtTimestamps_PPG)-1]*1000,signalPPG);
			else:
				PPG = loadPPG.loadCrtECG(crtTimestamps_PPG[0]*1000,crtTimestamps_PPG[len(crtTimestamps_PPG)-1]*1000,signalPPG);
			
			# PPG FFT
			[PPG_spec_axis,PPG_spec] = getHRFromPulse.fft(PPG[0],PPG[1]);
			PPG_FFT = [PPG_spec_axis,PPG_spec];
			
			if(current_hr_index >= 0):
				# print "index PPG : " + str(current_hr_index);
				hr_ppg = getHRFromPulse.extractHeartBeat(PPG_spec_axis,PPG_spec,isFFT=True);
	
		crtTimestamps_rPPG = [float(x) / 1000 for x in crtTimestamps_rPPG];
		
		return [crtTimestamps_PPG, crtTimestamps_rPPG, self.P[0:k+1], hr_rppg, hr_rppg_filtered, PPG, hr_ppg, FFT, None, PPG_FFT];
		
		
	def saveFile(self):
		getTraceFromFileSeq.saveFile1D("..\\Result\\Method_3\\"+repo+".csv",P,'\n');
		
		
# Skin detection :
def getSkinMatrixTracker(file,current,trackingWindow):
	# Full matrix of image (with [0,0,0] values)
	skin_matrix = getTraceFromFileSeq.face_skin_tracker(file,current,trackingWindow,onlySkin=True);
	
	# List of RGB values (2D matrix)
	skin_list = skin_matrix.reshape(-1,skin_matrix.shape[2]);
	
	skin_list_not_null = [RGB for RGB in skin_list if np.all(RGB) != np.all([0,0,0])];
	
	# print skin_list_not_null;
	return np.array(skin_list_not_null, dtype=float);
	
# Get the current timestamps of the PPG
def getCrtTimestampPPG(timestamps,currentRight,sizeWindow):
	currentLeft = currentRight - sizeWindow + 1;
	
	if(currentLeft < 0):
		currentLeft = 0;

	# Take the timeStamp of the window (in secondes)
	crtTimestamps = timestamps[currentLeft:currentRight+1];
	crtTimestamps[:] = [float(x) / 1000 for x in crtTimestamps];

	return crtTimestamps;

				
# Get the current timestamps and frames of the rPPG
def getCrtFrames(RGBTraces,timestamps,currentRight,sizeWindow):
	currentLeft = currentRight - sizeWindow + 1;
	
	if(currentLeft < 0):
		currentLeft = 0;
		
	# Take the frames of the window
	crtRGBTraces = RGBTraces[currentLeft:currentRight+1];

	# Take the timeStamp of the window (in secondes)
	crtTimestamps = timestamps[currentLeft:currentRight+1].copy();
	crtTimestamps[:] = [x / 1000 for x in crtTimestamps];

	return [crtRGBTraces,crtTimestamps];