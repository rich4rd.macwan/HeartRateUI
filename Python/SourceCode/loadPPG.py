import numpy as np;
import sys,os,glob;
import getTraceFromFileSeq;

# Read a XMP file as an array
def readXmp(file):
	array = np.genfromtxt(open(file), delimiter=',', dtype=np.dtype(np.int64))[:];
	return array;

# Extract the values of the PPG between 2 values (not obligatory in the array)
def loadCrtPPG(first,last,array_xmp):
	
	res = [];
	indexes = [];

	currentIndex = 0;
	
	# Ignore values before first value given
	while(first > array_xmp[currentIndex,0]):
		currentIndex = currentIndex + 1;

	# Add values before second value given
	while(last > array_xmp[currentIndex,0]):

		res.append(array_xmp[currentIndex,3]);
		indexes.append(array_xmp[currentIndex,0]);
		currentIndex = currentIndex + 1;

	indexes[:] = [float(i) / 1000 for i in indexes];

	return [indexes,res];

# Read a ECG file as an array
def readECG(f):
	repo = os.path.join(f,"*.txt");
	channel = 11;
	begin = 8*channel+14;
	
	all_data = np.zeros((1, channel));
	
	for file_path in glob.glob(repo):
		print file_path;
		all_file = np.genfromtxt(open(file_path), delimiter='\n', dtype='string')[begin:];
		data = np.genfromtxt(all_file, delimiter=',', dtype='f8');
		npData = np.array(data);
		all_data = np.concatenate((all_data,npData),axis=0);
		
	all_data = np.delete(all_data, 0, 0);
	
	[row,col] = all_data.shape;
	
	time = np.transpose(np.array(range(1,row+1),ndmin=2));
	
	all_data = np.concatenate((time,all_data),axis=1);
	
	return all_data;
	
# Extract the values of the ECG between 2 values (not obligatory in the array)
def loadCrtECG(first,last,array_xmp):
	
	res = [];
	indexes = [];

	currentIndex = 0;
	
	# Ignore values before first value given
	while(first > array_xmp[currentIndex,0]):
		currentIndex = currentIndex + 1;

	# Add values before second value given
	while(last > array_xmp[currentIndex,0]):

		res.append(array_xmp[currentIndex,12]);
		indexes.append(array_xmp[currentIndex,0]);
		currentIndex = currentIndex + 1;

	indexes[:] = [float(i) for i in indexes];
	indexes[:] = [x / 1000 for x in indexes];

	return [indexes,res];

# TEST for ECG
if __name__ == '__main__':
	patient = "patient3-2";
	folder = "..\\Videos\\" + patient;
	ecg = readECG(folder + "\\ecg\\");
	traces = getTraceFromFileSeq.readCsv(folder + "\\RBGtraces.csv");

	np.set_printoptions(suppress=True);

	print "ECG";
	print ecg.shape;
	print ecg;
	print "Traces";
	print traces[1].shape
	print traces;