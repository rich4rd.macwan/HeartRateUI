# README #

This README enlists the steps are necessary to get the application up and running.

### HeartRate Estimator ###

* A simple offline heart rate estimator.
* Works on uncompressed videos for now.
* Version: 1.0


### How do I get set up? ###
* The following steps can be taken to run this application 
	* Create a folder called "build" in the project directory and cd to it.
	* Execute commands  "cmake ../heartRateUI" and "make" to compile and build the project
	* Run ./HeartRateUI (on unix) or HeartRateUI.exe (on windows) to run the application
* Configuration
* Dependencies
     * CMake
     * OpenCV (built with tbb and nonfree support)
	 * Thread Building Blocks (TBB)
     * OpenGL
	 * Qt (built with opengl with the appropriate settings)
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* rich4rd.macwan@gmail.com
* yannick.benezeth@u-bourgogne.fr