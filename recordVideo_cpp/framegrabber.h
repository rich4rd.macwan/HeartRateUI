#ifdef _WIN32
#include <dshow.h>
#include <Windows.h>
#pragma comment(lib, "strmiids")
#endif
#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat)
#include <opencv2/highgui/highgui.hpp>  // Video write
#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>
#include <string>
#include <iostream>
#include <cstdio>
#include <memory>
#include <iostream>
#include <ueyeopencv.h>

class FrameGrabber{
public:
    FrameGrabber(bool ueye, int cId);
    ~FrameGrabber();
    void run();
    cv::Mat &grabFrame();
    cv::Size getVideoSize();
    bool beginSetWhiteBalance();
    bool endSetWhiteBalance();
    bool setAutoGain(bool);
    bool setGamma(bool);
    std::string exec(const char* cmd);
    void release();
    bool isUEye;
#ifdef _WIN32
    IMoniker *pMoniker = NULL;
    IBaseFilter *pCap = NULL;
    IGraphBuilder *m_pGraph = NULL;
    ICaptureGraphBuilder2 *m_pBuild = NULL;

    HRESULT InitCaptureGraphBuilder(
            IGraphBuilder **ppGraph,  // Receives the pointer.
            ICaptureGraphBuilder2 **ppBuild  // Receives the pointer.
            )
    {
        if (!ppGraph || !ppBuild)
        {
            return E_POINTER;
        }
        IGraphBuilder *pGraph = NULL;
        ICaptureGraphBuilder2 *pBuild = NULL;

        // Create the Capture Graph Builder.
        HRESULT hr = CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL,
                                      CLSCTX_INPROC_SERVER, IID_ICaptureGraphBuilder2, (void**)&pBuild);
        if (SUCCEEDED(hr))
        {
            // Create the Filter Graph Manager.
            hr = CoCreateInstance(CLSID_FilterGraph, 0, CLSCTX_INPROC_SERVER,
                                  IID_IGraphBuilder, (void**)&pGraph);
            if (SUCCEEDED(hr))
            {
                // Initialize the Capture Graph Builder.
                pBuild->SetFiltergraph(pGraph);

                // Return both interface pointers to the caller.
                *ppBuild = pBuild;
                *ppGraph = pGraph; // The caller must release both interfaces.
                return S_OK;
            }
            else
            {
                pBuild->Release();
            }
        }
        return hr; // Failed
    }



    HRESULT EnumerateDevices(REFGUID category, IEnumMoniker **ppEnum)
    {
        // Create the System Device Enumerator.
        ICreateDevEnum *pDevEnum;
        HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL,
                                      CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pDevEnum));

        if (SUCCEEDED(hr))
        {
            // Create an enumerator for the category.
            hr = pDevEnum->CreateClassEnumerator(category, ppEnum, 0);
            if (hr == S_FALSE)
            {
                hr = VFW_E_NOT_FOUND;  // The category is empty. Treat as an error.
            }
            else{
                hr=InitCaptureGraphBuilder(&m_pGraph, &m_pBuild);
                if (hr == S_FALSE){
                    std::cout << "Could not initialize capture filter"<<std::endl;
                    hr = VFW_E_CANNOT_CONNECT;
                }
            }
            pDevEnum->Release();
        }
        return hr;
    }


    void DisplayDeviceInformation(IEnumMoniker *pEnum)
    {

        while (pEnum->Next(1, &pMoniker, NULL) == S_OK)
        {
            IPropertyBag *pPropBag;
            HRESULT hr = pMoniker->BindToStorage(0, 0, IID_PPV_ARGS(&pPropBag));
            if (FAILED(hr))
            {
                pMoniker->Release();
                continue;
            }

            VARIANT var;
            VariantInit(&var);

            // Get description or friendly name.
            hr = pPropBag->Read(L"Description", &var, 0);
            if (FAILED(hr))
            {
                hr = pPropBag->Read(L"FriendlyName", &var, 0);
            }
            if (SUCCEEDED(hr))
            {
                printf("%S\n", var.bstrVal);
                VariantClear(&var);
            }

            hr = pPropBag->Write(L"FriendlyName", &var);

            // WaveInID applies only to audio capture devices.
            hr = pPropBag->Read(L"WaveInID", &var, 0);
            if (SUCCEEDED(hr))
            {
                printf("WaveIn ID: %d\n", var.lVal);
                VariantClear(&var);
            }

            hr = pPropBag->Read(L"DevicePath", &var, 0);
            if (SUCCEEDED(hr))
            {
                // The device path is not intended for display.
                printf("Device path: %S\n", var.bstrVal);
                VariantClear(&var);
            }

            hr = pMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&pCap);
            if (SUCCEEDED(hr))
            {
                hr = m_pGraph->AddFilter(pCap, L"Capture Filter");
            }



            pPropBag->Release();
            pMoniker->Release();
        }
    }
#endif
private:
    FrameGrabber(int);
    static cv::VideoCapture cap;
    static cv::Mat frame;
    static int cameraId;
    boost::mutex mtx;
    bool running;
};
cv::VideoCapture FrameGrabber::cap=0;
cv::Mat FrameGrabber::frame=cv::Mat(100,100,CV_8UC3);
int FrameGrabber::cameraId=-1;
std::stringstream videodev;
UEyeOpenCV* ueyeopencv=NULL;
/** Constructor for handling UEye cameras. By default, we use v4l2 cameras
 *  unless specifically mentioned.
 */
FrameGrabber::FrameGrabber(bool ueye=false,int cId=0)
{
    if(ueye){
        isUEye=true;
        ueyeopencv=UEyeOpenCV::getInstance();
    }else{
        //Use V4l2 camera, default to cId=0
        isUEye=false;
        FrameGrabber(cId);
    }

}

/** Constructor for V4L2 cameras.
 *  The integer argument corresponds to the N in '/dev/videoN'.
 */
FrameGrabber::FrameGrabber(int c){
    // cap.open(cameraId);
    // if(!cap.isOpened()){
    // 	std::cout<<"Could not open webcam"<<std::endl;
    // 	exit(0);
    // }
    // else{
    // 	boost::thread thr(&FrameGrabber::run);
    // }
    cameraId=c;
    videodev<<"/dev/video";
    videodev<<c;
    running=true;
    if(!isUEye)
        boost::thread thr(boost::bind(&FrameGrabber::run,this));

}

void FrameGrabber::release(){
    if(running){
        boost::unique_lock<boost::mutex> lock(mtx);
        running=false;
        delete ueyeopencv;
    }

}
FrameGrabber::~FrameGrabber(){
    release();
}

void FrameGrabber::run(){
    if(isUEye){
        //Get frame from Ueye camera
        frame=UEyeOpenCV::getInstance()->getFrame();
    }
    else{
        cap.open(cameraId);
        if(!cap.isOpened()){
            std::cout<<"Could not open webcam. For V4l2 cameras, try using the FrameGrabber(int) constructor."<<std::endl;
            exit(0);
        }
    }
    while(running){

        //cv::imshow("Frame", frame);
        if(!isUEye){
            cap>>frame;
            boost::this_thread::sleep(boost::posix_time::milliseconds(20));
        }
        else{

        }
        //cv::waitKey(33);
    }
}
cv::Mat& FrameGrabber::grabFrame(){
    if(isUEye){
        frame=ueyeopencv->getFrame();
    }
    else{
        boost::unique_lock<boost::mutex> lock(mtx);
        return frame;
    }

}

cv::Size FrameGrabber::getVideoSize()
{
    if(isUEye){
        return cv::Size(UEyeOpenCV::getInstance()->getVideoWidth(),	UEyeOpenCV::getInstance()->getVideoHeight());
    }else{
        return cv::Size((int)cap.get(CV_CAP_PROP_FRAME_WIDTH),	(int)cap.get(CV_CAP_PROP_FRAME_HEIGHT));
    }


}

bool FrameGrabber::beginSetWhiteBalance()
{
    if(!isUEye){
        if(!cap.isOpened()){
            std::cout<<"Please call cap.open(cameraId) before setting white balance"<<std::endl;
            return false;
        }
        else{
    #ifdef __linux__
            std::stringstream cmd;
            cmd<<"v4l2-ctl --set-parm=24 -d"<<videodev.str();
            exec(cmd.str().c_str());
            cmd.str(std::string());
            cmd<<"v4l2-ctl --set-ctrl white_balance_temperature_auto=1 -d"<<videodev.str();
            exec(cmd.str().c_str());
            cmd.str(std::string());
            return true;
    #endif
    #ifdef _WIN32
            HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
            if (SUCCEEDED(hr))
            {
                IEnumMoniker *pEnum;

                hr = EnumerateDevices(CLSID_VideoInputDeviceCategory, &pEnum);
                if (SUCCEEDED(hr))
                {
                    DisplayDeviceInformation(pEnum);
                    //Set white balance
                    IAMVideoProcAmp *pProcAmp = 0;
                    hr = pCap->QueryInterface(IID_IAMVideoProcAmp, (void**)&pProcAmp);
                    if (FAILED(hr))
                    {
                        // The device does not support IAMVideoProcAmp, so disable the control.
                        std::cout << "Cannot set white balance using dshow" << std::endl;
                    }
                    else
                    {
                        long Min, Max, Step, Default, Flags, Val;

                        // Get the range and default value.

                        hr = pProcAmp->GetRange(VideoProcAmp_WhiteBalance, &Min, &Max, &Step,
                                                &Default, &Flags);
                        if (SUCCEEDED(hr))
                        {
                            // Get the current value.
                            hr = pProcAmp->Get(VideoProcAmp_WhiteBalance, &Val, &Flags);

                        }
                        if (SUCCEEDED(hr))
                        {
                            //Set white balance here
                            std::cout << "Whitebalance min = " << Min << ",max=" << Max << ",default ="<<Default<<std::endl;

                        }

                    }
                    pEnum->Release();
                }
                CoUninitialize();
            }
    #endif
        }

    }
    else{
        //Set white balance auto for ueye camera
        ueyeopencv->setAutoWhiteBalance(true);
    }

}

bool FrameGrabber::endSetWhiteBalance()
{
    bool result=false;
    if(!isUEye){
        int wb=4000;
        if(!cap.isOpened()){
            std::cout<<"Please call cap.open(cameraId) before setting white balance"<<std::endl;
        }
        else{
    #ifdef __linux__
            //Set white balance auto
            std::stringstream cmd;

            //Set white balance auto off
            cmd<<"v4l2-ctl --set-ctrl focus_auto=0,exposure_auto_priority=0,exposure_auto=1,white_balance_temperature_auto=0,power_line_frequency=1 -d "<<videodev.str();
            std::cout<<exec(cmd.str().c_str())<<std::endl;
            cmd.str(std::string());
            std::cout<<"Disabling auto white balance"<<std::endl;
    #elif __win32
            //Code for windows
    #endif

            result=true;
        }
    }
    else{
        //Disable auto white balance for ueye camera
        ueyeopencv->setAutoWhiteBalance(false);
    }

    return result;
}

bool FrameGrabber::setAutoGain(bool set)
{
    if(isUEye){
    return ueyeopencv->setAutoGain(set);
    }
    else
        return false;
}

bool FrameGrabber::setGamma(bool set)
{
    if(isUEye){
    return ueyeopencv->setGamma(set);
    }
    else
        return false;
}

std::string FrameGrabber::exec(const char* cmd) {
#ifdef __linux__
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
#elif _WIN32
    std::shared_ptr<FILE> pipe(_popen(cmd, "r"), _pclose);
#endif
    if (!pipe) return "ERROR";
    char buffer[128];
    std::string result = "";
    while (!feof(pipe.get())) {
        if (fgets(buffer, 128, pipe.get()) != NULL)
            result += buffer;
    }
    return result;
}
