/*****************************************************************************/
/*! \file    ueyeopencv.cpp
*   \author  Richard Macwan, Le2I
*   \date    Date: 23/05/2016
*
*   \brief   Simple application to access the uEye camera(EO-1312C)
*//***************************************************************************/
#include <iostream> // for standard I/O1
#include <string>   // for strings
#include <fstream>
#include <ueye.h>
#include <sys/mman.h>
#include <boost/thread.hpp>
#include <boost/signals2.hpp>
#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat)
#include <opencv2/highgui/highgui.hpp>  // Video write


class UEyeOpenCV{
    // uEye varibles
    HIDS	m_hCam;				// handle to camera
    HWND	m_hWndDisplay;		// handle to diplay window
    INT		m_nColorMode;		// Y8/RGB16/RGB24/REG32
    INT		m_nBitsPerPixel;	// number of bits needed store one pixel
    INT		m_nSizeX;			// width of image
    INT		m_nSizeY;			// height of image
    INT		m_nPosX;			// left offset of image
    INT		m_nPosY;			// right offset of image
    INT     m_nPixelClock;      //Value for pixel clock 7-35MHz, higher value gives higher framerate
    INT     m_nFrameRate;       //Frame rate, highest settings gives about 25 fps

    // memory needed for live display while using DIB
    INT*		m_lMemoryId;		// camera memory - buffer ID
    char*	m_pcImageMemory;	// camera memory - pointer to buffer
    SENSORINFO m_sInfo;			// sensor information struct
    INT     m_nRenderMode;		// render  mode
    INT     m_nFlipHor;			// horizontal flip flag
    INT     m_nFlipVert;		// vertical flip flag

    INT InitCamera (HIDS *hCam, HWND hWnd);
    bool OpenCamera();
    void ExitCamera();

    int  InitDisplayMode();
    void LoadParameters();
    void GetMaxImageSize(INT *pnSizeX, INT *pnSizeY);
    bool cameraInitialized;
    cv::Mat frame;
    UEyeOpenCV();
    IMAGE_FILE_PARAMS ImageFileParams;
    double actualfps;
public:
    void imwrite(std::string imgfilename);
    void Start();
    ~UEyeOpenCV();
    cv::Mat &getFrame();
    INT nRet=-1;
    static UEyeOpenCV* instance;
    static UEyeOpenCV* getInstance();
    bool setAutoWhiteBalance(bool);
    bool setAutoGain(bool);
    bool setGamma(bool);
    int getVideoWidth();
    int getVideoHeight();
};

