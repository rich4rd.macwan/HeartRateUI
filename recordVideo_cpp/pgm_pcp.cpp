#include <iostream> // for standard I/O1
#include <string>   // for strings
#include <fstream>
#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat)
#include <opencv2/highgui/highgui.hpp>  // Video write
#include <boost/thread.hpp>
#include "boost/filesystem/path.hpp"
#include "boost/filesystem/operations.hpp"
#include "framegrabber.h"
#include "cms50listener_boost.h"
#include <ctime>
#include <chrono>
#ifdef _WIN32
#include <direct.h>
#endif

using namespace std;
using namespace cv;
namespace fs = boost::filesystem;

//#include<windows.h>
#ifdef __linux__
#include <inttypes.h>
#define __int64 uint64_t
//#define SYNCTEST
bool runFlag=true;
boost::mutex mtx_runFlag;
bool forCHU=false;
bool forUEye=true; //Default to UEye cameras
//Parameter for adjusting the frame rate
int framerate=30;
int delay=(int)(1000/framerate);
bool startTimer=false;
//boost::condition_variable cond_recordReady;
boost::mutex mtx_startTimer;

//This will run in a separate thread on linux and if SYNCTEST is defined
void timerWindow(){

    Mat syncFrame(1080, 1920, CV_8UC3, Scalar::all(0));
    syncFrame.setTo(255);
    Mat cpy=syncFrame.clone();
    string text = "00000000";

    int fontFace = FONT_HERSHEY_COMPLEX;
    double fontScale = 10;
    int thickness = 15;
    // then put the text itself

    int baseline=15;
    Size textSize = getTextSize(text, fontFace,
                                fontScale, thickness, &baseline);

    // center the text
    Point textOrg((syncFrame.cols - textSize.width)/2,
                  (syncFrame.rows + textSize.height)/2);
    baseline += thickness;
    double i=0;

    auto tt1 = std::chrono::high_resolution_clock::now();
    double ms=0;
    cv::namedWindow("Syncframe");
    bool firstPass=true;

    while(runFlag){
        if(startTimer && firstPass){
            tt1 = std::chrono::high_resolution_clock::now();
            firstPass=false;
        }
        //        text=(h<10?"0"+to_string(h):to_string(h))+":"+(m<10?"0"+to_string(m):to_string(m))+":"+(s<10?"0"+to_string(s):to_string(s))+":"+to_string(ms);
        text=to_string(ms);
        //Clear the frame
        cpy.copyTo(syncFrame);
        putText(syncFrame, text, textOrg, fontFace, fontScale,Scalar::all(100), thickness, 8);
        cv::imshow("Syncframe",syncFrame);
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
        //        cv::waitKey(100);
        if(startTimer){
            auto tt2 = std::chrono::high_resolution_clock::now();
            ms=(std::chrono::duration_cast<std::chrono::milliseconds>(tt2 - tt1).count());
            ms=ms/1000;
        }


    }
    cout<<"Thread ends"<<endl;
}


#endif
// YB TODO : left pad with file namese.


void help(){
    //Show usage
    std::cout<<"Usage"<<std::endl;
    std::cout<<"-------"<<std::endl;

    std::cout<<"For V4l2 cameras:                      recordVideo [camNumber=0]"<<std::endl;
    std::cout<<"For UEye cameras (default):            recordVideo --forUEye"<<std::endl;
    std::cout<<"For CHU recordings without PPG sensor: recordVideo --forCHU"<<std::endl;
}

//int main(int argc, char *argv[])
int main(int argc, char *argv[])
{
#ifdef __linux__
    //string outFolderName = "/home/richard/rppgVideos/testvideo/";
    string outFolderName = "/home/yannick/video/";
    string imgFolder = outFolderName + "/img/";
    const int dir_err = mkdir(imgFolder.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
#else
    string outFolderName = "C:/Users/Richard/rppgVideos/synctest-masterslave";
    string imgFolder = outFolderName + "/img/";
    const int dir_err = _mkdir(imgFolder.c_str());
#endif
    // check if img folder exists, if not -> create it
    
    
    if (-1 == dir_err)
    {

        printf("\nError creating directory : %s\n",imgFolder.c_str());
        exit(1);
    }


    long int frameNb = 0;
    int camNumber=0;

    if (argc >= 2) {
        if(isdigit(argv[1][0])){
            camNumber=atoi(argv[1]);
            std::cout<<camNumber<<std::endl;
        }

        for(int i=2;i<=argc;i++){
            //If argv[1] == forCHU
            if(strcmp(argv[i-1],"--forCHU")==0){
                forCHU=true;
                std::cout<<"Recording data for CHU"<<std::endl;
            }
            if(strcmp(argv[i-1],"--forUEye")==0){
                forUEye=true;
                std::cout<<"Using UEye camera"<<std::endl;
            }
        }

    }
    //Get option (--forCHU) to record for CHU or with PPG sensor


    /*std::vector<int> params;
    params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    params.push_back(0);   // that's compression level, 9 == full , 0 == none*/
    //FrameGrabber framegrabber(camNumber);
    //Pass false for v4l2 cameras

    FrameGrabber framegrabber(forUEye,camNumber);

    CMS50EListener_BOOST cms50elistener;
    cms50elistener.connect();


    Mat img;
    //     VideoCapture inputVideo(0);              // Open input

    // 	if (!inputVideo.isOpened())
    // 	{
    // 		cout << "Could not open the webcam "<<  endl;
    // 		return -1;
    // 	}


    Size S = framegrabber.getVideoSize();

    cout << "Input frame resolution: Width=" << S.width << "  Height=" << S.height << endl << endl;

    cout << "Adjust cam settings and then press ENTER" << endl;
    //Start timer window

    // 	// Infinite loop to have time to adjust cam setting

    //Stuff to display oxymeter readings
    Oximeter_data oxydata;

    int fontFace = FONT_HERSHEY_COMPLEX;
    double fontScale = 1;
    int thickness = 1;
    // then put the text itself

    int baseline=1;

    // center the text
    Point textOrg(30,30);
    baseline += thickness;

    bool cms50Connected = cms50elistener.isConnected();
    int i=0;
    //Start setting white balance. It needs a few frames first to work properly.

    while (true)
    {

        img=framegrabber.grabFrame();

        //Set white balance after  a few frames
        if(i++==2){
            framegrabber.setAutoGain(true);
            framegrabber.setGamma(true);
            framegrabber.beginSetWhiteBalance();
            std::cout<<"Starting wb set"<<std::endl;

        }
#ifdef __linux__
        if(i==20){
            framegrabber.endSetWhiteBalance();
        }
#endif
        if (cms50Connected){
            //Get oxymeter data
            oxydata = cms50elistener.get_oxidata();
            std::stringstream oxytext;
            oxytext << "";
            oxytext << "HR:" << oxydata.heart_rate << " " << "SPO2:" << oxydata.oxygen_sat;

#ifdef __linux__
            //opencv in windows gives a pointless bug here. Not important
            putText(img, oxytext.str().c_str(), textOrg, fontFace, fontScale, Scalar(0, 255, 80), thickness, 8);
#endif
        }


#ifdef __linux__
        //opencv in windows gives a pointless bug here. Not important
        putText(img, "Adjust the camera and press Enter. Press 'X' to exit", cv::Point(5,img.rows-10), fontFace, fontScale/2,Scalar(80,255,255), 1, 8);
#endif
        imshow("win", img);

        int ch = cv::waitKey(20);
#ifdef __linux__
        if (char(ch) == 10)
#endif
#ifdef _WIN32
            if (char(ch) == 13)
#endif
            {
                break;
            }
        if(char(ch)=='X' || char(ch)=='x' ){
            cms50elistener.disconnect();
            framegrabber.release();
            exit(0);
        }
    }

#ifdef SYNCTEST
    std::cout<<"Running timer window in a separate thread..."<<std::endl;
    boost::thread timerthrd(&timerWindow);
    //    timerWindow();
#endif

    //    //Send signal to timerthread to start the timer


    //Wait to start
    waitKey(50);
#ifdef SYNCTEST
    mtx_startTimer.lock();
    startTimer=true;
    mtx_startTimer.unlock();
#endif
    cout << "Recording....." << endl;
    auto t1 = std::chrono::high_resolution_clock::now(); // strat timer
    // start recording
    cms50elistener.startRecordingGT();
    cms50Connected = cms50elistener.isConnected();
    //std::cout<<"Isconnected = "<<cms50Connected<<std::endl;

    std::stringstream startTime,endTime;

    while (true)
    {
        img=framegrabber.grabFrame();
        stringstream frameNbStream;
        if(frameNb==1){

            std::time_t t=std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
            startTime<<"start: "<<std::ctime(&t)<<std::endl;

        }
        frameNbStream << frameNb;

        auto t2 = std::chrono::high_resolution_clock::now();
        __int64 t = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
        stringstream timestampStream;
        timestampStream << t;


        string imgFileName = outFolderName + "img/frame_" + frameNbStream.str() + "_" + timestampStream.str() + ".bmp";
        //ueyeopencv->imwrite(imgFileName);
        imwrite(imgFileName, img);

        //putText(img, "Recording... Press Enter to finish.", cv::Point(5,img.rows-10), fontFace, fontScale/2,Scalar(80,255,255), 1, 8);
        imshow("win", img);
#ifdef __linux__
        //system("xclock -padding 0 -digital -face Monospace -32 -update 1 &");
#endif

        int ch = waitKey(1);
#ifdef __linux__
        if (char(ch) == 10)
#endif
#ifdef _WIN32
            if (char(ch) == 13)
#endif
            {
#ifdef SYNCTEST
                mtx_runFlag.lock();
                runFlag=false;
                mtx_runFlag.unlock();
#endif
                std::time_t t=std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
                endTime<<"end: "<<std::ctime(&t)<<std::endl;
#ifdef __linux__
                //system("killall xclock");
#endif

                break;
            }
        frameNb++;
    }

    cms50elistener.disconnect();

    if(!forCHU){
        //Write groundtruth data to file
        std::string gtdata=cms50elistener.get_gt_dump();
        std::ofstream gtdatafile;
        gtdatafile.open(outFolderName+"gtdump.xmp",std::ios::out|std::ios::trunc);
        gtdatafile.write(gtdata.c_str(),gtdata.length());
        gtdatafile.close();
    }
    else{
        //Write the current time to file
        std::ofstream gtdatafile;
        gtdatafile.open(outFolderName+"gtdump.xmp",std::ios::out|std::ios::trunc);

        gtdatafile.write(startTime.str().c_str(),startTime.str().length());
        gtdatafile.write(endTime.str().c_str(),endTime.str().length());
        gtdatafile.close();
    }




    cout << "Finished writing" << endl;


    return 0;
}
