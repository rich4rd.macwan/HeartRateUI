/*****************************************************************************/
/*! \file    ueyeopencv.cpp
*   \author  Richard Macwan, Le2I
*   \date    Date: 23/05/2016
*
*   \brief   Simple application to access the uEye camera(EO-1312C)
*//***************************************************************************/
#include <ueyeopencv.h>
//int main(int argc, char *argv[]){
//    UEyeOpenCV ueyetest;

//    ueyetest.Start();
//}
UEyeOpenCV *UEyeOpenCV::instance = NULL;
void UEyeOpenCV::Start()
{
    char c=0;
    while(c!=10){
        //boost::this_thread::sleep(boost::posix_time::milliseconds(30));
        cv::imshow("uEye",getFrame());
        c=cv::waitKey(1);
    }

}

UEyeOpenCV *UEyeOpenCV::getInstance()
{
    if(instance==NULL){
        instance=new UEyeOpenCV;
    }
    return instance;
}

bool UEyeOpenCV::setAutoWhiteBalance(bool set)
{
    double empty;
    double on = set ? 1 : 0;
    int retInt = is_SetAutoParameter(m_hCam, IS_SET_ENABLE_AUTO_WHITEBALANCE, &on, &empty);
    if (retInt != IS_SUCCESS) {
        std::cout<<"Error setting auto white balance. Error code : "<<retInt<<std::endl;
        return false;
    }
    else return true;
}

bool UEyeOpenCV::setAutoGain(bool set)
{
    double empty;
    double on = set ? 1 : 0;
    int retInt = is_SetAutoParameter(m_hCam, IS_SET_ENABLE_AUTO_GAIN, &on, &empty);
    if (retInt != IS_SUCCESS) {
        std::cout<<"Error setting auto gain. Error code : "<<retInt<<std::endl;
        return false;
    }else{
        return true;
    }

}

bool UEyeOpenCV::setGamma(bool set)
{
    INT nGamma;

    int retInt = is_Gamma(m_hCam, IS_GAMMA_CMD_GET_DEFAULT, (void*)&nGamma, sizeof(nGamma));
    nGamma=nGamma+50;
    if(nRet==IS_SUCCESS){
        std::cout<<"Setting gamma to "<<nGamma<<std::endl;
        retInt = is_Gamma(m_hCam, IS_GAMMA_CMD_SET, (void*)&nGamma, sizeof(nGamma));
    }
    if (retInt != IS_SUCCESS) {
        std::cout<<"Error setting gamma on. Error code : "<<retInt<<std::endl;
        return false;
    }else{
        return true;
    }
}


UEyeOpenCV::UEyeOpenCV()
{
    m_hCam = 0;
    m_pcImageMemory=NULL;
//    m_lMemoryId=NULL;
    cameraInitialized=false;
    //Set color mode and bits per pixel here only
    m_nBitsPerPixel=32;
    m_nColorMode=IS_CM_BGRA8_PACKED;

    m_nRenderMode = IS_RENDER_FIT_TO_WINDOW;

    int i=0;
    //Image file params
    ImageFileParams.pwchFileName = NULL;
    ImageFileParams.pnImageID = NULL;
    ImageFileParams.ppcImageMem = NULL;
    ImageFileParams.nQuality = 0;
    ImageFileParams.nFileType = IS_IMG_BMP;

    OpenCamera();		// open a camera handle

}

void UEyeOpenCV::imwrite(std::__cxx11::string imgfilename)
{
    ImageFileParams.pwchFileName = (wchar_t*)imgfilename.c_str();

    ImageFileParams.ppcImageMem=&m_pcImageMemory;
    ImageFileParams.pnImageID=(UINT*)m_lMemoryId;
    nRet = is_ImageFile(m_hCam, IS_IMAGE_FILE_CMD_SAVE, (void*)&ImageFileParams,
                           sizeof(ImageFileParams));
    std::cout<<"Saving image : "<<(char*)ImageFileParams.pwchFileName<<" --- result = "<<nRet<<std::endl;
}

int UEyeOpenCV::getVideoWidth()
{
    return m_nSizeX;
}

int UEyeOpenCV::getVideoHeight()
{
    return m_nSizeY;
}

UEyeOpenCV::~UEyeOpenCV()
{
    if(m_hCam!=NULL)
        ExitCamera();

}
/** Obtains a frame from the uEye camera. Requires the cv::Mat frame to be preinitialized.
 *
 *
 */
cv::Mat& UEyeOpenCV::getFrame()
{
    nRet = is_WaitEvent(m_hCam, IS_SET_EVENT_FRAME, int(1000/actualfps));
    if(nRet==IS_TIMED_OUT){
        std::cout<<"Timed out waiting for frame"<<std::endl;
    }
    nRet=is_SetImageMem(m_hCam,m_pcImageMemory,*m_lMemoryId);
    //INT nRet=is_FreezeVideo(m_hCam,IS_WAIT);
    if(nRet==IS_SUCCESS){
        if( m_pcImageMemory != NULL ){
            //Obtain latest frame data from the camera
            frame.data=(uchar*)m_pcImageMemory;
        }
    }
    return frame;
}


INT UEyeOpenCV::InitCamera(HIDS *hCam, HWND hWnd)
{
    //Setting frame rate the correct way
    //    1.Change pixel clock.
    //    2.Query frame rate range and, if applicable, set new value.
    //    3.Query exposure time range and, if applicable, set new value.


    INT nRet = is_InitCamera (hCam, hWnd);
    /************************************************************************************************/
    /*                                                                                              */
    /*  If the camera returns with "IS_STARTER_FW_UPLOAD_NEEDED", an upload of a new firmware       */
    /*  is necessary. This upload can take several seconds. We recommend to check the required      */
    /*  time with the function is_GetDuration().                                                    */
    /*                                                                                              */
    /*  In this case, the camera can only be opened if the flag "IS_ALLOW_STARTER_FW_UPLOAD"        */
    /*  is "OR"-ed to m_hCam. This flag allows an automatic upload of the firmware.                 */
    /*                                                                                              */
    /************************************************************************************************/
    if (nRet == IS_STARTER_FW_UPLOAD_NEEDED)
    {
        // Time for the firmware upload = 25 seconds by default
        INT nUploadTime = 25000;
        is_GetDuration (*hCam, IS_STARTER_FW_UPLOAD, &nUploadTime);

        std::string Str1, Str2, Str3;
        Str1 = "This camera requires a new firmware. The upload will take about ";
        Str2 = " seconds. Please wait ...";
        std::cout<<Str1<<nUploadTime / 1000<<Str2;

        // Try again to open the camera. This time we allow the automatic upload of the firmware by
        // specifying "IS_ALLOW_STARTER_FIRMWARE_UPLOAD"
        *hCam = (HIDS) (((INT)*hCam) | IS_ALLOW_STARTER_FW_UPLOAD);
        nRet = is_InitCamera (hCam, hWnd);
    }


    return nRet;
}

bool UEyeOpenCV::OpenCamera()
{
    INT nRet = IS_NO_SUCCESS;
    ExitCamera();

    // init camera (open next available camera)
    m_hCam = (HIDS) 0;
    nRet = InitCamera(&m_hCam, m_hWndDisplay);
    if (nRet == IS_SUCCESS)
    {
        // Get sensor info
        is_GetSensorInfo(m_hCam, &m_sInfo);

        //m_nSizeX=1024;
        //m_nSizeY=768;
         m_nSizeX=640;
        m_nSizeY=480;
        //GetMaxImageSize(&m_nSizeX, &m_nSizeY);
        //Create opencv Mat object
        frame=cv::Mat(cv::Size(m_nSizeX,m_nSizeY),CV_8UC4);
        //UpdateData(TRUE);
        nRet = InitDisplayMode();
        UINT nRange[3];
        ZeroMemory(nRange, sizeof(nRange));

        // Get pixel clock range
        nRet = is_PixelClock(m_hCam, IS_PIXELCLOCK_CMD_GET_RANGE, (void*)nRange, sizeof(nRange));
        UINT nPixelClock;

        // Get current pixel clock
        nRet = is_PixelClock(m_hCam, IS_PIXELCLOCK_CMD_GET_DEFAULT, (void*)&nPixelClock, sizeof(nPixelClock));
        if (nRet == IS_SUCCESS)
        {
    //      UINT nMin = nRange[0];
    //      UINT nMax = nRange[1];
    //      UINT nInc = nRange[2];
          //Set max pixel clock
          m_nPixelClock=32;
          nRet = is_PixelClock(m_hCam, IS_PIXELCLOCK_CMD_SET,
                                 (void*)&m_nPixelClock, sizeof(m_nPixelClock));
          nRet = is_PixelClock(m_hCam, IS_PIXELCLOCK_CMD_GET, (void*)&nPixelClock, sizeof(nPixelClock));

          std::cout<<"Pixel clock set to "<<nPixelClock<<" MHz"<<std::endl;
          //Set frame rate
          double min,max,interval;
          nRet=is_GetFrameTimeRange (m_hCam, &min,&max,&interval);
          double fpsmin=1/max;
          double fpsmax=1/min;
          double fpsn=1/interval;

          std::cout<<"FPS limits:"<<fpsmin<<","<<fpsmax<<","<<fpsn<<std::endl;
          //Set max frame rate
          nRet=is_SetFrameRate(m_hCam,int(fpsmax),&actualfps);
          std::cout<<"Set FPS to "<<actualfps<<std::endl;
        }


        if (nRet == IS_SUCCESS)
        {
            // Enable Messages
            //            std::cout<<"Enable uEye messages. 0 = IS_SUCCESS"<<std::endl;
            //            std::cout<<"Enable IS_DEVICE_REMOVED = "<<is_EnableMessage(m_hCam, IS_DEVICE_REMOVED, (HWND*)window.getSystemHandle())<<std::endl;
            //            std::cout<<"Enable IS_DEVICE_RECONNECTED = "<<is_EnableMessage(m_hCam, IS_DEVICE_RECONNECTED, (HWND*)window.getSystemHandle())<<std::endl;
            //            std::cout<<"Enable IS_FRAME = "<<is_EnableMessage(m_hCam, IS_FRAME, (HWND*)window.getSystemHandle())<<std::endl<<std::endl;

            nRet=is_EnableEvent(m_hCam,IS_SET_EVENT_FRAME);

            // start live video
            is_CaptureVideo( m_hCam, IS_WAIT );

        }
        else
            std::cout<<"Initializing the display mode failed!"<<std::endl;
        //LoadParameters();
        while(mlock(&cameraInitialized,sizeof(int))){
            boost::this_thread::sleep_for(boost::chrono::milliseconds(30));
        }
        cameraInitialized=true;
        munlock(&cameraInitialized,sizeof(int));
        //LoadParameters();
        is_SetExternalTrigger(m_hCam, IS_SET_TRIGGER_OFF);
        return true;
    }
    else
    {
        std::cout<<"No uEye camera could be opened !"<<std::endl;
        return false;
    }
}

void UEyeOpenCV::LoadParameters()
{
    if ( m_hCam == 0 )
        OpenCamera();

    if ( m_hCam != 0 )
    {
        if( is_ParameterSet(m_hCam, IS_PARAMETERSET_CMD_LOAD_FILE, NULL, NULL) == IS_SUCCESS && m_pcImageMemory != NULL )
        {
            // determine live capture
            BOOL bWasLive = (BOOL)(is_CaptureVideo( m_hCam, IS_GET_LIVE ));
            if( bWasLive )
                is_StopLiveVideo(m_hCam, IS_FORCE_VIDEO_STOP);

            // realloc image mem with actual sizes and depth.
            is_FreeImageMem( m_hCam, m_pcImageMemory, *m_lMemoryId );

            IS_SIZE_2D imageSize;
            is_AOI(m_hCam, IS_AOI_IMAGE_GET_SIZE, (void*)&imageSize, sizeof(imageSize));

            INT nAllocSizeX = 0;
            INT nAllocSizeY = 0;

            m_nSizeX = nAllocSizeX = imageSize.s32Width;
            m_nSizeY = nAllocSizeY = imageSize.s32Height;

            UINT nAbsPosX = 0;
            UINT nAbsPosY = 0;

            // absolute pos?
            is_AOI(m_hCam, IS_AOI_IMAGE_GET_POS_X_ABS, (void*)&nAbsPosX , sizeof(nAbsPosX));
            is_AOI(m_hCam, IS_AOI_IMAGE_GET_POS_Y_ABS, (void*)&nAbsPosY , sizeof(nAbsPosY));

            if (nAbsPosX)
            {
                nAllocSizeX = m_sInfo.nMaxWidth;
            }
            if (nAbsPosY)
            {
                nAllocSizeY = m_sInfo.nMaxHeight;
            }

            switch( is_SetColorMode( m_hCam, IS_GET_COLOR_MODE ) )
            {
            case IS_CM_RGBA12_UNPACKED:
            case IS_CM_BGRA12_UNPACKED:
                m_nBitsPerPixel = 64;
                break;

            case IS_CM_RGB12_UNPACKED:
            case IS_CM_BGR12_UNPACKED:
            case IS_CM_RGB10_UNPACKED:
            case IS_CM_BGR10_UNPACKED:
                m_nBitsPerPixel = 48;
                break;

            case IS_CM_RGBA8_PACKED:
            case IS_CM_BGRA8_PACKED:
            case IS_CM_RGB10_PACKED:
            case IS_CM_BGR10_PACKED:
            case IS_CM_RGBY8_PACKED:
            case IS_CM_BGRY8_PACKED:
                m_nBitsPerPixel = 32;
                break;

            case IS_CM_RGB8_PACKED:
            case IS_CM_BGR8_PACKED:
                m_nBitsPerPixel = 24;
                break;

            case IS_CM_BGR565_PACKED:
            case IS_CM_UYVY_PACKED:
            case IS_CM_CBYCRY_PACKED:
                m_nBitsPerPixel = 16;
                break;

            case IS_CM_BGR5_PACKED:
                m_nBitsPerPixel = 15;
                break;

            case IS_CM_MONO16:
            case IS_CM_SENSOR_RAW16:
            case IS_CM_MONO12:
            case IS_CM_SENSOR_RAW12:
            case IS_CM_MONO10:
            case IS_CM_SENSOR_RAW10:
                m_nBitsPerPixel = 16;
                break;

            case IS_CM_RGB8_PLANAR:
                m_nBitsPerPixel = 24;
                break;

            case IS_CM_MONO8:
            case IS_CM_SENSOR_RAW8:
            default:
                m_nBitsPerPixel = 8;
                break;
            }

            // memory initialization
            is_AllocImageMem( m_hCam, nAllocSizeX, nAllocSizeY, m_nBitsPerPixel, &m_pcImageMemory, m_lMemoryId);

            // set memory active
            is_SetImageMem(m_hCam, m_pcImageMemory,*m_lMemoryId );

            // display initialization
            IS_RECT rectAOI;

            rectAOI.s32X     = 128;
            rectAOI.s32Y     = 128;
            rectAOI.s32Width = m_nSizeX;
            rectAOI.s32Height = m_nSizeY;

            nRet = is_AOI( m_hCam, IS_AOI_IMAGE_SET_AOI, (void*)&rectAOI, sizeof(rectAOI));

            // run live image again
            //if( bWasLive )
            //   is_CaptureVideo(m_hCam, IS_DONT_WAIT);
        }
    }
}

void UEyeOpenCV::GetMaxImageSize(INT *pnSizeX, INT *pnSizeY)
{
    // Check if the camera supports an arbitrary AOI
    // Only the ueye xs does not support an arbitrary AOI
    INT nAOISupported = 0;
    BOOL bAOISupported = TRUE;
    if (is_ImageFormat(m_hCam,
                       IMGFRMT_CMD_GET_ARBITRARY_AOI_SUPPORTED,
                       (void*)&nAOISupported,
                       sizeof(nAOISupported)) == IS_SUCCESS)
    {
        bAOISupported = (nAOISupported != 0);
    }

    if (bAOISupported)
    {
        // All other sensors
        // Get maximum image size
        SENSORINFO sInfo;
        is_GetSensorInfo (m_hCam, &sInfo);
        *pnSizeX = sInfo.nMaxWidth;
        *pnSizeY = sInfo.nMaxHeight;
    }
    else
    {
        // Only ueye xs
        // Get image size of the current format
        IS_SIZE_2D imageSize;
        is_AOI(m_hCam, IS_AOI_IMAGE_GET_SIZE, (void*)&imageSize, sizeof(imageSize));

        *pnSizeX = imageSize.s32Width;
        *pnSizeY = imageSize.s32Height;
    }
}

void UEyeOpenCV::ExitCamera()
{
    if( m_hCam != 0 )
    {
        // Disable messages
        is_EnableMessage( m_hCam, IS_FRAME, NULL );

        // Stop live video
        is_StopLiveVideo( m_hCam, IS_WAIT );

        // Free the allocated buffer
        if( m_pcImageMemory != NULL )
            is_FreeImageMem( m_hCam, m_pcImageMemory, *m_lMemoryId );

        m_pcImageMemory = NULL;

        // Close camera
        is_ExitCamera( m_hCam );
        m_hCam = NULL;
    }
}

int UEyeOpenCV::InitDisplayMode()
{
    INT nRet = IS_NO_SUCCESS;

    if (m_hCam == NULL)
        return IS_NO_SUCCESS;

    if (m_pcImageMemory != NULL)
    {
        is_FreeImageMem( m_hCam, m_pcImageMemory, *m_lMemoryId );
    }
    m_pcImageMemory = NULL;
    m_lMemoryId=new INT;

    // Set display mode to DIB
    nRet = is_SetDisplayMode(m_hCam, IS_SET_DM_DIB);
    if (m_sInfo.nColorMode == IS_COLORMODE_BAYER)
    {
        // setup the color depth to the current windows setting
        is_GetColorDepth(m_hCam, &m_nBitsPerPixel, &m_nColorMode);
    }
    else if (m_sInfo.nColorMode == IS_COLORMODE_CBYCRY)
    {
        // for color camera models use RGB32 mode
        m_nColorMode = IS_CM_BGRA8_PACKED;
        m_nBitsPerPixel = 24;
    }
    else
    {
        // for monochrome camera models use Y8 mode
        m_nColorMode = IS_CM_MONO8;
        m_nBitsPerPixel = 8;
    }

    // set the desired color mode
    is_SetColorMode(m_hCam, m_nColorMode);

    // allocate an image memory.
    if (is_AllocImageMem(m_hCam, m_nSizeX, m_nSizeY, m_nBitsPerPixel, &m_pcImageMemory, m_lMemoryId ) != IS_SUCCESS)
    {
        //AfxMessageBox(TEXT("Memory allocation failed!"), MB_ICONWARNING );
        std::cout<<"Memory allocation failed"<<std::endl;
    }
    else
        is_SetImageMem( m_hCam, m_pcImageMemory, *m_lMemoryId );


    if (nRet == IS_SUCCESS)
    {


        // set the image size to capture
        IS_RECT rectAOI;

        rectAOI.s32X     = 128;
        rectAOI.s32Y     = 128;
        rectAOI.s32Width = m_nSizeX;
        rectAOI.s32Height = m_nSizeY;

        nRet = is_AOI( m_hCam, IS_AOI_IMAGE_SET_AOI, (void*)&rectAOI, sizeof(rectAOI));
    }

    return nRet;
}
