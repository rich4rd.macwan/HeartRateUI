#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat)
#include <opencv2/highgui/highgui.hpp>  // Video write
#include <opencv2/imgproc/imgproc.hpp>
#include <boost/thread.hpp>
#include <iostream>
#include <boost/asio/serial_port.hpp>
#include <boost/asio.hpp>
#include <exception>
#include<chrono>
//TODO: Add waveform display for windows platform. Non-critical
#ifdef __linux__
#include <SFML/Graphics.hpp>
#endif

struct Oximeter_data{
    int wave_y,heart_rate,oxygen_sat;
    Oximeter_data(){
        wave_y = 0;
        heart_rate = 0;
        oxygen_sat = 0;
    }
    Oximeter_data(int wavey,int heartrate,int oxygensat){
        wave_y=wavey;
        heart_rate=heartrate;
        oxygen_sat=oxygensat;
    }
};

class CMS50EListener_BOOST{

public:
    CMS50EListener_BOOST();
    ~CMS50EListener_BOOST();
    void run();
    void disconnect();
    void connect();
    void connect(std::string portname);
    Oximeter_data get_oxidata();
    void startRecordingGT();
    std::string get_gt_dump();
    bool isConnected();
    void join();
private:
    unsigned int baudrate;
    bool running;
    boost::mutex mtx;
    bool busy;
    boost::asio::io_service m_io;
    std::string portname;
    uchar data[5];
    int byte1,byte2,byte3;
    int wave_y,heart_rate,oxygen_sat;
    int statusbyte;
    int statusbit;
    bool display_waveform;
#ifdef __linux__
    sf::RenderWindow *window;
    sf::CircleShape *shape;
    sf::Image monitorsf;
    sf::Texture texture;
    sf::Sprite sprite;

#endif
    cv::Mat monitor,monitorRGBA;
    double monitorW,monitorH;
    uchar* camData;
    double maxY,minY;
    std::vector<int> waveform;
    std::stringstream stream_to_save;
    bool startRecording;
    bool firstPass;
    std::chrono::high_resolution_clock::time_point t1,t2;
    boost::thread thrd;

};

bool CMS50EListener_BOOST::isConnected(){
    return running;
}

void CMS50EListener_BOOST::join()
{
    thrd.join();
}

CMS50EListener_BOOST::CMS50EListener_BOOST()
{
    baudrate=19200;
#ifdef __linux__
    portname="/dev/ttyUSB0";
#endif
#ifdef _WIN32
    portname = "COM7";
#endif
    wave_y=0;
    heart_rate=0;
    oxygen_sat=0;
    display_waveform=false;

    monitorW=500; monitorH=200;
    monitor=cv::Mat(monitorH,monitorW,CV_8UC4);
    monitor=cv::Scalar(255,255,255,255);
    //cv::cvtColor(monitor,monitorRGBA,cv::COLOR_BGR2RGBA);
#ifdef __linux__
    monitorsf.create(monitor.cols,monitor.rows,monitor.ptr());
    texture.loadFromImage(monitorsf);
    sprite.setTexture(texture);
#endif
    maxY=0;
    minY=monitorH;
    firstPass=true;
    startRecording=false;
}

CMS50EListener_BOOST::~CMS50EListener_BOOST()
{

}

void CMS50EListener_BOOST::run()
{
    boost::asio::basic_serial_port<boost::asio::serial_port_service> m_port(m_io);
    try{
        m_port.open(portname);
        m_port.set_option(boost::asio::serial_port_base::baud_rate(baudrate));
        m_port.set_option(boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::one));
        m_port.set_option(boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::odd));
        m_port.set_option(boost::asio::serial_port_base::character_size(8));
        running=true;
    }
    catch(std::exception& e){
        std::cout<<"Could not open serial port. Make sure the oximeter is connected."<<std::endl;
        running=false;
    }
    if(display_waveform){
#ifdef __linux__
        window=new sf::RenderWindow(sf::VideoMode(monitorW, monitorH), "Waveform monitor!");
#endif
    }


    int i=0;
    while(running){
        busy=true;
        //The device outputs data 60 times a second, i.e every 16.67 ms. The read_some method wais if there is no data.
        boost::this_thread::sleep(boost::posix_time::milliseconds(16));

        //std::cout<<"Reading data from serial port"<<std::endl;
        int bytes_read=-1;
        try{
            bytes_read=m_port.read_some(boost::asio::buffer(data,5));
        }
        catch(std::exception& e){
            //running=false;
            if(firstPass){
                std::cout<<"Finger out! Ground truth data will NOT be recorded."<<std::endl;
                firstPass=false;
            }
            continue;
        }


        //5 byte sequence
        if(bytes_read==5){
            for(int i=0;i<5;i++){
                int val=(uchar)data[i];
                if(val>128){ //this is the first byte
                    wave_y=(uchar)data[(i+1)%5];
                    //Check high heart rate status bit. 5th bit of the 3rd byte, byte before the one that shows the heart rate.
                    //If it is 0, add 127 to the heart rate
                    //Problem: Above algorithm does not work. Original webpage where this info was available does not work.
                    //Observation: For HR>127, byte3>64 otherwise byte3<10. We use this info as a switch
                    statusbyte= (int)data[(i+2)%5];
                    //Right shift by 3( 8-5) to get the 5th bit value
                    //statusbit=(statusbyte >> 3);

                    heart_rate=(statusbyte>64?(128+(uchar)data[(i+3)%5]):(uchar)data[(i+3)%5]);
                    oxygen_sat=(uchar)data[(i+4)%5];
                    break;
                }
            }
            //std::cout<<int(data[0])<<" "<<int(data[1])<<" "<<int(data[2])<<" "<<int(data[3])<<" "<<int(data[4])<<"   HR="<<heart_rate<<std::endl;

        }

        if(bytes_read==3){

            //std::cout<<"3 byte sequence"<<std::endl;
            byte1=(uchar)data[0];
            byte2=(uchar)data[1];
            byte3=(uchar)data[2];

            if(byte1>128){
                wave_y=byte2;
            }
            else if(byte2>128){
                wave_y=byte3;
                oxygen_sat=byte1;
            }
            else {
                //heart_rate=byte1;
                heart_rate=(byte3>64?(128+byte1):byte1);
                oxygen_sat=byte2;
            }
            //std::cout<<int(data[0])<<" "<<int(data[1])<<" "<<int(data[2])<<"   HR="<<heart_rate<<std::endl;
        }
        if(display_waveform){

#ifdef __linux__
            sf::Event event;
            while (window->pollEvent(event))
            {
                if (event.type == sf::Event::Closed)
                    window->close();
            }
            for(int i=1;i<waveform.size();i++){
                //cv::rectangle(monitor,cv::Rect(i,waveform[i],1,1),cv::Scalar(0,0,255,255));
                cv::line(monitor,cv::Point(i-1,monitorH-waveform[i-1]),cv::Point(i,monitorH-waveform[i]),cv::Scalar(100,0,255,255));
            }
            monitorsf.create(monitor.cols,monitor.rows,monitor.ptr());
            texture.loadFromImage(monitorsf);
            sprite.setTexture(texture);
            window->clear();
            window->draw(sprite);
            window->display();
            if(wave_y>maxY){
                maxY=wave_y;
            }
            if(wave_y<minY){
                minY=wave_y;
            }
            //        waveform.push_back((maxY-wave_y)/(wave_y-minY));
            waveform.push_back(wave_y);
            if(waveform.size()>monitor.cols){
                waveform.erase(waveform.begin());
                monitor=cv::Scalar(255,255,255,255);
            }
#endif
        }

        if(startRecording){
            t2 = std::chrono::high_resolution_clock::now();
            uint64_t t = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
            stream_to_save<<t<<","<<heart_rate<<","<<oxygen_sat<<","<<wave_y<<std::endl;
            //std::cout<<t<<", HR : "<<heart_rate<<std::endl;
        }
        else{
            //std::cout<<"StatusByte="<<int(statusbyte)<<", statusbit="<<statusbit<<", HR : "<<heart_rate<<std::endl;
        }
        busy=false;
    }
    busy=false;
    m_port.close();
}

void CMS50EListener_BOOST::disconnect()
{
    //Stop the thread
    boost::unique_lock<boost::mutex> lock(mtx);
    running=false;
    int count=0;
    while(busy){
        boost::this_thread::sleep(boost::posix_time::milliseconds(5));
        count++;
        if(count==20){
            return;
        }
    }
    //    delete window;
    //    delete shape;
}

void CMS50EListener_BOOST::connect()
{
    //Connect to serial port


    //Start a thread that reads continuously from the serial port and prints the data

    thrd=boost::thread(boost::bind(&CMS50EListener_BOOST::run,this));
}

void CMS50EListener_BOOST::connect(std::string portname)
{
    this->portname=portname;
    connect();
}

Oximeter_data CMS50EListener_BOOST::get_oxidata()
{
    boost::unique_lock<boost::mutex> lock(mtx);
    return  Oximeter_data(wave_y,heart_rate,oxygen_sat);
}

void CMS50EListener_BOOST::startRecordingGT()
{
    boost::unique_lock<boost::mutex> lock(mtx);
    startRecording=true;
    t1 = std::chrono::high_resolution_clock::now(); // strat timer
}

std::string CMS50EListener_BOOST::get_gt_dump()
{
    return stream_to_save.str();
}
