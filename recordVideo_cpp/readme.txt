1. install camera linux driver
- Edmund optics - USB3 - E01312C
- sudo sh ./ueyesdk-setup-4.81-usb-amd64.gz.run
- Start daemon:  sudo /etc/init.d/ueyeusbdrc start
- Check cam parameters with ueyedemo

2. install recordVideo dependencies 
- sudo apt-get install cmake
- sudo apt-get  install libopencv-dev
- sudo apt-get  install libsfml-dev
(if necessary copy cmake files of sfml -> sudo cp -i  FindSFML.cmake /usr/share/cmake-3.5/Modules)
- sudo apt-get install libboost-all-dev

3. Compile and run 
- make && ./recordVideo



