#include "qvideo.h"
#include <QToolButton>
#include <QFileDialog>
#include <QDebug>
#include <highgui.h>
#include <QMessageBox>
QVideo::QVideo(QWidget *parent) :
    QWidget(parent)
{
}

void QVideo::setHorizontalPadding(int h){
    if(h<50  &&h>=0)
        hGap=h;
}

void QVideo::setVerticalPadding(int v){
    if(v<=50  &&v>=0)
        vGap=v;
}

QSize QVideo::sizeHint(){
    return QSize(WIDTH+5,HEIGHT+5);
}

QSize QVideo::minimumSizeHint(){
    return QSize(WIDTH+5,HEIGHT+5);
}

QSize QVideo::maximumSize()
{
    return QSize(WIDTH+5,HEIGHT+5);
}

void QVideo::webcamButtonClicked(int camera,bool checked)
{
    prevCamSource=camera;
    //Init camera of the webcam number clicked
    //qDebug()<<camera;
    QList<QAbstractButton*> buttons=webcamButtons.buttons();
    //    for(int i=0;i<buttons.size();i++){
    //        qDebug()<<buttons.at(i)->isChecked()<<" : "<<checked;
    //    }
    if(checked)
        emit videoSourceSelected(camera);

    prevFileName="";
}


QVideo::QVideo(QString caption, QWidget *parent)
{	
    //Set up ui of qvideo widget here.
    this->parent=parent;
    hGap=5; vGap=25;
    //this->qImage=new QGLCanvas(this,caption);



    WIDTH=384;
    HEIGHT=288;


    this->qImage=new QGLCanvas(this,caption);
    qImage->setGeometry(hGap,/*this->caption->height()+*/vGap,qImage->WIDTH,qImage->HEIGHT);

    //    this->btnToggleVisibility=new QToolButton(this);
    //    //this->btnToggleVisibility->setText("Hide");
    //    this->btnToggleVisibility->setGeometry(qImage->WIDTH-20,0,24,24);
    //    this->btnToggleVisibility->setIconSize(QSize(24,24));
    //    //this->btnToggleVisibility->setCheckable(true);
    //    //this->btnToggleVisibility->setFlat(true);
    //    this->btnToggleVisibility->setIcon(QIcon(":/on.png"));

    //    this->btnToggleVisibility->setToolTip("Toggle Visibility");
    //    this->btnToggleVisibility->setStyleSheet("QToolButton{\
    //                                             border: none;\
    //                                             } QToolButton:pressed{background:qlineargradient(x1:-,y1:0,x2:0,y2:1,stop:0 #dadbde, stop: 1 #f6f7fa); border-radius:3px;\
    //                                             }\
    //                                             QToolButton:checked{background:qlineargradient(x1:-,y1:0,x2:0,y2:1,stop:0 #dadbde, stop: 1 #f6f7fa); border-radius:3px;}\
    //                                             QToolButton:hover{border-radius:5px;border: 1px solid;border-color:#aaabae;}");

    this->btnSourceVideo=new QToolButton(this);
    this->btnSourceVideo->setGeometry(hGap,0,32,24);
    this->btnSourceVideo->setIconSize(QSize(24,24));
    this->btnSourceVideo->setIcon(QIcon(":/images/video.png"));
    this->btnSourceVideo->setToolTip("Load video from file");
    this->btnSourceVideo->setStyleSheet("QToolButton{\
                                        border: none;\
} QToolButton:pressed{background:qlineargradient(x1:-,y1:0,x2:0,y2:1,stop:0 #dadbde, stop: 1 #f6f7fa); border-radius:3px;\
}\
QToolButton:checked{background:qlineargradient(x1:-,y1:0,x2:0,y2:1,stop:0 #dadbde, stop: 1 #f6f7fa); border-radius:3px;}\
QToolButton:hover{border-radius:5px;border: 1px solid;border-color:#aaabae;}");




//connect(this->btnToggleVisibility,SIGNAL(clicked()),this,SLOT(toggleVisibility()));
connect(this->btnSourceVideo,SIGNAL(clicked()),this,SLOT(loadVideo()));
nCameras=0;
populateCameras();
//Add buttons for each camera. On their click event, init the corresponding camera
int x=0;
for(int i=0;i<nCameras;i++){
    QToolButton* b=new QToolButton(this);

    b->setGeometry((i+1)*(btnSourceVideo->width()+hGap),0,32,24);
    b->setIcon(QIcon(":/images/webcam.png"));
    b->setToolTip("Load camera "+QString::number(i));
    b->setStyleSheet("QToolButton{\
                     border: none;\
} QToolButton:pressed{background:qlineargradient(x1:-,y1:0,x2:0,y2:1,stop:0 #dadbde, stop: 1 #f6f7fa); border-radius:3px;\
}\
QToolButton:checked{background:qlineargradient(x1:-,y1:0,x2:0,y2:1,stop:0 #dadbde, stop: 1 #f6f7fa); border-radius:3px;}\
QToolButton:hover{border-radius:5px;border: 1px solid;border-color:#aaabae;}");
b->setText(QString::number(i));
b->setCheckable(true);
b->setAutoExclusive(true);

b->setObjectName("B"+QString::number(i));
x=b->x()+b->width();
webcamButtons.addButton(b,i);

}

this->caption=new QLabel(QString(caption),this);
this->caption->setWordWrap(true);
this->caption->setGeometry(x+hGap,0,200,20);

connect(&webcamButtons,SIGNAL(buttonToggled(int,bool)),this,SLOT(webcamButtonClicked(int,bool)));

//setMinimumHeight(vGap*2/*+this->caption->height()*/+this->HEIGHT);
//    setMinimumWidth(qImage->WIDTH+hGap*2);

//    setMaximumHeight(vGap*3/*+this->caption->height()*/+qImage->HEIGHT);
//    setMaximumWidth(qImage->WIDTH+hGap*2);
}



QString QVideo::getCaption(){
    return this->caption->text();
}

void QVideo::setImage(IplImage& image){
    qImage->setImage(image);
}

//void QVideo::setImage(cv::Mat image)
//{
//    qImage->setImage(image);
//}

//void QVideo::setImage(QIplImage *qImage){
//this->qImage=qImage;
//}
void QVideo::setVideoVisible(bool visible){
    if(visible){
        qImage->show();
        btnToggleVisibility->setIcon(QIcon(":/on.png"));
    }
    else{
        qImage->hide();
        btnToggleVisibility->setIcon(QIcon(":/off.png"));
    }
    this->visible=visible;
}

void QVideo::setVideoSize(int width, int height)
{
    //Transmit size to underlying qglcanvas instant
    qImage->setSize(hGap,vGap,width,height);
    emit videoResized(width,height);

}

QSize QVideo::videoSize()
{
    return QSize(qImage->WIDTH,qImage->HEIGHT);
}

void QVideo::populateCameras()
{
    /**
     * Get the number of camera available
     */

    int maxTested = 5;
    for (int i = 0; i < maxTested; i++){

        cv::VideoCapture temp_camera(i);
        bool res = (!temp_camera.isOpened());
        //qDebug()<<i<<": :"<<res;
        if (!res)
        {
            //qDebug()<<"Guid "<< i<<": "<<temp_camera.get(CV_CAP_PROP_FRAME_WIDTH);
            nCameras=i+1;
        }
        temp_camera.release();
    }

}

void QVideo::updateCameraStatus()
{
    webcamButtons.setExclusive(false);
    if(webcamButtons.checkedButton()!=NULL){
        webcamButtons.checkedButton()->setChecked(false);
    }
    webcamButtons.setExclusive(true);
}

void QVideo::enableROISelection(bool enable)
{
    qImage->enableROISelection(enable);
}

void QVideo::setCaption(QString caption)
{
    this->caption->setText(caption);
    update();
}


void QVideo::loadVideo()
{

    //     qDebug()<<"QVideo()::loadVideo: Trying to open file";
    //     QMessageBox msgBox;
    // msgBox.setText("The document has been modified.");
    // msgBox.exec();
    QString fileName=QFileDialog::getOpenFileName(this,"Open Video");
    if(fileName!=""){
        //emit video path for renderthread
        prevFileName=fileName;
        prevCamSource=-1;
        emit videoSourceSelected(fileName);
    }



}
void QVideo::toggleVisibility(){

    if(visible){

        visible=false;
        qImage->hide();
        btnToggleVisibility->setIcon(QIcon(":/off.png"));
    }
    else{

        visible=true;
        qImage->show();

        btnToggleVisibility->setIcon(QIcon(":/on.png"));


    }
    update();

    emit showVideo(visible);

}


int QVideo::getVerticalPadding(){
    return vGap;
}

int QVideo::getHorizontalPadding(){
    return hGap;
}
