#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QVBoxLayout>
#include <QMainWindow>
#include "renderthread.h"
#include "qvideo.h"
#include "oximeterwidget.h"
#include "stimuluswidget.h"
#include "recordingdialog.h"
#include "evaluationdialog.h"
#include <QRadioButton>
#include <QVBoxLayout>
#include "resultdialog.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    static MainWindow* instance;
    static QString metadataExt;
    QVideo* getVideoWidget();
    void setUpVideoThread();
    void show();
    OximeterWidget* oximeterWidget;
    bool startRecording123();
    std::vector<QString> recordFileNames;
    void reinitSource();
    static const int NUM_RECORDINGS;
    MusicPlayer* getMusicPlayerRef();
    StimulusWidget* getStimulusWidgetRef();
    int windowSize();
    bool manualROIMode();
    bool isEmbeddedTestEnabled();
    bool isEvaluationEnabled();
    void setEvaluationFlag(bool);
    int evaluationFileIndex;
    QString evaluationFileName;
    void startEvaluation();
    QStringList evaluationFileList;
    QString currentDumpFilePath();
    bool overwriteDumpFiles();
    QString RPPGMethod();
    int rppgMethod();
    int windowLengthInFrames();
    int recordLengthInSeconds;
signals:
    void sendMessage(QString);
    void sendStopSignal();
public slots:

    void performEvaluation();
    void playButtonPressed();
    void playButtonPressed(int msec);
    void stopButtonPressed();
    void manualROIButtonPressed();
    void onStopped();
    void recordButtonPressed(bool);
    void evaluationButtonPressed(bool val);
    bool setRecordFileName();
    void showStatusMessage(QString,bool isError);
    void onVideoSourceSelected(QString);
    void onVideoSourceSelected(int);
    void onVideoSourceSelected(QString, bool recordMode);
    void onVideoSourceSelected(int,bool reinit);
    void closeEvent(QCloseEvent* evt);
    void onVideoResized(int,int);
    void setManualROISelection(cv::Rect roi);
    bool modeIsFaceDetection();
private:
    Ui::MainWindow *ui;
    QWidget centralWidget;
    QVBoxLayout* vlayout;
    QHBoxLayout* hlayout;
    RenderThread* renderThread;
    QVideo* video;

    bool playing; //Flag for indicating whether the video is being played or paused
    QIcon iconPlay;
    QIcon iconPause;
    QString recordFileName;
    StimulusWidget* stimuluswidget;
    RecordingDialog* recordingDialog;
    EvaluationDialog* evaluationDialog;
    ResultDialog* resultDialog;

    bool recordMode;
    int prevCameraSource;
    QString prevVideoSource;
    int nRecordings;
    QString recordFilenamePrefix;
    bool showDialogs;
    QComboBox cmbRPPGMethods;
    QRadioButton* rdoFaceDetection;
    QRadioButton* rdoSkinDetection;
    QVBoxLayout* rdoLayout;
    QWidget rdoWidget;
};

#endif // MAINWINDOW_H
