#ifndef MUSICPLAYER_H
#define MUSICPLAYER_H
#include<QProcess>
#include <QObject>

class MusicPlayer : public QObject
{
    Q_OBJECT
public:
    explicit MusicPlayer (QObject *parent = 0);
    ~MusicPlayer();
    MusicPlayer(QString musicFile);
    void setMusicFile(QString musicFile);
    QString getMusicFile();
    bool validMusicFile();


    void start();
    void stop();
    void pause();
    bool playing;
    bool paused;
public slots:
    void onReadyReadOutput();
    void onReadyReadError();
private:
    void validateMusicFile();
    QProcess playerProcess;
    QString musicFile;
    bool validSource;
    QString processString;
};

#endif // BGMUSICPLAYER_H
