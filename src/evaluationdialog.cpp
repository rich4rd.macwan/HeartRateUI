#include "evaluationdialog.h"
#include "ui_evaluationdialog.h"
#include <QMessageBox>
EvaluationDialog::EvaluationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EvaluationDialog)
{
    ui->setupUi(this);
    connect(ui->btnVideoDir,SIGNAL(clicked()),this,SLOT(selectVideoDirectory()));
    connect(ui->btnDumpDir,SIGNAL(clicked()),this,SLOT(selectDumpDirectory()));
    ui->chkOverWrite->setChecked(true);
}

EvaluationDialog::~EvaluationDialog()
{
    delete ui;
}

QString EvaluationDialog::videoDirectory()
{
    return ui->txtVideoDirectory->text();
}

QString EvaluationDialog::dumpDirectory()
{
    return ui->txtDumpDirectory->text();
}

bool EvaluationDialog::overwriteDumpFiles()
{
    return ui->chkOverWrite->isChecked();
}

QString EvaluationDialog::selectDumpDirectory()
{

    QString directory= QFileDialog::getExistingDirectory(this, tr("Select dump directory"),
                                                         "/home",
                                                         QFileDialog::ShowDirsOnly
                                                         | QFileDialog::DontResolveSymlinks);
    if(directory!=""){
        ui->txtDumpDirectory->setText(directory);
    }
    return directory;
}

QString EvaluationDialog::selectVideoDirectory()
{

    QString directory= QFileDialog::getExistingDirectory(this, tr("Select video directory"),
                                                         "/home",
                                                         QFileDialog::ShowDirsOnly
                                                         | QFileDialog::DontResolveSymlinks);
    if(directory!=""){
        ui->txtVideoDirectory->setText(directory);
    }
    return directory;
}
