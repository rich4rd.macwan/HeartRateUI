#ifndef HeartRateOSX_gui_h
#define HeartRateOSX_gui_h

#include <string>
#include <opencv2/core/core.hpp>
#include "peak.h"
#include "functions.h"
class Gui {

public:
    void addInfoOnFrame(cv::Mat &frame, cv::Rect faceRect, int crtPulse, std::vector<std::vector<double> > &traces, std::vector<std::vector<double> > &detrended, std::vector<Peak> &peakFlags,int rppgMethod=BASIC);
    Gui();
    void drawText(std::string text, cv::Mat frame, CvPoint p = cvPoint(30,30), int font = 1);//1 = FONT_HERSHEY_PLAIN
private:
    

    void plot(std::vector<std::vector<double> > &signal,std::vector<Peak> &peakFlags, int row, int begin, int end, cv::Mat& img, int thickness=2,CvScalar color=cvScalar(10,210,0));
    double Pix2X(int x, double xmax, double xmin, int width);
    double Pix2Y(int y, double ymax, double ymin, int height);
    int X2Pix(double x, double xmax, double xmin, int width);
    int Y2Pix(double y, double ymax, double ymin, int height);
    
    void overlayImage(const cv::Mat &background, const cv::Mat &foreground, cv::Mat &output, cv::Point2i location);
    void overlayHeartImg(cv::Mat & frame, cv::Point2i position= cv::Point(0,0));
    void overlayGraphImg(cv::Mat & frame, cv::Point2i position= cv::Point(0,0));
    int dx;
    int dy;
    int width;
    int height;
    int fauxHeight;
    int graphY;
    int graphX;
};


#endif
