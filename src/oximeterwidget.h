#ifndef OXIMETERWIDGET_H
#define OXIMETERWIDGET_H
#include <QIcon>
#include <QWidget>
#include "oxidata.h"
#include "waveformwidget.h"
#include "cms50elistener.h"

namespace Ui {
class OximeterWidget;
}

class OximeterWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OximeterWidget(QWidget *parent = 0);
    ~OximeterWidget();
    WaveformWidget* waveformWidget;
    CMS50eListener* CMS50eListenerRef();
signals:
    void sendSignalToSerialPort(bool);
    void updateWaveformWidgetView();
public slots:
    void onConnectPressed();
    void onConnectStatusReceived(bool);
    void showStatusMessage(QString,bool);
    void heartRateSPOReceived(int,int);
    void newFrameArrived(int);
    void onOxiDataArrived(OxiData oxidata);
private:
    Ui::OximeterWidget *ui;

    QIcon connectedIcon,disconnectedIcon;
    bool connected;
    CMS50eListener cms50eListener;
    QString prevMsg;
};

#endif // OXIMETERWIDGET_H
