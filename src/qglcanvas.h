#ifndef QGLCANVAS_H
#define QGLCANVAS_H
#include<cv.h>
#include <QtOpenGL/QGLWidget>
#include <QPushButton>
#include <QPoint>

class QGLCanvas : public QGLWidget
{
    Q_OBJECT

public:
	QImage::Format imageFormat;
    QGLCanvas(QWidget* parent = NULL,QString name="Video");
	void hide();
	void show();
	bool drawing;
	bool pboSupported;
    void setSize(int x, int y, int width,int height);
	QSize sizeHint() const;
    QSize minimumSizeHint() const;
        int WIDTH,HEIGHT;
        void enableROISelection(bool);
public slots:
    //void setImage(const QImage& image);
    void setImage(IplImage &);
    //void setImage(cv::Mat);
	void mousePressEvent(QMouseEvent*);
	void mouseMoveEvent(QMouseEvent*);
	void mouseReleaseEvent(QMouseEvent*);
	void mouseDoubleClickEvent(QMouseEvent*);

signals:
    void finishManualROISelection(cv::Rect);
protected:
    //void paintEvent(QPaintEvent*);
	void paintGL();
	void resizeGL(int,int);
	void initializeGL();
	GLuint texture;
	GLuint bglogoTexture;
private:
    QImage img;
    IplImage original,backup;
    //cv::Mat original;
    QColor bgColor;
	QString name;
	bool hidden;

	int mouseX,mouseY;
    GLenum format;
	
	GLuint pboIds[2];                   // IDs of PBO
	
	int pboMode;
	bool paintFlag;

    QImage bg,bgGL;
    int startX,startY,endX,endY;
    int DATA_SIZE;
    bool roiselect;
    cv::Rect roiRect;
    CvPoint p1,p2;

};

#endif // QGLCANVAS_H
