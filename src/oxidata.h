#ifndef OXIDATA_H
#define OXIDATA_H
#ifndef quint64
    typedef unsigned long long quint64;
#endif

class OxiData
{
public:
    OxiData();
    OxiData(int frameid,int heartrate,int oxygensat,int waveformvalue,int ts);
    ~OxiData();
    int frameId,heartRate,oxygenSaturation,waveformValue;
    quint64 timestamp;

};

#endif // OXIDATA_H
