//
//  gui.cpp
//  HeartRateOSX
//
//  Created by Yannick on 07/11/14.
//  Copyright (c) 2014 Yannick. All rights reserved.
//

#include "gui.h"
#include "pulsedetector.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include<QDebug>
#define sign(a) ( ( (a) < 0 )  ?  -1   : ( (a) > 0 ) )

using namespace cv;

void Gui::addInfoOnFrame(cv::Mat &frame, cv::Rect faceRect, int crtPulse, std::vector<std::vector<double> > &traces, std::vector<std::vector<double> > &detrended,std::vector<Peak> &peakFlags, int rppgMethod)
{
    
    rectangle(frame, faceRect, Scalar(0, 255, 0), 2, 8, 0);

    // add graph to the GUI
    // YB TODO : do it better
    overlayHeartImg(frame, cv::Point(40, frame.rows - 240));
    // overlayGraphImg(frame, cv::Point(graphX, frame.rows -graphY-32));
    
    string bmpString;
    if (crtPulse != 0) {
        //bmpString =  std::to_string(static_cast<long long>(crtPulse)) + " bpm";
        std::stringstream strm;
        strm<<crtPulse;
        bmpString=strm.str();
    }
    else {
        bmpString = "please wait...";
    }
   // std::cout<<"Gui::addInfoOnFrame::bmp="<<bmpString<<std::end;
    drawText(bmpString, frame, Point(120, frame.rows - 50 - 162), 2);
    std::string rppgString=RPPGMethodString(rppgMethod);
    drawText(rppgString, frame, Point(20, frame.rows - 162), 1);
    // add signal to the graph

    CvScalar blue=cvScalar(210,10,0);
    //plot(traces,peakFlags, 1, 0, traces[0].size(), frame,2,blue);

   if(peakFlags.size()>2)
     plot(detrended,peakFlags, 1, 0, detrended[0].size(), frame,1,cvScalar(40,240,30));
    
}

Gui::Gui()
{
    dx = 32;
    dy = 250;
    width = 400;
    height = 100;
    fauxHeight = 100;
    graphY=160;
    graphX=2;
}

void Gui::drawText(string text, Mat frame, CvPoint p, int font)
{
    //FONT_HERSHEY_PLAIN ; //FONT_HERSHEY_COMPLEX_SMALL;
    putText(frame, text, cvPoint(p.x-1,p.y-1), font, 0.8, cvScalar(30,30,30), 1, CV_AA);
    putText(frame, text, cvPoint(p.x-1,p.y+1), font, 0.8, cvScalar(30,30,30), 1, CV_AA);
    putText(frame, text, cvPoint(p.x+1,p.y-1), font, 0.8, cvScalar(30,30,30), 1, CV_AA);
    putText(frame, text, cvPoint(p.x+1,p.y+1), font, 0.8, cvScalar(30,30,30), 1, CV_AA);
    putText(frame, text, cvPoint(p.x+1,p.y), font, 0.8, cvScalar(30,30,30), 1, CV_AA);
    putText(frame, text, cvPoint(p.x-1,p.y), font, 0.8, cvScalar(30,30,30), 1, CV_AA);
    putText(frame, text, p,font , 0.8, cvScalar(255,255,255), 1, CV_AA);
}


void Gui::plot(std::vector<std::vector<double> > &tab/*Mat& tab*/, std::vector<Peak> &peakFlags, int row, int begin, int end, Mat& img, int thickness, CvScalar color)
{
    dy = img.rows -graphY;
    width=img.cols-dx*3;
    //Init
    int nbElem = end-begin;
    float stepPix = width/(float)nbElem;
    
    Mat plotIMG= img;
    //Mat plotIMG=cv::Mat::zeros(img.rows,img.cols*3,CV_8UC3);


    //rectangle(plotIMG, cvPoint(0,0), cvPoint(width-1, height-1), cvScalar(255, 255, 255), -1);
    
    //line(plotIMG, cvPoint( 0+dx, ((height-1)/2)+dy), cvPoint(width-1+dx, ((height-1)/2)+dy), cvScalar(255, 255, 255));
    
    //Find Min and Max
    
    double ymax=tab[row][begin], ymin=tab[row][begin];

    for(int i = begin+1; i < end; ++i)
    {
        if( tab[row][i] > ymax)
            ymax = tab[row][i];
        else if(tab[row][i] < ymin)
            ymin = tab[row][i];
    }
    //qDebug()<<"GUI:"<<ymax<<","<<ymin;
    //Draw
    float pixAbscisse = 0.;
    CvPoint pPrev, pCurr;
    pPrev.x = 0+dx;
    //pPrev.y = Y2Pix(tab.at<float>(row,begin),ymax,ymin,fauxHeight)+dy;
    pPrev.y = Y2Pix(tab[row][begin],ymax,ymin,fauxHeight)+dy;

    int fauxYO = Y2Pix(0.0,ymax,ymin,fauxHeight);
    int vraiYO = height/2;
    int delta =  vraiYO-fauxYO;
    line(plotIMG, cvPoint( 0+dx, Y2Pix(0.0,ymax,ymin,fauxHeight)+delta+dy), cvPoint(width-1+dx, Y2Pix(0.0,ymax,ymin,fauxHeight)+delta+dy), cvScalar(0, 0, 0));
    Peak prevPeak;
    for(int i = begin+1; i < end; ++i)
    {
        pixAbscisse += stepPix;
        pCurr.x = int(floor(pixAbscisse))+dx;


        pCurr.y = Y2Pix(tab[row][i],ymax,ymin,fauxHeight) +delta+dy;

        line(plotIMG, pPrev, pCurr, color, thickness);
        //  drawText(to_string(peakFlags[i-1].peakValue),plotIMG,cvPoint(pPrev.x,pPrev.y-10));
        if(peakFlags[i-1].isPeak){
            //rectangle(plotIMG,Rect(pCurr.x-4,pCurr.y-4,8,8),cvScalarAll(100));
            line(plotIMG,pPrev,cvPoint(pPrev.x,400),cvScalar(255,0,0),1);
            //drawText(to_string(peakFlags[i-1].peakValue),plotIMG,cvPoint(pPrev.x,410));
         //   drawText(to_string(double(peakFlags[i-1].timestamp-prevPeak.timestamp)),plotIMG,cvPoint(pPrev.x,pPrev.y-10));
            prevPeak=peakFlags[i-1];
        }

        pPrev = pCurr;

    }
    

    //imshow( "Plot", plotIMG );

}

double Gui::Pix2X(int x, double xmax, double xmin, int width)
{
    double ratio = (xmax-xmin)/width;
    return ratio*x+xmin;
}

double Gui::Pix2Y(int y, double ymax, double ymin, int height)
{
    double ratio = (ymax-ymin)/height;
    return -(ratio*y+ymin);
}

int Gui::X2Pix(double x, double xmax, double xmin, int width)
{
    double ratio = (xmax-xmin)/width;
    return (x-xmin)/ratio;
}

int Gui::Y2Pix(double y, double ymax, double ymin, int height)
{
    double ratio = (ymax-ymin)/height;
    return height-(y-ymin)/ratio;
    //return (y-ymin)/ratio;
}

void Gui::overlayHeartImg(cv::Mat & frame, cv::Point2i position)
{
    Mat coeur;
    
    coeur = imread("../../data/heart_little.png", CV_LOAD_IMAGE_UNCHANGED);
    
    overlayImage(frame, coeur, frame, position);
    
}

void Gui::overlayGraphImg(cv::Mat & frame, cv::Point2i position)
{
    Mat graph;
    
    graph = imread("../../data/vital_signal_graph_little.png", CV_LOAD_IMAGE_UNCHANGED);
    
    overlayImage(frame, graph, frame, position);
    
}

//Thank's to Jepson's Blog http://jepsonsblog.blogspot.fr/2012/10/overlay-transparent-image-in-opencv.html
void Gui::overlayImage(const cv::Mat &background, const cv::Mat &foreground, cv::Mat &output, cv::Point2i location)
{
    background.copyTo(output);
    
    
    // start at the row indicated by location, or at row 0 if location.y is negative.
    for(int y = std::max(location.y , 0); y < background.rows; ++y)
    {
        int fY = y - location.y; // because of the translation
        
        // we are done of we have processed all rows of the foreground image.
        if(fY >= foreground.rows)
            break;
        
        // start at the column indicated by location,
        
        // or at column 0 if location.x is negative.
        for(int x = std::max(location.x, 0); x < background.cols; ++x)
        {
            int fX = x - location.x; // because of the translation.
            
            // we are done with this row if the column is outside of the foreground image.
            if(fX >= foreground.cols)
                break;
            
            // determine the opacity of the foregrond pixel, using its fourth (alpha) channel.
            double opacity =
                    ((double)foreground.data[fY * foreground.step + fX * foreground.channels() + 3])/ 255.;
            
            
            // and now combine the background and foreground pixel, using the opacity,
            
            // but only if opacity > 0.
            for(int c = 0; opacity > 0 && c < output.channels(); ++c)
            {
                unsigned char foregroundPx = foreground.data[fY * foreground.step + fX * foreground.channels() + c];
                unsigned char backgroundPx = background.data[y * background.step + x * background.channels() + c];
                
                output.data[y*output.step + output.channels()*x + c] = backgroundPx * (1.-opacity) + foregroundPx * opacity;
            }
            
        }
    }
}

