#ifndef BANDPASSFILTER_H
#define BANDPASSFILTER_H
#include <vector>
#include <iostream>

#define NZEROS 8
#define NPOLES 8
#define GAIN   1.580359063e+02
struct FilterCoefficients;
class BandPassFilter
{
    /**
     * @brief Taken from http://www.exstrom.com/journal/sigproc/bwbpf.c
     */
    int order,s;
    double f1,f2,a,a2,b,b2,r,x;
//    double A[],d1[],d2[],d3[],d4[],w0[],w1[],w2[],w3[],w4[];
    //Filter coefficients

     float xv[NZEROS+1], yv[NPOLES+1];
     double gain;
     double* xcoeffs;
     double* ycoeffs;
public:
    BandPassFilter();
    BandPassFilter(int filterOrder,int samplingFrequency,double lowerCutoff,double higherCutoff);
    void process(std::vector<double>& x);
    ~BandPassFilter();
};

#endif // BANDPASSFILTER_H
