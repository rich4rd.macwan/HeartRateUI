#ifndef RENDERTHREAD_H
#define RENDERTHREAD_H
#include <QThread>
#include <QReadWriteLock>
#include <cv.h>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/contrib/hybridtracker.hpp"
#include <vector>
#include "qvideo.h"
#include "gui.h"
#include "videoreaderwriter.h"
#include "pulsedetector.h"
#include <QTimer>
class RenderThread : public QObject
{
    Q_OBJECT
public:
    explicit RenderThread(QObject *parent = 0);
    ~RenderThread();
    void stop();
    void pause();
    void play();
    void closeVideoFile();
    void play(int msec);
    bool setRecordMode(bool,QString);
    bool initVideo(QString videoSource, QVideo* video);
    bool initCamera(int cam, QVideo* video);
    bool setVideoSource(QString source, QVideo *video);
    bool setVideoSource(int cameraId, QVideo *video);
    static long long getCurrentFrameId();
    void updateCurrentFrameTimestamp();
    VideoReaderWriter* videoReaderWriter();
    void moveFaceGuides(cv::Mat& frame);
    void getNextOvalCenter(cv::Mat &frame);
    QString getRecordFileName();
    bool displayOval;
    void ensureFaceRectInFrame(cv::Rect&,cv::Mat&);
    bool checkCollision(cv::Point &p1, cv::Point &p2);
    int windowSize();
    void detectSkin(cv::Mat& source);
    void clearTraces();
    void startManualROISelection();
    void setManualROI(cv::Rect roi);
    cv::Rect manualROI;
    void wait();
    int RPPGMethod;
    int windowLengthInFrames();
    bool modeIsFaceDetection;
    bool recordTimerEnabled;

protected:

public slots:
    void run();
    void onPlayTimerElapsed();
    void stopSignalReceived();
    void errorString(QString);

signals:
    //void sendImage(const cv::Mat);
    void finished();
    void sendImage(IplImage&);
    void message(QString,bool);
    void endOfVideo();
    void stopped();
    void paused();
    void newFrameArrived(int);
    void sendRPPGSignal(std::vector<double>);
private:
    int videoOrCamera;
    bool enableRecord;
    bool playing;
    cv::Mat frame;
    cv::Mat contours,mask,maskROI;
    IplImage* iframe;
    IplImage* resized;
    QReadWriteLock lock;
    QString videoSource;
    int cameraSource;
    //cv::VideoCapture vcap;
    //CvCapture* vcap;
    cv::VideoCapture vcap;
    VideoReaderWriter videoWriter;
    bool videoSourceValid;
    int maxWidth, maxHeight;
    /* Main parameters that can be modified */
    int fps ; // Supposed frame rate - calculated for live streaming (Hz)
    int  winLengthSec ;// sliding window length in seconds
    bool useWebcam ; // if true -> use webcam; false -> a video file
    int lastN ; // average last N pulses to smooth results
   //string vidFile = "../../video_5.mj2";
    std::string vidFile;
    int nbFrames;
    bool faceDetected;
    cv::Rect faceRect; // define the rectangle around the face
    Gui myGui; // To control the display
    /* face detection and tracking vaiables */
    QString face_cascade_name;
    std::string face_cascade_name_absolute;
    cv::CascadeClassifier face_cascade;
    cv::HybridTrackerParams params;
    cv::HybridTracker *tracker;
    std::vector<std::vector<double> > traces,detrended,normalized,separated; // the rgb traces
    std::vector<double> pulses;// filtered Heart Rate
    clock_t tframe;
    int idx;
    // fps counter begin
    int idxFps;
    QVideo* video;
    QString recordFileName;
    static long long frameid;
    bool videoThreadBusy;
    PulseDetector pulseDetector;
    quint64 currentFrameTimestamp,prevTimestamp;
    std::vector<Peak> peakFlags;
    QString videoTimestampData;
    QTextStream *videoTimestampDataStream;
    bool videoMetadataValid;
    bool switchOvalMovePattern;
    int ovarlapH,overlapV,overlapC,overlapCC;
    bool started;
    QTimer *playTimer;
    cv::Point ovalCenters[4];
    int overlaps[4];
    int ovalCenterIndex;
    int countdownToSwitch;
    std::vector<double> rppgSignals;
    int manualROISelectionFrameStart;
    int waitCount;
    QThread* thread;
    int nSkipFrames;
    bool manualROISelected;
};


#endif // RENDERTHREAD_H
