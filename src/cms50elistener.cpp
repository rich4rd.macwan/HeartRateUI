#include "cms50elistener.h"
#include <QDebug>
#include <iostream>
#include "mainwindow.h"
#include "renderthread.h"

CMS50eListener::CMS50eListener(QThread *parent)
    :QThread(parent)
{

    QSerialPortInfo spi;
    serialPort=NULL;
    metadataFile=NULL;
    metadataOut=NULL;
    fingerin=false;
    baudRate=19200;
    heartRate=0;
    oxygenSat=0;
    waveY=0;
    currentFrameId=0;
    nextByteHeartRate=false;
    controlInt=134; //Default value of the control byte from cms50e
    paused=false;
    liveMode=true;
    connected=false;
    //Temp option for disabling reading from metadata file
    useMetadataFile=false;
}

CMS50eListener::~CMS50eListener()
{
    stopReading();
}

void CMS50eListener::run()
{
    while(true){
        emit heartRateSPOCalculated(heartRate,oxygenSat);
        if(connected){
            emit message("Monitoring",false);
            //emit message(QString::number(currentFrameId)+"<->"+QString::number(RenderThread::getCurrentFrameId()),false);
        }
        else{
            emit message("Disconnected",false);
        }
        this->msleep(1000);
    }


}

void CMS50eListener::setRecordMode(bool enableRecord, QString recordFileName)
{
    this->enableRecord=enableRecord;
    this->recordFileName=recordFileName;
    connectToSerialPort();
}

/**
 * @brief CMS50eListener::connectToSerialPort
 * Connects to serial port on /dev/ttyUSBO for linux
 */
void CMS50eListener::connectToSerialPort()
{
    //If we are not in live mode,
    if(!liveMode){
        //Set up reading from metadata file instead of live.
        //The metadata file should be of the format <video-name>-oxi.xpm
        if(!connected){
            if(useMetadataFile){
                metadataFileName=videoSource.split(".")[0]+"-oxi.xmp";
                qDebug()<<"CMS50eListener : "<<metadataFileName;
                metadataFile=new QFile(metadataFileName);
                if(metadataFile->open(QIODevice::ReadOnly|QIODevice::Text)){
                    metadataOut=new QTextStream(metadataFile);
                    emit message("Reading pulse data from metadata file",false);
                    connected=true;
                    emit connectStatus(true);
                }
                else{
                    emit message("Cannot open oximeter metadata file! "+((metadataFileName.length()>20)?metadataFileName.left(10)+"..."+metadataFileName.right(20):metadataFileName),true);
                    connected=false;
                    emit connectStatus(false);
                }
            }

        }
        else{ //Toggle connection
            if(metadataFile!=NULL){
                metadataFile->close();
                metadataFile=NULL;
            }
            connected=false;
            emit connectStatus(false);

        }

    }
    else{
        if(serialPort==NULL){
            serialPort=new QSerialPort(this);
            serialPort->setPortName("/dev/ttyUSB0");
            serialPort->setDataBits(QSerialPort::Data8);
            serialPort->setStopBits(QSerialPort::OneStop);
            serialPort->setParity(QSerialPort::OddParity);
            serialPort->setBaudRate(baudRate);
            connected=true;
            //Need to set permissions on /dev/ttyUSB0 for access
            //Udev rule: KERNEL=="ttyUSB0", MODE="0666"
            //in /etc/udev/rules.d/51-myusbdevice.rules
            if(serialPort->open(QIODevice::ReadWrite)){
                qDebug()<<"Serial port"<<serialPort->portName()<<" open";
                count=0;
                fingerin=true;
                connected=true;
                emit connectStatus(true);
                //Prepare metadata file
                if(enableRecord && useMetadataFile){
                    metadataFileName=recordFileName.split(".")[0]+"-oxi"+MainWindow::metadataExt;
                    qDebug()<<"CMS50eListener : "<<metadataFileName;
                    metadataFile=new QFile(metadataFileName);
                    //IMP: If the file exists, it will be replaced.
                    if(metadataFile->open(QIODevice::ReadWrite|QIODevice::Text|QIODevice::Truncate)){
                        metadataOut=new QTextStream(metadataFile);
                        emit message(serialPort->portName()+" open and PPG metadata file ready",false);
                    }
                    else{
                        emit message("Cannot write to metadata file "+metadataFileName,true);
                    }
                }
                else{
                    //                if(metadataFile!=NULL && metadataFile->isOpen()){
                    //                    metadataFile->close();
                    //                }
                    emit message("Switched to live mode. Not recording data from pulse oximeter",false);
                }

            }
            else{
                connected=false;
                liveMode=false;
                qDebug()<<"CMS50E device not present!"<<serialPort->errorString();
                emit message("Connection to serialPort "+serialPort->portName()+" failed: "+serialPort->errorString(),true);
                emit connectStatus(false);
                delete serialPort;
                serialPort=NULL;

            }

        }
        else{
            //Serial port already connected.
            qDebug()<<"Serial port"<<serialPort->portName()<<" open";
            count=0;
            connected=true;
            emit connectStatus(true);
            //Recheck metadata file
            if(enableRecord && useMetadataFile){
                if(metadataFile->isOpen()){
                    emit message(serialPort->portName()+" already open and PPG metadata file ready",false);
                }
                else if (metadataFile->open(QIODevice::ReadWrite|QIODevice::Text)){
                    metadataOut=new QTextStream(metadataFile);
                    emit message(serialPort->portName()+" open and PPG metadata file ready",false);
                }
                else{
                    emit message("Cannot write to metadata file "+metadataFileName,true);
                }
            }
            else{
                //                if(metadataFile!=NULL && metadataFile->isOpen()){
                //                    metadataFile->close();
                //                }
                emit message("Switched to live mode. Not recording data from pulse oximeter",false);
            }

        }
    }

}

void CMS50eListener::disconnectSerialPort()
{
    //Stop
    emit message("Connection to serialPort "+serialPort->portName()+" closed",false);
    if(serialPort->isOpen()){
        serialPort->close();
        serialPort=NULL;
    }
    if(metadataFile!=NULL &&  metadataFile->isOpen()){
        metadataFile->close();
    }
    connected=false;
    emit connectStatus(false);

}

void CMS50eListener::setLiveMode(bool mode, QString videoFileName)
{
    this->liveMode=mode;
    if(!liveMode){
        if(videoFileName==""){ //If we are not in live mode, and the videofilename is not given, show an error
            emit message("CMS50EListener: Video mode requires a video file name",true);
        }
        else{
            this->videoSource=videoFileName;
        }
    }
    else{
        connectToSerialPort();
    }
}

bool CMS50eListener::isLiveMode()
{
    return liveMode;
}

void CMS50eListener::readData()
{
    if(liveMode){
        QByteArray data = serialPort->read(5);
        //5-byte sequence
        if((uchar)data[3]!=0 && (uchar)data[4]!=0 ){
            for(int i=0;i<5;i++){
                int val=(uchar)data[i];
                if(val>128){ //this is the first byte
                    waveY=(uchar)data[(i+1)%5];
                    heartRate=(uchar)data[(i+3)%5];
                    oxygenSat=(uchar)data[(i+4)%5];
                    break;
                }
            }
            emit waveFormY(waveY,QDateTime::currentMSecsSinceEpoch());
        }
        else{ //3-byte sequence
            //qDebug()<<(uchar)data[0]<<","<<(uchar)data[1]<<","<<(uchar)data[2];
            byte1=(uchar)data[0];
            byte2=(uchar)data[1];
            byte3=(uchar)data[2];
            if(byte1>128){
                waveY=byte2;
            }
            else if(byte2>128){
                waveY=byte3;
                oxygenSat=byte1;
            }
            else {
                heartRate=byte1;
                oxygenSat=byte2;
            }

            emit waveFormY(waveY,QDateTime::currentMSecsSinceEpoch());

        }


        //Write current waveform data to file

        if(enableRecord && !paused){
            //TODO: emit OxiData for VideoReaderWriter
            emit sendOxiData(OxiData(currentFrameId,heartRate,oxygenSat,waveY,QDateTime::currentMSecsSinceEpoch()));
//            lock.lockForWrite();
//            //Write waveform data to file
//            line=QString::number(currentFrameId)+":"+QString::number(heartRate)+","+
//                    QString::number(oxygenSat)+","+QString::number(waveY)+","+QString::number(QDateTime::currentMSecsSinceEpoch());
//            *metadataOut<<line<<"\r\n";
//            lock.unlock();
        }

    }
    else{ //Read pulse data from file according to the currentFrameId

        if(useMetadataFile){

            line=metadataOut->readLine();
            //qDebug()<<line;
            if(line.length()>0){
            QStringList tokens=line.split(":");

                //Get frame id from line
                int frameid=tokens[0].toInt();
                //Read values for the current frame. frameid==0 depicts the waveform data before any frame has arrived
                  //while(false){
                while(currentFrameId==frameid || frameid==0){
                    tokens=tokens[1].split(",");
                    heartRate=tokens[0].toInt();
                    oxygenSat=tokens[1].toInt();
                    waveY=tokens[2].toInt();
                    emit waveFormY(waveY,tokens[3].toLongLong());
                    line=metadataOut->readLine();
                    if(line.length()>0){
                        tokens=line.split(":");
                        frameid=tokens[0].toInt();
                    }
                    else{
                        break;
                    }
                }
            }


        }
    }

}

void CMS50eListener::newFrameArrived(int frameid)
{
    currentFrameId=frameid;
    //If not in livemode, read data from file
    if(connected)
        readData();
}

void CMS50eListener::controlSignalReceived(bool connect)
{
    if(connect){
        startReading();
    }
    else{
        stopReading();
    }
}

void CMS50eListener::startReading()
{
    //Try to connect to oximeter if not already connected
    if(!connected){
        connectToSerialPort();
    }
    //If connection was successful, start reading data from it
    if(connected){
    if(liveMode)
    {
        connect(serialPort,SIGNAL(readyRead()),this,SLOT(readData()));
    }
    paused=false;
    start();
    }
}

void CMS50eListener::stopReading()
{
    if(serialPort!=NULL && serialPort->isOpen()){
        emit message("Connection to serialPort "+serialPort->portName()+" closed",false);
        serialPort->close();
        serialPort=NULL;
    }
    if(metadataFile!=NULL && metadataFile->isOpen()){
        metadataFile->close();
        metadataFile=NULL;
    }
    connected=false;
    paused=false;
    //emit connectStatus(false);

}

void CMS50eListener::pauseReading()
{
    emit message("Connection to serialPort "+serialPort->portName()+" closed",false);
    if(serialPort!=NULL && serialPort->isOpen()){
        serialPort->close();
        serialPort=NULL;
    }
    connected=false;
    paused=true;
    emit connectStatus(false);

}



