//#include "videoreaderwriter.h"
#include <QDebug>
#include "mainwindow.h"
#include <QMessageBox>
VideoReaderWriter::VideoReaderWriter(int rows, int cols,int channels, QString filename, QObject *parent) : QObject(parent)
{
    if(filename!=""){
        this->open(filename);
    }
    videometadataFile=NULL;
    videometadataOut=NULL;
    vcap=NULL;
    //Default size for video
    nRows=rows;
    nCols=cols;
    nChannels=channels;
    imageBuffer=new char[nRows*nCols*nChannels];
    totalFrames=0;
    //Average number of heartbeats. Also 432 is the num of dimples on a golf ball
    headerCode=43200;
    //We default to custom video file mode.
    customVideoFileMode=CUSTOM_VIDEO_MODE;
    operationMode=-1;
    recordEnabled=false;
    dirtyBuffer=false;
    dirtyBufferEval=false;
    //Initialize pointers to the two oximeter data vectors. We use two for double buffering.
    //All new oxidata is accumulated to the buffer pointed by frontOxiBuffer
    //When  a frame is to be written, the buffers are swapped so that the data to be written is read from the one that was being accumulated
    //Data is always written to file from the backOxiBuffer
    frontOxiBuffer=&oxibuffer1;
    backOxiBuffer=&oxibuffer2;
    nOxiDataRows=0;
    heartRate=0;
}

VideoReaderWriter::~VideoReaderWriter()
{
    if(vcap!=NULL){
        vcap->release();
        delete vcap;
    }
}
/**
 * @brief VideoReaderWriter::open
 * @param filename Filename to write to if it recording from camera or filename to read from if reading from file
 * @param mode =-1 if reading from file. Otherwise it is WRITE mode and the value is the camera id
 * @param fps
 * @return
 */
bool VideoReaderWriter::open(QString filename, int mode,int cameraId)
{
    operationMode=mode;
    qDebug()<<"VideoWriter::open() called filename="<<filename;
    this->filename=filename;
    QFileInfo fileInfo(filename);
    QFileInfo evalfileInfo(MainWindow::instance->currentDumpFilePath());
    QString fileNameWithoutExtension=fileInfo.baseName();
    bool returnValue=false;
    separator=dir.separator().cell();
    if(mode==READ_VIDEO){
        QString filename=(fileInfo.absolutePath().endsWith("/"))?fileInfo.absolutePath()+fileNameWithoutExtension+".vid":fileInfo.absolutePath()+ separator+fileNameWithoutExtension+".vid";
        embeddedFilename=(fileInfo.absolutePath().endsWith("/"))?fileInfo.absolutePath()+fileNameWithoutExtension+"-embedded.xmp":fileInfo.absolutePath()+ separator+fileNameWithoutExtension+"-embedded.xmp";
        QString methodDir="";
        //Create a directory for the rppg method to save the  dump files separately
        if( evalfileInfo.exists() ){
            if(evalfileInfo.isDir()){
                //Evaluation directory is present
                if(MainWindow::instance->RPPGMethod()!=""){
                    methodDir=MainWindow::instance->RPPGMethod();
                    //If ends with /, dont add the separator
                    QFileInfo methodDirInfo((evalfileInfo.absoluteFilePath().endsWith("/"))?evalfileInfo.absoluteFilePath()+methodDir:evalfileInfo.absoluteFilePath()+separator+methodDir);
                    //If method directory is not present, create it
                    if(!methodDirInfo.exists()){
                        QDir mDir;
                        mDir.mkdir(methodDirInfo.absoluteFilePath());
                    }
                }
                evaluationFilename=evalfileInfo.absoluteFilePath()+ separator+methodDir+separator+"rppgdump-"+fileNameWithoutExtension+".txt";
            }
            else{
                evaluationFilename=evalfileInfo.absoluteFilePath();
            }
        }
        else{
            if(evalfileInfo.absoluteDir().exists()){
                //Dumpfile not present. Normal. will create one
                if(MainWindow::instance->RPPGMethod()!=""){
                    methodDir=MainWindow::instance->RPPGMethod();
                    //If ends with /, dont add the separator
                    QFileInfo methodDirInfo(evalfileInfo.absoluteDir().absolutePath()+separator+methodDir);
                    //If method directory is not present, create it
                    if(!methodDirInfo.exists()){
                        QDir mDir;
                        mDir.mkdir(methodDirInfo.absoluteFilePath());
                    }
                }
                evaluationFilename=evalfileInfo.absolutePath()+ separator+methodDir+separator+"rppgdump-"+fileNameWithoutExtension+".txt";
            }

            else{
                message("Evaluation directory may not be set up. Please click on Run Evaluation and set it.",true);
                qDebug()<<"Evaluation directory may not set up. Please click on Run Evaluation and set it.";
            }
        }

        qDebug()<<"VideoReaderWriter::open():: Saving evaluation dump to "<<evaluationFilename;
        inFile.open(filename.toStdString().c_str(),std::ios::binary|std::ios::in);
        //Read header and update nRows,nCols, etc.
        int headercode;
        inFile.read(reinterpret_cast<char *>(&headercode),sizeof(int));
        if(headercode!=headerCode){
            //This is probably not our encoded file. Give a corrupt file message
            emit message("Video file seems to be corrupt. It has to be recorded using this software.",true);
            inFile.close();
            //Create a videocapture
            qDebug()<<"VideoWriter::open() reading generic video :"<<this->filename;
            vcap=new cv::VideoCapture(this->filename.toStdString().c_str());
            //vcap->open(filename.toStdString().c_str());
            if(vcap->isOpened()){
                customVideoFileMode=GENERIC_VIDEO_OR_CAM_MODE;
                nCols=vcap->get(CV_CAP_PROP_FRAME_WIDTH);
                nRows=vcap->get(CV_CAP_PROP_FRAME_HEIGHT);
                framerate=vcap->get(CV_CAP_PROP_FPS);
                returnValue=true;
            }
            else{
                qDebug()<<"RenderThread(initVideo): Error opening video";
                emit message("Error opening video",true);
                customVideoFileMode=ERROR_VIDEO_MODE;
                returnValue=false;
            }
        }
        else{
            customVideoFileMode=CUSTOM_VIDEO_MODE;
            readFileHeader();

            returnValue=true;
        }

    }

    else {
        //Write mode. We can read frames from video or from file(in case we need to convert any video to our format)


        QString filename=fileInfo.absolutePath()+ separator+fileNameWithoutExtension+".vid";

        if(mode==READ_CAM){
            //Init the video reader to camera
            if(cameraId==-1){
                //Camera id was not provided. Defaulting to 0
                cameraId=0;
            }
            //If already opened, close it
            if(vcap!=NULL && vcap->isOpened()){
                vcap->release();
                delete vcap;
                vcap=NULL;
            }
            vcap=new cv::VideoCapture(cameraId);
            if(vcap->isOpened()){
                customVideoFileMode=GENERIC_VIDEO_OR_CAM_MODE;
                //Init rows, cols, etc.
                nCols=vcap->get(CV_CAP_PROP_FRAME_WIDTH);
                nRows=vcap->get(CV_CAP_PROP_FRAME_HEIGHT);
                framerate=vcap->get(CV_CAP_PROP_FPS);

                //Camera does not report fps
                if(framerate<=0){
                    //TODO: Maybe infer fps in a better way.
                    framerate=30;
                }
                returnValue=true;
            }
            else{
                qDebug()<<"VideoReaderWriter: open(cameraId): Error opening source";
                emit message("Error opening source",true);
                customVideoFileMode=ERROR_VIDEO_MODE;
                returnValue=false;
            }


        }
        else if (mode==READ_VIDEO_WRITE_VIDEO){
            //This mode is used when we want to read from a generic video and convert it to our custom format
            vcap->open(filename.toStdString().c_str());
            if(vcap->isOpened()){
                customVideoFileMode=GENERIC_VIDEO_OR_CAM_MODE;
                returnValue=true;
            }
            else{
                qDebug()<<"RenderThread(initVideo): Error opening video";
                emit message("Error opening video",true);
                customVideoFileMode=ERROR_VIDEO_MODE;
                returnValue=false;
            }
            //Init stuff to write custom video frames
            outFile.open((filename+"_custom").toStdString().c_str(),std::ios::binary|std::ios::out|std::ios::trunc|std::ios::app);
            //Write video file header
            //First byte is the heaeder code followed by the  number of frames. We will update the frame number at the end when we close the video

            writeFileHeader();
            //End frame header
        }

    }

    return returnValue;

}

bool VideoReaderWriter::enableRecord(QString recordFilename)
{
    if(customVideoFileMode==ERROR_VIDEO_MODE){
        return false;
    }
    
    this->storageDir="";
    
    this->filename=recordFilename;
    QFileInfo fileInfo(filename);
    QString storageDirName=fileInfo.baseName();
    bool returnValue=false;
    separator=dir.separator().cell();
    
    ffmpegProcessString="ffmpeg -framerate "+ QString::number(this->framerate)+" -i frame\%d.png -vcodec r210 movie.mov";
    
    //Make directory of the filename(minus extension) in the temp directory
    
    //Set the output video and metadata filenames
    filenameToSave=fileInfo.absolutePath()+ separator+storageDirName+".mov";
    
    //Init stuff for video metadata file
    //    videometadataFileName=fileInfo.absolutePath()+ separator+storageDirName+"-vid"+MainWindow::metadataExt;
    //    videometadataFile=new QFile(videometadataFileName);
    //    if(videometadataFile->open(QIODevice::ReadWrite|QIODevice::Text|QIODevice::Truncate)){
    //        videometadataOut=new QTextStream(videometadataFile);
    //    }
    //    else{
    //        qDebug()<<"Cannot write to video metadata file "<<videometadataFileName;
    //        emit message("Cannot write to video metadata file "+videometadataFileName,true);
    //        returnValue=false;
    //    }
    
    //Prepare directory to save images
    //Change to the temp directory
    //    dir.cd(dir.tempPath());
    //    if(dir.cd(storageDirName)){
    //        //If the directory is present, empty the directory of .png files
    //        emit message(tr("Clearing temp directory for generating uncompressed video"),false);
    //        qDebug()<<"Clearing temp directory for generating uncomrpessed video";
    //        dir.setNameFilters(QStringList()<<"*.png"<<"*.mov");
    //        dir.setFilter(QDir::Files);
    //        qDebug()<<"Clearing image cache";emit endProcessingVideo("Successfully built video. Clearing cache.",false);
    //        //        foreach(QString dirFile, dir.entryList())
    //        //        {
    //        //            dir.remove(dirFile);
    //        //        }
    //        //emit message("Temp directory cleared.",false);
    //        //qDebug()<<"Temp directory cleared.";
    //        storageDir=dir.absolutePath();
    //        // qDebug()<<"Videowriter: Storage dir : "<<storageDir;
    //        returnValue=true;
    //    }
    //    else {
    //        if(dir.mkdir(storageDirName)){
    //            dir.cd(storageDirName);
    //            storageDir=dir.absolutePath();
    //            qDebug()<<"Videowriter: Storage dir : "<<storageDir;
    //            emit message(tr("Temp directory created for generating uncompressed video."),false);
    //            returnValue=true;
    //        }
    //        else{
    //            emit message(tr("Cannot create temp directory for generating uncompressed video!"),true);
    //            returnValue=false;
    //        }
    //    }
    
    
    
    
    QString filename=fileInfo.absolutePath()+ separator+storageDirName+".vid";
    //Init stuff to write custom video frames if we have enabled record
    outFile.open(filename.toStdString().c_str(),std::ios::binary|std::ios::out|std::ios::trunc);
    
    //Write video file header
    //First byte is the heaeder code followed by the  number of frames. We will update the frame number at the end when we close the video
    if(outFile.is_open()){
        writeFileHeader();
        recordEnabled=true;
        //End frame header
        return true;
        
    }
    else{
        std::cout<<"VideoReaderWriter cannot write to outFile: "<<filename.toStdString()<<std::endl;
        recordEnabled=false;
    }
    
    
}

void VideoReaderWriter::writeFrame(cv::Mat frame, int frameNumber,quint64 timestamp)
{
    
    //  cv::imwrite()
    //    std::ostringstream outframe;
    //    outframe<<storageDir.toStdString()<<separator<<"frame"<<frameNumber<<".png";
    
    //    cv::imwrite(outframe.str(),frame);
    writeFrameHeader(frameNumber,nRows*nCols*nChannels);
    writeRawFrame(frame);
    //write timestamp to file
    outFile.write(reinterpret_cast<const char *>(&timestamp),sizeof(quint64));
    // qDebug()<<"VideoReaderWriter wrote timestamp="<<timestamp;
    
    writeOxiData();
    //Should use the timestamp when the image was captured!
    //line=QString::number(frameNumber)+":"+QString::number(QDateTime::currentMSecsSinceEpoch());
    //    line=QString::number(frameNumber)+":"+QString::number(timestamp);
    //    *videometadataOut<<line<<"\r\n";
    totalFrames=frameNumber;
}
/**
 * @brief VideoReaderWriter::writeRawFrame
 * @param frame
 * Write raw frame data to file using the custom video frame format defined for this application
 */
void VideoReaderWriter::writeRawFrame(cv::Mat frame)
{
    //Write to video file
    
    outFile.write((const char*)frame.data,frame.rows*frame.cols*frame.channels());
}

/**
 * @brief VideoReaderWriter::readFrame
 * Read one frame from the video file and return it
 * @return
 */
bool VideoReaderWriter::readFrame(cv::Mat& frame)
{
    //qDebug()<<"customVideoFileMode="<<customVideoFileMode;
    if(customVideoFileMode==CUSTOM_VIDEO_MODE){
        //Read frame header
        inFile.read(reinterpret_cast<char *>(&currentFrameId),sizeof(int));
        inFile.read(reinterpret_cast<char *>(&dataSize),sizeof(int));
        inFile.read(reinterpret_cast<char *>(&framerate),sizeof(int));
        //qDebug()<<"currentFrameId="<<currentFrameId<<",dataSize="<<dataSize;
        //Read image data
        inFile.read(imageBuffer,dataSize);
        memcpy(frame.data,(uchar*)imageBuffer,dataSize);
        //read timestamp from file
        inFile.read(reinterpret_cast<char *>(&frametimestamp),sizeof(quint64));
        // qDebug()<<"Read timestamp= "<<frametimestamp;
        readOxiData();
        if(inFile.eof()){
            return false;
        }
    }
    else if(customVideoFileMode==GENERIC_VIDEO_OR_CAM_MODE){
        if(vcap==NULL){
            qDebug()<<"This should not happen!";
            vcap=new cv::VideoCapture(this->filename.toStdString().c_str());
            if(!vcap->isOpened()){
            QMessageBox::critical(NULL,"Error","Cannot open video source! Please restart the application.");
            return false;
            }
        }
        //qDebug()<<"VideoReaderWriter read frame vcap.isOpen?"<<vcap->isOpened();
        vcap->read(frame);
        
        
        
    }
    else{
        return false;
    }
    //cv::imshow("Frame",frame);
    //cv::waitKey();
    return true;
}

bool VideoReaderWriter::readFrame(cv::Mat &frame, quint64 &currentFrameTimestamp,int &fps)
{
    //qDebug()<<"customVideoFileMode="<<customVideoFileMode;
    if(customVideoFileMode==CUSTOM_VIDEO_MODE){
        //Read frame header
        inFile.read(reinterpret_cast<char *>(&currentFrameId),sizeof(int));
        inFile.read(reinterpret_cast<char *>(&dataSize),sizeof(int));
        inFile.read(reinterpret_cast<char *>(&framerate),sizeof(int));
        //qDebug()<<"currentFrameId="<<currentFrameId<<",dataSize="<<dataSize;
        //Read image data
        inFile.read(imageBuffer,dataSize);
        memcpy(frame.data,(uchar*)imageBuffer,dataSize);
        //read timestamp from file
        inFile.read(reinterpret_cast<char *>(&frametimestamp),sizeof(quint64));
       // qDebug()<<frametimestamp-currentFrameTimestamp;
        currentFrameTimestamp=frametimestamp;

        //qDebug()<<"VideoReaderWriter::readFrame()-->currentFrameTimestamp="<<currentFrameTimestamp<<", frameid="<<currentFrameId;
        fps=framerate;
        // qDebug()<<"Read timestamp= "<<frametimestamp;
        readOxiData();
        if(inFile.eof()){
            return false;
        }
    }
    else if(customVideoFileMode==GENERIC_VIDEO_OR_CAM_MODE){
        if(vcap==NULL){
            qDebug()<<"This should not happen!";
            vcap=new cv::VideoCapture(this->filename.toStdString().c_str());

            if(!vcap->isOpened()){
            QMessageBox::critical(NULL,"Error","Cannot open video source! Please restart the application.");
            return false;
            }
        }
        //qDebug()<<"VideoReaderWriter read frame vcap.isOpen?"<<vcap->isOpened();
        vcap->read(frame);
        
    }
    else{
        return false;
    }
    //cv::imshow("Frame",frame);
    //cv::waitKey();
    return true;
}

void VideoReaderWriter::close()
{
    if(vcap!=NULL && vcap->isOpened()){
        vcap->release();
        vcap=NULL;
    }
    
    //Close metadata file
    //    if(videometadataFile!=NULL){
    //        videometadataFile->close();
    //        videometadataFile=NULL;
    //        videometadataOut=NULL;
    //    }
    //Write the total number of frames
    if((operationMode==READ_CAM && recordEnabled )|| operationMode==READ_VIDEO_WRITE_VIDEO){
        //BUGFIX: For a lot of videos, this code was executed thereby writing the wrong number of totalFrames
        //TODO: Run all the videos to the end while this is enabled to fix.
        outFile.seekp(std::ios_base::beg+sizeof(int));
        outFile.write(reinterpret_cast<const char *>(&totalFrames),sizeof(int));
    }
    //qDebug()<<"VideoReaderWriter::close() updated totalFrames="<<totalFrames;
    outFile.close();
    //Execute ffmpeg to build a movie
    //buildVideoFromImage();
    //Write data to file for embedded version
    //qDebug()<<embeddedFilename;
    if(dirtyBuffer){
        outFile.open(embeddedFilename.toStdString().c_str(),std::ios::out|std::ios::trunc);
        if(outFile.is_open()){
            std::string dataToWrite=embeddedostr.str();
            outFile.write(dataToWrite.c_str(),dataToWrite.length());
        }
        outFile.close();
    }
    if(dirtyBufferEval){
        //Check if file exists
        bool writeToFile=false;
        std::ifstream ifile(evaluationFilename.toStdString().c_str());
        if(ifile){ //File exists
            if(MainWindow::instance->overwriteDumpFiles()){
                writeToFile=true;
            }
        }
        else{
            //File does not exist
            writeToFile=true;
        }
        if(writeToFile){
            emit message("Overwriting dump file : "+evaluationFilename,false);
            outFile.open(evaluationFilename.toStdString().c_str(),std::ios::out|std::ios::trunc);
            
            if(outFile.is_open()){
                std::string dataToWrite=evaluationstr.str();
                outFile.write(dataToWrite.c_str(),dataToWrite.length());
            }
            outFile.close();
        }
        else{
            message("Skipping dump file : "+evaluationFilename,false);
        }
        
    }
    
}

void VideoReaderWriter::buildVideoFromImage()
{
    ffmpegProcess=new QProcess(this);
    //Connect signals and slots for the ffmpeg process
    //    //Run ffmpeg command here
    dir.cd(storageDir);
    dir.setCurrent(storageDir);
    ffmpegProcess->setWorkingDirectory(dir.absolutePath());
    ffmpegProcess->waitForStarted(-1);
    qDebug()<<"Executing : "<<ffmpegProcessString;
    int exitCode=ffmpegProcess->execute(ffmpegProcessString);
    if(exitCode==-2){
        qDebug()<<"ffmpeg process could not be started";
        emit message("ffmpeg process could not be started",true);
    }
    else if(exitCode==-1){
        qDebug()<<"ffmpeg process crashed!";
        emit message("ffmpeg process crashed!",true);
    }
    else{
        qDebug()<<"ffmpeg process exited normally with exit code "<<exitCode;
        emit message("ffmpeg process exited normally with exit code " + QString::number(exitCode),false);
    }
    qDebug()<<ffmpegProcess->workingDirectory();
    //Move video
    qDebug()<<"Executing command: mv movie.mov "<<filenameToSave;
    ffmpegProcess->execute("mv movie.mov "+filenameToSave);
    
    //Delete image cache
    emit message(tr("Clearing temp directory"),false);
    qDebug()<<"Clearing temp directory";
    dir.setNameFilters(QStringList()<<"*.png");
    dir.setFilter(QDir::Files);
    //    qDebug()<<"Clearing image cache";
    //    foreach(QString dirFile, dir.entryList())
    //    {
    //        dir.remove(dirFile);
    //    }
    //    emit message("Temp directory cleared.",false);
    //    qDebug()<<"Temp directory cleared.";
    
    
    //Disable record
    emit endProcessingVideo("Successfully built video.",false);
}

int VideoReaderWriter::rows()
{
    return nRows;
}

int VideoReaderWriter::cols()
{
    return nCols;
}

int VideoReaderWriter::channels()
{
    return nChannels;
}

int VideoReaderWriter::fps()
{
    return framerate;
}

void VideoReaderWriter::setFps(int fps)
{
    framerate=fps;
}

void VideoReaderWriter::writeFileHeader()
{
    outFile.write(reinterpret_cast<const char *>(&headerCode),sizeof(int));
    outFile.write(reinterpret_cast<const char *>(&totalFrames),sizeof(int));
    outFile.write(reinterpret_cast<const char *>(&nRows),sizeof(int));
    outFile.write(reinterpret_cast<const char *>(&nCols),sizeof(int));
    outFile.write(reinterpret_cast<const char *>(&nChannels),sizeof(int));
}

void VideoReaderWriter::writeFrameHeader(int frameNumber, int dataSize)
{
    outFile.write(reinterpret_cast<const char *>(&frameNumber),sizeof(int));
    outFile.write(reinterpret_cast<const char *>(&dataSize),sizeof(int));
    outFile.write(reinterpret_cast<const char *>(&framerate),sizeof(int));
}

void VideoReaderWriter::writeOxiData()
{
    swapOxiBuffers();
    //Write number of rows in oxidata vector
    int n=backOxiBuffer->size();
    //Write number of oxidata rows to file
    outFile.write(reinterpret_cast<const char *>(&n),sizeof(int));
    //qDebug()<<"Writing oxidata to file: "<<n<<" oxidata rows";
    //Write each oxidata row
    for(int i=0;i<n;i++){
        OxiData od=backOxiBuffer->at(i);
        
        //  qDebug()<<"Oxidata:"<<od.frameId<<","<<od.heartRate<<","<<od.oxygenSaturation<<","<<od.waveformValue<<","<<od.timestamp;
        outFile.write(reinterpret_cast<const char *>(&od.frameId),sizeof(int));
        outFile.write(reinterpret_cast<const char *>(&od.heartRate),sizeof(int));
        outFile.write(reinterpret_cast<const char *>(&od.oxygenSaturation),sizeof(int));
        outFile.write(reinterpret_cast<const char *>(&od.waveformValue),sizeof(int));
        outFile.write(reinterpret_cast<const char *>(&od.timestamp),sizeof(quint64));
    }
}

void VideoReaderWriter::readOxiData()
{
    //Read number of rows from oxidata header
    inFile.read(reinterpret_cast<char *>(&nOxiDataRows),sizeof(int));
    //Read n rows from the file and emit the data to be displayed
    // qDebug()<<"Oxidata rows="<<nOxiDataRows;
    for(int i=0;i<nOxiDataRows;i++){
        OxiData oxidata;
        inFile.read(reinterpret_cast<char *>(&oxidata.frameId),sizeof(int));
        inFile.read(reinterpret_cast<char *>(&oxidata.heartRate),sizeof(int));
        //Set current heart rate so that it can be written to dump file.
        if(oxidata.heartRate!=0)
            heartRate=oxidata.heartRate;

        inFile.read(reinterpret_cast<char *>(&oxidata.oxygenSaturation),sizeof(int));
        inFile.read(reinterpret_cast<char *>(&oxidata.waveformValue),sizeof(int));
        currentGTSignal=oxidata.waveformValue;
       // std::cout<<oxidata.frameId<<":"<<oxidata.heartRate<<":"<<oxidata.waveformValue<<", currentGTSignal="<<currentGTSignal<<std::endl;

        inFile.read(reinterpret_cast<char *>(&oxidata.timestamp),sizeof(quint64));

        emit sendOxiData(oxidata);
    }

    
}

void VideoReaderWriter::readFileHeader()
{
    inFile.read(reinterpret_cast<char *>(&totalFrames),sizeof(int));
    inFile.read(reinterpret_cast<char *>(&nRows),sizeof(int));
    inFile.read(reinterpret_cast<char *>(&nCols),sizeof(int));
    inFile.read(reinterpret_cast<char *>(&nChannels),sizeof(int));
    qDebug()<<"Read file header: "<<totalFrames<<","<<nRows<<","<<nCols<<","<<nChannels;
}

void VideoReaderWriter::swapOxiBuffers()
{
    //Swap the buffers
    bufferLock.lockForWrite();
    std::vector<OxiData>* temp=frontOxiBuffer;
    frontOxiBuffer=backOxiBuffer;
    backOxiBuffer=temp;
    //Clear the old oximeter data
    frontOxiBuffer->clear();
    bufferLock.unlock();
}

int VideoReaderWriter::CustomVideoFileMode()
{
    return customVideoFileMode;
}

void VideoReaderWriter::writeFrameDataForEmbedded(long long frameid, quint64 currentFrameTimestamp, std::vector<double> rppgValue)
{
    dirtyBuffer=true;
    embeddedostr<<frameid<<","<<currentFrameTimestamp<<","<<rppgValue[0]<<","<<rppgValue[1]<<","<<rppgValue[2]<<std::endl;
    // std::cout<<frameid<<","<<currentFrameTimestamp<<","<<rppgValue[0]<<","<<rppgValue[1]<<","<<rppgValue[2]<<std::endl;
}

void VideoReaderWriter::writeFrameDataForEvaluation(long long timestamp,std::vector<double> rppgValue)
{
    dirtyBufferEval=true;
    qDebug()<<"Writing heartRate: "<<heartRate<<", currentGTSignal="<<currentGTSignal;
    evaluationstr<<timestamp<<","<<heartRate<<","<<rppgValue[0]<<","<<rppgValue[1]<<","<<rppgValue[2]<<","<<currentGTSignal<<std::endl;
}

int VideoReaderWriter::currentHeartRate()
{
    return heartRate;
}

void VideoReaderWriter::onOxiDataReceived(OxiData oxidata)
{
    //Works!!
    //qDebug()<<"VideoReaderWriter: Oxidata received";
    frontOxiBuffer->push_back(oxidata);
}
