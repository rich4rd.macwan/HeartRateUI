#include "waveformwidget.h"
#include "mainwindow.h"
#include <QPainter>
#include <QDebug>

#define sign(a) ( ( (a) < 0 )  ?  -1   : ( (a) > 0 ) )
//WaveformWidget::WaveformWidget(QWidget *parent) : QWidget(parent)
//{
//setStyleSheet("background-color:white");


//setMinimumWidth(400);
//setMinimumHeight(250);
//}

WaveformWidget::~WaveformWidget()
{

}

//void WaveformWidget::paintEvent(QPaintEvent *)
//{

//}



WaveformWidget::WaveformWidget(QWidget *parent)
    : QWidget(parent)
{
    shape = Path;
    antialiased = true;
    transformed = false;
    showTimestamp = false;

    showOximeterIBI=true;
    resultLabel="Oximiter IBI";
    waveformLabel="Pulse Waveform";
    textBG=QColor(130,150,250);
    xInc=5;
    waveformIndex=0;
    pixmap.load(":/images/webcam.png");
    //setPalette(parent->palette());
    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);

    padding=0;
    gridSizePx=50;
    gridOn=false;
    //    right=QPoint(this->width(),this->height()/2);
    left=QPoint(padding,this->sizeHint().height()/2);
    right=QPoint(this->width()-padding,this->height()/2);

    // this->setGeometry(0,0,400,300);
    horizontalAxisColor=QColor(qRgb(100,100,100));
    curveColor=QColor(qRgb(10,210,30));
    curvePen=QPen(curveColor);

    curvePen.setStyle(Qt::SolidLine);
    curvePen.setWidth(2);

    gridPen=QPen(horizontalAxisColor);

    pen=QPen(qRgb(0,0,0));
    pen.setWidthF(1.5);
    font.setPixelSize(14);

    rect=QRect(10,20,150,25);
    timestampRect=QRect(25,50,200,50);
    //connect(&timer,SIGNAL(timeout()),this,SLOT(onTimerElapsed()));
    //timer.start(100);
    yValue=0;
    yMax=0;
    yMin=100000;
    yStep=0.1;
    xValue=0;

    xValueGRPPG=0;
    yValueGRPPG=0;
    yGRPPGDisplay=0;
    yMaxGRPPG=0;
    yMinGRPPG=1000;

}

QSize WaveformWidget::minimumSizeHint() const
{
    return QSize(100, 100);
}

QSize WaveformWidget::sizeHint() const
{
    return QSize(400, 200);
}

void WaveformWidget::resizeEvent(QResizeEvent *)
{
    left.setX(padding);
    left.setY(this->height()/2);

    right.setX(this->width()-padding);
    right.setY(this->height()/2);

    top.setX(padding);
    top.setY(padding);
    bottom.setX(padding);
    bottom.setY(this->height());
    //    qDebug()<<sizeHint().width()<<","<<sizeHint().height();
    //    qDebug()<<this->width()<<","<<this->height();

}

void WaveformWidget::setCurvePenColor(QColor color)
{
    curvePen.setColor(color);
}

void WaveformWidget::setShape(Shape shape)
{
    this->shape = shape;
    update();
}

void WaveformWidget::setPen(const QPen &pen)
{
    this->pen = pen;
    update();
}

void WaveformWidget::setBrush(const QBrush &brush)
{
    this->brush = brush;
    update();
}

void WaveformWidget::setAntialiased(bool antialiased)
{
    this->antialiased = antialiased;
    update();
}

void WaveformWidget::setTransformed(bool transformed)
{
    this->transformed = transformed;
    update();
}

void WaveformWidget::setGRPPGWaveformY(int yDisp)
{

    //path.lineTo(QPointF(xValue,yDisplay));
    if(xValueGRPPG==0)
        pathGRPPG.moveTo(QPointF(xValueGRPPG,yGRPPGDisplay));
    else
        pathGRPPG.lineTo(QPointF(xValueGRPPG,yGRPPGDisplay));

    yGRPPGDisplay=yDisp;
    xValueGRPPG+=5;
    //    yValue+=yStep;
    //    yDisplay=sin(yValue);
    //Keep track of max and min value
    if(yGRPPGDisplay>yMaxGRPPG){
        yMaxGRPPG=yGRPPGDisplay;
        if(yMaxGRPPG>this->height()){
            yMaxGRPPG=this->height();
        }
    }
    if(yGRPPGDisplay<yMinGRPPG){
        yMinGRPPG=yGRPPGDisplay;
    }
    //qDebug()<<yGRPPGDisplay;
    //Normalize and flip for the window. The 5 is for padding
    yGRPPGDisplay=(1.0-(double)yGRPPGDisplay/(yMaxGRPPG-yMinGRPPG))*(double)this->height()-50;


    // repaint();
}

void WaveformWidget::setWaveformY(int yDisp, quint64 timestamp)
{
    //path.lineTo(QPointF(xValue,yDisplay));
    //    if(xValue==0)
    //        path.moveTo(QPointF(xValue,yDisplay));
    //    else
    //        path.lineTo(QPointF(xValue,yDisplay));

    //    yDisplay=yDisp;
    //    xValue+=1;
    //    //    yValue+=yStep;
    //    //    yDisplay=sin(yValue);
    //    //Keep track of max value
    //    if(yDisplay>yMax){
    //        yMax=yDisplay;
    //        if(yMax>this->height()){
    //            yMax=this->height();
    //        }
    //    }

    //    //Normalize and flip for the window. The 5 is for padding
    //    yDisplay=(1.0-(double)yDisplay/yMax)*(double)(this->height()-5)+5;
    waveform.push_back(yDisp);
    timestamps.push_back(timestamp);
    //xInc=width()/waveform.size();
    //qDebug()<<xInc;
    if(waveformIndex<MainWindow::instance->windowSize()){
        ++waveformIndex;
    }
    else{
        //Do sliding window
        waveform.erase(waveform.begin());
        timestamps.erase(timestamps.begin());
    }

    prevTimestamp=currentTimestamp;
    currentTimestamp=timestamp;
    repaint();

}

void WaveformWidget::setWaveformY(double yValue)
{
    //path.lineTo(QPointF(xValue,yDisplay));
    //    if(xValue==0)
    //        path.moveTo(QPointF(xValue,yDisplay));
    //    else
    //        path.lineTo(QPointF(xValue,yDisplay));


    xValue+=xInc;
    //    yValue+=yStep;

    //Keep track of max value
    //    yDisplay=yValue;
    //    if(yDisplay<2 && yDisplay>yMax){
    //        yMax=yDisplay;
    //    }
    //    if(yDisplay>-2 && yDisplay<yMin){
    //        yMin=yDisplay;
    //    }

    //Normalize and flip for the window. The 5 is for padding
    // qDebug()<<yDisplay<<","<<yMax<<","<<yMin<<","<<this->height()-5;
    // yDisplay=(yDisplay-yMin)/(yMax-yMin)*(double)(this->height()-10)+5;
    //qDebug()<<"Waveformwidget set "<<objectName()<<", value="<<yValue;
    waveform.push_back(yValue);
    //xInc=width()/waveform.size();
    //qDebug()<<xInc;
    if(waveformIndex<MainWindow::instance->windowSize()){
        ++waveformIndex;
    }
    else{
        //Do sliding window
        waveform.erase(waveform.begin());

    }
    //   qDebug()<<yDisplay;
    repaint();
}

void WaveformWidget::setGridOn(bool val)
{
    gridOn=val;
    repaint();
}

void WaveformWidget::updateView()
{
    currDiff=yDisplay-yPrev;

    if(sign(prevDiff)!=sign(currDiff) && currDiff>0){

        //Peak or valley. Display timestamp value

        showTimestamp=currentTimestamp-prevTimestamp;

        setResultLabel(QString::number(showTimestamp));
        prevTimestamp=currentTimestamp;
    }


    yPrev=yDisplay;
    prevDiff=currDiff;
    //repaint();
}

void WaveformWidget::setWaveformLabel(QString val)
{
    waveformLabel=val;
    repaint();
}

void WaveformWidget::setResultLabel(QString val)
{
    resultLabel=val;
}

void WaveformWidget::clearTraces()
{
    waveform.clear();
}

void WaveformWidget::paintEvent(QPaintEvent * /* event */)
{



    //    static const QPoint points[4] = {
    //        QPoint(10, 80),
    //        QPoint(20, 10),
    //        QPoint(80, 30),
    //        QPoint(90, 70)
    //    };



    //    int startAngle = 20 * 16;
    //    int arcLength = 120 * 16;



    QPainter painter(this);
    //    if(xValue>this->width()){
    //        path=QPainterPath();
    //        xValue=0;
    //        //yMin=10000;

    //        //yMax=0;

    //    }
    //    else{
    //        path.lineTo(QPointF(xValue,yDisplay));
    //    }
    //    if(xValueGRPPG>this->width()){
    //        pathGRPPG=QPainterPath();
    //        xValueGRPPG=0;
    //    }
    //    else{
    //        pathGRPPG.lineTo(QPointF(xValueGRPPG,yGRPPGDisplay));
    //    }


    painter.setPen(pen);
    painter.setBrush(brush);
    if (antialiased)
        painter.setRenderHint(QPainter::Antialiasing, true);

    painter.save();


    switch (shape) {
    //                painter.drawLine(left, right);
    //                painter.drawPoints(points, 4);
    //                painter.drawPolyline(points, 4);
    //                painter.drawPolygon(points, 4);
    //                painter.drawRect(rect);
    //                painter.drawRoundedRect(rect, 25, 25, Qt::RelativeSize);
    //                painter.drawEllipse(rect);
    //                painter.drawArc(rect, startAngle, arcLength);
    //                painter.drawChord(rect, startAngle, arcLength);
    //                painter.drawPie(rect, startAngle, arcLength);
    case Path:



        //Draw the curve
        painter.setPen(curvePen);

        // painter.drawPath(path);
        //painter.drawPolyline(waveform.data(),waveform.size());
        yMax=0;
        yMin=1;
        for(int i=0;i<waveform.size();i++){
            if(yMax<waveform[i]){
                yMax=waveform[i];
            }
            if(yMin>waveform[i]){
                yMin=waveform[i];
            }
        }
        //qDebug()<<"yMax="<<yMax<<",yMin="<<yMin;
        //setResultLabel(QString::number(yMax)+","+QString::number(yMin));
        //        if(yMax>2) yMax=0.3;
        //        if(yMin<-2) yMin=-0.3;
        QPointF pPrev,pCurr;
//        quint64 currTs=0,prevTs=0;
//        double currPeak=0,prevPeak=0;
        int prevDiff;
        for(int i=0;i<waveform.size();i++){
            pCurr.setX(double(i)*width()/waveform.size());
            pCurr.setY((waveform[i]-yMin)/(yMax-yMin)*(double)(this->height()-10)+5);
            painter.drawLine(pPrev,pCurr);
        //    painter.drawText(pCurr.x(),pCurr.y()-10,QString::number(waveform[i]));
//            int prevSign=sign(prevDiff);
//            int currSign=sign(pPrev.y()-pCurr.y());
//            if(timestamps.size()>0 && prevSign != currSign && currSign<0)
//            {
//                currTs=timestamps[i];
//                currPeak=pCurr.y();
//                //We have a peak here at prevPeak
//                if(currTs-prevTs<5000){ //For skipping the point where the timestamp diff is wrong. happens once in a while
//                    painter.fillRect(pPrev.x()-3,pPrev.y()-3,6,6,Qt::SolidPattern);
//                    //qDebug()<<"Time diff between peaks : "<<currTs-prevTs;
//                  //  qDebug()<<"Vertcial diff between peaks: "<<prevPeak-currPeak;
//                  //  painter.drawText(pPrev.x()-5,pPrev.y()+20,QString::number((prevPeak))+","+QString::number(currTs-prevTs));
//                }
//                prevTs=currTs;
//                prevPeak=currPeak;
//            }

            prevDiff=pPrev.y()-pCurr.y();
            pPrev=pCurr;

        }
        pen.setColor(QColor(50,50,50));
        painter.setPen(pen);
        painter.setFont(font);
        painter.fillRect(rect,textBG);
        painter.drawText(rect, Qt::AlignCenter, waveformLabel);

        painter.drawText(timestampRect, Qt::AlignLeft, resultLabel+" : "+QString::number(showTimestamp));

        //Draw the axes
        if(gridOn){

            gridPen.setStyle(Qt::DotLine);

            for(int i=0;i<height();i+=gridSizePx){
                painter.setPen(gridPen);
                left.setY(i);
                right.setY(i);
                painter.drawLine(left, right);
            }

            for(int i=0;i<width();i+=gridSizePx){
                painter.setPen(gridPen);
                top.setX(i);
                bottom.setX(i);
                painter.drawLine(top, bottom);
            }
        }

        //painter.setPen(qRgb(10,210,10));
        //painter.drawPath(pathGRPPG);
        //pen.setWidth(1);
        //painter.drawPixmap(10, 10, pixmap);

        break;
    }
    painter.setRenderHint(QPainter::Antialiasing, false);
    painter.setPen(palette().dark().color());
    painter.setBrush(Qt::NoBrush);
    painter.drawRect(QRect(0, 0, width() - 1, height() - 1));

    painter.restore();



}
