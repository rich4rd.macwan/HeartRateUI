#ifndef RESULTPLOT_H
#define RESULTPLOT_H
#include <QWidget>
#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QWidget>
#include <QTimer>
#include <QTime>
#include <qmath.h>
#include <cv.h>
#include <QReadWriteLock>
#include "rppgsignalvalue.h"
#include "functions.h"
class ResultPlot : public QWidget
{
    Q_OBJECT
public:
    explicit ResultPlot(QWidget *parent = 0);
    ~ResultPlot();
    enum Shape { Line, Points, Polyline, Polygon, Rect, RoundedRect, Ellipse, Arc,
                 Chord, Pie, Path, Text, Pixmap };

    QSize minimumSizeHint() const Q_DECL_OVERRIDE;
    QSize sizeHint() const Q_DECL_OVERRIDE;
    void resizeEvent(QResizeEvent *);
    void setCurvePenColor(QColor);
    void clearTraces();

public slots:
    void setShape(Shape shape);
    void setPen(const QPen &pen);
    void setBrush(const QBrush &brush);
    void setAntialiased(bool antialiased);
    void setTransformed(bool transformed);
    void setGridOn(bool);
    void updateView();
    void setWaveformLabel(QString val);
    void setResultLabel(QString val);
    void setRPPGSignal(std::vector<RPPGSignalValue> rppgSignal);
#ifdef ENABLE_ICA
    void setRPPGSignal(itpp::mat rppgSignal);
#endif
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
    Shape shape;
    QPen pen,curvePen,gridPen,gtPen,axisPen;
    QBrush brush;
    QColor textBG;
    bool antialiased;
    bool transformed;
    QPixmap pixmap;
    QPoint right,left,top,bottom;
    int padding;
    QTimer timer;
    QTime time;
    double yValue,yDisplay,yValueGRPPG,yGRPPGDisplay;
    double prevDiff,currDiff,yPrev;
    int xValue,xValueGRPPG;
    QPainterPath path,pathGRPPG;
    QVector<QPointF> sinePoints;
    double yStep;
    QColor horizontalAxisColor,curveColor,gtColor,dataPointBG;
    int gridSizePx;
    bool gridOn;
    double yMax,yMaxGRPPG,yMinGRPPG,yMin;
    quint64 showTimestamp;
    quint64 prevTimestamp,currentTimestamp;
    QRect rect,meanErrorRect,legendRect,gtRect,valueRect,dataPointRect;
    QFont font,dataPointFont;
    bool showOximeterIBI;
    QString waveformLabel;
    QString resultLabel;
    std::vector<double> waveform;
    std::vector<quint64> timestamps;
    int xInc;
    int waveformIndex;
    std::vector<RPPGSignalValue> RPPGSignal;
    QReadWriteLock lock;
    double RMSD;
    int dataPointGT,dataPointValue;
    bool drawDataPoint;
};

#endif // RESULTPLOT_H
