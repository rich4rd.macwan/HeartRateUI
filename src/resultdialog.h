#ifndef RESULTDIALOG_H
#define RESULTDIALOG_H

#include <QDialog>
#include <QButtonGroup>
#include "resultplot.h"
#include <iostream>
#include <fstream>

namespace Ui {
class ResultDialog;
}

class ResultDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ResultDialog(QWidget *parent = 0);
    ~ResultDialog();
    QString dumpDirectory();
    void setDumpDirectory(QString txt);
    int exec();
    void populateButtons();

    QString currentDumpFile;
public slots:
    void selectDumpDirectory();
    void drawPlot();
signals:
    void sendRPPGSignal1(std::vector<RPPGSignalValue>);
    void sendRPPGSignal2(std::vector<RPPGSignalValue>);
    void sendRPPGSignal3(std::vector<RPPGSignalValue>);
#ifdef ENABLE_ICA
    void sendRPPGSignal1(itpp::mat);
    void sendRPPGSignal2(itpp::mat);
    void sendRPPGSignal3(itpp::mat);
#endif
private:
    Ui::ResultDialog *ui;
    QButtonGroup buttonGroup;
    std::vector<ResultPlot*> plots;
    std::ifstream inFile;
};

#endif // RESULTDIALOG_H
