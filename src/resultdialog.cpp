#include "resultdialog.h"
#include "ui_resultdialog.h"
#include <QFileDialog>
#include <QDebug>

#include "functions.h"
ResultDialog::ResultDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ResultDialog)
{
    ui->setupUi(this);
    connect(ui->btnSelectDumpDiretory,SIGNAL(clicked()),this,SLOT(selectDumpDirectory()));
    ui->gridLayoutButtons->setColumnMinimumWidth(3,40);


    for(int i=0;i<3;i++){
        ResultPlot *plot=new ResultPlot(this);
        if(i==0){
            connect(this,SIGNAL(sendRPPGSignal1(std::vector<RPPGSignalValue>)),plot,SLOT(setRPPGSignal(std::vector<RPPGSignalValue>)));
            connect(this,SIGNAL(sendRPPGSignal1(itpp::mat)),plot,SLOT(setRPPGSignal(itpp::mat)));
        }
        if(i==1){
            connect(this,SIGNAL(sendRPPGSignal2(std::vector<RPPGSignalValue>)),plot,SLOT(setRPPGSignal(std::vector<RPPGSignalValue>)));
            connect(this,SIGNAL(sendRPPGSignal2(itpp::mat)),plot,SLOT(setRPPGSignal(itpp::mat)));
        }
        if(i==2){
            connect(this,SIGNAL(sendRPPGSignal3(std::vector<RPPGSignalValue>)),plot,SLOT(setRPPGSignal(std::vector<RPPGSignalValue>)));
            connect(this,SIGNAL(sendRPPGSignal3(itpp::mat)),plot,SLOT(setRPPGSignal(itpp::mat)));
        }

        plots.push_back(plot);
        plot->setWaveformLabel("Video "+QString::number(i+1));
        ui->resultPlotFrame->layout()->addWidget(plot);
    }

    //ICA test
//    itpp::mat t(1,300),s(3,300),x(3,300);
//    itpp::mat mean_x(3,1),cntrd_x(3,300),cov_cntrd_x(3,3);
//    itpp::mat xtilde(3,300),cov_xtilde(3,3);
//    itpp::mat V(3,3);
//    itpp::vec d(3);


//        for(int j=0;j<300;j++){
//                t(1,j)=j/10;
////                std::cout<<sin(j/10)<<std::endl;
//                s(0,j)=sin(0.4*(j+1));
//                s(1,j)=sin(1.2*(j+1));
//                s(2,j)=2*sin(0.8*(j+1));
//        }



//    for(int j=0;j<300;j++){
//            x(0,j)=1.2*s(0,j) +3.2*s(1,j) +0.7*s(2,j);
//            x(1,j)=0.3*s(0,j) +1.3*s(1,j) +1.8*s(2,j);
//            x(2,j)=1.8*s(0,j) +2*s(1,j) +3*s(2,j);
//        }

//mean_x(0,0)=0;
//mean_x(1,0)=0;
//mean_x(2,0)=0;
//    for (int i=0;i<300;i++){
//        mean_x(0,0)+=x(0,i);
//        mean_x(1,0)+=x(1,i);
//        mean_x(2,0)+=x(2,i);
//    }
//    mean_x=mean_x/300;

//    qDebug()<<"mean_x="<<mean_x(0,0)<<","<<mean_x(1,0)<<","<<mean_x(2,0);
//   for(int j=0;j<300;j++){
//        cntrd_x(0,j)=x(0,j)-mean_x(0,0);
//        cntrd_x(1,j)=x(1,j)-mean_x(1,0);
//        cntrd_x(2,j)=x(2,j)-mean_x(2,0);
//    }
//   //Start from here in the code
//   cov_cntrd_x=cntrd_x*cntrd_x.transpose()/300;
//   qDebug()<<"cov_cntrd_x = ";
//   displayItppMat(cov_cntrd_x);

//   itpp::eig_sym(cov_cntrd_x,d,V);




//   qDebug()<<"V = ";

//   for(int i=0;i<3;i++){
//       for(int j=0;j<3;j++){
//        std::cout<<V(i,j)<<" ";
//       }
//       std::cout<<std::endl;
//   }
////    for(int i=0;i<3;i++){
////        d(i)=d(i)/sqrt(d(i));
////    }
////    qDebug()<<"1/sqrt(d) = ";
////    for(int i=0;i<d.size();i++){
////        std::cout<<d(i)<<" ";
////    }
////    std::cout<<std::endl;
//    itpp::mat D(3,3);
//    for(int i=0;i<3;i++){
//        for(int j=0;j<3;j++){
//            if(i==j)
//                D(i,j)=1/sqrt(d(i));
//            else
//                D(i,j)=0;
//        }
//    }
//    displayItppMat(D);

//    //Whitened x
//    xtilde=V*D*V.transpose()*x;
//    cov_xtilde=xtilde*xtilde.transpose()/300;
//    displayItppMat(cov_xtilde);


//    emit(sendRPPGSignal1(xtilde(0,0,0,300)));
//    emit(sendRPPGSignal2(xtilde(1,1,0,300)));
//    emit(sendRPPGSignal3(xtilde(2,2,0,300)));
}

ResultDialog::~ResultDialog()
{
    delete ui;
}

QString ResultDialog::dumpDirectory()
{
    return ui->txtDumpDirectory->text();
}

void ResultDialog::setDumpDirectory(QString txt)
{
    ui->txtDumpDirectory->setText(txt);
}


int ResultDialog::exec()
{
    if(dumpDirectory()!=""){
        populateButtons();
    }

    return QDialog::exec();
}

void ResultDialog::populateButtons()
{
    QDir dir(dumpDirectory());
    QFileInfoList dumpfiles=dir.entryInfoList(QStringList("*.txt"));

    buttonGroup.buttons().clear();
    //Iterate through the dump files and create one button for each subject(3 videos)
    int i=0;
    QString currentFile="",prevFile="";
    while(i<dumpfiles.size()){
        //qDebug()<<dumpfiles[i].absoluteFilePath();

        //Set the full path of the first file in the triplet e.g. subject11.txt for subject1.
        QString objectName=dumpfiles[i].absoluteFilePath();
        QStringList tokens= objectName.split("/");
        //Set the subject name as the tooltip. e.g. subject1,subject12,etc.
        QString tooltiptext=tokens[tokens.size()-1];


        //Set just the number of the subject as the text of the button
        QString subjectno=tooltiptext.split(".")[0].split("-")[1].remove("subject");


        //Create one button for 3 dumpfiles
        currentFile=subjectno.mid(0,subjectno.length()-1);
        //int buttonNo=subjectno.mid(0,subjectno.length()-1);
        //qDebug()<<currentFile;
        if(currentFile!=prevFile){
            QPushButton * btn=new QPushButton();
            btn->setText(currentFile);
            btn->setObjectName(objectName);
            btn->setToolTip(tooltiptext);

            connect(btn,SIGNAL(clicked()),this,SLOT(drawPlot()));
            buttonGroup.addButton(btn);
            ui->gridLayoutButtons->addWidget(btn);
        }

        ++i;

        prevFile=currentFile;
    }
}

void ResultDialog::drawPlot()
{
    QPushButton* source=(QPushButton*)QObject::sender();
    QString firstFile=source->objectName();
    QString dumpFile;
    for(int i=0;i<3;i++){
        dumpFile=firstFile.mid(0,firstFile.length()-5)+QString::number(i+1)+".txt";
        //If dumpFile exists, draw plot
        inFile.open(dumpFile.toStdString().c_str(),std::ios::in);
        std::string line;
        std::vector<RPPGSignalValue> rppgSignal;
        if(inFile.is_open()){
            while(getline(inFile,line)){
            //Read waveform data in a vector of rppgsignal and send it to resultplot(1,2 or 3)
                RPPGSignalValue rppgVal;
                std::vector<std::string> tokens=split(line,',');
                std::stringstream(tokens[0])>>rppgVal.index;
                std::stringstream(tokens[1])>>rppgVal.value;
                std::stringstream(tokens[2])>>rppgVal.groundtruth;
                if(rppgVal.value!=0 && rppgVal.groundtruth!=0){
                    rppgSignal.push_back(rppgVal);
                }
              // std::cout<<rppgVal.index<<","<<rppgVal.value<<","<<rppgVal.groundtruth<<std::endl;
            }
        }
        inFile.close();
        if(i==0)
        //Send the rppgsignal to the plot
            emit sendRPPGSignal1(rppgSignal);
        if(i==1)
            emit sendRPPGSignal2(rppgSignal);
        if(i==2)
            emit sendRPPGSignal3(rppgSignal);
    }
}

void ResultDialog::selectDumpDirectory()
{
    QString directory= QFileDialog::getExistingDirectory(this, tr("Select dump directory"),
                                                         "/home",
                                                         QFileDialog::ShowDirsOnly
                                                         | QFileDialog::DontResolveSymlinks);
    if(directory!=""){
        ui->txtDumpDirectory->setText(directory);
        populateButtons();
    }
}
