//#include <GL/glew.h>
#include "qglcanvas.h"
#include <QMouseEvent>
#include <QList>
#include <QPoint>
#include <QPointF>
#include <QDebug>
#include <highgui.h>
#include "qvideo.h"
#include "glInfo.h"
#include <QDebug>

QGLCanvas::QGLCanvas(QWidget* parent,QString caption)
    : QGLWidget(parent)
{
    //QMessageBox::information(this,"Instantiating QGLCanvas",caption,QMessageBox::Ok);
    imageFormat=QImage::Format_RGB888;
    this->name=caption;
    WIDTH=384; HEIGHT=288;

    DATA_SIZE = WIDTH * HEIGHT * 3;
    //    original=cvCreateImage(cvSize(WIDTH,HEIGHT),IPL_DEPTH_8U,3);
    //    cvZero(original);
    cv::Mat zeros=cv::Mat(WIDTH,HEIGHT,CV_8UC3);
    zeros.setTo(0);
    original=zeros;
    backup=zeros;
    switch(original.nChannels) {
    case 1:
        format = GL_LUMINANCE;
        break;
    case 2:
        format = GL_LUMINANCE_ALPHA;
        break;
    case 3:
        format = GL_BGR;
        break;
    default:
        return;
    }


    drawing=false;
    setMouseTracking(true);
    mouseX=0;mouseY=0;
    startX=0; endX=0;
    startY=0; endY=0;


    bg.load(":/images/play.png");
    bgGL=convertToGLFormat(bg);


    makeCurrent();
    //	GLenum result=glewInit();
    //	if(result){
    //		qDebug()<<(const char*)(glewGetErrorString(result));
    //	}
    //qDebug()<<"Open GL Version: "<<(const char*)glGetString(GL_VERSION);
    bgColor=QColor::fromRgb(100,100,100);
    initializeGL();
    qglClearColor(bgColor);
    glInfo glInfo;
    glInfo.getInfo();

    //#ifdef _WIN32
    //	// check PBO is supported by your video card

    //	if(glInfo.isExtensionSupported("GL_ARB_pixel_buffer_object"))
    //	{
    //		// get pointers to GL functions
    //		glGenBuffersARB = (PFNGLGENBUFFERSARBPROC)wglGetProcAddress("glGenBuffersARB");
    //		glBindBufferARB = (PFNGLBINDBUFFERARBPROC)wglGetProcAddress("glBindBufferARB");
    //		glBufferDataARB = (PFNGLBUFFERDATAARBPROC)wglGetProcAddress("glBufferDataARB");
    //		glBufferSubDataARB = (PFNGLBUFFERSUBDATAARBPROC)wglGetProcAddress("glBufferSubDataARB");
    //		glDeleteBuffersARB = (PFNGLDELETEBUFFERSARBPROC)wglGetProcAddress("glDeleteBuffersARB");
    //		glGetBufferParameterivARB = (PFNGLGETBUFFERPARAMETERIVARBPROC)wglGetProcAddress("glGetBufferParameterivARB");
    //		glMapBufferARB = (PFNGLMAPBUFFERARBPROC)wglGetProcAddress("glMapBufferARB");
    //		glUnmapBufferARB = (PFNGLUNMAPBUFFERARBPROC)wglGetProcAddress("glUnmapBufferARB");

    //		// check once again PBO extension
    //		if(glGenBuffersARB && glBindBufferARB && glBufferDataARB && glBufferSubDataARB &&
    //			glMapBufferARB && glUnmapBufferARB && glDeleteBuffersARB && glGetBufferParameterivARB)
    //		{
    //			pboSupported = true;
    //			cout << "Video card supports GL_ARB_pixel_buffer_object." << endl;
    //		}
    //		else
    //		{
    //			pboSupported = false;
    //		//	cout << "Video card does NOT support GL_ARB_pixel_buffer_object." << endl;
    //		}
    //	}

    //#else // for linux, do not need to get function pointers, it is up-to-date
    //	if(glInfo.isExtensionSupported("GL_ARB_pixel_buffer_object"))
    //	{
    //        pboSupported = true;
    //        qDebug() << "Video card supports GL_ARB_pixel_buffer_object." << endl;
    //	}
    //	else
    //	{
    //        pboSupported =  false;
    //        qDebug()<< "Video card does NOT support GL_ARB_pixel_buffer_object." << endl;
    //	}
    //#endif

    //	if(pboSupported){
    //		glGenBuffersARB(2, pboIds);
    //		glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, pboIds[0]);
    //		glBufferDataARB(GL_PIXEL_UNPACK_BUFFER_ARB, DATA_SIZE, 0, GL_STREAM_DRAW_ARB);
    //		glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, pboIds[1]);
    //		glBufferDataARB(GL_PIXEL_UNPACK_BUFFER_ARB, DATA_SIZE, 0, GL_STREAM_DRAW_ARB);
    //		glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
    //		//Note: pboMode=2 somehow does not work while calibration. Fix this later.
    //        pboMode=2;

    //	}
    //	else{
    //        pboMode=0;
    //	}
    pboMode=0;
    //qDebug()<<"PBO Mode = "<<pboMode;
    paintFlag=false;
    //QMessageBox::information(this,"Instantiated QGLCanvas",caption,QMessageBox::Ok);

}

void QGLCanvas::setImage(IplImage& image){

    //Experimental start
    //if(QString(this->name)=="Shadow Mask" ){

    //	//Mouse square
    //	cvRectangle(&image,cvPoint(startX,startY),cvPoint(startX+100,startY+100),cvScalarAll(0xee));
    //	int cogAx = startX+25; int cogAy=startY+25;
    //	//Static square
    //	cvRectangle(&image, cvPoint(150,50),cvPoint(250,150),cvScalarAll(0xa5));
    //	int cogBx=200; int cogBy=100;

    //	cvLine(&image,cvPoint(cogAx,cogAy),cvPoint(cogBx,cogBy),cvScalarAll(0x55));
    //	//find equation of CaCb. The line joining the centroids
    //	double m=double(cogBy-cogAy) / (cogBx-cogAx);
    //	double c=cogAy- m * cogAx;
    //	//Now find the intersection of this line with the blobs. 8 edges to check


    //Experimental end



    //cvShowImage(name,&image);
    //	Display a rectangle between startX ,startY and endX,endY if we are in calibration mode
    //and drawing flag is set.(typically, by a mouse click)

    //Only draw on the video widget with the name "Final"


    {
        //qDebug()<<name<<":Saving original image";
        //	ExclusiveLock xlock(globalXMutex);
        original=image;
        if(drawing){

            original=*(cvCloneImage(&backup));
                cvRectangle(&original,p1,p2,cvScalarAll(200),2);

        }

        paintFlag=true;
    }

    updateGL();


    /*if(this->name=="Final"){
    cvShowImage("Final",original);
    cvWaitKey(1);
    }*/
}

//void QGLCanvas::setImage(cv::Mat image)
//{
//    this->original=image;
//    paintFlag=true;
//    updateGL();
//}

//void QGLCanvas::paintEvent(QPaintEvent*)
//{
//    QPainter p(this);
//
//    //Set the painter to use a smooth scaling algorithm.
//    p.setRenderHint(QPainter::SmoothPixmapTransform, 1);
//
//    p.drawImage(this->rect(), img);
//}

void QGLCanvas::initializeGL(){

    glDisable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glClearColor(100, 100, 100, 0);                   // background color
    glClearStencil(0);                          // clear stencil buffer
    glClearDepth(1.0f);                         // 0 is near, 1 is far
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_TEXTURE_2D);
    glGenTextures(1,&texture);

    glBindTexture(GL_TEXTURE_2D,texture);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glBindTexture(GL_TEXTURE_2D,texture);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,WIDTH,HEIGHT,0,GL_BGR,GL_UNSIGNED_BYTE,NULL);
    glBindTexture(GL_TEXTURE_2D, 0);

    //Create texture for bg logo when video is not playing/visible
    glGenTextures(1,&bglogoTexture);
    glBindTexture(GL_TEXTURE_2D,bglogoTexture);
    glTexImage2D(GL_TEXTURE_2D,0,4,bg.width(),bg.height(),0,GL_BGRA,GL_UNSIGNED_BYTE,bgGL.bits());
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glDisable(GL_TEXTURE_2D);


    glClearStencil(0);                          // clear stencil buffer
    glClearDepth(1.0f);                         // 0 is near, 1 is far
    glDepthFunc(GL_LEQUAL);
    setAutoBufferSwap(false);
}


void QGLCanvas::resizeGL(int width,int height){

    if (height==0)										// Prevent A Divide By Zero By
    {
        height=1;										// Making Height Equal One
    }

    glViewport(0,0,WIDTH,HEIGHT);						// Reset The Current Viewport
    glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
    glLoadIdentity();									// Reset The Projection Matrix


    glOrtho(0.0f,WIDTH,HEIGHT,0.0f,0.0f,1.0f);
    glEnable(GL_TEXTURE_2D);


    glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
    glLoadIdentity();									// Reset The Modelview Matrix

}

void QGLCanvas::paintGL(){
    //qDebug()<<name<<":PaintGL start";

    static int index = 0;
    int nextIndex = 0;                  // pbo index used for next frame
    if(paintFlag){
        //  qDebug()<<"PBO Mode : "<<pboMode;
        //		if(pboMode > 0)
        //		{
        //			// "index" is used to copy pixels from a PBO to a texture object
        //			// "nextIndex" is used to update pixels in a PBO
        //			if(pboMode == 1)
        //			{
        //				// In single PBO mode, the index and nextIndex are set to 0
        //				index = nextIndex = 0;
        //			}
        //			else if(pboMode == 2)
        //			{
        //				// In dual PBO mode, increment current index first then get the next index
        //				index = (index + 1) % 2;
        //				nextIndex = (index + 1) % 2;
        //			}

        //			// start to copy from PBO to texture object ///////

        //			// bind the texture and PBO
        //			glBindTexture(GL_TEXTURE_2D, texture);
        //			glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, pboIds[index]);

        //			// copy pixels from PBO to texture object
        //			// Use offset instead of ponter.
        //			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, WIDTH, HEIGHT, GL_BGR, GL_UNSIGNED_BYTE, 0);

        //			// measure the time copying data from PBO to texture object
        //			//t1.stop();
        //			//copyTime = t1.getElapsedTimeInMilliSec();
        //			///////////////////////////////////////////////////


        //			// start to modify pixel values ///////////////////
        //			//       t1.start();

        //			// bind PBO to update pixel values
        //			glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, pboIds[nextIndex]);

        //			// map the buffer object into client's memory
        //			// Note that glMapBufferARB() causes sync issue.
        //			// If GPU is working with this buffer, glMapBufferARB() will wait(stall)
        //			// for GPU to finish its job. To avoid waiting (stall), you can call
        //			// first glBufferDataARB() with NULL pointer before glMapBufferARB().
        //			// If you do that, the previous data in PBO will be discarded and
        //			// glMapBufferARB() returns a new allocated pointer immediately
        //			// even if GPU is still working with the previous data.
        //			glBufferDataARB(GL_PIXEL_UNPACK_BUFFER_ARB, DATA_SIZE, 0, GL_STREAM_DRAW_ARB);
        //			GLubyte* ptr = (GLubyte*)glMapBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, GL_WRITE_ONLY_ARB);
        //			if(ptr)
        //			{
        //				// update data directly on the mapped buffer
        //				//updatePixels(ptr, DATA_SIZE);
        //				memcpy(ptr,imageData,DATA_SIZE);
        //				glUnmapBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB); // release pointer to mapping buffer
        //				ptr=NULL;
        //			}

        //			// measure the time modifying the mapped buffer
        //			//t1.stop();
        //			//updateTime = t1.getElapsedTimeInMilliSec();
        //			///////////////////////////////////////////////////

        //			// it is good idea to release PBOs with ID 0 after use.
        //			// Once bound with 0, all pixel operations behave normal ways.
        //			glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
        //		}
        //		else
        //		{
        ///////////////////////////////////////////////////
        // start to copy pixels from system memory to textrure object
        //t1.start();

        glBindTexture(GL_TEXTURE_2D, texture);

        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, WIDTH,HEIGHT, GL_BGR, GL_UNSIGNED_BYTE, (GLvoid*)original.imageData);

        //t1.stop();
        //copyTime = t1.getElapsedTimeInMilliSec();


        //}

        // clear buffer
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glBegin(GL_QUADS);
        glTexCoord2i(0,1); glVertex2i(0,HEIGHT);
        glTexCoord2i(0,0); glVertex2i(0,0);
        glTexCoord2i(1,0); glVertex2i(WIDTH,0);
        glTexCoord2i(1,1); glVertex2i(WIDTH,HEIGHT);
        glEnd();
        //glFlush();
        glBindTexture(GL_TEXTURE_2D, 0);
        swapBuffers();

        //qDebug()<<name<<":PaintGL end PBO";
        paintFlag=false;
    }
    else{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        //Transparent bg
        glEnable(GL_BLEND);
        glBindTexture(GL_TEXTURE_2D,texture);
        glBegin(GL_QUADS);
        glTexCoord2i(0,1); glVertex2i(0,HEIGHT);
        glTexCoord2i(0,0); glVertex2i(0,0);
        glTexCoord2i(1,0); glVertex2i(WIDTH,0);
        glTexCoord2i(1,1); glVertex2i(WIDTH,HEIGHT);

        glEnd();
        swapBuffers();
        //qDebug()<<name<<":PaintGL end non PBO";
    }



}
void QGLCanvas::mousePressEvent(QMouseEvent *qevent){

    if(roiselect && qevent->button()== Qt::LeftButton){
        //Start Drawing shape

        drawing=true;
        startX=qevent->x();
        startY=qevent->y();
        p1.x=startX;
        p1.y=startY;

        qDebug()<<"QGLCanvas::Mouse Click at "<<p1.x<<","<<p1.y;
        QPoint* point=new QPoint(startX,startY);
        bool add=true;
        //     qDebug()<<"Location:"<<startX<<","<<startY;
        //			for(int i=1;i<points.count();i++){
        //				//Add a new point only if it is outside a square of 5x5 pixels from a previous point
        //				if(abs(point->x()-points[i]->x())>5 || abs(point->y()-points[i]->y())>5){
        //					add=true;
        //				}
        //				else add=false;
        //			}
        //			if(add) points.append(point);

        endX=startX; endY=startY;


    }
    /*if(calibrating){
        int x=qevent->x(); int y=qevent->y();
        if( !calib_image)
        return;
        if( calib_image->origin )
        y = calib_image->height - y;

        if( select_object )
        {
        selection.x = MIN(x,origin.x);
        selection.y = MIN(y,origin.y);
        selection.width = selection.x + CV_IABS(x - origin.x);
        selection.height = selection.y + CV_IABS(y - origin.y);

        selection.x = MAX( selection.x, 0 );
        selection.y = MAX( selection.y, 0 );
        selection.width = MIN( selection.width, calib_image->width );
        selection.height = MIN( selection.height, calib_image->height );
        selection.width -= selection.x;
        selection.height -= selection.y;
        }
        origin = cvPoint(x,y);
        selection = cvRect(x,y,0,0);
        select_object = 1;
        }*/



}

void QGLCanvas::mouseMoveEvent(QMouseEvent *qevent){
    /*if(QString(this->name)=="Shadow Mask" ){
        startX=qevent->x();
        startY=qevent->y();
    }*/
    if(roiselect ){
    if(drawing){

        endX=qevent->x();
        endY=qevent->y();
        //select_object=0;
        if(endX>WIDTH) endX=WIDTH;
        if(endX<0) endX=0;
        if(endY<0) endY=0;

    }
    mouseX=qevent->x(); mouseY=qevent->y();
    p2.x=endX;
    p2.y=endY;
    }
//    //Mouse moving left to right
//    if(endX>=startX){
//        //roiRect.width=endX-startX;
//        p2.
//    }
//    else{
//        //Mouse moving right to left
//        //Check for boundary condition
//        roiRect.width=startX-endX;
//        roiRect.x=endX;

//    }
//    //Same logic as above for y axis
//    if(endY>=startY){
//        roiRect.height=endY-startY;
//    }
//    else{
//        roiRect.height=startY-endY;
//        roiRect.y=endY;
//    }
    /*if(calibrating){
    if( !calib_image)
    return;
    if( calib_image->origin )
    y = calselect_objectib_image->height - y;

    if( select_object )
    {
    selection.x = MIN(x,origin.x);
    selection.y = MIN(y,origin.y);
    selection.width = selection.x + CV_IABS(x - origin.x);
    selection.height = selection.y + CV_IABS(y - origin.y);

    selection.x = MAX( selection.x, 0 );
    selection.y = MAX( selection.y, 0 );
    selection.width = MIN( selection.width, calib_image->width );
    selection.height = MIN( selection.height, calib_image->height );
    selection.width -= selection.x;
    selection.height -= selection.y;
    }
    }*/
}

void QGLCanvas::mouseReleaseEvent(QMouseEvent *qevent){
    if(roiselect){
  p2.x=qevent->x(); p2.y=qevent->y();
 // qDebug()<<"P1[ "<<p1.x<<","<<p1.y<<" ] == P2[ "<<p2.x<<","<<p2.y<<" ]";
    if(qevent->button()== Qt::LeftButton && drawing){

        //If the rectangle is big enough, set the ROI
        if(abs(p2.x-p1.x)>10 && abs(p2.y-p1.y)>10){
            roiRect.x=MIN(p1.x,p2.x);
            roiRect.y=MIN(p1.y,p2.y);
            roiRect.width=abs(p2.x-p1.x);
            roiRect.height=abs(p2.y-p1.y);
            drawing=false;
            roiselect=false;
            emit finishManualROISelection(roiRect);
        }
    }
        /*if(calibrating || addingRule){
            endX=qevent->x();
            endY=qevent->y();

            if(points.count()==2){
                double speedLimit=qtTestInstance->sldSpeed->value();
                if(calibrating){
                    selection.x=MIN(points[0]->x(),points[1]->x());
                    selection.y=MIN(points[0]->y(),points[1]->y());
                    selection.width=abs(points[1]->x()-points[0]->x());
                    selection.height=abs(points[1]->y()-points[0]->y());
                    select_object=1;
                }
                else if(currentShape==RULE_SHAPE_RECT){
                    currentRule=new RectangleRule(points[0]->x(),points[0]->y(),points[1]->x(),points[1]->y(),currentTarget,currentZoneType,speedLimit,qtTestInstance->sldSpeed->isMax());
                    //rulesList.append(currentRule);
                    emit ruleAdded(currentRule);
                    if(currentZoneType!=RULE_ZONE_TYPE_ENTRY_EXIT)//More processing required to complete rule creation
                        qtTestInstance->endAddRule();
                    else{
                        createEntryExitRule();
                    }
                }
                else if(currentShape==RULE_SHAPE_TRIPLINE){
                    currentRule=new SingleTripline(points,currentTarget,currentZoneType,speedLimit,qtTestInstance->sldSpeed->isMax());
                    //rulesList.append(currentRule);
                    emit ruleAdded(currentRule);
                    if(currentZoneType!=RULE_ZONE_TYPE_ENTRY_EXIT)
                        qtTestInstance->endAddRule();
                }
                //If shape is rectangle or tripline, stop drawing after 2 points
                if(currentShape!=RULE_SHAPE_POLY){
                    drawing=false;
                    points.clear();
                }
            }
        }*/
        /*if(calibrating){
        int x=qevent->x(); int y=qevent->y();
        if( !calib_image)
        return;

        if( calib_image->origin )
        y = calib_image->height - y;

        if( select_object )
        {
        selection.x = MIN(x,origin.x);
        selection.y = MIN(y,origin.y);
        selection.width = selection.x + CV_IABS(x - origin.x);
        selection.height = selection.y + CV_IABS(y - origin.y);

        selection.x = MAX( selection.x, 0 );
        selection.y = MAX( selection.y, 0 );
        selection.width = MIN( selection.width, calib_image->width );
        selection.height = MIN( selection.height, calib_image->height );
        selection.width -= selection.x;
        selection.height -= selection.y;
        }
        select_object = 0;
        }*/

    }
}


void QGLCanvas::mouseDoubleClickEvent(QMouseEvent *qevent){
    /*if(currentShape==RULE_SHAPE_POLY){
        double speedLimit=qtTestInstance->sldSpeed->value();
        //Complete the polygon since the last point intersects the first point.
        drawing=false;
        currentRule=new PolygonRule(points,currentTarget,currentZoneType,speedLimit,qtTestInstance->sldSpeed->isMax());
        //rulesList.append(currentRule);
        emit ruleAdded(currentRule);
        if(currentZoneType!=RULE_ZONE_TYPE_ENTRY_EXIT){
            qtTestInstance->endAddRule();
            points.clear();
        }
        else{
            createEntryExitRule();
        }

    }*/
}





void QGLCanvas::show(){
    setVisible(true);
    paintFlag=false;
}

void QGLCanvas::setSize(int x, int y, int width, int height)
{

    this->WIDTH=width;
    this->HEIGHT=height;

    DATA_SIZE = WIDTH * HEIGHT * 3;
    //cvReleaseImage(&original);
    //    original=cvCreateImage(cvSize(WIDTH,HEIGHT),IPL_DEPTH_8U,3);
    //    cvZero(original);
    //    original.release();
    //    original=cv::Mat(HEIGHT,WIDTH,CV_8UC3);
    //    original.setTo(0);
    cv::Mat zeros=cv::Mat(WIDTH,HEIGHT,CV_8UC3);
    zeros.setTo(0);
    original=zeros;
    backup=zeros;
    switch(original.nChannels) {
    case 1:
        format = GL_LUMINANCE;
        break;
    case 2:
        format = GL_LUMINANCE_ALPHA;
        break;
    case 3:
        format = GL_BGR;
        break;
    default:
        return;
    }

    //Update gl coordinates
    glBindTexture(GL_TEXTURE_2D,texture);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,WIDTH,HEIGHT,0,GL_BGR,GL_UNSIGNED_BYTE,NULL);
    glBindTexture(GL_TEXTURE_2D, 0);
    setGeometry(x,/*this->caption->height()+*/y,this->WIDTH,this->HEIGHT);
    //    glBindTexture(GL_TEXTURE_2D, bglogoTexture);
    //    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, WIDTH,HEIGHT, GL_BGR, GL_UNSIGNED_BYTE, (GLvoid*)bgGL.bits());
    //    glBindTexture(GL_TEXTURE_2D, 0);
}

void QGLCanvas::hide(){
    setVisible(false);
    paintFlag=false;
}


QSize QGLCanvas::minimumSizeHint() const{
    return QSize(WIDTH+5,HEIGHT+5);
}

void QGLCanvas::enableROISelection(bool enable)
{
    roiselect=enable;
    backup=*(cvCloneImage(&original));

}

QSize QGLCanvas::sizeHint() const {
    return QSize(WIDTH,HEIGHT);
}
