
#include <string>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <time.h>

#include "functions.h"
#include "gui.h"

#include "renderthread.h"
#include <QSize>
#include <QDebug>
#include "mainwindow.h"
#include <QApplication>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QResource>

//using namespace std;
using namespace cv;
RNG rng(12345);
long long RenderThread::frameid=0;
RenderThread::RenderThread(QObject *parent)

{

    RPPGMethod=MainWindow::instance->rppgMethod();
    modeIsFaceDetection=MainWindow::instance->modeIsFaceDetection();
    enableRecord=false;
    playing=false;
    videoSourceValid=false;
    videoOrCamera=-1;
    maxWidth=1024;
    maxHeight=768;
    videoMetadataValid=false;
    //Connect signal for sencurrentFrameTimestampding messages first
    connect(this,SIGNAL(message(QString,bool)),MainWindow::instance,SLOT(showStatusMessage(QString,bool)));
    connect(this,SIGNAL(stopped()),MainWindow::instance,SLOT(onStopped()));
    //    connect(&pulseDetector,SIGNAL(setRPPGWaveformY(int)),MainWindow::instance->oximeterWidget->waveformWidget,SLOT(setGRPPGWaveformY(int)),Qt::QueuedConnection);
    //Defaults
    /* Main parameters that can be modified */
    fps = 30; // Supposed frame rate - calculated for live streaming (Hz)
    winLengthSec = 10 ;// sliding window length in seconds
    useWebcam =  0; // if true -> use webcam; false -> a video file
    lastN = 30; // average last N pulses to smooth results
    //vidFile = "C:\\Users\\Richard\\Documents\\video1.mov";
    //vidFile = "../../../my_video-3.mj2";
    videoSource=QString::fromStdString(vidFile);


    nbFrames = fps*winLengthSec;
    faceDetected=false;
    traces.push_back(vector<double>());
    traces.push_back(vector<double>());
    traces.push_back(vector<double>());
    traces[0].reserve(nbFrames);
    traces[1].reserve(nbFrames);
    traces[2].reserve(nbFrames);

    detrended.push_back(vector<double>());
    detrended.push_back(vector<double>());
    detrended.push_back(vector<double>());
    detrended[0].reserve(nbFrames);
    detrended[1].reserve(nbFrames);
    detrended[2].reserve(nbFrames);

    normalized.push_back(vector<double>());
    normalized.push_back(vector<double>());
    normalized.push_back(vector<double>());
    normalized[0].reserve(nbFrames);
    normalized[1].reserve(nbFrames);
    normalized[2].reserve(nbFrames);


    separated.push_back(vector<double>());
    separated.push_back(vector<double>());
    separated.push_back(vector<double>());
    separated[0].reserve(nbFrames);
    separated[1].reserve(nbFrames);
    separated[2].reserve(nbFrames);

    face_cascade_name = ":/data/haarcascade_frontalface_default.xml";
    QDir dir;

    // motion model params
    params.motion_model = CvMotionModel::KALMAN_FILTER;// KALMAN_FILTER or LOW_PASS_FILTER
    params.low_pass_gain = 0.3f;
    // mean shift params
    params.ms_tracker_weight = 0.8f;
    params.ms_params.tracking_type = CvMeanShiftTrackerParams::HS;
    // feature tracking params
    params.ft_tracker_weight = 0.2f;
    params.ft_params.feature_type = CvFeatureTrackerParams::SIFT; // SIFT OPTICAL_FLOW SURF
    params.ft_params.window_size = 0;

    /* Initialize */
    // Init tracker
    tracker=new HybridTracker(params);


    /** Ugly hack: The qt resource system embeds the cascade file(added as a resource here) into the binary
    The opencv cascade load function expects an xml file. Unfortunately, the only workaround here is to write
    the file to a temp directory and use it as a source.
    **/
    // Load the cascades
    QFile file(face_cascade_name);
    QFileInfo fileInfo(file);

    face_cascade_name_absolute=(dir.tempPath()+dir.separator()+fileInfo.fileName()).toStdString();
    //qDebug()<<QString::fromStdString(face_cascade_name_absolute);
    QFile fileAbs(face_cascade_name_absolute.c_str());

    //First see if we have already created the cascade file earlier.
    //qDebug()<<fileAbs.exists();
    if(!fileAbs.exists()){
        qDebug()<<"Cascade file does not exist in temp directory. Create it.";
        //If it doesn't exist yet, we read from the resource and create it in the temp directory
        QFile file(face_cascade_name);
        file.open(QFile::ReadOnly);
        QString xmldata=file.readAll();
        file.close();
        //Now write the xmldata to the tmp folder
        fileAbs.open(QFile::WriteOnly);
        fileAbs.write(xmldata.toStdString().c_str());
        fileAbs.close();
    }



    //qDebug()<<xmldata;

    //qDebug()<<faceCascadeNameAbsolute.toStdString().c_str();

    if( !face_cascade.load( face_cascade_name_absolute) ){
        emit message("Error loading haar cascade",true);

        qDebug()<<"Error loading haar cascade";
    };


    //Need to reinit these every time the video is replayed
    tframe=0;
    idx=0;
    idxFps=1;
    prevTimestamp=0;

    // vcap=NULL;
    resized=NULL;
    video=NULL;
    videoThreadBusy=false;
    // initVideo(videoSource,MainWindow::instance->getVideoWidget());

    displayOval=false;
    switchOvalMovePattern=false;
    for(int i=0;i<4;i++){
        ovalCenters[i]=cv::Point(0,0);
        overlaps[i]=0;
    }
    ovalCenterIndex=0;
    countdownToSwitch=0;

    started=false;
    playTimer=NULL;
    manualROISelectionFrameStart=0;
    manualROISelected=false;
    waitCount=0;
    nSkipFrames=80;
    thread=NULL;

}



RenderThread::~RenderThread()
{
    //qDebug()<<"RenderThread::Deleting thread: "<<currentThreadId();
    if(MainWindow::instance->getMusicPlayerRef()->playing){
        MainWindow::instance->getMusicPlayerRef()->stop();
    }
    videoWriter.close();
}

void RenderThread::run()
{

    int delay=33;
    if(fps!=0)
        delay=1000/fps;
    // lock.lockForRead();
    while(true){

        //if(playing){
        //qDebug()<<"Run begin";
        videoThreadBusy=true;
        clock_t startTime = clock();
        //qDebug()<<"Playing ...";
        //iframe=cvQueryFrame(vcap);
        //if(enableRecord){
        //            playing=vcap.read(frame);
        //        }
        //        else{
        //Get a new frame only if we dont have one yet or if manual ROI mode is deselected
        if(this->playing){
            if(!MainWindow::instance->manualROIMode()  || frameid<manualROISelectionFrameStart+nSkipFrames/20){
                if(!videoWriter.readFrame(frame,currentFrameTimestamp,fps))
                    break;
            }
            if(frameid==manualROISelectionFrameStart+nSkipFrames/20-1){
                //Pause for ROI selection
                //We enable this in the video widget and not as soon as the manual roi select toolbutton is clicked
                //so that we can save a copy of the current frame
                //for better feedback while selecting the roi. See QGLCanvas::enableROISelection
                //This needs to happen only once
                MainWindow::instance->getVideoWidget()->enableROISelection(MainWindow::instance->manualROIMode());
            }
            ++frameid;
        }
        //  myGui.drawText(to_string(double(frameid)),frame);
        // cv::imshow(QString("Frame"+currentThread()->objectName()).toStdString().c_str(),frame);
        //     playing=vcap.read(frame);
        //}
        //If we do not have the custom video file mode, just use the current timestamp to record in the file.
        if(videoWriter.CustomVideoFileMode()==VideoReaderWriter::GENERIC_VIDEO_OR_CAM_MODE){
            currentFrameTimestamp=QDateTime::currentMSecsSinceEpoch();
        }
        //updateCurrentFrameTimestamp();
        //  lock.unlock();
        if(this->playing && frame.data){
            resize(frame,frame,cvSize(resized->width,resized->height));

            if(!MainWindow::instance->manualROIMode()){


                //Signal that a new frame has arrived. We will use this as a timestamp for the oximeter
                //Only emit when reading from camera in live mode
                if(videoOrCamera==1 || videoWriter.CustomVideoFileMode()==VideoReaderWriter::GENERIC_VIDEO_OR_CAM_MODE){
                    emit newFrameArrived(frameid);
                }

                if(useWebcam){
                    flip(frame, frame, 1); //Flip the mirror to get a mirror, easier to face the camera
                }

                // The face has not been detected yet. We detect face and initialize the tracker

                if(modeIsFaceDetection){
                    if(!faceDetected){
                        // Detect the face
                        vector<Rect> faces;
                        Mat frame_gray;
                        cvtColor( frame, frame_gray, CV_BGR2GRAY );
                        equalizeHist( frame_gray, frame_gray );
                        face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(100, 100) );

                        if(faces.size() > 0) {
                            faceDetected = true;
                            faceRect = faces[0];
                            //Initialize the tracker
                            tracker->newTracker(frame, faceRect);
                        }
                    }
                    else{
                        //Update the face position

                        Mat frameCopy = frame.clone(); // have to do the copy because this function modify frame
                        tracker->updateTracker(frameCopy);
                        faceRect = tracker->getTrackingWindow();

                        if(faceRect.x < 0 || faceRect.y < 0 || faceRect.x > frame.rows || faceRect.y > frame.cols) {
                            faceRect = Rect(frame.cols/3, frame.rows/3, frame.cols/3, frame.rows/3); // -> can be useful to debug
                            //return 0;
                        }

                        if(useWebcam && idxFps==1)
                            tframe = clock();

                    }
                }


                if(enableRecord){
                    //Save frame to disk
                    videoWriter.writeFrame(frame,frameid,QDateTime::currentMSecsSinceEpoch());
                    cv::rectangle(frame, faceRect, Scalar(0, 255, 0), 2, 8, 0);
                }
                else{
                    //Skip the first few frames for consistency
                    //Don't skip them if manualROI selection is done because it doesnt work right now.
                    //TODO: fix it
                    //                    if(!manualROISelected && frameid<nSkipFrames){
                    //                        if(frameid%20==0){
                    //                            emit message("Skipping "+QString::number(nSkipFrames-frameid)+" more frames",false);
                    //                        }
                    //                        if(nSkipFrames-frameid==1){
                    //                            emit message("Extracting RPPG signal",false);
                    //                        }
                    //                        *resized=frame;
                    //                        emit sendImage(*resized);
                    //                        videoThreadBusy=false;
                    //                        continue;
                    //                    }

                    //Skin detection test
                    if(!modeIsFaceDetection){
                        detectSkin(frame);
                    }


                    // faceDetected=true;
                    //Extract heartrate here
                    if(faceDetected){
                        //cv::rectangle(frame,faceRect,cv::Scalar(0,200,200),2);
                        ensureFaceRectInFrame(faceRect,frame);
                        Mat faceROI = frame(faceRect); // define face ROI with faceRect
                        int filterSize = 3;
                        //Use different rppg methods here


                        if(RPPGMethod==BASIC || RPPGMethod==ICA_RGB){
                            bool isSimple=false;
                            /* Get one RGB value from the face ROI */
                            Scalar rgbPixel = getMoyPixels(faceROI,maskROI,isSimple);


                            // smooth the trace

                            if(traces[0].size() >= filterSize) {
                                for (int chn=0; chn < 3; chn++) {
                                    double val = rgbPixel(chn);
                                    for (int i=0; i < filterSize-1; i++) {
                                        val += traces[chn][traces[chn].size()-i-1];
                                    }
                                    rgbPixel(chn) = val/filterSize;
                                }
                            }
                            // qDebug()<<"rgbPixel=[ "<<rgbPixel(0)<<","<<rgbPixel(1)<<","<<rgbPixel(2)<<" ]";
                            for(int x=0;x<traces.size();x++){
                                traces[x].push_back(rgbPixel(x));
                                normalized[x].push_back(rgbPixel(x));
                                detrended[x].push_back(rgbPixel(x));
                                if(RPPGMethod==ICA_RGB){
                                    separated[x].push_back(rgbPixel(x));
                                }
                            }

                            // cout << rgbPixel(0) <<  " " << rgbPixel(1) << " " << rgbPixel(2) << endl;
                            // Do the sliding window stuff
                            if(traces[0].size() > nbFrames)  {
                                for(int x=0;x<traces.size();x++){
                                    traces[x].erase(traces[x].begin());
                                    detrended[x].erase(detrended[x].begin());
                                    normalized[x].erase(normalized[x].begin());
                                    if(RPPGMethod==ICA_RGB){
                                        separated[x].erase(separated[x].begin());
                                    }
                                }
                                peakFlags.erase(peakFlags.begin());
                            }

                        }

                        if(RPPGMethod==CHROM ){

                            //TODO:
                            //1. Track pixels of face ROI
                            //2. Generate differences between prev and current frame's tracked pixels
                            //3. Obtain X and Y from these RGB differences
                            //4. Using a temporal sliding window, calculate Pulse trace

                            //Simple CHROM implementation
                            //We just derive X and Y channels from the formula. Although the paper doesn't really use averaging, we first test this simple method

                            //X=3R-2G and Y=1.5R+G-1.5B

                            /* Get one RGB value from the face ROI */
                            Scalar xyPixel = getCHROMPixels(faceROI,maskROI);

                            // smooth the trace
                            if(traces[0].size() >= filterSize) {
                                for (int chn=0; chn < 2; chn++) {
                                    double val = xyPixel(chn);
                                    for (int i=0; i < filterSize-1; i++) {
                                        val += traces[chn][traces[chn].size()-i-1];
                                    }
                                    xyPixel(chn) = val/filterSize;
                                }
                            }

                            for(int x=0;x<traces.size();x++){
                                traces[x].push_back(xyPixel(x));
                                normalized[x].push_back(xyPixel(x));
                                detrended[x].push_back(xyPixel(x));

                            }

                            // cout << rgbPixel(0) <<  " " << rgbPixel(1) << " " << rgbPixel(2) << endl;
                            // Do the sliding window stuff
                            if(traces[0].size() > nbFrames)  {
                                for(int x=0;x<traces.size();x++){
                                    traces[x].erase(traces[x].begin());
                                    detrended[x].erase(detrended[x].begin());
                                    normalized[x].erase(normalized[x].begin());
                                    if(RPPGMethod==ICA_RGB){
                                        separated[x].erase(separated[x].begin());
                                    }
                                }
                                peakFlags.erase(peakFlags.begin());
                            }

                        }
                        /* Get pulse from rgb trace */

                        // YB TODO: getPulseFromTrace return double or float ?
                        //tic();

                        pulseDetector.updatePulseData(traces,normalized,detrended,separated,30,currentFrameTimestamp,peakFlags,RPPGMethod);
                        int crtPulse = pulseDetector.getCurrentPulse();
                        //qDebug()<<"Pulse detection end";
                        //emit message(QString("Time take to extract pulse(us) :")+QString::number(toc()),false);
                        rppgSignals.clear();

                        if(RPPGMethod==ICA_RGB){
                            int n=separated[0].size();
                            rppgSignals.push_back(separated[0][n-1]);
                            rppgSignals.push_back(separated[1][n-1]);
                            rppgSignals.push_back(separated[2][n-1]);
                        }
                        else{
                            int n=detrended[0].size();
                            rppgSignals.push_back(detrended[0][n-1]);
                            rppgSignals.push_back(detrended[1][n-1]);
                            rppgSignals.push_back(detrended[2][n-1]);
//                            rppgSignals.push_back(normalized[0][n-1]);
//                            rppgSignals.push_back(normalized[1][n-1]);
//                            rppgSignals.push_back(normalized[2][n-1]);
                        }



                        //Check to skip extremely high or low values
//                        if(traces[0].size()==nbFrames){
                        if(frameid>=50){
                            emit sendRPPGSignal(rppgSignals);

                            //qDebug()<<"rppgSignal=[ "<<rppgSignal[0]<<","<<rppgSignal[1]<<","<<rppgSignal[2]<<" ]";
                            //For embedded mode test, write frameid, currentFrameTimestamp and calculated rppg signal value to file
                            if(MainWindow::instance->isEmbeddedTestEnabled()){
                                //We store the rppg signal value for each channel
                                videoWriter.writeFrameDataForEmbedded(frameid,currentFrameTimestamp,rppgSignals);
                            }


                            //Append to the pulse sliding window
                            pulses.push_back((double)crtPulse);

                            // Do not keep more than nbFrames pulse
                            if(pulses.size() > nbFrames)  {
                                pulses.erase(pulses.begin());
                            }

                            // Smooth pulse
                            if(pulses.size() > lastN) {
                                crtPulse = smoothPulse(pulses, lastN);
                            }

                            if(RPPGMethod==ICA_RGB){
                                /* GUI - Show results */
                                myGui.addInfoOnFrame(frame, faceRect, crtPulse, normalized,separated,peakFlags,RPPGMethod);
                            }
                            else{
                                myGui.addInfoOnFrame(frame, faceRect, crtPulse, normalized,detrended,peakFlags,RPPGMethod);
                            }
                            if(MainWindow::instance->isEvaluationEnabled()){
                                videoWriter.writeFrameDataForEvaluation(currentFrameTimestamp,rppgSignals);
                            }
                        }
                    }
                    rppgSignals.push_back(0);rppgSignals.push_back(0);rppgSignals.push_back(0);
                    if(MainWindow::instance->isEvaluationEnabled()){
                        videoWriter.writeFrameDataForEvaluation(currentFrameTimestamp,rppgSignals);
                    }
                    idx++;

                }

            }

            if(displayOval)
                moveFaceGuides(frame);

            // compute fps every 100 frames
            delay=2;
            if (useWebcam)
            {
                idxFps++;
                tframe = clock() - tframe;
                float cumulated_time = (((float)tframe) / CLOCKS_PER_SEC);
                if(idxFps == 100) {

                    std::cout << cumulated_time ;
                    fps = (1 / (cumulated_time));
                    std::cout << ", fps : " << fps << endl;
                    idxFps =  1;
                    videoWriter.setFps(fps);
                    //qDebug()<<"Calculated FPS: "<<fps;
                }
                tframe = clock() ;
                //We set the delay to the next frame as much as to obtain frames at 30 fps
                //qDebug()<<"Renderthread::run() delay="<<cumulated_time*1000;
                if(cumulated_time*1000<33){
                    delay=33-cumulated_time;
                }
            }
        }
        myGui.drawText(to_string(double(int(fps)))+" fps",frame);

        if(resized==NULL || resized->imageData==NULL){
            break;
        }
        *resized=frame;
        emit sendImage(*resized);
        videoThreadBusy=false;

        delay=33;
        if(prevTimestamp==0||videoWriter.CustomVideoFileMode()==VideoReaderWriter::GENERIC_VIDEO_OR_CAM_MODE){
            //cv::imshow("Frame",frame);

            thread->msleep(delay);
        }
        else{
            delay=((currentFrameTimestamp-prevTimestamp)==0)?delay:currentFrameTimestamp-prevTimestamp;
            // delay=100;
            thread->msleep(delay);
        }

        //Update prevTimestamp only if we are playing the video
        //qDebug()<<"RenderThread: currentFrameTimestamp="<<currentFrameTimestamp;
        if(!enableRecord && videoOrCamera==0)
            prevTimestamp=currentFrameTimestamp;

        //   qDebug()<<"Run end. videoThreadBusy="<<videoThreadBusy<<",playing="<<playing;
        //   qDebug()<<"WaitCount="<<waitCount;
    }



    videoThreadBusy=false;
    emit stopped();
    emit finished();
    pulseDetector.stopPulseCalculationTimer();
}

void RenderThread::onPlayTimerElapsed()
{
    playTimer->stop();
    qDebug()<<"RenderThread::onPlayTimerElapsed() ";

    this->stop();
}

void RenderThread::stopSignalReceived()
{
    this->stop();
}

void RenderThread::errorString(QString err)
{
    emit message(err,true);
}

void RenderThread::stop()
{
    while(videoThreadBusy){
        waitCount++;
        qDebug()<<"Waiting for video thread...videoThreadBusy="<<videoThreadBusy;
        if(waitCount==10){
            waitCount=0;
            videoThreadBusy=false;
            qDebug()<<"RenderThread::stop() Terminating VideoThread, playing="<<playing;
        }

        thread->wait(5);
    }
    //currentThread()->wait();
    //if(!videoThreadBusy)
    //   lock.lockForWrite();
    {
        waitCount=1000;
        this->playing=false;
        emit finished();
        qDebug()<<"RenderThread Stopping playing,playing="<<playing;
        emit stopped();
    }
    //lock.unlock();
    //if(enableRecord){
    //Finalize video5
    videoWriter.close();
    //}
    frameid=0;
    enableRecord=false;
    pulseDetector.stopPulseCalculationTimer();
}

void RenderThread::pause()
{
    lock.lockForWrite();
    this->playing=false;
    lock.unlock();
    qDebug()<<"Paused playing";
    emit paused();
}

void RenderThread::play()
{
    qDebug()<<"RenderThread: Start Play";
    RPPGMethod=MainWindow::instance->rppgMethod();
    modeIsFaceDetection=MainWindow::instance->modeIsFaceDetection();
    //    while(!lock.tryLockForWrite()){
    //        thread->msleep(50);
    //        qDebug()<<"Waiting to acquire lock on playing variable";
    //        lock.unlock();
    //    }
    playing=true;
    qDebug()<<"Renderthread::play(), playing="<<playing;
    frameid=0;

    //    if(videoOrCamera==0 && videoSourceValid){
    //        if(!vcap.isOpened()){
    //            //    vcap=VideoCapture(videoSource.toStdString());
    //        }
    //    }
    //    if(videoOrCamera==1){
    //        if(!vcap.isOpened()){
    //            vcap=VideoCapture(this->cameraSource);
    //        }
    //    }

    if(!started){
        //TODO: Revert this to the old version without using separate QThread. or fix the auto stop playTimer that does not work
        thread=new QThread;
        this->moveToThread(thread);
        connect(thread,SIGNAL(started()),this,SLOT(run()));
        connect(this,SIGNAL(finished()),thread,SLOT(quit()));
        //  connect(this,SIGNAL(finished()),this,SLOT(deleteLater()));
        connect(thread,SIGNAL(finished()),thread,SLOT(deleteLater()));
        thread->start();
        pulseDetector.startPulseCalculationTimer();

    }

}

void RenderThread::closeVideoFile()
{
    videoWriter.close();
}

void RenderThread::play(int msec)
{
    //Start a timer for the given amount of time and stop after the timeout.
    if(playTimer!=NULL){
        delete playTimer;
    }
    playTimer=new QTimer();
    //playTimer->setSingleShot(true);
    connect(playTimer,SIGNAL(timeout()),this,SLOT(onPlayTimerElapsed()));
    playTimer->start(msec);
    play();
}

bool RenderThread::setRecordMode(bool rec, QString recordFileName)
{

    enableRecord=rec;
    this->recordFileName=recordFileName;
    qDebug()<<"Enable record="<<enableRecord<<", cameraSource="<<cameraSource;
    if(enableRecord){
        //fps=cvGetCaptureProperty(vcap,CV_CAP_PROP_FPS);

        //fps=vcap.get(CV_CAP_PROP_FPS);
        //qDebug()<<"FPS: "<<fps;
        //if(fps<=0) fps=30;
        if(videoWriter.enableRecord(recordFileName)){
            connect(&videoWriter,SIGNAL(message(QString,bool)),MainWindow::instance,SLOT(showStatusMessage(QString,bool)));
            connect(&videoWriter,SIGNAL(endProcessingVideo(QString,bool)),MainWindow::instance,SLOT(showStatusMessage(QString,bool)));

            qDebug()<<"RenderThread::setRecordMode(): Ready to record";
            emit message("Ready to record...",false);
            return true;
        }
        else{
            enableRecord=false;
            qDebug()<<"RenderThread::setRecordMode(): Cannot record";
            emit message("RenderThread::setRecordMode(): Cannot record",true);
            return false;
        }
    }
    else{
        emit message("Disabled recording.",false);
        return false;
    }
}


bool RenderThread::initVideo(QString videoSource, QVideo *video)
{
    this->videoSource=videoSource.trimmed();
    this->video=video;
    //Create the IplImage structure for the resized video

    bool custom=true;
    if(custom){

        videoSourceValid=videoWriter.open(videoSource,VideoReaderWriter::READ_VIDEO);
        //Get these from the videoWriter
        int w= videoWriter.cols();
        int h= videoWriter.rows();
        //fps=videoWriter.fps();
        frame=cv::Mat::zeros(h,w,CV_8UC3);
        mask=cv::Mat::zeros(h,w,CV_8UC1);
        contours=cv::Mat::zeros(h,w,CV_8UC1);
        //If w > 768|| h >488, scale down keeping constant aspect ratio...
        double hscale=1,vscale=1;
        // 720, 720 -- 1:1
        if(w>maxWidth){
            hscale=double(w)/maxWidth;
            //hscale=0.94
        }
        if(h>maxHeight){
            vscale=double(h)/maxHeight;
            //vscale = 1.48
        }
        if(hscale>vscale){
            //Use hscale for scaling both
            w=w/hscale;
            h=h/hscale;
        }
        else if(vscale>hscale){
            //Use vscale for scaling both
            w=w/vscale;
            h=h/vscale;
        }


        resized=cvCreateImage(cvSize(w,h),IPL_DEPTH_8U,3);
        //Initialize manualROI for use with detectSkin later.
        manualROI.x=0;
        manualROI.y=0;
        manualROI.width=w;
        manualROI.height=h;

        video->setVideoSize(w,h);

        qDebug()<<"RenderThread : FPS: "<<fps;
        videoOrCamera=0;

        //Remove this block later
        //Read timestamp data from file and store in videoTimestampData for use later
        QFile file(videoSource.split(".")[0]+"-vid.xmp");
        if(file.open(QIODevice::ReadOnly)){
            videoTimestampData=file.readAll();
            videoTimestampDataStream=new QTextStream(&videoTimestampData);
            videoMetadataValid=true;
        }
        return true;
    }
    else{
        //Validate video source

        //    if(vcap!=NULL){
        //        cvReleaseCapture(&vcap);
        //    }

        vcap=cv::VideoCapture(videoSource.toStdString().c_str());

        if(!vcap.isOpened()){
            qDebug()<<"RenderThread(initVideo): Error opening video";
            emit message("Error opening video",true);
            return false;
        }
        //        else{
        //            videoSourceValid=true;

        //            int w= vcap.get(CV_CAP_PROP_FRAME_WIDTH);//cvGetCaptureProperty(vcap,CV_CAP_PROP_FRAME_WIDTH);
        //            int h= vcap.get(CV_CAP_PROP_FRAME_HEIGHT);//cvGetCaptureProperty(vcap,CV_CAP_PROP_FRAME_HEIGHT);
        //            //If w > 768|| h >488, scale down keeping constant aspect ratio...
        //            double hscale=1,vscale=1;
        //            // 720, 720 -- 1:1
        //            if(w>maxWidth){
        //                hscale=double(w)/maxWidth;
        //                //hscale=0.94
        //            }
        //            if(h>maxHeight){
        //                vscale=double(h)/maxHeight;
        //                //vscale = 1.48
        //            }
        //            if(hscale>vscale){
        //                //Use hscale for scaling both
        //                w=w/hscale;
        //                h=h/hscale;
        //            }
        //            else if(vscale>hscale){
        //                //Use vscale for scaling both
        //                w=w/vscale;
        //                h=h/vscale;
        //            }

        //            //qDebug()<<w<<","<<h;
        //            //        if(resized!=NULL){
        //            //            cvReleaseImage(&resized);
        //            //        }
        //            resized=cvCreateImage(cvSize(w,h),IPL_DEPTH_8U,3);
        //            //resized=Mat(h,w,CV_8UC3);
        //            video->setVideoSize(w,h);
        //            //fps=cvGetCaptureProperty(vcap,CV_CAP_PROP_FPS);
        //            fps=vcap.get(CV_CAP_PROP_FPS);
        //            qDebug()<<"RenderThread : FPS: "<<fps;
        //            videoOrCamera=0;
        //            //Read timestamp data from file and store in videoTimestampData for use later
        //            QFile file(videoSource.split(".")[0]+"-vid.xmp");
        //            if(file.open(QIODevice::ReadOnly)){
        //                videoTimestampData=file.readAll();
        //                videoTimestampDataStream=new QTextStream(&videoTimestampData);
        //                videoMetadataValid=true;
        //            }
        //            return true;
        //        }
    }

}


bool RenderThread::initCamera(int cam, QVideo *video)
{

    this->cameraSource=cam;
    this->video=video;
    //    if(vcap!=NULL){
    //        cvReleaseCapture(&vcap);
    //    }

    //start camera

    //if(!vcap.isOpened()){
    if(!videoWriter.open(recordFileName,VideoReaderWriter::READ_CAM,cam)){
        qDebug()<<"Error opening video";
        emit message("Error opening video",true);
        return false;
    }
    else{
        videoWriter.disconnect();
        connect(&videoWriter,SIGNAL(message(QString,bool)),MainWindow::instance,SLOT(showStatusMessage(QString,bool)));
        videoSourceValid=true;
        int w=videoWriter.cols();// cvGetCaptureProperty(vcap,CV_CAP_PROP_FRAME_WIDTH);
        int h=videoWriter.rows();// cvGetCaptureProperty(vcap,CV_CAP_PROP_FRAME_HEIGHT);
        //If w > 768|| h >488, scale down keeping constant aspect ratio...
        frame=cv::Mat::zeros(h,w,CV_8UC3);
        mask=cv::Mat::zeros(h,w,CV_8UC1);
        contours=cv::Mat::zeros(h,w,CV_8UC1);
        double hscale=1,vscale=1;
        // 720, 720 -- 1:1
        if(w>maxWidth){
            hscale=double(w)/maxWidth;
            //hscale=0.94
        }
        if(h>maxHeight){
            vscale=double(h)/maxHeight;
            //vscale = 1.48
        }
        if(hscale>vscale){
            //Use hscale for scaling both
            w=w/hscale;
            h=h/hscale;
        }
        else if(vscale>hscale){
            //Use vscale for scaling both
            w=w/vscale;
            h=h/vscale;
        }

        //qDebug()<<w<<","<<h;
        if(resized!=NULL){
            cvReleaseImage(&resized);
        }
        resized=cvCreateImage(cvSize(w,h),IPL_DEPTH_8U,3);
        //Initialize manualROI for use with detectSkin later.
        manualROI.x=0;
        manualROI.y=0;
        manualROI.width=w;
        manualROI.height=h;
        video->setVideoSize(w,h);
        fps=videoWriter.fps();//cvGetCaptureProperty(vcap,CV_CAP_PROP_FPS);
        qDebug()<<"RenderThread : FPS: "<<fps;
        videoOrCamera=1;
        useWebcam=1;
        //        cv::Mat temp;
        //        videoWriter.readFrame(temp);
        //        cv::imshow("Temp",temp);
        return true;
    }
}

bool RenderThread::setVideoSource(QString source,QVideo* video)
{
    if(playing){
        this->stop();
    }
    bool result=initVideo(source,video);
    if(result){
        emit message("Video loaded",false);
    }
    else{
        emit message("Error loading video!",true);
    }
    return result;

}

bool RenderThread::setVideoSource(int cameraId, QVideo *video)
{
    if(playing){
        this->stop();
    }
    bool result=initCamera(cameraId,video);
    if(result){
        emit message("Camera initialized",false);
    }
    else{
        emit message("Error initializing camera ",true);
    }
    return result;
}

long long RenderThread::getCurrentFrameId()
{
    return RenderThread::frameid;
}
/** DEPRECATED
 * @brief RenderThread::updateCurrentFrameTimestamp
 * This function updates the current timestamp based on whether the source is a camera or a video file.
 * When it's a video file it reads the timestamp for the current frameid.
 * Otherwise just updates the current timestamp.
 */
void RenderThread::updateCurrentFrameTimestamp()
{

    if(videoOrCamera==0){
        //Read current frame's timestamp from videoTimestampData
        QString line=videoTimestampDataStream->readLine();

        long long fid=line.split(":")[0].toLongLong();
        while(fid<(frameid+1) && line!=""){
            //The actual frameid is ahead of stored. Read lines until we arrive at the correct frameid
            line=videoTimestampDataStream->readLine();
        }
        if(fid==frameid+1){
            //Frameids are in sync
            currentFrameTimestamp=line.split(":")[1].toLongLong();

        }
        //else Do nothing. Don't update the timestamp
        //Maybe some frames were not recorded. Need to wait until we sync

    }
    //This block executes if it is camera mode or we cannot read the metadata file
    if(videoOrCamera==1 || !videoMetadataValid){
        currentFrameTimestamp=QDateTime::currentMSecsSinceEpoch();

    }

}

VideoReaderWriter *RenderThread::videoReaderWriter()
{
    return &videoWriter;
}

/**
 * @brief RenderThread::drawOval
 * @param frame
 * To collect dataset for moving faces. Display an oval in which the user will be asked to place his/her face
 */
void RenderThread::moveFaceGuides(Mat &frame)
{


    //cv::Point ovalCentre=cv::Point(video->width()/2+sin(double(frameid)/20)*80,video->height()/2+sin(double(frameid)/20)*80);

    getNextOvalCenter(frame);
    double dist=sqrt(((faceRect.x+faceRect.width/2) -ovalCenters[ovalCenterIndex].x)*((faceRect.x+faceRect.width/2) -ovalCenters[ovalCenterIndex].x)+((faceRect.y+faceRect.height/2) -ovalCenters[ovalCenterIndex].y)*((faceRect.y+faceRect.height/2) -ovalCenters[ovalCenterIndex].y));
    //qDebug()<<"Distance from oval:"<<dist;

    myGui.drawText("Please align your face with the oval",frame,cv::Point(ovalCenters[ovalCenterIndex].x-100,ovalCenters[ovalCenterIndex].y));
    if(dist>50){
        cv::ellipse(frame,ovalCenters[ovalCenterIndex],cv::Size(100,120),0,0,360,cv::Scalar(255,180,190),2);
    }
    else{
        cv::ellipse(frame,ovalCenters[ovalCenterIndex],cv::Size(100,120),0,0,360,cv::Scalar(122,160,250),3);
    }

}

void RenderThread::getNextOvalCenter(Mat& frame)
{
    //cv::Point ovalCentre,ovalCentreV,ovalCentreH,ovalCentreCC,ovalCentreC;
    //Approx every 10 seconds since fps is about 30
    //   qDebug()<<frameid;
    double dy=sin(double(frameid)/20);
    double dx=cos(double(frameid)/20);
    //CounterClockwise, Clockwise,Horizontal and Vertical in sequence

    ovalCenters[0].x=video->width()/2+2*dy*50;  ovalCenters[0].y=video->height()/2+dx*80;
    ovalCenters[1].x=video->width()/2+2*dx*50;  ovalCenters[1].y=video->height()/2+dy*80;
    ovalCenters[2].x=video->width()/2+2*dy*50;  ovalCenters[2].y=video->height()/2;
    ovalCenters[3].x=video->width()/2;          ovalCenters[3].y=video->height()/2+dy*80;

    int i=0;
    for(;i<4;i++){
        if(checkCollision(ovalCenters[i],ovalCenters[ovalCenterIndex])){
            if(switchOvalMovePattern){
                ovalCenterIndex=i;
                switchOvalMovePattern=false;
                countdownToSwitch=0;
                break;
            }
        }
        // myGui.drawText(QString::number(overlaps[i]).toStdString(),frame,ovalCenters[i]);
    }


    //rectangle(frame,cvPoint(ovalCenters[ovalCenterIndex].x-4,ovalCenters[ovalCenterIndex].y-4),cvPoint(ovalCenters[ovalCenterIndex].x+4,ovalCenters[ovalCenterIndex].y+4),cvScalar(255,0,0));

    //Check which path the currentOvalcenter coincides with and switch to that one
    //    rectangle(frame,cvPoint(ovalCenters[0].x-4,ovalCenters[0].y-4),cvPoint(ovalCenters[0].x+4,ovalCenters[0].y+4),cvScalar(255,0,0));
    //  rectangle(frame,cvPoint(ovalCenters[1].x-4,ovalCenters[1].y-4),cvPoint(ovalCenters[1].x+4,ovalCenters[1].y+4),cvScalar(0,255,0));
    //    rectangle(frame,cvPoint(ovalCenters[2].x-4,ovalCenters[2].y-4),cvPoint(ovalCenters[2].x+4,ovalCenters[2].y+4),cvScalar(255,255,0));
    //    rectangle(frame,cvPoint(ovalCenters[3].x-4,ovalCenters[3].y-4),cvPoint(ovalCenters[3].x+4,ovalCenters[3].y+4),cvScalar(0,0,255));
    //qDebug()<<frameid;
    if(++countdownToSwitch==200){
        qDebug()<<"Swich oval move";
        switchOvalMovePattern=true;
    }

}

QString RenderThread::getRecordFileName()
{
    return recordFileName;
}

void RenderThread::ensureFaceRectInFrame(Rect &faceRect, Mat &frame)
{
    if(faceRect.x<0){
        faceRect.x=0;
    }
    if((faceRect.width+faceRect.x)>frame.cols){
        faceRect.x=faceRect.width+faceRect.x-frame.cols;
    }
    if(faceRect.y<0){
        faceRect.y=0;
    }
    if((faceRect.height+faceRect.y)>frame.rows){
        faceRect.y=faceRect.height+faceRect.y-frame.rows;
    }

}

bool RenderThread::checkCollision(Point &p1, Point &p2)
{
    if(abs(p1.x-p2.x)<5 && abs(p1.y-p2.y)<5){
        if(abs(p1.x-p2.x)==0 && abs(p1.y-p2.y)==0){
            return false;
        }
        else{
            return true;
        }

    }
    else return false;
}

int RenderThread::windowSize()
{
    return nbFrames;
}

void RenderThread::detectSkin(Mat &source)
{
    int r,g,b;
    tic();
    cv::Mat kernel=cv::getStructuringElement(cv::MORPH_ELLIPSE,cv::Size(11,11));
    float r1=0,r2=0,r3=0,r4=0;
    int topLeftX=1000,topLeftY=1000,bottomRightX=0,bottomRightY=0;
    //TODO: For manual ROI, only detect skin within the ROI
    //manualROI has the same dimensions as the image. When it's set manually, the dimensions change.
    //qDebug()<<"ManaulROI=[ "<<manualROI.x<<","<<manualROI.y<<","<<manualROI.width<<","<<manualROI.height<<" ]";
    //qDebug()<<"Frame=[ "<<frame.cols<<","<<frame.rows<<" ]";
    for(int i=manualROI.x;i<manualROI.x+manualROI.width;i++){
        for(int j=manualROI.y;j<manualROI.y+manualROI.height;j++){
            uchar* ptr=source.ptr<uchar>(j,i);
            b=ptr[0];
            g=ptr[1];
            r=ptr[2];
            //Skin detection algo from http://arxiv.org/abs/1212.2692 Enhanced Skin colour classifier using rgb ratio model
            r1=float(r-g)/(r+g);
            r2=float(b)/(r+g);
            r3=float(g-b)/(r+g);
            r4=float(r)/(r+g);
            mask.at<uchar>(j,i)=0;

            //if((r1>0 && r1<0.5 && r2<0.5 && r3<0.08 ) ){
            if(true){
                //  qDebug()<<r4;
                //source.ptr<uchar>(j,i)[0]=0;
                //                source.ptr<uchar>(j,i)[1]=30;
                //                source.ptr<uchar>(j,i)[2]=30;
                //Find intial boundingrect
                if(i<topLeftX)
                    topLeftX=i;
                if(j<topLeftY)
                    topLeftY=j;

                if(i>bottomRightX)
                    bottomRightX=i;
                if(j>bottomRightY)
                    bottomRightY=j;
                mask.at<uchar>(j,i)=255;
            }
            //Clip the boundaries a bit to remove artefacts
            if(i>frame.cols-35 || i<35 ){
                mask.at<uchar>(j,i)=0;
            }
        }
    }

    bool skipRest=true;

    if(skipRest){
        faceDetected=true;
      //  faceRect=manualROI;
      //  frame.setTo(0,mask);
       // return;
    }
    cv::erode(mask,mask,kernel,cv::Point(-1,-1),2);

    long long duration=toc();

    //Calculate refined bounding rect
    int topLeftXX=1000,topLeftYY=1000,bottomRightXX=0,bottomRightYY=0;
    for(int i=topLeftX;i<bottomRightX;i++){
        for(int j=topLeftY;j<bottomRightY;j++){
            if(mask.at<uchar>(j,i)==255){
                if(i<topLeftXX)
                    topLeftXX=i;
                if(j<topLeftYY)
                    topLeftYY=j;

                if(i>bottomRightXX)
                    bottomRightXX=i;
                if(j>bottomRightYY)
                    bottomRightYY=j;
            }
        }
    }
    //qDebug()<<topLeftXX<<topLeftXX<<bottomRightXX<<bottomRightXX;
    //    namedWindow( "Contours", CV_WINDOW_AUTOSIZE );
    //    imshow( "Contours", drawing );
    Rect bb;
    //Assign the bounding box if it is not erroneous,i.e. doesn't have wrong skin pixels like those of the boundary in some videos
    //    qDebug()<<abs(topLeftXX-bottomRightXX)<<","<<abs(topLeftYY-bottomRightYY);
    //    myGui.drawText(to_string(double(abs(topLeftXX-bottomRightXX))),frame);
//    if(abs(topLeftXX-bottomRightXX)<(frame.cols-100) && abs(topLeftYY-bottomRightYY)<(frame.rows-100)
//            && topLeftXX>10 && abs(frame.cols-bottomRightXX)>10){
//        bb.x=topLeftXX;
//        bb.y=topLeftYY;
//        bb.width=bottomRightXX-topLeftXX;
//        bb.height=bottomRightYY-topLeftYY;
//        //myGui.drawText(to_string(double(duration)),source);
//        //        rectangle(source,bb,cv::Scalar(0,200,0),2);
//        faceRect=bb;

//        if(faceRect.x>0 && faceRect.y>0 && faceRect.width>10 && faceRect.height>10){
//            faceDetected=true;
//            maskROI=mask(faceRect);
//        }
//        else
//            faceDetected=false;
//    }
//    else{

//        faceRect=manualROI;
//        //This is to check the RPPG of the wall. Set faceDetected=false to disable.
//        faceDetected=true;
//    }

    faceRect=manualROI;
    maskROI=mask(faceRect);

    // cv::rectangle(mask,faceRect,cvScalar(255,0,255));
   //cv::imshow("Mask",mask);
   //cv::imshow("Frame",frame(faceRect));

}
/**
 * @brief RenderThread::clearTraces
 * Used to clear all the traces. Useful for switching to manualROI mode
 */
void RenderThread::clearTraces()
{
    traces.clear();
    normalized.clear();
    detrended.clear();
    peakFlags.clear();
    separated.clear();

}

void RenderThread::startManualROISelection()
{
    lock.lockForWrite();
    manualROISelectionFrameStart=frameid;
    manualROISelected=true;
    lock.unlock();
}

void RenderThread::setManualROI(Rect roi)
{
    this->manualROI=roi;
    qDebug()<<"ManaulROI=[ "<<manualROI.x<<","<<manualROI.y<<","<<manualROI.width<<","<<manualROI.height;
}

void RenderThread::wait()
{
    thread->wait();
}

int RenderThread::windowLengthInFrames()
{
    return nbFrames;
}

