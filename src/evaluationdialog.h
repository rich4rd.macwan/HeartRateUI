#ifndef EVALUATIONDIALOG_H
#define EVALUATIONDIALOG_H

#include <QDialog>
#include <QFileDialog>
namespace Ui {
class EvaluationDialog;
}

class EvaluationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EvaluationDialog(QWidget *parent = 0);
    ~EvaluationDialog();
    QString videoDirectory();
    QString dumpDirectory();
    bool overwriteDumpFiles();
public slots:
    QString selectDumpDirectory();
    QString selectVideoDirectory();
private:
    Ui::EvaluationDialog *ui;

};

#endif // EVALUATIONDIALOG_H
