#include "bandpassfilter.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "functions.h"
#include "mkfilter.h"
#include <cstdio>
BandPassFilter::BandPassFilter()
{
    order=4;
    s=30;
    f1=0.75;
    f2=4.0;

    BandPassFilter(order,s,f1,f2);

}

BandPassFilter::BandPassFilter(int filterOrder, int samplingFrequency, double lowerCutoff, double higherCutoff)
{
    order=filterOrder;
    if(order % 4){
        std::cout<<"BandPass filter error: Order must be 4,8,12,16,...\n";
        order=4;
    }
    s=samplingFrequency;
    f1=lowerCutoff;
    f2=higherCutoff;
    //Build command line args for mkfilter. Doing this so that we don't have to change a lot of code
    //const char* lfc=//to_string(lowerCutoff/samplingFrequency).c_str();
    //const char* hfc=to_string(higherCutoff/samplingFrequency).c_str();


    char lfc[20],hfc[20];
    std::sprintf(lfc,"%f",lowerCutoff/samplingFrequency);
    std::sprintf(hfc,"%f",higherCutoff/samplingFrequency);
    //std::cout<<lfc<<","<<hfc<<std::endl;
    const char* argv[]={"mkfilter", "-Bu","-Bp","-o", "4", "-a",lfc,hfc, "-l",'\0'};
    FilterCoefficients filtercoeffs=build_bandpass_filter(argv);
    gain=filtercoeffs.gain;
    xcoeffs=filtercoeffs.xcoeffs;
    ycoeffs=filtercoeffs.ycoeffs;

  //  std::cout<<"In bandpassfilter cpp"<<std::endl;
//    std::cout<<"Gain: "<<gain<<std::endl;
//    for(int i=0;i<9;i++){
//        std::cout<<xcoeffs[i]<<" ";
//    }
//    std::cout<<std::endl;
//    for(int i=0;i<9;i++){
//        std::cout<<ycoeffs[i]<<" ";
//    }
//    std::cout<<std::endl;
    for(int i=0;i<NZEROS+1;i++){
        xv[i]=0; yv[i]=0;
    }
}

void BandPassFilter::process(std::vector<double> &data)
{
    int nbSamples=data.size();
    //std::cout<<nbSamples<<std::endl;
    for(int n=0;n<nbSamples;n++){
        //        for(int i=0;i<order/4;i++){
        //            w0[i] = d1[i]*w1[i] + d2[i]*w2[i]+ d3[i]*w3[i]+ d4[i]*w4[i] + data[n];
        //            std::cout<<w0[i]<<","<<w1[i]<<","<<w2[i]<<","<<w3[i]<<","<<w4[i]<<" + "<<data[n]<<std::endl;
        //            data[n] = A[i]*(w0[i] - 2.0*w2[i] + w4[i]);
        //            w4[i] = w3[i];
        //            w3[i] = w2[i];
        //            w2[i] = w1[i];
        //            w1[i] = w0[i];
        //        }

        xv[0] = xv[1]; xv[1] = xv[2]; xv[2] = xv[3]; xv[3] = xv[4]; xv[4] = xv[5]; xv[5] = xv[6]; xv[6] = xv[7]; xv[7] = xv[8];
//         std::cout<<data[n]<<"-->";

        xv[8] = data[n]/ gain;
        yv[0] = yv[1]; yv[1] = yv[2]; yv[2] = yv[3]; yv[3] = yv[4]; yv[4] = yv[5]; yv[5] = yv[6]; yv[6] = yv[7]; yv[7] = yv[8];
        yv[8] =   (xv[0]*xcoeffs[0] + xv[8]*xcoeffs[8]) +  (xcoeffs[2]*xv[2] + xcoeffs[6]*xv[6]) + xcoeffs[4] * xv[4]
                +(xv[1]*xcoeffs[1])+(xv[3]*xcoeffs[3])+(xv[5]*xcoeffs[5])+(xv[7]*xcoeffs[7])

                + (ycoeffs[0] * yv[0]) + (ycoeffs[1] * yv[1])
                + (ycoeffs[2] * yv[2]) + (ycoeffs[3] * yv[3])
                + (ycoeffs[4] * yv[4]) + (ycoeffs[5] * yv[5])
                + (ycoeffs[6] * yv[6]) + (ycoeffs[7] * yv[7]);
        data[n] = yv[8];
//       std::cout<<data[n]<<std::endl;
    }

}



BandPassFilter::~BandPassFilter()
{

}


