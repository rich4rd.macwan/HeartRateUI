#ifndef PULSEDETECTOR_H
#define PULSEDETECTOR_H
#include <cstdlib>
#include <iostream>
#include <vector>
#include "functions.h"
#include <QObject>
#include "bandpassfilter.h"
#include "peak.h"
#include"functions.h"
#include <QTimer>

class PulseDetector : public QObject
{
    Q_OBJECT

    const double PI ;

    const double MIN_FREQ_HEARTRATE ;//(Hz)
    const double MIN_PERIOD_HEARTRATE;
    const double MAX_FREQ_HEARTRATE ;//(Hz)

#ifdef ENABLE_ICA
    //Itpp mat for storing data used for performing ica
    //! covariance matrix of centered data 3x3
    itpp::mat cov_cntrd_x;
    //! Eigen vector matrix for evd decomp.
    itpp::mat V;
    //! Eigen values for evd decomp.
    itpp::vec d;
    //! The whitened source signals
    itpp::mat whitened;
    //! Covariance of the whitened data. Should be identity. Only for checking
    itpp::mat cov_whitened;
    //! The separated signals

    itpp::mat sepMat;
    //! The detrended signals(centered)
    itpp::mat detrended_itpp;
    void performICA(std::vector<std::vector<double> > &detrended,std::vector<std::vector<double> > &separated);
#endif

    int nChannels;
    int windowLengthInFrames;
    int rppgMethod;
    enum TEMPORAL_FILTER {HANNING_FILTER=0x01u, BUTTERWORTH_FILTER=0x02u, MEAN_FILTER=0x04u, NO_FILTER=0x00u};
    enum RGB_CODE {RED = 0u, GREEN = 1u, BLUE = 2u, NB_RGB_CODES};
    quint64 currentFrameTimestamp;
    //std::pair<double,quint64> currentWaveformValue,previousWaveformValue,currentPeak,previousPeak; //Contains peak value and timestamp

    Peak currentWaveformValue,previousWaveformValue,currentPeak,previousPeak;
    //Contains peak value, timestamp, and index of the peak in peakFlags;

    int interpolate(double y, double ymax, double ymin, int height=100);
    std::vector<Peak> candidatePeaks;
    int pulseResult;
    int peaksWindowSize;
    //Remembers the index when the pulse calculation timer times out so that we can calculate the HR every timeout
    int pulseCalculationWindow_StartIndex;
    QTimer* pulseCalculationTimer;
    bool rising;
    //Minimum distance between peaks in milliseconds. Set to static now. Later will be set to half the peak width
    quint64 interBeatThreshold,interBeatInterval;
    int peakFlagIndex;
    bool firstPass;
    BandPassFilter *bpf;
    double ymax,ymin;
    struct DetrendParams{
        static const int lambda=10;
        static const int T=300;
        cv::Mat I,d,D2,D2T,D2Tprod,BottomRightBlockD2Tprod;

        double arr[3];

        DetrendParams(){
            int T=300;
            arr[0]=1; arr[1]=-2; arr[2]=1;
            I=cv::Mat::eye(T,T,CV_64F);
            d=cv::Mat(1,3,CV_64F,arr);
            D2=cv::Mat::zeros(T-2,T,CV_64F);
            D2T=cv::Mat::zeros(T,T-2,CV_64F);
            D2Tprod=cv::Mat::zeros(T,T,CV_64F);
            //Using the detrend method by Tarvainen et. al.
            //D2 matrix is of the form      1  -2  1  0  0  0 ...
            //                              0   1 -2  1  0  0
            //                              0   0  1 -2  1  0 ...
            //                              .
            //                              .
            //                              .
            for(int i=0;i<D2.rows;i++){
                d.copyTo(D2.rowRange(i,i+1).colRange(i,i+3));
            }

            cv::mulTransposed(D2,D2Tprod,true);
            BottomRightBlockD2Tprod=D2Tprod.rowRange(298,300).colRange(296,300);
        }
    }detrendParams;

public slots:
    void pulseCalculationTimeOut();
public:
    int getCurrentPulse();
    void startPulseCalculationTimer();
    void stopPulseCalculationTimer();
    explicit PulseDetector(QObject *parent = 0);
    ~PulseDetector();

    /**
        Return the estimated cardiac pulse frequency by analyzing RGB traces acquired at fps values per seconds (Hz) of durationSample seconds of video (seconds)
        Analyse one of the specified RGB Channel
        */
    void updatePulseData(std::vector<std::vector<double> > &traces, std::vector<std::vector<double> > &normalized, std::vector<std::vector<double> > &detrended, std::vector<std::vector<double> > &separated, int fps, quint64 currentFrameTimestamp, std::vector<Peak > &peakFlags, int rppgMethod=BASIC);
    /**
        The normalization transforms a trace x(t) to a trace x'(t) which is zero-mean and has unit variance.
        */
    void normalizeTraces(std::vector<std::vector<double> > &traces,std::vector<std::vector<double> > &normalized);



    void detrend(std::vector<std::vector<double> > &traces, std::vector<std::vector<double> > &detrended, std::vector<std::vector<double> > &separated, std::vector<Peak> &peakFlags);

    void detectPeaks(std::vector<std::vector<double> > &detrended, std::vector<Peak> &peakFlags);

    void computePulse();

    void updateCandidatePeaks(Peak prevPeak, bool replace=false);

    std::vector<double> averageTraces(const std::vector<std::vector<double> >& traces, std::vector<int> avgChannels);

    void temporalFiltering(std::vector<std::vector<double> >& traces, int fps, int filterFlags );

    void hanningFilter(std::vector<std::vector<double> >& traces);

    void butterworthFilter(std::vector<std::vector<double> >& traces, int fps);

    void meanFilter(std::vector<std::vector<double> >& traces, int maskSize);



    std::vector<double> realFourierTransform(std::vector<std::vector<double> >& traces, int fps, int analysedChannel=0);

    int heuristicSelectPulse(std::vector<double>& FTSpectrum, int fps, unsigned long nbFrames);

    void displayMat(cv::Mat&);
    //void displayEigenMat(Eigen::MatrixXd&);
    void displayVector(std::vector<double>&);


    //The 1st double stands for FREQUENCY (Hz) and the 2nd double stands for POWER SPECTRUM
    //Return true when the the power spectrum of the first element is greater than the power spectrum in the second element
    static bool powerSpectrumCompare(const std::pair<double, double>& firstElem, const std::pair<double, double>& secondElem);


signals:
    void setRPPGWaveformY(int);
};

#endif // QPULSEDETECTOR_H
