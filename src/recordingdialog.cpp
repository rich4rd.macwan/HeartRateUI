#include "recordingdialog.h"
#include "ui_recordingdialog.h"

RecordingDialog::RecordingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RecordingDialog)
{

    setModal(true);

    ui->setupUi(this);

}

RecordingDialog::~RecordingDialog()
{
    delete ui;
}

int RecordingDialog::showMessage0()
{/*
    defaultMessage="<html><head/><body><p align=\"center\"><span style=\"font-size:36pt;font-family:Monospace; text-decoration: underline; color:#3355bb;\">Instructions</span>\
            </p><p align=\"center\"><span style=\"font-size:20pt;\">1. Make sure that the oximeter is connected properly<br/>and is showing heartrate and oxygen saturation.</span></p>\
            <p align=\"center\"><span style=\"font-size:20pt;\">2. Three recordings will be made with different settings</span></p>\
            <p align=\"center\"><span style=\"font-size:20pt;\">3. Press OK to start recording</span></p><p><br/></p><p><br/></p></body></html>";
    */

    defaultMessage="<html><head/><body><p align=\"center\"><span style=\"font-size:36pt;font-family:Monospace; text-decoration: underline; color:#3355bb;\">Instructions</span>\
            </p><p align=\"center\"><span style=\"font-size:20pt;\"><br/>1. Avant de commencer, merci de vérifier que <br/> le capteur  est correctement placé  sur votre doigt, <br/> qu'il est bien connecté et qu'il affiche votre rythme <br/> cardiaque. Ce capteur est très sensible aux mouvements, <br/>essayez autant que possible de ne pas <br/> bouger votre main pendant toute l'expérience !.</span></p>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>2. Nous allons procéder à l'enregistrement de 3 sessions.</span></p>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>3. N'hésitez pas à nous poser une question avant de <br/>commencersi quelque chose <br/> n'est pas clair, sinon appuyez sur OK <br/> pour commencer la première session.</span></p><p><br/></p><p><br/></p></body></html>";
    ui->lblRecordingMessage->setText(defaultMessage);
    return exec();
}


int RecordingDialog::showMessage1()
{
    //Also enter game specific information
//    defaultMessage="<html><head/><body><p align=\"center\"><span style=\"font-size:36pt;font-family:Monospace; text-decoration: underline; color:#3355bb;\">Recording I</span>\
//            </p><p align=\"center\"><span style=\"font-size:20pt;\">1. Try not to move your face</span></p>\
//            <p align=\"center\"><span style=\"font-size:20pt;\">2. Close your eyes and Relax as long as you can hear music</span></p>\
//            <p align=\"center\"><span style=\"font-size:20pt;\">3. Press OK to start recording. The recording will end when the music stops</span></p><p><br/></p><p><br/></p></body></html>";

    defaultMessage="<html><head/><body><p align=\"center\"><span style=\"font-size:36pt;font-family:Monospace; text-decoration: underline; color:#3355bb;\">Session I</span>\
            </p><p align=\"center\"><span style=\"font-size:20pt;\"><br/>1.  Pour cette première session, vous allez fermer les yeux et <br/>vous détendre en écoutant de la musique relaxante.</span></p>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>2. Essayez de bouger le moins possible.</span></p>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>3. Vous pouvez garder les yeux fermés tant que vous entendez <br/>de la musique (1 minute). Dans le doute, gardez les yeux fermés, <br/>nous vous avertirons si l'enregistrement est terminé.</span></p>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>4. Si vous n'avez pas de questions, appuyez <br/>sur OK pour commencer l'enregistrement.</span></p><p><br/></p><p><br/></p></body></html>";

    ui->lblRecordingMessage->setText(defaultMessage);
    return exec();
}

int RecordingDialog::showMessage2()
{
    defaultMessage="<html><head/><body><p align=\"center\"><span style=\"font-size:36pt;font-family:Monospace; text-decoration: underline; color:#3355bb;\">Session II</span>\
            </p>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>1. Pour cette deuxième session, vous allez jouer au jeu 2048 <br/> dans la partie droite de la fenêtre. L'objectif étant <br/>bien sûr de faire le meilleur score <br/>possible en une minute seulement.</span></p>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>2. Pour jouer, il faut simplement appuyer sur <br/> les flèches du clavier pour déplacer les tuiles. <br/>Si deux tuiles ayant le même nombre entrent <br/>en collision durant un mouvement, elles fusionnent<br/> en une nouvelle tuile de valeur double!</span></p>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>3. Si vous ne connaissez pas ce jeu, vous pouvez<br/> nous demander de vous exercer un peu avant de commencer.</span>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>4. Essayer de ne pas bouger pendant cette session.</span>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>5. Si vous n'avez pas de questions, appuyez sur OK<br/> pour commencer l'enregistrement</span></p><p><br/></p><p><br/></p></body></html>";
    ui->lblRecordingMessage->setText(defaultMessage);
    return exec();
}

int RecordingDialog::showMessage3()
{
    defaultMessage="<html><head/><body><p align=\"center\"><span style=\"font-size:36pt; font-family:Monospace;text-decoration: underline; color:#3355bb;\">Session III</span>\
            </p>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>1. Avant de commencer la 3ième session, <br/>donnez-nous votre score de la session 3.</span></p>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>2. Pour cette 3ième session, vous allez voir un oval qui <br/>se déplace dans la vidéo. L'objectif est maintenant<br/> de bouger pour essayer de placer votre tête à <br/>l'intérieur de l'oval autant que possible.</span></p>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>3. Essayez au maximum de ne pas bouger<br/> la main qui porte le capteur.</span></p>\
            <p align=\"center\"><span style=\"font-size:20pt;\"><br/>4. Si vous n'avez pas de questions, appuyez sur OK<br/> pour commencer l'enregistrement.</span></p><p><br/></p><p><br/></p></body></html>";
    ui->lblRecordingMessage->setText(defaultMessage);
    return exec();
}

int RecordingDialog::showLastMessage()
{
    defaultMessage="<html><head/><body><p align=\"center\"><span style=\"font-size:36pt; font-family:Monospace;text-decoration: underline; color:#3355bb;\">Merci!</span>\
            </p>\
            <p align=\"center\"><span style=\"font-size:20pt;\">Si vous n'avez pas de questions, appuyez sur OK pour commencer l'enregistrement..</span></p>\
            <p align=\"center\"><span style=\"font-size:20pt;\">Bonne journée!</span></p>";
    ui->buttonBox->setStandardButtons(QDialogButtonBox::Ok);
    ui->lblRecordingMessage->setText(defaultMessage);
    return exec();
}

