#ifndef CMS50ELISTENER_H
#define CMS50ELISTENER_H

#include <QObject>
#include <QThread>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QFile>
#include <QTextStream>
#include <QReadWriteLock>
#include "oxidata.h"
class CMS50eListener: public QThread
{
    Q_OBJECT
public:
    CMS50eListener(QThread* parent=0);
    ~CMS50eListener();
    void run();
    QSerialPort *serialPort;
    void setRecordMode(bool enableRecord,QString recordFileName="");
    void connectToSerialPort();
    void disconnectSerialPort();
    void setLiveMode(bool mode, QString videoFileName="");
    bool isLiveMode();
signals:
    void message(QString,bool);
    void connectStatus(bool);
    void heartRateSPOCalculated(int,int);
    void waveFormY(int,quint64);
    //VideoReaderWriter should connect to this signal to write to file
    void sendOxiData(OxiData oxidata);
public slots:
    void readData();
    void newFrameArrived(int);
    void controlSignalReceived(bool);
    void startReading();
    void stopReading();
    void pauseReading();
private:
    int count;
    int baudRate;
    bool fingerin,connected;
    bool nextByteHeartRate;

    int waveY,oldY;
    int waveX;
    int heartRate;
    int controlInt;
    int oxygenSat;
    int byte1,byte2,byte3;
    int currentFrameId;
    bool enableRecord;
    QString recordFileName;
    QString metadataFileName;
    QFile *metadataFile;
    QTextStream *metadataOut;
    QString line;
    QReadWriteLock lock;
    bool paused;
    bool liveMode;
    QString videoSource;
    bool useMetadataFile;
};

#endif // CMS50ELISTENER_H
