#ifndef VIDEOREADERWRITER_H
#define VIDEOREADERWRITER_H
#include <string>
#include <iostream>
#include <cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <QDir>
#include <QObject>
#include <QFileInfo>
#include <QProcess>
#include <QTextStream>
#include <iostream>
#include <fstream>
#include <QReadWriteLock>
#include "oxidata.h"
#include <vector>

class VideoReaderWriter : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief recordEnabled
     * Record from camera or video to video file.
     */
    bool recordEnabled;

    /**
     * Mode of operation. Can take any of the following 3 flag values, READ_CAM,READ_VIDEO,READ_VIDEO_WRITE_VIDEO
     * */
    int operationMode;
    //The following  3 flags determine what actions we are doing with the video or camera data, i.e. what are we reading from and writing to.

    /**
     * @brief READ_CAM
     * Read from camera. Recording mode. This variable is called READ_CAM because it is also set when only reading from camera in live mode.
     */

    static const int READ_CAM=-1;
    /**
     * @brief READ_VIDEO
     * Only read from video. Play mode
     */

    static const int READ_VIDEO=0;
    /**
     * @brief READ_VIDEO_WRITE_VIDEO
     * Read from video, write to video. Rarely used. To copy from generic video to custom video
     */

    static const int READ_VIDEO_WRITE_VIDEO=1;




    //The following flags determine whether we are using our own custom video format or generic video format
    /**
     * @brief CUSTOM_VIDEO_MODE
     * Used to depict which mode we are recording in. Our custom mode
     */
    static const int CUSTOM_VIDEO_MODE=1;

    /**
     * @brief GENERIC_VIDEO_OR_CAM_MODE
     * Used to depict that we are using either camera or video
     */
    static const int GENERIC_VIDEO_OR_CAM_MODE=0;

    /**
     * @brief ERROR_VIDEO_MODE
     * Used to depict any error in the recording modes
     */
    static const int ERROR_VIDEO_MODE=-1;

    explicit VideoReaderWriter(int rows=480,int cols=640,int channels=3,QString filename = 0,QObject *parent = 0);
    ~VideoReaderWriter();

    bool open(QString filename, int mode=READ_CAM, int cameraId=-1);
    bool enableRecord(QString recordFilename);
    void writeFrame(cv::Mat, int currentFrameId, quint64 timestamp);
    void writeRawFrame(cv::Mat frame);
    bool readFrame(cv::Mat &frame);
    bool readFrame(cv::Mat &frame, quint64 &currentFrameTimestamp, int &fps);
    void close();
    void buildVideoFromImage();
    int rows();
    int cols();
    int channels();
    int fps();
    void setFps(int fps);
    void writeFileHeader();
    void writeFrameHeader(int currentFrameId,int dataSize);
    void writeOxiData();
    void readOxiData();
    void readFileHeader();
    void swapOxiBuffers();
    int CustomVideoFileMode();
    void writeFrameDataForEmbedded(long long frameid, quint64 currentFrameTimestamp, std::vector<double> rppgValue);
    void writeFrameDataForEvaluation(long long frameid, std::vector<double> rppgValue);
    int currentHeartRate();
    int heartRate;
public slots:
    void onOxiDataReceived(OxiData oxidata);
signals:
    void message(QString,bool);
    void beginProcessingVideo();
    void endProcessingVideo(QString,bool);
    //OximeterWidget should connect to this signal to display
    void sendOxiData(OxiData oxidata);
private:
    QString filename;
    QString filenameToSave;
    QString embeddedFilename;
    QString evaluationFilename;
    QString storageDir;

    char separator;
    QProcess *ffmpegProcess;
    QDir dir;
    QString ffmpegProcessString;
    QString videometadataFileName;
    QFile *videometadataFile;
    QTextStream *videometadataOut;
    QString line;

    std::ofstream outFile;
    std::ifstream inFile;
    int nRows,nCols,nChannels;
    int framerate;
    int totalFrames;
    char* imageBuffer;
    int headerCode;
    //customVideoFileMode=1 means our custom format
    //customVideoFileMode=0 means generic format
    //customVideoFileMode=-1 means invalid source

    int customVideoFileMode;
    cv::VideoCapture* vcap;
    bool recordFromCamera;
    int dataSize;
    int currentFrameId;
    quint64 frametimestamp;
    std::vector<OxiData> oxibuffer1,oxibuffer2;
    std::vector<OxiData>* frontOxiBuffer;
    std::vector<OxiData>* backOxiBuffer;
    QReadWriteLock bufferLock;
    int nOxiDataRows;
    std::ostringstream embeddedostr;
    std::ostringstream evaluationstr;
    bool dirtyBuffer,dirtyBufferEval;
    int currentGTSignal;

};

#endif // VIDEOWRITER_H
