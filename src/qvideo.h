#ifndef QVIDEO_H
#define QVIDEO_H
#include <cv.h>
#include <QWidget>
#include <QSize>
#include <QLabel>
#include <QAction>
#include "qglcanvas.h"
#include <QGridLayout>
#include <qcombobox.h>
#include <qtoolbutton.h>
#include <QButtonGroup>
class QVideo : public QWidget
{
    Q_OBJECT
public:
    explicit QVideo(QWidget *parent = 0);
    explicit QVideo(QString caption,QWidget *parent = 0);
  //  QIplImage* getQImage();
    QString getCaption();
    QGLCanvas* qImage;
	void setHorizontalPadding(int);
	void setVerticalPadding(int);
    int getHorizontalPadding();
    int getVerticalPadding();
	QGridLayout* gridLayout;
	void setVideoVisible(bool);
    void setVideoSize(int width,int height);
    QSize videoSize();
    void addQImage(int width, int height);
    void populateCameras();
    void updateCameraStatus();
    void enableROISelection(bool enable);
    void setCaption(QString caption);
signals:

	 void showVideo(bool);
     void videoSourceSelected(QString videoSource);
     void videoSourceSelected(int camera);
     void videoResized(int,int);
public slots:
   void loadVideo();
   void setImage(IplImage &);
//   void setImage(cv::Mat);
   //void setImage(QIplImage* qImage);
   void toggleVisibility();
	QSize sizeHint();
	QSize minimumSizeHint();
    QSize maximumSize();
    void webcamButtonClicked(int,bool);


	//void onFormatSelected(int);
private:
	bool visible;
    QWidget* parent;
    QLabel* caption;
    //QTextEdit* captionEdit;
	QLabel* lblPbo;
    QAction* actionToggleVisibility;
	int hGap,vGap;
	QToolButton* btnToggleVisibility;
    QToolButton* btnSourceVideo;
	QComboBox* imageFormats;
    int WIDTH,HEIGHT;
    QButtonGroup webcamButtons;
    int nCameras;
    int prevCamSource;
    QString prevFileName;

};

extern QVideo* org;
#endif // QVIDEO_H
