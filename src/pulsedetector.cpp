#include "pulsedetector.h"
#include <cmath>
#include <algorithm>
#include <numeric>
#include <math.h>
#include <QDebug>
#include "mainwindow.h"

using namespace cv;
#define sign(a) ( ( (a) < 0 )  ?  -1   : ( (a) > 0 ) )
//#include <DspFilters\Common.h>
//#include <DspFilters\Butterworth.h>

PulseDetector::PulseDetector(QObject *parent) : QObject(parent),
    PI(3.14159265358979323846),MIN_FREQ_HEARTRATE(0.75),  MIN_PERIOD_HEARTRATE(1000./MIN_FREQ_HEARTRATE),MAX_FREQ_HEARTRATE (4.),peaksWindowSize(20),interBeatThreshold(10),peakFlagIndex(0),pulseResult(0)
{

    previousPeak=Peak();
    previousWaveformValue=previousPeak;
    firstPass=true;
    currentWaveformValue=Peak();
    ymax=0;
    ymin=0;
    bpf=NULL;
    nChannels=3;
    //    icaMat=NULL;
    //    sepMat=NULL;
    pulseCalculationWindow_StartIndex=0;
    pulseCalculationTimer=new QTimer();
    windowLengthInFrames=MainWindow::instance->windowLengthInFrames();
#ifdef ENABLE_ICA
    cov_cntrd_x.set_size(nChannels,nChannels);
    V.set_size(nChannels,nChannels);
    d.set_size(nChannels);
#endif
    connect(pulseCalculationTimer,SIGNAL(timeout()),this,SLOT(pulseCalculationTimeOut()));
}

PulseDetector::~PulseDetector()
{

}



void PulseDetector::updatePulseData(std::vector<std::vector<double> > &traces, std::vector<std::vector<double> > &normalized, std::vector<std::vector<double> > &detrended, std::vector<std::vector<double> > &separated,int fps, quint64 currentFrameTimestamp, std::vector<Peak> &peakFlags, int rppgMethod)
{

    this->rppgMethod=rppgMethod;
    //    //Error managing
    //    if(traces.size() == 0){

    //        return -7;
    //    }
    //    if(fps <= 0)
    //        return -3;
    if(traces.size()>0 && fps>0){


        //Save current frame timestamp
        this->currentFrameTimestamp=currentFrameTimestamp;



        RGB_CODE analysedChannel = GREEN;


        //Normalize ( Zero mean signal and variance = 1)
        normalizeTraces(traces,normalized);
        //Filter the signal

       detrend(normalized,detrended,separated,peakFlags);
       temporalFiltering(detrended, fps, PulseDetector::BUTTERWORTH_FILTER);

#ifdef  ENABLE_ICA
        if(rppgMethod==ICA_RGB){
            //The detrend function should have populated the itppMat with the required data.
            //The separated vector of vector here is an output parameter for the separated sources
            //std::cout<<"Mean of detrended:"<<cv::mean(detrended[0])<<","<<cv::mean(detrended[0])<<","<<cv::mean(detrended[0])<<std::endl;
            //Need to whiten

            performICA(detrended,separated);

            //Filter the signal
            temporalFiltering(separated, fps, PulseDetector::BUTTERWORTH_FILTER);
        }
        else{
            //Filter the signal
//            temporalFiltering(detrended, fps, PulseDetector::BUTTERWORTH_FILTER);
        }
#endif


        detectPeaks(detrended,peakFlags);

        //FFT
        //std::vector<double> traces_FT = realFourierTransform(detrended, fps, analysedChannel);

        //Estimate frequency according to the absolute-real fourier transform
        //int pulseResult = heuristicSelectPulse(traces_FT, fps, traces[0].size());
        //computePulse();
        //Extract time period of peaks
        //int pulseResult=0;

    }
}


void PulseDetector::detrend(std::vector<std::vector<double> > &traces, std::vector<std::vector<double> > &detrended, std::vector<std::vector<double> > &separated, std::vector<Peak> &peakFlags)
{
    int nbChannels = (int) traces.size();//Number of rows/channels
    int nbValues = (int) traces[0].size();//Number of columns/values for each rows

    //    if(icaMat==NULL){
    //        //If we dont have 3 channels(maybe more?), recreate the icaMat.
    //        icaMat=new itpp::mat(nbChannels,MainWindow::instance->windowSize());
    //        sepMat=new itpp::mat(nbChannels,MainWindow::instance->windowSize());
    //        nChannels=nbChannels;
    //    }

    if(rppgMethod==ICA_RGB){
        #ifdef ENABLE_ICA
        detrended_itpp.set_size(nbChannels,nbValues);
        sepMat.set_size(nbChannels,nbValues);
        #endif
    }
    if(nbValues>2){
        Mat d2tprod;
        //Instead of multiplying the d2 with its transpose everytime, we construct it dynamically from the already calculated D2Tprod
        //We multiply for nbValues < 5 since after 4x4 the middle pentat in d2 repeats itself.
        //e.g. D2' * D2 looks like this for 8x8:

        //        1 -2  1  0  0  0  0  | First 2 rows
        //       -2  5 -4  1  0  0  0  | ___________________________________
        //        1 -4  6 -4  1  0  0  | This pattern repeats for n-4 rows
        //        0  1 -4  6 -4  1  0  |
        //        0  0  1 -4  6 -4  1  | ___________________________________
        //        0  0  0  1 -4  5 -2  |
        //        0  0  0  0  1 -2  1  | Last 2 rows
        //This way the time taken to perform detrended gets reduced to the order of 20-25ms (for 300 values) compared to 300-350ms
        if(nbValues<5){
            Mat d2=detrendParams.D2.rowRange(0,nbValues-2).colRange(0,nbValues);
            cv::mulTransposed(d2,d2tprod,true);
        }
        else{
            //Extract the product matrix from detrendParams.D2Tprod
            d2tprod=(detrendParams.D2Tprod.rowRange(0,nbValues).colRange(0,nbValues)).clone();

            // displayMat(d2tprod);

            // d2tprod.adjustROI(nbValues-2,nbValues,nbValues-4,nbValues) =detrendParams.BottomRightBlockD2Tprod;
            detrendParams.BottomRightBlockD2Tprod.copyTo(d2tprod.rowRange(nbValues-2,nbValues).colRange(nbValues-4,nbValues) );

        }


        // displayMat(d2tprod);

        Mat I=detrendParams.I.rowRange(0,nbValues).colRange(0,nbValues);

        Mat prod=(I-(I+detrendParams.lambda*detrendParams.lambda*d2tprod).inv());


        //    displayMat(d2tprod);
        //        Eigen::MatrixXd d2e=detrendParams.D2E.block(0,0,nbValues-2,nbValues);
        //        Eigen::MatrixXd d2te=detrendParams.D2ET.block(0,0,nbValues,nbValues-2);
        //        Eigen::MatrixXd Ie=detrendParams.IE.block(0,0,nbValues,nbValues);
        for(int i=0;i<nbChannels;i++){

            Mat z=Mat(traces[i],true);
            //Using the detrend method by Tarvainen et. al.
            Mat zstat=prod*z;

            //Convert back to vector
            const double* p = zstat.ptr<double>(0);
            cv::minMaxLoc(zstat,&ymax,&ymin);

            //            std::cout<<"PulseDetector::detrend() --> Mean["<<i<<"] = "<<cv::mean(zstat)<<std::endl;
            // Copy data to a vector.  (p + zstat.rows) points to the
            // end of the column.
            //detrended[i]=std::vector<double>(p, p + zstat.rows);

            for(int j=0;j<detrended[i].size();j++){
                detrended[i][j]=zstat.at<double>(j);
#ifdef ENABLE_ICA
                if(rppgMethod==ICA_RGB){
                    // separated[i][j]=detrended[i][j];
                    //Populate the itpp mat here, it is centered
                    detrended_itpp(i,j)=zstat.at<double>(j);
                    }
#endif

            }

        }

        //Send waveform y of green channel for now
        //emit setRPPGWaveformY(detrended[1][nbValues-1]*100);
    }
    peakFlags.push_back(Peak(currentFrameTimestamp,peakFlagIndex));
    ++peakFlagIndex;

}

void PulseDetector::detectPeaks(std::vector<std::vector<double> > &detrended, std::vector<Peak> &peakFlags)
{
    int nbChannels = (int) detrended.size();//Number of rows/channels
    int nbValues = (int) detrended[0].size();//Number of columns/values for each rows

    int fauxYO = interpolate(0.0,ymax,ymin);
    int vraiYO = 100/2;
    int delta =  vraiYO-fauxYO;
    int dy=150;
    // Peak detection here
    double prevDiff=0,prev,curr;

    Peak previousPeak;

    int analysedChannel=1;
    if(rppgMethod==CHROM ){
        analysedChannel=2;
    }
    for(int i=0;i<nbChannels;i++){
        int currY=0,prevY=interpolate(detrended[i][0],ymax,ymin);
        for(int n=0;n<nbValues;n++){
            //curr=zstat.at<double>(n);

            //Update detrended signal
            //detrended[i][n]=curr;

            currY=interpolate(detrended[i][n],ymax,ymin)+delta+dy;

            if(n>0 && i==analysedChannel){
                peakFlags[n-1].isPeak=false;
                peakFlags[n-1].peakValue=prevY;
                //The logic for which peak value is a higher number is reversed between PYR and CHROM methods

                if(rppgMethod==ICA_RGB || rppgMethod==BASIC){

                    if((sign(prevDiff) !=sign(prevY-currY)) && (prevY-currY)>0){
                        //Also perform non-max suppression here
                        //Non-max suppression, if the time difference between the previous peak and current peak is less than the human heart rate, eliminate that peak
                        quint64 timediff=(peakFlags[n-1].timestamp-previousPeak.timestamp);
                        //Choose the peak with the higher value if peaks are close by
                        if(timediff<interBeatThreshold)
                        {
                            //qDebug()<<previousPeak.peakValue<<"<"<<prevY;
                            if(previousPeak.peakValue<prevY && prevY>200){
                                //Subtract the peakflagIndex of the first peak in the vector to account for moving window
                                peakFlags[previousPeak.peakFlagIndex-peakFlags[0].peakFlagIndex].isPeak=false;

                                peakFlags[n-1].isPeak=true;
                                previousPeak=peakFlags[n-1];
                                //Remove the false peak from candidatePeaks
                                updateCandidatePeaks(previousPeak,true);

                            }
                        }
                        else{
                            if(prevY>200){//Will change the static value later
                                peakFlags[n-1].isPeak=true;
                                previousPeak=peakFlags[n-1];
                                updateCandidatePeaks(previousPeak);
                            }
                        }

                    }
                }
                if(rppgMethod==CHROM ){

                    if((sign(prevDiff) !=sign(prevY-currY)) && (prevY-currY)<0){
                        //Also perform non-max suppression here
                        //Non-max suppression, if the time difference between the previous peak and current peak is less than the human heart rate, eliminate that peak
                        quint64 timediff=(peakFlags[n-1].timestamp-previousPeak.timestamp);
                        //Choose the peak with the higher value if peaks are close by
                        if(timediff<interBeatThreshold)
                        {
                            //qDebug()<<previousPeak.peakValue<<"<"<<prevY;
                            if(previousPeak.peakValue>prevY){
                                //Subtract the peakflagIndex of the first peak in the vector to account for moving window
                                peakFlags[previousPeak.peakFlagIndex-peakFlags[0].peakFlagIndex].isPeak=false;

                                peakFlags[n-1].isPeak=true;
                                previousPeak=peakFlags[n-1];
                                //Remove the false peak from candidatePeaks
                                updateCandidatePeaks(previousPeak,true);

                            }
                        }
                        else{
                            if(prevY<200){//Will change the static value later
                                peakFlags[n-1].isPeak=true;
                                previousPeak=peakFlags[n-1];
                                updateCandidatePeaks(previousPeak);
                            }
                        }

                    }

                }

            }

            prevDiff=prevY-currY;
            prevY=currY;
        }
    }
}

void PulseDetector::computePulse()
{
    double avgIBI=0;
    int count=0;
    //  qDebug()<<"PulseDetetor::computePulse:: candidatePeaks.size()="<<candidatePeaks.size();
    for(int i=2;i<candidatePeaks.size()-pulseCalculationWindow_StartIndex;i++){
        if(candidatePeaks[i].timestamp>candidatePeaks[i-2].timestamp){
            //  qDebug()<<"PulseDetector::computePulse:: IBI(i)="<<candidatePeaks[i].timestamp<<"-"<<candidatePeaks[i-2].timestamp<<"="<<(candidatePeaks[i].timestamp-candidatePeaks[i-2].timestamp);
            avgIBI+=(candidatePeaks[i].timestamp-candidatePeaks[i-2].timestamp);
            ++count;
        }
    }

    avgIBI/=count;

    //avgIBI/=count;
    //For now return IBI to check. Will return pulse later
    pulseResult=60000/(.5*avgIBI);
    if(pulseResult<0)
        pulseResult=0;

}

void PulseDetector::updateCandidatePeaks(Peak prevPeak,bool replace)
{
    if(replace && candidatePeaks.size()>0){
        candidatePeaks.erase(candidatePeaks.end()-1);
    }
    candidatePeaks.push_back(prevPeak);
    if(candidatePeaks.size()>peaksWindowSize){
        candidatePeaks.erase(candidatePeaks.begin());
    }

}


/**
    /!\ Each traces have the same size
*/
void PulseDetector::normalizeTraces(std::vector<std::vector<double> > &traces,std::vector<std::vector<double> > &normalized)
{
    int  nbChannels = (int) traces.size();//Number of rows/channels
    int nbValues = (int) traces[0].size();//Number of columns/values for each rows
    std::vector<double> means(nbChannels, 0.);// mean of traces[i]
    std::vector<double> sigmas(nbChannels, 0.);// standard deviation of traces[i]
    //When nbValues==1, sigma becomes 0(since means[i]==d and thus gives divide by zero error
    if(nbValues>2){
        //Mean
        for(int i = 0; i < nbChannels; ++i) {

            double sum =0;
            sum=std::accumulate(traces[i].begin(), traces[i].end(), 0.0);
            //        for(int j=0;j<traces[i].size();j++)
            //            sum+=traces[i][j];
            means[i] =  sum / nbValues;

        }

        //Standard deviation
        for(int i = 0; i < nbChannels; ++i) {
            double accum = 0.0;
            //            std::for_each (traces[i].begin(), traces[i].end(), [&]( double d) {
            //                accum += (d -  means[i]) * (d -  means[i]);
            //            });


            for(int k=0;k<traces[i].size();k++){
                accum+=(traces[i][k] -  means[i]) * (traces[i][k] -  means[i]);
            }
            sigmas[i] = sqrt(accum / (nbValues));

        }

        double alpha=sigmas[0]/sigmas[1];
        //Normalization zero-mean and variance=1
        for(int i = 0; i < nbChannels; ++i){
            for(int k=0; k < nbValues; ++k){
                if(rppgMethod==CHROM ){
                    //Obtain the Pulse trace from the XY channels
                    //P[i]=X[i]-alpha*Y[i]
                    //This?
                    traces[2][k]=traces[0][k]-alpha*traces[1][k];
                    //or this?
                    //normalized[2][k]=normalized[0][k]-alpha*normalized[1][k];
                }
                normalized[i][k] = (traces[i][k]-means[i])/sigmas[i];
                // std::cout<<normalized[i][k]<<",";
            }

        }
    }
    //std::cout<<std::endl;
}

#ifdef ENABLE_ICA
void PulseDetector::performICA(std::vector<std::vector<double> > &detrended,std::vector<std::vector<double> > &separated)
{


    //    //TODO: Finish this
    //First need to whiten the data
    //Cov of centered data
    whitened.set_size(nChannels,detrended_itpp.get_row(0).size());

    cov_cntrd_x=detrended_itpp*detrended_itpp.transpose()/windowLengthInFrames;
    //EVD decomp.
    itpp::eig_sym(cov_cntrd_x,d,V);
    //Rotate the data
    for(int i=0;i<3;i++){
        d(i)=1/sqrt(d(i));
    }
    itpp::mat D=itpp::diag(d);


    whitened=V*D*V.transpose()*detrended_itpp;
    cov_whitened=whitened*whitened.transpose()/whitened.get_row(0).size();
//    qDebug()<<"PulseDetector::performICA, winSize=("<<whitened.get_row(0).size()<<")  cov(whitened)= ";
//    displayItppMat(cov_whitened);
    itpp::Fast_ICA fastica(whitened);
    //Need to whiten first

    fastica.set_fine_tune(true);
//    fastica.set_max_num_iterations(10000);
    fastica.set_nrof_independent_components(nChannels);
    fastica.set_non_linearity(FICA_NONLIN_SKEW );
    fastica.set_approach( FICA_APPROACH_DEFL );
    tic();
    fastica.separate();
    qDebug()<<"ICA separation of signal size "<< whitened.get_row(0).size()<<" in "<<toc()<<" ms";
    sepMat= fastica.get_independent_components();
    //  itpp::mat sep=fastica.get_independent_components();
    for(int i=0;i<nChannels;i++){
        //double* p=icaMat->get_row(i)._data();
        //detrended[i]=std::vector<double>(p,p+(icaMat->get_row(i).size()));
        for(int j=0;j<whitened.get_row(i).size();j++){
            separated[i][j]=sepMat(i,j);
        }

    }

}
#endif
void PulseDetector::temporalFiltering(std::vector<std::vector<double> >& traces, int fps, int filterFlags )
{
    if(filterFlags & PulseDetector::HANNING_FILTER)
        hanningFilter(traces);

    if(filterFlags & PulseDetector::BUTTERWORTH_FILTER)
        butterworthFilter(traces, fps);

    if(filterFlags & PulseDetector::MEAN_FILTER)
        meanFilter(traces, 120);
}

void PulseDetector::hanningFilter(std::vector<std::vector<double> >& traces)
{
    std::vector<double> hannWindow;
    int nbValues = (int) traces[0].size();//Number of columns/values for each rows
    int nbChannels = (int) traces.size();//Number of rows/channels
    int N = nbValues-1;//Number of values indices for Hanning computation

    //Compute Hanning window
    if(nbValues == 1)
        hannWindow.push_back(1.);
    else
    {
        for(int i = 0; i < nbValues;++i)
        {
            //Computation from the MatLab Hanning window : http://www.mathworks.fr/fr/help/signal/ref/hann.html
            //0.5*(1.-cos(2*PI*(i/N)))
            double val = 0.5*(1.-cos(2*PI*(i/(double)N)));
            hannWindow.push_back(val);
        }
    }

    //Apply hanning to traces
    for(int i = 0; i < nbChannels; ++i){
        for(int k=0; k < nbValues; ++k){
            //  qDebug()<<traces[i][k]<<"*"<<hannWindow[k];
            traces[i][k] *= hannWindow[k];
        }
    }

}

void PulseDetector::butterworthFilter(std::vector<std::vector<double> >& traces, int fps)
{
    /*
    int nbChannels = traces.size();//Number of rows/channels
    int nbValues = traces[0]->size();//Number of columns/values for each rows
    double bandWidth = PulseDetector::MAX_FREQ_HEARTRATE-PulseDetector::MIN_FREQ_HEARTRATE;
    double centerFrequency = (PulseDetector::MAX_FREQ_HEARTRATE+PulseDetector::MIN_FREQ_HEARTRATE)/2.;

    //Double pointer of double means 2D Matrix, channels*samples
    //Such as we have with traces by doing this :
    double * channel = traces[0];
    double** arrayOfChannels = &((*channel)[0]); //Pointer to traces pointers,

    // Create a Butterworth Band Pass filter of order 4
    // with state for processing 1 channel.
    Dsp::SimpleFilter <Dsp::Butterworth::BandPass <4>, 1> f;
    f.setup(4, fps, centerFrequency, bandWidth);//Order, SampleRate, CenterFrequency and BandWidth (defining interval)
    f.process<double>(nbValues, arrayOfChannels);
    */
    //Initialize bandpass filter the first time
    if(bpf==NULL){
        bpf=new BandPassFilter(4,fps,MIN_FREQ_HEARTRATE,MAX_FREQ_HEARTRATE);
    }
    for(int i=0;i<traces.size();i++){
        if(traces[i].size()>4)
            bpf->process(traces[i]);
    }

}

void PulseDetector::meanFilter(std::vector<std::vector<double> >& traces, int maskSize)
{
    int nbChannels = (int) traces.size();//Number of rows/channels
    int nbValues = (int) traces[0].size();//Number of columns/values for each rows
    if(nbValues > maskSize)
    {
        for(int i = 0; i < nbChannels; ++i)
        {
            for(int k = 0; k < (nbValues); k++)
            {
                double sum = 0.;

                //Sum of the convolution mask
                //side effect : Symetry ( ... 2 1 [0 1 2 3 .... N-2 N-1 N] N-1 N-2... )
                for(int j = k-(maskSize/2);j < k+(maskSize/2); j++)
                {
                    int indexVal = 0;
                    if(j < 0) // Avoid sub indexing
                        indexVal = abs(j);
                    else if(j >= nbValues) // Avoid over indexing
                        indexVal = (nbValues - (j-nbValues)) - 1;
                    sum+=traces[i][indexVal];
                }

                traces[i][k] = sum/maskSize;
            }
        }
    }
    //std::cout << "Mean Filtering !" << std::endl;
}



std::vector<double> PulseDetector::realFourierTransform(std::vector<std::vector<double> >& traces, int fps, int analysedChannel)
{
    // int nbChannels = (int) traces.size();//Number of rows/channels
    int nbValues = (int) traces[0].size();//Number of columns/values for each rows
    //    Mat _signals(1, nbValues, CV_64F);

    //    for(int i = 0; i < nbValues; ++i)
    //        _signals.at<double>(0,i) = traces[analysedChannel][i];

    Mat _signals=Mat(traces[analysedChannel],true);
    //Create two planes (represents complex number : One for Real plane[0], One for Image plane[1])
    Mat planes[] = {Mat_<double>(_signals), Mat::zeros(_signals.size(), CV_64F)};
    merge(planes, 2, _signals);         // Add to the expanded another plane with zeros

    dft(_signals, _signals, DFT_ROWS | DFT_COMPLEX_OUTPUT);

    split(_signals, planes);
    planes[0].copyTo(_signals);//Get real part of the DFT

    std::vector<double> realResult;

    for(int i = 0; i < nbValues; ++i)
        realResult.push_back(_signals.at<double>(0,i));

    return realResult;
}


int PulseDetector::heuristicSelectPulse(std::vector<double>& FTSpectrum, int fps, unsigned long nbFrames)
{
    std::vector<std::pair<float,float> > sortedSpectrum; //The FTSpectrum sorted descending according to Power of spectrum (amplitude)
    sortedSpectrum.reserve(FTSpectrum.size());


    //Compute the Frequency domain associated to the frequency domain computed by realFourierTransform
    std::vector<double> freq;
    freq.reserve(nbFrames);

    for(int i=0; i< nbFrames;++i)
        freq.push_back((((double)i/(double)nbFrames)*fps));

    //Apply mask (set 0 to useless frequencies)
    for(int i=0; i < FTSpectrum.size(); ++i)
    {
        if( !(freq[i] >= MIN_FREQ_HEARTRATE && freq[i] <= MAX_FREQ_HEARTRATE))
        {
            FTSpectrum[i] = 0.;
        }
    }
    //Mat prod=(I-(I+detrendParams.lambda*detrendParams.lambda*d2tprod).inv());
    //Compute Absolute and POWER of spectrum (square power ²)
    for(int i=0; i< FTSpectrum.size();++i){
        //        std::cout<<FTSpectrum[i]<<" >> ";
        FTSpectrum[i] = abs(FTSpectrum[i]);
        //  std::cout<<FTSpectrum[i]<<" | ";
    }

    //Creating a vector of pair to associate the right frequency to the right power (amplitude) of spectrum
    for(int i = 0; i < FTSpectrum.size(); ++i)
    {
        if(FTSpectrum[i] != 0.)
            sortedSpectrum.push_back(std::pair<float, float>( freq[i], FTSpectrum[i]) );
    }

    //Sorting descending according to the amplitude (keep the associated frequency)
    std::sort(sortedSpectrum.begin(), sortedSpectrum.end(),PulseDetector::powerSpectrumCompare);

    //return the 1st hight peak frequency which is a sample heuristic to estimate the Heart rate
    //std::cout << sortedSpectrum.size() << ":" << sortedSpectrum[0].first*60. << std::endl;
    int ret;
    if(sortedSpectrum.size() >0)
        ret = (int)((sortedSpectrum[0].first)*60.);
    else
        ret = 0;
    return ret;
}

bool PulseDetector::powerSpectrumCompare(const std::pair<double, double>& firstElem, const std::pair<double, double>& secondElem)
{
    return firstElem.second > secondElem.second;
}




void PulseDetector::displayMat(Mat &temp)
{
    for(int x=0;x<temp.rows;x++){
        for(int y=0;y<temp.cols;y++){
            std::cout<<temp.at<double>(x,y)<<" ";
        }
        std::cout<<std::endl;
    }
    std::cout<<"--------------------"<<std::endl;

}


void PulseDetector::displayVector(std::vector<double>& temp)
{
    std::cout<<" [ ";
    for(int c=0;c<temp.size();c++){
        std::cout<<temp[c]<<" ";
    }
    std::cout<<" ] ";
    std::cout<<std::endl<<"----------"<<std::endl;
}

/**
 * @brief PulseDetector::interpolate
 * @param y The value to be interpolated
 * @param ymax
 * @param ymin
 * @param height =100 dummy range
 * @return value in int
 *
 * This function interpolates the double values to a suitable range so that peak comparisons work properly
 */
int PulseDetector::interpolate(double y, double ymax, double ymin, int height)
{
    double ratio = (ymax-ymin)/height;
    return height-(y-ymin)/ratio;
    //return (y-ymin)/ratio;
}

void PulseDetector::pulseCalculationTimeOut()
{

    computePulse();
    //   qDebug()<<"PulseDetector::pulseCalculationTimeOut():: Computing pulse after "<<candidatePeaks.size()<<" peaks. HR="<<pulseResult;
    //    pulseCalculationWindow_StartIndex=candidatePeaks.size();
}

int PulseDetector::getCurrentPulse()
{
    return pulseResult;
}
/**
 * @brief PulseDetector::startPulseCalculationTimer
 * Used for live mode to display calculated pulse every 5 seconds
 */
void PulseDetector::startPulseCalculationTimer()
{
    //Start the HR calculation timer at the first pass
    pulseCalculationTimer->start(3000);
}

void PulseDetector::stopPulseCalculationTimer()
{
    pulseCalculationTimer->stop();
}
