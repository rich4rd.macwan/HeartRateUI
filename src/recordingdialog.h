#ifndef RECORDINGDIALOG_H
#define RECORDINGDIALOG_H

#include <QDialog>

namespace Ui {
class RecordingDialog;
}

class RecordingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RecordingDialog(QWidget *parent = 0);
    ~RecordingDialog();
int showMessage1();
int showMessage0();
int showMessage2();
int showMessage3();
int showLastMessage();
private:
    Ui::RecordingDialog *ui;
    QString defaultMessage;
};

#endif // RECORDINGDIALOG_H
