#include "resultplot.h"
#include "mainwindow.h"
#include <QPainter>
#include <QDebug>
#include <QMouseEvent>
#define sign(a) ( ( (a) < 0 )  ?  -1   : ( (a) > 0 ) )

ResultPlot::~ResultPlot()
{

}


ResultPlot::ResultPlot(QWidget *parent)
    : QWidget(parent)
{
    shape = Path;
    antialiased = true;
    transformed = false;
    showTimestamp = false;
    drawDataPoint=false;
    showOximeterIBI=true;
    resultLabel="Root Mean Squared Error";
    waveformLabel="Pulse Waveform";
    textBG=QColor(qRgba(170,180,250,50));
    xInc=5;
    waveformIndex=0;
    pixmap.load(":/images/webcam.png");
    //setPalette(parent->palette());
    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);


    padding=10;
    gridSizePx=50;
    gridOn=true;
    //    right=QPoint(this->width(),this->height()/2);
    left=QPoint(padding,this->sizeHint().height()/2);
    right=QPoint(this->width()-padding,this->height()/2);

    // this->setGeometry(0,0,400,300);
    horizontalAxisColor=QColor(qRgb(100,100,100));
    curveColor=QColor(qRgb(10,210,30));
    dataPointBG=QColor(qRgba(255,255,224,50));
    curvePen=QPen(curveColor);

    curvePen.setStyle(Qt::SolidLine);
    curvePen.setWidth(1.5);

    gtColor=QColor(qRgb(210,100,50));
    gtPen=QPen(gtColor);
    gtPen.setWidth(1.5);

    gridPen=QPen(horizontalAxisColor);
    axisPen=QPen(horizontalAxisColor);


    pen=QPen(qRgb(0,0,0));
    pen.setWidthF(1.5);
    font.setPixelSize(14);

    dataPointFont.setPixelSize(12);
    rect=QRect(10,140,150,25);
    meanErrorRect=QRect(25,50,200,50);
    //connect(&timer,SIGNAL(timeout()),this,SLOT(onTimerElapsed()));
    //timer.start(100);
    yValue=0;
    yMax=0;
    yMin=100000;
    yStep=0.1;
    xValue=0;

    xValueGRPPG=0;
    yValueGRPPG=0;
    yGRPPGDisplay=0;
    yMaxGRPPG=0;
    yMinGRPPG=1000;
    RMSD=0;
}

QSize ResultPlot::minimumSizeHint() const
{
    return QSize(600, 150);
}

QSize ResultPlot::sizeHint() const
{
    return QSize(600, 150);
}

void ResultPlot::resizeEvent(QResizeEvent *)
{
    left.setX(padding);
    left.setY(this->height()/2);

    right.setX(this->width()-padding);
    right.setY(this->height()/2);

    top.setX(padding);
    top.setY(padding);
    bottom.setX(padding);
    bottom.setY(this->height());

    rect.setHeight(20);
    rect.setWidth(80);
    rect.setX(20);
    rect.setY(height()-50);

    meanErrorRect.setWidth(250);
    meanErrorRect.setHeight(20);
    meanErrorRect.setX(20);
    meanErrorRect.setY(rect.y()-meanErrorRect.height()-5);

    legendRect.setX(width()-160);
    legendRect.setY(height()-130);
    legendRect.setWidth(150);
    legendRect.setHeight(100);

    valueRect.setX(legendRect.x()+5);
    valueRect.setY(legendRect.y()+30);
    valueRect.setWidth(20);
    valueRect.setHeight(20);

    gtRect.setX(legendRect.x()+5);
    gtRect.setY(valueRect.y()+30);
    gtRect.setWidth(20);
    gtRect.setHeight(20);

    dataPointRect.setX(0);
    dataPointRect.setY(0);
    dataPointRect.setWidth(0);
    dataPointRect.setHeight(0);

}

void ResultPlot::setCurvePenColor(QColor color)
{
    curvePen.setColor(color);
}

void ResultPlot::setShape(Shape shape)
{
    this->shape = shape;
    update();
}

void ResultPlot::setPen(const QPen &pen)
{
    this->pen = pen;
    update();
}

void ResultPlot::setBrush(const QBrush &brush)
{
    this->brush = brush;
    update();
}

void ResultPlot::setAntialiased(bool antialiased)
{
    this->antialiased = antialiased;
    update();
}

void ResultPlot::setTransformed(bool transformed)
{
    this->transformed = transformed;
    update();
}


void ResultPlot::setGridOn(bool val)
{
    gridOn=val;
    repaint();
}

void ResultPlot::updateView()
{
    currDiff=yDisplay-yPrev;

    if(sign(prevDiff)!=sign(currDiff) && currDiff>0){

        //Peak or valley. Display timestamp value

        showTimestamp=currentTimestamp-prevTimestamp;

        setResultLabel(QString::number(showTimestamp));
        prevTimestamp=currentTimestamp;
    }


    yPrev=yDisplay;
    prevDiff=currDiff;
    //repaint();
}

void ResultPlot::setWaveformLabel(QString val)
{
    waveformLabel=val;
    repaint();
}

void ResultPlot::setResultLabel(QString val)
{
    resultLabel=val;
}

void ResultPlot::setRPPGSignal(std::vector<RPPGSignalValue> rppgSignal)
{
    yMax=0;
    yMin=1000;

    for(int i=0;i<rppgSignal.size();i++){
        RMSD+=((rppgSignal[i].groundtruth-rppgSignal[i].value)*(rppgSignal[i].groundtruth-rppgSignal[i].value));

        if(yMax<rppgSignal[i].value){
            yMax=rppgSignal[i].value;
        }
        if(yMin>rppgSignal[i].value){
            yMin=rppgSignal[i].value;
        }
        if(yMax<rppgSignal[i].groundtruth){
            yMax=rppgSignal[i].groundtruth;
        }
        if(yMin>rppgSignal[i].groundtruth){
            yMin=rppgSignal[i].groundtruth;
        }
    }

    RMSD=sqrt(RMSD);
    //NRMSD/=(yMax-yMin);
    lock.lockForWrite();
    this->RPPGSignal=rppgSignal;
    lock.unlock();
    setMouseTracking(true);
    repaint();
}
#ifdef ENABLE_ICA
void ResultPlot::setRPPGSignal(itpp::mat rppgSignal)
{
    yMax=0;
    yMin=1000;
    lock.lockForWrite();
    for(int i=0;i<rppgSignal.cols();i++){

        if(yMax<rppgSignal.get_row(0).get(i)){
            yMax=rppgSignal.get_row(0).get(i);
        }
        if(yMin>rppgSignal.get_row(0).get(i)){
            yMin=rppgSignal.get_row(0).get(i);
        }
        RPPGSignalValue s;
        this->RPPGSignal.push_back(s);
        this->RPPGSignal[i].value=rppgSignal(0,i);

//        qDebug()<<this->RPPGSignal[i].value;
    }


    //NRMSD/=(yMax-yMin);

  //  qDebug()<<yMax<<","<<yMin;
    lock.unlock();
    setMouseTracking(true);
    repaint();
}
#endif

void ResultPlot::mousePressEvent(QMouseEvent *event)
{
    //qDebug()<<event->x()<<","<<event->y();
    //When x==10, signal x starts
    double X=(10)*width()/(RPPGSignal.size()-10);
     int x=event->x();
     if(x>=X && event->y()<height()/3){
         //qDebug()<<RPPGSignal[x-X].groundtruth<<","<<RPPGSignal[x-X].value;
         dataPointGT=RPPGSignal[x-X].groundtruth;
         dataPointValue=RPPGSignal[x-X].value;
         dataPointRect.setX(event->x());
         dataPointRect.setY(event->y());
         dataPointRect.setWidth(80);
         dataPointRect.setHeight(60);
         drawDataPoint=true;
     }
     else{
         drawDataPoint=false;
     }
     repaint();
}

void ResultPlot::mouseMoveEvent(QMouseEvent *event)
{
    double X=(10)*width()/(RPPGSignal.size()-10);
     int x=event->x();
     if(x>=X && event->y()<height()/3){
         //qDebug()<<RPPGSignal[x-X].groundtruth<<","<<RPPGSignal[x-X].value;
         dataPointGT=RPPGSignal[x-X].groundtruth;
         dataPointValue=RPPGSignal[x-X].X;
         dataPointRect.setX(event->x());
         dataPointRect.setY(event->y());
         dataPointRect.setWidth(80);
         dataPointRect.setHeight(60);
         drawDataPoint=true;
     }
     else{
         drawDataPoint=false;
     }
     repaint();
}

void ResultPlot::clearTraces()
{
    waveform.clear();
}

void ResultPlot::paintEvent(QPaintEvent * /* event */)
{



    //    static const QPoint points[4] = {
    //        QPoint(10, 80),
    //        QPoint(20, 10),
    //        QPoint(80, 30),
    //        QPoint(90, 70)
    //    };



    //    int startAngle = 20 * 16;
    //    int arcLength = 120 * 16;



    QPainter painter(this);
    //    if(xValue>this->width()){
    //        path=QPainterPath();
    //        xValue=0;
    //        //yMin=10000;

    //        //yMax=0;

    //    }
    //    else{
    //        path.lineTo(QPointF(xValue,yDisplay));
    //    }
    //    if(xValueGRPPG>this->width()){
    //        pathGRPPG=QPainterPath();
    //        xValueGRPPG=0;
    //    }
    //    else{
    //        pathGRPPG.lineTo(QPointF(xValueGRPPG,yGRPPGDisplay));
    //    }


    painter.setPen(pen);
    painter.setBrush(brush);
    painter.setCompositionMode(QPainter::CompositionMode_Multiply);
    if (antialiased)
        painter.setRenderHint(QPainter::Antialiasing, true);

    painter.save();


    switch (shape) {
    case Path:
        //Draw the curve
        painter.setPen(curvePen);

        // painter.drawPath(path);
        //painter.drawPolyline(waveform.data(),waveform.size());

        //qDebug()<<"ResultPlot::paint(): RPPGSignal.size()="<<RPPGSignal.size()<<" yMax="<<yMax<<",yMin="<<yMin;
        QPointF pPrev,pCurr;
        QPointF gtPrev,gtCurr;
        int prevDiff;
        for(int i=0;i<RPPGSignal.size();i++){
            RPPGSignal[i].X=0;
            pCurr.setX(double(i+10)*width()/(RPPGSignal.size()-10));
            pCurr.setY(this->height()-10- (RPPGSignal[i].value-yMin)/(yMax-yMin)*(double)(this->height()-20));
            RPPGSignal[i].X=pCurr.x();
            RPPGSignal[i].Y=pCurr.y();

            gtCurr.setX(double(i+10)*width()/(RPPGSignal.size()-10));
            gtCurr.setY(this->height()-10- (RPPGSignal[i].groundtruth-yMin)/(yMax-yMin)*(double)(this->height()-20));
            RPPGSignal[i].gtX=gtCurr.x();
            RPPGSignal[i].gtY=gtCurr.y();


            //std::cout<<RPPGSignal[i].groundtruth<<",";
            painter.setPen(curvePen);
            if(i>0){
            painter.drawLine(pPrev,pCurr);
            //painter.drawRect(pPrev.x()-1,pPrev.y()-1,2,2);
            painter.setPen(gtPen);
            painter.drawLine(gtPrev,gtCurr);
        }

            prevDiff=pPrev.y()-pCurr.y();
            pPrev=pCurr;
            gtPrev=gtCurr;

        }
       // std::cout<<std::endl;
        pen.setColor(QColor(50,50,50));
        painter.setPen(pen);
        painter.setFont(font);
        painter.fillRect(rect,textBG);
        painter.drawText(rect, Qt::AlignTop, waveformLabel);

        painter.drawText(meanErrorRect, Qt::AlignLeft, resultLabel+" : "+QString::number(RMSD));


        painter.drawRect(legendRect);
        painter.drawText(legendRect.x()+5,legendRect.y()+15,"  Legend");

        painter.fillRect(gtRect,gtColor);
        painter.drawText(gtRect.x()+gtRect.width()+10,gtRect.y()+15,"Ground Truth");
        painter.fillRect(valueRect,curveColor);
        painter.drawText(valueRect.x()+valueRect.width()+10,valueRect.y()+15,"RPPG Value");
        //Draw the axes
        painter.drawLine(padding/2,padding/2,padding/2,height()-10);
        painter.drawLine(padding/2,height()-10,width()-padding/2,height()-10);

        //Draw the grid
        if(gridOn){

            gridPen.setStyle(Qt::DotLine);

            for(int i=0;i<height();i+=gridSizePx){
                painter.setPen(gridPen);
                left.setY(i);
                right.setY(i);
                painter.drawLine(left, right);
            }

            for(int i=0;i<width();i+=gridSizePx){
                painter.setPen(gridPen);
                top.setX(i);
                bottom.setX(i);
                painter.drawLine(top, bottom);
            }
        }

        //Draw data point
        if(drawDataPoint){
            painter.setFont(dataPointFont);
            painter.fillRect(dataPointRect,dataPointBG);
            painter.drawText(dataPointRect.x()+5,dataPointRect.y()+20,"GT: "+QString::number(dataPointGT));
            painter.drawText(dataPointRect.x()+5,dataPointRect.y()+40,"Value: "+QString::number(dataPointValue));
        }
        //painter.setPen(qRgb(10,210,10));
        //painter.drawPath(pathGRPPG);
        //pen.setWidth(1);
        //painter.drawPixmap(10, 10, pixmap);

        break;
    }
    painter.setRenderHint(QPainter::Antialiasing, false);
    painter.setPen(palette().dark().color());
    painter.setBrush(Qt::NoBrush);
    painter.drawRect(QRect(0, 0, width() - 1, height() - 1));

    painter.restore();



}
