#ifndef RPPGSIGNAVALUEL_H
#define RPPGSIGNALVALUE_H

#include <QObject>
class RPPGSignalValue{
public:
    int index;
    int value;
    int groundtruth;
    double X,gtX;
    double Y,gtY;
    RPPGSignalValue(){
        index=0;
        value=0;
        groundtruth=0;
        X=0; gtX=0;
        Y=0; gtY=0;
    }

    RPPGSignalValue(int index,int value,int groundtruth){
        this->index=index;
        this->value=value;
        this->groundtruth=groundtruth;
    }
};

#endif // RPPGSIGNALVALUE_H
