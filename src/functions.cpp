#include "functions.h"

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <cmath>
#include <vector>
#include <sstream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>


#define WLow 0.75
#define WHigh 4//2.5 4

using namespace std;
using namespace cv;

/* Global variables */

extern cv::Point pt1, pt2;
extern bool roi_capture;
extern Mat frame;

#include <string>
#include <list>
#include <iostream>
#include <cstring>

const char *wschars = "\t\n ";

vector<string> split(const string &s, char delim) {
    vector<string> elems;
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::string to_string(long long num)
{
    std::stringstream strm;
    strm<<num;
    return strm.str();
}


string to_string(double num)
{
    std::stringstream strm;
    strm<<num;
    // std::cout<<strm.str()<<std::endl;
    return strm.str();
}
Scalar getMoyPixels(Mat &frame, Mat &mask, bool simple)
{
    Scalar rgb;
    //Check results without scaling

    /** NOTE: Uncomment this block when using face tracking.
    float scaleW = 0.6;
    float scaleH = 0.9;
    
    //namedWindow( "my window", CV_WINDOW_AUTOSIZE );
    //imshow( "my window", frame );
    
    // keep 60% of the face
    int faceWidth = (frame.cols*scaleW);
    int faceHeight = (frame.rows*scaleH);
    int faceX = (frame.cols - faceWidth) / 2;
    int faceY = (frame.rows - faceHeight) / 2;
    Rect myRect =  Rect(faceX, faceY, faceWidth, faceHeight);
    Mat faceROI = frame(myRect);
    Mat maskROI= mask(myRect);

    // YB TODO: select the downsampled size automatically according to the size of the face
    // downsample the face to increase PSNR

    **/
    /** NOTE: We are using skin detection right now, so better not scale the face
     *  Comment this block to use face detection instead of skin detection
     * */
    Mat faceROI=frame;
    Mat maskROI=mask;
    /**
     * Skin detection block ends here
      */
    /* downsample the image if PYR method is chosen*/
    if(!simple){
        for(int i=0;i<3;i++)
        {
            pyrDown( faceROI, faceROI, Size( faceROI.cols/2, faceROI.rows/2 ) );
            if(maskROI.rows>0 && maskROI.cols>0)
                pyrDown( maskROI, maskROI, Size( maskROI.cols/2, maskROI.rows/2 ) );
            //imshow( "my window", faceROI );
        }
    }
    /* and then compute the mean */
    // imshow("MaskROI",maskROI);
    // imshow("FaceROI",faceROI);
    if(maskROI.rows>0 && maskROI.cols>0)
        rgb =  mean(faceROI,maskROI);
    else
        rgb =  mean(faceROI);

//    for(int c=0;c<3;c++)
//        rgb(c)=round(rgb(c));

   // std::cout<<rgb(0)<<","<<rgb(1)<<","<<rgb(2)<<std::endl;
    return rgb;
}

Scalar getCHROMPixels(Mat &frame,Mat &mask)
{
    /** NOTE: Uncomment this block when using face tracking.
    float scaleW = 0.6;
    float scaleH = 0.9;

    //namedWindow( "my window", CV_WINDOW_AUTOSIZE );
    //imshow( "my window", frame );

    // keep 60% of the face
    int faceWidth = (frame.cols*scaleW);
    int faceHeight = (frame.rows*scaleH);
    int faceX = (frame.cols - faceWidth) / 2;
    int faceY = (frame.rows - faceHeight) / 2;
    Rect myRect =  Rect(faceX, faceY, faceWidth, faceHeight);
    Mat faceROI = frame(myRect);
    Mat maskROI= mask(myRect);

    // YB TODO: select the downsampled size automatically according to the size of the face
    // downsample the face to increase PSNR

    **/
    /** NOTE: We are using skin detection right now, so better not scale the face
     *  Comment this block to use face detection instead of skin detection
     * */
    Mat faceROI=frame;
    Mat maskROI=mask;
    /**
     * Skin detection block ends here
      */
    /* downsample the image */
    for(int i=0;i<3;i++)
    {
        pyrDown( faceROI, faceROI, Size( faceROI.cols/2, faceROI.rows/2 ) );
        pyrDown( maskROI, maskROI, Size( maskROI.cols/2, maskROI.rows/2 ) );
        //imshow( "my window", faceROI );
    }
    /* and then compute the mean */


    std::vector<Mat> rgbPlanes;
    cv::split(faceROI,rgbPlanes);
    /** A more intuitive implementation for the CHROM method
     * */
    Mat chrom1(faceROI.rows,faceROI.cols,CV_32SC1);
    Mat chrom2(faceROI.rows,faceROI.cols,CV_32SC1);
    Mat chrom3(faceROI.rows,faceROI.cols,CV_32SC1);
    chrom1=-1.5*rgbPlanes[0]+2*rgbPlanes[1];
    chrom2=1.5*rgbPlanes[0]+rgbPlanes[1]-1.5*rgbPlanes[2];
    chrom3=1.5*rgbPlanes[0]-3*rgbPlanes[1]+1.5*rgbPlanes[2];
    //        cv::imshow("R",rgbPlanes[0]);
    //        cv::imshow("G",rgbPlanes[1]);
    //        cv::imshow("B",rgbPlanes[2]);
    //std::cout<<mean(chrom1)[0]<<","<<mean(chrom1)[1]<<","<<mean(chrom1)[2]<<std::endl;
    Scalar xyp,rgb;
    xyp(0)=mean(chrom1,maskROI)[0];
    xyp(1)=mean(chrom2,maskROI)[0];
    xyp(2)=mean(chrom3,maskROI)[0];
    /** Simpler CHROM implementation. Do not delete
    rgb =  mean(faceROI);
    xyp(0)=1.5*rgb(0)-2*rgb(1);
    xyp(1)=1.5*rgb(0)+rgb(1)-1.5*rgb(2);
    xyp(2)=1.5*rgb(0)-3*rgb(1)+1.5*rgb(2);
    **/

    return xyp;
}

double smoothPulse(std::vector<double> t, int lastN)
{
    if(t.size() < lastN)
        return 0;

    double somme = 0;
    int idx = 0;
    for(int i=(int)t.size()-lastN; i < t.size(); ++i)
    {
        somme += t[i];
        idx++;
    }

    return (somme)/lastN;
}


std::vector<double> findPeaks(Mat & resPCA, int column)
{
    std::vector<double> pics;
    for(int i = 1; i< resPCA.rows-1; ++i)
    {
        if(((resPCA.at<float>(i,column)-resPCA.at<float>(i+1,column)) > 0) && ((resPCA.at<float>(i,column)-resPCA.at<float>(i-1,column)) > 0))
        {
            pics.push_back((double)resPCA.at<float>(i,column));
        }

    }

    return pics;
}



void tic()
{

    //t1 = std::chrono::high_resolution_clock::now();
    t1=clock();

}


long long toc()
{
    //t2=std::chrono::high_resolution_clock::now();
    t2=clock();
    long long duration = ( t2-t1)*1000/CLOCKS_PER_SEC;
    //std::cout<<duration<<std::endl;
    return duration;
}



#ifdef ENABLE_ICA
Mat itpp_to_cvMat(itpp::mat &)
{
    //TODO: Write a fast implementation for this
}


itpp::mat cv_to_itppMat(Mat &)
{
    //TODO: Write a fast implementation for this
}


void displayItppMat(itpp::mat &m)
{
    for(int i=0;i<m.rows();i++){
        for(int j=0;j<m.cols();j++){
         std::cout<<m(i,j)<<" ";
        }
        std::cout<<std::endl;
    }
        std::cout<<std::endl;
}

#endif
string RPPGMethodString(int rppgMethod)
{
    std::string method="BASIC";
    switch(rppgMethod){
    case 0:
        method="BASIC";
        break;
    case 1:
        method="CHROM";
        break;
    case 2:
        method="ICA_RGB";
        break;
    }
    return method;
}

