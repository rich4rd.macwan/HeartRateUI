#include "stimuluswidget.h"
#include "ui_stimuluswidget.h"

#include <QKeyEvent>
QTimer* StimulusWidget::timer = NULL;
StimulusWidget::StimulusWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StimulusWidget)
{
    ui->setupUi(this);
    timer=new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(timerElapsed()));
    gameUrls.push_back("http://canvasrider.com/tracks/911084");
    gameUrls.push_back("file:///home/richard/src/2048/index.html");
    gameUrls.push_back("file:///home/richard/src/floppybird/index.html");
    //ui->webView->setUrl(gameUrls[1]);

    //Set music to be played. If the files are installed
    QString src1="/opt/HeartRateUI/data/soothing.mp3";
    QFile file(src1);
    if(file.exists())
        playlist.push_back(src1);
    else
        playlist.push_back("soothing.mp3");


    QString src2="/opt/HeartRateUI/data/disturbing.mp3";
    QFile file2(src2);
    if(file2.exists())
        playlist.push_back(src2);
    else
        playlist.push_back("disturbing.mp3");

    ui->btnPlayer->setVisible(false);
    gameid=0;
    blink=false;
    recordLengthInSeconds=10;
    connect(&blinkTimer,SIGNAL(timeout()),this,SLOT(setBlue()));


    for(int i=0;i<3;i++){
        WaveformWidget* widget=new WaveformWidget();
        widget->setResultLabel("Source "+QString::number(i+1)+" IBI");
        widget->setGridOn(true);
        widget->setCurvePenColor(QColor(230*(i==0),230*(i==1),230*(i==2)));
        widget->setObjectName("source"+QString::number(i+1));
        //widget->setCurvePenColor(QColor(200,200,200));
        ui->tabRPPG->layout()->addWidget(widget);
        icaSourceWidgets.push_back(widget);

      }

}

StimulusWidget::~StimulusWidget()
{
    delete ui;
}
/**
 * @brief MentalCalculationWidget::startTimer
 * Need to call this when we start recording video
 */
void StimulusWidget::startTimer()
{
    m=0;s=0;

    //    QKeyEvent *event = new QKeyEvent ( QEvent::KeyPress, Qt::Key_Enter, Qt::NoModifier);
    //    QCoreApplication::postEvent (ui->webView, event);

    timer->start(1000);
}

void StimulusWidget::stopTimer()
{
    qDebug()<<"MCWidget::stopTimer()";
    //ui->webView->setUrl(gameUrls.at(1));
    musicPlayer.stop();
    timer->stop();
    ui->lblTimer->setText("00:00");
    this->updateGeometry();
}

void StimulusWidget::setupRecording1()
{
    ui->tabWidget->setCurrentIndex(0);
    musicPlayer.setMusicFile(playlist.at(0));
    musicPlayer.start();
//    ui->webView->setVisible(false);
    fontStyleSheet="font: 48pt;";
    ui->lblTimer->setStyleSheet(fontStyleSheet);
    ui->lblTimer->setGeometry(ui->lblTimer->x(),ui->lblTimer->y(),200,80);
    ui->lblMcwMessage->setVisible(false);
    setVisible(true);
    updateGeometry();

}

void StimulusWidget::setupRecording2()
{
    ui->tabWidget->setCurrentIndex(0);
    musicPlayer.stop();
    musicPlayer.setMusicFile(playlist.at(1));
    musicPlayer.start();
    setVisible(true);
//    ui->webView->setUrl(gameUrls.at(1));
//    ui->webView->setVisible(true);
//    ui->webView->setFocus();

    fontStyleSheet="font: 24pt;";
    ui->lblTimer->setStyleSheet(fontStyleSheet);
    ui->lblMcwMessage->setVisible(true);
}

void StimulusWidget::setupRecording3()
{
    ui->tabWidget->setCurrentIndex(0);
    musicPlayer.setMusicFile("");
    musicPlayer.stop();
//    ui->webView->setVisible(false);
    fontStyleSheet="font: 48pt;";
    ui->lblTimer->setStyleSheet(fontStyleSheet);
    ui->lblTimer->setGeometry(ui->lblTimer->x(),ui->lblTimer->y(),200,50);
    ui->lblTimer->setVisible(true);
    ui->lblMcwMessage->setVisible(false);
    updateGeometry();

}

void StimulusWidget::setRecordLength(int secs)
{
    //Minimum record length is fixed to 10s
    if(secs<10)
        secs=10;
    recordLengthInSeconds=secs;
}

void StimulusWidget::showGameWidget()
{
    ui->tabWidget->setCurrentIndex(0);
//    ui->webView->setVisible(true);
    ui->tabRPPG->setVisible(false);
}

void StimulusWidget::disableGameWidget()
{
//    ui->webView->setEnabled(false);
}

void StimulusWidget::showICAWidget()
{
    ui->tabWidget->setCurrentIndex(1);
    setVisible(true);

}

MusicPlayer *StimulusWidget::getMusicPlayerRef()
{
    return &musicPlayer;
}

void StimulusWidget::setRPPGSignal(std::vector<double> rppgValues)
{
    //Send the values to waveformwidgets
  // qDebug()<<rppgValues[0]<<","<<rppgValues[1]<<","<<rppgValues[2];
    icaSourceWidgets[0]->setWaveformY(rppgValues[0]);
    icaSourceWidgets[1]->setWaveformY(rppgValues[1]);
    icaSourceWidgets[2]->setWaveformY(rppgValues[2]);

}

void StimulusWidget::timerElapsed()
{
    //qDebug()<<"MCWidget timer elapsed";
    s++;
    if(s>recordLengthInSeconds-7){
        blink=!blink;
        if(blink){
           ui->lblTimer->setStyleSheet(fontStyleSheet+";color:crimson");
           blinkTimer.start(500);
        }
        else{
           ui->lblTimer->setStyleSheet(fontStyleSheet+"color:black");
        }
    }
    if(s==recordLengthInSeconds){
        ui->lblTimer->setStyleSheet(fontStyleSheet+"color:black");
    }
    if(s==60){
        s=0;
        m++;
    }

    ui->lblTimer->setText( (m<10?"0"+QString::number(m):QString::number(m))+":"+(s<10?"0"+QString::number(s):QString::number(s)));
    this->updateGeometry();
}

void StimulusWidget::setBlue()
{
    ui->lblTimer->setStyleSheet(fontStyleSheet+"color:darkblue");
    ui->lblTimer->updateGeometry();
    blinkTimer.stop();
}



void StimulusWidget::btnPlayerClicked()
{
    if(musicPlayer.validMusicFile() && !musicPlayer.playing){
        musicPlayer.start();
        //Set stop icon
        ui->btnPlayer->setIcon(QIcon(":/images/stop.png"));
        updateGeometry();
    }
    else if(musicPlayer.validMusicFile() && musicPlayer.playing){
        musicPlayer.stop();
        //Set play icon
        ui->btnPlayer->setIcon(QIcon(":/images/play.png"));
        updateGeometry();
    }

}
