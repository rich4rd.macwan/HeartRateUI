#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#pragma once
#include <iostream>
#include <stdlib.h>
#include <vector>
//#include <chrono>
#include <vector>
#include <opencv2/core/core.hpp>
#include <ctime>



#ifdef ENABLE_ICA
    #include <itpp/signal/fastica.h>
    #include <itpp/itbase.h>
    #include <itpp/stat/misc_stat.h>
cv::Mat itpp_to_cvMat(itpp::mat&);
itpp::mat cv_to_itppMat(cv::Mat&);
void displayItppMat(itpp::mat&);
#endif
/* Prototypes */
cv::Scalar getMoyPixels(cv::Mat &frame, cv::Mat &maskROI,bool simple=false);
cv::Scalar getCHROMPixels(cv::Mat &frame, cv::Mat &mask);
double smoothPulse(std::vector<double> t, int lastN); // compute the mean of the last N element of a vector
std::vector<std::string> split(const std::string &s, char delim);
std::string to_string(long long);
std::string to_string(double);
//static std::chrono::high_resolution_clock::time_point t1,t2;
namespace {
clock_t t1,t2;
//const int SIMPLE=0,PYR=1,CHROM=2,PYR_ICA=3,CHROM_ICA=4,SIMPLE_ICA=5;
const int BASIC=0,CHROM=1,ICA_RGB=2;
}

std::string RPPGMethodString(int);
/** Function timers.
 *  Call tic() to start the timer. Call toc() to get the time elapsed from the last tic().
 * */
void tic();
/** std::chrono::high_resolution_clock
 * Call toc() to retrieve time elapsed(in microseconds) from the last tic()
 * */
long long toc();
//For recursive usage of tic-tocs
//std::vector<std::chrono::time_point> tics;


#endif
