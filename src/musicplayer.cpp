#include "musicplayer.h"
#include <QFile>
#include <QDebug>
MusicPlayer::MusicPlayer(QObject* parent):
    QObject(parent)
{
    playing=false;
    paused=false;
    validSource=false;
#ifdef __linux__
    processString="/usr/bin/cvlc";
#endif

#ifdef _WIN32 || _WIN64
  // Set path of windows programs directory
  //#error Can't be compiled on Windows yet
    processString="C://Program Files  (x86)//VideoLAN//VLC//vlc --intf rc";
#endif


}

MusicPlayer::MusicPlayer(QString musicFile)
{

#ifdef __linux__
    processString="/usr/bin/cvlc";
#endif

#ifdef _WIN32 || _WIN64
  // Set path of windows programs directory
  //#error Can't be compiled on Windows yet
    processString="C://Program Files  (x86)//VideoLAN//VLC//vlc --intf rc";
#endif


    this->musicFile=musicFile;
    playing=false;
    paused=false;
    connect(&playerProcess,SIGNAL(readyReadStandardError()),this,SLOT(onReadyReadError()));
    connect(&playerProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(onReadyReadOutput()));
    validateMusicFile();



}

void MusicPlayer::setMusicFile(QString musicFile)
{
    this->musicFile=musicFile;
    validateMusicFile();
}

QString MusicPlayer::getMusicFile()
{
    return musicFile;
}

bool MusicPlayer::validMusicFile()
{
    //Check if musicFile is valid
    return true;
}

MusicPlayer::~MusicPlayer()
{

}

void MusicPlayer::start()
{
    qDebug()<<"MusicPlayer::start(), QProcess status="<<playerProcess.state();
    if(playerProcess.state()==QProcess::Running){
        playerProcess.terminate();
      //  playerProcess.waitForFinished();
    }
    //Start playing music file
    if(validMusicFile()){

        playerProcess.start(processString+" "+musicFile);
        qDebug()<<"Playing music : "<<processString+" "+musicFile;
        playing=true;
        paused=false;
    }else{
        qDebug()<<"Cannot play music";
        playing=false;
        paused=false;
        playerProcess.terminate();
    }
}

void MusicPlayer::stop()
{
    playerProcess.terminate();
    playerProcess.waitForFinished();
    //Stop playing music file
    playing=false;
    paused=false;
}

void MusicPlayer::pause()
{
    playing=false;
    paused=true;
}

void MusicPlayer::onReadyReadOutput()
{
    qDebug()<<playerProcess.readAllStandardOutput();
}

void MusicPlayer::onReadyReadError()
{
       qDebug()<<playerProcess.readAllStandardError();
}

void MusicPlayer::validateMusicFile()
{
    validSource=false;
    //Check if musicFile exists and is valid.
    QFile file(musicFile);
    if(file.exists()){
        QStringList tokens=file.fileName().split(".");
        QString ext=tokens[tokens.length()-1];
        //Currently support these extensions. We only will use mp3
        if(ext=="mp3" || ext=="ogg" || ext=="wav"){
            validSource=true;
        }
    }
    file.close();
}

