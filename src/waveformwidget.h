#ifndef WAVEFORMWIDGET_H
#define WAVEFORMWIDGET_H
#include <QWidget>
#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QWidget>
#include <QTimer>
#include <QTime>
#include <qmath.h>
#include <cv.h>
class WaveformWidget : public QWidget
{
    Q_OBJECT
public:
    explicit WaveformWidget(QWidget *parent = 0);
    ~WaveformWidget();
    enum Shape { Line, Points, Polyline, Polygon, Rect, RoundedRect, Ellipse, Arc,
                 Chord, Pie, Path, Text, Pixmap };

    QSize minimumSizeHint() const Q_DECL_OVERRIDE;
    QSize sizeHint() const Q_DECL_OVERRIDE;
    void resizeEvent(QResizeEvent *);
    void setCurvePenColor(QColor);
    void clearTraces();
public slots:
    void setShape(Shape shape);
    void setPen(const QPen &pen);
    void setBrush(const QBrush &brush);
    void setAntialiased(bool antialiased);
    void setTransformed(bool transformed);
    void setWaveformY(int yDisplay,quint64 timestamp);
    void setWaveformY(double yValue);
    void setGRPPGWaveformY(int yDisplay);
    void setGridOn(bool);
    void updateView();
    void setWaveformLabel(QString val);
    void setResultLabel(QString val);

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
    Shape shape;
    QPen pen,curvePen,gridPen;
    QBrush brush;
    QColor textBG;
    bool antialiased;
    bool transformed;
    QPixmap pixmap;
    QPoint right,left,top,bottom;
    int padding;
    QTimer timer;
    QTime time;
    double yValue,yDisplay,yValueGRPPG,yGRPPGDisplay;
    double prevDiff,currDiff,yPrev;
    int xValue,xValueGRPPG;
    QPainterPath path,pathGRPPG;
    QVector<QPointF> sinePoints;
    double yStep;
    QColor horizontalAxisColor,curveColor;
    int gridSizePx;
    bool gridOn;
    double yMax,yMaxGRPPG,yMinGRPPG,yMin;
    quint64 showTimestamp;
    quint64 prevTimestamp,currentTimestamp;
    QRect rect,timestampRect;
    QFont font;
    bool showOximeterIBI;
    QString waveformLabel;
    QString resultLabel;
    std::vector<double> waveform;
    std::vector<quint64> timestamps;
    int xInc;
    int waveformIndex;
};

#endif // WAVEFORMWIDGET_H
