#ifndef PEAK_H
#define PEAK_H

#include <QObject>

class Peak{
public:
    //We need to pull the peakValue from the detrended vector directly since it keeps on changing
    //So we save the index in the detrended
    //double *peakValue;
    quint64 timestamp;
    int peakFlagIndex;
    bool isPeak;
    int peakValue;
    Peak(){
        isPeak=false;
        timestamp=0;
        peakFlagIndex=0;
        peakValue=0;
    }

    Peak(quint64 t,int pF){
        //Whether this is a peak or not is determined and updated later
        isPeak=false;
        timestamp=t;
        peakFlagIndex=pF;
        peakValue=0;
    }
};

#endif // PEAK_H
