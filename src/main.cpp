#include "mainwindow.h"
#include <QApplication>
#include <cv.h>
#include <opencv2/core/core.hpp>
#define ENABLE_ICA
#include "functions.h"
using namespace std;

//#define sign(a) ( ( (a) < 0 )  ?  -1   : ( (a) > 0 ) )
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow w;
    w.show();

    //itpp::mat to std::vector<double>
//    itpp::mat imat(3,300);
//    for(int i=0;i<300;i++){
//        imat(i)=i;
//    }
//    //std::cout<<imat<<std::endl;
//    double* p=imat._data();
//     std::vector<double> vec=std::vector<double>(p, p + imat.cols());
//     for(int i=0;i<vec.size();i++){
//         std::cout<<vec[i]<<" ";
//     }
//     std::cout<<std::endl;

    return app.exec();
}
