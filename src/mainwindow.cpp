#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include<opencv2/nonfree/nonfree.hpp>
#include "renderthread.h"
MainWindow* MainWindow::instance =NULL;
const int MainWindow::NUM_RECORDINGS=3;
//XMP – Extensible Metadata Platform (an ISO standard)
QString MainWindow::metadataExt=".xmp";
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    cv::initModule_nonfree();
    qRegisterMetaType<cv::Mat>("cv::Mat");
    qRegisterMetaType<std::vector<RPPGSignalValue> >("std::vector<RPPGSignalValue>");

    qRegisterMetaType<IplImage>("IplImage&");
    qRegisterMetaType<OxiData>("OxiData");
    qRegisterMetaType<std::vector<double> >("std::vector<double>");
#ifdef ENABLE_ICA
    qRegisterMetaType<itpp::mat >("itpp::mat");
#endif
    ui->setupUi(this);

    recordLengthInSeconds=10;
    nRecordings=NUM_RECORDINGS;
    recordMode=false;
    showDialogs=true;
    //ui->mainToolBar->setStyleSheet("border:solid 1px");


    //        setMinimumHeight(video->minimumHeight());
    //        setMaximumHeight(video->minimumHeight());

    setCentralWidget(&centralWidget);


    vlayout=new QVBoxLayout();

    //Horizontal layout for adding mental calculation widget
    hlayout=new QHBoxLayout();

    //vlayout->addLayout(hlayout);

    video=new QVideo("",this);
    connect(video,SIGNAL(videoSourceSelected(QString)),this,SLOT(onVideoSourceSelected(QString)));
    connect(video,SIGNAL(videoSourceSelected(int)),this,SLOT(onVideoSourceSelected(int)));
    connect(video,SIGNAL(videoResized(int,int)),this,SLOT(onVideoResized(int,int)));
    connect(video->qImage,SIGNAL(finishManualROISelection(cv::Rect)),this,SLOT(setManualROISelection(cv::Rect)));


    stimuluswidget=new StimulusWidget();
    stimuluswidget->setRecordLength(recordLengthInSeconds);
    hlayout->addLayout(vlayout,1);
    hlayout->addWidget(stimuluswidget,2);
    stimuluswidget->setVisible(false);
    vlayout->addWidget(video);
    centralWidget.setLayout(hlayout);

    instance=this;

    setWindowTitle("Heart rate estimation - Le2i");

    //Play pause stuff
    playing=false;
    iconPlay=QIcon(":/images/play.png");
    iconPause=QIcon(":/images/pause.png");
    recordFileName="";

    connect(ui->actionPlay,SIGNAL(triggered()),this,SLOT(playButtonPressed()));
    connect(ui->actionStop,SIGNAL(triggered()),this,SLOT(stopButtonPressed()));
    connect(ui->actionRecord,SIGNAL(triggered(bool)),this,SLOT(recordButtonPressed(bool)));
    connect(ui->actionManual_ROI,SIGNAL(triggered()),this,SLOT(playButtonPressed()));



    setWindowIcon(QIcon(":/images/rppg.png"));
    //Oximeter widget
    oximeterWidget=new OximeterWidget();
    //oximeterWidget->setStyleSheet("background-color:white;");

    vlayout->addWidget(oximeterWidget);


    recordingDialog=new RecordingDialog(this);
    evaluationDialog=new EvaluationDialog(this);
    resultDialog=new ResultDialog(this);
    connect(ui->actionRun_Evaluation,SIGNAL(triggered(bool)),this,SLOT(evaluationButtonPressed(bool)));
    //connect(ui->actionView_Results,SIGNAL(triggered()),this,SLOT(resultsButtonPressed()));
    setEvaluationFlag(false);

    QString videoSource=QString::fromStdString("/home/richard/Copy/RPPG Videos/video1.mov");

    for(int i=0;i<3;i++){
        cmbRPPGMethods.addItem(QString::fromStdString(RPPGMethodString(i)));
    }
    ui->mainToolBar->insertWidget(ui->actionManual_ROI,&cmbRPPGMethods);

    rdoFaceDetection=new QRadioButton("Face Detection",this);
    rdoSkinDetection=new QRadioButton("Skin Detection",this);
    rdoLayout=new QVBoxLayout(this);
    rdoLayout->addWidget(rdoFaceDetection);
    rdoLayout->addWidget(rdoSkinDetection);
    rdoSkinDetection->setChecked(true);
    rdoWidget.setLayout(rdoLayout);

    ui->mainToolBar->insertWidget(ui->actionRun_Evaluation,&rdoWidget);
    cmbRPPGMethods.setCurrentIndex(0);

    renderThread=NULL;
    //    setUpVideoThread();
    onVideoSourceSelected(videoSource);
    //ui->actionPlay->setEnabled(false);
}

MainWindow::~MainWindow()
{
    if(stimuluswidget!=NULL)
        stimuluswidget->getMusicPlayerRef()->stop();
    delete ui;
}


QVideo *MainWindow::getVideoWidget()
{
    return video;
}

void MainWindow::setUpVideoThread()
{
    qDebug()<<"MainWindow::setupVideoThread() called ";
    if(renderThread!=NULL){
        renderThread->videoReaderWriter()->disconnect();
        renderThread->disconnect();
        oximeterWidget->disconnect();
        renderThread->stop();
      //  delete renderThread;
    }
    //    qDebug()<<"MainWindow::setUpVideoThread() - Wait for 2 seconds";
    //    QThread::currentThread()->msleep(2000);

    renderThread=new RenderThread();

    connect(renderThread,SIGNAL(sendImage(IplImage&)),video,SLOT(setImage(IplImage&)),Qt::QueuedConnection);
    //connect(renderThread,SIGNAL(endOfVideo()),this,SLOT(stopButtonPressed()));
    connect(renderThread,SIGNAL(newFrameArrived(int)),oximeterWidget,SLOT(newFrameArrived(int)),Qt::QueuedConnection);
    connect(renderThread,SIGNAL(sendRPPGSignal(std::vector<double>)),stimuluswidget,SLOT(setRPPGSignal(std::vector<double>)));
    connect(this,SIGNAL(sendStopSignal()),renderThread,SLOT(stopSignalReceived()));
    //Connection for reading oximeter data from file and displaying it on the waveformwidget
    connect(renderThread->videoReaderWriter(),SIGNAL(sendOxiData(OxiData)),oximeterWidget,SLOT(onOxiDataArrived(OxiData)),Qt::QueuedConnection);
    //Connection for reading oximeter data from the device and writing it to file
    connect(oximeterWidget->CMS50eListenerRef(),SIGNAL(sendOxiData(OxiData)),renderThread->videoReaderWriter(),SLOT(onOxiDataReceived(OxiData)),Qt::QueuedConnection);
}

void MainWindow::show()
{
    QMainWindow::show();

}

/**
 * @brief MainWindow::startRecording123
 * Recording based on the new video record protocol for data accquisition
 * Since we are recording 3 types of videos for the same subject, we need to set up 3 different names .
 * This function takes the names from the recordFileNames vector and records 1 min video of the first name in the vector and erases it.
 * This function will be automatically called again when a recording ends.
 *
 */
bool MainWindow::startRecording123(){
    //    if(recordFileNames.size()>0){
    //        recordFileName=recordFileNames[0];
    --nRecordings;
    if(nRecordings<0){
        qDebug()<<"nRecordings ="<<nRecordings<<", Stop recording!";
        return false;
    }
    bool notcancelled=true;
    //Inform cms50elistener about the recordFileName so that it can setup the metadata file for writing ppg data
    oximeterWidget->CMS50eListenerRef()->setRecordMode(true,recordFileName);
    //Show message about checking the oximeter contacts.
    //Inform about the 3 recordings with different settings
    //qDebug()<<"startRecording123(): recordFileNames.size()="<<recordFileNames.size();

    if(nRecordings==2){//(recordFileNames.size()==3){
        //If ok is pressed, Show base and first message one by one and start recording
        if(recordingDialog->showMessage0()==RecordingDialog::Accepted){
            if(!showDialogs || recordingDialog->showMessage1()==RecordingDialog::Accepted){
                //Set game 1
                if(!renderThread->setRecordMode(true,recordFileName)){
                    ui->actionRecord->setChecked(false);
                }
                stimuluswidget->setupRecording1();
                stimuluswidget->startTimer();
                playButtonPressed(recordLengthInSeconds*1000);
            }
            else{
                //Do not proceed if the user did not press OK
                ++nRecordings;
                notcancelled=false;
            }
        }
        else{
            //Do not proceed if the user did not press OK
            ++nRecordings;
            notcancelled=false;
        }
    }
    else if(nRecordings==1){//(recordFileNames.size()==2){
        //If ok is pressed, Show base and first message one by one and start recording
        if(!showDialogs || recordingDialog->showMessage2()==RecordingDialog::Accepted){
            //Set game 2
            reinitSource();
            stimuluswidget->setupRecording2();
            stimuluswidget->startTimer();
            playButtonPressed(recordLengthInSeconds*1000);
        }
        else{
            //Do not proceed if the user did not press OK
            ++nRecordings;
            notcancelled=false;
        }
    }

    else if(nRecordings==0){//(recordFileNames.size()==1){
        //If ok is pressed, Show base and first message one by one and start recording
        if(!showDialogs || recordingDialog->showMessage3()==RecordingDialog::Accepted){
            //Set game 3
            reinitSource();
            renderThread->displayOval=true;
            stimuluswidget->setupRecording3();
            stimuluswidget->startTimer();
            playButtonPressed(recordLengthInSeconds*1000);

        }
        else{
            //Do not proceed if the user did not press OK
            ++nRecordings;
            notcancelled=false;
        }
    }
    return notcancelled;
}

void MainWindow::reinitSource()
{
    //setUpVideoThread();
    if(prevCameraSource!=-1){
        onVideoSourceSelected(prevCameraSource,true);
    }
    if(prevVideoSource!=""){
        onVideoSourceSelected(prevVideoSource,true);
    }

}

MusicPlayer *MainWindow::getMusicPlayerRef()
{
    return stimuluswidget->getMusicPlayerRef();

}

StimulusWidget *MainWindow::getStimulusWidgetRef()
{
    return stimuluswidget;
}

int MainWindow::windowSize()
{
    return renderThread->windowSize();
}
/**r
 * @brief MainWindow::manualROIMode
 * @return
 * Indicates whether manual ROI selection is enabled.
 */
bool MainWindow::manualROIMode()
{
    return ui->actionManual_ROI->isChecked();
}

bool MainWindow::isEmbeddedTestEnabled()
{
    return ui->actionEnableEmbeddedTest->isChecked();
}

bool MainWindow::isEvaluationEnabled()
{
    return ui->actionEnableEvaluate->isChecked();
}

void MainWindow::setEvaluationFlag(bool val)
{
    ui->actionEnableEvaluate->setChecked(val);
}

void MainWindow::startEvaluation()
{

    //Start evaluation

    //Load filename list from directory
    ui->actionEnableEvaluate->setChecked(true);
    setEvaluationFlag(true);
    QDir dir(evaluationDialog->videoDirectory());
    evaluationFileList=dir.entryList(QStringList("*.vid"));
    sendMessage("Starting evaluation");
    //Disable all the other controls
    ui->actionPlay->setEnabled(false);
    ui->actionRecord->setEnabled(false);
    ui->actionStop->setEnabled(false);
    performEvaluation();
}

QString MainWindow::currentDumpFilePath()
{
    if(evaluationFileList.size()>0){
        //this is executed when run evaluation is not clicked
        QDir dir;
        QStringList tokens=evaluationFileList[evaluationFileIndex].split("/");
        QString dumpfile="rppgdump-"+tokens[tokens.size()-1].split(".")[0]+".txt";
        return evaluationDialog->dumpDirectory()+ dir.separator()+dumpfile;
    }
    else{
        return evaluationDialog->dumpDirectory();
    }
}

bool MainWindow::overwriteDumpFiles()
{
    return evaluationDialog->overwriteDumpFiles();
}

QString MainWindow::RPPGMethod()
{
    return cmbRPPGMethods.currentText();
}

int MainWindow::rppgMethod()
{
    return cmbRPPGMethods.currentIndex();
}

int MainWindow::windowLengthInFrames()
{
    if(renderThread==NULL)
        return 300;
    else
        return renderThread->windowLengthInFrames();
}



void MainWindow::performEvaluation()
{
    QDir dir;
    //Run evaluation for current file. This function will be called again from onStopped
    if(evaluationFileIndex<evaluationFileList.size()){
        if(!isEvaluationEnabled()){
            evaluationFileList.clear();
        }
        else{
            qDebug()<<"MainWindow::performEvaluation():: Evaluating file "<<evaluationFileList[evaluationFileIndex];
            onVideoSourceSelected(evaluationDialog->videoDirectory()+ dir.separator()+evaluationFileList[evaluationFileIndex],false);
            playButtonPressed();
            evaluationFileIndex++;

        }
    }
    else{
        qDebug()<<"End of evaluation";
        renderThread->closeVideoFile();
        ui->actionRun_Evaluation->setChecked(false);
        ui->actionRecord->setEnabled(true);
        ui->actionEnableEvaluate->setEnabled(true);
//        ui->actionView_Results->setEnabled(true);
        ui->actionEnableEmbeddedTest->setEnabled(true);
        ui->actionRun_Evaluation->setEnabled(true);

    }


}


void MainWindow::setManualROISelection(cv::Rect roi)
{
    ui->actionManual_ROI->setChecked(false);
    //Set roi to renderthread
    renderThread->setManualROI(roi);
}

bool MainWindow::modeIsFaceDetection()
{
    return rdoFaceDetection->isChecked();
}
void MainWindow::stopButtonPressed()
{
    //emit sendStopSignal();
    renderThread->stop();
    video->updateCameraStatus();

    ui->actionRecord->setEnabled(true);
    ui->actionEnableEvaluate->setEnabled(true);
    //ui->actionView_Results->setEnabled(true);
    ui->actionEnableEmbeddedTest->setEnabled(true);
    ui->actionRun_Evaluation->setEnabled(true);
}

void MainWindow::manualROIButtonPressed()
{
    playButtonPressed();
}



void MainWindow::onStopped()
{
    playing=false;
    if(recordFileNames.size()==0)
        recordButtonPressed(false);

    oximeterWidget->CMS50eListenerRef()->stopReading();
    ui->actionPlay->setIcon(iconPlay);
    ui->actionStop->setEnabled(false);
    ui->actionManual_ROI->setEnabled(true);
    ui->actionRecord->setChecked(false);
    qDebug()<<"MainWindow: On stopped...";
    if(recordMode){
        //QThread::currentThread()->msleep(2000);
        stimuluswidget->stopTimer();

    }

    if(nRecordings==1){
        //Disable the game widget so that the score is frozen
        stimuluswidget->disableGameWidget();
    }
    if(nRecordings==0){
        recordingDialog->showLastMessage();
        close();
    }

    performEvaluation();
}

bool MainWindow::setRecordFileName()
{
    QDir dir;
    QString fileName=QFileDialog::getSaveFileName(this,"Save video to...",dir.homePath(),"*.vid");
    //Check if the user is saving the same file again. Confirm overwrite
    if(fileName!=""){
        QFile file(fileName.split(".")[0]+".vid");
        if(file.exists()){
            //Confirm with user about it
            QMessageBox::StandardButton reply=QMessageBox::question(this,"Overwrite existing file?","This name was used the last time. Are you sure you want to overwrite?",QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
            if(reply==QMessageBox::Cancel){
                //Cancelled
                return false;
            }
            else if(reply==QMessageBox::No){
                //If user wants to rename,
                return setRecordFileName();
            }
            else{
                recordFileName=file.fileName();
                statusBar()->showMessage("Overwriting file "+recordFileName);
                return true;
            }
        }
        else{
            recordFileName=fileName;
            return true;
        }
    }
    else{
        return false;
    }

}

void MainWindow::playButtonPressed()
{

    if(!playing){
        //runEvaluation=ui->actionEnableEvaluate->isChecked();
        ui->actionPlay->setIcon(iconPause);
        ui->actionRecord->setEnabled(false);
        ui->actionEnableEvaluate->setEnabled(false);
        qDebug()<<ui->actionEnableEvaluate->isChecked();
        //ui->actionView_Results->setEnabled(false);
        ui->actionEnableEmbeddedTest->setEnabled(false);
        ui->actionRun_Evaluation->setEnabled(false);
        renderThread->play();
        oximeterWidget->CMS50eListenerRef()->startReading();
    }
    else{
        ui->actionPlay->setIcon(iconPlay);

        renderThread->pause();
        oximeterWidget->CMS50eListenerRef()->pauseReading();
    }

    playing=!playing;
    ui->actionStop->setEnabled(true);
    ui->actionManual_ROI->setEnabled(false);
}



void MainWindow::playButtonPressed(int msec)
{

    if(!playing){
        ui->actionPlay->setIcon(iconPause);
        ui->actionRecord->setEnabled(false);
        ui->actionEnableEvaluate->setEnabled(false);
        //ui->actionView_Results->setEnabled(false);
        ui->actionEnableEmbeddedTest->setEnabled(false);
        ui->actionRun_Evaluation->setEnabled(false);
        renderThread->play(msec);
        oximeterWidget->CMS50eListenerRef()->startReading();
    }
    else{
        ui->actionPlay->setIcon(iconPlay);
        renderThread->pause();
        oximeterWidget->CMS50eListenerRef()->pauseReading();
    }

    playing=!playing;
    ui->actionStop->setEnabled(true);
    ui->actionManual_ROI->setEnabled(false);
}


void MainWindow::recordButtonPressed(bool rec)
{

    if(rec){
        recordMode=rec;
        if(nRecordings==NUM_RECORDINGS && setRecordFileName()){

            statusBar()->showMessage("Save video file as .vid");
            //If recordFileName doesnt have .vid extension or a different extension, then save it as .vid
            QStringList tokens=recordFileName.split(".");
            //If an extension was given extract the name
            if(tokens.length()>1){
                recordFilenamePrefix=tokens[0];
            }
            else{
                recordFilenamePrefix=recordFileName;
            }
            recordFileName=recordFilenamePrefix+QString::number(NUM_RECORDINGS-nRecordings+1);
            qDebug()<<"MainWindow:recordButtonPressed(): recordFileName="<<recordFileName;
            //Add the three (or as many as required) names to recordFileNames
            //            recordFileNames.clear();
            //            for(int i=1;i<=3;i++){
            //                recordFileNames.push_back(recordFileName+QString::number(i)+".vid");
            //            }

        }
        else{
            recordFileName=recordFilenamePrefix+QString::number(NUM_RECORDINGS-nRecordings+1);
            qDebug()<<"Record filename: "<<recordFileName;
            ui->actionRecord->setChecked(false);
        }
        if(!startRecording123()){
            ui->actionRecord->setChecked(false);
        }




    }

}

void MainWindow::evaluationButtonPressed(bool val)
{
    if(val){
        if(evaluationDialog->exec()==EvaluationDialog::Accepted){
            evaluationFileIndex=0;
            startEvaluation();
        }
        else{
            ui->actionRun_Evaluation->setChecked(false);
        }
    }
    else{
        //Stop evaluation
        setEvaluationFlag(false);
        ui->actionPlay->setEnabled(true);
        ui->actionRecord->setEnabled(true);
        ui->actionStop->setEnabled(true);

        stopButtonPressed();
    }
}



void MainWindow::showStatusMessage(QString message, bool isError)
{
    //qDebug()<<"Mainwindow: isError = "<<isError;
    if(isError){
        ui->statusBar->setStyleSheet("color: rgb(250,0,0);");
    }
    else{
        ui->statusBar->setStyleSheet("color: rgb(50,50,50);");
    }
    statusBar()->showMessage(tr(message.toStdString().c_str()));
}

void MainWindow::onVideoSourceSelected(QString videoSource)
{
    prevVideoSource=videoSource;
    prevCameraSource=-1;

    QFileInfo fileInfo(videoSource);
    video->setCaption(fileInfo.fileName().split(".")[0]);
    setUpVideoThread();
    if( renderThread->setVideoSource(videoSource,video)){
        //Set up flags for reading from metadata files.
        oximeterWidget->CMS50eListenerRef()->setLiveMode(false,videoSource);
        //CMS50EListener will read from the oximeter metadata file and take timestamps from the video metadata file
        ui->actionPlay->setEnabled(true);
        stimuluswidget->showICAWidget();
    }
    else{
        //Video was not loaded
        ui->actionPlay->setEnabled(false);
    }

}

void MainWindow::onVideoSourceSelected(int camera)
{
    prevCameraSource=camera;
    prevVideoSource="";
    setUpVideoThread();
    if( renderThread->setVideoSource(camera,video)){

        oximeterWidget->CMS50eListenerRef()->setLiveMode(true);
        ui->actionPlay->setEnabled(true);
    }
    else{
        //Camera was not loaded
        ui->actionPlay->setEnabled(false);
    }

}

void MainWindow::onVideoSourceSelected(QString videoSource, bool recordMode)
{
    prevVideoSource=videoSource;
    prevCameraSource=-1;

    QFileInfo fileInfo(videoSource);
    video->setCaption(fileInfo.fileName().split(".")[0]);

    qDebug()<<"onVideoSourceSelected(QString) calling setupVideoThread()";
    setUpVideoThread();
    if(!renderThread->setRecordMode(recordMode,recordFileName)){
        ui->actionRecord->setChecked(false);
    }

    if( renderThread->setVideoSource(videoSource,video)){
        //Set up flags for reading from metadata files.
        oximeterWidget->CMS50eListenerRef()->setLiveMode(false,videoSource);
        //CMS50EListener will read from the oximeter metadata file and take timestamps from the video metadata file
        ui->actionPlay->setEnabled(true);
    }
    else{
        //Video was not loaded
        ui->actionPlay->setEnabled(false);
    }


}

void MainWindow::onVideoSourceSelected(int camera, bool reinit)
{
    prevCameraSource=camera;
    prevVideoSource="";
    qDebug()<<"onVideoSourceSelected(int) calling setupVideoThread()";
    setUpVideoThread();

    if(!renderThread->setRecordMode(true,recordFileName)){
        ui->actionRecord->setChecked(false);
    }

    if( renderThread->setVideoSource(camera,video)){

        oximeterWidget->CMS50eListenerRef()->setLiveMode(true);
        ui->actionPlay->setEnabled(true);
    }
    else{
        //Camera was not loaded
        ui->actionPlay->setEnabled(false);
    }
    qDebug()<<"End of onVideoSourceSelected";
}

void MainWindow::closeEvent(QCloseEvent *evt)
{
    //  renderThread->stop();
}



void MainWindow::onVideoResized(int w, int h)
{
    //qDebug()<<"Resized : "<<w<<","<<h;
    video->setMinimumHeight(h);
    video->setMinimumWidth(w);
    stimuluswidget->setMinimumHeight(h);
    //qDebug()<<"Resize window to : "<<this->width()+oximeterWidget->height();
    this->resize(this->width(),video->height()+oximeterWidget->height());
}
