#include "oximeterwidget.h"
#include "ui_oximeterwidget.h"
#include <QDebug>
OximeterWidget::OximeterWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OximeterWidget)
{
    ui->setupUi(this);
    waveformWidget=new WaveformWidget();
    this->layout()->addWidget(waveformWidget);
    waveformWidget->setWaveformLabel("Ground Truth");
    connectedIcon=QIcon(":/images/connected.png");
    disconnectedIcon=QIcon(":/images/disconnected.png");
    ui->btnConnectSerialPort->setIcon(connectedIcon);
    connect(ui->btnConnectSerialPort,SIGNAL(clicked()),this,SLOT(onConnectPressed()));
    connect(this,SIGNAL(sendSignalToSerialPort(bool)),&cms50eListener,SLOT(controlSignalReceived(bool)));
    connect(ui->btnGridOn,SIGNAL(toggled(bool)),waveformWidget,SLOT(setGridOn(bool)));

    connect(&cms50eListener,SIGNAL(message(QString,bool)),this,SLOT(showStatusMessage(QString,bool)),Qt::QueuedConnection);
    connect(&cms50eListener,SIGNAL(connectStatus(bool)),this,SLOT(onConnectStatusReceived(bool)));
    connect(&cms50eListener,SIGNAL(heartRateSPOCalculated(int,int)),this,SLOT(heartRateSPOReceived(int,int)),Qt::QueuedConnection);
    connect(&cms50eListener,SIGNAL(waveFormY(int,quint64)),waveformWidget,SLOT(setWaveformY(int,quint64)),Qt::QueuedConnection);

    connect(this,SIGNAL(updateWaveformWidgetView()),waveformWidget,SLOT(updateView()));
    connected=false;
    ui->btnGridOn->setChecked(true);
}

OximeterWidget::~OximeterWidget()
{
    delete ui;
}

CMS50eListener *OximeterWidget::CMS50eListenerRef()
{
    return &cms50eListener;
}

void OximeterWidget::onConnectPressed()
{
     ui->btnConnectSerialPort->setEnabled(false);
   //Send signal for connection and disable the button
    emit sendSignalToSerialPort(!connected);

}

void OximeterWidget::onConnectStatusReceived(bool connectStatus)
{
    connected=connectStatus;
    if(connected){
        ui->btnConnectSerialPort->setIcon(disconnectedIcon);
        ui->btnConnectSerialPort->setText("Disconnect");
    }
    else{
        ui->btnConnectSerialPort->setIcon(connectedIcon);
        ui->btnConnectSerialPort->setText("Connect");
    }
    ui->btnConnectSerialPort->setEnabled(true);
    updateGeometry();
}

void OximeterWidget::showStatusMessage(QString msg, bool isError)
{
    if(isError){
    ui->lblMessage->setStyleSheet("color: rgb(250,0,0);");
    }
    else{
    ui->lblMessage->setStyleSheet("color: rgb(50,50,50);");
    }
    if(prevMsg!=msg)
        ui->lblMessage->setText("<font size=2>"+prevMsg+"</font><br>"+"<font  size=4><i>"+msg+"</i></font>");
    prevMsg=msg;
    this->update();
}

void OximeterWidget::heartRateSPOReceived(int heartRate,int oxygenSat)
{
    if(heartRate!=0){
        if(heartRate<10)
            heartRate+=127;
        ui->lblHeartRate->setText(QString::number(heartRate));

    }
    if(oxygenSat!=0){
        ui->lblSPO2->setText(QString::number(oxygenSat));
    }
}

void OximeterWidget::newFrameArrived(int frameid)
{
    //This signal will be emitted by renderthread only when livemode is being used
    cms50eListener.newFrameArrived(frameid);
    emit updateWaveformWidgetView();
}

void OximeterWidget::onOxiDataArrived(OxiData oxidata)
{
    //Extract measurements and waveform value
   // qDebug()<<"Oximeter Widget::onOxiDataArrived: Got oxidata at timestamp="<<oxidata.timestamp<<"waveformvalue="<<oxidata.waveformValue;
    heartRateSPOReceived(oxidata.heartRate,oxidata.oxygenSaturation);
    waveformWidget->setWaveformY(oxidata.waveformValue,oxidata.timestamp);
}

