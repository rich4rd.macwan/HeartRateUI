#ifndef STIMULUSWIDGET_H
#define STIMULUSWIDGET_H
#include <QTimer>
#include <QWidget>
#include "musicplayer.h"
#include "waveformwidget.h"
namespace Ui {
class StimulusWidget;
}

class StimulusWidget : public QWidget
{
    Q_OBJECT

public:
    explicit StimulusWidget(QWidget *parent = 0);
    ~StimulusWidget();
    static QTimer *timer;
    void startTimer();
    void stopTimer();
    void setupRecording1();
    void setupRecording2();
    void setupRecording3();
    void setRecordLength(int secs);
    void showGameWidget();
    void disableGameWidget();
    void showICAWidget();
    MusicPlayer* getMusicPlayerRef();
signals:
    void sendRPPGSignal(double);
public slots:
    void timerElapsed();
    void setBlue();
    void btnPlayerClicked();
    void setRPPGSignal(std::vector<double> rppgValues);
private:
    Ui::StimulusWidget *ui;
    std::vector<QString> gameUrls;
    int m,s;
    MusicPlayer musicPlayer;
    std::vector<QString> playlist;
    int gameid;
    bool blink;
    int recordLengthInSeconds;
    QString fontStyleSheet;
    QTimer blinkTimer;
    std::vector<WaveformWidget*> icaSourceWidgets;

};

#endif // MENTALCALCULATIONWIDGET_H
