#include "oxidata.h"

OxiData::OxiData(){
    frameId=0;
    heartRate=0;
    oxygenSaturation=0;
    waveformValue=0;
    timestamp=0;
}

OxiData::OxiData(int frameid, int heartrate, int oxygensat, int waveformvalue, int ts)
{
    frameId=frameid;
    heartRate=heartrate;
    oxygenSaturation=oxygensat;
    waveformValue=waveformvalue;
    timestamp=ts;
}

OxiData::~OxiData()
{

}

