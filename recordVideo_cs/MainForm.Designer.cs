﻿namespace recorderCS
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelDisplay = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBoxDisplay = new System.Windows.Forms.PictureBox();
            this.toolStripMain = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonOpenFreerun = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonExit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonFitToWnd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1To1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1To2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonLoadParam = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSettings = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonVideoRec = new System.Windows.Forms.ToolStripButton();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelFPS = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelFailed = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelFrameCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelCamera = new System.Windows.Forms.ToolStripStatusLabel();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.loadParameterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemLoadParameterFromFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemLoadParameterFromEEPROM = new System.Windows.Forms.ToolStripMenuItem();
            this.saveParameterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSaveParameterToFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSaveParameterToEEPROM = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.cameraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemOpenCameraLive = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemCloseCamera = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemApiErrorReport = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemFitToWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1To1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1To2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemMirrorRightLeft = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemMirrorUpDown = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panelDisplay.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDisplay)).BeginInit();
            this.toolStripMain.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelDisplay
            // 
            this.panelDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelDisplay.AutoScroll = true;
            this.panelDisplay.Controls.Add(this.panel1);
            this.panelDisplay.Controls.Add(this.pictureBoxDisplay);
            this.panelDisplay.Location = new System.Drawing.Point(12, 66);
            this.panelDisplay.Name = "panelDisplay";
            this.panelDisplay.Size = new System.Drawing.Size(802, 746);
            this.panelDisplay.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 593);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(768, 150);
            this.panel1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(2);
            this.label1.Size = new System.Drawing.Size(4, 29);
            this.label1.TabIndex = 0;
            //this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // pictureBoxDisplay
            // 
            this.pictureBoxDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxDisplay.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxDisplay.Image = global::recorderCS.Properties.Resources.uEyeLogo;
            this.pictureBoxDisplay.Location = new System.Drawing.Point(12, 14);
            this.pictureBoxDisplay.Name = "pictureBoxDisplay";
            this.pictureBoxDisplay.Size = new System.Drawing.Size(768, 576);
            this.pictureBoxDisplay.TabIndex = 1;
            this.pictureBoxDisplay.TabStop = false;
            // 
            // toolStripMain
            // 
            this.toolStripMain.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonOpenFreerun,
            this.toolStripButtonExit,
            this.toolStripSeparator1,
            this.toolStripSeparator2,
            this.toolStripButtonFitToWnd,
            this.toolStripButton1To1,
            this.toolStripButton1To2,
            this.toolStripSeparator3,
            this.toolStripButtonLoadParam,
            this.toolStripButtonSettings,
            this.toolStripSeparator4,
            this.toolStripButtonVideoRec});
            this.toolStripMain.Location = new System.Drawing.Point(0, 24);
            this.toolStripMain.Name = "toolStripMain";
            this.toolStripMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStripMain.Size = new System.Drawing.Size(834, 39);
            this.toolStripMain.Stretch = true;
            this.toolStripMain.TabIndex = 1;
            this.toolStripMain.Text = "toolStripMain";
            // 
            // toolStripButtonOpenFreerun
            // 
            this.toolStripButtonOpenFreerun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOpenFreerun.Image = global::recorderCS.Properties.Resources.Camera_Open_Freerun;
            this.toolStripButtonOpenFreerun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpenFreerun.Name = "toolStripButtonOpenFreerun";
            this.toolStripButtonOpenFreerun.Size = new System.Drawing.Size(36, 36);
            this.toolStripButtonOpenFreerun.Text = "Open Freerun";
            this.toolStripButtonOpenFreerun.Click += new System.EventHandler(this.toolStripButtonOpenFreerun_Click);
            // 
            // toolStripButtonExit
            // 
            this.toolStripButtonExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonExit.Enabled = false;
            this.toolStripButtonExit.Image = global::recorderCS.Properties.Resources.Camera_Exit;
            this.toolStripButtonExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonExit.Name = "toolStripButtonExit";
            this.toolStripButtonExit.Size = new System.Drawing.Size(36, 36);
            this.toolStripButtonExit.Text = "Exit";
            this.toolStripButtonExit.Click += new System.EventHandler(this.toolStripButtonExit_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButtonFitToWnd
            // 
            this.toolStripButtonFitToWnd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFitToWnd.Enabled = false;
            this.toolStripButtonFitToWnd.Image = global::recorderCS.Properties.Resources.Display_FitWindow_1;
            this.toolStripButtonFitToWnd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFitToWnd.Name = "toolStripButtonFitToWnd";
            this.toolStripButtonFitToWnd.Size = new System.Drawing.Size(36, 36);
            this.toolStripButtonFitToWnd.Text = "FitToWnd";
            this.toolStripButtonFitToWnd.ToolTipText = "Fit to Window";
            this.toolStripButtonFitToWnd.Click += new System.EventHandler(this.toolStripButtonFitToWnd_Click);
            // 
            // toolStripButton1To1
            // 
            this.toolStripButton1To1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1To1.Enabled = false;
            this.toolStripButton1To1.Image = global::recorderCS.Properties.Resources.Display_1_1;
            this.toolStripButton1To1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1To1.Name = "toolStripButton1To1";
            this.toolStripButton1To1.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton1To1.Text = "toolStripButton1To1";
            this.toolStripButton1To1.ToolTipText = "Display 1 to 1";
            this.toolStripButton1To1.Click += new System.EventHandler(this.toolStripButton1To1_Click);
            // 
            // toolStripButton1To2
            // 
            this.toolStripButton1To2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1To2.Enabled = false;
            this.toolStripButton1To2.Image = global::recorderCS.Properties.Resources.Display_1_2;
            this.toolStripButton1To2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1To2.Name = "toolStripButton1To2";
            this.toolStripButton1To2.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton1To2.Text = "toolStripButton1To2";
            this.toolStripButton1To2.ToolTipText = "Display 1 to 2";
            this.toolStripButton1To2.Click += new System.EventHandler(this.toolStripButton1To2_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButtonLoadParam
            // 
            this.toolStripButtonLoadParam.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLoadParam.Enabled = false;
            this.toolStripButtonLoadParam.Image = global::recorderCS.Properties.Resources.saveImage;
            this.toolStripButtonLoadParam.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLoadParam.Name = "toolStripButtonLoadParam";
            this.toolStripButtonLoadParam.Size = new System.Drawing.Size(36, 36);
            this.toolStripButtonLoadParam.Text = "Load parameters from file";
            this.toolStripButtonLoadParam.Click += new System.EventHandler(this.toolStripMenuItemLoadParameterFromFile_Click);
            // 
            // toolStripButtonSettings
            // 
            this.toolStripButtonSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSettings.Enabled = false;
            this.toolStripButtonSettings.Image = global::recorderCS.Properties.Resources.Camera_Settings;
            this.toolStripButtonSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSettings.Name = "toolStripButtonSettings";
            this.toolStripButtonSettings.Size = new System.Drawing.Size(36, 36);
            this.toolStripButtonSettings.Text = "toolStripButtonSettings";
            this.toolStripButtonSettings.ToolTipText = "Settings";
            this.toolStripButtonSettings.Click += new System.EventHandler(this.toolStripButtonSettings_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButtonVideoRec
            // 
            this.toolStripButtonVideoRec.CheckOnClick = true;
            this.toolStripButtonVideoRec.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonVideoRec.Enabled = false;
            this.toolStripButtonVideoRec.Image = global::recorderCS.Properties.Resources.video;
            this.toolStripButtonVideoRec.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonVideoRec.Name = "toolStripButtonVideoRec";
            this.toolStripButtonVideoRec.Size = new System.Drawing.Size(36, 36);
            this.toolStripButtonVideoRec.Text = "Record video";
            this.toolStripButtonVideoRec.Click += new System.EventHandler(this.toolStripButtonVideoRec_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelFPS,
            this.toolStripStatusLabelFailed,
            this.toolStripStatusLabelFrameCount,
            this.toolStripStatusLabelCamera});
            this.statusStrip.Location = new System.Drawing.Point(0, 801);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusStrip.Size = new System.Drawing.Size(834, 22);
            this.statusStrip.TabIndex = 2;
            // 
            // toolStripStatusLabelFPS
            // 
            this.toolStripStatusLabelFPS.Name = "toolStripStatusLabelFPS";
            this.toolStripStatusLabelFPS.Size = new System.Drawing.Size(29, 17);
            this.toolStripStatusLabelFPS.Text = ":FPS";
            this.toolStripStatusLabelFPS.Visible = false;
            // 
            // toolStripStatusLabelFailed
            // 
            this.toolStripStatusLabelFailed.Name = "toolStripStatusLabelFailed";
            this.toolStripStatusLabelFailed.Size = new System.Drawing.Size(41, 17);
            this.toolStripStatusLabelFailed.Text = ":Failed";
            this.toolStripStatusLabelFailed.Visible = false;
            // 
            // toolStripStatusLabelFrameCount
            // 
            this.toolStripStatusLabelFrameCount.Name = "toolStripStatusLabelFrameCount";
            this.toolStripStatusLabelFrameCount.Size = new System.Drawing.Size(48, 17);
            this.toolStripStatusLabelFrameCount.Text = ":Frames";
            this.toolStripStatusLabelFrameCount.Visible = false;
            // 
            // toolStripStatusLabelCamera
            // 
            this.toolStripStatusLabelCamera.Name = "toolStripStatusLabelCamera";
            this.toolStripStatusLabelCamera.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripStatusLabelCamera.Size = new System.Drawing.Size(51, 17);
            this.toolStripStatusLabelCamera.Text = "Camera:";
            this.toolStripStatusLabelCamera.Visible = false;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator6,
            this.loadParameterToolStripMenuItem,
            this.saveParameterToolStripMenuItem,
            this.toolStripSeparator7,
            this.toolStripMenuItemExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(154, 6);
            // 
            // loadParameterToolStripMenuItem
            // 
            this.loadParameterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemLoadParameterFromFile,
            this.toolStripMenuItemLoadParameterFromEEPROM});
            this.loadParameterToolStripMenuItem.Name = "loadParameterToolStripMenuItem";
            this.loadParameterToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.loadParameterToolStripMenuItem.Text = "Load parameter";
            // 
            // toolStripMenuItemLoadParameterFromFile
            // 
            this.toolStripMenuItemLoadParameterFromFile.Enabled = false;
            this.toolStripMenuItemLoadParameterFromFile.Name = "toolStripMenuItemLoadParameterFromFile";
            this.toolStripMenuItemLoadParameterFromFile.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuItemLoadParameterFromFile.Text = "from file...";
            this.toolStripMenuItemLoadParameterFromFile.Click += new System.EventHandler(this.toolStripMenuItemLoadParameterFromFile_Click);
            // 
            // toolStripMenuItemLoadParameterFromEEPROM
            // 
            this.toolStripMenuItemLoadParameterFromEEPROM.Enabled = false;
            this.toolStripMenuItemLoadParameterFromEEPROM.Name = "toolStripMenuItemLoadParameterFromEEPROM";
            this.toolStripMenuItemLoadParameterFromEEPROM.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuItemLoadParameterFromEEPROM.Text = "from EEPROM";
            this.toolStripMenuItemLoadParameterFromEEPROM.Click += new System.EventHandler(this.toolStripMenuItemLoadParameterFromEEPROM_Click);
            // 
            // saveParameterToolStripMenuItem
            // 
            this.saveParameterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemSaveParameterToFile,
            this.toolStripMenuItemSaveParameterToEEPROM});
            this.saveParameterToolStripMenuItem.Name = "saveParameterToolStripMenuItem";
            this.saveParameterToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.saveParameterToolStripMenuItem.Text = "Save parameter";
            // 
            // toolStripMenuItemSaveParameterToFile
            // 
            this.toolStripMenuItemSaveParameterToFile.Enabled = false;
            this.toolStripMenuItemSaveParameterToFile.Name = "toolStripMenuItemSaveParameterToFile";
            this.toolStripMenuItemSaveParameterToFile.Size = new System.Drawing.Size(134, 22);
            this.toolStripMenuItemSaveParameterToFile.Text = "to file...";
            this.toolStripMenuItemSaveParameterToFile.Click += new System.EventHandler(this.toolStripMenuItemSaveParameterToFile_Click);
            // 
            // toolStripMenuItemSaveParameterToEEPROM
            // 
            this.toolStripMenuItemSaveParameterToEEPROM.Enabled = false;
            this.toolStripMenuItemSaveParameterToEEPROM.Name = "toolStripMenuItemSaveParameterToEEPROM";
            this.toolStripMenuItemSaveParameterToEEPROM.Size = new System.Drawing.Size(134, 22);
            this.toolStripMenuItemSaveParameterToEEPROM.Text = "to EEPROM";
            this.toolStripMenuItemSaveParameterToEEPROM.Click += new System.EventHandler(this.toolStripMenuItemSaveParameterToEEPROM_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(154, 6);
            // 
            // toolStripMenuItemExit
            // 
            this.toolStripMenuItemExit.Image = global::recorderCS.Properties.Resources.exit;
            this.toolStripMenuItemExit.Name = "toolStripMenuItemExit";
            this.toolStripMenuItemExit.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItemExit.Text = "Exit";
            this.toolStripMenuItemExit.Click += new System.EventHandler(this.toolStripMenuItemExit_Click);
            // 
            // cameraToolStripMenuItem
            // 
            this.cameraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemOpenCameraLive,
            this.toolStripMenuItemCloseCamera,
            this.toolStripSeparator8,
            this.toolStripMenuItemApiErrorReport});
            this.cameraToolStripMenuItem.Name = "cameraToolStripMenuItem";
            this.cameraToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.cameraToolStripMenuItem.Text = "Camera";
            // 
            // toolStripMenuItemOpenCameraLive
            // 
            this.toolStripMenuItemOpenCameraLive.Image = global::recorderCS.Properties.Resources.Camera_Open_Freerun;
            this.toolStripMenuItemOpenCameraLive.Name = "toolStripMenuItemOpenCameraLive";
            this.toolStripMenuItemOpenCameraLive.Size = new System.Drawing.Size(166, 22);
            this.toolStripMenuItemOpenCameraLive.Text = "Open camera live";
            this.toolStripMenuItemOpenCameraLive.Click += new System.EventHandler(this.toolStripButtonOpenFreerun_Click);
            // 
            // toolStripMenuItemCloseCamera
            // 
            this.toolStripMenuItemCloseCamera.Enabled = false;
            this.toolStripMenuItemCloseCamera.Image = global::recorderCS.Properties.Resources.Camera_Exit;
            this.toolStripMenuItemCloseCamera.Name = "toolStripMenuItemCloseCamera";
            this.toolStripMenuItemCloseCamera.Size = new System.Drawing.Size(166, 22);
            this.toolStripMenuItemCloseCamera.Text = "Close camera";
            this.toolStripMenuItemCloseCamera.Click += new System.EventHandler(this.toolStripButtonExit_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(163, 6);
            // 
            // toolStripMenuItemApiErrorReport
            // 
            this.toolStripMenuItemApiErrorReport.CheckOnClick = true;
            this.toolStripMenuItemApiErrorReport.Image = global::recorderCS.Properties.Resources.apiErrorReport;
            this.toolStripMenuItemApiErrorReport.Name = "toolStripMenuItemApiErrorReport";
            this.toolStripMenuItemApiErrorReport.Size = new System.Drawing.Size(166, 22);
            this.toolStripMenuItemApiErrorReport.Text = "Api Error report";
            this.toolStripMenuItemApiErrorReport.Click += new System.EventHandler(this.toolStripMenuItemApiErrorReport_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayToolStripMenuItem,
            this.toolStripSeparator9,
            this.toolStripMenuItemMirrorRightLeft,
            this.toolStripMenuItemMirrorUpDown});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            this.viewToolStripMenuItem.DropDownOpening += new System.EventHandler(this.viewToolStripMenuItem_DropDownOpening);
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemFitToWindow,
            this.toolStripMenuItem1To1,
            this.toolStripMenuItem1To2});
            this.displayToolStripMenuItem.Image = global::recorderCS.Properties.Resources.monitor;
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.displayToolStripMenuItem.Text = "Display";
            // 
            // toolStripMenuItemFitToWindow
            // 
            this.toolStripMenuItemFitToWindow.CheckOnClick = true;
            this.toolStripMenuItemFitToWindow.Enabled = false;
            this.toolStripMenuItemFitToWindow.Image = global::recorderCS.Properties.Resources.Display_FitWindow_1;
            this.toolStripMenuItemFitToWindow.Name = "toolStripMenuItemFitToWindow";
            this.toolStripMenuItemFitToWindow.Size = new System.Drawing.Size(146, 22);
            this.toolStripMenuItemFitToWindow.Text = "Fit to window";
            this.toolStripMenuItemFitToWindow.Click += new System.EventHandler(this.toolStripButtonFitToWnd_Click);
            // 
            // toolStripMenuItem1To1
            // 
            this.toolStripMenuItem1To1.CheckOnClick = true;
            this.toolStripMenuItem1To1.Enabled = false;
            this.toolStripMenuItem1To1.Image = global::recorderCS.Properties.Resources.Display_1_1;
            this.toolStripMenuItem1To1.Name = "toolStripMenuItem1To1";
            this.toolStripMenuItem1To1.Size = new System.Drawing.Size(146, 22);
            this.toolStripMenuItem1To1.Text = "1 to 1";
            this.toolStripMenuItem1To1.Click += new System.EventHandler(this.toolStripButton1To1_Click);
            // 
            // toolStripMenuItem1To2
            // 
            this.toolStripMenuItem1To2.CheckOnClick = true;
            this.toolStripMenuItem1To2.Enabled = false;
            this.toolStripMenuItem1To2.Image = global::recorderCS.Properties.Resources.Display_1_2;
            this.toolStripMenuItem1To2.Name = "toolStripMenuItem1To2";
            this.toolStripMenuItem1To2.Size = new System.Drawing.Size(146, 22);
            this.toolStripMenuItem1To2.Text = "1 to 2";
            this.toolStripMenuItem1To2.Click += new System.EventHandler(this.toolStripButton1To2_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(160, 6);
            // 
            // toolStripMenuItemMirrorRightLeft
            // 
            this.toolStripMenuItemMirrorRightLeft.CheckOnClick = true;
            this.toolStripMenuItemMirrorRightLeft.Enabled = false;
            this.toolStripMenuItemMirrorRightLeft.Name = "toolStripMenuItemMirrorRightLeft";
            this.toolStripMenuItemMirrorRightLeft.Size = new System.Drawing.Size(163, 22);
            this.toolStripMenuItemMirrorRightLeft.Text = "Mirror Left/Right";
            this.toolStripMenuItemMirrorRightLeft.Click += new System.EventHandler(this.toolStripMenuItemMirrorRightLeft_Click);
            // 
            // toolStripMenuItemMirrorUpDown
            // 
            this.toolStripMenuItemMirrorUpDown.CheckOnClick = true;
            this.toolStripMenuItemMirrorUpDown.Enabled = false;
            this.toolStripMenuItemMirrorUpDown.Name = "toolStripMenuItemMirrorUpDown";
            this.toolStripMenuItemMirrorUpDown.Size = new System.Drawing.Size(163, 22);
            this.toolStripMenuItemMirrorUpDown.Text = "Mirror Up/Down";
            this.toolStripMenuItemMirrorUpDown.Click += new System.EventHandler(this.toolStripMenuItemMirrorUpDown_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.cameraToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(834, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // timer1
            // 
            this.timer1.Interval = 17;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 823);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStripMain);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panelDisplay);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "recorderCS";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panelDisplay.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDisplay)).EndInit();
            this.toolStripMain.ResumeLayout(false);
            this.toolStripMain.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelDisplay;
        private System.Windows.Forms.PictureBox pictureBoxDisplay;
        private System.Windows.Forms.ToolStrip toolStripMain;
        private System.Windows.Forms.ToolStripButton toolStripButtonOpenFreerun;
        private System.Windows.Forms.ToolStripButton toolStripButtonExit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButtonFitToWnd;
        private System.Windows.Forms.ToolStripButton toolStripButton1To1;
        private System.Windows.Forms.ToolStripButton toolStripButton1To2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButtonSettings;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButtonLoadParam;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelFPS;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelFailed;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelFrameCount;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelCamera;
        private System.Windows.Forms.ToolStripButton toolStripButtonVideoRec;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem loadParameterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLoadParameterFromFile;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLoadParameterFromEEPROM;
        private System.Windows.Forms.ToolStripMenuItem saveParameterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSaveParameterToFile;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSaveParameterToEEPROM;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExit;
        private System.Windows.Forms.ToolStripMenuItem cameraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemOpenCameraLive;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCloseCamera;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemApiErrorReport;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemFitToWindow;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1To1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1To2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemMirrorRightLeft;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemMirrorUpDown;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
    }
}

