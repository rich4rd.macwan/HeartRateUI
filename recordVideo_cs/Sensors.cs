﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Diagnostics;
using System.Media;
using System.Runtime.InteropServices;

namespace recorderCS
{
    public class SensorManager
    {
        #region --------------------------------- VARIABLES
        int nbSensor;
        List<Sensor> sensors;
        List<Thread> openDevices;
        #endregion
        #region --------------------------------- CONSTRUCT
        public SensorManager()
        {
            openDevices = new List<Thread>();
            nbSensor = 1;
            sensors = new List<Sensor>();
        }
        #endregion
        #region --------------------------------- METHODS
        //public string[] getDevices()
        //{
        //    List<string> ret = new List<string>();
        //    uint signature;
        //    uint numDevices;

        //    NGIO.SearchForDevices(NGIOptr, NGIO.DEVTYPE_LABQUEST_MINI, NGIO.COMM_TRANSPORT_USB, IntPtr.Zero, out signature);
        //    IntPtr hList = NGIO.OpenDeviceListSnapshot(NGIOptr, NGIO.DEVTYPE_LABQUEST_MINI, out numDevices, out signature);

        //    for (int i = 0; i < numDevices; ++i)
        //    {
        //        StringBuilder deviceName = new StringBuilder((int)NGIO.MAX_SIZE_DEVICE_NAME);
        //        uint status_mask;
        //        int status = NGIO.DeviceListSnapshot_GetNthEntry(hList, (uint)i, deviceName, NGIO.MAX_SIZE_DEVICE_NAME, out status_mask);
        //        if (status == 0)
        //        {
        //            string deviceName2 = deviceName.ToString();
        //            ret.Add(deviceName2);
        //        }
        //    }
        //    NGIO.CloseDeviceListSnapshot(hList);
        //    devices = ret.ToArray();
        //    return devices;
        //}
        ////----------------------------------------------
        //public void LED(byte color, byte bright)
        //{
        //    if (deviceOpened)
        //    {
        //        NGIOSetLedStateParams ledParams = new NGIOSetLedStateParams();
        //        ledParams.color = color;
        //        ledParams.brightness = bright;
        //        NGIO.Device_SendCmdAndGetResponse2(devicePtr, NGIO_ParmBlk.CMD_ID_SET_LED_STATE, ledParams, NGIO.TIMEOUT_MS_DEFAULT);
        //    }
        //}
        ////----------------------------------------------
        //string deviceName;
        //bool threadLaunched;
        //public void open(string deviceName)
        //{
        //    if (deviceOpened)
        //        close();

        //    Thread deviceThread = new Thread(deviceWork);
        //    this.deviceName = deviceName;
        //    deviceThread.Start();

        //    openDevices.Add(deviceThread);
        //    while (!threadLaunched) ;
        //    threadLaunched = false;
        //}
        //public void deviceWork()
        //{
        //    devicePtr = NGIO.Device_Open(NGIOptr, deviceName, 0);
        //    if (devicePtr != IntPtr.Zero)
        //    {
        //        deviceOpened = true;
        //        LED(NGIOSetLedStateParams.LED_COLOR_GREEN, NGIOSetLedStateParams.LED_BRIGHTNESS_MAX);
        //        setPeriod(0.01);
        //        NGIO.Device_SendCmdAndGetResponse4(devicePtr, NGIO_ParmBlk.CMD_ID_START_MEASUREMENTS,
        //            NGIO.TIMEOUT_MS_DEFAULT);
        //        setupSensors();
        //    }
        //    threadLaunched = true;

        //    while (libRun)
        //    {
        //        bool changed = update(false);
        //        if (changed)
        //        {
        //            EventHandler handler = setupChanged;
        //            if (null != handler) handler(this, EventArgs.Empty);
        //        }
        //        Thread.Sleep((int)(period * 100));
        //    }
        //}
        //public void open(int id)
        //{
        //    if (devices == null)
        //        getDevices();
        //    if (id > -1 & id < devices.Length)
        //        open(devices[id]);
        //}
        ////----------------------------------------------
        //public void close()
        //{
        //    libRun = false;
        //    foreach (Thread t in openDevices)
        //        t.Join();
        //    if (deviceOpened)
        //    {
        //        NGIO.Device_Close(devicePtr);
        //    }
        //    if(sensors!=null)
        //        foreach(Sensor s in sensors)
        //            s.stop();

        //    devicePtr = IntPtr.Zero;
        //    deviceOpened = false;
        //    NGIO.Uninit(NGIOptr);
        //}
        ////----------------------------------------------
        //public void setPeriod(double period)
        //{
        //    this.period = period;
        //    NGIO.Device_SetMeasurementPeriod(devicePtr, -1, period, NGIO.TIMEOUT_MS_DEFAULT);
        //}
        ////----------------------------------------------
        //public string getSensorsToString()
        //{
        //    if (sensors == null)
        //        setupSensors();

        //    update(false);
        //    string ret = "";
        //    foreach (Sensor s in sensors)
        //        ret += s.getUnit() + " . ";

        //    return ret;
        //}
        ////----------------------------------------------
        public Sensor[] getSensors()
        {
            return sensors.ToArray();
        }

        public void addSensor(Sensor newSensor)
        {
            newSensor.setPlace(nbSensor);
            sensors.Add(newSensor);
            nbSensor++;
        }
       
        #endregion
    }

    public interface Sensor
    {
        #region------------------------ SENSOR
        void updateSensor(bool onlyValue);
        bool isConnected();
        string getUnit();
        string[] getProUnit();
        int getSensorNumber();
        float getValue();
        float getLastValue();
        float[] getValues();
        float[] getProValue();
        float[] getLastProValue();
        int getPlace();
        void setPlace(int place);
        string getFileName();
        void stop();
        int getCalibration();
        void startRecording(string folder);
        void stopRecording();
        #endregion
    }

    
    public class OxySensor : Sensor
    {
        #region------------------------ OXYMETER:SENSOR
        OxyCMS50 oxy;
        bool opened;
        int value;
        int[] proValue;
        int[] lastProValue;
        int place;
        string unit;
        string[] proUnit;
        float lastValue;
        Stopwatch watch;
        public bool recording;
        string saveFileName;
        string saveFolderName;
        static Dictionary<string, Dictionary<int, List<float>>> savingData;


        public OxySensor(OxyCMS50 oxy)
        {
            proValue = new int[2];
            lastProValue = new int[2];
            unit = "(hwa)";
            proUnit = new string[]{"(BPM)","(%Sp02)"};
            this.oxy = oxy;
            oxy.pulseReceived += oxy_pulseReceived;
            updateSensor(false);
            saveFileName = "gtdump.xmp";
            savingData = new Dictionary<string, Dictionary<int, List<float>>>();
        }
        //----------------------------------------------
        public void oxy_pulseReceived(object sender, EventArgs e)
        {
            value = oxy.pulseWave;
            proValue[0] = oxy.heartRate;
            proValue[1] = oxy.SpO2;

            if (recording)
            {         
                int nowTime = (int)watch.ElapsedMilliseconds;
                string saveFile = saveFolderName + saveFileName;

                if (!savingData.ContainsKey(saveFile))
                    savingData.Add(saveFile, new Dictionary<int, List<float>>());

                savingData[saveFile].Add(nowTime, new List<float>());
                savingData[saveFile][nowTime].Add(proValue[0]);
                savingData[saveFile][nowTime].Add(proValue[1]);
                savingData[saveFile][nowTime].Add(value);
          
            }
        }
        //----------------------------------------------
        public void updateSensor(bool onlyValue)
        {
            opened = oxy.isOpened();
            if (!opened)
            {
                oxy.open();
            }
        }
        //----------------------------------------------
        public bool isConnected()
        {
            return oxy.isOpened();
        }
        //----------------------------------------------
        public string getUnit()
        {
            if (!isConnected())
                return "N/A";
            else
                return unit;
        }
        //----------------------------------------------
        public string[] getProUnit()
        {
            if (isConnected())
                return proUnit;
            else
                return new string[]{"N/A","N/A"};
        }
        //----------------------------------------------
        public int getSensorNumber()
        {
            return 5920;
        }
        //----------------------------------------------
        public float getValue()
        {
            return value;
        }
        //----------------------------------------------
        public float[] getValues()
        {
            float[]ret = new float[] { value };
            lastValue = value;
            return ret;
        }
        //----------------------------------------------
        public float[] getProValue()
        {
            lastProValue = proValue;
            return new float[]{proValue[0],proValue[1]};
        }
        //----------------------------------------------
        public float[] getLastProValue()
        {
            return new float[] { lastProValue[0], lastProValue[1] };
        }
        //----------------------------------------------
        public float getLastValue()
        {
            return lastValue;
        }
        //----------------------------------------------
        public int getPlace()
        {
            return place;
        }
        //----------------------------------------------
        public void setPlace(int place)
        {
            this.place = place;
        }
        //----------------------------------------------
        public void startRecording(string dataFolder)
        {
            this.recording = true;
            saveFolderName = dataFolder;
            watch = new Stopwatch();
            watch.Reset();
            watch.Start();
        }
        //----------------------------------------------
        public void stopRecording()
        {
            this.recording = false;
            foreach (string file in savingData.Keys)
            {
                foreach (int time in savingData[file].Keys)
                {
                    using (StreamWriter w = File.AppendText(file))
                    {
                        w.Write("\n" + time);
                    }
                    foreach (float value in savingData[file][time])
                    {
                        using (StreamWriter w = File.AppendText(file))
                        {
                            w.Write("," + value.ToString().Replace(',', '.'));
                        }
                    }
                }
            }
            
        }
        //----------------------------------------------
        public string getFileName()
        {
            return saveFileName;
        }
        //----------------------------------------------
        public void stop()
        {
            if (oxy.isOpened())
                oxy.close();
        }
        //----------------------------------------------
        public int getCalibration()
        {
            return 1;
        }
        #endregion
    }

}
