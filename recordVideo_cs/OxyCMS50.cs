﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;

namespace recorderCS
{
    public class OxyCMS50
    {
        #region -----------------------------------------------------VARIABLES
        public event EventHandler pulseReceived;
        SerialPort port;
        public int signalStrength;
        public bool searchingTooLong;
        public bool droppingOfSpO2;
        public int pulseWave;
        public int barGraph;
        public bool probeError;
        public bool searching;
        public bool heartPulse;
        public int heartRate;
        public int SpO2;
        public int[] lastPulseInfos;
        private bool calibration = true;
        private int id = 0;
        #endregion
        #region ----------------------------------------------------- OXYCMS50
        public OxyCMS50()
        {
            lastPulseInfos = new int[5];
        }
        #endregion
        #region ----------------------------------------------------- METHODS
        public bool open()
        {
            string[] ports = SerialPort.GetPortNames();
            if (ports.Length <= 0)
                return false;

            try
            {
                port = new SerialPort("COM1", 19200, Parity.Odd, 8, StopBits.One);
                if (!port.IsOpen)
                    port.Open();
                port.DataReceived += port_DataReceived;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

            return true;
        }
        public void close()
        {
            if (port!=null && port.IsOpen)
                port.Close();
        }
        public bool isOpened()
        {
            if (port == null)
                return false;
            return port.IsOpen;
        }
        #endregion
        #region ----------------------------------------------------- RECEIVE DATAS
        int[]spcheck;
        void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                while (id < 5)
                {
                    while (port.BytesToRead <= 0) ;
                    int v = port.ReadByte();
                    if(calibration && v == 40)
                    {
                        id = 1;
                        calibration = false;
                    }
                    lastPulseInfos[id] = v;
                    ++id;
                }
                id = 0;

                signalStrength = getValueFromByte((byte)lastPulseInfos[0], 0, 3);
                searchingTooLong = getBitFromByte((byte)lastPulseInfos[0], 4) == 1;
                droppingOfSpO2 = getBitFromByte((byte)lastPulseInfos[0], 5) == 1;

                pulseWave = getValueFromByte((byte)lastPulseInfos[1], 0, 6);

                barGraph = getValueFromByte((byte)lastPulseInfos[2], 0, 3);
                probeError = getBitFromByte((byte)lastPulseInfos[2], 4) == 1;
                searching = getBitFromByte((byte)lastPulseInfos[2], 5) == 1;
                heartPulse = getBitFromByte((byte)lastPulseInfos[2], 6) == 1;

                heartRate = getValueFromByte((byte)lastPulseInfos[3], 0, 6);
                SpO2 = getValueFromByte((byte)lastPulseInfos[4], 0, 6);

                int maxCheck = 5;
                if (spcheck==null)
                {
                    spcheck = new int[maxCheck];
                    for(int i=0;i<maxCheck;++i)
                    {
                        spcheck[i] = 0;
                    }
                }
                else
                {
                    bool assigned = false;
                    for (int i = 0; i < maxCheck && !assigned; ++i)
                    {
                        if(spcheck[i]==-1)
                        {
                            spcheck[i] = SpO2;
                            assigned = true;
                        }
                    }
                    if(!assigned)
                    {
                        int nbg = 0;
                        for (int i = 0; i < maxCheck && !assigned; ++i)
                        {
                            for (int j = i+1; j < maxCheck; ++j)
                            {
                                if (spcheck[i] == spcheck[j])
                                    nbg++;
                            }
                        }
                        if (nbg >= maxCheck - 1)
                            calibration = true;
                        spcheck = null;
                    }
                }

                EventHandler handler = pulseReceived;
                if (handler != null)
                    handler(this, EventArgs.Empty);
            }
            catch
            {
                port.Close();
            }
        }
        #endregion
        #region ----------------------------------------------------- PRIVATES
        private int getBitFromByte(byte b,int bitNumber)
        {
            return ((b & (1 << bitNumber)) != 0)?1:0;
        }
        private int getValueFromByte(byte b, int startBit, int endBit)
        {
            int ret = 0;
            int leng = endBit - startBit;

            for(int i=startBit;i<endBit+1;++i)
            {
                ret += getBitFromByte(b, i) * (int)Math.Pow(2,i);
            }
            return ret;
        }
        #endregion
    }
}
