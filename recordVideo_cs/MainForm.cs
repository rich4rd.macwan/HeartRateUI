﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace recorderCS
{
    public partial class MainForm : Form
    {

        #region Variables

        // our camera class
        uEye.Camera m_Camera;

        uEye.Defines.DisplayRenderMode m_RenderMode; // render mode
        Boolean m_IsLive; // saves the capture state
        Int32 m_s32FrameCount; 
        private Timer m_UpdateTimer = new Timer();

        // Application
        string dataFolder;
        Pen myPen;
        int[] sensorTimeCache;
        bool recording;

        // SPO2 sensors
        SensorManager sensorMgmt;
        Stopwatch watch;
        //------------ View
        Panel[] panels;
        Label[] labels;
        float[] panelIndex;
        SolidBrush panelColor;
        int valueMaxPerPanel;
        float[][] minmax;

        #endregion

        public MainForm()
        {
            InitializeComponent();
            this.FormClosing += MainFormClosing;

            // Check Runtime Version
            Version verMin = new Version(3, 5);
            Boolean bOk = false;
            foreach (Version ver in InstalledDotNetVersions())
            {
                if (ver >= verMin)
                {
                    bOk = true;
                    break;
                }
            }

            if (!bOk)
            {
                this.Load += CloseOnStart;
            }

            pictureBoxDisplay.SizeMode = PictureBoxSizeMode.CenterImage;
            this.pictureBoxDisplay.Width = panelDisplay.Width;
            this.pictureBoxDisplay.Height = panelDisplay.Height;

            // initialize camera object
            // camera is not opened here
            m_Camera = new uEye.Camera();

            m_IsLive = false;
            m_RenderMode = uEye.Defines.DisplayRenderMode.FitToWindow;

            m_UpdateTimer.Interval = 100;
            m_UpdateTimer.Tick += OnUpdateControls;

            // Sensor management
            sensorMgmt = new SensorManager();

            //dataFolder = "";

            panels = new Panel[] { panel1 }; // add here new panels
            labels = new Label[] { label1 };

            panelIndex = new float[panels.Length];
            panelColor = new SolidBrush(panel1.BackColor);

            // Create a new pen.
            myPen = new Pen(Brushes.Crimson);
            myPen.Width = 4.0F;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

            // ---- Sensor Mgmt
            OxyCMS50 oxy = new OxyCMS50();
            OxySensor os = new OxySensor(oxy);
            sensorMgmt.addSensor(os);
            

            //// ---- VIEW
            timer1.Start();

            foreach (Panel p in panels)
                p.Enabled = false;
        }

        void MainFormClosing(object sender, FormClosingEventArgs e)
        {

            m_Camera.Exit();

            //if (savingCache)
            //{
            //    MessageBox.Show("Des données venant du Record sont en cours d'enregistrement...",
            //        "Enregistrement en cours",
            //        MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    e.Cancel = true;
            //    return;
            //}
            //applicationRunning = false;

            //stopCamera();
            //stopLabQuest();
            timer1.Stop();
            /*
            Application.Exit();
            ProcessThreadCollection currentThreads = Process.GetCurrentProcess().Threads;
            foreach (ProcessThread thread in currentThreads)
            {
                Console.WriteLine("Thread - " + thread.Id+", "+ thread.ThreadState);
                if(thread.ThreadState.ToString()=="Running")
                {
                    Console.WriteLine("   " + thread.WaitReason);
                }
            }*/
        }

        private void CloseOnStart(object sender, EventArgs e)
        {
            MessageBox.Show(".NET Runtime Version 3.5.0 is required", "Runtime Error");
            this.Close();
        }

        public static System.Collections.ObjectModel.Collection<Version> InstalledDotNetVersions()
        {
            System.Collections.ObjectModel.Collection<Version> versions = new System.Collections.ObjectModel.Collection<Version>();
            Microsoft.Win32.RegistryKey NDPKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP");
            if (NDPKey != null)
            {
                string[] subkeys = NDPKey.GetSubKeyNames();
                foreach (string subkey in subkeys)
                {
                    GetDotNetVersion(NDPKey.OpenSubKey(subkey), subkey, versions);
                    GetDotNetVersion(NDPKey.OpenSubKey(subkey).OpenSubKey("Client"), subkey, versions);
                    GetDotNetVersion(NDPKey.OpenSubKey(subkey).OpenSubKey("Full"), subkey, versions);
                }
            }
            return versions;
        }

        private static void GetDotNetVersion(Microsoft.Win32.RegistryKey parentKey, string subVersionName, System.Collections.ObjectModel.Collection<Version> versions)
        {
            if (parentKey != null)
            {
                string installed = Convert.ToString(parentKey.GetValue("Install"));
                if (installed == "1")
                {
                    string version = Convert.ToString(parentKey.GetValue("Version"));
                    if (string.IsNullOrEmpty(version))
                    {
                        if (subVersionName.StartsWith("v"))
                            version = subVersionName.Substring(1);
                        else
                            version = subVersionName;
                    }

                    Version ver = new Version(version);

                    if (!versions.Contains(ver))
                        versions.Add(ver);
                }
            }
        }

        private void OnUpdateControls(object sender, EventArgs e)
        {
            // we update here our statusbar 
            Double dFramerate;
            m_Camera.Timing.Framerate.GetCurrentFps(out dFramerate);
            toolStripStatusLabelFPS.Text = "Fps: " + dFramerate.ToString("00.00");

            uEye.Types.CaptureStatus captureStatus;
            m_Camera.Information.GetCaptureStatus(out captureStatus);

            toolStripStatusLabelFailed.Text = "Failed: " + captureStatus.Total;
            toolStripStatusLabelFrameCount.Text = "Frames: " + m_s32FrameCount;
        }

        private uEye.Defines.Status initCamera()
        {
            CameraChoose chooseForm = new CameraChoose();
            uEye.Defines.Status statusRet = uEye.Defines.Status.NO_SUCCESS;
            
            if (chooseForm.ShowDialog() == DialogResult.OK)
            {
                statusRet = m_Camera.Init(chooseForm.DeviceID | (Int32)uEye.Defines.DeviceEnumeration.UseDeviceID, pictureBoxDisplay.Handle);
                if (statusRet != uEye.Defines.Status.SUCCESS)
                {
                    MessageBox.Show("Initializing the camera failed");
                    return statusRet;
                }
               
                statusRet = m_Camera.Memory.Allocate();
                if (statusRet != uEye.Defines.Status.SUCCESS)
                {
                    MessageBox.Show("Allocating memory failed");
                    return statusRet;
                }

                // set event
                m_Camera.EventFrame += onFrameEvent;

                // reset framecount
                m_s32FrameCount = 0;

                // start update timer for our statusbar
                m_UpdateTimer.Start();

                uEye.Types.SensorInfo sensorInfo;
                m_Camera.Information.GetSensorInfo(out sensorInfo);

                pictureBoxDisplay.SizeMode = PictureBoxSizeMode.Normal;
                toolStripStatusLabelCamera.Text = sensorInfo.SensorName;
                
         
            }

            return statusRet;
        }

        private void UpdateToolbar()
        {
            toolStripButton1To1.Enabled = m_Camera.IsOpened;
            toolStripButton1To2.Enabled = m_Camera.IsOpened;
            toolStripButtonFitToWnd.Enabled = m_Camera.IsOpened;

            // general function to update all mainwindow controls
            if (m_Camera.IsOpened)
            {
                // check if directrender or dib mode is active
                uEye.Defines.DisplayMode displayMode;
                m_Camera.Display.Mode.Get(out displayMode);
    
                toolStripButton1To2.Enabled = displayMode == uEye.Defines.DisplayMode.DiB;
            }

            // enable/disable controls
            //toolStripButtonAES.Enabled = m_Camera.IsOpened ? m_Camera.AutoFeatures.Software.Shutter.Supported : false;
            //toolStripButtonAWB.Enabled = m_Camera.IsOpened ? m_Camera.AutoFeatures.Software.WhiteBalance.Supported : false;
            toolStripButtonExit.Enabled = m_Camera.IsOpened;
            //toolStripButtonFreerun.Enabled = m_Camera.IsOpened;
            toolStripButtonSettings.Enabled = m_Camera.IsOpened;
            //toolStripButtonSnapshot.Enabled = m_Camera.IsOpened;
            toolStripButtonLoadParam.Enabled = m_Camera.IsOpened;
            //toolStripButtonLoadImage.Enabled = m_Camera.IsOpened;
            toolStripButtonVideoRec.Enabled = m_Camera.IsOpened;

            toolStripButtonOpenFreerun.Enabled = !m_Camera.IsOpened;
                       
            //toolStripButtonFreerun.Checked = m_Camera.IsOpened ? m_IsLive : false;
            toolStripButtonVideoRec.Checked = m_Camera.IsOpened ? m_Camera.Video.Running : false;

            toolStripMenuItemCloseCamera.Enabled = m_Camera.IsOpened;
            //toolStripMenuItemLoadImage.Enabled = m_Camera.IsOpened;
            toolStripMenuItemLoadParameterFromEEPROM.Enabled = m_Camera.IsOpened;
            toolStripMenuItemLoadParameterFromFile.Enabled = m_Camera.IsOpened;
            //toolStripMenuItemOpenCamera.Enabled = !m_Camera.IsOpened;
            toolStripMenuItemOpenCameraLive.Enabled = !m_Camera.IsOpened;
            //toolStripMenuItemSaveImage.Enabled = m_Camera.IsOpened;
            toolStripMenuItemSaveParameterToEEPROM.Enabled = m_Camera.IsOpened;
            toolStripMenuItemSaveParameterToFile.Enabled = m_Camera.IsOpened;

            //toolStripMenuItemSnapshot.Enabled = m_Camera.IsOpened;
            //toolStripMenuItemFreerun.Enabled = m_Camera.IsOpened;
            //toolStripMenuItemFreerun.Checked = m_Camera.IsOpened ? m_IsLive : false;

            toolStripMenuItem1To1.Enabled = m_Camera.IsOpened;
            toolStripMenuItem1To2.Enabled = m_Camera.IsOpened;
            toolStripMenuItemFitToWindow.Enabled = m_Camera.IsOpened;

            toolStripMenuItemMirrorRightLeft.Enabled = m_Camera.IsOpened;
            toolStripMenuItemMirrorUpDown.Enabled = m_Camera.IsOpened;
            //toolStripMenuItemCrosshair.Enabled = m_Camera.IsOpened;
            //toolStripMenuItemTimestamp.Enabled = m_Camera.IsOpened;
            toolStripMenuItemApiErrorReport.Enabled = m_Camera.IsOpened;

            toolStripButtonFitToWnd.Checked = false;
            toolStripButton1To1.Checked = false;
            toolStripButton1To2.Checked = false;

            toolStripMenuItemFitToWindow.Checked = false;
            toolStripMenuItem1To1.Checked = false;
            toolStripMenuItem1To2.Checked = false;

            toolStripStatusLabelFPS.Visible = m_Camera.IsOpened;
            toolStripStatusLabelFailed.Visible = m_Camera.IsOpened;
            toolStripStatusLabelFrameCount.Visible = m_Camera.IsOpened;
            toolStripStatusLabelCamera.Visible = m_Camera.IsOpened;

            if (m_Camera.IsOpened)
            {
                // update render mode
                switch (m_RenderMode)
                {
                    case uEye.Defines.DisplayRenderMode.FitToWindow:
                        toolStripButtonFitToWnd.Checked = true;
                        toolStripMenuItemFitToWindow.Checked = true;
                        break;

                    case uEye.Defines.DisplayRenderMode.Normal:
                        toolStripButton1To1.Checked = true;
                        toolStripMenuItem1To1.Checked = true;
                        break;

                    case uEye.Defines.DisplayRenderMode.DownScale_1_2:
                        toolStripButton1To2.Checked = true;
                        toolStripMenuItem1To2.Checked = true;
                        break;
                }

                // update 
                //Boolean isEnabled;
                //m_Camera.AutoFeatures.Software.WhiteBalance.GetEnable(out isEnabled);
                //toolStripButtonAWB.Checked = isEnabled;

                //m_Camera.AutoFeatures.Software.Shutter.GetEnable(out isEnabled);
                //toolStripButtonAES.Checked = isEnabled;
            }
        }

        // auto refresh for each frame
        private void onFrameEvent(object sender, EventArgs e)
        {
            // convert sender object to our camera object
            uEye.Camera camera = sender as uEye.Camera;

            if (camera.IsOpened)
            {
                uEye.Defines.DisplayMode mode;
                camera.Display.Mode.Get(out mode);

                // only display in dib mode
                if (mode == uEye.Defines.DisplayMode.DiB)
                {
                    Int32 s32MemID;
                    camera.Memory.GetActive(out s32MemID);
                    camera.Memory.Lock(s32MemID);

                    camera.Memory.Unlock(s32MemID);
                    camera.Display.Render(s32MemID, m_RenderMode);
                }

                ++m_s32FrameCount;
            }

        }

        //---------------------------------------- PANELS
        private void timer1_Tick(object sender, EventArgs e)
        {
            // event periodically and automatically called
            updateSensorView();
        }

        private void updateSensorView()
        {

            valueMaxPerPanel = 1000;
            Sensor[] sensors = sensorMgmt.getSensors();
            if (sensorTimeCache == null)
                sensorTimeCache = new int[sensorMgmt.getSensors().Length];

            // ------ Init minmax array ...
            if (minmax == null || minmax.Length < sensors.Length)
            {
                minmax = new float[sensors.Length][];
                for (int i = 0; i < sensors.Length; ++i)
                {
                    minmax[i] = new float[2];
                    minmax[i][0] = 10000;
                    minmax[i][1] = -10000;
                }
            }

            for (int i = 0; i < panels.Length; ++i)
            {
                if (i >= sensors.Length)
                    panels[i].Enabled = false;
            }

            // ------ for each sensors ...
            for (int i = 0; i < sensors.Length ; ++i)
            {
                Sensor sensor = sensors[i];
                Panel panel = panels[i];
                Label label = labels[i];
                Graphics graph = panel.CreateGraphics();
                float index = panelIndex[i];
                string displayText = "";
                int trueHeight = panel.Height - label.Height;

                // ------ sensor connected ...
                if (sensor.isConnected())
                {
                    float lastValue = sensor.getLastValue();
                    float[] values = sensor.getValues();
                    float[] proValue = sensor.getProValue();
                    string[] proUnit = sensor.getProUnit();
                    float value = sensor.getValue();
                    string unit = sensor.getUnit();
                    float indexAdder = (float)panel.Width / (float)valueMaxPerPanel;
                    string saveFile = dataFolder + sensor.getFileName();

                    minmax[i][0] = Math.Min(minmax[i][0], value);
                    minmax[i][1] = Math.Max(minmax[i][1], value);
                    float min = minmax[i][0];
                    float max = minmax[i][1];

                    float minmaxDiff = max - min;
                    minmaxDiff = minmaxDiff > 0 ? minmaxDiff : 1;

                    panel.Enabled = true;
                    
                    // ------ for each values ...
                    for (int v = 0; v < values.Length; ++v)
                    {
                        index = panelIndex[i];

                        value = values[v];
                        if (v > 0)
                        {
                            lastValue = values[v - 1];
                            value = values[v];
                        }

                        float drawlastValue = lastValue;
                        float drawValue = value;

                        drawlastValue -= min;
                        drawValue -= min;

                        drawlastValue = (drawlastValue * trueHeight) / minmaxDiff;
                        drawValue = (drawValue * trueHeight) / minmaxDiff;

                        drawlastValue = panel.Height - (drawlastValue + 1);
                        drawValue = panel.Height - (drawValue + 1);


                        graph.FillRectangle(panelColor, index, 0, indexAdder + 5, panel.Height);
                        graph.DrawLine(myPen, index - indexAdder, drawlastValue, index, drawValue);

                        panelIndex[i] += indexAdder;
                        if (panelIndex[i] > panel.Width)
                            panelIndex[i] = 0;                       
                    }

                    // ------ for each proceed values ...
                    for (int pv = 0; pv < proValue.Length; ++pv)
                    {
                        displayText += proUnit[pv] + " " + proValue[pv] + "\n" ;
                    }

                }
                else
                {
                    minmax[i][0] = 10000;
                    minmax[i][1] = -10000;

                    panel.Enabled = false;
                }
                label.Text = displayText;
            }
        }


        public int getTimeBetween(int lastTime, int nowTime, int valuesCount, int valueIndex)
        {
            int timeDifference = nowTime - lastTime;
            float timeRatio = (float)timeDifference / (float)valuesCount;
            float valueTime = ((float)lastTime + (timeRatio * (float)(valueIndex + 1)));
            return (int)valueTime;
        }

        #region Toolbar Events

        private void toolStripButtonOpenFreerun_Click(object sender, EventArgs e)
        {
            uEye.Defines.Status statusRet;
            statusRet = initCamera();

            if (statusRet == uEye.Defines.Status.SUCCESS)
            {
                // start capture
                statusRet = m_Camera.Acquisition.Capture();
                if (statusRet != uEye.Defines.Status.SUCCESS)
                {
                    MessageBox.Show("Starting live video failed");
                }
                else
                {
                    // everything is ok
                    m_IsLive = true;
                    UpdateToolbar();
                }
            }

            // cleanup on any camera error
            if (statusRet != uEye.Defines.Status.SUCCESS && m_Camera.IsOpened)
            {
                m_Camera.Exit();
            }
        }

        private void toolStripButtonExit_Click(object sender, EventArgs e)
        {
            m_UpdateTimer.Stop();

            m_IsLive = false;
            m_Camera.Exit();

            UpdateToolbar();

            // set correct display size
            this.pictureBoxDisplay.Width = panelDisplay.Width;
            this.pictureBoxDisplay.Height = panelDisplay.Height;
            this.pictureBoxDisplay.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;

            pictureBoxDisplay.Invalidate();
            pictureBoxDisplay.SizeMode = PictureBoxSizeMode.CenterImage;

            m_RenderMode = uEye.Defines.DisplayRenderMode.FitToWindow;
        }

        private void toolStripButtonFitToWnd_Click(object sender, EventArgs e)
        {
            // render mode == Fit to window
            m_RenderMode = uEye.Defines.DisplayRenderMode.FitToWindow;

            this.pictureBoxDisplay.Width = panelDisplay.Width;
            this.pictureBoxDisplay.Height = panelDisplay.Height;

            this.pictureBoxDisplay.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;

            // direct render
            uEye.Defines.DisplayMode mode;
            m_Camera.Display.Mode.Get(out mode);
            if (mode != uEye.Defines.DisplayMode.DiB)
            {
                m_Camera.DirectRenderer.SetScaling(true);
            }

            UpdateToolbar();
        }

        private void toolStripButton1To1_Click(object sender, EventArgs e)
        {
            // render mode == 1 to 1
            m_RenderMode = uEye.Defines.DisplayRenderMode.Normal;

            this.pictureBoxDisplay.Anchor = AnchorStyles.Top | AnchorStyles.Left;

            // get image size
            System.Drawing.Rectangle rect;
            m_Camera.Size.AOI.Get(out rect);

            this.pictureBoxDisplay.Width = rect.Width;
            this.pictureBoxDisplay.Height = rect.Height;

            // direct render
            uEye.Defines.DisplayMode mode;
            m_Camera.Display.Mode.Get(out mode);
            if (mode != uEye.Defines.DisplayMode.DiB)
            {
                m_Camera.DirectRenderer.SetScaling(false);
            }

            UpdateToolbar();
        }

        private void toolStripButton1To2_Click(object sender, EventArgs e)
        {
            // render mode == 1 to 2
            m_RenderMode = uEye.Defines.DisplayRenderMode.DownScale_1_2;

            this.pictureBoxDisplay.Anchor = AnchorStyles.Top | AnchorStyles.Left;

            // get image size
            System.Drawing.Rectangle rect;
            m_Camera.Size.AOI.Get(out rect);

            this.pictureBoxDisplay.Width = rect.Width / 2;
            this.pictureBoxDisplay.Height = rect.Height / 2;

            UpdateToolbar();
        }

        #endregion

        private void OnDisplayChanged(object sender, EventArgs e)
        {
            uEye.Defines.DisplayMode displayMode;
            m_Camera.Display.Mode.Get(out displayMode);

            // set scaling options
            if (displayMode != uEye.Defines.DisplayMode.DiB)
            {
                if (m_RenderMode == uEye.Defines.DisplayRenderMode.DownScale_1_2)
                {
                    m_RenderMode = uEye.Defines.DisplayRenderMode.Normal;

                    this.pictureBoxDisplay.Anchor = AnchorStyles.Top | AnchorStyles.Left;

                    // get image size
                    System.Drawing.Rectangle rect;
                    m_Camera.Size.AOI.Get(out rect);

                    this.pictureBoxDisplay.Width = rect.Width;
                    this.pictureBoxDisplay.Height = rect.Height;
                }
                else
                {
                    m_Camera.DirectRenderer.SetScaling(m_RenderMode == uEye.Defines.DisplayRenderMode.FitToWindow);
                }

                // update drawings
               // toolStripMenuItemCrosshair_Click(null, EventArgs.Empty);
            }
            else
            {
                if (m_RenderMode != uEye.Defines.DisplayRenderMode.FitToWindow)
                {
                    this.pictureBoxDisplay.Anchor = AnchorStyles.Top | AnchorStyles.Left;

                    // get image size
                    System.Drawing.Rectangle rect;
                    m_Camera.Size.AOI.Get(out rect);

                    if (m_RenderMode != uEye.Defines.DisplayRenderMode.Normal)
                    {

                        this.pictureBoxDisplay.Width = rect.Width / 2;
                        this.pictureBoxDisplay.Height = rect.Height / 2;
                    }
                    else
                    {
                        this.pictureBoxDisplay.Width = rect.Width;
                        this.pictureBoxDisplay.Height = rect.Height;
                    }
                }
            }

            UpdateToolbar();
        }

        private void toolStripButtonSettings_Click(object sender, EventArgs e)
        {
            uEye.Types.SensorInfo sensorInfo;
            m_Camera.Information.GetSensorInfo(out sensorInfo);

            if (sensorInfo.SensorID != uEye.Defines.Sensor.XS &&
                sensorInfo.SensorID != uEye.Defines.Sensor.UI1008_C &&
                sensorInfo.SensorID != uEye.Defines.Sensor.UI1013XC)
            {
                // avoid multiple instances
                SettingsForm settingsForm = new SettingsForm(m_Camera);
                settingsForm.SizeControl.AOIChanged += OnDisplayChanged;
                settingsForm.FormatControl.DisplayChanged += OnDisplayChanged;
                settingsForm.ShowDialog();

                //Boolean isEnabled;

                // check if any autofeature is enabled
                //m_Camera.AutoFeatures.Software.WhiteBalance.GetEnable(out isEnabled);
                //toolStripButtonAWB.Checked = isEnabled;

                //m_Camera.AutoFeatures.Software.Shutter.GetEnable(out isEnabled);
                //toolStripButtonAES.Checked = isEnabled;

                UpdateToolbar();
            }
            else
            {
                MessageBox.Show("Settings are not supported with this type of camera!");
            }
        }

       /* private void toolStripButtonAES_Click(object sender, EventArgs e)
        {
            Boolean isEnabled;
            m_Camera.AutoFeatures.Software.Shutter.GetEnable(out isEnabled);
            
            m_Camera.AutoFeatures.Software.Shutter.SetEnable(!isEnabled);
            toolStripButtonAES.Checked = !isEnabled;
        }*/

       /* private void toolStripButtonAWB_Click(object sender, EventArgs e)
        {
            Boolean isEnabled;
            m_Camera.AutoFeatures.Software.WhiteBalance.GetEnable(out isEnabled);

            m_Camera.AutoFeatures.Software.WhiteBalance.SetEnable(!isEnabled);
            toolStripButtonAWB.Checked = !isEnabled;
        }*/

       /* private void toolStripButtonSaveImage_Click(object sender, EventArgs e)
        {
            m_Camera.Image.Save("");
        }*/

       /* private void toolStripButtonLoadImage_Click(object sender, EventArgs e)
        {
            uEye.Defines.Status statusRet;
            statusRet = m_Camera.Image.Load("");

            if (statusRet == uEye.Defines.Status.SUCCESS)
            {
                // update drawing
                onFrameEvent(m_Camera, EventArgs.Empty);
            }
        }*/

       /* private void toolStripButtonAbout_Click(object sender, EventArgs e)
        {
            AboutBox aboutBox = new AboutBox();
            aboutBox.ShowDialog();
        }*/

        private void toolStripMenuItemLoadParameterFromFile_Click(object sender, EventArgs e)
        {
            if (m_IsLive)
            {
                m_Camera.Acquisition.Stop();
            }

            Int32[] memList;
            m_Camera.Memory.GetList(out memList);
            m_Camera.Memory.Free(memList);

            m_Camera.Parameter.Load("");

            uEye.Defines.ColorMode colorMode;
            m_Camera.PixelFormat.Get(out colorMode);

            // allocate new standard memory
            m_Camera.Memory.Allocate();

            if (m_IsLive)
            {
                m_Camera.Acquisition.Capture();
            }

            UpdateToolbar();
        }

        private void toolStripMenuItemLoadParameterFromEEPROM_Click(object sender, EventArgs e)
        {
            if (m_IsLive)
            {
                m_Camera.Acquisition.Stop();
            }

            Int32[] memList;
            m_Camera.Memory.GetList(out memList);
            m_Camera.Memory.Free(memList);

            m_Camera.Parameter.Load();

            uEye.Defines.ColorMode colorMode;
            m_Camera.PixelFormat.Get(out colorMode);

            // allocate new standard memory
            m_Camera.Memory.Allocate();

            if (m_IsLive)
            {
                m_Camera.Acquisition.Capture();
            }

            UpdateToolbar();
        }

        private void toolStripMenuItemSaveParameterToFile_Click(object sender, EventArgs e)
        {
            m_Camera.Parameter.Save("");
        }

        private void toolStripMenuItemSaveParameterToEEPROM_Click(object sender, EventArgs e)
        {
            m_Camera.Parameter.Save();
        }

        private void toolStripMenuItemExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void toolStripMenuItemMirrorRightLeft_Click(object sender, EventArgs e)
        {
            m_Camera.RopEffect.Set(uEye.Defines.RopEffectMode.LeftRight, toolStripMenuItemMirrorRightLeft.Checked);
        }

        private void toolStripMenuItemMirrorUpDown_Click(object sender, EventArgs e)
        {
            m_Camera.RopEffect.Set(uEye.Defines.RopEffectMode.UpDown, toolStripMenuItemMirrorUpDown.Checked);
        }

        private void toolStripMenuItemApiErrorReport_Click(object sender, EventArgs e)
        {
            m_Camera.Information.SetEnableErrorReport(toolStripMenuItemApiErrorReport.Checked);
        }

        private void viewToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if (m_Camera.IsOpened)
            {
                uEye.Defines.Status statusRet;

                // update selected ropeffect
                uEye.Defines.RopEffectMode ropMode;
                statusRet = m_Camera.RopEffect.Get(out ropMode);

                toolStripMenuItemMirrorRightLeft.Checked = (ropMode & uEye.Defines.RopEffectMode.LeftRight) == uEye.Defines.RopEffectMode.LeftRight;
                toolStripMenuItemMirrorUpDown.Checked = (ropMode & uEye.Defines.RopEffectMode.UpDown) == uEye.Defines.RopEffectMode.UpDown;

                uEye.Defines.DisplayMode displayMode;
                statusRet = m_Camera.Display.Mode.Get(out displayMode);

                // directrenderer
                //toolStripMenuItemTimestamp.Enabled = displayMode == uEye.Defines.DisplayMode.DiB;
            }
        }

        /*private void toolStripMenuItemCrosshair_Click(object sender, EventArgs e)
        {
            uEye.Defines.Status statusRet;

            uEye.Defines.DisplayMode mode;
            statusRet = m_Camera.Display.Mode.Get(out mode);

            // directrenderer
            if (mode != uEye.Defines.DisplayMode.DiB)
            {
                statusRet = m_Camera.DirectRenderer.SetWindowHandle(pictureBoxDisplay.Handle.ToInt32());
                statusRet = m_Camera.DirectRenderer.Overlay.SetVisible(toolStripMenuItemCrosshair.Checked);

                Graphics graphics;
                statusRet = m_Camera.DirectRenderer.Overlay.GetGraphics(out graphics);

                if (statusRet == uEye.Defines.Status.SUCCESS && graphics != null)
                {
                    DoDrawing(ref graphics, 0);
                    statusRet = m_Camera.DirectRenderer.Overlay.SetGraphics(ref graphics);
                }
            }
        }*/

        /*private void toolStripMenuItemFreerun_Click(object sender, EventArgs e)
        {
            uEye.Defines.Status statusRet;
            if (toolStripMenuItemFreerun.Checked)
            {
                // start capture
                statusRet = m_Camera.Acquisition.Capture();
                m_IsLive = true;
            }
            else
            {
                statusRet = m_Camera.Acquisition.Stop();
                m_IsLive = false;
            }

            UpdateToolbar();
        }*/

        private void toolStripButtonVideoRec_Click(object sender, EventArgs e)
        {
            uEye.Defines.Status statusRet = uEye.Defines.Status.SUCCESS;
            Sensor[] sensors = sensorMgmt.getSensors();

            if (toolStripButtonVideoRec.Checked)
            {
                // Create directory
                dataFolder = @"c:\data\" + DateTime.Now.ToString("yyyy_MM_dd-HH_mm_ss") + "\\";
                // Determine whether the directory exists.
                if (Directory.Exists(dataFolder))
                {
                    Console.WriteLine("That path exists already.");
                    return;
                }
                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(dataFolder);
                Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(dataFolder));

                m_Camera.Video.SetQuality(100);
                statusRet = m_Camera.Video.Start(dataFolder + "vid.avi");
                if (statusRet != uEye.Defines.Status.SUCCESS)
                {
                    MessageBox.Show("Could not start video recording");
                    toolStripButtonVideoRec.Checked = false;
                   
                    // ------ for each sensors ...
                    for (int i = 0; i < sensors.Length; ++i)
                    {
                        Sensor sensor = sensors[i];
                        sensor.stopRecording();
                        recording = false;
                    }
                   
                }
                else
                {
                    watch = new Stopwatch();
                    watch.Reset();
                    watch.Start();

                    // ------ for each sensors ...
                    for (int i = 0; i < sensors.Length; ++i)
                    {
                        Sensor sensor = sensors[i];
                        sensor.startRecording(dataFolder);
                        //recording = true;
                    }
                }
            }
            else
            {
                statusRet = m_Camera.Video.Stop();
                for (int i = 0; i < sensors.Length; ++i)
                {
                    Sensor sensor = sensors[i];
                    sensor.stopRecording();
                    recording = false;
                }
            }
        }
    }
}
